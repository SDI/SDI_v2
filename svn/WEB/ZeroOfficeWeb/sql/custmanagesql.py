#-*- coding: utf-8 -*-
'''
Created on 2015. 5. 11.

@author: 세민
'''

class CustSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getAllCust(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user"
        return result

    def getIsCust(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = %s"
        return result

    def getCustLogin(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = %s and password = (SELECT encode(encrypt(%s, 'gigaoffice!234', '3des'), 'hex'))"
        return result

    def getCustIndexLogin(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = %s"
        return result

    def getLoginModify(self, username):
        result = """select a.userid,
                    convert_from(decrypt(decode((select password from tb_user where userid = '%s'), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.username, a.userauth as userauth,
                    a.orgseq, b.orgname
                    from tb_user a join tb_org b on a.orgseq = b.orgseq where 1=1
                    and userid like '%s' and a.userauth = '0'""" % (str(username), str(username))
        return result

    def getCustRow(self, user, userid, username, orgseq):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.userid) AS R, a.orgseq
                    FROM tb_user a join tb_org b on a.orgseq = b.orgseq
                    inner join tb_user as auth
                    on b.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1 and a.userauth = 2"""
        if not userid == "" :
            result = result + " and a.userid like '%s'" % userid
        if not username == "" :
            result = result + " and a.username like '%s'" % username
        if not orgseq == "0" :
            result = result + " and b.orgseq = '%s'" % orgseq
        if not user == "" :
            result = result + " and auth.userid = '%s'" % user
        result = result + " group by a.userid, a.orgseq ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getCust(self, user, userid, username, orgseq, start):
        result = """select ROW_NUMBER() OVER(ORDER BY a.userid DESC) as rownum, a.userid, a.password, a.username,
                    case a.userauth
                    when '0' then '관리자'
                    when '1' then '운영자'
                    when '2' then '고객'
                    END as userauth,
                    a.orgseq, b.orgname, c.tenantid, c.telnum, c.email, c.customername,
                    case
                    when ns.procstatecode = '1' then 'y'
                    else 'n'
                    end as is_prov
                    from tb_user a join tb_org b on a.orgseq = b.orgseq
                    inner join tb_user as auth
                    on b.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    left join tb_customer as c
                    on c.userid = a.userid
                    left join tb_nsprovisioning as ns
                    on ns.del_dttm is null and a.userid = ns.userid
                    where 1=1 and a.userauth = 2"""
        if not userid == "" :
            result = result + " and a.userid like '%s'" % str(userid)
        if not username == "" :
            result = result + " and a.username like '%s'" % str(username)
        if not orgseq == "0" :
            result = result + " and b.orgseq = '%s'" % str(orgseq)
        if not user == "" :
            result = result + " and auth.userid = '%s'" % user
        result = result + " group by a.userid, a.password, a.username, a.userauth, a.orgseq, b.orgname, c.tenantid, c.telnum, c.email, c.customername, ns.procstatecode ORDER BY a.reg_dttm desc, a.userid DESC LIMIT 15 OFFSET '%s';" % start
        return result

    def setUserInsert(self, userid, password, username, userauth, orgseq):
        result = """insert into tb_user(userid, password, username, userauth, orgseq, reg_dttm)
                    values('%s', (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), '%s', '%s', '%s', now())""" % (userid, password, username, userauth, orgseq)
        return result

    def setCustInsert(self, custid, custname, custTenant, telnum, email):
        result = """insert into tb_customer(userid, customername, tenantid, telnum, email)
                    values('%s', '%s', '%s', '%s', '%s')""" % (custid, custname, custTenant, telnum, email)
        return result

    def getCustDetail(self, userid, orgseq):
        result = """select a.userid,
                    convert_from(decrypt(decode((select password from tb_user where userid = '%s' and orgseq = %s), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.username,
                    case a.userauth
                    when '0' then '관리자'
                    when '1' then '운영자'
                    when '2' then '고객'
                    END as userauth,
                    a.orgseq, b.orgname, c.tenantid, c.telnum, c.email, c.customername
                    from tb_user a join tb_org b on a.orgseq = b.orgseq
                    left join tb_customer as c
                    on c.userid = a.userid
                    where 1=1 and a.userauth = 2
                    and a.userid = '%s' and a.orgseq = %s""" % (str(userid), orgseq, str(userid), orgseq)
        return result

    def setUserUpdate(self, userid, password, username, orgseq, originOrgSeq):
        result = """update tb_user set password = (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')),  username = '%s', orgseq = '%s'
                    where userid = '%s' and orgseq = %s"""% (password, username, orgseq, userid, originOrgSeq)
        return result

    def setCustUpdate(self, custid, custname, custTenant, telnum, email):
        result = """update tb_customer set customername = '%s',  tenantid = '%s', telnum = '%s', email = '%s'
                    where userid = '%s'"""% (custname, custTenant, telnum, email, custid)
        return result

    def setUserDelete(self, userid, orgseq):
        result = "delete from tb_user where userid = '%s' and orgseq = %s" % (userid, orgseq)
        return result

    def setCustDelete(self, userid):
        result = "delete from tb_customer where userid = '%s'" % (userid)
        return result

    def setCustomer(self, customername, empcnt, telnum, hpnum, email):
        result = """insert into tb_customer(customername, emp_cnt, telnum, hp_num, email)
                    values('%s', '%s', '%s', '%s', '%s')""" % (customername, empcnt, telnum, hpnum, email)
        return result

    def setCustCustomer(self, userid, password, username, userauth, customerseq):
        result = """insert into tb_user(userid, password, username, userauth, customerseq)
                    values('%s', (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), '%s', '%s', '%s')""" % (userid, password, username, userauth, customerseq)
        return result

    def getCustomerSeq(self, customername, empcnt, telnum, hpnum, email):
        result = """SELECT customerseq FROM tb_customer WHERE customername = '%s'
                    AND  emp_cnt = '%s' AND telnum = '%s' AND hp_num = '%s'
                    AND email = '%s'""" % (customername, empcnt, telnum, hpnum, email)
        return result

    def getOrgs(self, userid):
        result = """select org.orgname, org.orgseq
                    from tb_org as org
                    inner join tb_user as u
                    on org.orgseq = u.orgseq or u.userauth = 0 or u.userauth = 99
                    where pop_yn = 'Y' and userid = '%s'
                    order by orgseq""" % userid
        return result

    def getCustCheck(self, userId):
        result = "SELECT * FROM tb_user WHERE userid = '%s'" % userId
        return result

    def getIsCustomer(self, userId):
        result = "select * from tb_customer where userid = '%s'" % userId
        return result

    def getOrgName(self, userid):
        result = """SELECT org.orgseq, org.orgname from tb_org as org
                    inner join tb_user as us
                    on us.orgseq = org.orgseq WHERE us.userid = '%s'""" % userid
        return result