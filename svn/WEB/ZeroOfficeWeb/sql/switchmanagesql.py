#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

class SwitchSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getSwitchRow(self, userid, orgseq):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY sw_seq) AS R
                    from tb_switch_info as sw
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not orgseq == 0 :
            result = result + " and sw.orgseq = '%s'" % orgseq
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getSwitch(self, userid, orgseq, start):
        result = """select ROW_NUMBER() OVER(ORDER BY sw.sw_seq DESC) as rownum, sw.sw_seq as sw_seq, sw.sw_name as sw_name,
                    sw.sw_mgmt_ip as sw_mgmt_ip, org.orgname as orgname, sw.sw_type as sw_type, sw.sw_port as sw_port, sw.sw_start||'~'||sw.sw_end as sw_range
                    from tb_switch_info as sw
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if orgseq != 0 :
            result = result + " and org.orgseq = '%s'" % orgseq
        result = result + " ORDER BY sw.reg_dttm DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self, userid):
        result = """select org.orgname, org.orgseq
                    from tb_org as org
                    inner join tb_user as u
                    on org.orgseq = u.orgseq or u.userauth = 0 or u.userauth = 99
                    where pop_yn = 'Y' and userid = '%s'
                    order by orgseq""" % userid
        return result

    def setSwitchInsert(self, sw_name, orgseq, sw_mgmt_ip, sw_type, sw_port, sw_start, sw_end):
        result = """insert into tb_switch_info(sw_name, orgseq, sw_mgmt_ip, reg_dttm, sw_type, sw_port, sw_start, sw_end)
                    values('%s', '%s', '%s', CURRENT_TIMESTAMP, '%s', '%s', '%s', '%s')""" % (sw_name, orgseq, sw_mgmt_ip, sw_type, sw_port, sw_start, sw_end)
        return result

    def getSwitchDetail(self, sw_seq):
        result = """select sw_name, orgseq, sw_mgmt_ip, sw_type, sw_port, sw_start, sw_end
                    from tb_switch_info where sw_seq = '%s'""" % sw_seq
        return result

    def setSwitchModify(self, sw_seq, sw_name, orgseq, sw_mgmt_ip, sw_type, sw_port, sw_start, sw_end):
        result = """update tb_switch_info set sw_name = '%s',  orgseq = '%s', sw_mgmt_ip = '%s', sw_type = '%s', sw_port = '%s', sw_start = '%s', sw_end = '%s'
                    where sw_seq = '%s'"""% (sw_name, orgseq, sw_mgmt_ip, sw_type, sw_port, sw_start, sw_end, sw_seq)
        return result

    def setSwitchDelete(self, sw_seq):
        result = "delete from tb_switch_info where sw_seq = '%s'" % sw_seq
        return result

    def setUtmConnDelete(self, sw_seq):
        result = "delete from tb_utm_conn_info where sw_seq = '%s'" % sw_seq
        return result

    def getSwitchCheck(self, mgmtIp):
        result = "SELECT * FROM tb_switch_info WHERE sw_mgmt_ip = '%s'" % mgmtIp
        return result