#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

class SmsSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getSmsRow(self, user):
        result = """SELECT count(*) AS R
                    from tb_user_failsms"""
        if not user == "" :
            result = result + " where username like '%s'" % user
        return result

    def getSms(self, start, user):
        result = """select ROW_NUMBER() OVER(ORDER BY smsseq DESC) as rownum, smsseq, username,
                    hp_num, to_char(ins_dttm, 'YYYY-MM-DD HH24:MI:SS') as ins_dttm, alarmtype, member, u.orgseq, orgname
                    from tb_user_failsms as u inner join tb_org as o on u.orgseq = o.orgseq"""

        if not user == "" :
            result = result + " where username like '%s'" % user

        result = result + """ ORDER BY ins_dttm DESC LIMIT 15 OFFSET '%s'"""  % start
        return result

    def setSmsInsert(self, user, hp, dep, orgs, types):
        result = """insert into tb_user_failsms(username, hp_num, member, orgseq, alarmtype)
                    values('%s', '%s', '%s', '%s', '%s')""" % (user, hp, dep, orgs, types)
        return result

    def setSmsModify(self, smsseq, user, hp, dep, orgs, types):
        result = """update tb_user_failsms set username = '%s',  hp_num = '%s', member = '%s', orgseq = '%s', alarmtype = '%s'
                    where smsseq = '%s'"""% (user, hp, dep, orgs, types, smsseq)
        return result

    def setSmsDelete(self, smsseq):
        result = "delete from tb_user_failsms where smsseq = '%s'" % smsseq
        return result

    def getOrg(self):
        result = "select * from tb_org order by orgseq"
        return result