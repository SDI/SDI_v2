#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 15.

@author: 세민
'''

class TemplateSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def selectTemplate(self):
        result = """select neclsseq, maincode, subcode, dtlcode, description, mainname, subname, dtlname, ico_small 
                    from tb_neclscode where maincode = '90' and dtlcode='S' order by neclsseq"""
        return result

    def getTemplateName(self):
        result = """select nstemplateseq, templatedesc from tb_nstemplate where del_dttm is null order by nstemplateseq asc"""
        return result

    def selectNeTableTemplate(self, templateseq):
        result = """SELECT a.icon_large, n.procstatecode, n.console_url, n.templateseq, t.templatedesc, 
                    n.provisionseq, n.neseq, n.neclsseq, n.config_props, c.subname, c.maincode, c.subcode, 
                    c.dtlcode, c.description, c.mainname, c.dtlname, c.ico_small
                    FROM tb_ne n
                    INNER JOIN tb_neclscode c ON n.neclsseq = c.neclsseq
                    INNER JOIN tb_nstemplate t ON n.templateseq = t.nstemplateseq
                    LEFT OUTER JOIN tb_necat a ON n.necatseq = a.necatseq
                    WHERE n.del_dttm is null AND n.usgstatuscode=9 and n.neusgcode='9001' 
                    AND n.templateseq = \'%s\' ORDER BY n.neclsseq ASC""" % templateseq
        return result
    
    def getTemplateRow(self, templateseq):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY neseq) AS R
                    FROM tb_ne
                    WHERE usgstatuscode=9 AND neusgcode='9001' AND templateseq = \'%s\' 
                    ORDER BY R DESC LIMIT 1 OFFSET 0;""" % templateseq
        return result
    
    def getNeclsSeq(self, templateseq):
        result = """SELECT neclsseq
                    FROM tb_ne
                    WHERE usgstatuscode=9 AND neusgcode='9001' AND templateseq = \'%s\'""" % templateseq
        return result
    
    def deleteTemplate(self, templateseq, neclsseq):
        result = """UPDATE tb_ne SET del_dttm = CURRENT_TIMESTAMP WHERE usgstatuscode=9 AND neusgcode='9001' AND 
                    templateseq = \'%s\' AND neclsseq = \'%s\'""" % (templateseq, neclsseq)
        return result
    
    def upTemplateNeclsSeq(self, neclsseq, templateseq):
        result = """update tb_ne SET usgstatuscode = 9 WHERE neclsseq = \'%s\' and neusgcode='9001' and templateseq = \'%s\'""" % (neclsseq, templateseq)
        return result

    def addNewTemplate(self, templateName, author):
        result = """insert into tb_nstemplate (templatedesc,authorid) values (\'%s\', \'%s\')""" % (templateName, author)
        return result

    def insertTemplate(self, neclsseq, displayseq, templateseq ):
        result = """insert into tb_ne (neclsseq, templateseq, neusgcode, usgstatuscode, displayseq) 
                    values (\'%s\',\'%s\','9001',9, \'%s\')""" % (neclsseq, templateseq, displayseq)
        return result
    
    def deleteNsTemplate(self, templateseq):
        result = """update tb_nstemplate set del_dttm = CURRENT_TIMESTAMP where nstemplateseq = \'%s\'""" % templateseq
        return result