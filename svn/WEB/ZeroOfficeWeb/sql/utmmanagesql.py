#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

class UtmSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getUtmRow(self):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY utm_seq) AS R
                    from tb_utm_info
                    ORDER BY R DESC LIMIT 1 OFFSET 0"""
        return result

    def getUtm(self, start):
        result = """select ROW_NUMBER() OVER(ORDER BY utm_seq DESC) as rownum, utm_seq, utm_name,
                    utm_mgmt_ip, utm_type
                    from tb_utm_info
                    ORDER BY reg_dttm DESC LIMIT 15 OFFSET '%s'"""  % start
        return result

    def setUtmInsert(self, utm_name, utm_mgmt_ip, utm_type):
        result = """insert into tb_utm_info(utm_name, utm_mgmt_ip, utm_type, reg_dttm)
                    values('%s', '%s', '%s', CURRENT_TIMESTAMP)""" % (utm_name, utm_mgmt_ip, utm_type)
        return result

    def getUtmDetail(self, utm_seq):
        result = """select utm_name, utm_mgmt_ip, utm_type
                    from tb_utm_info where utm_seq = '%s'""" % utm_seq
        return result

    def setUtmModify(self, utm_seq, utm_name, utm_mgmt_ip, utm_type):
        result = """update tb_utm_info set utm_name = '%s',  utm_mgmt_ip = '%s', utm_type = '%s'
                    where utm_seq = '%s'"""% (utm_name, utm_mgmt_ip, utm_type, utm_seq)
        return result

    def setUtmDelete(self, utm_seq):
        result = "delete from tb_utm_info where utm_seq = '%s'" % utm_seq
        return result

    def setUtmConnDelete(self, utm_seq):
        result = "delete from tb_utm_conn_info where utm_seq = '%s'" % utm_seq
        return result