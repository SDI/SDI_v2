#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

class HaSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getHaRow(self, userid, orgseq):
        result = """SELECT count(*) AS R
                    from tb_ha_info as ha
                    inner join tb_utm_conn_info as uci
                    on uci.utm_seq = ha.utm_seq
                    inner join tb_switch_info as sw
                    on uci.sw_seq = sw.sw_seq
                    inner join tb_utm_info as utm
                    on utm.utm_seq = uci.utm_seq
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not orgseq == 0 :
            result = result + " and sw.orgseq = '%s'" % orgseq
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getHa(self, userid, orgseq, start):
        result = """select ha.ha_seq as ha_seq, ha.ha_gname as ha_gname,
                    case
                    when ha.is_primary = 0 then 'Backup'
                    when ha.is_primary = 1 then 'Primary'
                    end as is_primary, ha.is_primary as is_primary_num, ha.ha_state as ha_state, ha.ha_pri as ha_pri,
                    uci.utm_seq as utm_seq, utm.utm_mgmt_ip as utm_mgmt_ip, utm.utm_type as utm_type,
                    sw.sw_name as sw_name, utm.utm_name as utm_name, uci.status as status, uci.sw_seq as sw_seq, org.orgname as orgname
                    from tb_ha_info as ha
                    inner join tb_utm_conn_info as uci
                    on uci.utm_seq = ha.utm_seq
                    inner join tb_switch_info as sw
                    on uci.sw_seq = sw.sw_seq
                    inner join tb_utm_info as utm
                    on utm.utm_seq = uci.utm_seq
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if orgseq != 0 :
            result = result + " and org.orgseq = '%s'" % orgseq
        result = result + " group by sw.reg_dttm, ha.ha_seq, ha.ha_gname, ha.is_primary, ha.is_primary, ha.ha_state, ha.ha_pri, uci.utm_seq, utm.utm_mgmt_ip, utm.utm_type, sw.sw_name, utm.utm_name, uci.status, uci.sw_seq, org.orgname ORDER BY sw.reg_dttm DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result

    def getUtmConns(self):
        result = """select utm.utm_seq as utm_seq, utm.utm_name as utm_name,
                    utm.utm_mgmt_ip as utm_mgmt_ip, utm.utm_type as utm_type
                    from tb_utm_conn_info as uci
                    inner join tb_utm_info as utm
                    on uci.utm_seq = utm.utm_seq
                    left join tb_ha_info as ha
                    on ha.utm_seq = uci.utm_seq
                    where ha.utm_seq is null
                    order by utm.utm_seq desc"""
        return result

    def setHaInsert(self, utmSeq, haSeq, haGName, isPrimary, haState):
        result = """insert into tb_ha_info(utm_seq, ha_seq, ha_gname, is_primary, ha_state, reg_dttm)
                    values('%s', '%s', '%s', '%s', '%s', CURRENT_TIMESTAMP)""" % (utmSeq, haSeq, haGName, isPrimary, haState)
        return result

    def getHaDetail(self, haSeq):
        result = """select ha.utm_seq, ha.ha_seq, ha.ha_gname, ha.is_primary, ha.ha_state, utm.utm_name
                    from tb_ha_info as ha inner join tb_utm_info as utm on ha.utm_seq = utm.utm_seq where ha.ha_seq = '%s'""" % haSeq
        return result

    def setHaModify(self, utmSeq, haGName, isPrimary, haState):
        result = """update tb_ha_info set ha_gname = '%s',  is_primary = '%s', ha_state = '%s'
                    where utm_seq = '%s'"""% (haGName, isPrimary, haState, utmSeq)
        return result

    def setHaDelete(self, utmSeq):
        result = "delete from tb_ha_info where utm_seq = '%s'" % (utmSeq)
        return result

    def setHaGroupDelete(self, haSeq):
        result = "delete from tb_ha_info where ha_seq = '%s'" % (haSeq)
        return result

    def getHaSeq(self):
        result = "select case when max(utm_seq) is null then 1 else (max(utm_seq) + 1) end from tb_ha_info"
        return result