#-*- coding: utf-8 -*-
'''
Created on 2015. 2. 25.

@author: SM
'''

class IpSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getIpRow(self, userid, custname, ip, orgseq, allocate_yn):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY ip) AS R
                    from tb_ip as ips join tb_org o on ips.orgseq = o.orgseq
                    left join tb_user as use
                    on use.userid = ips.userid
                    left join tb_customer as cust
                    on cust.customerseq = use.customerseq
                    inner join tb_user as auth
                    on ips.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        if not ip == "" :
            result = result + " and ips.ip = '%s'" % ip
        if not orgseq == 0 :
            result = result + " and o.orgseq = '%s'" % orgseq
        if allocate_yn == 'y' :
            result = result + " and ips.allocate_yn = 'Y'"
        if allocate_yn == 'n' :
            result = result + " and ips.allocate_yn = 'N'"
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getIp(self, userid, custname, ip, orgseq, start, allocate_yn):
        result = """select ROW_NUMBER() OVER(ORDER BY ips.ip DESC) as rownum, cust.customername as userid, ips.ip,
                    case when ips.allocate_yn = 'Y' then '사용'
                        when ips.allocate_yn = 'N' then '미사용' END as allocate,
                    to_char(ips.reg_dttm, 'YYYY-MM-DD HH:MM:SS')::text as regdttm, o.orgname, o.orgseq
                    from tb_ip as ips join tb_org o on ips.orgseq = o.orgseq
                    left join tb_user as use
                    on use.userid = ips.userid
                    left join tb_customer as cust
                    on cust.customerseq = use.customerseq
                    inner join tb_user as auth
                    on ips.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        if not ip == "" :
            result = result + " and ips.ip = '%s'" % ip
        if orgseq != 0 :
            result = result + " and o.orgseq = '%s'" % orgseq
        if allocate_yn == 'y' :
            result = result + " and ips.allocate_yn = 'Y'"
        if allocate_yn == 'n' :
            result = result + " and ips.allocate_yn = 'N'"
        result = result + " ORDER BY ips.reg_dttm DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result

    def getIpCheck(self, ip):
        result = "SELECT * FROM tb_ip WHERE ip = '%s'" % ip
        return result

    def setIpInsert(self, ip, orgseq):
        result = """insert into tb_ip(ip, allocate_yn, orgseq, reg_dttm)
                    values('%s', 'N', '%s', CURRENT_TIMESTAMP)""" % (ip, orgseq)
        return result

    def setIpModify(self, ip, userid, orgseq, allocate):
        result = """update tb_ip set userid = '%s',  orgseq = '%s', allocate_yn = '%s'
                    where ip = '%s'"""% (userid, orgseq, allocate, ip)
        return result

    def setUserDelete(self, ip):
        result = "delete from tb_ip where ip = '%s'" % ip
        return result