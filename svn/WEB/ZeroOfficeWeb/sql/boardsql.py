#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class BoardSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getOrgRow(self):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.orgname) AS R
                    FROM tb_org a join tb_addr b on a.orgseq = b.orgseq
                    WHERE 1=1 ORDER BY R DESC LIMIT 1 OFFSET 0;"""
        return result

    def getOrg(self,start):
        result = """select ROW_NUMBER() OVER(ORDER BY a.orgseq DESC) as rownum, a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a join tb_addr b on a.orgseq = b.orgseq where 1=1
                    ORDER BY a.orgseq DESC LIMIT 15 OFFSET \'%s\'""" % start
        return result

    def getOrgs(self, addr3):
        result = """select a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3 FROM tb_org a
                    join tb_addr b on a.orgseq = b.orgseq where 1=1 and b.addr3 like \'%s\'
                    ORDER BY a.orgseq DESC;""" % ("%"+addr3+"%")
        return result

    def getBoardRow(self, userid, title, content):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY boardseq) AS R
                    FROM tb_board
                    WHERE 1=1 AND del_dttm is null and typecode = 1 """
        if not userid == "" :
            result = result + " and userid like \'%s\'" % userid
        if not title == "" :
            result = result + " and title like \'%s\'" % title
        if not content == "" :
            result = result + " and content like \'%s\'" % content
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getBoard(self, userid, title, content, start):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY boardseq DESC) as rownum, boardseq, userid, title,
                    content, filepath, to_char(reg_dttm, 'YYYY-MM-DD HH24:MM:SS') AS regdttm
                    FROM tb_board WHERE del_dttm is null and typecode = 1"""
        if not userid == "" :
            result = result + " and userid like \'%s\'" % userid
        if not title == "" :
            result = result + " and title like \'%s\'" % title
        if not content == "" :
            result = result + " and content like \'%s\'" % content
        result = result + " ORDER BY boardseq DESC LIMIT 15 OFFSET \'%s\';" % start
        return result

    def setBoardInsert(self, title, content, userid, boardpass, filename, filepath):
        result = """insert into tb_board(typecode, title, content, userid, boardpass, filename, filepath, reg_dttm )
                    values(1, \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', CURRENT_TIMESTAMP)"""%(title, content, userid, boardpass, filename, filepath)
        return result

    def getBoardDetail(self, boardseq):
        result = """SELECT boardseq, userid, title, content, boardpass, filepath, filename
                    FROM tb_board
                    WHERE boardseq = \'%s\'""" % boardseq
        return result

    def setBoardUpdate(self, title, content, userid, boardpass, path, filename, boardseq):
        result = """UPDATE tb_board set title=\'%s\', content = \'%s\', userid = \'%s\', boardpass = \'%s\', filepath = \'%s\', filename = \'%s\'
                    WHERE boardseq = \'%s\'""" % (title, content, userid, boardpass, path, filename, boardseq)
        return result

    def setBoardDelete(self, boardseq):
        result = "UPDATE tb_board set del_dttm = CURRENT_TIMESTAMP WHERE boardseq = \'%s\'" % boardseq
        return result





    def getSelectOrgList(self, orgname):
        result = """select a.orgseq
                    FROM tb_org a where 1=1 and orgname = \'%s\'""" % orgname
        return result

    def getSelectAddrRow(self, addr1, addr2, addr3):
        result = """SELECT a.addrseq FROM (
                        SELECT ROW_NUMBER() OVER(ORDER BY addr1) AS r, addrseq
                        FROM tb_addr
                        where addr1 = \'%s\' and addr2 = \'%s\' and addr3 = \'%s\') a
                    WHERE r = 1""" % (addr1, addr2, addr3)
        return result

    def setAddrUpdate(self, orgseq, addrseq):
        result = """update tb_addr set orgseq = \'%s\' where addrseq = \'%s\'""" % (orgseq, addrseq)
        return result

    def setOrgUpdateAddr(self, orgseq, addrseq):
        result = """update tb_org set addrseq = \'%s\' where orgseq = \'%s\'""" % (addrseq, orgseq)
        return result

    def getOrgDetail(self, orgseq):
        result = """SELECT a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a join tb_addr b on a.orgseq = b.orgseq where 1=1
                    and a.orgseq = \'%s\'""" % orgseq
        return result

    def setOrgUpdate(self, orgseq, orgname):
        result = "update tb_org set orgname = \'%s\' where orgseq = \'%s\'" % (orgname, orgseq)
        return result

    def setOrgDelete(self, orgseq):
        result = "delete from tb_org where orgseq = \'%s\'" % orgseq
        return result

    def setAddrDelete(self, orgseq):
        result = "update tb_addr set orgseq = 0 where orgseq = \'%s\'" % orgseq
        return result
