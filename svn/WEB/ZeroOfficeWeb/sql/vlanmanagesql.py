#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class VlanSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getVlanRow(self, userid, custname, vlan, orgseq, allocate_yn):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY vlan) AS R
                    from tb_vlan v join tb_org o on v.orgseq = o.orgseq
                    left join tb_user as use
                    on use.userid = v.userid
                    left join tb_customer as cust
                    on cust.customerseq = use.customerseq
                    inner join tb_user as auth
                    on v.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        if not vlan == "" :
            result = result + " and v.vlan = '%s'" % vlan
        if not orgseq == 0 :
            result = result + " and o.orgseq = '%s'" % orgseq
        if allocate_yn == 'y' :
            result = result + " and v.allocate_yn = 'Y'"
        if allocate_yn == 'n' :
            result = result + " and v.allocate_yn = 'N'"
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getVlan(self, userid, custname, vlan, orgseq, start, allocate_yn):
        result = """select use.username, v.vlan, v.allocate_yn,
                    to_char(v.reg_dttm, 'YYYY-MM-DD HH:MM:SS')::text as regdttm, o.orgname, o.orgseq, v.type, use.userid
                    from tb_vlan v join tb_org o on v.orgseq = o.orgseq
                    left join tb_user as use
                    on use.userid = v.userid
                    inner join tb_user as auth
                    on v.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        if not vlan == "" :
            result = result + " and v.vlan = '%s'" % vlan
        if orgseq != 0 :
            result = result + " and o.orgseq = '%s'" % orgseq
        if allocate_yn == 'y' :
            result = result + " and v.allocate_yn = 'Y'"
        if allocate_yn == 'n' :
            result = result + " and v.allocate_yn = 'N'"
        result = result + " group by use.username, v.vlan, v.allocate_yn, v.reg_dttm, o.orgname, o.orgseq, v.type, use.userid ORDER BY v.reg_dttm DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self, userid):
        result = "select org.orgname, org.orgseq from tb_org as org inner join tb_user as u on org.orgseq = u.orgseq or u.userauth = 0 or u.userauth = 99 where pop_yn = 'Y' and userid = '%s' order by orgseq" % userid
        return result

    def getVlanCheck(self, vlan, orgseq):
        result = "SELECT * FROM tb_vlan WHERE vlan = '%s' and orgseq = %s" % (vlan, orgseq)
        return result

    def setVlanInsert(self, vlan, orgseq):
        result = """insert into tb_vlan(vlan, allocate_yn, orgseq, reg_dttm)
                    values('%s', 'N', '%s', CURRENT_TIMESTAMP)""" % (vlan, orgseq)
        return result

    def setVlanModify(self, vlan, userid, orgseq, allocate, types):
        result = """update tb_vlan set userid = '%s', allocate_yn = '%s', type = '%s'
                    where vlan = '%s' and orgseq = '%s'"""% (userid, allocate, types, vlan, orgseq)
        return result

    def setUserDelete(self, vlan, orgseq):
        result = "delete from tb_vlan where vlan = '%s' and orgseq = '%s'" % (vlan, orgseq)
        return result

    def getOrgUser(self, orgseq):
        result = "select userid, username from tb_user where orgseq = %s and userauth = 2" % orgseq
        return result