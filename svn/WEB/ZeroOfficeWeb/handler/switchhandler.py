#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.switchmanagesql import SwitchSql
from helper.psycopg_helper import PsycopgHelper

# 스위치 관리 페이지 처리 파일

# 스위치 관리 관련 URL
class SwitchURL(object):
    def url(self):
        url = [(r'/switch/switchManage', SwitchManageHandler),
               (r'/switch/switchList', SwitchListHandler),
               (r'/switch/insertSwitch', SwitchInsertHandler),
               (r'/switch/updateSwitch', SwitchModifyHandler),
               (r'/switch/deleteSwitch', SwitchDeleteHandler),
               (r'/switch/switchDetail', SwitchDetailHandler),
               (r'/switch/switchCheck', SwitchCheckHandler)
              ]
        return url

# 스위치 관리 페이지 진입 처리
class SwitchManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/switchList.html', username = username, userauth = userauth)

# 스위치 리스트 조회 처리
class SwitchListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        username = self.get_secure_cookie("user")
        orgseq = int(self.get_argument('orgseq'))
        try:
            switchsql = SwitchSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(switchsql.getSwitchRow(username, orgseq))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 스위치 리스트 조회
            switchs = self.execute(switchsql.getSwitch(username, orgseq, start))
            if not switchs['rows'] :
                switchs = json.dumps([])
            else :
                result = []

                for row in switchs['rows']:
                    row = dict(zip(switchs['columns'], row))
                    result.append(row)
                switchs = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(switchsql.getOrgs(username))
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "switchs" : switchs, "orgs" : orgs, "orgseq" : orgseq})

# 스위치 정보 등록 처리
class SwitchInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        switchName = self.get_argument('switchName')
        mgmtIp = self.get_argument('mgmtIp')
        orgseq = self.get_argument('orgSeq')
        sw_type = self.get_argument('switchType')
        sw_port = self.get_argument('port')
        sw_start = self.get_argument('start')
        sw_end = self.get_argument('end')

        switchsql = SwitchSql()
        data = 0

        try:
            self.execute_commit(switchsql.setSwitchInsert(switchName, orgseq, mgmtIp, sw_type, sw_port, sw_start, sw_end))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 스위치 상세정보 조회 처리
class SwitchDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        sw_seq = self.get_argument('sw_seq')
        try:
            switchsql = SwitchSql()
            result = []
            switchs = self.execute(switchsql.getSwitchDetail(sw_seq))
            for row in switchs['rows']:
                row = dict(zip(switchs['columns'], row))
                result.append(row)

            switchs = result
        except psycopg2.Error as e:
            print e
        self.write({"switchs" : switchs})

# 스위치 정보 수정 처리
class SwitchModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        sw_seq = self.get_argument('sw_seq')
        switchName = self.get_argument('switchName')
        mgmtIp = self.get_argument('mgmtIp')
        orgseq = self.get_argument('orgSeq')
        sw_type = self.get_argument('switchType')
        sw_port = self.get_argument('port')
        sw_start = self.get_argument('start')
        sw_end = self.get_argument('end')
        data = 0

        try:
            switchsql = SwitchSql()
            self.execute_commit(switchsql.setSwitchModify(sw_seq, switchName, orgseq, mgmtIp, sw_type, sw_port, sw_start, sw_end))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# 스위치 정보 삭제 처리
class SwitchDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        sw_seq = self.get_argument('sw_seq')
        try:
            switchsql = SwitchSql()
            self.execute_commit(switchsql.setSwitchDelete(sw_seq))
            self.execute_commit(switchsql.setUtmConnDelete(sw_seq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# mgmtIp 중복 체크 처리
class SwitchCheckHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        mgmtIp = self.get_argument('mgmtIp')
        result = "f"
        try:
            switchsql = SwitchSql()
            result = self.execute(switchsql.getSwitchCheck(mgmtIp))

            if result['rows'] == []:
                result = "t"
            else :
                result = "f"
        except psycopg2.Error as e:
            print e
        self.write({"result" : result})