#-*- coding: utf-8 -*-

'''
Created on 2015. 1. 5.

@author: 만구
'''

import psycopg2
import tornado.web
import json
from sql.orgmanagesql import OrgSql
from helper.psycopg_helper import PsycopgHelper

###############################################
# 사용 안함
###############################################

class OrgURL(object):
    def url(self):
        url = [(r'/org/orgManage', OrgManageHandler),
               (r'/org/orgList', OrgListHandler),
               (r'/org/orgSearch', OrgSerachHandler),
               (r'/org/addressSearch', AddressSearchHandler),
               (r'/org/insertOrg', OrgInsertHandler),
               (r'/org/updateAddr', AddrUpdateHandler),
               (r'/org/orgDetail', OrgDetailHandler),
               (r'/org/modifyOrg', OrgModifyHandler),
               (r'/org/deleteOrg', OrgDeleteHandler)
              ]
        return url

class OrgManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/orgList.html', username = username, userauth = userauth)

class OrgListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        try:
            orgsql = OrgSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            result = []
            rownum = self.execute(orgsql.getOrgRow())
            for row in rownum['rows']:
                row = dict(zip(rownum['columns'], row))
                result.append(row)

            rownum = result
            rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1
            if rownum % 15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            result = []
            orgs = self.execute(orgsql.getOrg(start))
            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "orgs" : orgs})

class OrgSerachHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        searchAddr1 = self.get_argument('searchAddr1')
        searchAddr2 = self.get_argument('searchAddr2')
        searchAddr3 = self.get_argument('searchAddr3')
        page = int(self.get_argument('page'))
        try:
            orgsql = OrgSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(orgsql.getOrgSearchRow(searchAddr1, searchAddr2, searchAddr3))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            orgs = self.execute(orgsql.getOrgSearch(searchAddr1, searchAddr2, searchAddr3, start))
            if not orgs['rows'] :
                orgs = json.dumps([])
            else :
                result = []

                for row in orgs['rows']:
                    row = dict(zip(orgs['columns'], row))
                    result.append(row)

                orgs = json.dumps(result)

        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "orgs" : orgs})

class AddressSearchHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        dongName = "%"+self.get_argument('dongName')+"%"
        try:
            orgsql = OrgSql()
            result = []
            address = self.execute(orgsql.getAddressSearch(dongName))
            for row in address['rows']:
                row = dict(zip(address['columns'], row))
                result.append(row)

            address = result
        except psycopg2.Error as e:
            print e

        self.write({"address" : address})

class OrgInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgname = self.get_argument('orgname')
        addr1 = self.get_argument('addr1')
        addr2 = self.get_argument('addr2')
        addr3 = self.get_argument('addr3')
        addrs = self.get_argument('addrs', '0')
        try:
            orgsql = OrgSql()
            self.execute_commit(orgsql.setOrgInsert(orgname, addrs))

            result = []
            orgseq = self.execute(orgsql.getSelectOrg(orgname))
            for row in orgseq['rows']:
                row = dict(zip(orgseq['columns'], row))
                result.append(row)

            orgseq = result

            result = []
            addrseq = self.execute(orgsql.getSelectAddrRow(addr1, addr2, addr3))
            for row in addrseq['rows']:
                row = dict(zip(addrseq['columns'], row))
                result.append(row)

            addrseq = result
        except psycopg2.Error as e:
            print e

        self.write({"orgseq" : orgseq, "addrseq" : addrseq})

class AddrUpdateHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        addrseq = self.get_argument('addrseq')
        try:
            orgsql = OrgSql()
            self.execute_commit(orgsql.setAddrUpdate(orgseq, addrseq))
            self.execute_commit(orgsql.setOrgUpdateAddr(orgseq, addrseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})
class OrgDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        try:
            orgsql = OrgSql()
            result = []
            orgs = self.execute(orgsql.getOrgDetail(orgseq))
            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = result
        except psycopg2.Error as e:
            print e
        self.write({"orgs" : orgs})

class OrgModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        orgname = self.get_argument('orgname')
        try:
            orgsql = OrgSql()
            self.execute_commit(orgsql.setOrgUpdate(orgseq, orgname))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class OrgDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        try:
            orgsql = OrgSql()
            self.execute_commit(orgsql.setOrgDelete(orgseq))
            self.execute_commit(orgsql.setAddrDelete(orgseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})
