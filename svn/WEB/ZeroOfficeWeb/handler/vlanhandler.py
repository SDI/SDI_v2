#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 5.

@author: 만구
'''
import psycopg2
import tornado.web
import json
from sql.vlanmanagesql import VlanSql
from helper.psycopg_helper import PsycopgHelper

# VALN관리 페이지 처리 파일

# VLAN관리 관련 URL
class VlanURL(object):
    def url(self):
        url = [(r'/vlan/vlanManage', VlanManageHandler),
               (r'/vlan/vlanList', VlanListHandler),
               (r'/vlan/vlanCheck', VlanCheckHandler),
               (r'/vlan/vlanInsert', VlanInsertHandler),
               (r'/vlan/vlanModify', VlanModifyHandler),
               (r'/vlan/vlanDelete', VlanDeleteHandler),
               (r'/vlan/getUser', getUserHandler)
              ]
        return url

# VLAN관리 페이지 진입 처리
class VlanManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        # 권한 확인
        if userauth != '0' and userauth != '1' and userauth != '99':
            self.redirect('/error')
        self.render('inventory/vlanList.html', username = username, userauth = userauth)

# VLAN리스트 조회 처리
class VlanListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        username = self.get_secure_cookie("user")
        custname = self.get_argument('userid')
        vlan = self.get_argument('vlan')
        orgseq = int(self.get_argument('orgseq'))
        allocate_yn = self.get_argument('allocateYn')
        try:
            vlansql = VlanSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(vlansql.getVlanRow(username, custname, vlan, orgseq, allocate_yn))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # VLAN 리스트 조회
            vlans = self.execute(vlansql.getVlan(username, custname, vlan, orgseq, start, allocate_yn))
            if not vlans['rows'] :
                vlans = json.dumps([])
            else :
                result = []

                for row in vlans['rows']:
                    row = dict(zip(vlans['columns'], row))
                    result.append(row)
                vlans = json.dumps(result)

            # 국사 리스트 조회(운영자 권한은 해당 국사만 나옴)
            orgs = self.execute(vlansql.getOrgs(username))
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "vlan" : vlans, "orgs" : orgs, "orgseq" : orgseq})

# VLAN 중복 체크 처리
class VlanCheckHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('vlan')
        orgseq = self.get_argument('orgseq')
        response = "succ"
        try:
            vlansql = VlanSql()
            for val in json.loads(data) :
                if response == "succ" :
                    result = self.execute(vlansql.getVlanCheck(val['vlan'], orgseq))
                    if result['rows'] == []:
                        response = "succ"
                    else :
                        response = "fail"
        except psycopg2.Error as e:
            print e
        self.write({"response" : response})

# VLAN 등록 처리
class VlanInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('vlan')
        orgseq = self.get_argument('orgseq')
        try:
            vlansql = VlanSql()
            for val in json.loads(data) :
                self.execute_commit(vlansql.setVlanInsert(val['vlan'], orgseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# VLAN 정보 수정 처리
class VlanModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        vlan = self.get_argument('vlan')
        userid = self.get_argument('userid')
        orgseq = self.get_argument('orgseq')
        allocate = self.get_argument('allocate')
        types = self.get_argument('type')
        try:
            vlansql = VlanSql()
            self.execute_commit(vlansql.setVlanModify(vlan, userid, orgseq, allocate, types))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# VLAN 삭제 처리
class VlanDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        vlan = self.get_argument('vlan')
        try:
            vlansql = VlanSql()
            for val in json.loads(vlan) :
                self.execute_commit(vlansql.setUserDelete(val['vlan'], val['orgseq']))
                data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 국사별 고객 리스트 조회 처리(VLAN을 사용중으로 처리할 때 사용하는 고객사를 표시하기 위함)
class getUserHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        try:
            vlansql = VlanSql()

            data = self.execute(vlansql.getOrgUser(orgseq))
            result = []

            for row in data['rows']:
                row = dict(zip(data['columns'], row))
                result.append(row)

            data = json.dumps(result)

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})