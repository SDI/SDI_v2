#-*- coding: utf-8 -*-
'''
Created on 2015. 5. 14.

@author: 세민

'''


import tornado.web
import psycopg2
import json
from sql.actionsql import ActionSql
from helper.psycopg_helper import PsycopgHelper
from helper.logHelper import myLogger

log = myLogger(tag='action', logdir='./log', loglevel='debug', logConsole=True).get_instance()

# 작업이력 관리 페이지 처리 파일

# 작업이력 관리 관련 URL
class ActionManageURL(object):
    def url(self):
        url = [(r'/action/actionManage', ActionManagementHandler),
               (r'/action/actionList', ActionListHandler),
               (r'/action/actionSearch', ActionSearchHandler),
               (r'/action/actionDetail', ActionDetailhandler)
              ]
        return url

# 작업이력 페이지 진입 처리 클래스
class ActionManagementHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '99' :
            self.redirect('/error')
        self.render('monitoring/actionLog.html',username=username, userauth=userauth)

# 작업이력 조회 처리 클래스
class ActionListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        # 현재 조회할 페이지 번호
        page = int(self.get_argument('page'))
        # 조회국사seq
        orgseq = int(self.get_argument('orgseq', '0'))
        try:
            actionsql = ActionSql()
            # 페이징 처리 계산
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            # 조회되는 데이터 수 구하는 부분
            result = []
            rownum = self.execute(actionsql.getActionRow(orgseq))
            columns = rownum['columns']
            for row in rownum['rows']:
                row = dict(zip(columns, row))
                result.append(row)

            rownum = result
            rownum = rownum[0]['count']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            actionsql = ActionSql()

            # 조회되는 데이터 저장하는 부분
            result = []
            action = self.execute(actionsql.getAction(orgseq, start))
            columns = action['columns']
            for row in action['rows']:
                row = dict(zip(columns, row))
                result.append(row)

            action = json.dumps(result)

            # 국사 리스트 저장하는 부분
            orgs = self.execute(actionsql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e

        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "actions" : action, "orgs" : orgs, "orgseq" : orgseq})

# 조회 조건을 입력하고 검색을 클릭했을 때 처리하는 클래스
class ActionSearchHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        # 조회 할 페이지 값
        page = int(self.get_argument('page'))
        # 검색조건(사용자)
        userNm = self.get_argument('userNm')
        # 검색조건(조회일자[시작일])
        startdt = self.get_argument('startdt')
        # 검색조건(조회일자[종료일])
        enddt = self.get_argument('enddt')
        # 검색조건(국사)
        orgseq = int(self.get_argument('orgseq', '0'))
        try:
            actionsql = ActionSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(actionsql.getActionSearchRow(userNm, startdt, enddt, orgseq))
            if not rownum['rows']:
                rownum = 0
            else :
                columns = rownum['columns']
                result = []

                for row in rownum['rows']:
                    row = dict(zip(columns, row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['count']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            actionsql = ActionSql()
            action = self.execute(actionsql.getActionSearch(userNm, startdt, enddt, start, orgseq))
            if not action['rows'] :
                action = json.dumps([])
            else :
                columns = action['columns']
                result = []

                for row in action['rows']:
                    row = dict(zip(columns, row))
                    result.append(row)

                action = json.dumps(result)

        except psycopg2.Error as e:
            print e

        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "actions" : action})

# 작업이력 목록 테이블에서 특정 row를 클릭했을 때 상세 정보 팝업을 제공하는 클래스
class ActionDetailhandler(tornado.web.RequestHandler, PsycopgHelper):
    # 작업이력 상세정보 페이지 진입 처리 함수
    def get(self):
        # 쿠키값에서 권한 값 가져오기
        userauth = self.get_secure_cookie("userauth")
        # 쿠키값에서 권한 확인 0: 관리자, 99: 슈퍼유저
        if userauth != '0' and userauth != '99':
            self.redirect('/error')
        seq = self.get_argument("seq")

        self.render('monitoring/actionDetail.html', seq = seq)

    # 작업이력 상세정보 데이터 처리 함수
    def post(self):
        seq = self.get_argument("seq")

        actionsql = ActionSql()

        # DB에서 작업이력 상세정보 조회
        result = self.execute(actionsql.getActionDetail(seq))['rows']

        result = {'result' : result}

        self.write(result)