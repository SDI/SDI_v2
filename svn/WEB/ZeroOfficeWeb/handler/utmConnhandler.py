#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.utmConnmanagesql import UtmConnSql
from helper.psycopg_helper import PsycopgHelper

# UTM연결 관리 페이지 처리 파일

# UTM연결 관리 관련 URL
class UtmConnURL(object):
    def url(self):
        url = [(r'/utmConn/utmConnManage', UtmConnManageHandler),
               (r'/utmConn/utmConnList', UtmConnListHandler),
               (r'/utmConn/insertUtmConn', UtmConnInsertHandler),
               (r'/utmConn/updateUtmConn', UtmConnModifyHandler),
               (r'/utmConn/deleteUtmConn', UtmConnDeleteHandler),
               (r'/utmConn/getPort', getPortHandler)
              ]
        return url

# UTM연결 관리 페이지 진입 처리
class UtmConnManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        # 권한 확인
        if userauth != '0' and userauth != '1' and userauth != '99' :
                self.redirect('/error')
        self.render('inventory/utmConnList.html', username = username, userauth = userauth)

# UTM연결 리스트 조회 처리
class UtmConnListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        username = self.get_secure_cookie("user")
        orgseq = int(self.get_argument('orgseq'))
        try:
            utmConnsql = UtmConnSql()
            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(utmConnsql.getUtmConnRow(username, orgseq))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # UTM연결 리스트 조회
            utmConns = self.execute(utmConnsql.getUtmConn(username, orgseq, start))
            if not utmConns['rows'] :
                utmConns = json.dumps([])
            else :
                result = []

                for row in utmConns['rows']:
                    row = dict(zip(utmConns['columns'], row))
                    result.append(row)
                utmConns = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(utmConnsql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)

            # UTM 리스트 조회
            utms = self.execute(utmConnsql.getUtms())
            result = []

            for row in utms['rows']:
                row = dict(zip(utms['columns'], row))
                result.append(row)

            utms = json.dumps(result)

            # 스위치 리스트 조회
            switchs = self.execute(utmConnsql.getSwitchs())
            result = []

            for row in switchs['rows']:
                row = dict(zip(switchs['columns'], row))
                result.append(row)

            switchs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "utmConns" : utmConns, "orgs" : orgs, "orgseq" : orgseq, "utms" : utms, "switchs" : switchs})

# UTM연결 정보 등록 처리
class UtmConnInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmSeq = self.get_argument('utmSeq')
        utmLinkType = self.get_argument('utmLinkType')
        swSeq = self.get_argument('swSeq')
        sw_port = self.get_argument('sw_port')

        utmConnsql = UtmConnSql()

        try:
            self.execute_commit(utmConnsql.setUtmConnInsert(utmSeq, utmLinkType, swSeq, sw_port))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})


# UTM연결 정보 수정 처리
class UtmConnModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        swSeq = self.get_argument('swSeq')
        sw_port = self.get_argument('sw_port')
        utmSeq = self.get_argument('utmSeq')
        utmLinkType = self.get_argument('utmLinkType')
        data = 0

        try:
            utmConnsql = UtmConnSql()
            self.execute_commit(utmConnsql.setUtmConnModify(swSeq, sw_port, utmSeq, utmLinkType))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# UTM연결 정보 삭제 처리
class UtmConnDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmSeq = self.get_argument('utmSeq')
        utmLinkType = self.get_argument('utmLinkType')
        swSeq = self.get_argument('swSeq')
        try:
            utmConnsql = UtmConnSql()
            self.execute_commit(utmConnsql.setUtmConnDelete(utmSeq, utmLinkType, swSeq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 스위치 포트수 조회 처리(UTM과 스위치의 연결관계에서 스위치의 포트를 벗어나는 값 방지 처리)
class getPortHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        swSeq = self.get_argument('swSeq')
        try:
            utmConnsql = UtmConnSql()
            port = self.execute(utmConnsql.getPort(swSeq))['rows'][0][0]

        except psycopg2.Error as e:
            print e

        self.write({"port" : port})