#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 23.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.utmmanagesql import UtmSql
from helper.psycopg_helper import PsycopgHelper

# UTM관리 페이지 처리 파일

# UTM관리 관련 URL
class UtmURL(object):
    def url(self):
        url = [(r'/utm/utmManage', UtmManageHandler),
               (r'/utm/utmList', UtmListHandler),
               (r'/utm/insertUtm', UtmInsertHandler),
               (r'/utm/updateUtm', UtmModifyHandler),
               (r'/utm/deleteUtm', UtmDeleteHandler),
               (r'/utm/utmDetail', UtmDetailHandler)
              ]
        return url

# UTM관리 페이지 진입 처리
class UtmManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/utmList.html', username = username, userauth = userauth)

# UTM 리스트 조회 처리
class UtmListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        try:
            utmsql = UtmSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(utmsql.getUtmRow())
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # UTM리스트 조회
            utms = self.execute(utmsql.getUtm(start))
            if not utms['rows'] :
                utms = json.dumps([])
            else :
                result = []

                for row in utms['rows']:
                    row = dict(zip(utms['columns'], row))
                    result.append(row)
                utms = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "utms" : utms})

# UTM 등록 처리
class UtmInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmName = self.get_argument('utmName')
        mgmtIp = self.get_argument('mgmtIp')
        utmType = self.get_argument('utmType')

        utmsql = UtmSql()

        try:
            self.execute_commit(utmsql.setUtmInsert(utmName, mgmtIp, utmType))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# UTM 상세정보 조회 처리
class UtmDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utm_seq = self.get_argument('utm_seq')
        try:
            utmsql = UtmSql()
            result = []
            utms = self.execute(utmsql.getUtmDetail(utm_seq))
            for row in utms['rows']:
                row = dict(zip(utms['columns'], row))
                result.append(row)

            utms = result
        except psycopg2.Error as e:
            print e
        self.write({"utms" : utms})

# UTM 정보 수정 처리
class UtmModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utm_seq = self.get_argument('utm_seq')
        utmName = self.get_argument('utmName')
        mgmtIp = self.get_argument('mgmtIp')
        utmType = self.get_argument('utmType')
        data = 0

        try:
            utmsql = UtmSql()
            self.execute_commit(utmsql.setUtmModify(utm_seq, utmName, mgmtIp, utmType))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# UTM 삭제 처리
class UtmDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utm_seq = self.get_argument('utm_seq')
        try:
            utmsql = UtmSql()
            self.execute_commit(utmsql.setUtmDelete(utm_seq))
            self.execute_commit(utmsql.setUtmConnDelete(utm_seq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})