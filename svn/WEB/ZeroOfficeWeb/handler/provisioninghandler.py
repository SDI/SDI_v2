#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 7.

@author: SM
'''


import json

import psycopg2
import tornado.web

from helper.psycopg_helper import PsycopgHelper
from sql.provisioningsql import ProvisionSql
from util.restclient import SoapClient

from helper.logHelper import myLogger

log = myLogger(tag='provisioning', logdir='./log', loglevel='debug', logConsole=True).get_instance()

# UTM 개통 관리 페이지 처리 파일

# UTM 개통 관리 관련 URL
class ProvisionURL(object):
    def url(self):
        url = [
               (r'/provisioning/templateData', TemplateDataHandler),
               (r'/provisioning/pool', PoolHandler),
               (r'/provisioning/appSlide', AppSlideHandler),
               (r'/provisioning/tbNecat', TbNecatHandler),
               (r'/provisioning/provisions/checkuser', ProvisionCheckUserHandler),
               (r'/provisioning/provisions/readyApp', ProvisionReadyAppHandler),
               (r'/provisioning/provisions/createApp', ProvisionCreateAppHandler),
               (r'/provisioning/provisions/deleteApp', ProvisionDeleteAppHandler),
               (r'/provisioning/provisions/step', ProvisionStepHandler),
               (r'/provisioning/userList', ProvisionUserListHandler),
               (r'/provisioning/nfvProvisioning', ProvisioningHandler2),
               (r'/provisioning/getSwitch', getSwitchHandler),
               (r'/provisioning/getport', getPortHandler),
              ]
        return url

# UTM 개통 관리 페이지 진입 처리
class ProvisioningHandler2(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        # 권한 확인
        if userauth != '0' and userauth != '1' and userauth != '2' and userauth != '3' and userauth != '99' :
            self.redirect('/error')
        '''pseq = 0
        templateseq = 1
        orgseq = 4'''
        pseq = self.get_argument('prov', '0')
        templateseq = self.get_argument('template', '1')
        orgSeq = self.get_argument('org', '0')
        try:
            provisionsql = ProvisionSql()

            # 의미 불명 조회들(정확히 처음 만든 의도를 모름[확인 중])
            result = []
            templateList = self.execute(provisionsql.getTemplateList())
            for row in templateList['rows']:
                row = dict(zip(templateList['columns'], row))
                result.append(row)

            templateList = result

            result = []
            fixListName = self.execute(provisionsql.getFixListName())
            for row in fixListName['rows']:
                row = dict(zip(fixListName['columns'], row))
                result.append(row)

            fixListName = result
            if pseq != '0' :
                result = []
                provisionUserid = self.execute(provisionsql.getProvisionUserid(pseq))
                provisionUserid = provisionUserid['rows'][0][0]
            else :
                provisionUserid = 0
            result = []
            orgName = self.execute(provisionsql.getOrgName(username))
            for row in orgName['rows']:
                row = dict(zip(orgName['columns'], row))
                result.append(row)

            orgName = result[0]['orgname']

            if orgSeq == '0':
                orgSeq = result[0]['orgseq']
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)
        self.render('service/nfvprovision2.html',templateList = templateList, fixListName = fixListName, orgName = orgName, username = username,
                    prov = pseq, org = orgSeq, template = templateseq, userauth = userauth, provisionUserid = provisionUserid)

# provisioning을 위한 정보들 조회 처리
class TemplateDataHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_secure_cookie("user")
        orgseq = self.get_argument('org')
        try:
            provisionsql = ProvisionSql()

            # 템플릿 리스트 조회
            template = self.execute(provisionsql.getTemplate())
            if template :
                result = []
                for row in template['rows']:
                    row = dict(zip(template['columns'], row))
                    result.append(row)

                template = json.dumps(result)
            else :
                template = [{"nstemplateseq":"0"}]

            # poolList조회
            result = []
            poolList = self.execute(provisionsql.getPoolList())
            for row in poolList['rows']:
                row = dict(zip(poolList['columns'], row))
                result.append(row)

            poolList = json.dumps(result)

            # 해당 국사의 사용 가능한 VLAN 리스트 조회
            result = []
            vlan = ''
            vlan = self.execute(provisionsql.getVlanList(orgseq))
            for row in vlan['rows']:
                row = dict(zip(vlan['columns'], row))
                result.append(row)
            vlan = result

            # 해당 국사의 고객 조회
            result = []
            users = self.execute(provisionsql.getUserList(orgseq))
            for row in users['rows']:
                row = dict(zip(users['columns'], row))
                result.append(row)
            users = result

            # 국사 리스트 조회(권한과 소속에 따라 다름)
            result = []
            orgs = self.execute(provisionsql.getOrgs(userid))
            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)
            orgs = json.dumps(result)

            # 해당 국사의 UTM 조회
            result = []
            utms = self.execute(provisionsql.getUtms(orgseq))
            for row in utms['rows']:
                row = dict(zip(utms['columns'], row))
                result.append(row)
            utms = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

        self.write({"template":template, "poolList":poolList, "vlan" : vlan, "users" : users, "orgs" : orgs, "utms" : utms})

# 현재 사용하지 않는 기능(용도를 파악해야 됩니다.)
class PoolHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        templateseq = self.get_argument('templateseq')
        provisionseq = self.get_argument('provisionseq')
        try:
            provisionsql = ProvisionSql()
            pool = self.execute(provisionsql.getPool(provisionseq, templateseq))
            if pool :
                result = []
                for row in pool['rows']:
                    row = dict(zip(pool['columns'], row))
                    result.append(row)

                pool = json.dumps(result)
            else :
                pool = json.dumps([{"neclsseq":"0"}])

            result = []
            fixField = self.execute(provisionsql.getFixListName())
            for row in fixField['rows']:
                row = dict(zip(fixField['columns'], row))
                result.append(row)

            fixField = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)
        self.write({"pool":pool, "fixField":fixField})


# app 리스트 조회(현재 Olleh-vUTM고정으로 사용 안함)
class AppSlideHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        neclsseq = self.get_argument('neclsseq')
        try:
            provisionsql = ProvisionSql()
            result = []
            appSlide = self.execute(provisionsql.getAppSlide(neclsseq))
            for row in appSlide['rows']:
                row = dict(zip(appSlide['columns'], row))
                result.append(row)

            appSlide = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

        self.write({"appSlide":appSlide})

# 선택한 상품의 설정 값들 조회(json 형태)
class TbNecatHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        necatseq = self.get_argument('necatseq')
        try:
            provisionsql = ProvisionSql()
            result = []
            necat = self.execute(provisionsql.getNecat(necatseq))
            for row in necat['rows']:
                row = dict(zip(necat['columns'], row))
                result.append(row)

            necat = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

        self.write({"necat":necat})

# AP에 고객 정보 전송
class ProvisionCheckUserHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        userid = self.get_argument('userid')
#         templateseq = self.get_argument('templateseq')
        is_backup = self.get_argument('is_backup')

        provisionsql = ProvisionSql()

        data = self.execute(provisionsql.getProvUser(userid, orgseq))['rows'][0]

        # 이중화 사용
        if is_backup == 't':
            body = {"templateseq": 36, "orgseq": orgseq, "userid": data[0], "username":data[1], "password":data[2], "backuputmseq":1, "standbyyn":"Y"}
        else:
            body = {"templateseq": 36, "orgseq": orgseq, "userid": data[0], "username":data[1], "password":data[2], "backuputmseq":1}

        log.info("createProvision userid : %s, userauth : %s, orgname : %s, body : [%s]", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), body)

        body = json.dumps(body)

        soap = SoapClient()
        data = soap.sendData("/service/create/checkuser", "post", body)
        self.write(data)

# AP에 사용할 seq값 요청
class ProvisionReadyAppHandler(tornado.web.RequestHandler):
    def post(self):
        data = self.get_argument('data')
        soap = SoapClient()
        data = soap.sendData("/service/provisions/ready", "post", data)
        self.write(data)

# AP에 UTM 생성 요청
class ProvisionCreateAppHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('appProvisions')
        user = self.get_argument('user')
        vlans = self.get_argument('vlans')
        username = self.get_secure_cookie("user")
        provisionsql = ProvisionSql()
        soap = SoapClient()
        data = soap.sendData("/service/provisions", "post", data)

        param = "user : %s, vlan : %s" % (user, vlans)
        self.execute_commit(provisionsql.setCommandLog(username, '/provisioning/provisions/createApp', 'createProvisioning', param))

        log.info("createProvision userid : %s, userauth : %s, orgname : %s, content : [%s]", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), param)
        self.write(data)

# AP에 UTM 삭제 요청
class ProvisionDeleteAppHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('provisionseq')
        username = self.get_secure_cookie("user")
        user = self.get_argument('user')
        provisionsql = ProvisionSql()
        soap = SoapClient()
        data = soap.sendData("/service/provisions/", "delete", data)

        param = "user : %s, seq : %s" % (user, data)
        self.execute_commit(provisionsql.setCommandLog(username, '/provisioning/provisions/deleteApp', 'deleteProvisioning', param))

        log.info("deleteProvision userid : %s, userauth : %s, orgname : %s, content : [%s]", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), param)
        self.write(data)

# UTM의 생성/삭제 진행 상태 조회
class ProvisionStepHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        pseq = self.get_argument('provisionseq')
        try:
            provisionsql = ProvisionSql()
            result = []
            step = self.execute(provisionsql.getProvisionStep(pseq))
            for row in step['rows']:
                row = dict(zip(step['columns'], row))
                result.append(row)

            step = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)
        self.write({"step":step})

# 해당 국사의 고객 조회
class ProvisionUserListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        try:
            provisionsql = ProvisionSql()
            result = []
            users = self.execute(provisionsql.getUserList(orgseq))
            for row in users['rows']:
                row = dict(zip(users['columns'], row))
                result.append(row)

            users = json.dumps(result)
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

        self.write({"users":users})

# 해당 국사의 스위치 조회
class getSwitchHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmseq = self.get_argument('utmseq')
        try:
            provisionsql = ProvisionSql()

            switchs = self.execute(provisionsql.getSwitch(utmseq))

            result = []
            for row in switchs['rows']:
                row = dict(zip(switchs['columns'], row))
                result.append(row)

            switchs = json.dumps(result)

            switchsList = self.execute(provisionsql.getSwitchPortList(utmseq))['rows']
        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)
        self.write({"switchs" : switchs, "switchsList" : switchsList})

# 해당 스위치의 포트수 조회
class getPortHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        swseq = self.get_argument('swseq')
        try:
            provisionsql = ProvisionSql()
            port = self.execute(provisionsql.getSwitchPort(swseq))['rows'][0][0]
            start = self.execute(provisionsql.getSwitchPort(swseq))['rows'][0][1]
            end = self.execute(provisionsql.getSwitchPort(swseq))['rows'][0][2]

        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)
        self.write({"port" : port, "start" : start, "end" : end})