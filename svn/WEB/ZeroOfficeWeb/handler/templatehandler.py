#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 7.

@author: 만구
'''
import json
import tornado.web
import psycopg2
from helper.psycopg_helper import PsycopgHelper

from sql.templatesql import TemplateSql

###############################################
# 사용 안함
###############################################

class TemplateURL(object):
    def url(self):
        url = [(r'/template/templatePage', InventoryTemplatesHandler),
               (r'/template/getTemplate', GetTemplatesHandler),
               (r'/template/templateApp', AppTemplatesHandler),
               (r'/template/addNewTemplate', AddTemplatesHandler),
               (r'/template/setTemplate', InsertTemplatesHandler),
               (r'/template/updateTemplate', UpdateTemplatesHandler),
               (r'/template/deleteNsTemplate', DelTemplatesHandler)
              ]
        return url

class InventoryTemplatesHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/templates.html', username = username, userauth=userauth)

class GetTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        try:
            templateSql = TemplateSql()
            templateList = self.execute(templateSql.selectTemplate())

            result = []

            for row in templateList['rows']:
                row = dict(zip(templateList['columns'], row))
                result.append(row)

            templateList = json.dumps(result)

            templateName = self.execute(templateSql.getTemplateName())

            result = []

            for row in templateName['rows']:
                row = dict(zip(templateName['columns'], row))
                result.append(row)
            templateName = json.dumps(result)

        except psycopg2.Error as e:
            print e
        self.write({'templateList' : templateList, 'templateName' : templateName})

class AppTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        templateSeq = self.get_argument("templateSeq")

        try:
            templateSql = TemplateSql()
            templatesApp = self.execute(templateSql.selectNeTableTemplate(templateSeq))

            result = []

            for row in templatesApp['rows']:
                row = dict(zip(templatesApp['columns'], row))
                result.append(row)

            templatesApp = json.dumps(result)

        except psycopg2.Error as e:
            print e

        self.write({'templatesApp' : templatesApp})

class AddTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        templateName = self.get_argument("templateName")
        username = self.get_argument("username")

        try:
            templateSql = TemplateSql()
            self.execute_commit(templateSql.addNewTemplate(templateName, username))

            templateName = self.execute(templateSql.getTemplateName())

            result = []

            for row in templateName['rows']:
                row = dict(zip(templateName['columns'], row))
                result.append(row)

            templateName = json.dumps(result)
        except psycopg2.Error as e:
            print e

        self.write({'templateName' : templateName})

class InsertTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        template = json.loads(self.get_argument("template"))
        templateSeq = self.get_argument("templateSeq")

        try:
            templateSql = TemplateSql()
            for index, var in enumerate(template) :
                self.execute_commit(templateSql.insertTemplate(var['neclsseq'], index, templateSeq))
                data = 1

        except psycopg2.Error as e:
            print e
        self.write({'data' : data})
class UpdateTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        template = json.loads(self.get_argument("template"))
        templateSeq = self.get_argument("templateSeq")

        try:
            templateSql = TemplateSql()
            neclsseq = self.execute(templateSql.getNeclsSeq(templateSeq))
            result = []
            for row in neclsseq['rows']:
                row = dict(zip(neclsseq['columns'], row))
                result.append(row)
            neclsseq = result

            for seq in neclsseq :
                self.execute_commit(templateSql.deleteTemplate(templateSeq, seq['neclsseq']))

            for index, tem in enumerate(template) :
                self.execute_commit(templateSql.insertTemplate(tem['neclsseq'], index, templateSeq))
            data = 0
        except psycopg2.Error as e:
            print e
        self.write({"data" :data})

class DelTemplatesHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        templateSeq = self.get_argument("templateSeq")
        try:
            templateSql = TemplateSql()
            self.execute_commit(templateSql.deleteNsTemplate(templateSeq))
            data = 0

        except psycopg2.Error as e:
            print e

        self.write({"data" :data})