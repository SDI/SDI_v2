#-*- coding: utf-8 -*-

'''
Created on 2015. 1. 5.

@author: 만구
'''

import psycopg2
import tornado.web
import json
from sql.appliancesql import ApplianceSql
from helper.psycopg_helper import PsycopgHelper

###############################################
# 사용 안함
###############################################

class ApplianceURL(object):
    def url(self):
        url = [(r'/appliance/appliancePage', ApplianceHandler),
               (r'/appliance/appList', AppListHandler),
               (r'/appliance/appSubmit', AppSubmitHandler),
               (r'/appliance/appDelete', AppDeleteHandler),
               (r'/appliance/getNecat', getNecatHandler)
              ]
        return url

class ApplianceHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/appliance.html', username = username, userauth = userauth)
class AppListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        dtlcode = self.get_argument('dtlcode')
        necatNm = self.get_argument('necatNm')
        appNm = self.get_argument('appNm')
        serviceNm = self.get_argument('serviceNm')
        serverSpec = self.get_argument('serverSpec')
        enddt = self.get_argument('enddt')
        startdt = self.get_argument('startdt')
        try:
            appliancesql = ApplianceSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(appliancesql.getAppRow(dtlcode, necatNm, appNm, serviceNm, serverSpec, startdt, enddt))
            if not rownum['rows']:
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1
            if rownum % 15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            apps = self.execute(appliancesql.getApp(dtlcode, necatNm, appNm, serviceNm, serverSpec, startdt, enddt, start))
            if not apps['rows']:
                apps = json.dumps([])
            else :
                result = []

                for row in apps['rows']:
                    row = dict(zip(apps['columns'], row))
                    result.append(row)

                apps = json.dumps(result)

            dtlCode = self.execute(appliancesql.getDtlcode())
            result = []

            for row in dtlCode['rows']:
                row = dict(zip(dtlCode['columns'], row))
                result.append(row)

            dtlCode = json.dumps(result)

            dtl = self.execute(appliancesql.getDtl())
            result = []

            for row in dtl['rows']:
                row = dict(zip(dtl['columns'], row))
                result.append(row)

            dtl = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "apps" : apps, "dtlCode" : dtlCode, "dtl" : dtl})

class AppSubmitHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        name = str(self.get_argument('appNm'))
        servicename = str(self.get_argument('serviceNm'))
        neclsseq = int(self.get_argument('neclsseq'))
        icon_small = str(self.get_argument('icon_small'))
        icon_large = str(self.get_argument('icon_large'))
        imagename = str(self.get_argument('imagename'))
        service = str(self.get_argument('service'))
        print service
        try:
            appliancesql = ApplianceSql()
            self.execute_commit(appliancesql.setServiceInsert(name, neclsseq, icon_small, icon_large, servicename, imagename, service))
            result = "success"
        except psycopg2.Error as e:
            print e
            result = "fail"
        self.write({"result" : result})

class AppDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = json.loads(self.get_argument('data'))
        try:
            appliancesql = ApplianceSql()
            for row in data :
                self.execute_commit(appliancesql.setTemplateUpdate(row['seq']))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class getNecatHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        necatSeq = self.get_argument('necatSeq')
        try:
            appliancesql = ApplianceSql()
            necat = self.execute(appliancesql.getNecat(necatSeq))
            result = []

            for row in necat['rows']:
                row = dict(zip(necat['columns'], row))
                result.append(row)

            necat = result
        except psycopg2.Error as e:
            print e
        self.write({"data" : necat})