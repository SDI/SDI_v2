#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 5.
@summary:
@author: 진수
'''
import json

from helper.psycopg_helper import PsycopgHelper

class ProvisionService(PsycopgHelper):
    def __init__(self):
        pass

    def createProvision(self, reqbody):
        """
        Created on 2015. 2. 5.
        @author: 진수
        @summary: 프로비저닝을 생성한다.
        @param reqbody: 사용자요청 json format string
        @rtype: string: json format
        """
        reqbodydic = json.loads(reqbody)
        print reqbodydic
        print type(reqbodydic)

        provisionseq = reqbodydic['provision']['provisionseq']
        templateseq = reqbodydic['provision']['templateseq']
        if provisionseq == 0:
            pass
        else:
            applications = reqbodydic['provision']['applications']
            for application in applications:
                if application['neclsseq'] == 32:
                    pass
                else:
                    pass





if __name__ == '__main__':
    psvc = ProvisionService()
    reqbody = {'name':'jini', 'age':45}
    print reqbody
    print json.dumps(reqbody)
    psvc.createProvision(json.dumps(reqbody))
