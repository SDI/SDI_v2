#!/usr/bin/python -B
# -*- coding: utf-8 -*-

import paramiko

class myParamiko():
    """
        paramiko를 이용한 interactive 원격쉘 실행 프로그램
    """

    def __init__(self):
        self.debug      = False # remote ssh stdin/out display to console
        self.fdebug     = True  # remote ssh stdin/out display to file
        self.timeout    = 5     # remote ssh command timeout

        # ssh를 사용할 채널에 관한 meta 정보
        self.servinfo   = {}
        # ssh 로 연결된 채널 인스턴스 정보
        self.servinst   = {}

    def register(self, name, ip, port, id, pw):
        self.servinfo[name] = (ip, port, id, pw)

    def serv_keys(self):
        return self.servinfo.keys()

    def getservinfo(self, name):
        if self.servinfo.has_key(name):
            return self.servinfo[name]
        else:
            errmsg = "name[%s] does not exist!! register first!!" % (name)
            print errmsg
            raise RuntimeError(errmsg)

    def getserv_ip(self, name):
        (ip, port, id, pw) = self.getservinfo(name)
        return ip

    def getserv_pw(self, name):
        (ip, port, id, pw) = self.getservinfo(name)
        return pw

    def getserv_instance(self, name):
        if self.servinst.has_key(name):
            return self.servinst[name]
        else:
            errmsg = "name[%s] does not connect!! connect first!!" % (name)
            print errmsg
            raise RuntimeError(errmsg)
    # ------------------------------------------------------------------------------

    # --------------------------------------------------------------------------
    def connect(self, name):
        """name tag에 해당하는 ssh 서버에 접속"""

        if self.servinst.has_key(name):
            errmsg = "name[%s] is already connected!!" % (name)
            print errmsg
            raise RuntimeError(errmsg)

        (ip, port, id, pw) = self.getservinfo(name)

        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(ip,username=id, password=pw)

        self.servinst[name] = client

    # --------------------------------------------------------------------------
    def run(self, name, command):
        """
            명령을 실행하고 결과를 포함하는 stdout 채널을 리턴한다.
            기본적으로는 명령의 결과를 리턴하나,
            다양하게 결과를 분석해서 처리해야 하는 경우가 많으므로
            결과가 저장되있는 stdout 채널을 직접 리턴한다.
        """

        conn = self.getserv_instance(name)
        stdin, stdout, stderr = conn.exec_command(command)
        stdin.close()

        return stdout

        """
        for line in stdout.read().splitlines():
            print 'host: %s: %s' % (name, line)

        return stdout.readlines()
        """

    def run_with_result(self, name, command):

        conn = self.getserv_instance(name)
        stdin, stdout, stderr = conn.exec_command(command)
        stdin.close()

        #return stdout.readlines()
        return stdout.read()

    def run_with_result_line(self, name, command):

        conn = self.getserv_instance(name)
        stdin, stdout, stderr = conn.exec_command(command)
        stdin.close()

        return "".join(stdout.readlines())

    def close(self, name):
        conn = self.getserv_instance(name)
        conn.close()

    def close_all(self):
        for name, conn in self.servinst.iteritems():
            print "%-20s close !!" % (name)
            conn.close()

    def sendCommand(self, ipAddress, cmd, username, password):
        print "----------sendCommand"
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=ipAddress, username=username, password=password)
            cmd_list = [cmd]
            req = ';'.join(cmd_list)
            stdin, stdout, stderr = client.exec_command(req)
            print "sendCommand --> %s" %req
            for line in stdout:
                result.append(line)
#                 logging.info(line.replace('\n',''))
        except Exception, e:
            print "sendCommand error %s" %str(e)
#             sys.exit()

        finally:
            client.close()

        return result
# ------------------------------------------------------------------------------

if __name__ == '__main__':
    rssh = myParamiko()
    print rssh.sendCommand('221.151.188.15', 'service nova-api start')