# -*- coding: utf8 -*-
'''
Created on 2015. 1. 13.

@author: 진수
'''
import json

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders
from config.monitor_config import ap_url_info, key, ha_url_info
from util.aescipher import AESCipher

#다른 서버와 통신하기 위함 클래스
class SoapClient():
    #AP와 통신하기 위한 함수
    def sendData(self, url, httpMethod, reqBody):
        #복호화를 위한 키값 불러오기
        aes = AESCipher(key['key'])
        #AP서버 IP 복호화
        soapUrl = aes.decrypt(ap_url_info['url'])
        http_client = httpclient.HTTPClient()
        #restful통신을 위한 header값 설정
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        #프로비저닝 처리(응답이 없기 때문에 timeout을 1초로 줘서 바로 끊을 수 있도록 처리)
        if url == "/service/provisions":
            #ip와 그 뒤 url 합치기
            create_url = soapUrl+url
            #request형식 만들기(헤더, 바디, url, 메소드, timeout설정)
            request = HTTPRequest(create_url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=1)
        #post방식으로 통신 할때 처리
        elif httpMethod == "post":
            create_url = soapUrl+url
            request = HTTPRequest(create_url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=30)
        #delete방식으로 통신 할때 처리
        elif httpMethod == "delete" :
            delete_url=soapUrl+url+reqBody
            request = HTTPRequest(delete_url, headers=h, method=httpMethod.upper(), body=None, request_timeout=60)
        try:
            #만들어진 request형식을 전송 및 응답 저장
            response = http_client.fetch(request=request)
            #통신 종료
            http_client.close()
            return response.body
        #http통신 에러 처리
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        #그외 에러 처리
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result

    #HA Manager와 통신하기 위한 함수
    def sendHaData(self, url, httpMethod, reqBody):
        aes = AESCipher(key['key'])
        #HA Manager url복호화
        soapUrl = aes.decrypt(ha_url_info['url'])
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        if httpMethod == "post":
            create_url = soapUrl+url
            request = HTTPRequest(create_url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "delete" :
            delete_url=soapUrl+url+reqBody
            request = HTTPRequest(delete_url, headers=h, method=httpMethod.upper(), body=None, request_timeout=3)
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            return response.body
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result

if __name__ == '__main__':
    pass