function master_view(){ // 그래픽뷰->토폴로지 정보
	master = Ext.widget('panel', {
		title : '토폴로지 정보',
		icon : '../static/img/title_icon.png',
		autoScroll: true,
		flex : 1,
		id : 'master',
		html : "<div style='position: relative; margin-left:130px;'>" +
					"<div style='position: absolute;' id='nonsel'>" +
			   			"<img src='../static/img/gigaoffice_bg8.png'>" +
			   		"</div>" +
			   		"<div style='position: absolute;' id='activeCloud' class='hidden'>" +
			   			"<img src='../static/img/gigaoffice_cloud_bg_line_on.png'>" +
			   		"</div>" +
			   		"<div style='position: absolute;' id='activeHwutm' class='hidden'>" +
			   			"<img src='../static/img/gigaoffice_hwutm_bg_line_on.png'>" +
			   		"</div>" +
			   		"<div style='color: #4d4c4c; position: absolute; font-family: verdana; font-size: 14px; left:58px; top: 190px; font-weight: bold;'>" +
					"</div>" +
					"<div id='orgname' style='color: #4d4c4c; position: absolute; font-family: verdana; font-size: 14px; left:310px; top: 40px; font-weight: bold; text-align: center; width: 160px;'>" +
					"</div>" +

			   		"<div id='utm' style='position: absolute; left:235px; top: 76px;' onclick=selectBorder('utm')>" +
			   			"<div style='position: absolute; font-family: verdana; font-size: 13px; color: white; left:14px; top: 8px;'>H/W-UTM" +
			   			"</div>" +
			   			"<img src='../static/img/gigaoffice_hwutm_n.gif'>" +
			   		"</div>" +
			   		"<div style='position: absolute; left:239px; top: 103px; z-index: 10001'>" +
			   			"<i id='utm_ative' class='fa fa-gear'></i>" +
			   		"</div>" +
			   		"<div id='utm_a' style='position: absolute; left:235px; top: 76px;' onclick=selectBorder('utm') class='hidden'>" +
			   			"<div style='position: absolute; font-family: verdana; font-size: 13px; color: white; left:14px; top: 8px;'>H/W-UTM" +
			   			"</div>" +
			   			"<img src='../static/img/gigaoffice_hwutm_n.gif'>" +
		   			"</div>" +
		   			"<div id='utm_border' style='position: absolute; left:235px; top: 76px;' onclick=selectBorder('utm')>" +
			   			"<img src='../static/img/gigaoffice_hwutm_onbox.gif'>" +
		   			"</div>" +

			   		"<div id='cloud_css' style='position: absolute; left:339px; top: 76px;' onclick=selectBorder('cloud')>" +
			   			"<div style='position: absolute; font-family: verdana; font-size: 13px; color: white; left:47px; top: 8px;'>NFV-UTM" +
			   			"</div>" +
			   			"<img src='../static/img/gigaoffice_cloud_n.gif'>" +
			   		"</div>" +
			   		"<div style='position: absolute; left:343px; top: 103px; z-index: 10001'>" +
			   			"<i id='cloud_ative' class='fa fa-gear'></i>" +
			   		"</div>" +
			   		"<div id='cloud_css_a' style='position: absolute; left:339px; top: 76px;' onclick=selectBorder('cloud') class='hidden'>" +
			   			"<div style='position: absolute; font-family: verdana; font-size: 13px; color: white; left:47px; top: 8px;'>NFV-UTM" +
			   			"</div>" +
			   			"<img src='../static/img/monitoringError/gigaoffice_cloud_3seo.gif'>" +
		   			"</div>" +
		   			"<div id='cloud_border' style='position: absolute; left:339px; top: 76px;' onclick=selectBorder('cloud') class='hidden'>" +
			   			"<img src='../static/img/gigaoffice_cloud_onbox.gif'>" +
		   			"</div>" +

		   			"<div id='switchsInfo' style='position: absolute; left:184px; top: 245px;text-align: center;width: 200px;'>" +
				   		"<div id='switchInfo' style='font-size: 14px; margin: auto;'>" +
			   			"</div>" +
			   		"</div>" +

		   			"<div id='serverfarm' style='position: absolute; left:233px; top: 277px;'onclick=selectBorder('serverfarm')>" +
				   		"<div id='farmName' style='color: #4d4c4c; position: absolute; font-family: verdana; font-weight: bold; font-size: 14px; left:40px; top: 8px;'>" +
			   			"</div>" +
			   			"<img src='../static/img/gigaoffice_serverpom_n.gif'>" +
			   		"</div>" +
			   		"<div id='serverfarm_a' style='position: absolute; left:233px; top: 277px;' onclick=selectBorder('serverfarm') class='hidden'>" +
				   		"<div id='farmName_a' style='color: #4d4c4c; position: absolute; font-family: verdana; font-weight: bold; font-size: 14px; left:40px; top: 8px;'>" +
			   			"</div>" +
			   			"<img src='../static/img/monitoringError/gigaoffice_serverpom_3seo.gif'>" +
			   		"</div>" +
			   		"<div id='serverfarm_border' style='position: absolute; left:233px; top: 277px;' onclick=selectBorder('serverfarm') class='hidden'>" +
			   			"<img src='../static/img/gigaoffice_serverpom_onbox.gif'>" +
		   			"</div>" +
		   		"</div>",
		contentEl : "master_view",
        tbar: Ext.create('Ext.toolbar.Toolbar',{// 절체/복구 요청 버튼
    		items: [{xtype:'buttongroup',
				items: [{
					text: '절체 요청',
					cls: 'tttt',
					handler: function() { // 버튼 클릭에 대한 이벤트 처리
						if (userauth == '0' || userauth == '99') {

						}
						if (s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
							Ext.Msg.show({
							    title: '확인',
							    message: '절체 요청 하시겠겠습니까?',
							    width: 300,
							    buttons: Ext.Msg.YESNO,
							    cls: 'x-message-box-alert-color',
							    fn: function(btn) { // confirm창의 선택에 대한 이벤트 처리
							    	if (btn === 'yes') { // yes를 선택했을 때 절체 팝업 생성
							    		var win = window.open("/monitor/fail?orgs="+s_Monitor.config.org_seq+"&types=HW","fail",'width=500, height=200, left=50, top=50, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
							    	}
							    }
							});
						} else {
							Ext.example.msg('절체 요청', '국사를 선택해 주세요.');
						}
					}
				}]
			},{xtype:'buttongroup',
				items: [{
					text: '복구 요청',
					cls: 'tttt',
					handler: function() {
						if (userauth == '0' || userauth == '99') {

						}
						if (s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
							Ext.Msg.show({
							    title: '확인',
							    message: '복구 요청 하시겠겠습니까?',
							    width: 300,
							    buttons: Ext.Msg.YESNO,
							    cls: 'x-message-box-alert-color',
							    fn: function(btn) {
							    	if (btn === 'yes') {
							    		var win = window.open("/monitor/fail?orgs="+s_Monitor.config.org_seq+"&types=NFV","fail",'width=500, height=200, left=50, top=50, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
							    	}
							    }
							});
						} else {
							Ext.example.msg('복구 요청', '국사를 선택해 주세요.');
						}
					}
				}]
			}]
        })
	});
	return master;
}

// 토폴로지 정보 이미지에서 H/W-UTM, NFV-UTM, 고객 서버팜을 선택 했을 때 동작하는 함수
function selectBorder(topology){
	if(topology == 'utm'){ // H/W-UTM 주변 테두리 처리 및 하단 탭 변경
		$('#utm_border').attr('class','');
		$('#cloud_border').attr('class','hidden');
		$('#serverfarm_border').attr('class','hidden');
		detail.setActiveTab(0);
	} else if(topology == 'cloud'){ // NFV-UTM 주변 테두리 처리 및 하단 탭 변경
		$('#utm_border').attr('class','hidden');
		$('#cloud_border').attr('class','');
		$('#serverfarm_border').attr('class','hidden');
		detail.setActiveTab(1);
	} else if(topology == 'serverfarm'){ // 고객 서버팜 주변 테두리 처리 및 하단 탭 변경
		$('#utm_border').attr('class','hidden');
		$('#cloud_border').attr('class','hidden');
		$('#serverfarm_border').attr('class','');
		detail.setActiveTab(3);
	}
}

// H/W-UTM 장비 정보 GRID Panel 생성 함수
function dev_hw_customer_tap(){
	Ext.define('Grid_dev_hw_cust_Model',{
    extend: 'Ext.data.Model',
    fields: [ "appliance_name", "ip_addr", "status", "is_error", "RULE" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_dev_hw_cust_Model',
		storeId : 'grid_dev_hw_utm_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	// 원본 룰 다운로드 기능 처리
	function ruleLinks(val, meta, record) {
		if(val == null || val == ''){
			return "";
		}
		var filename = JSON.parse(val)['original_rule'];
		return '<a target="_self" style="text-decoration:none;" download="../static/ruleFile/runFull' + filename + '" href="../static/ruleFile/runFull/' + filename + '">원본 룰 다운로드</a>'
	}

	// grid panel 생성
	dev_hw_customer_grid_panel = Ext.create('Ext.grid.Panel', {
		flex : 1,
	    store: Ext.data.StoreManager.lookup('grid_dev_hw_utm_store'),
	    columns: [
	        {xtype: 'rownumberer',width: 30, sortable: false},
	        {text: "장비명", width: 100, dataIndex: 'appliance_name', sortable: true},
	        {text: "관리 IP", width: 160, dataIndex: 'ip_addr', sortable: true},
	        {text: "상태", width: 40, align: 'center',
	        	renderer : function(v, m, rec) {if (rec.get('is_error')  == '1') { // 장비 상태 표시 처리
	        				return '<span style="color:red;">off</span>';
	        			} else {
	        				return '<span style="color:green;">on</span>';
	        			}
	        	}, dataIndex: 'result', sortable: true},
		     {text: "RULE", width: 70, renderer : ruleLinks, dataIndex: 'RULE', sortable: true, flex: 1}
	    ],
	    tbar: Ext.create('Ext.toolbar.Toolbar',{ // 상단 툴바 생성(refresh)
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(data_text, s_Monitor.config.org_seq);
				}
			}]
	    })
	});
}

// H/W-UTM 장비 정보 GRID Panel의 데이터 입력 처리 함수
function dev_hw_customer_stores(devHwCustomer) {
	var dev_hw_cust_stores = Ext.data.StoreManager.lookup('grid_dev_hw_utm_store');

	dev_hw_cust_stores.removeAll();

	var error = 0;
	for(var j=0; j<devHwCustomer.length; j++){
		var temp_arr = devHwCustomer[j];
		if(temp_arr[2] == 'ACTIVE'){
			dev_hw_cust_stores.insert(j, { 'appliance_name' : temp_arr[0], 'ip_addr' : temp_arr[1], 'status' : temp_arr[2], 'is_error' : 0, "RULE" : ''});
		} else {
			dev_hw_cust_stores.insert(j, { 'appliance_name' : temp_arr[0], 'ip_addr' : temp_arr[1], 'status' : temp_arr[2], 'is_error' : 1, "RULE" : ''});
			error++;
		}
		temp_arr = [];
	}
	if(error != 0){
		setTimeout(function() {
			$('#utm_a').attr('class','');
			$('#utm').attr('class','hidden');
		}, 100);
	}
}

// H/W-UTM 고객 정보 GRID Panel
function hw_customer_tap(){
	Ext.define('Grid_hw_cust_Model',{
    extend: 'Ext.data.Model',
    fields: [ "appliance_name", "ip_addr", "companyname","status",
              "DHCP", "DNAT", "SNAT", "FIREWALL",
              "ROUTER", "RULE", "is_error", "zone_id",
              "label", "vd", "INCOMING", "OUTGOING", "INTERZONE"]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_hw_cust_Model',
		storeId : 'grid_hw_utm_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	function error_check(val) {
		if (val != 'ACTIVE') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function dhcpLinks(val, meta, record) {//DHCP 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'1\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function dnatLinks(val, meta, record) {//DNAT 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'2\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function snatLinks(val, meta, record) {//SNAT 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'3\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function routerLinks(val, meta, record) {//ROUTER 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'5\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function incomingLinks(val, meta, record) {//INCOMING 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'6\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function outgoingLinks(val, meta, record) {//OUTGOING 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'7\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	function interLinks(val, meta, record) {//INTERZONE 룰 정보 링크
		if(val == null){
			return "";
		}
		return '<a href="#" onclick="utm_rule_pop_data(\'8\', \'' + record.get('userid') + '\'); return false;" style="text-decoration:none;">' + val + '</a>'
	}
	hw_customer_grid_panel = Ext.create('Ext.grid.Panel', {
	    flex : 3,
	    store: Ext.data.StoreManager.lookup('grid_hw_utm_store'),
	    split: true,
		plain: true,
	    columns: [
	        {xtype: 'rownumberer',width: 30, sortable: false},
	        {text: "관리 IP", width: 160, dataIndex: 'ip_addr', sortable: true, hidden: true},
	        {text: "상태", width: 40, align: 'center',
	        	renderer : function(v, m, rec) {if (rec.get('is_error')  == '1') {
	        				return '<span style="color:red;">off</span>';
	        			} else {
	        				return '<span style="color:green;">on</span>';
	        			}
	        	}, dataIndex: 'result', sortable: true, hidden: true},
	        {text: "MSS대표존", width: 70,  dataIndex: 'zone_id', sortable: true},
        	{text: "MSS고객사", width: 100,  dataIndex: 'companyname', sortable: true},
        	{text: "H/W-UTM Label", width: 100,  dataIndex: 'label', sortable: true},
        	{text: "H/W-UTM VD", width: 100,  dataIndex: 'vd', sortable: true},
	        {text: "DHCP", width: 70, align: 'center', renderer : dhcpLinks, dataIndex: 'DHCP', sortable: true},
	        {text: "DNAT", width: 70, align: 'center', renderer : dnatLinks, dataIndex: 'DNAT', sortable: true},
	        {text: "SNAT", width: 70, align: 'center', renderer : snatLinks, dataIndex: 'SNAT', sortable: true},
	        {text: "FW[INCOMING]", width: 100, align: 'center', renderer : incomingLinks, dataIndex: 'INCOMING', sortable: true},
	        {text: "FW[OUTGOING]", width: 100, align: 'center', renderer : outgoingLinks, dataIndex: 'OUTGOING', sortable: true},
	        {text: "FW[INTERZONE]", width: 100, align: 'center', renderer : interLinks, dataIndex: 'INTERZONE', sortable: true},
	        {text: "ROUTER", width: 70, align: 'center', renderer : routerLinks, dataIndex: 'ROUTER', sortable: true},
	        {text: "장비명", width: 100, dataIndex: 'appliance_name', sortable: true, flex: 1}
	    ]
	});
}

//H/W-UTM 고객 정보 GRID Panel 데이터 입력 처리 함수
function hw_customer_stores(hwCustomer) {
	var hw_cust_stores = Ext.data.StoreManager.lookup('grid_hw_utm_store');

	hw_cust_stores.removeAll();

	var error = 0;
	for(var j=0; j<hwCustomer.length; j++){
		var temp_arr = hwCustomer[j];
		if(temp_arr[3] == 'ACTIVE'){
			hw_cust_stores.insert(j, { 'appliance_name' : temp_arr[0], 'ip_addr' : temp_arr[1], 'companyname' : temp_arr[2],'status' : temp_arr[3],
										'DHCP' : temp_arr[4], 'DNAT' : temp_arr[5], 'SNAT' : temp_arr[6], 'userid' : temp_arr[7],
										'RULE' : temp_arr[8], 'is_error' : 0, 'zone_id' : temp_arr[9], 'FIREWALL' : temp_arr[10],
										'ROUTER' : temp_arr[11], "label" : temp_arr[12], "vd" : temp_arr[13], "INCOMING" : temp_arr[14],
										'OUTGOING' : temp_arr[15], 'INTERZONE' : temp_arr[16]});
		} else {
			hw_cust_stores.insert(j, { 'appliance_name' : temp_arr[0], 'ip_addr' : temp_arr[1], 'companyname' : temp_arr[2], 'status' : temp_arr[3],
										'DHCP' : temp_arr[4], 'DNAT' : temp_arr[5], 'SNAT' : temp_arr[6], 'userid' : temp_arr[7],
										'RULE' : temp_arr[8], 'is_error' : 1, 'zone_id' : temp_arr[9], 'FIREWALL' : temp_arr[10],
										'ROUTER' : temp_arr[11], "label" : temp_arr[12], "vd" : temp_arr[13], "INCOMING" : temp_arr[14],
										'OUTGOING' : temp_arr[15], 'INTERZONE' : temp_arr[16]});
			error++;
		}
		if (temp_arr[8] != null && temp_arr[8] != '') {
			Ext.data.StoreManager.lookup('grid_dev_hw_utm_store').findRecord('ip_addr', temp_arr[1]).data.RULE = temp_arr[8];
		}
		temp_arr = [];
	}
	if(error != 0){
		setTimeout(function() {
			$('#utm_a').attr('class','');
			$('#utm').attr('class','hidden');
		}, 100);
	}
	grid_sorters('hw_utm');
}

// NFV-UTM 정보 GRID Panel
function cloud_tap(){
	// contextmenu
	var getNetwork = Ext.create('Ext.Action', {
	    text: '네트워크 정보',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event, a, b, c, d, e) {
	    	network_pop_data();
	    }
	});

	// contextmenu
	var getE2ELog = Ext.create('Ext.Action', {
	    text: '실행이력',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        openE2E();
	    }
	});

	// contextmenu
	var getVNCConsole = Ext.create('Ext.Action', {
	    text: 'VNC 접속',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        serversendcommand('vncconsole');
	    }
	});

	// contextmenu
	var getHostConsole = Ext.create('Ext.Action', {
	    text: 'Host 접속',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        serversendcommand('hostconsole');
	    }
	});

	// contextmenu
	var getWebConsole = Ext.create('Ext.Action', {
		text: 'Web 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('endian');
		}
	});

	// contextmenu
	var getHorizon = Ext.create('Ext.Action', {
		text: 'Horizon 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			openHorizon();
		}
	});

	// contextmenu
	var setServerResume = Ext.create('Ext.Action', {
	    text: '서버 재구동',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        serversendcommand('resume');
	    }
	});

	// contextmenu
	var setServerRestart = Ext.create('Ext.Action', {
	    text: '서버 재시작',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        serversendcommand('rebootserver');
	    }
	});

	// contextmenu
	var setServerSuspendserver = Ext.create('Ext.Action', {
	    text: '서버 정지',
	    icon: '../static/img/stitle_icon.png',
	    handler: function(widget, event) {
	        serversendcommand('suspend');
	    }
	});

	// 현재 상태에 따라 색 표시
	function error_check(val) {
		if (val != 'ACTIVE') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return '<span style="color:green;">' + val + '</span>';
	}

	function cust_name(val, meta, record) {
		if (val == '' || val == null || val == 'null') {
			return '<span style="color : red;">미정</span>'
		}

		return val
	}

	// UTM 세션 리스트 링크
	function links(val, meta, record) {
		return '<a href="#" onclick="section_pop_data(\'' + record.get('vm_id') + '\'); return false;">' + val + '</a>'
	}

	// UTM 생성일에서 문자 제거
	function createvms(val) {
		val = val.replace(/T/gi, " ");
		val = val.replace(/Z/gi, "");
		return val
	}

	// grid panel
	cloud_grid_panel = Ext.create('Ext.grid.Panel', {
	    title: 'NFV-UTM',
	    icon : '../static/img/cloud_icon.png',
	    flex : 3,
	    id : 'cloud_grid_panel',
	    store: Ext.data.StoreManager.lookup('grid_server_store'),
	    columns: [
	        {xtype: 'rownumberer',width: 30, sortable: false},
	        {text: "고객사", width: 100, render : cust_name, dataIndex: 'cust', sortable: true},
	        {text: "사용자", width: 160, dataIndex: 'user', sortable: true},
	        {text: "VLAN", width: 140, dataIndex: 'vlan', sortable: true},
	        {text: "서비스유형", width: 100,  dataIndex: 'cat', sortable: true},
	        {text: "노드", width: 70, dataIndex: 'node', sortable: true},
	        {text: "상태", width: 90, renderer : error_check, dataIndex: 'state', sortable: true},
	        {text: "UTM이름", width: 230, dataIndex: 'vm_name', sortable: true},
	        {text: "UTM생성일", width: 150, renderer : createvms, dataIndex: 'createvm', sortable: true},
	        {text: "UTM구동시간", width: 150, dataIndex: 'ativedt', sortable: true},
	        {text: "UTM세션수", width: 150, renderer : links, dataIndex: 'conntrackcnt', sortable: true, flex: 1}
	    ],
        listeners: {
            itemcontextmenu: function(view, rec, node, index, e) {
                e.stopEvent();

                var getRow = cloud_grid_panel.getSelectionModel().getSelection()[0]; // 선택한 VM 값 가져요기
    	        pseq = getRow.data.provisionseq; // provisioningseq 값 저장
    	        vmid = getRow.data.vm_id; // VM ID 저장

    	        contextMenus.destroy(); // 생성된 contextmenu 제거

    	        contextMenus = Ext.create('Ext.menu.Menu', { // contextmenu 구현
    	    	    items: [ getNetwork, getE2ELog, '-', getVNCConsole, getHostConsole, getWebConsole, getHorizon, '-', setServerResume, setServerRestart, setServerSuspendserver ]
    	    	});

                contextMenus.showAt(e.getXY()); // contextmenu 표시
                return false;
            }
        },
        tbar: Ext.create('Ext.toolbar.Toolbar',{//상단 툴바 구현(refresh버튼)
    		items: ['->', {
    			icon: '../static/img/Very-Basic-Refresh-icon.png',
    			cls: 'x-btn-icon',
    			handler: function() {
    				call_org_state(data_text, s_Monitor.config.org_seq);
    			}
    		}]
        }),
        viewConfig: {
            getRowClass: function(record, index, rowParams, store) { // 장애로 판단된 row를 빨간 글씨로 표시(해당 row에 class추가)
                if (record.data.is_error == '1') {
                	return 'error';
                }
            }
        }
	});
}

//고객 서버팜 GRID Panel
function server_Cust_Tap() {
	Ext.define('Grid_serverCust_Model',{
	    extend: 'Ext.data.Model',
	    fields: [ "org", "cust_name", "server_name", "server_ip", "server_type", "store_dttm", "release_dttm", "is_error" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_serverCust_Model',
		storeId : 'grid_serverCust_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		},
	});

	// grid panel 구현
	server_farm_grid_panel = Ext.create('Ext.grid.Panel', {
	    title: '고객 서버팜',
	    flex : 1,
	    icon : '../static/img/person_server_pom.png',
		id: 'server_farm_grid_panel',
	    store: Ext.data.StoreManager.lookup('grid_serverCust_store'),
	    columns: [
	        {text: "국사", width: 70, dataIndex: 'org', sortable: true},
	        {text: "고객사", width: 100, dataIndex: 'cust_name', sortable: true},
	        {text: "장치명", width: 150, renderer : function(val, meta, record)
	        	{
		        	if(record.data.is_error == '1'){
		        		return '<span style="color:red;">' + val + '</span>';
		        	}
		        	return val;
	        	}, dataIndex: 'server_name', sortable: true},
	        {text: "장치IP", width: 150, dataIndex: 'server_ip', sortable: true},
	        {text: "장치유형", width: 100, dataIndex: 'server_type', sortable: true},
	        {text: "상태", width: 40, align: 'center', renderer :
	        	function(v, meta, rec) {
			        	if (rec.get('status')  == 'DEACTIVE') {
		    				return '<span style="color:red; text-align: center;">실패</span>';
		    			} else {
		    				return '<span style="color:green; text-align: center;">성공</span>';
		    			}
	        		}, dataIndex: 'status', sortable: true},
	        {text: "입고일", width: 150, dataIndex: 'store_dttm', sortable: true},
	        {text: "출고일", width: 150, dataIndex: 'release_dttm', sortable: true, flex: 1}
	    ],
        listeners: {
            itemcontextmenu: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            }
        },
        viewConfig: {
            getRowClass: function(record, index, rowParams, store) {
            }
        },
        tbar: Ext.create('Ext.toolbar.Toolbar',{ // 상단 툴바
    		items: ['->', {
    			icon: '../static/img/Very-Basic-Refresh-icon.png',
    			cls: 'x-btn-icon',
    			handler: function() {
    				call_org_state(data_text, s_Monitor.config.org_seq);
    			}
    		}]
        })
	});
}

// 고객 서버팜 grid panel 데이터 저장 처리
function serverCust_stores(serverCustomer) {
	var server_stores = Ext.data.StoreManager.lookup('grid_serverCust_store');
	var errors = 0;
	var total = 0;
	server_stores.removeAll();

	var server_id = '';
	for (var i=0; i<serverCustomer.length; i++) {
		var datas = serverCustomer[i];
		total++;
		if(datas[7] == 'ACTIVE' || datas[7] == null){
			server_stores.insert(i, { 'org' : datas[0], 'cust_name' : datas[1], 'server_name' : datas[2], 'server_ip' : datas[3],
				'server_type' : datas[4], 'store_dttm' : datas[5], 'release_dttm' : datas[6], 'status' : datas[7], 'is_error' : 0 });
		} else {
			server_stores.insert(i, { 'org' : datas[0], 'cust_name' : datas[1], 'server_name' : datas[2], 'server_ip' : datas[3],
				'server_type' : datas[4], 'store_dttm' : datas[5], 'release_dttm' : datas[6], 'status' : datas[7], 'is_error' : 1 });
			errors++;
		}
	}

	if(errors != 0){
		setTimeout(function() {
			$('#serverfarm_a').attr('class','');
			$('#serverfarm').attr('class','hidden');
		}, 100);
	} else {
		setTimeout(function() {
			$('#serverfarm_a').attr('class','hidden');
			$('#serverfarm').attr('class','');
		}, 100);
	}

	s_Monitor.config.serverfarm_total = total;
	s_Monitor.config.serverfarm_error = errors;
	$('#farmName').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
	$('#farmName_a').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
}

// 원본룰/변환 룰 비교 grid panel
function utm_rule_pop_grid() {
	Ext.define('Grid_UtmRule_Model',{
	    extend: 'Ext.data.Model',
	    fields: [ "original_data", "parsing_data" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_UtmRule_Model',
		storeId : 'grid_utm_rule_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	// grid panel
	utm_rule_pop_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'utm_rule_pop_grid',
	    store: Ext.data.StoreManager.lookup('grid_utm_rule_store'),
	    region: 'center',
	    columnLines: true,
	    flex: 1,
	    height: '100%',
	    columns: [
	        {text: "H/W-UTM rule", flex: 1, dataIndex: 'original_data', sortable: false},
	        {text: "NFV-UTM rule", flex: 1, dataIndex: 'parsing_data', sortable: false}
	    ],
        listeners: {
			itemdblclick: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            },
        },
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// 원본룰/변환룰 비교 팝업 생성(ExtJs lib)
function utm_rule_pop() {
	if (!win_utm) { // 팝업이 생성여부 판단
		win_utm = Ext.create('widget.window', {
            title: 'H/W-UTM rule & NFV-UTM rule compare',
            closable: true,
            closeAction: 'hide',
            constrain: true,
            width: 900,
            height: 500,
            layout: 'border',
            items: [utm_rule_pop_grid_panel]
        });
	}

	// 팝업의 현재 상태 확인(show/hidden)
	if (win_utm.isVisible()) {
    } else {
    	win_utm.show(this, function() {});
    }
}

// 원본룰/변환룰 정보 조회 및 저장
function utm_rule_pop_data(type, userid) {
	var utm_rule_stores = Ext.data.StoreManager.lookup('grid_utm_rule_store');
	utm_rule_stores.removeAll();

	Ext.Ajax.request({
		url : '/monitor/utmRuleDetail',
		type : 'POST',
		dataType : 'json',
		params : {utmtype : type, utmid : userid, orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			var result = JSON.parse(JSON.parse(data.responseText).result);

			utm_rule_stores.insert(0, {
				"original_data" : '<pre style="white-space: pre-line;">' + result['original_data'] + '</pre>',
				"parsing_data" : '<pre style="white-space: pre-line;">' + result['parsing_data'] + '</pre>'
			});
		}
	});

	utm_rule_pop();
}