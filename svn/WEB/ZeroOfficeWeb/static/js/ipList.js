function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("ipService",function($scope,$http,$window,$timeout) {
		$scope.init = function(){
			$scope.userDel = false;
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUserBtn = true;
			$scope.checked = false;
			$scope.searchUserId = "";
			$scope.searchUserName = "";
			$scope.searchOrgName = "";
			$scope.modal = "modal";
			$scope.orgseq = 0;
			$scope.orgname = 0;
			$scope.allocateYn = "0";
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			if($scope.searchIp ==null || $scope.searchIp==""){
				$scope.searchIp ="";
			}

			if($scope.searchUserId ==null || $scope.searchUserId==""){
				searchUserId ="";
			} else {
				searchUserId = "%"+$scope.searchUserId+"%";
			}

			$.ajax({
		        type:'post',
		        data : {
		        	ip : $scope.searchIp,
		        	userid : searchUserId,
		        	orgseq : $scope.orgseq,
		        	page : page,
		        	allocateYn : $scope.allocateYn, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/ip/ipList',
		        success: function(data) {
		        	$scope.ipList = JSON.parse(data.ip)
		        	$scope.orgList = JSON.parse(data.orgs);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({
							page:i
						});
					}
					$("#user").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addIp = function(){
			$scope.startIp = "";
			$scope.endIp = "";
		};

		$scope.ipSubmit = function(){
			var re = /^[0-9]+$/;
			var result = true;
	        if(!re.test($scope.startIp1) || !re.test($scope.startIp2) || !re.test($scope.startIp3) || !re.test($scope.startIp4) || !re.test($scope.endIp)) {
	        	$.SmartMessageBox({
					title : "IP를 등록 할 수가 없습니다",
					content : "숫자를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
	        }
	        if($scope.insertOrg == ""){
	        	$.SmartMessageBox({
					title : "IP를 등록 할 수가 없습니다",
					content : "국사를 선택해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
	        }
	        if(result){
	        	if(Number($scope.startIp4) > Number($scope.endIp)){
	        		$.SmartMessageBox({
						title : "IP을 등록 할 수가 없습니다",
						content : "StartIP 가 EndIP 보다 클수가 없습니다.",
						buttons : '[닫기]'
					});
	        	} else {
	        		var ips = [];
	        		for(var i = Number($scope.startIp4); i <= Number($scope.endIp); i++){
	        			ips.push({
	        				ip : $scope.startIp1 + "." + $scope.startIp2 + "." + $scope.startIp3 + "." + i
	        			})
	        		}
	        		$.ajax({
				        type:'post',
				        data : {
				        	ip : JSON.stringify(ips), _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/ip/ipCheck',
				        success: function(data) {
				        	if(data.response == "succ"){
				        		$.ajax({
							        type:'post',
							        data : {
							        	orgseq : $scope.insertOrg,
							        	ip : JSON.stringify(ips), _xsrf : getCookie("_xsrf")
							        },
							        dataType:'json',
							        url: '/ip/ipInsert',
							        success: function(data) {
							        	$('#insertIp').modal('hide');
							        	$scope.init();
							        },
							        error: function(jqXHR, textStatus, errorThrown) {
							            alert("데이터 실패");
							        }
							    });
				        	} else {
				        		$.SmartMessageBox({
									title : "IP를 등록 할 수가 없습니다",
									content : "등록 되어 있는 IP 가 있습니다.",
									buttons : '[닫기]'
								});
				        	}
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
	        	}
	        }
		};

		$scope.ipDetail = function(ip, userid, orgseq, allocate){
			$scope.modifyIp = ip;
			$scope.modifyId = userid;
			$scope.modifyOrg = orgseq;
			$scope.modifyAllocate = allocate;
		};

		$scope.ipModify = function(){
			if($scope.modifyAllocate == "Y" && ($scope.modifyId == "" && $scope.modifyId == null)){
				$.SmartMessageBox({
					title : "IP를 수정 할 수가 없습니다",
					content : "ID를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
			} else {
				if($scope.modifyAllocate == "미사용") {
					$scope.modifyId = "";
					$scope.modifyAllocate = "N";
				} else {
					$scope.modifyAllocate = "Y";
				}
				$.ajax({
			        type:'post',
			        data : {
			        	ip : $scope.modifyIp,
			        	userid : $scope.modifyId,
			        	orgseq : $scope.modifyOrg,
			        	allocate : $scope.modifyAllocate, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/ip/ipModify',
			        success: function(data) {
			        	$('#detailIp').modal('hide');
			        	$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			}
		};

		$scope.ipDelete = function(){
			$.SmartMessageBox({
				title : "IP 관리 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.ipList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								ip : $("#tdValue"+i).html()
							});
						}
					}
					if($scope.item == ""){
						$.SmartMessageBox({
							title : "IP를 삭제 할 수 없습니다",
							content : "IP를 선택하여 주시기 바랍니다.",
							buttons : '[닫기]'
						});
					} else {
						$.ajax({
					        type:'post',
					        data : {
					        	ip : JSON.stringify($scope.item), _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/ip/ipDelete',
					        success: function(data) {
					        	$.smallBox({
							        title : "삭제 완료",
							        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 2000,
							        icon : "fa fa-bell"
							    });
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').attr('class','active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
