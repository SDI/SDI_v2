function ruleLink(seq, reg) {
	var win = window.open("/monitor/ruledetail?seq="+seq+"&rt="+reg,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

function rules() {
	Ext.define('Grid_Rule_Model',{
		extend: 'Ext.data.Model',
		fields: [ "seq", "reg", "host", "ip", "name", "case", "mod", "prc", "msg", "dt", "username" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_Rule_Model',
		storeId : 'grid_rule_store',
		autoLoad: false,
		pageSize: 50,
		proxy : {
			type: 'ajax',
			api: {
				read: '/monitor/ruleCompareErrorList'
			},
			reader: {
				type: 'json',
				root: 'items',
				totalProperty: 'totalCount'
			}
		},
	});

	function cust_name(val, meta, record) {
		if (val == '' || val == null || val == 'null') {
			if (record.data.username == '' || record.data.username == null || record.data.username == 'null') {
				return ''
			}
			return '<span style="color : gray;">' + record.data.username + '</span>'
		}

		return val
	}

	function links(val, meta, record) {
		return '<a href="#" onclick="ruleLink(\'' + record.data.seq + '\', \'' + record.data.reg + '\'); return false;">' + val + '</a>';
	}

	rule_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'nfv_rule_panel',
		store: Ext.data.StoreManager.lookup('grid_rule_store'),
		renderTo: Ext.getBody(),
		autoScroll: true,
		columns: [
		    {text: "번호", width: 70, dataIndex: 'seq', sortable: true},
			{text: "장비명", width: 120, dataIndex: 'host', sortable: true},
			{text: "장치IP", width: 120, dataIndex: 'ip', sortable: true},
			{text: "CASE TYPE", width: 100,
				renderer : function(val, meta, record) {
					if (val == 'null' || val == null) {
						return ''
					} else if (val == '명명규칙 위반') {
						return '<span style="color: #E86F0B;">' + val + '</span>';
					} else if (val == '미지원 기능') {
						return '<span style="color: #FFC500;">' + val + '</span>';
					} else {
						return val;
					}
				}, dataIndex: 'case', sortable: true},
			{text: "분류", width: 100, dataIndex: 'mod', sortable: true},
			{text: "내용", width: 400, renderer : links, dataIndex: 'msg', sortable: true},
			{text: "고객사", width: 150, renderer : cust_name, dataIndex: 'name', sortable: true},
			{text: "처리", width: 100, dataIndex: 'prc', sortable: true},
			{text: "일시", width: 100, dataIndex: 'dt', sortable: true, flex: 1},
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
		},
		dockedItems: [{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			store: Ext.data.StoreManager.lookup('grid_rule_store'),
			displayMsg: '현재데이터수 : {0} - {1}/총 데이터수 : {2}',
			emptyMsg: '데이터가 존재하지 않습니다.'
		}]
	});
}

Ext.EventManager.onWindowResize(function(w, h){
	rule_grid_panel.setSize(w, h);
});

function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

Ext.onReady(function() {
	rules();
	Ext.data.StoreManager.lookup('grid_rule_store').getProxy().setExtraParam('orgs', orgs);
	Ext.data.StoreManager.lookup('grid_rule_store').getProxy().setExtraParam('ts', ts);
	Ext.data.StoreManager.lookup('grid_rule_store').loadPage(1);
});