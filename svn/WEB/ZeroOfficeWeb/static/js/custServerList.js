function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);
	var ip_test = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("custServer",function($scope,$http,$window,$timeout) {
		$scope.init = function(){
			$scope.userDel = false;
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUserBtn = true;
			$scope.checked = false;
			$scope.searchServerName = "";
			$scope.searchServerType = "";
			$scope.searchUserName = "";
			$scope.searchOrgName = "";
			$scope.modal = "modal";
			$scope.orgseq = 0;
			$scope.orgname = 0;
			$scope.paging(1);
			$scope.state = 'i';
			$scope.publicIpYn = 'n';
			$scope.noteYn = 'n';
			$scope.serverseq = '';
        	$scope.serverName = '';
        	$scope.insertCust = '';
        	$scope.serverIp = '';
        	$scope.serverType = '';
        	$scope.storeDttm = '';
        	$scope.releaseDttm = '';
        	$scope.locat = '';
        	$scope.spec = '';
        	$scope.note = '';
        	$scope.noteYn = '';
        	$scope.modistoreDttm = '';
        	$scope.modireleaseDttm = '';
        	$scope.customername = '';
		};

		$scope.paging = function(page) {
			if($scope.searchServerName ==null || $scope.searchServerName==""){
				searchServerName ="";
			} else {
				searchServerName = "%"+$scope.searchServerName+"%";
			}

			if($scope.searchServerType ==null || $scope.searchServerType==""){
				searchServerType ="";
			} else {
				searchServerType = "%"+$scope.searchServerType+"%";
			}

			if($scope.customername ==null || $scope.customername==""){
				customername ="";
			} else {
				customername = "%"+$scope.customername+"%";
			}

			$.ajax({
		        type:'post',
		        data : {
		        	servername : searchServerName,
		        	servertype : searchServerType,
		        	customername : customername,
		        	orgseq : $scope.orgseq,
		        	page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/cust/serverList',
		        success: function(data) {
		        	console.log(data);
		        	$scope.custList = JSON.parse(data['custs']);
		        	$scope.serverList = JSON.parse(data['custserver']);
		        	$scope.orgList = JSON.parse(data.orgs);
		        	$scope.$apply();
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({
							page:i
						});
					}
					$("#user").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addVlan = function(){
			$scope.startVlan = "";
			$scope.endVlan = "";
		};

		$scope.custSubmit = function(){
			var re = /^[0-9]+$/;
			var result = true;
			if($scope.serverName == '' || $scope.serverName == null){
				$.SmartMessageBox({
					title : "장치명이 입력되지 않았습니다.",
					content : "장치명을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.insertCust == '' || $scope.insertCust == null){
				$.SmartMessageBox({
					title : "고객사가 선택되지 않았습니다.",
					content : "고객사를 선택해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.serverIp == '' || $scope.serverIp == null){
				$.SmartMessageBox({
					title : "장치IP가 입력되지 않았습니다.",
					content : "장치IP를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if(!ip_test.test($scope.serverIp)) {
				$.SmartMessageBox({
					title : "장치IP 형식이 맞지 않았습니다.",
					content : "장치IP 형식을 확인해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.publicIpYn == '' || $scope.publicIpYn == null){
				$.SmartMessageBox({
					title : "사설/공인 IP 여부가 입력되지 않았습니다.",
					content : "사설/공인 IP 여부를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.serverType == '' || $scope.serverType == null){
				$.SmartMessageBox({
					title : "장치유형이 입력되지 않았습니다.",
					content : "장치유형을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.state == '' || $scope.state == null){
				$.SmartMessageBox({
					title : "입출고 상태가 입력되지 않았습니다.",
					content : "입출고 상태를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.state == 'i'){
				$scope.releaseDttm = '';
				if($scope.storeDttm == ''){
					$.SmartMessageBox({
						title : "입고일이 입력되지 않았습니다.",
						content : "입고일을 입력해 주시기 바랍니다.",
						buttons : '[닫기]'
					});
		        	result = false;
				}
			}
			if($scope.state == 'o' || $scope.state == null){
				$scope.storeDttm = '';
				if($scope.releaseDttm == ''){
					$.SmartMessageBox({
						title : "출고일이 입력되지 않았습니다.",
						content : "출고일을 입력해 주시기 바랍니다.",
						buttons : '[닫기]'
					});
		        	result = false;
				}
			}
			if($scope.locat == '' || $scope.locat == null){
				$.SmartMessageBox({
					title : "위치가 입력되지 않았습니다.",
					content : "위치를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.spec == '' || $scope.spec == null){
				$.SmartMessageBox({
					title : "장비사양이 입력되지 않았습니다.",
					content : "장비사양을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.note != '' && $scope.note != null){
				$scope.noteYn = 'y';
			}
	        if(result){
        		$.ajax({
			        type:'post',
			        data : {
			        	serverName : $scope.serverName,
			        	insertCust : $scope.insertCust,
			        	serverIp : $scope.serverIp,
			        	publicIpYn : $scope.publicIpYn,
			        	serverType : $scope.serverType,
			        	state : $scope.state,
			        	storeDttm : $scope.storeDttm,
			        	releaseDttm : $scope.releaseDttm,
			        	locat : $scope.locat,
			        	spec : $scope.spec,
			        	note : $scope.note,
			        	noteYn : $scope.noteYn, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/cust/serverInsert',
			        success: function(data) {
			        	$('#insertServer').modal('hide');
			        	$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
	        }

		};

		$scope.serverDetail = function(serverseq, servername, customerseq, server_ip, public_ip_yn, server_type, state, store_dttm, release_dttm, locat, spec, note){
			$scope.serverseq = serverseq;
			$scope.serverName = servername;
        	$scope.insertCust = customerseq;
        	$scope.serverIp = server_ip;
        	$scope.publicIpYn = public_ip_yn;
        	$scope.serverType = server_type;
        	$scope.state = state;
        	$scope.modistoreDttm = store_dttm;
        	$scope.modireleaseDttm = release_dttm;
        	$scope.locat = locat;
        	$scope.spec = spec;
        	$scope.note = note;
		};

		$scope.custClose = function() {
			$scope.init();
    		$scope.$apply();
		}

		$scope.custModify = function(){
			var result = true;
			if($scope.serverName == '' || $scope.serverName == null){
				$.SmartMessageBox({
					title : "장치명이 입력되지 않았습니다.",
					content : "장치명을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.insertCust == '' || $scope.insertCust == null){
				$.SmartMessageBox({
					title : "고객사가 선택되지 않았습니다.",
					content : "고객사를 선택해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.serverIp == '' || $scope.serverIp == null){
				$.SmartMessageBox({
					title : "장치IP가 입력되지 않았습니다.",
					content : "장치IP를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if(!ip_test.test($scope.serverIp)) {
				$.SmartMessageBox({
					title : "장치IP 형식이 맞지 않았습니다.",
					content : "장치IP 형식을 확인해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.publicIpYn == '' || $scope.publicIpYn == null){
				$.SmartMessageBox({
					title : "사설/공인 IP 여부가 입력되지 않았습니다.",
					content : "사설/공인 IP 여부를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.serverType == '' || $scope.serverType == null){
				$.SmartMessageBox({
					title : "장치유형이 입력되지 않았습니다.",
					content : "장치유형을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.state == '' || $scope.state == null){
				$.SmartMessageBox({
					title : "입출고 상태가 입력되지 않았습니다.",
					content : "입출고 상태를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.state == 'i'){
				$scope.modireleaseDttm = '';
				if($scope.modistoreDttm == ''){
					$.SmartMessageBox({
						title : "입고일이 입력되지 않았습니다.",
						content : "입고일을 입력해 주시기 바랍니다.",
						buttons : '[닫기]'
					});
		        	result = false;
				}
			}
			if($scope.state == 'o' || $scope.state == null){
				$scope.modistoreDttm = '';
				if($scope.modireleaseDttm == ''){
					$.SmartMessageBox({
						title : "출고일이 입력되지 않았습니다.",
						content : "출고일을 입력해 주시기 바랍니다.",
						buttons : '[닫기]'
					});
		        	result = false;
				}
			}
			if($scope.locat == '' || $scope.locat == null){
				$.SmartMessageBox({
					title : "위치가 입력되지 않았습니다.",
					content : "위치를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if($scope.spec == '' || $scope.spec == null){
				$.SmartMessageBox({
					title : "장비사양이 입력되지 않았습니다.",
					content : "장비사양을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
			}
			if(result){
				$.ajax({
			        type:'post',
			        data : {
			        	serverseq : $scope.serverseq,
			        	serverName : $scope.serverName,
			        	insertCust : $scope.insertCust,
			        	serverIp : $scope.serverIp,
			        	publicIpYn : $scope.publicIpYn,
			        	serverType : $scope.serverType,
			        	state : $scope.state,
			        	storeDttm : $scope.modistoreDttm,
			        	releaseDttm : $scope.modireleaseDttm,
			        	locat : $scope.locat,
			        	spec : $scope.spec,
			        	note : $scope.note,
			        	noteYn : $scope.noteYn, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/cust/serverModify',
			        success: function(data) {
			        	$('#detailServer').modal('hide');
			        	$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			}
		};

		$scope.serverDelete = function(){
			$.SmartMessageBox({
				title : "고객서버 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.serverList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								serverseq : $("#seq"+i).html()
							});
						}
					}
					if($scope.item == "" || $scope.item == null){
						$.SmartMessageBox({
							title : "서버를 삭제 할 수 없습니다",
							content : "서버를 선택하여 주시기 바랍니다.",
							buttons : '[닫기]'
						});
					} else {
						$.ajax({
					        type:'post',
					        data : {
					        	serverseq : JSON.stringify($scope.item), _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/cust/serverDelete',
					        success: function(data) {
					        	$.smallBox({
							        title : "삭제 완료",
							        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 2000,
							        icon : "fa fa-bell"
							    });
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		$("#storeDttm").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>'
		});
		$("#releaseDttm").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>'
		});
		$("#modistoreDttm").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>'
		});
		$("#modireleaseDttm").datepicker({
			changeMonth : true,
			numberOfMonths : 1,
			dateFormat : 'yy-mm-dd',
			prevText : '<i class="fa fa-chevron-left"></i>',
			nextText : '<i class="fa fa-chevron-right"></i>'
		});
		functionMenu();
		init();
	});

	function init() {
		$('#lp_service_ul').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_service_nfv').removeClass('active');
		$('#lp_service_list').removeClass('active');
		$('#lp_service_custServer').attr('class','active');
		$("#main").css("visibility","visible");
	}
