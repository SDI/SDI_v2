function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("switchService",function($scope,$timeout) {
		$scope.init = function(){
			$scope.modify = false;
			$scope.submit = false;
			$scope.addSwitchBtn = true;
			$scope.checked = false;
			$scope.mgmtIp1 = 0;
			$scope.mgmtIp2 = 0;
			$scope.mgmtIp3 = 0;
			$scope.mgmtIp4 = 0;
			$scope.insertSwitchPort = 1;
			$scope.insertSwitchPortStart = 1;
			$scope.orgseq = "0";
			$scope.modal = "modal";
			$scope.sw_seq = "0";
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			$.ajax({
		        type:'post',
		        data : {
					orgseq : $scope.orgseq,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/switch/switchList',
		        success: function(data) {
		        	$scope.switchList = JSON.parse(data.switchs);
		        	$scope.orgList = JSON.parse(data.orgs);
		        	$scope.orgseq = data.orgseq;
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addSwitch = function(){
			$scope.insertSwitchName = "";
			$scope.insertSwitchType = "SDN";
			$scope.mgmtIp1 = 0;
			$scope.mgmtIp2 = 0;
			$scope.mgmtIp3 = 0;
			$scope.mgmtIp4 = 0;
			$scope.insertSwitchPort = 1;
			$scope.insertSwitchPortStart = 1;
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
		};

		$scope.switchSubmit = function(){
			var num_check=/^[0-9]*$/;
			if (!num_check.test($scope.insertSwitchPort)) {
				$.smallBox({
			        title : "물리 포트수 오류",
			        content : "숫자를 입력해 주세요.",
			        color : "#e8a0dc",
			        timeout: 2000
			    });
				return false;
			} else if($scope.insertSwitchPort < 1){
				$.smallBox({
			        title : "물리 포트수 오류",
			        content : "입력하신 포트수가 1보다 작습니다.",
			        color : "#e8a0dc",
			        timeout: 2000
			    });
				return false;
			} else if(!num_check.test($scope.insertSwitchPortStart)){
				$.smallBox({
			        title : "논리 포트 시작번호",
			        content : "숫자를 입력해 주세요.",
			        color : "#e8a0dc",
			        timeout: 2000
			    });
				return false;
			} else if($scope.insertSwitchPortStart < 1){
				$.smallBox({
			        title : "물리 포트수 오류",
			        content : "입력하신 포트수가 1보다 작습니다.",
			        color : "#e8a0dc",
			        timeout: 2000
			    });
				return false;
			} else if ($scope.mgmtIp1 < 0 || $scope.mgmtIp1 > 255 || $scope.mgmtIp2 < 0 || $scope.mgmtIp2 > 255 || $scope.mgmtIp3 < 0 || $scope.mgmtIp3 > 255 || $scope.mgmtIp4 < 0 || $scope.mgmtIp4 > 255) {
				$.smallBox({
			        title : "IP 형식 오류",
			        content : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4 + "로 IP를 입력 하셨습니다.",
			        color : "#e8a0dc",
			        timeout: 2000
			    });
				return false;
			} else {
				$.ajax({
					type:'post',
			        data : {
						mgmtIp : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4,
						_xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/switch/switchCheck',
			        success: function(data) {
			        	if (data.result == 't') {
			        		$scope.switchCommit();
			        	} else {
			        		alert("중복된 MGMT IP가 존재 합니다.");
			        	}
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
				});
			}
		};

		$scope.switchCommit = function() {
			$.ajax({
		        type:'post',
		        data : {
		        	switchName : $scope.insertSwitchName,
		        	switchType : $scope.insertSwitchType,
					mgmtIp : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4,
					port : $scope.insertSwitchPort,
					start : $scope.insertSwitchPortStart,
					end : $scope.insertSwitchPortStart + $scope.insertSwitchPort - 1,
					orgSeq : $scope.insertOrgseq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/switch/insertSwitch',
		        success: function(data) {
		        	$.smallBox({
				        title : "등록 완료",
				        content : $scope.insertSwitchName+"로 스위치 등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000
				    });
					$('#insertSwitch').modal('hide');
					$scope.init();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		}

		$scope.switchDetail = function(sw_seq){
			$scope.sw_seq = sw_seq;
			$scope.modalTitle = "수정";
			$.ajax({
		        type:'post',
		        data : {
		        	sw_seq : sw_seq, _xsrf : getCookie("_xsrf"),
		        },
		        dataType:'json',
		        url: '/switch/switchDetail',
		        success: function(data) {
		        	$scope.insertSwitchName = data.switchs[0].sw_name;
		        	$scope.insertSwitchType = data.switchs[0].sw_type;
		        	var ipArr = data.switchs[0].sw_mgmt_ip.split('.');
		        	$scope.mgmtIp1 = ipArr[0] * 1;
					$scope.mgmtIp2 = ipArr[1] * 1;
					$scope.mgmtIp3 = ipArr[2] * 1;
					$scope.mgmtIp4 = ipArr[3] * 1;
					$scope.insertSwitchPort = data.switchs[0].sw_port;
					$scope.insertOrgseq = data.switchs[0].orgseq;
					$scope.insertSwitchPortStart = data.switchs[0].sw_start;
					$scope.modify = true;
					$scope.submit = false;
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.switchModify = function(){
			$.ajax({
		        type:'post',
		        data : {
		        	sw_seq : $scope.sw_seq,
		        	switchName : $scope.insertSwitchName,
		        	switchType : $scope.insertSwitchType,
					mgmtIp : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4,
					port : $scope.insertSwitchPort,
					start : $scope.insertSwitchPortStart,
					end : $scope.insertSwitchPortStart + $scope.insertSwitchPort - 1,
					orgSeq : $scope.insertOrgseq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/switch/updateSwitch',
		        success: function(data) {
		        	$('#insertSwitch').modal('hide');
					$.smallBox({
				        title : "수정 완료",
				        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000
				    });
					$scope.init();
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("수정 실패");
		        }
		    });
		};

		$scope.switchClose = function(){
			$scope.orgModal = false;
		};

		$scope.switchDelete = function(){
			$.SmartMessageBox({
				title : "스위치 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.switchList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								sw_seq : $("#tdValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	sw_seq : $scope.item[j].sw_seq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/switch/deleteSwitch',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertSwitch').draggable();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').attr('class','active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
