function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("OrgService",function($scope,$http,$window,$timeout) {
		$scope.init = function(){
			$scope.dongName = "";
			$scope.orgName = "";
			$scope.searchAddr1 = "";
			$scope.searchAddr2 = "";
			$scope.searchAddr3 = "";
			$scope.addrseq = '';

			$scope.orgTable = false;
			$scope.dongSearch = true;
			$scope.resultAddr = false;
			$scope.addrSubmitBtn = false;

			$scope.orgTable = false;
			$scope.resultAddr = false;
			$scope.dongSearch = true;
			$scope.checked = false;
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			$scope.pagination1=true;
			$scope.pagination2=false;

			$.ajax({
		        type:'post',
		        data : {
		        	page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/org/orgList',
		        success: function(data) {
		        	$scope.orgList = JSON.parse(data.orgs);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({
							page:i
						});
					}
					$("#user").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

	    $scope.orgSearch = function(page){
			$scope.pagination1=false;
			$scope.pagination2=true;
			if($scope.searchAddr1 == null || $scope.searchAddr1 == ""){
				searchAddr1 = "";
			}else{
				searchAddr1 = "%"+$scope.searchAddr1+"%";
			}

			if($scope.searchAddr2 == null || $scope.searchAddr2 == ""){
				searchAddr2 = "";
			}else{
				searchAddr2 ="%"+$scope.searchAddr2+"%";
			}

			if($scope.searchAddr3 == null || $scope.searchAddr3 == ""){
				searchAddr3 = "";
			}else{
				searchAddr3 ="%"+$scope.searchAddr3+"%";
			}

			$.ajax({
		        type:'post',
		        data : {
		        	searchAddr1 : searchAddr1,
		        	searchAddr2 : searchAddr2,
		        	searchAddr3 : searchAddr3,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/org/orgSearch',
		        success: function(data) {
		        	$scope.orgList = JSON.parse(data.orgs);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					$scope.searchAddr1 = "";
					$scope.searchAddr2 = "";
					$scope.searchAddr3 = "";

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current2"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		// 국사 등록 버튼 실행
		$scope.addOrg = function(){
			$scope.modalTitle = "등록";
			$scope.dongName = "";
			$scope.orgName = "";
			$scope.orgTable = false;
			$scope.addrModifyBtn = false;
			$scope.addrSubmitBtn = false;
		};

		// 국사등록 Div 동 조회
		$scope.searchDong = function(){
	    	var dongName = "";
	    	dongName = $scope.dongName;
	    	if(dongName == ''){
	    		$.SmartMessageBox({
					title : "주소 조회를 진행 할 수가 없습니다",
					content : "동을 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	    	} else {
	    		$.ajax({
			        type:'post',
			        data : {
			        	dongName : dongName, _xsrf : getCookie("_xsrf"),
			        },
			        dataType:'json',
			        url: '/org/addressSearch',
			        success: function(data) {
			        	console.log(JSON.stringify(data.address));
			        	$scope.addrList = data.address;
						$scope.orgTable = true;
						$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
	    	}
	    };

	    // 주소 선택
		$scope.selectedAddr = function(addr1, addr2, addr3, addrseq){
			$scope.addr1 = addr1;
			$scope.addr2 = addr2;
			$scope.addr3 = addr3;
			$scope.addrseq = addrseq;
			$scope.orgTable = false;
			$scope.dongSearch = false;
			$scope.resultAddr = true;
			$scope.addrSubmitBtn = true;
		};

		// 국사 등록 및 주소 테이블 업데이트
		$scope.submitOrg = function(){
			if ($scope.orgName != '' && $scope.orgName != null) {
				$.ajax({
			        type:'post',
			        data : {
			        	orgname : $scope.orgName,
						addr1 : $scope.addr1,
						addr2 : $scope.addr2,
						addr3 : $scope.addr3,
						addrs : $scope.addrseq,
						_xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/org/insertOrg',
			        success: function(data) {
			        	var orgseq = data.orgseq[0].orgseq;
			        	var addrseq = data.addrseq[0].addrseq;
			        	$.ajax({
					        type:'post',
					        data : {
					        	orgseq : orgseq,
					        	addrseq : addrseq, _xsrf : getCookie("_xsrf"),
					        },
					        dataType:'json',
					        url: '/org/updateAddr',
					        success: function(data) {
					        	$.smallBox({
					        		title : "등록 완료",
							        content : "등록을 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 2000,
							        icon : "fa fa-bell"
							    });
								$('#insertOrg').modal('hide');
								$scope.init();
					    		$scope.$apply();
								$scope.closeOrg();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					            alert("데이터 실패");
					        }
					    });
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			} else {
				$.SmartMessageBox({
					title : "국사를 등록할 수 없습니다.",
					content : "국사명이 입력되지 않았습니다.",
					buttons : '[닫기]'
				});
			}
		};

		// 국사 등록 취소
		$scope.closeOrg = function(){
			$scope.orgTable = false;
			$scope.dongSearch = true;
			$scope.resultAddr = false;
		};

		$scope.orgDetail = function(orgseq){
			$scope.modalTitle = "수정";
	    	$scope.orgModal = true;
	    	$scope.resultAddr = true;
	    	$scope.addrModifyBtn = true;
			$scope.orgTable = false;
			$scope.dongSearch =  false;
			$scope.addrSubmitBtn = false;
			$.ajax({
		        type:'post',
		        data : {
		        	orgseq : orgseq, _xsrf : getCookie("_xsrf"),
		        },
		        dataType:'json',
		        url: '/org/orgDetail',
		        success: function(data) {
		        	console.log(JSON.stringify(data.orgs[0].addr1));
		        	$scope.addr1 = data.orgs[0].addr1;
					$scope.addr2 = data.orgs[0].addr2;
					$scope.addr3 = data.orgs[0].addr3;
					$scope.orgName = data.orgs[0].orgname;
					$scope.orgSeq = data.orgs[0].orgseq;
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.modifyOrg = function(){
			$.ajax({
		        type:'post',
		        data : {
		        	orgseq : $scope.orgSeq,
					orgname : $scope.orgName, _xsrf : getCookie("_xsrf"),
		        },
		        dataType:'json',
		        url: '/org/modifyOrg',
		        success: function(data) {
		        	$('#insertUser').modal('hide');
					$.smallBox({
				        title : "수정 완료",
				        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000,
				        icon : "fa fa-bell"
				    });
					$scope.init();
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("수정 실패");
		        }
		    });
		};

		$scope.userClose = function(){
			$scope.orgModal = false;
		};

		$scope.deleteOrg = function(){
			$.SmartMessageBox({
				title : "국사 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.orgList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								orgseq : $("#orgseq"+i).val()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	orgseq : $scope.item[j].orgseq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/org/deleteOrg',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').attr('class','active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
