/**
 * E2E Trace 상세정보 조회
 */

var col = ["매니저", "구분", "메시지", "요청명령", "요청파라미터", "응답결과", "시작시간", "종료시간", "소요시간", "결과", "상세메시지"];

function traces() {
	Ext.define('Grid_E2E_Model',{
	    extend: 'Ext.data.Model',
	    fields: [ "col", "msg", "idx" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_E2E_Model',
		storeId : 'grid_e2e_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	// 성공/실패, JSON표현 등의 데이터 표현 가공
	function error_check(val, meta, rec) {
		console.log(val);
		if (rec.internalId == 11) {
			//return '<pre>' + val.replace(/ /gi, '<br>') + '</pre>';
			val = val.replace(/\\n/gi, ',<br>');
			return '<pre>' + val.replace(/, /gi, ',<br>') + '</pre>';
		}
		if (val == '실패') {
			return '<span style="color:red;">' + val + '</span>';
		} else if (val == 'null' || val == null) {
			return '';
		}
		if (val.indexOf("{") > -1) {
			var te = JSON.parse(val);
			return '<pre>' + JSON.stringify(te, null, 4) + '</pre>';
		}
		return val;
	}

	// grid panel
	trace_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'e2e_trace_panel',
		renderTo: Ext.getBody(),
		/*maxHeight: 690,*/
	    store: Ext.data.StoreManager.lookup('grid_e2e_store'),
	    columns: [
	        {text: "", width: 135, dataIndex: 'col', sortable: true, tdCls: 'title-column'},
	        {text: "", width: 280, renderer : error_check, dataIndex: 'msg', sortable: true, tdCls: 'content-column', flex: 1}
	    ],
        listeners: {
            /* itemcontextmenu: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            }, */
			itemdblclick: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            },
        },
        buttons : [{
			text : '닫기',
			handler : function() {self.close();}
		}],
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// 원본/변환 룰 비교 팝업
function rule_compare() {
	var win = window.open("/monitor/ruleCompare?seq="+seq+'&rt='+reg,"compare",'width=1010, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// 팝업 창 크기 변화에 따른 grid panel 리사이즈
Ext.EventManager.onWindowResize(function(w, h){
	trace_grid_panel.setSize(w, h);
});

// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

Ext.onReady(function() {
	// 팝업의 title 제어
	if (titles == '0') {
		document.title = 'HA Manager 실행이력 상세';
	} else if (titles == '1') {
		document.title = 'Sync Manager 실행이력 상세';
	} else if (titles == '2') {
		document.title = 'SDI Provisioning 실행이력 상세';
	} else if (titles == '3') {
		document.title = '백업서비스 Provisioning 실행이력 상세';
	} else if (titles == '4') {
		document.title = 'Provisioning 실행이력 상세';
	}

	// grid panel 호출(초기화)
	traces();

	// 정렬
	var traces_stores = Ext.data.StoreManager.lookup('grid_e2e_store');
	traces_stores.sort([ { property: 'idx', direction: 'ASC' } ]);

	// E2E Trace 상세정보 조회
	Ext.Ajax.request({
		url : '/monitor/tracedetail',
		type : 'POST',
		dataType : 'json',
		params : {seq : seq, comm : comm, reg : reg, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			trace = JSON.parse(data.responseText).result;
			console.log(trace);

			for (var i=0; i<trace[0].length; i++) {
				if (i < 10) {
					traces_stores.insert(i, {
						"idx" : i,
						"col" : col[i],
						"msg" : trace[0][i+1]
					});
				} else if (i == 10 && trace[0].length < 19) {
					traces_stores.insert(i, {
						"idx" : i,
						"col" : col[i],
						"msg" : trace[0][i+1]
					});
				} else if (i == 18) {
					traces_stores.insert(10, {
						"idx" : 10,
						"col" : col[10] + '<br><br>' + trace[0][16] + ' 룰 정보<br><a href="/" onclick="rule_compare(); return false;">룰 비교',
						"msg" : trace[0][11]
					});
				}/*else if (i == 18) {
					var msg = '<pre>';
					for (var j=0; j<trace[0][18].length; j++) {
						msg += '<br>' + trace[0][18][j];
					}
					msg = msg.replace('<br>', '');
					msg += '</pre>';

					traces_stores.insert(i, {
						"idx" : i,
						"col" : trace[0][16]+' 룰 정보<br><a href="/" onclick="rule_compare(); return false;">룰 비교</a>',
						"msg" : msg
					});
				}*/
			}

			$('#viewLoading').hide();
		}
	});
});