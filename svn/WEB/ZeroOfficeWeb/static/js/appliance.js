/**
 * 현재 사용 안함
 * @param name
 * @returns
 */

function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

	$("#addTooltip").tooltip();
	$("#listTooltip").tooltip();
	$('#btn-upload').on('click',function() {
						if (($("#file1").val() == "" || $("#file1").val() == null)) {

							$.SmartMessageBox({
								title : "이미지를 업로드 할 수 없습니다.",
								content : "Service Icon을 선택하세요",
								buttons : '[닫기]'
							});

						} else if (($("#file2").val() == "" || $("#file2").val() == null)) {

							$.SmartMessageBox({
								title : "이미지를 업로드 할 수 없습니다.",
								content : "Service Image를 선택하세요",
								buttons : '[닫기]'
							});
						} else {

							$("#uploadForm").ajaxForm({

								dataType : 'text',
								cache: false,
							    processData: false,
							    contentType: false,
								success : function(response) {
									$("#progress1").css("width",
											"100%");
									$("#progress1").text("100%");
								},
								error : function(jqXHR) {
									console.log(jqXHR);
									alert('error');
								}
							});
						}
					});
	$("#fromdate").datepicker({
		changeMonth : true,
		numberOfMonths : 1,
		dateFormat : 'yy-mm-dd',
		prevText : '<i class="fa fa-chevron-left"></i>',
		nextText : '<i class="fa fa-chevron-right"></i>',
		onClose : function(selectedDate) {
						$("#todate").datepicker("option","minDate",selectedDate);
		}
	});
	$("#todate").datepicker({
						changeMonth : true,
						numberOfMonths : 1,
						dateFormat : 'yy-mm-dd',
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
										$("#fromdate").datepicker("option","maxDate",selectedDate);
						}
	});



	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("AppService",function($scope,$http,$window,$timeout) {

		var cnt = 1;
		var start,end;
		$scope.dtlcode="0";
		$scope.getAppList = function(page){

			if($scope.searchNeCatNm ==null || $scope.searchNeCatNm==""){
				$scope.searchNeCatNm ="";
			}

			if($scope.searchAppNm ==null || $scope.searchAppNm==""){
				searchAppNm ="";
			} else {
				searchAppNm = "%"+$scope.searchAppNm+"%";
			}

			if($scope.searchServiceNm ==null || $scope.searchServiceNm==""){
				searchServiceNm ="";
			} else {
				searchServiceNm = "%"+$scope.searchServiceNm+"%";
			}

			if($scope.searchSpec ==null || $scope.searchSpec==""){
				$scope.searchSpec ="";
			}

			if($scope.todate ==null || $scope.todate==""){
				$scope.todate ="";
			}

			if($scope.fromdate ==null || $scope.fromdate==""){
				$scope.fromdate ="";
			}

			$.ajax({
		        type:'post',
		        data : {
		        	dtlcode : $scope.dtlcode,
					necatNm : $scope.searchNeCatNm,
					appNm : searchAppNm,
					serviceNm : searchServiceNm,
					serverSpec : $scope.searchSpec,
					enddt : $scope.todate,
					startdt : $scope.fromdate,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/appliance/appList',
		        success: function(data) {
		        	$scope.necatList = JSON.parse(data.apps);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({
							page:i
						});
					}
					$scope.dtl_Code = JSON.parse(data.dtlCode);
					$scope.dtlData = JSON.parse(data.dtl);
					$scope.applianceList=true;
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.imgUpload = function() {
			$scope.file1 = $("#file1").val().replace(
					/c:\\fakepath\\/i, "");
			$scope.file2 = $("#file2").val().replace(
					/c:\\fakepath\\/i, "");

		};

		$scope.changed = function() {
			$scope.neclscode="";
		};

		$scope.addService = function() {
			if ($scope.serviceNm == "" || $scope.serviceNm == null) {
				$.SmartMessageBox({
					title : "Service를 추가 할 수 없습니다.",
					content : "서비스명을 입력하세요.",
					buttons : '[닫기]'
				});
			} else {
				if (cnt == 1) {
					$scope.li = "active";
					$scope.tabPane = "active in";
					$(".liTab").css("display", "inline-block");
					$("#seviceAdd").addClass("disabled");
				}
				$scope.cnt = cnt;

				$scope.service.push({
					name : $scope.serviceNm,
					image : $scope.image,
					dtlcode : $scope.dtlcode,
					neclsseq : $scope.neclscode,
					icon_small : "/soweb2/resources/catalog/image/"
							+ $scope.file1,
					icon_large : "/soweb2/resources/catalog/image/"
							+ $scope.file2,
					item : $scope.item

				});

				cnt++;
			}
		};

		$scope.addItem = function() {

			$scope.item.push({
				name : $scope.name,
				display_name : $scope.display_name,
				default_value : $scope.default_value,
				type : $scope.type,
				target : $scope.target,
				action : $scope.action,
				delimiter : $scope.delimiter,
				dir : $scope.url,
				display_yn : $scope.display_yn
			});
			$scope.name = "";
			$scope.display_name = "";
			$scope.type = "";
			$scope.target = "";
			$scope.action = "";
			$scope.delimiter = "";
			$scope.url = "";
			$scope.default_value = "";
			$scope.display_yn = "";
		};

		$scope.appSubmit = function(classify) {
			if ($scope.appNm == "" || $scope.appNm == null) {
				$.SmartMessageBox({
					title : "등록 할 수 없습니다.",
					content : "Application명을 입력하세요",
					buttons : '[닫기]'
				});
			} else if ($scope.serviceNm == ""
					|| $scope.serviceNm == null) {

				$.SmartMessageBox({
					title : "등록 할 수 없습니다.",
					content : "서비스명을 입력하세요",
					buttons : '[닫기]'
				});
			} else if (cnt == 1) {
				$.SmartMessageBox({
					title : "등록 할 수 없습니다.",
					content : "서비스를 추가하세요.",
					buttons : '[닫기]'
				});
			} else {
				if($scope.imageId == null || $scope.imageId==""){
					$scope.imageId = {
							id:"",
							name:""
					}
				}
				var service = [{
					name : $scope.serviceNm,
					item : $scope.item,
					name : $scope.appNm,
					image : $scope.image,
					dtlcode : $scope.dtlcode,
					neclsseq : parseInt($scope.neclscode),
					icon_small : "/soweb2/resources/catalog/image/"
							+ $scope.file1,
					icon_large : "/soweb2/resources/catalog/image/"
							+ $scope.file2,
					imageid : $scope.imageId.id,
					imagename : $scope.imageId.name,
					serverspec : $scope.serverSpec,
					rootpassword : $scope.rootPwd
				}];
				var ap = {
					service : service,
					name : $scope.serviceNm,
					serverspec : $scope.serverSpec
				};
				var applince = {
					application : ap
				};
				$scope.applince = applince;
				console.log(JSON.stringify($scope.applince));

				if(classify=="1"){
					console.log("aaa");
					$.ajax({
				        type:'post',
				        data : {
				        	appNm : $scope.appNm, _xsrf : getCookie("_xsrf"),
							serviceNm : $scope.serviceNm,
							neclsseq : parseInt($scope.neclscode),
							icon_small : "/soweb2/resources/catalog/image/"
									+ $scope.file1,
							icon_large : "/soweb2/resources/catalog/image/"
									+ $scope.file2,
							imagename : $scope.imageId.name,
							service : JSON.stringify(applince)
				        },
				        dataType:'json',
				        url: '/appliance/appSubmit',
				        success: function(data) {
				        	if (data.result == "success"){
								$.SmartMessageBox({
									title : "Appliance 등록 성공",
									content : "Appliance가 정상적으로 등록 되었습니다.",
									buttons : '[닫기]'
								});
							}else if(data.result == "fail"){
								$.SmartMessageBox({
									title : "Appliance 등록 실패",
									content : "Appliance 등록 중 오류가 발생 했습니다.",
									buttons : '[닫기]'
								});
							}
				    		$scope.$apply();
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
				}else if(classify=="2"){
					alert("수정버튼 클릭");
				}
			};
		};

		$scope.trClick = function(necatSeq){
			$scope.addAppliance = true;
			$scope.applianceList = false;
			$scope.editNecatBtn = true;
			$scope.addNecatBtn = false;
			$("#main").css("height","950");
			$scope.appInit();
			$("#seviceAdd").addClass("disabled");
			$.ajax({
		        type:'post',
		        data : {
		        	necatSeq : necatSeq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        async : false,
		        url: '/appliance/getNecat',
		        success: function(data) {
					var temp = data.data[0].config_props;
					console.log(JSON.stringify($scope.dtl_Code));
					$scope.item= temp.provision.application[0].service[0].item;
					$scope.dtlcode = temp.provision.application[0].service[0].dtlcode;
					console.log(JSON.stringify($scope.dtlcode));
					$("#neclscode").val(temp.provision.application[0].service[0].neclsseq);
					$scope.appNm = temp.provision.application[0].name;
					$scope.serviceNm = temp.provision.application[0].service[0].name;
					$scope.serverSpec = temp.provision.application[0].serverspec;
					$scope.rootPwd = temp.provision.application[0].rootpassword;
					/*$scope.file1 = temp.provision.application[0].service[0].icon_small.replace("/soweb2/resources/catalog/image/","");
					$scope.file2 = temp.provision.application[0].service[0].icon_large.replace("/soweb2/resources/catalog/image/","");*/
					/*$scope.imageId = temp.provision.application[0].service[0].image;*/
					$scope.addService();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.delItem = function(idx) {
			$scope.item[idx] = null;
			var items = $scope.item;
			$scope.item = [];
			angular.forEach(items, function(data) {
				if (data != null) {
					$scope.item.push(data);
				}
			});

		};

		$scope.delBtn = function(){
			$scope.delItem=[];
			for(var i=0; i<$scope.necatList.length; i++){
				if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
					$scope.delItem.push({
						seq : $("#necatseq"+i).text()
					});
				}
			}
			if($scope.delItem.length == 0){
				$.SmartMessageBox({
					title : "삭제 실패",
					content : "삭제 할 Template을 선택 하세요.",
					buttons : '[닫기]'
				});
			}else{
				console.log(JSON.stringify($scope.delItem));
				$.ajax({
			        type:'post',
			        data : {
			        	data : JSON.stringify($scope.delItem), _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/appliance/appDelete',
			        success: function(data) {
			        	$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
			        	console.log(JSON.stringify(data.data));
						$scope.getAppList(1);
			        }, error: function(jqXHR, textStatus, errorThrown) {
			            alert("provision 실패");
			        }
				});
			}
		};

		$scope.showList = function(){
			$scope.addAppliance = false;
			$scope.applianceList = true;
			$("#main").css("height","500");
			$scope.appInit();
		};
		$scope.showAdd = function(){
			$scope.addAppliance = true;
			$scope.applianceList = false;
			$scope.addNecatBtn = true;
			$scope.editNecatBtn= false;
			$("#main").css("height","950");
			$scope.appInit();
		};
		$scope.appInit=function(){
			$scope.dtlcode ="0";
			$scope.searchNeCatNm ="";
			$scope.searchSpec ="";
			$scope.todate ="";
			$scope.fromdate ="";
			$scope.service=[];
			$("#seviceAdd").removeClass("disabled");
			$scope.item= [];
			$("#neclscode").val("");
			$scope.appNm="";
			$scope.serviceNm="";
			$scope.serverSpec="";
			$scope.searchServiceNm = "";
			$scope.searchAppNm = "";
			$scope.rootPwd="";
			$scope.file1="";
			$scope.file2="";

		};

		$("#main").css("visibility","visible");


	});
	$(document).ready(function() {
		functionMenu();
		init();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').attr('class','active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
