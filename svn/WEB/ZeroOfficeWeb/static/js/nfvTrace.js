// E2E Trace 상세정보 팝업
function e2edtlink(reg) {
	var win = window.open("/monitor/tracedetail?seq="+seq+"&comm="+comm+"&rt="+reg+"&titles="+titles,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// E2E Trace 팝업(Sync Manager에서 provisioningseq 링크에 사용)
function open_e2e(seq) {
	var win = window.open("/monitor/e2etrace?seq="+seq+"&comm=2","e2e",'width=730, height=600, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

function traces() {
	Ext.define('Grid_E2E_Model',{
	    extend: 'Ext.data.Model',
	    fields: [ "seq", "mgr", "type", "msg", "req_cmd", "req_para", "resp", "st", "et", "exc", "result", "detail", "reg", "axgate", "thread", "pseq", "indent", "warn" ]
	});

	Ext.create('Ext.data.Store', {
		model: 'Grid_E2E_Model',
		storeId : 'grid_e2e_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	// 데이터 가공
	function error_check(val) {
		if (val != null && val != ''){
			if (val.indexOf("<br>") > -1) {
				console.log(val);
				val = val.replace(/<br>/gi, ", ");
			}
		}
		if (val == '실패') {
			return '<span style="color:red;">' + val + '</span>';
		} else if (val == 'null') {
			val = '';
		} else if (val == '성공') {
			return '<span style="color:green;">' + val + '</span>';
		}
		return val;
	}

	// indent level에 따른 들여쓰기 표현
	function indent_check(val, meta, rec) {
		if (rec.get('indent')  > 0) {
			val = '└───&nbsp;' + val;
            for (var i=1; i<rec.get('indent'); i++) {
            	val = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + val;
            }
        }
		return '<a href="#" onclick="e2edtlink(\'' + rec.data.reg + '\'); return false;">' + val + '</a>';
	}

	// provisoining 링크
	function link_prov(val, meta, rec) {
		if (val == null || val == '' || val == 'null' || val == 'None') {
			return ''
		}
		return '<a href="#" onclick="open_e2e(\'' + val + '\'); return false;">' + val + '</a>';
	}

	if (comm != '1'){// Provision Manager / HA Manager
		trace_grid_panel = Ext.create('Ext.grid.Panel', {
			id: 'e2e_trace_panel',
			renderTo: Ext.getBody(),
		    store: Ext.data.StoreManager.lookup('grid_e2e_store'),
		    autoScroll: true,
		    flex: 1,
		    //maxHeight: 590,
		    columns: [
		        {text: "매니저", width: 90, renderer : error_check, dataIndex: 'mgr', sortable: true},
		        {text: "구분", width:100, renderer : error_check, dataIndex: 'type', sortable: true},
		        {text: "메시지", width: 420, renderer : indent_check, dataIndex: 'msg', sortable: true},
		        {text: "결과", width: 40, align: 'center', renderer : error_check, dataIndex: 'result', sortable: true},
	            {text: "소요시간", width: 70, renderer : function(val, meta, record) { if (val == 'null' || val == null) { return '' } return '<span style="color:' + record.data.warn + ';">' + val + '</span>'; }, dataIndex: 'exc', sortable: true},
		        {text: "시작시간", width: 110, renderer : error_check, dataIndex: 'st', sortable: true},
		        {text: "종료시간", width: 110, renderer : error_check, dataIndex: 'et', sortable: true, flex: 1},
		    ],
	        listeners: {
	            itemcontextmenu: function(view, rec, node, index, e) {
	                e.stopEvent();
	                return false;
	            },
				itemdblclick: function(view, rec, node, index, e) {
	                e.stopEvent();
	                console.log(rec.data);
	                var win = window.open("/monitor/tracedetail?seq="+seq+"&comm="+comm+"&rt="+rec.data.reg+"&titles="+titles,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	                return false;
	            },
	        },
	        bbar: Ext.create('Ext.ux.StatusBar',{
				id: 'basic-statusbar',
				defaultText: 'Ready',
				defaultIconCls: 'x-status-valid',
				items: ['->', {xtype:'buttongroup',
					items: [{
						text: '닫기',
						cls: 'tttt',
						width: 75,
						handler : function() {self.close();}
					}]
				}]
			}),
			viewConfig: {
				getRowClass : function(record) {
					if (record.get('type') == 'Rule Sync' || record.get('type') == 'VIM-mgr' || record.get('type') == 'TEST-mgr') {
						return 'white_row'
					} else {
						return 'blue_row'
					}
				}
			}
		});
	} else { // Sync Manager
		trace_grid_panel = Ext.create('Ext.grid.Panel', {
			id: 'e2e_trace_panel',
			renderTo: Ext.getBody(),
		    store: Ext.data.StoreManager.lookup('grid_e2e_store'),
		    autoScroll: true,
		    flex: 1,
		    //maxHeight: 590,
		    columns: [
		        {text: "매니저", width: 90, renderer : error_check, dataIndex: 'mgr', sortable: true},
		        {text: "구분", width:100, renderer : error_check, dataIndex: 'type', sortable: true},
		        {text: "메시지", width: 420, renderer : indent_check, dataIndex: 'msg', sortable: true},
		        /*{text: "결과", width: 40, align: 'center', xtype: 'actioncolumn', items: [{
	                getClass: function(v, meta, rec) {
	                    if (rec.get('result')  == '실패') {
	                        return 'is_fail';
	                    } else {
	                        return 'is_success';
	                    }
	                }
	            }], dataIndex: 'result', sortable: true},*/
		        {text: "결과", width: 40, align: 'center', renderer : error_check, dataIndex: 'result', sortable: true},
	            {text: "소요시간", width: 70, renderer : function(val, meta, record) { if (val == 'null' || val == null) { return '' } return '<span style="color:' + record.data.warn + ';">' + val + '</span>'; }, dataIndex: 'exc', sortable: true},
		        {text: "시작시간", width: 110, renderer : error_check, dataIndex: 'st', sortable: true},
		        {text: "종료시간", width: 110, renderer : error_check, dataIndex: 'et', sortable: true},
		        {text: "프로비저닝", width: 70, renderer : link_prov, dataIndex: 'pseq', sortable: true, flex: 1},
		    ],
	        listeners: {
	            itemcontextmenu: function(view, rec, node, index, e) {
	                e.stopEvent();
	                return false;
	            },
				itemdblclick: function(view, rec, node, index, e) {
	                e.stopEvent();
	                console.log(rec.data);
	                var win = window.open("/monitor/tracedetail?seq="+seq+"&comm="+comm+"&rt="+rec.data.reg+"&titles="+titles,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	                return false;
	            },
	        },
	        bbar: Ext.create('Ext.ux.StatusBar',{
				id: 'basic-statusbar',
				defaultText: 'Ready',
				defaultIconCls: 'x-status-valid',
				items: ['->', {xtype:'buttongroup',
					items: [{
						text: '닫기',
						cls: 'tttt',
						width: 75,
						handler : function() {self.close();}
					}]
				}]
			}),
			viewConfig: {
				getRowClass : function(record) {
					if (record.get('type') == 'CustomInfo Sync' || record.get('type') == 'VIM-mgr' || record.get('type') == 'TEST-mgr') {
						return 'white_row'
					} else {
						return 'blue_row'
					}
				}
			}
		});
	}
}

// 팝업창 크기 변경에 따른 내부 크기 조절
Ext.EventManager.onWindowResize(function(w, h){
	trace_grid_panel.setSize(w, h);
});

// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

var call_trace = function () {
	Ext.getCmp('basic-statusbar').setStatus({ text: '진행중', iconCls: 'x-status-busy'}); // 현재 진행 상태 표시

	var traces_stores = Ext.data.StoreManager.lookup('grid_e2e_store');
	var type = '';

	// E2E Trace 정보 조회
	Ext.Ajax.request({
		url : '/monitor/e2etrace',
		type : 'POST',
		dataType : 'json',
		params : {seq : seq, comm : comm, org : org, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			if (end > 0) { // E2E Trace의 완료 여부 확인
				Ext.TaskManager.stop(task); // task 종료
				Ext.getCmp('basic-statusbar').setStatus({ text: '완료', iconCls: 'x-status-valid'}); // 하단에 완료로 상태 표시
			}
			trace = JSON.parse(data.responseText).result;

			var is_fail = false;
			var is_end = false;

			for (var i=0; i<trace.length; i++) {
				if (trace[i][3] == 'end') {
					is_end = true;
					if (trace[i][10] == '실패') {
						is_fail = true;
					}
				}

		    	if (trace[i] != null) {
			    	if (traces_stores.getAt(i) != null) {
				    } else {
					    if (comm != '1'){ // Provision Manager / HA Manager
						    if (trace[i][3] != 'end') {
						    	traces_stores.insert(i, {
									"seq" : trace[i][0],
									"mgr" : trace[i][1],
									"type" : trace[i][2],
									"msg" : trace[i][3],
									"req_cmd" : trace[i][4],
									"req_para" : trace[i][5],
									"resp" : trace[i][6],
									"st" : trace[i][7],
									"et" : trace[i][8],
									"exc" : trace[i][9],
									"result" : trace[i][10],
									"detail" : trace[i][11],
									"reg" : trace[i][12],
									"indent" : trace[i][13],
									"warn" : trace[i][14]
								});
						    }
					    } else { // Sync Manager
					    	if (trace[i][3] != 'end') {
						    	traces_stores.insert(i, {
									"seq" : trace[i][0],
									"mgr" : trace[i][1],
									"type" : trace[i][2],
									"msg" : trace[i][3],
									"req_cmd" : trace[i][4],
									"req_para" : trace[i][5],
									"resp" : trace[i][6],
									"st" : trace[i][7],
									"et" : trace[i][8],
									"exc" : trace[i][9],
									"result" : trace[i][10],
									"detail" : trace[i][11],
									"reg" : trace[i][12],
									"axgate" : trace[i][13],
									"thread" : trace[i][14],
									"pseq" : trace[i][15],
									"indent" : trace[i][16],
									"warn" : trace[i][17]
								});
					    	}
						}
					}
		    	}
			}

			if (end == 0) { // 완료 여부 확인
				if (is_end) {
					if (is_fail) {
						Ext.TaskManager.stop(task);
						Ext.getCmp('basic-statusbar').setStatus({ text: '완료', iconCls: 'x-status-valid'});
						Ext.Msg.alert('안내','종료되었습니다.');
					}/* else {
						Ext.TaskManager.stop(task);
						Ext.Msg.alert('안내','완료되었습니다.');
					}*/
				}
			}

			$('#viewLoading').hide();
		}
	});

	// 데이터 정렬
	if (comm != '1') {
		traces_stores.sort([ { property: 'reg', direction: 'ASC' } ]);
	} else {
		traces_stores.sort([ { property: 'axgate', direction: 'ASC' },
		                     { property: 'thread', direction: 'ASC' },
		                     //{ property: 'pseq', direction: 'ASC' },
		                     { property: 'reg', direction: 'ASC' } ]);
	}
}

Ext.onReady(function() {
	// 팝업의 title 표시 제어
	if (titles == '0') {
		document.title = 'HA Manager 실행이력';
	} else if (titles == '1') {
		document.title = 'Sync Manager 실행이력';
	} else if (titles == '2') {
		document.title = 'SDI Provisioning 실행이력';
	} else if (titles == '3') {
		document.title = '백업서비스 Provisioning 실행이력';
	} else if (titles == '4') {
		document.title = 'Provisioning 실행이력';
	}

	// grid panel 초기화
	traces();

	// grid panel 크기 초기화
	trace_grid_panel.setSize(window.innerWidth, window.innerHeight);

	// 3초마다 task 실행(E2E Trace 데이터 조회)
	task = Ext.TaskManager.start({
		run : call_trace,
		interval : 3000
	});
});