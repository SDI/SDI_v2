#!/bin/bash

TODAY=`/bin/date +%Y%m%d%H%M%S`
echo "======================================================"
echo "Backup SyncManager Start ...!!!"

HOME_PATH="/usr/local"
SYNC_PATH="$HOME_PATH/Sync"
BACK_PATH="$HOME_PATH/Sync_Backup"
LOG_FILE="$BACK_PATH/Backup_List.log"
#############################################################
# SyncManager DIR CHECKING
#############################################################
echo "------------------------------------------------------"
echo " 1. SyncManager Dir Checking ..."
if [ -d $SYNC_PATH ] ; then
    echo "   - Exists Sync Path : [SYNC_PATH]"
else
   echo "   - None Exists Sync Path : [SYNC_PATH]"
   echo "   - [ERROR] Confirm SyncManager Path !!!!"
   echo "------------------------------------------------------"
   echo "Backup SyncManager Failed ...!!! [$BACK_FILE]" >> $LOG_FILE
   echo "Backup SyncManager End ...!!!"
   echo "======================================================"
   exit 0
fi

#############################################################
# BACKUP DIR CHECKING
#############################################################
echo "------------------------------------------------------"
echo " 2. Backup Dir Checking ..."
if [ -d $BACK_PATH ] ; then
    echo "   - Exists Backup Path : [$BACK_PATH]"
    echo "   - 30day ago Backup File Delete..."
    
    find $BACK_PATH -mtime +31 -name Sync_\* -exec rm -f {} \;
    #find $BACK_PATH -mtime +31 -name Sync_\* -type d -exec rm -rf {} \;

else
    echo "   - Make Backup Path : [$BACK_PATH]"
    mkdir -p $BACK_PATH
    chmod 755 $BACK_PATH
fi
echo "------------------------------------------------------"

#############################################################
# BACKUP CHECKING
#############################################################
#RUN_CMD="tar -zcvf"
RUN_CMD="tar -zcf"
RUN_PATH="./Sync"
BACK_FILE="$BACK_PATH/Sync_$TODAY.tar.gz"
echo " 3. Backup SyncManager Running ..."
echo "   - cmd : cd $HOME_PATH"
echo "   - cmd : $RUN_CMD $BACK_FILE $RUN_PATH"

cd $HOME_PATH
$RUN_CMD $BACK_FILE $RUN_PATH

echo "------------------------------------------------------"
echo "Backup SyncManager Success ...!!! [$BACK_FILE]" >> $LOG_FILE
echo "Backup SyncManager End ...!!!"
echo "======================================================"

