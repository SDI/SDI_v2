#-*- coding: utf-8 -*-
'''
Created on 2015-02-08
@author: 정현성
@summary: 메인 스케줄러
    testScheduled - 테스트용 스케줄
'''

import config
from pgSql.SyncManagerSql import SyncManagerSql
from scheduler.DBAccess import DBAccess
from scheduler.HttpClient import HttpClient
from util.aescipher import AESCipher
import sys
import datetime
import json
from util.Logger import Logger

log = Logger()

def config_decoding():
    aes = AESCipher(config.SyncConfig.security_key)
    config.SyncConfig.postgresql_db_info["db_host"] = aes.decrypt(config.SyncConfig.postgresql_db_info["db_host"])
    config.SyncConfig.postgresql_db_info["id"] = aes.decrypt(config.SyncConfig.postgresql_db_info["id"])
    config.SyncConfig.postgresql_db_info["pw"] = aes.decrypt(config.SyncConfig.postgresql_db_info["pw"])
    
    config.SyncConfig.mss_linkage_info["host"] = aes.decrypt(config.SyncConfig.mss_linkage_info["host"])
    
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"])
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"])
    
    config.SyncConfig.orchestrator_server_info["info"]["ip"] = aes.decrypt(config.SyncConfig.orchestrator_server_info["info"]["ip"])
    
    config.SyncConfig.utm_password = aes.decrypt(config.SyncConfig.utm_password)
    
def delete_utm_scheduled(arg_caller, arg_orgseq):
    
    log.info("%s" % (config.SyncConfig.line_split))
    log.info("1. DeleteUTM Start Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
    log.info("   time : %s" % str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
    log.info("%s" % (config.SyncConfig.sub_split))
        
    try:
        #DB커넥션 연결.
        connect = DBAccess()
        sql = SyncManagerSql()
        
        ###########################################
        #테스트 용도로 만들어진 로직입니다.
        utm_info = connect.select(sql.select_test_delete_list(arg_orgseq))
  
        client = HttpClient()
        
        #UTM정보가 있을 경우 UTM삭제를 진행한다.
        if len(utm_info) > 0:
            for row in utm_info:
                
                utm_userid   = str(utm_info[row]["utm_userid"])
                provisionseq = int(utm_info[row]["provisionseq"])
                
                log.info("    2.%s.1 DELETE %s %s --------" % (row, utm_userid, str(provisionseq)))
  
                redata = client.send(provisionseq, 
                                     config.SyncConfig.orchestrator_server_info["info"]["url"]["delete_utm"], 
                                     "delete", 
                                     "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
                
                if redata.get("result") != "fail":
                    utmInfoDeleteResult = connect.delete(sql.deleteUtmInfo(utm_userid))
                    log.info("    2.%s.2 DB 삭제 결과 : %s" % (row, str(utmInfoDeleteResult)))
                
                log.info("    2.%s.3 DELETE : %s" % (row, json.dumps(redata)))
                log.info("    %s" % (config.SyncConfig.sub_split))
        else :
            log.info("    2. UTM 이 존재하지 않습니다.")
        ###########################################
    except Exception as ex :
        log.error("8. UTM 삭제 서비스 오류 : %s"  % (ex))
        
    finally:
        log.info("%s" % (config.SyncConfig.sub_split))
        log.info("9. DeleteUTM End Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
        log.info("   time : %s" % str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M")))
        log.info("%s" % (config.SyncConfig.line_split)) 

if __name__ == '__main__':
    
    arg_caller = "System";
    arg_orgseq = "";
    
    if len(sys.argv) == 2 :
        arg_caller = sys.argv[1]   # caller
    
    if len(sys.argv) == 3 :
        arg_orgseq = sys.argv[2]   # orgseq
    
    config_decoding()
    delete_utm_scheduled(arg_caller, arg_orgseq)
