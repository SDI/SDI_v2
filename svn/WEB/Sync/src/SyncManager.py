#-*- coding: utf-8 -*-
'''
Created on 2015-02-08
@author: 정현성
@summary: 메인 스케줄러
    testScheduled - 테스트용 스케줄
'''
import datetime
import json

import config.SyncConfig
from pgSql.SyncManagerSql import SyncManagerSql
from scheduler.AXGateData import AXGateData
from scheduler.AXGateThread import AXGateThread
from scheduler.DBAccess import DBAccess
from scheduler.HttpClient import HttpClient
from util.Logger import Logger
from util.MsgData import MsgData
from util.aescipher import AESCipher
import sys

log = Logger()

#루트 폴더 설정.(현재 테스트를 위해 프로젝트내 임시 폴더를 사용하기 위한 설정으로 추후 배포시 변경되야함.)
#os.chdir(config.SyncConfig.directory_path["path"])

def mss_linkage(connect, sql, trace_max_count, db_officeList, arg_orgseq):
    
    fail_msg    = ""   #연동오류 혹은 문제 발생시 실패에 대한 메시지를 담기 위한 변수 선언
    try:
        log.info("MSS연동 시작")
        #E2E Trace 생성
        e2e_msg_main = MsgData()
        e2e_msg_main.set_sysnc_seq(trace_max_count)
        e2e_msg_main.set_orgseq(arg_orgseq)
        e2e_msg_main.set_main_class("Sync Manager")
        e2e_msg_main.set_sub_class("CustomInfo Sync")
        e2e_msg_main.set_response_status("")
        e2e_msg_main.set_start_dttm()
        e2e_msg_main.set_message("MSS연동 시작")
        e2e_msg_main.set_indent(0)
        connect.insert(sql.insert_e2e_trace(e2e_msg_main))
        
        #연동 기준 시간 설정(나중에 연동 기준 시간으로 UPDATE되지 않은 AXGate, 고객사는 삭제시키기 위함)
        eventTime = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        
        #MSS연동을 위한 RestClient 객체생성
        client = HttpClient()
        
        #보조 E2E Trace 초기 설정
        e2e_msg_sub = MsgData()
        e2e_msg_sub.set_start_dttm()
        e2e_msg_sub.set_sysnc_seq(trace_max_count)
        e2e_msg_sub.set_orgseq(arg_orgseq)
        e2e_msg_sub.set_main_class("Sync Manager")
        e2e_msg_sub.set_sub_class("CustomInfo Sync")
        e2e_msg_sub.set_indent(1)

        # 국사 정보가 존재하지 않는다. EEROR
        detail_msg  = "MSS연동 AXGate 국사 조회 결과 : (%s)" % (len(db_officeList))
        if len(db_officeList) < 1 :
            fail_msg = "MSS연동 AXGate 국사 조회 결과 없음"
            detail_msg  = fail_msg
            e2e_msg_sub.set_orgseq(arg_orgseq)
        
        log.info(detail_msg)
            
        e2e_msg_sub.set_end_dttm()
        e2e_msg_sub.set_message(detail_msg)
        #e2e_msg_sub.set_request_parameter(data)
        e2e_msg_sub.set_response_status(fail_msg)
        e2e_msg_sub.set_response(json.dumps(db_officeList))
        e2e_msg_sub.set_message_detail(detail_msg)
        connect.insert(sql.insert_e2e_trace(e2e_msg_sub))
        
        tot_deleteCnt = 0   #해지고객 총건수
        tot_updateCnt = 0   #수정/삽입 총건수
        office_axgate_info = {}
        if fail_msg == "" : 
            #국사별 고객 정보 MSS연동
            for db_officeRow in db_officeList:
                tot_deleteCnt = 0
                tot_updateCnt = 0
                
                #파라미터 생성(국사 코드로 연동한다)
                db_officecode   = db_officeList[db_officeRow].get("officecode")
                db_orgname      = db_officeList[db_officeRow].get("orgname")
                db_orgseq       = db_officeList[db_officeRow].get("orgseq")
                
                # 입력받은 국사번호와 일치하는 것만 Loop 수행항다.
                if arg_orgseq != "" and db_orgseq != int(arg_orgseq):
                    continue 
                
                data = "office_code=" + db_officecode
                
                #보조 E2E Trace 시작 시간 설정.
                e2e_msg_sub.set_start_dttm()
    
                #MSS연동 시작
                redata = {}
                redata = client.send(data, config.SyncConfig.mss_linkage_info["url"], config.SyncConfig.mss_linkage_info["httpMethod"], "http://%s" % (config.SyncConfig.mss_linkage_info["host"]))
                
                #MSS연동 결과 분석
                if redata.get("result") != "fail":
                    
                    #MSS연동 성공 처리
                    for mss_officeRow in redata:
                        
                        for mss_companyList in redata[mss_officeRow]:
                            ax_gate_ip  = ""
                            zone_id     = ""
                            company_name= ""
                            
                            for mss_companyRow in mss_companyList:
                                if mss_companyRow.get("ax_gate_ip") != None:
                                    ax_gate_ip = mss_companyRow.get("ax_gate_ip")#.encode('utf-8')
                                    
                                if mss_companyRow.get("zone_id") != None:
                                    zone_id = mss_companyRow.get("zone_id")#.encode('utf-8')
                                    
                                if mss_companyRow.get("company_name") != None:
                                    company_name = mss_companyRow.get("company_name")#.encode('utf-8')
                            # Loop End mss_companyRow
                            
                            if ax_gate_ip == "" or len(ax_gate_ip) > 15: # AXGate IP 이상함.
                                #fail_msg = "장치 IP 이상"
                                continue
                                
                            if zone_id == "" : # zone_id 이상.
                                #fail_msg = "zone_id 이상"
                                continue
                            
                            if office_axgate_info.get(mss_officeRow) == None :
                                office_axgate_info[mss_officeRow] = {}
                            
                            if office_axgate_info[mss_officeRow].get(ax_gate_ip) == None :
                                office_axgate_info[mss_officeRow][ax_gate_ip] = {}
                            
                            if office_axgate_info[mss_officeRow][ax_gate_ip].get(zone_id) == None :
                                office_axgate_info[mss_officeRow][ax_gate_ip][zone_id] = {"company_name":company_name}
                            else :
                                temp_data = office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"]
                                office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"] = temp_data +","+ company_name
                            
                            # officecode, ax_gate_ip, zone_id, company_name("," 구분 멀티), utm_mappid 저장
                            # update     
                            updateState = connect.update(sql.updateMssOrder(
                                                                    mss_officeRow, 
                                                                    ax_gate_ip, 
                                                                    zone_id, 
                                                                    office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"],    # 두개 이상일수 있음.
                                                                    "%s_%s" % (zone_id, ax_gate_ip.replace(".", "_")), 
                                                                    eventTime))
                            # insert
                            if updateState == 0:
                                updateState = connect.insert(sql.insertMssOrder(
                                                                mss_officeRow, 
                                                                ax_gate_ip, 
                                                                zone_id, 
                                                                office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"], 
                                                                "%s_%s" % (zone_id, ax_gate_ip.replace(".", "_")), 
                                                                eventTime))
                            tot_updateCnt = tot_updateCnt + updateState
                        # Loop End mss_companyList
                    # Loop End mss_officeRow
                     
                else:
                    #MSS연동 실패 처리
                    fail_msg = "MSS 연동 실패"
                
                if fail_msg == "" :
                    deleteState = connect.delete(sql.delete_order_info(eventTime, db_officecode))
                    tot_deleteCnt = tot_deleteCnt + deleteState
                
                #보조 E2E Trace 결과 설정
                if office_axgate_info.get(db_officecode) == None:
                    fail_msg = "MSS 연동 결과 없음"
                    
                e2e_msg_sub.set_orgseq(db_orgseq)
                e2e_msg_sub.set_end_dttm()
                e2e_msg_sub.set_message("MSS연동 : %s" % (db_orgname if db_orgname != None else ""))
                #e2e_msg_sub.set_request_parameter(json.dumps(data))
                e2e_msg_sub.set_response(json.dumps(redata))
                e2e_msg_sub.set_response_status(fail_msg)
                e2e_msg_sub.set_message_detail("고객사 DB 적재 결과 (%d건), 해지 고객 삭제 결과(%d건)" % (tot_updateCnt, tot_deleteCnt))
                e2e_msg_sub.set_indent(1)
                connect.insert(sql.insert_e2e_trace(e2e_msg_sub))
                
                #보조 E2E Trace 저장 정보 초기화
                e2e_msg_sub.msg_clear()
                
                fail_msg = ""
                # MSS연동으로 오더가 추가/갱신이 되지 않은 정보를 axgate에대한 국사의 고객정보를  삭제한다.(해지 고객으로 판단.)
#                 if arg_orgseq != "" : # 국사 1개만 수행의 경우
#                     deleteState = connect.delete(sql.delete_order_info(eventTime, db_officecode))
#                     tot_deleteCnt = tot_deleteCnt + deleteState
            
            # Loop End db_officeRow
            
            # MSS연동으로 오더가 추가/갱신이 되지 않은 모든 고객정보를 삭제한다.(해지 고객으로 판단.)   - 전체 수행의 경우
            #if arg_orgseq == "" : 
            #    tot_deleteCnt = connect.delete(sql.delete_order_info(eventTime, None)) 
            #    log.info("해지 고객 삭제 결과 : %s" % (tot_deleteCnt))
        
        # End if fail_msg
        
    except Exception as ex:
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("mss_linkage", str(type(ex)), str(ex.args), str(ex)))
        fail_msg = str(ex)
    finally:
        #메인 E2E Trace 결과 설정
        log.info("MSS연동 종료")
        e2e_msg_main.set_sysnc_seq(trace_max_count)
        e2e_msg_main.set_orgseq(arg_orgseq)
        e2e_msg_main.set_main_class("Sync Manager")
        e2e_msg_main.set_sub_class("CustomInfo Sync")
        e2e_msg_main.set_response_status("")
        e2e_msg_main.set_end_dttm()
        e2e_msg_main.set_response_status(fail_msg)
        e2e_msg_main.set_message("MSS연동 종료")
        e2e_msg_main.set_message_detail(fail_msg)
        e2e_msg_main.set_indent(0)
        connect.insert(sql.insert_e2e_trace(e2e_msg_main))

    return "" if fail_msg == "" else None

def get_sync_seq(connect, sql, arg_orgseq):
    
    try:
        #syncseq 값 Select (Serial)
        dic_trace_count = connect.select(sql.select_syncmgr_trace_max())
        
        trace_seq = 0
        
        #Trace가 최초 시작일때 값이 없으므로 1로 시작할 수 있도록 예외 처리한다.
        if dic_trace_count[0].get("max_count") == None:
            trace_seq = 1
        else:
            trace_seq = dic_trace_count[0].get("max_count")

    except Exception as ex :
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("get_sync_seq", str(type(ex)), str(ex.args), str(ex)))
        trace_seq = None
        
    return trace_seq
            
def ax_gate_rule_scheduled(arg_caller, arg_orgseq):
    
    log.info(config.SyncConfig.line_split)
    log.info("SyncManager Start Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
    
    try:        
        #기준 시간 설정(파일이름 기준 시간 설정값)
        file_name_time = str(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        
        #DB커넥션 연결.
        connect = DBAccess()
        sql = SyncManagerSql()

        #SyncSeq 값을 가져온다
        trace_seq = get_sync_seq(connect, sql, arg_orgseq)              
        
        #SyncSeq 값을 가져오지 못하면 프로그램을 종료 한다
        if trace_seq == None:
            log.info("DB 접속 실패")
            return
        
        #국사 조회
        
        #국사 정보 Select(NFC에서 관리되고 있는 국사 코드를 MSS에 연동하여 해당 국사에 고객 정보를 요청한다)
        #data = "%s officecode IS NOT NULL AND pop_yn = 'Y'" % ("" if arg_orgseq == "" else "orgseq = %s" % (arg_orgseq))
        db_officeList = connect.select(sql.select_office_list(arg_orgseq))
        
#         # test 국사 Data
#         db_officeList = {0: {'orgname': '대전연구소', 'orgseq': 11, 'officecode': 'R99999'},
#                      1: {'orgname': '서초국사', 'orgseq': 4, 'officecode': 'R00439'},
#                      2: {"orgname": "아현국사", "orgseq": 1, "officecode": "R00421"},
#                      3: {"orgname": "강서국사","orgseq": 2,"officecode": "R00435"},
#                      4: {"orgname": "안산국사","orgseq": 5,"officecode": "R00580"},
#                      5: {"orgname": "구로국사","orgseq": 3,"officecode": "R00437"},
#                      6: {"orgname": "서부산국사","orgseq": 9,"officecode": "R00945"},
#                      7: {"orgname": "남천안국사","orgseq": 8,"officecode": "R02151"},
#                      8: {"orgname": "인천국사","orgseq": 7,"officecode": "R00515"},
#                      9: {"orgname": "분당분기국사(동판교)","orgseq": 10,"officecode": "R08458"},
#                     10: {"orgname": "가락국사","orgseq": 6,"officecode": "R00444"},
#                     11: {"orgname": "테스트","orgseq": 12,"officecode": "R99998"}}
        
        #MSS연동
        result = mss_linkage(connect, sql, trace_seq, db_officeList, arg_orgseq)
        if result == None: #MSS연동에 실패한 경우 종료 한다
            if arg_orgseq != "" :
                connect.insert(sql.insert_trace_history(trace_seq, int(arg_orgseq), arg_caller))
                connect.update(sql.update_trace_history(int(arg_orgseq), trace_seq))
            return
        
        #파일이름 설정.
        show_run_file_name  = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["runFull"])
        dhcp_file_name      = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["dhcp"])
        dnat_file_name      = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["dNat"])
        snat_file_name      = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["sNat"])
        firewall_file_name  = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["firewall"])
        fw_outgoing_file_name  = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["fw_outgoing"])
        fw_incoming_file_name  = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["fw_incoming"])
        fw_interzone_file_name = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["fw_interzone"])
        router_file_name    = "%s_%s" % (file_name_time, config.SyncConfig.file_name_info["router"])
        
        #AXGate 연동을 위한 AXGate정보 Select
        ax_gate_data_list = connect.select(sql.select_ax_gate_server_info(arg_orgseq))
        
        # 국사에 대한 axGate IP가 존재 하지 않으면 trace_history에 넣는다.(화면 보여지기 위함)
        isAXGateInfo = False
        for db_officeRow in db_officeList:
            db_orgseq       = db_officeList[db_officeRow].get("orgseq")
            
            isAXGateInfo = False
            for axgateRow in ax_gate_data_list:
                ax_orgseq   = ax_gate_data_list[axgateRow].get("orgseq")
                
                if db_orgseq == ax_orgseq :
                    isAXGateInfo = True
                    break
            
            if isAXGateInfo == False :  # 하나도 없는 경우임.(국사별)
                connect.insert(sql.insert_trace_history(trace_seq, db_orgseq, arg_caller))
                connect.update(sql.update_trace_history(db_orgseq, trace_seq))
            
        
        #AXGate별 스레드를 관리하기 위한 관리 객체 선언
        ax_gate_threads = []
        
        #ax_gate_data_list 에는 국사별 AXGate의 정보를 가지고있다
        for axgateNum in ax_gate_data_list:
            main_data = AXGateData()
            
            # main data정보 초기화 및 할당
            main_data.ax_gate_info["ax_gate_ip"]    = ax_gate_data_list[axgateNum]["ax_gate_ip"]
            main_data.ax_gate_info["ax_gate_name"]  = ax_gate_data_list[axgateNum]["ax_gate_name"]
            main_data.ax_gate_info["office_code"]   = ax_gate_data_list[axgateNum]["officecode"]
            main_data.ax_gate_info["org_seq"]       = ax_gate_data_list[axgateNum]["orgseq"]
            main_data.ax_gate_info["trace_seq"]     = trace_seq
            
            main_data.file_info["directory_path"]   = "%s/%s/%s/%s/%s" % (config.SyncConfig.directory_path["path"], config.SyncConfig.directory_info["logDirectory"],
                                                       config.SyncConfig.directory_info["rootDirectory"],
                                                       main_data.ax_gate_info["office_code"],
                                                       main_data.ax_gate_info["ax_gate_ip"])
            
            main_data.file_info["rule"]["name"]         = show_run_file_name
            main_data.file_info["rule"]["path"]         = "%s/%s" % (main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"])
            
            main_data.file_info["dhcp"]["name"]         = dhcp_file_name
            main_data.file_info["dnat"]["name"]         = dnat_file_name
            main_data.file_info["snat"]["name"]         = snat_file_name
            main_data.file_info["firewall"]["name"]     = firewall_file_name
            main_data.file_info["fw_outgoing"]["name"]  = fw_outgoing_file_name
            main_data.file_info["fw_incoming"]["name"]  = fw_incoming_file_name
            main_data.file_info["fw_interzone"]["name"] = fw_interzone_file_name
            main_data.file_info["router"]["name"]       = router_file_name
            
            #AXGate별 작업 스레드 할당
            ax_gate_thread = AXGateThread(main_data)
            
            #스레드 시작
            ax_gate_thread.start()
                
            #스레드 관리를 위한 스레드 객체 저장
            ax_gate_threads.append(ax_gate_thread)
                
            #AXGate별 작업 히스토리 확인을 위한 DB이력 저장(UI출력을 위한 Insert)
            connect.insert(sql.insert_trace_history(trace_seq, ax_gate_data_list[axgateNum]["orgseq"], arg_caller))
          
        #스레드 관리 - 할당된 AXGate별 작업이 끝날때까지 대기 한다
        for thread in ax_gate_threads:
            thread.join()
        
    except Exception as ex :
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("ax_gate_rule_scheduled", str(type(ex)), str(ex.args), str(ex)))
        
    finally:
        #UI에서 E2E Trace 감시 종료를 위해 end값 입력을 입력한다(작업이 종료될때 Insert되는 것이기 때문에 작업 결과와는 무관하다) 
        e2e_msg_main = MsgData()
        e2e_msg_main.set_sysnc_seq(trace_seq)
        e2e_msg_main.set_main_class("Sync Manager")
        e2e_msg_main.set_response_status("")
        e2e_msg_main.set_message("end")
        e2e_msg_main.set_indent(0)
        connect.insert(sql.insert_e2e_trace(e2e_msg_main))
        
        log.info("SyncManager End Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
        log.info(config.SyncConfig.line_split)
    
    return

def config_decoding():
    aes = AESCipher(config.SyncConfig.security_key)
    config.SyncConfig.postgresql_db_info["db_host"] = aes.decrypt(config.SyncConfig.postgresql_db_info["db_host"])
    config.SyncConfig.postgresql_db_info["id"] = aes.decrypt(config.SyncConfig.postgresql_db_info["id"])
    config.SyncConfig.postgresql_db_info["pw"] = aes.decrypt(config.SyncConfig.postgresql_db_info["pw"])
    
    config.SyncConfig.mss_linkage_info["host"] = aes.decrypt(config.SyncConfig.mss_linkage_info["host"])
    
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"])
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"])
    
    config.SyncConfig.orchestrator_server_info["info"]["ip"] = aes.decrypt(config.SyncConfig.orchestrator_server_info["info"]["ip"])
    
    config.SyncConfig.utm_password = aes.decrypt(config.SyncConfig.utm_password)
    
    
if __name__ == '__main__':
    
    arg_caller = "System";
    arg_orgseq = "";
    
    if len(sys.argv) >= 2 :
        arg_caller = sys.argv[1]   # caller
    
    if len(sys.argv) >= 3 :
        try :
            arg_orgseq = sys.argv[2]   # orgseq
            temp_int = int(arg_orgseq) 
        except Exception as ex:
            log.error("Input Parameter : caller[%s], orgseq[%s]" % (arg_caller, arg_orgseq))
            arg_orgseq = ""
        finally:
            pass
        
    #암호화된 정보 복호화
    config_decoding()
    
    ax_gate_rule_scheduled(arg_caller, arg_orgseq)
    