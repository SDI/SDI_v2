# -*- coding: utf-8 -*-
###################################################################
# author : 정현성
# Date : 2015.2.10
# Description : DB 쿼리 저장소
###################################################################
import json

class SyncManagerSql(object):
    '''
    DB 쿼리를 집합.
    '''

    def __init__(self):
        '''
        Constructor
        '''
    
    """
    @Delete 삭제 필요
    """
    #테스트 용도로 만들어진 쿼리(향후 삭제 필요)
    def select_test_delete_list(self, orgseq):
        
        if orgseq == "" and orgseq == "" :
            result = """SELECT provisionseq, utm_userid FROM tb_mg_utm """
        
        else :
            result = """SELECT M.provisionseq, M.utm_userid FROM tb_mg_utm M
                         INNER JOIN tb_user U ON M.utm_userid = U.userid
                         WHERE U.orgseq = %s""" % (orgseq)
        return result
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - MSS로부터 연동받은 AXGate의 IP정보를 가져온다.
    """
    def select_ax_gate_server_info(self, orgseq):
        
        result = """SELECT DISTINCT MO.ax_gate_ip, MO.officecode, O.orgseq, 
                    (SELECT appliance_name FROM tb_hwutm_info AS HW WHERE HW.officecode = MO.officecode AND HW.ip_addr = MO.ax_gate_ip) AS ax_gate_name 
                    FROM tb_mss_order AS MO INNER JOIN tb_org AS O ON MO.officecode = O.officecode"""
        if orgseq != "" and orgseq != None:
            result = "%s AND O.orgseq = %s" % ( result, orgseq )
        
        return result
    
    def insert_trace_history(self, trace_max_count, orgseq, caller):
        result = """INSERT INTO tb_trace_history 
                    (trace_type,
                    step_msg,
                    start_dttm,
                    traceseq,
                    orgseq,
                    response_status,
                    caller)
                    VALUES (1, 'Sync Manager', now(), %d, %d, null, '%s')
                """ % (trace_max_count, orgseq, caller)
        return result
    
    def update_trace_history(self, orgseq, trace_max_count):
        result = """
        UPDATE tb_trace_history 
         SET response_status = (CASE WHEN (SELECT COUNT(*) FROM tb_syncmgr_trace WHERE syncseq = %d AND orgseq = %d AND response_status = false) = 0 THEN 'S' ELSE 'F' END), end_dttm = now() 
        WHERE traceseq = %d AND orgseq = %d """ % (trace_max_count, orgseq, trace_max_count, orgseq) # subclass != 'Rule Sync' 
        return result
    
    def insert_parsing_date(self, ax_gate_ip, parsing_data):
        result = """INSERT INTO tb_axgate_parsing_data_his(insert_date, axgate_ip, parsing_data) VALUES (now(), '%s', '%s'::json)
                """ % (ax_gate_ip, parsing_data)
        return result
    
    def select_parsing_date(self, ax_gate_ip):
        result = """SELECT parsing_data FROM tb_axgate_parsing_data_his WHERE axgate_ip = '%s' ORDER BY insert_date DESC LIMIT 1
                """ % (ax_gate_ip)
        return result
    
    def select_syncmgr_trace_max(self):
        result = """Select nextval('tb_syncmgr_trace_syncseq_seq') AS max_count"""
        #result = """SELECT MAX(syncseq) + 1 AS max_count FROM tb_syncmgr_trace"""
        return result
    
    def selectUtmIdInfo(self, provisionseq):
        result = """SELECT res_id FROM tb_openstack_resource WHERE res_type = 'instance' AND provisionseq = %d """ % (int(provisionseq))
        return result
    
    def select_template_seq(self, templatedesc):
        result = """SELECT nstemplateseq FROM tb_nstemplate WHERE templatedesc = '%s' """ % (templatedesc)
        return result
    
    def selectUtmInfo(self, utm_userid):
        result = """SELECT provisionseq, utm_provision_id FROM tb_mg_utm WHERE utm_userid = '%s' """ % (utm_userid)
        return result
    
    def select_utm_delete_check(self, provisionseq):
        result = """SELECT provisionseq, del_dttm, procstatecode FROM tb_ne WHERE provisionseq = %d AND del_dttm IS NOT NULL AND procstatecode = '7' """ % (int(provisionseq))
        return result

    def deleteUtmInfo(self, utm_userid):
        result = """DELETE FROM tb_mg_utm WHERE utm_userid = '%s' """ % (utm_userid)
        return result
    
    def delete_order_info(self, eventTime, officecode):
        result = """DELETE FROM tb_mss_order WHERE event_date != to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss') """ % (eventTime)
        if officecode != "" and officecode != None :
            result = "%s AND officecode = '%s'" % ( result, officecode )        
#         result = """DELETE FROM tb_mss_order WHERE event_date != to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss') """ % (eventTime)
#         if ax_gate_ip_list != None :
#             result = """DELETE FROM tb_mss_order WHERE ax_gate_ip IN (%s) AND event_date != to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss') """ % ( str(ax_gate_ip_list).replace("[", "").replace("]", ""), eventTime)
        return result
    
    def select_create_utm_orgseq_info(self, ax_gate_ip):
        result = """SELECT DISTINCT O.orgseq
                    FROM tb_mss_order AS MO
                    INNER JOIN tb_org AS O ON MO.officecode = O.officecode
                    WHERE MO.ax_gate_ip = '%s' """ % (ax_gate_ip)
        return result
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 생성된 UTM 관리 정보 저장
    """
    def insert_provision_info(self, utm_userid, utm_vm_password, utm_provision_id, provisionseq, utm_mappid):
        result = """INSERT INTO tb_mg_utm 
                    (insert_date,
                    utm_userid,
                    utm_vm_password,
                    utm_provision_id,
                    provisionseq,
                    utm_mappid)
                    VALUES (now(), '%s', '%s', '%s', %d, '%s') """ % (utm_userid, utm_vm_password, utm_provision_id, int(provisionseq), utm_mappid)
        return result
    
    def selectAXGateServerInfoTest(self, utm_userid):
        result = """
        SELECT 
         MU.utm_provision_id,
         MU.utm_userid,
         MU.utm_vm_password,
         MO.companyname
        FROM tb_mg_utm AS MU
         LEFT JOIN tb_mss_order AS MO ON MO.utm_mappid = MU.utm_mappid
        WHERE 1=1
         AND MU.utm_userid = '%s' """ % (utm_userid)
         
        return result
    
    def selectAXGateZoneInfo(self, axGateIP):
        result = """SELECT zone_id, utm_vm_id FROM tb_mss_order WHERE ax_gate_ip = '%s'""" % (axGateIP)
        return result
    
    def selectMssLikageTest(self, officeCode):
        result = """SELECT * FROM tb_mss_likage_test WHERE officecode = '%s'""" % (officeCode)
        
        return result
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - Management UTM의 접속 정보를 가져온다.
    """
    def selectMvInfo(self, orgseq):
        result = """SELECT infraprops FROM tb_openstack_infra WHERE orgseq = %d """ % (int(orgseq))
        
        return result
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - SyncManager에서 관리 중인 국사 정보를 가져온다.
    """
    def select_office_list(self, orgseq):
        result = """SELECT officecode, orgseq, orgname FROM tb_org WHERE officecode IS NOT NULL AND POP_YN = 'Y'"""
        if orgseq != "" and orgseq != None:
            result = """SELECT officecode, orgseq, orgname FROM tb_org WHERE orgseq = %s AND officecode IS NOT NULL AND POP_YN = 'Y'""" % (orgseq)
            
        return result
    
    def insertMssOrder(self, officeCode, axGateIP, zoneID, company, utm_mappid, startTime):
        result = """INSERT INTO tb_mss_order(officecode, ax_gate_ip, zone_id, companyname, utm_mappid, event_date) VALUES('%s', '%s', '%s', '%s', '%s', to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'))"""  \
                    % (officeCode, axGateIP, zoneID, company, utm_mappid, startTime)
        return result
    
    def updateMssOrder(self, officeCode, axGateIP, zoneID, company, utmVmID, startTime):
        result = """UPDATE tb_mss_order SET officecode = '%s', ax_gate_ip = '%s', zone_id = '%s', companyname = '%s', event_date = to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss') WHERE utm_mappid = '%s' """  \
                    % (officeCode, axGateIP, zoneID, company, startTime, utmVmID)
        return result
    
    def updateMssOrder_axgate_info(self, utmVmID, ax_gate_vd_label, ax_gate_vd):
        result = """UPDATE tb_mss_order SET ax_gate_vd_label = %s, ax_gate_vd = %s WHERE utm_mappid = '%s' """  \
                    % ( "%s" % ("null" if ax_gate_vd_label == None or ax_gate_vd_label == "" else """'%s'""" % (ax_gate_vd_label)),
                        "%s" % ("null" if ax_gate_vd == None or ax_gate_vd == "" else """'%s'""" % (ax_gate_vd)),
                        utmVmID)
        return result
    
    
    
    def insert_e2e_trace(self, msg):
        result = """INSERT INTO tb_syncmgr_trace(syncseq, mainclass, subclass, axgate_job, thread_job, provisionseq, message, request_cmd, request_parameter, execute_time, response, response_status, reg_dttm, start_dttm, end_dttm, message_detail, rule_file_name, indent, utm_userid, parsing_type, orgseq, parsing_cnt)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) """ \
                    % (msg.sync_seq, 
                       msg.main_class,
                       msg.sub_class, 
                       msg.axgate_job, 
                       msg.thread_job, 
                       "%s" % ("null" if msg.provision_seq == None else """%s""" % (msg.provision_seq)), 
                       "%s" % ("null" if msg.message == None else """'%s'""" % (str(msg.message).replace("'", " "))), 
                       "%s" % ("null" if msg.request_cmd == None or msg.request_cmd == "" else """'%s'""" % (str(msg.request_cmd).replace("'", " "))), 
                       "%s" % ("null" if msg.request_parameter == None or msg.request_parameter == "" else """'%s'""" % (str(msg.request_parameter).replace("'", " ")[1:2000])), 
                       "%s" % ("null" if msg.execute_time == None else """%s""" % (msg.execute_time)), 
                       "%s" % ("null" if msg.response == None or msg.response == "" else """'%s'""" % (str(msg.response).replace("'", " "))), 
                       "%s" % ("true" if msg.response_status == "" or msg.response_status == "" else "false"), 
                       "now()", 
                       "%s" % ("null" if msg.start_dttm == None else """%s""" % (msg.start_dttm)), 
                       "%s" % ("null" if msg.end_dttm == None else """%s""" % (msg.end_dttm)),
                       "%s" % ("null" if msg.message_detail == None or msg.message_detail == "" else """'%s'""" % (str(msg.message_detail).replace("'", " "))), 
                       "%s" % ("null" if msg.rule_file_name == None else """'%s'""" % (msg.rule_file_name)), 
                       "%s" % ("null" if msg.indent == None else """%s""" % (msg.indent)), 
                       "%s" % ("null" if msg.utm_userid == None or msg.utm_userid == "" else """'%s'""" % (msg.utm_userid)), 
                       "%s" % ("null" if msg.parsing_type == None else """'%s'""" % (msg.parsing_type)),
                       "%s" % ("null" if msg.orgseq == None or msg.orgseq == "" else """%s""" % (msg.orgseq)),
                       "%s" % ("null" if msg.parsing_cnt == None or msg.parsing_cnt == "" else """%s""" % (msg.parsing_cnt)))
                    
        return result
    
    """
    @Created - 2015. 05. 15.
    @author - 김도영
    @description - SyncManager 파싱중 Event 저장
    """    
    def insert_syncmgr_event(self, data):
        
        result = """INSERT INTO tb_syncmgr_event(syncseq, reg_dttm, hostname, ax_gate_ip, utm_userid, case_type, module_type, process_type, message, message_detail, orgseq)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ) """\
                    % (data["sync_seq"],
                       "now()",
                       "%s" % ("null" if data["host_name"] == None or data["host_name"] == "" else """'%s'""" % (data["host_name"])),
                       "%s" % ("null" if data["ax_gate_ip"] == None or data["ax_gate_ip"] == "" else """'%s'""" % (data["ax_gate_ip"])),
                       "%s" % ("null" if data["utm_userid"] == None or data["utm_userid"] == "" else """'%s'""" % (data["utm_userid"])),
                       "%s" % ("null" if data["case_type"] == None or data["case_type"] == "" else """'%s'""" % (data["case_type"])),
                       "%s" % ("null" if data["module_type"] == None or data["module_type"] == "" else """'%s'""" % (data["module_type"])),
                       "%s" % ("null" if data["process_type"] == None or data["process_type"] == "" else """'%s'""" % (data["process_type"])),
                       "%s" % ("null" if data["message"] == None or data["message"] == "" else """'%s'""" % (str(data["message"]).replace("'", " "))),
                       "%s" % ("null" if data["message_detail"] == None or data["message_detail"] == "" else """'%s'""" % (str(data["message_detail"]).replace("'", " "))),
                       "%s" % ("null" if data["orgseq"] == None or data["orgseq"] == "" else """%s""" % (data["orgseq"])) 
                       )
        
        return result
    
    """
    @Created - 2015. 05. 23.
    @author - 김도영
    @description - axgate Info
    """
    def insert_hwutm_info(self, officecode, ip_addr, appliance_name):
        result = """INSERT INTO tb_hwutm_info(officecode, ip_addr, appliance_name, appliance_type, reg_dttm)
                    VALUES (%s, %s, %s, %s, %s) """\
                    % ("'%s'" % officecode,
                       "'%s'" % ip_addr,
                       "%s" % ("null" if appliance_name == None or appliance_name == "" else """'%s'""" % (str(appliance_name).replace("'", ""))),
                       "'HW_UTM'",
                       "now()" 
                       )
        
        return result
    
    def update_hwutm_info(self,  officecode, ip_addr, appliance_name):
        result = """UPDATE tb_hwutm_info SET appliance_name = %s, appliance_type = 'HW_UTM', reg_dttm = now() WHERE officecode = '%s' AND ip_addr = '%s' """  \
                    % ( "%s" % ("null" if appliance_name == "" or appliance_name == None else """'%s'""" % (str(appliance_name).replace("'", ""))), 
                        officecode,
                        ip_addr )
        return result
    
    
if __name__ == '__main__':
    pass