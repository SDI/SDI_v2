# -*- coding: utf-8 -*-
###################################################################
# author : 정현성
# Date : 2015.2.10
# Description : 설정 데이터
###################################################################

#Run 장비 Real Or Local 구분
isReal = True  # 운영[True], DEV[False]
logLevel = 40  # 60[Trace], 50[Debug], 40[Info], 30[Noti], 20[Warn], 10[Error], 0[Fatal], 만일 -값 이면 로그 하나도 안찍음.

sub_split  = "-------------------------------------------------------"
line_split = "======================================================="

# DB 접속 정보.
postgresql_db_info = {
#     운용 배포시(10*0*0*72), 로컬 테스트(211*224*204*160)
    "db_host": "sr2o0Zy0p6/kB92yIv9qwSaKkh3ExdNiGjpXfiPYt7U=" if isReal == True else "Bv4kIwiPFbLb3LNM4Vpfke5FSu3TkZTaRzk/1EOo7sM=",
#     "db_host": "Bv4kIwiPFbLb3LNM4Vpfke5FSu3TkZTaRzk/1EOo7sM=",    # 로컬 테스트(211*224*204*160)
#     "db_host": "sr2o0Zy0p6/kB92yIv9qwSaKkh3ExdNiGjpXfiPYt7U=",    # 운용 배포시(10*0*0*72)
    "id"     : "ARhv6JOBrsnVdkHl0RBoZ/Z7ieaXzMw2AP65BniTig0=",  #(orchestrator)
    "pw"     : "F1a143sTy5e4JJtykpk0Bvl6GYAT3g3x2jU44ZWH05o=",  #(orch!234)
    "db"     : "sdi",
    "port"   : "5432",
    "tag"    : "postgresql"
}

# 기본 폴더
directory_path = {
#     운용 배포시 ["path":"/usr/local/SyncManager/src", "os_type":"linux"], 로컬 테스트["path":"C:/Project/KT-NFV/workspace/SyncManager/src", "os_type":"windows"]
    "path"      : "/usr/local/Sync/src"  if isReal == True else "C:/Project/KT-NFV/workspace/Sync/src",
    "os_type"   : "linux"                if isReal == True else "windows"
#     "path": "C:/Project/KT-NFV/workspace/SyncManager/src" , "os_type":"windows"  #로컬 테스트
#    "path": "D:/NFV/source/SyncManager/src" , "os_type":"windows"  #로컬 테스트
#     "path": "/usr/local/SyncManager/src", "os_type":"linux"        #운용 배포시
}

# DB 접속 정보.
mss_linkage_info = {
#     운용 배포시(218*144*90*62) 로컬 테스트(211*224*204*158:8089)
#     "host"       : "zVFD2Q/b9SA16ZI5iP2fTRkT/IeGuGDsCZY3Mh1L0qE=" if isReal == True else "coJyxdB/byefX6ut4hxgSyh/IySvQivsF4aTZPTbntBf4Qx2om6p+SDhh9OnqLD6",
#     "host"       : "coJyxdB/byefX6ut4hxgSyh/IySvQivsF4aTZPTbntBf4Qx2om6p+SDhh9OnqLD6",
#     "host": "coJyxdB/byefX6ut4hxgSyh/IySvQivsF4aTZPTbntBf4Qx2om6p+SDhh9OnqLD6",     #로컬 테스트(211*224*204*158:8089)
    "host": "zVFD2Q/b9SA16ZI5iP2fTRkT/IeGuGDsCZY3Mh1L0qE=",            #운용 배포시(218*144*90*62)
    "url"        : "/api/softutm/get_customer_list?",
    "httpMethod" : "get"
}

#디렉터리 정보.
directory_info = {
    "logDirectory"      : "orchestrator",
    "rootDirectory"     : "rulecollector",
    "ruleDirectory"     : "runFull"
}

#파일 이름 정보.
file_name_info = {
    "runFull"       : "XGateShowRunFull.log",
    "dhcp"          : "dhcp_converted.log",
    "dNat"          : "dnat_converted.log",
    "sNat"          : "snat_converted.log",
    "firewall"      : "firewall_converted.log",
    "fw_outgoing"   : "outgoing_converted.log",
    "fw_incoming"   : "incoming_converted.log",
    "fw_interzone"  : "interznoe_converted.log",
    "router"        : "router_converted.log"
}

#AXGate 접속 정보
ax_gate_info = {
    "ax_gate_server_info"  : {
            "ssh_access_path"   :"%s/%s" % (directory_path["path"] ,"SSHAccess.jar"),
            "id"                :"8MYKtj88gyO+WXRfy2r6KFfPf2Act+vYzv4lRpEdNgs=",
            "password"          :"M+2n9DBmNvMMt4RH3W25yrc/uria46ggnbdn/qp6ANA=",
            "port"              :2233}    #(axroot, Admin12#$)
}

#생성되는 모든 UTM의 비밀번호이다.(주의!! 아래 비밀번호 수정시 생성되어 있는 모든 UTM의 비밀번호를 수정하거나 기존에 생성된 UTM을 삭제해야한다)
# 소스상에서 처리하는 부분 : 고객사id + 패스워드로 변경됨 (2015.05.22)
utm_password = "KZ0Vqr/Kv9VFdlSVqHxWsvS2//ZlU4/Rj0n/D1qD1cA=" #(utmVm!234)
security_key = "Qfkrkstkghkwhgdk"

#orchestrator URL
orchestrator_server_info = {
#         운용 배포시(10*0*0*71:8080), 로컬 테스트(211*224*204*173:8080)
    "info"  : {"ip": "Uf2/CM9qJsT6a8s5dsWRuyyT4pQvygkOpTqZ09mb/OI=" if isReal == True else "/R5McRFE4ejg1PqkcIx/wcYp03MFjK9myywXE7szZL+T6xKnxMDxb7EX+KlWEjVI",
#     "info"  : {"ip": "/R5McRFE4ejg1PqkcIx/wcYp03MFjK9myywXE7szZL+T6xKnxMDxb7EX+KlWEjVI",     #로컬 테스트(211*224*204*173:8080)
#     "info"  : {"ip":"Uf2/CM9qJsT6a8s5dsWRuyyT4pQvygkOpTqZ09mb/OI=",           #운용 배포시(10*0*0*71:8080)
               "url":{"create_userid"       :"/soap2/service/checkuser", 
                      "get_provisions_seq"  :"/soap2/service/provisions/ready",
                      "create_provisions"   :"/soap2/service/provisions",
                      "delete_utm"          :"/soap2/service/provisions/",
                      "security_group"      :"/soap2/openstack/changesecuritygroup"}}
}

# Case message
case_type_info = {
    "module" : {"AA":"AXGate ACCESS",
                "ZO":"ZONE", "BR":"BRIDGE", "IF":"INTERFACE",
                "SG":"SERVICE GROUP", "IG":"IP GROUP",
                "SP":"SNAT PROFILE", "DP":"DNAT PROFILE",
                "DH":"DHCP", "DN":"DNAT", "SN":"SNAT", "RO":"ROUTER",
                "FW":"FIREWALL", "FI":"FW[INCOMING]", "FO":"FW[OUTGOING]", "FZ":"FW[INTERZONE]",
                "PO":"PROVISION"},
    "case" : {"C":"명명규칙 위반", "P":"미지원 기능", "U":"UTM 생성 오류", "S":"System 오류"},
    "process" : {"S":"Stop", "C":"Continue"}
    
}

#임시? 로직으로 김복순 박사님의 요청으로 추가되었음 (대전연구소에서 사용)
#계속 유지된다면.. 보안성 검토시 암호화로 바꿔야 하는 부분... --> 제거 필요 (소스부분까지)
testing_secondary_router = "on,,10.0.0.0/24,192.168.10.1,test_routing,,,,,,,"