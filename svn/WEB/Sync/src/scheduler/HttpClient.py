#-*- coding: utf-8 -*-
import json

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders

from util.Logger import Logger

log = Logger()

class HttpClient():
    def sendData(self, url, httpMethod, reqBody, soapUrl):
        try:
            http_client = httpclient.HTTPClient()
            h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
            
            if httpMethod == "post":
                log.debug("요청 파라미터 : %s" % (reqBody))
                #print "요청 파라미터 : %s" % (reqBody)
                request = HTTPRequest(url=soapUrl+url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=600)
            elif httpMethod == "delete" :
                log.debug("요청 파라미터 : %s" % (reqBody))
                #print "요청 파라미터 : %s" % (reqBody)
                request = HTTPRequest(url=soapUrl+url+reqBody, headers=h, method=httpMethod.upper(), body=None, request_timeout=30)
            elif httpMethod == "get" :
                log.debug("요청 파라미터 : %s" % (soapUrl+url+reqBody.replace('"', '')))
                #print "요청 파라미터 : %s" % (soapUrl+url+reqBody.replace('"', ''))
                request = HTTPRequest(url=soapUrl+url+reqBody.replace('"', ''), headers=h, method=httpMethod.upper(), body=None, request_timeout=30)

            response = http_client.fetch(request=request)
            http_client.close()
            
            log.debug("응답 파라미터 : %s" % (response.body))
            
            #print "응답 파라미터 : %s" % (response.body)
            return response.body
        except httpclient.HTTPError as ex:
            log.error(str(ex))
            errResponse = {"result":"fail", "description":str(ex)}
            return errResponse
        
        except Exception as ex:
            log.error("Error: " + str(ex))
            errResponse = {"result":"fail", "description":str(ex)}
            return errResponse
    
    def send(self, data, url, httpMethod, soapUrl):
        try:
            jsonData = json.dumps(data)
            redata = self.sendData(url, httpMethod, jsonData, soapUrl)
            return json.loads(redata)
        except Exception as ex:
            log.error(str(ex))
            errResponse = {"result":"fail", "description":str(ex)}
            return errResponse

