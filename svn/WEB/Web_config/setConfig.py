# -*- coding: utf-8 -*-
'''
Created on 2015. 6. 2.

@author: SM
'''
from util.aescipher import AESCipher

aeskey = 'Qfkrkstkghkwhgdk'
aes = AESCipher(aeskey)
header = ("# -*- coding: utf-8 -*-\n\n")

class Config_set(object):
    def Web_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # Provision Manager URL info input
        ap_url = raw_input("Provision Manager URL : ")

        # Provision Manager URL info encrypt
        ap_url = aes.encrypt(ap_url)

        # HA Manager URL info input
        ha_url = raw_input("HA Manager URL : ")

        # HA Manager URL info encrypt
        ha_url = aes.encrypt(ha_url)

        # Websocket IP info input
        websocket_url = raw_input("Websocket IP : ")

        # Websocket IP info encrypt
        websocket_url = aes.encrypt(websocket_url)

        # SSH URL info input
        ssh_url = raw_input("SSH URL : ")

        # SSH URL info encrypt
        ssh_url = aes.encrypt(ssh_url)

        postgresql_db_info = ("# DB\npostgresql_db_info = {\n\t'db' : '%s',\n\t'db_host' : '%s',\n\t'port' : '%s',\n\t'id' : '%s',\n\t'pw' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        ap_url_info = ("# Provision Manager URL\nap_url_info = {\n\t'url' : '%s'\n}\n\n" % ap_url)

        ha_url_info = ("# HA Manager URL\nha_url_info = {\n\t'url' : '%s'\n}\n\n" % ha_url)

        websocket_url_info = ("# Websocket IP\nwebsocket_url_info = {\n\t'url' : '%s'\n}\n\n" % websocket_url)

        ssh_url_info = ("# SSH URL\nssh_url_info = {\n\t'url' : '%s'\n}\n\n" % ssh_url)

        key = ("# key\nkey = {\n\t'key' : '%s'\n}" % aeskey)

        f = open("/usr/local/ZeroOfficeWeb/config/monitor_config.py", 'w')
#         f = open("monitor_config.py", 'w')

        f.write(header)
        f.write(postgresql_db_info)
        f.write(ap_url_info)
        f.write(ha_url_info)
        f.write(websocket_url_info)
        f.write(ssh_url_info)
        f.write(key)

        f.close()

    def Sync_config(self):
        # log Level
        logLevel = raw_input("log Level : 60[Trace], 50[Debug], 40[Info], 30[Noti], 20[Warn], 10[Error], 0[Fatal]\n")

        logLevel_info = ("logLevel = %s\n\n" % logLevel)

        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
#         db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
#         port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # mss 연결 정보
        mss_host = raw_input("mss host : ")

        # mss 연결 정보 encrypt
        mss_host = aes.encrypt(mss_host)

        # AXGate Connect info
        ax_gate_id = raw_input("AXGate ID : ")
        ax_gate_pw = raw_input("AXGate Password : ")
        ax_gate_port = raw_input("AXGate Port : ")

        # AXGate Connect info encrypt
        ax_gate_id = aes.encrypt(ax_gate_id)
        ax_gate_pw = aes.encrypt(ax_gate_pw)

        # UTM Password
        utm_password = raw_input("UTM Password : ")

        # UTM Password encrypt
        utm_password = aes.encrypt(utm_password)

        # orchestrator URL info
        orchestrator_ip = raw_input("Orchestrator IP : ")

        # orchestrator URL info encrypt
        orchestrator_ip = aes.encrypt(orchestrator_ip)

        postgresql_db_info = ("# DB\npostgresql_db_info = {\n\t'db' : '%s',\n\t'db_host' : '%s',\n\t'port' : '%s',\n\t'id' : '%s',\n\t'pw' : '%s',\n\t'tag' : 'postgresql'\n}\n\n" % (db, db_host, port, db_id, pw))

        directory_path = ("# default folder\ndirectory_path = {\n\t'path' : '/usr/local/Sync/src',\n\t'os_type' : 'linux'\n}\n\n")

        mss_linkage_info = ('# mss linkage info\nmss_linkage_info = {\n\t"host" : "%s",\n\t"url" : "/api/softutm/get_customer_list?",\n\t"httpMethod" : "get"\n}\n\n' % mss_host)

        directory_info = ('# directory info\ndirectory_info = {\n\t"logDirectory" : "orchestrator",\n\t"rootDirectory" : "rulecollector",\n\t"ruleDirectory" : "runFull"\n}\n\n')

        file_name_info = ('# file name info\nfile_name_info = {\n\t"runFull" : "XGateShowRunFull.log",\n\t"dhcp" : "dhcp_converted.log",\n\t"dNat" : "dnat_converted.log",\n\t"sNat" : "snat_converted.log",\n\t"firewall" : "firewall_converted.log",\n\t"fw_outgoing" : "outgoing_converted.log",\n\t"fw_incoming" : "incoming_converted.log",\n\t"fw_interzone" : "interznoe_converted.log",\n\t"router" : "router_converted.log"\n}\n\n')

        ax_gate_info = ('# AXGate Connect Info\nax_gate_info = {\n\t"ax_gate_server_info" : {\n\t\t"ssh_access_path" : "/usr/local/Sync/src/SSHAccess.jar",\n\t\t"id" : "%s",\n\t\t"password" : "%s",\n\t\t"port" : %s\n\t}\n}\n\n' % (ax_gate_id, ax_gate_pw, ax_gate_port))

        utm_password_info = ('utm_password = "%s"\n' % utm_password)

        orchestrator_server_info = ('# orchestrator URL\norchestrator_server_info = {\n\t"info" : {\n\t\t"ip" : "%s",\n\t\t"url" : {\n\t\t\t"create_userid" : "/soap2/service/checkuser",\n\t\t\t"get_provisions_seq" : "/soap2/service/provisions/ready",\n\t\t\t"create_provisions" : "/soap2/service/provisions",\n\t\t\t"delete_utm" : "/soap2/service/provisions/",\n\t\t\t"security_group" : "/soap2/openstack/changesecuritygroup"\n\t\t}\n\t}\n}\n\n' % orchestrator_ip)

        case_type_info = ('# Case message\ncase_type_info = {\n\t "module" : {\n\t\t"AA" : "AXGate ACCESS",\n\t\t"ZO" : "ZONE",\n\t\t"BR" : "BRIDGE",\n\t\t"IF" : "INTERFACE",\n\t\t"SG" : "SERVICE GROUP",\n\t\t"IG" : "IP GROUP",\n\t\t"SP" : "SNAT PROFILE",\n\t\t"DP" : "DNAT PROFILE",\n\t\t"DH" : "DHCP",\n\t\t"DN" : "DNAT",\n\t\t"SN" : "SNAT",\n\t\t"RO" : "ROUTER",\n\t\t"FW" : "FIREWALL",\n\t\t"FI" : "FW[INCOMING]",\n\t\t"FO" : "FW[OUTGOING]",\n\t\t"FZ" : "FW[INTERZONE]",\n\t\t"PO" : "PROVISION"\n\t},\n\t"case" : {\n\t\t"C" : "명명규칙 위반",\n\t\t"P" : "미지원 기능",\n\t\t"U" : "UTM 생성 오류",\n\t\t"S" : "System 오류"\n\t},\n\t"process" : {\n\t\t"S" : "Stop",\n\t\t"C":"Continue"\n\t}\n}\n\n')

        key = ('security_key = "%s"\n\n' % aeskey)

        sub_split = ('sub_split  = "-------------------------------------------------------"\n')
        line_split = ('line_split = "======================================================="\n\n')

        f = open("/usr/local/Sync/src/config/SyncConfig.py", 'w')
#         f = open("SyncConfig.py", 'w')

        f.write(header)
        f.write(logLevel_info)
        f.write(sub_split)
        f.write(line_split)
        f.write(postgresql_db_info)
        f.write(directory_path)
        f.write(mss_linkage_info)
        f.write(directory_info)
        f.write(file_name_info)
        f.write(ax_gate_info)
        f.write(utm_password_info)
        f.write(key)
        f.write(orchestrator_server_info)
        f.write(case_type_info)
        f.write('testing_secondary_router = "on,,10.0.0.0/24,192.168.10.1,test_routing,,,,,,,"')

        f.close()

    def HA_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # mss info
        mss_endpoint = raw_input("mss endpoint : ")

        # mss info encrypt
        mss_endpoint = aes.encrypt(mss_endpoint)

        # sdnc info
        sdnc_endpoint = raw_input("sdnc endpoint : ")
        nonsdnc_endpoint = raw_input("non-sdnc endpoint : ")

        # sdnc info encrypt
        sdnc_endpoint = aes.encrypt(sdnc_endpoint)
        nonsdnc_endpoint = aes.encrypt(nonsdnc_endpoint)

        postgresql_db_info = ("# DB\ndb_conn_info = {\n\t'database' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user' : '%s',\n\t'password' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        mss = ("mss = {\n\t'MSS_ENDPOIT' : '%s'\n}\n\n" % mss_endpoint)

        sdnc = ("sdnc = {\n\t'SDNC_ENDPOIT':'%s',\n\t'NONSDNC_ENDPOIT':'%s'\n}" % (sdnc_endpoint, nonsdnc_endpoint))

        key = ("AESCipherKey = {\n\t'key' : '%s'\n}\n\n" % aeskey)

        f = open("/usr/local/HAManager/config/connect_config.py", 'w')
#         f = open("HAManager_connect_config.py", 'w')

        f.write(header)
        f.write(key)
        f.write(postgresql_db_info)
        f.write(mss)
        f.write(sdnc)

        f.close()

    def prov_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # SMS DB info input
        sms_db = raw_input("SMS DB Name : ")
        sms_db_host = raw_input("SMS DB Host : ")
        sms_port = raw_input("SMS DB Port : ")
        sms_db_id = raw_input("SMS DB ID : ")
        sms_pw = raw_input("SMS DB Password : ")

        # SMS DB info encrypt
        sms_db = aes.encrypt(sms_db)
        sms_db_host = aes.encrypt(sms_db_host)
        sms_port = aes.encrypt(sms_port)
        sms_db_id = aes.encrypt(sms_db_id)
        sms_pw = aes.encrypt(sms_pw)

        # Websocket info input
        websocket_url = raw_input("Websocket URL : ")

        # Websocket info encrypt
        websocket_url = aes.encrypt(websocket_url)

        postgresql_db_info = ("# DB\ndb_conn_info = {\n\t'database' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user' : '%s',\n\t'password' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        sms_db_info = ("# SMS DB\ndb_conn_info_sms = {\n\t'database' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user': '%s',\n\t'password' : '%s'\n}\n\n" %(sms_db, sms_db_host, sms_port, sms_db_id, sms_pw))

        websocket_info = ("# Websocket Info\nwebsocket_conn_info = {\n\t'url': '%s'\n}" % websocket_url)

        key = ("AESCipherKey = {\n\t'key' : '%s'\n}\n\n" % aeskey)

        f = open("/usr/local/ProvisionManager/config/connect_config.py", 'w')
#         f = open("ProvisionManager_connect_config.py", 'w')

        f.write(header)
        f.write(key)
        f.write(postgresql_db_info)
        f.write(sms_db_info)
        f.write(websocket_info)

        f.close()

    def cloud_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # UTM connect info
        utm_user = raw_input("UTM User : ")
        utm_pw = raw_input("UTM Password : ")

        # UTM connect info encrypt
        utm_user = aes.encrypt(utm_user)
        utm_pw = aes.encrypt(utm_pw)

        # Websocket info input
        websocket_url = raw_input("Websocket URL : ")

        # Websocket info encrypt
        websocket_url = aes.encrypt(websocket_url)

        # Mgmt VM Connect info
        mgmt_user = raw_input("Mgmt User : ")
        mgmt_pw = raw_input("Mgmt Password : ")

        # Mgmt VM Connect info encrypt
        mgmt_user = aes.encrypt(mgmt_user)
        mgmt_pw = aes.encrypt(mgmt_pw)

        # Org seq
        orgseq = raw_input("Orgseq : ")

        # node info input
        node_ip = raw_input("Node IP : ")
        node_port = raw_input("Node Port : ")
        node_id = raw_input("Node ID : ")
        node_pw = raw_input("Node Password : ")

        # node info encrypt
        node_ip = aes.encrypt(node_ip)
        node_id = aes.encrypt(node_id)
        node_pw = aes.encrypt(node_pw)

        postgresql_db_info = ("# DB\ndb_conn_info = {\n\t'db' : '%s',\n\t'db_host' : '%s',\n\t'port' : '%s',\n\t'id' : '%s',\n\t'pw' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        utm_conn_info = ("# UTM Connect Info\nutm_conn_info = {\n\t'user' : '%s',\n\t'password' : '%s'\n}\n\n" % (utm_user, utm_pw))

        websocket_info = ("# Websocket Info\nwebsocket_conn_info = {\n\t'url': '%s'\n}" % websocket_url)

        vm_manage_info = ("# Mgmt VM Connect Info\nvm_manage_info = {\n\t'user' : '%s',\n\t'password' : '%s'\n}" % (mgmt_user, mgmt_pw))

        anode = ("# component info\nmonitor = {\n\t'host_list': ['anode'],\n\t'anode' : {\n\t\t'ip' : '%s',\n\t\t'port' : %s,\n\t\t'id' : '%s',\n\t\t'pw' : '%s'\n\t},\n\t'anode_components' : {\n\t\t'nova' : [\n\t\t\t'nova-api',\n\t\t\t'nova-conductor',\n\t\t\t'nova-scheduler',\n\t\t\t'nova-cert',\n\t\t\t'nova-consoleauth',\n\t\t\t'nova-novncproxy'\n\t\t],\n\t\t'neutron' : [\n\t\t\t'neutron-server',\n\t\t\t'neutron-openvswitch-agent',\n\t\t\t'neutron-dhcp-agent',\n\t\t\t'neutron-l3-agent',\n\t\t\t'neutron-metadata-agent',\n\t\t\t'neutron-ns-metadata-proxy'\n\t\t],\n\t\t'cinder' : [\n\t\t\t'cinder-api',\n\t\t\t'cinder-scheduler',\n\t\t\t'cinder-volume'\n\t\t],\n\t\t'glance' : [\n\t\t\t'glance-api',\n\t\t\t'glance-registry'\n\t\t],\n\t\t'keystone' : [\n\t\t\t'keystone-all'\n\t\t],\n\t\t'cinder_utils' : [\n\t\t\t'iscsid',\n\t\t\t'tgtd'\n\t\t],\n\t\t'horizon_utils' : [\n\t\t\t'apache2',\n\t\t\t'memcached'\n\t\t],\n\t\t'message_utils' : [\n\t\t\t'epmd',\n\t\t\t'beam.smp'\n\t\t],\n\t\t'db_utils' : [\n\t\t\t'mysqld'\n\t\t],\n\t\t'neutron_utils' : [\n\t\t\t'dnsmasq',\n\t\t\t'ovsdb-client',\n\t\t\t'ovsdb-server',\n\t\t\t'ovs-vswitchd'\n\t\t]\n\t},\n}" % (node_ip, node_port, node_id, node_pw))

        key = ("AESCipherKey = {\n\t'key' : '%s'\n}\n\n" % aeskey)

        f = open("/usr/local/CloudManager/scheduler/config/connect_config.py", 'w')
#         f = open("ProvisionManager_connect_config.py", 'w')

        f.write(header)
        f.write(key)
        f.write(postgresql_db_info)
        f.write(utm_conn_info)
        f.write(websocket_info)
        f.write(vm_manage_info)

        f.close()

        f2 = open("/usr/local/CloudManager/scheduler/config/monitor_config.py", 'w')

        f2.write(header)
        f2.write('# Org Set\norg = {\n\t"orgseq" : %s\n}\n\n' % orgseq)
        f2.write("# instance mgmt_net resource Threshold\nmgmt_net_alarm = {\n\t'cpu' : 80,\n\t'memory': 80,\n\t'disk':80\n}\n\n")
        f2.write(anode)

        f2.close()

        f3 = open("/usr/local/CloudManager/service/config/connect_config.py", 'w')

        f3.write(header)
        f3.write(key)
        f3.write(postgresql_db_info)
        f3.write(utm_conn_info)
        f3.write(websocket_info)
        f3.write(vm_manage_info)

        f3.close()

    def NSSC_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # snmp info input
        snmp_port = raw_input("SNMP Port : ")
        snmp_comm = raw_input("SNMP Comm : ")
        snmp_oid = raw_input("SNMP OID : ")

        # snmp info encrypt
        snmp_port = aes.encrypt(snmp_port)
        snmp_comm = aes.encrypt(snmp_comm)
        snmp_oid = aes.encrypt(snmp_oid)

        postgresql_db_info = ("# DB\ndbCfg = {\n\t'name' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user' : '%s',\n\t'pass' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        snmpCfg = ("snmpCfg = {\n\t'port': '%s',\n\t'comm': '%s',\n\t'portOID': '%s',\n}\n\n" %(snmp_port, snmp_comm, snmp_oid))

        f = open("/usr/local/NSSC/src/config.py", 'w')
#         f = open("NSSC_config.py", 'w')

        f.write(header)
        f.write("import logging\n\n")
        f.write("apiCfg = {\n\t'port': %s,\n\t'url': dict(hism=  '/soap2/service/nssm/HISMng', swc=  '/soap2/service/nssm/SWCtl', cli=  '/soap2/service/nssm/CliMng')\n}\n\n")
        f.write(postgresql_db_info)
        f.write(snmpCfg)
        f.write("logCfg = {\n\t'name': 'nsscLogger',\n\t'dir' : '../log',\n\t'file': 'nssc.log',\n\t'fileSize': 1024*1024*2,\n\t'fileCnt': 10,\n\t'level': logging.INFO\n}")

        f.close()

    def wasscheduler_config(self):
        # DB info input
        db = raw_input("DB Name : ")
        db_host = raw_input("DB Host : ")
        port = raw_input("DB Port : ")
        db_id = raw_input("DB ID : ")
        pw = raw_input("DB Password : ")

        # DB info encrypt
        db = aes.encrypt(db)
        db_host = aes.encrypt(db_host)
        port = aes.encrypt(port)
        db_id = aes.encrypt(db_id)
        pw = aes.encrypt(pw)

        # SMS DB info input
        sms_db = raw_input("SMS DB Name : ")
        sms_db_host = raw_input("SMS DB Host : ")
        sms_port = raw_input("SMS DB Port : ")
        sms_db_id = raw_input("SMS DB ID : ")
        sms_pw = raw_input("SMS DB Password : ")

        # SMS DB info encrypt
        sms_db = aes.encrypt(sms_db)
        sms_db_host = aes.encrypt(sms_db_host)
        sms_port = aes.encrypt(sms_port)
        sms_db_id = aes.encrypt(sms_db_id)
        sms_pw = aes.encrypt(sms_pw)

        # Websocket info input
        websocket_url = raw_input("Websocket URL : ")

        # Websocket info encrypt
        websocket_url = aes.encrypt(websocket_url)

        postgresql_db_info = ("# DB\ndb_conn_info = {\n\t'database' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user' : '%s',\n\t'password' : '%s'\n}\n\n" % (db, db_host, port, db_id, pw))

        sms_db_info = ("# SMS DB\ndb_conn_info_sms = {\n\t'database' : '%s',\n\t'host' : '%s',\n\t'port' : '%s',\n\t'user': '%s',\n\t'password' : '%s'\n}\n\n" %(sms_db, sms_db_host, sms_port, sms_db_id, sms_pw))

        websocket_info = ("# Websocket Info\nwebsocket_conn_info = {\n\t'url': '%s'\n}" % websocket_url)

        key = ("AESCipherKey = {\n\t'key' : '%s'\n}\n\n" % aeskey)

        f = open("/usr/local/wasscheduler/config/connect_config.py", 'w')
#         f = open("wasscheduler_connect_config.py", 'w')

        f.write(header)
        f.write(key)
        f.write(postgresql_db_info)
        f.write(sms_db_info)
        f.write(websocket_info)

        f.close()

selection = raw_input("1) Web Config\n2) Sync-mgr Config\n3) HA-mgr Config\n4) Prov-mgr Config\n5) Cloud-mgr Config\n6) NSSC config\n7) wasscheduler\n")
config_set = Config_set()

if selection == '1':
    config_set.Web_config()
elif selection == '2':
    config_set.Sync_config()
elif selection == '3':
    config_set.HA_config()
elif selection == '4':
    config_set.prov_config()
elif selection == '6':
    config_set.NSSC_config()
elif selection == '7':
    config_set.wasscheduler_config()
else:
    print 'sorry'