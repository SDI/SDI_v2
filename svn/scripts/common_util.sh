run_commands() {
  _green=$(tput setaf 2)
  normal=$(tput sgr0)

  commands=$*

  echo -e ${_green}${commands}${normal}
  eval $commands
  echo
}

update_apt_repo() {
	sudo sed -i 's/archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list
	sudo sed -i 's/kr.archive.ubuntu.com/ftp.daum.net/g' /etc/apt/sources.list
	sudo apt-get update
}

svn_co() {
  REPO=$1
  
  echo "svn checkout svn://$SVN_SERVER/$REPO --username $SVN_USER --password $SVN_PASS"
  svn checkout svn://$SVN_SERVER/$REPO --username $SVN_USER --password $SVN_PASS
  
}

run() {
  WORK_DIR=$1
  MAIN_PY=$2

  cd $HOME/$WORK_DIR

  echo "[$WORK_DIR] RUN $MAIN_PY"
  nohup python $MAIN_PY 1> /dev/null 2>&1 &
  sleep 1
  cd -
}