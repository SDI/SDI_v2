#!/bin/bash

source "config/default.cfg"
source "scripts/common_util.sh"

checkout_web_src() {
	mkdir -p WEB
	cd WEB
	
    cmd="svn_co ZeroOfficeWeb"
    run_commands $cmd

    cmd="svn_co WebShell"
    run_commands $cmd

    cmd="svn_co Web_config"
    run_commands $cmd

    cmd="svn_co Sync"
    run_commands $cmd
    
    cd -
}

checkout_was_src() {
	mkdir -p WAS
	cd WAS
	
	cmd="svn_co install_nfv/ProvisionManager"
	run_commands $cmd
	
	cmd="svn_co install_nfv/HAManager"
	run_commands $cmd
	
	cmd="svn_co install_nfv/NSSC"
	run_commands $cmd
	
	cmd="svn_co install_nfv/wasscheduler"
	run_commands $cmd
	
	cd -
}

checkout_web_src
checkout_was_src