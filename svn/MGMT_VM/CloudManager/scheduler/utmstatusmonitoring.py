#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: umisue
@summary: 수집모듈.
        openstack(mn_node, cnode01, cnode02) 관련 상태정보 수집
        switch(aggr, tor) 관련 상태정보 수집
        openstack instance 상태정보 수집

Modified on 2015. 01. 28 모니터링 모듈 추가
                         tb_openstack_state_change 변경내용 저장
Modified on 2015. 03. 03 모니터링 모듈 추가
                         tb_customer_servier_state_info 수집 대상 추가                         
'''
import collections, datetime, json, os, sys 

from apscheduler.schedulers.blocking import BlockingScheduler
import paramiko
import psycopg2
import ssl
from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders
from websocket import create_connection

from config.connect_config import db_conn_info, websocket_conn_info, vm_manage_info, AESCipherKey
from helper.logHelper import myLogger
from sql.monitorcollectorsql import OpenstackHostStateInfoSql
from sql.provisioningsql import Provisioningsql
from util.aescipher import AESCipher
from util.openstackclient import NovaClient
from config.monitor_config import org

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='utm_status_monitoring', logdir='./log', loglevel='debug', logConsole=True).get_instance()

orgseq = org["orgseq"]

class OpenstackInstanceMonitor(object):
    global dic_instance
    dic_instance = []
    d_instance = collections.OrderedDict()    
    """
    openstack api를 사용하여 해당노드의 인스턴스 정보 모니터링
    """
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.monitorsql = OpenstackHostStateInfoSql()


    def __del__(self):
        pass

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        sql = Provisioningsql()
        conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
        conn.autocommit = True
        cur = conn.cursor()
        query = sql.getopenstackinfra(orgseq)
        cur.execute(query)
        rows = cur.fetchone()
        self.tenathost = aes.decrypt(rows[0]["openstack_connect"]["host"])    
        self.tenantname = aes.decrypt(rows[0]["openstack_connect"]["tenantid"])
        self.tenantusername = aes.decrypt(rows[0]["openstack_connect"]["userid"])
        self.tenantpassword = aes.decrypt(rows[0]["openstack_connect"]["password"])
        self.tenantauthurl = aes.decrypt(rows[0]["openstack_connect"]["token_url"])
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            tokeninfo = self.getOpenstackToken()
#             log.debug("listServersDetail start")
            serverinfo = self.listServersDetail(tokeninfo['novaurl'], tokeninfo['tenantid'], tokeninfo['tokenid'])           
            servers = serverinfo['servers']

#             log.debug("listServersDetail servers %s"%json.dumps(servers))
#             log.debug("listServersDetail end")
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()
            server_count = 0
            for server in servers:
                status = server['status']
                vmname = server['name']
                vmid = server['id']
                hostname = server['OS-EXT-SRV-ATTR:host']
                created = server['created'].replace('T', ' ').replace('Z', '')        
                create_date = datetime.datetime.strptime(created, "%Y-%m-%d %H:%M:%S")
                created = str(create_date + datetime.timedelta(hours = 9))

                conntracklist = ""
                selectquery = self.monitorsql.selectNeUertypeInfo(vmid)
                cur.execute(selectquery)
                rows = cur.fetchone()
                usertype = str(rows[0])
                log.debug(" ")
                log.debug("#"*100)
                log.debug("vmname %s usertype %s"%(vmname, usertype))
                if usertype == "0":
                    prdname = ""
                    completeyn = "N"
                else:
                    prdnamequery = self.monitorsql.selectProductname(vmid)
                    cur.execute(prdnamequery)
                    rows = cur.fetchone()
                    prdname = str(rows[0])
                    completeyn = "Y"
                
#                 log.debug("prdname %s"%prdname)
                
                if(status == "ACTIVE"):                   
                    if usertype == "0":
                        alarm_type = "0"
                        resource = ["0","0","0","0"]
                    else:
                        try:
                            log.debug("getServerResource")
                            resource = self.getServerResource(vmid)    
#                             resource = ["0","0","0","0"]  
                        except Exception,e:
                            log.error("getServerResource error %s"%e.message)
                            continue          
                                          
                        if usertype == "1" and resource == []:
                            alarm_type = "1"
                            resource = ["0","0","0","0"]
                        elif usertype == "1" and resource[3] == "0":
                            alarm_type = "1"
                        elif usertype == "1" and resource[3] <> "0":
                            alarm_type = "0"
                            try :
                                log.debug("getServerConntrack")
                                conntracklist = self.getServerConntrack(vmid)
                                log.debug("getServerConntrack end")
#                                 log.debug("conntracklist %s"%conntracklist)
                            except Exception,e:
                                log.error("getServerConntrack error %s"%e.message)
                                continue                                
                else:
                    alarm_type = "1"
                    resource = ["0","0","0","0"]
                log.debug("usertype %s"%usertype)
                    
                log.debug("#"*100)
                myquery = self.monitorsql.insertOpenstackInstanceStateInfo(self.invokeTime, orgseq, vmid, status, vmname, hostname, resource, usertype, prdname, conntracklist, completeyn, created)
                cur.execute(myquery)
                log.debug("instacnce db insert")
                log.debug("instacnce completeyn %s"%completeyn)
                
                if alarm_type == "1" and status <> "BUILD":
                    selectquery = self.monitorsql.selectsmsdata(orgseq, hostname, 'UTM', vmname, status)
                    cur.execute(selectquery)
                    rows = cur.fetchone()
                    smscount = rows[0]
                    log.debug("smscount %s"%smscount)
                    if smscount == 0:
                        myquery = self.monitorsql.insertsmsdata(self.invokeTime, orgseq, hostname, 'UTM', vmname, status)
                        cur.execute(myquery)
                
                result_instance, checkData = self.checkInstanceStatus(self.invokeTime, orgseq, vmid, status, vmname, hostname, resource, prdname, completeyn, created)
                log.debug("[%s] hostname %s vmname %s status %s resource %s" %(result_instance, hostname, vmname, status, resource))
                if ((result_instance <> "EXIST") or (alarm_type == "1"))  :
                    if result_instance == "CHANGE":
                        data_type = "C"
                    elif result_instance == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 3, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":usertype, "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":resource, "create_time":self.invokeTime, "orgseq": orgseq, "infotype":3, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)
                server_count = server_count + 1
            data_len = len(dic_instance)
            for index in range(0, data_len) :
#                 log.debug("#"*100)
#                 log.debug("Deleted VMID check %s"%dic_instance[data_len-(index + 1)])
                if dic_instance[data_len-(index + 1)]["check"] == "F":
#                     print len(dic_instance[data_len-(index + 1)])
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":3, "collect_data":dic_instance[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_instance.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 3, "D", "N", json.dumps(dic_instance[data_len-(index + 1)]))
#                     cur.execute(deletedequery)
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetInstance()
            conn.commit()
        except Exception, e:
            log.error("OpenstackInstanceMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()

    def listServersDetail(self, url, tenantid, tokenid):
        url = url + '/servers/detail?all_tenants=1'
        httpHeaders = {"Content-Type":"application/json;charset=UTF-8", "accept":"application/json", "X-Auth-Token":tokenid}
        response = json.loads(self.sendData(url, 'get', httpHeaders, None))
        return response

    def getOpenstackToken(self):
        """
        openstack에서 토큰아이디와 tenantid, nova접근url을 dict으로 리턴
        """
#         url = 'http://'+self.anode['ip'] + ':5000/v2.0/tokens'
        url = self.tenantauthurl
        httpHeaders = {"content-type":"application/json;charset=UTF-8", "accept":"application/json"}
        reqBody = {'auth':{'tenantName':self.tenantname, 'passwordCredentials':{'username':self.tenantusername,'password':self.tenantpassword}}}
        response = json.loads(self.sendData(url, 'post', httpHeaders, json.dumps(reqBody)))
        result = {}
        result['tokenid'] = response['access']['token']['id']
        result['tenantid'] = response['access']['token']['tenant']['id']
        result['novaurl'] = response['access']['serviceCatalog'][0]['endpoints'][0]['publicURL'].replace("controller",self.tenathost)
        return result

    def sendData(self, url, httpMethod, httpHeaders, reqBody):
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders(httpHeaders)
        if httpMethod == "post":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "delete" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(),  body=None)
        elif httpMethod == "get" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(),  body=None)
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            return response.body
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result

    def checkInstanceStatus(self, create_time, orgseq, vmid, status, vmname, host, resource, prdname, completeyn, created):
        count = 0
        for row in dic_instance:
            if row["orgseq"] == orgseq and row["vmid"] == vmid:      
                row["check"] = "T"          
                row["cpu"] = resource[0]
                row["memory"] = resource[1]
                row["disk"] = resource[2]
                if row["status"] == status and row["completeyn"] == completeyn and row["conntrack"] == resource[3]:
                    return "EXIST", json.dumps(dic_instance[count])
                elif row["status"] == status and row["completeyn"] <> completeyn:
                    if row["completeyn"] == "N" and completeyn == "Y":
                        row["completeyn"] = completeyn
                        return "FIRST", json.dumps(dic_instance[count])
                else:
                    row["status"] = status
                    row["conntrack"] = resource[3]
                    return "CHANGE",json.dumps(dic_instance[count])
            count = count + 1
        d_instance = {"completeyn": completeyn, "create_time": create_time,"orgseq":orgseq,"vmid":vmid, "status":status, "vmname": vmname, "host": host, "prdname":prdname, "cpu":resource[0], "memory":resource[1], "disk":resource[2], "conntrack":resource[3],  "check":"T", "createvm":created}
        dic_instance.append(d_instance)
        return "FIRST",json.dumps(d_instance)
#
    def checkResetInstance(self):
        for row in dic_instance:
            row["check"] = "F"

    def sendBroadcasting(self, sendData):
        try:
#             print "#"*100
#             print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("Instance send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)
            
    def getServerResource(self, vmid):     
        try: 
            nova = NovaClient()
            nova.authenticate(self.tenathost, self.tenantname, self.tenantusername, self.tenantpassword, self.tenantauthurl)
            server = nova.getServer(vmid)

            if "result" in server and server["result"] == "fail":
                return ["0","0","0","0"]
            else:
                if not server["server"]["status"] == "ACTIVE": 
                    log.debug("not ACTIVE")
                    return ["0","0","0","0"]
                                    
            for row in server['server']['addresses']['global_mgmt_net']:
                if row['OS-EXT-IPS:type'] == "fixed":
                    addr = row['addr']
            cmd_cpu = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'"
            cmd_mem = "free -m |grep -i Mem: |awk '{print ($3/$2)*100}'"
            cmd_dis = "df -m /home |grep -v Use |awk '{print $5}'|tr -d \"%\""
            cmd_conntrack = "conntrack -L | wc -l "
            log.debug("getServerResource-sendCommandlist start")
            resource = self.sendCommandlist(addr, cmd_cpu, cmd_mem, cmd_dis, cmd_conntrack)
            log.debug("getServerResource-sendCommandlist end %s %s %s %s"%(cmd_cpu, cmd_mem, cmd_dis, cmd_conntrack))
        except Exception, e:
            log.error("getServerResource error %s" %e.message)
            resource = ["0","0","0","0"]
        return resource

    def getServerConntrack(self, vmid):      
        try:
            nova = NovaClient()
            nova.authenticate(self.tenathost, self.tenantname, self.tenantusername, self.tenantpassword, self.tenantauthurl)
            server = nova.getServer(vmid)

            if "result" in server and server["result"] == "fail":
                return ["0","0","0","0"]
            else:
                if not server["server"]["status"] == "ACTIVE": 
                    log.debug("not ACTIVE")
                    return ["0","0","0","0"]
                            
            for row in server['server']['addresses']['global_mgmt_net']:
                if row['OS-EXT-IPS:type'] == "fixed":
                    addr = row['addr']
    #         cmd_conntrackcnt = "conntrack -L | wc -l "
            cmd_conntracklist = "conntrack -L "
    #         resource = []
            log.debug("getServerConntrack-sendCommand start %s", cmd_conntracklist)
            resource = self.sendCommand(addr, cmd_conntracklist)
            log.debug("getServerConntrack-sendCommand end %s", len(resource))
            if resource <> "" and len(resource) > 5000:
                log.debug("length %s"%len(resource))
                resource = resource[1:4999]
        except Exception, e:
            log.debug("getServerConntrack-sendCommand error %s", len(resource))
            log.error("getServerConntrack error %s" %e.message)
            
        return resource

    def sendCommand(self, ipAddress, cmd):
        client = paramiko.SSHClient()
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(vm_manage_info['user'])
            password = aes.decrypt(vm_manage_info['password'])
            log.debug("connect start")
            client.connect(hostname=ipAddress, username=user, password=password, timeout=3)
            log.debug("connect success")
            stdin, stdout, stderr = client.exec_command(cmd)
            result = ""
            for index, line in enumerate(stdout):
                result = result + line.replace('\n','')     
                if index > 50:
                    break
#             print "***"*50
#             print "result %s"%result                
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(ipAddress,str(e)))
            sys.exit()
        finally:
            client.close()
            return result
            
    def sendCommandlist(self, ipAddress, cmd_cpu, cmd_mem, cmd_dis, cmd_conntrack):
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(vm_manage_info['user'])
            password = aes.decrypt(vm_manage_info['password'])
            log.debug("connect start")
            client.connect(hostname=ipAddress, username=user, password=password, timeout=3)
            log.debug("connect success")
            cmd_list = [cmd_cpu,cmd_mem,cmd_dis,cmd_conntrack]
            req = ';'.join(cmd_list)
            log.debug("exec_command start")
            stdin, stdout, stderr = client.exec_command(req)
            for line in stdout:
                result.append(line.replace('\n',''))     
            log.debug("exec_command end")  
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(ipAddress,str(e)))
            sys.exit()
        finally:
            client.close()
            return result

    def initdicInstance(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.monitorsql.selectOpenstackInstanceStateInfoExt(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_instance = dict(zip(columns, row))
                dic_instance.append(d_instance)
            log.info("initdicInstance %s" %len(dic_instance))
        except psycopg2.DatabaseError, e:
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
          
sched = BlockingScheduler()

aes = AESCipher(AESCipherKey['key'])

dbdatabase = aes.decrypt(db_conn_info['db'])
dbuser = aes.decrypt(db_conn_info['id'])
dbpassword = aes.decrypt(db_conn_info['pw'])
dbhost = aes.decrypt(db_conn_info['db_host'])
dbport = aes.decrypt(db_conn_info['port'])

sql = Provisioningsql()
conn = psycopg2.connect(database=dbdatabase, user=dbuser, password=dbpassword, host=dbhost, port=dbport)
conn.autocommit = True
cur = conn.cursor()
query = sql.getopenstackinfra(orgseq)
cur.execute(query)
rows = cur.fetchone()

if "utmstatusmonitoring" in rows[0]["process_period"]:
    period = rows[0]["process_period"]["utmstatusmonitoring"]
else:
    period = "10"
    
@sched.scheduled_job('cron', second= period)
def openstack_instance_monitor_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("openstack_instance_monitor_job start. %s period[%s]" %(starttime, period))
    log.debug("dicInstanceSize[%d]"%len(dic_instance))
    log.debug("-"*50)
    OpenstackInstanceMonitor().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicInstanceSize[%d]"%len(dic_instance))
    log.debug("openstack_instance_monitor_job end. %s" %endtime)
    log.debug("-"*50)
 
            
def main():
    log.info("Scheduler Start")
    OpenstackInstanceMonitor().initdicInstance()
   
    sched.start()

if __name__ == '__main__':
    main()
    
    
