#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 1. 28.
@summary:
@author: 진수
'''
import json
# from string import strip
# from time import sleep

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders

# import config.openstack_config  # config 정보파일을 import
from helper.logHelper import myLogger


log = myLogger(tag='openstackclient', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class BaseClient:
    """
    openstack rest 호출을 수행하는 class의 baseclass.
    token, endpoint 처리 수행
    """
#     def __init__(self):
#         self.init_config_from_dict_config_file()
#
#     def init_config_from_dict_config_file(self):
#         self.TOKEN_URL = config.openstack_config.seocho_openstack['connect']['token_url']
#         self.BASIC_NETWORKS = config.openstack_config.seocho_openstack['networks']
#         self.UTM = config.openstack_config.seocho_openstack['utm']

    def sendData(self, url, httpMethod, reqBody=None, tokenid=None):
        """
        @summary: openstack rest api 호출 수행
        @param url: openstack rest api URL
        @param httpMethod: HTTPMETHOD(get/post/put/delete)
        @param reqBody: post요청 시 , requestbody(json)
        @param tokenid: openstack api를 호출하기 위한 인증 토큰 아이디
        @rtype: dictionary
        """
        http_client = httpclient.HTTPClient()
        dict_h = {"content-type":"application/json;charset=UTF-8", "accept":"application/json"}
        if tokenid is not None:
            dict_h['X-Auth-Token'] =  tokenid

        print "request header --> %s" %dict_h
        h = HTTPHeaders(dict_h)
        if httpMethod == "post":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "delete" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=None)
        elif httpMethod == "get":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "put":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody)


        try:
            response = http_client.fetch(request=request)
            http_client.close()
#             print type(response.body)
#             print response.body
#             return response.body
            if httpMethod == "get" :
                return json.loads(response.body)
            elif httpMethod == "post" :
                return json.loads(response.body)
            else:
                return []
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse
        finally:
            if http_client:
                http_client.close()

    def set_openstack_config(self, authinfo):
        """
        openstack에 access한 후, token, endpoint 정보를 저장한다.
        keyston url만 adminURL로 저장한다.(publicURL과 주소 다르다. keystone에 명령은 대부분 admin 권한만이 수행가능)
        """
#         authinfo = json.loads(auth_result)
        print authinfo
        self.TOKEN_ID = authinfo['access']['token']['id']
        for serviceCatalog in authinfo['access']['serviceCatalog']:
            if serviceCatalog['type'] == 'compute':
#                 self.COMPUTE_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", "221.151.188.15" )
                self.COMPUTE_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", self.host )
                print "COMPUTE_URL --> %s" %self.COMPUTE_URL
            elif serviceCatalog['type'] == 'network':
#                 self.NETWORK_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", "221.151.188.15" )
                self.NETWORK_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", self.host  )
                print "NETWORK_URL --> %s" %self.NETWORK_URL
            elif serviceCatalog['type'] == 'identity':
#                 self.IDENTITY_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", "221.151.188.15" )
#                 self.IDENTITY_URL = serviceCatalog['endpoints'][0]['adminURL'].replace("controller", "221.151.188.15" )
                self.IDENTITY_URL = serviceCatalog['endpoints'][0]['adminURL'].replace("controller", self.host  )
                print "IDENTITY_URL --> %s" %self.IDENTITY_URL
            else:
                pass

    def authenticate(self, host, tenantname, username, password, authurl):
        self.tenantname = tenantname
        self.username = username
        self.password = password
        self.authurl = authurl
        self.host = host

        request = {"auth":{"tenantName":tenantname,"passwordCredentials":{"username":username,"password":password}}}
        self.set_openstack_config(self.sendData(authurl, 'post', json.dumps(request)))


class NeutronClient(BaseClient):
    def listNetworks(self):
        url = self.NETWORK_URL + '/v2.0/networks'
        print "request url --> %s" %url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getNetwork(self, networkid):
        url = self.NETWORK_URL + '/v2.0/networks/' + networkid
        print "request url --> %s" %url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createNetwork(self, tenantid, networkname, networkType, physicalNetwork, segmentationId, adminStateUp, routerExternal=False):
        """
        network를 생성 수행.
        - 이 함수는 "Networking API v2.0 extensions"을 사용하기 때문에, admin 권한의 계정만이 수행할 수 있음.
        """
        url = self.NETWORK_URL + '/v2.0/networks'
        print url
        log.debug('request url --> %s' %url)
        requestbody = {"network":{"name":networkname,"tenant_id":tenantid, "provider:network_type":networkType,
                                  "provider:physical_network":physicalNetwork,"provider:segmentation_id":segmentationId,
                                  "admin_state_up":adminStateUp, "router:external":routerExternal
                                  }
                       }
        print "request url --> %s" %url
        print "request body --> %s" %requestbody
        result = self.sendData(url, 'post', json.dumps(requestbody), tokenid=self.TOKEN_ID)
        return requestbody, result

    def deleteNetwork(self, networkid):
        url = self.NETWORK_URL + '/v2.0/networks/' + networkid
        print "request url --> %s" %url
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createSubnet(self, tenantid, subnetname, networkid, ipversion, cidr, enabledhcp=False, gatewayip=None, allocationpools=None, dnsservers=None, hostroutes=None):
        """
        Created on 2015. 3. 11.
        @author: 진수
        @summary:
        @param tenantid: 네트워크 소유자 구분아이디
        @param subnetname: 서브네트워크 이름
        @param networkid: 달라붙여질 네트워크의 아이디(UUID)
        @param ipversion: IP version(4 또는 6)
        @param cidr: CIDR
        @param enabledhcp: True인 경우 DHCP enabled, False인 경우 DHCP disabled
        @param gatewayip: gateway IP address
        @param allocationpools: The start and end addresses for the allocation pools.
                                [
                                    {
                                        "start": "10.0.0.2",
                                        "end": "10.0.0.254"
                                    }
                                ]
        @param dnsservers: subnet을 위한 dns name 서버아이피목록. space로 구분됨. 예) [8.8.8.7 8.8.8.8].
        @param hostroutes: A list of host route dictionaries for the subnet. 예)
                            [{destination: 0.0.0.0/0, nexthop: 123.456.78.9}, {destination: 192.168.0.0/24, nexthop: 192.168.0.1}]
        @rtype: dictionary. 예)
                                {
                                    "subnet": {
                                        "name": "",
                                        "enable_dhcp": true,
                                        "network_id": "d32019d3-bc6e-4319-9c1d-6722fc136a22",
                                        "tenant_id": "4fd44f30292945e481c7b8a0c8908869",
                                        "dns_nameservers": [],
                                        "allocation_pools": [
                                            {
                                                "start": "192.168.199.2",
                                                "end": "192.168.199.254"
                                            }
                                        ],
                                        "host_routes": [],
                                        "ip_version": 4,
                                        "gateway_ip": "192.168.199.1",
                                        "cidr": "192.168.199.0/24",
                                        "id": "3b80198d-4f7b-4f77-9ef5-774d54e17126"
                                    }
                                }
        """
        url = self.NETWORK_URL + '/v2.0/subnets'
        requestbody = {"subnet":{
                                 "tenant_id":tenantid,
                                 "name":subnetname,"network_id":networkid,
                                 "ip_version":ipversion, "cidr":cidr,
                                 "enable_dhcp":enabledhcp
                                 }
                       }
        if allocationpools is not None:
            requestbody['subnet']['allocation_pools'] = allocationpools

        print "request url --> %s" %url
        print "request body --> %s" %json.dumps(requestbody)
        result = self.sendData(url, 'post', json.dumps(requestbody), tokenid=self.TOKEN_ID)
        return requestbody, result


    def listSubnets(self):
        url = self.NETWORK_URL + '/v2.0/subnets'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def listSubnetsByNetworkID(self, networkid):
        url = self.NETWORK_URL + '/v2.0/subnets?network_id=' + networkid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getSubnet(self, subnetid):
        url = self.NETWORK_URL + '/v2.0/subnets/' + subnetid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def deleteSubnet(self, subnetid):
        url = self.NETWORK_URL + '/v2.0/subnets/' + subnetid
        print "request url --> %s" %url
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def listPorts(self):
        url = self.NETWORK_URL + '/v2.0/ports'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getPort(self, portid):
        url = self.NETWORK_URL + '/v2.0/ports/' + portid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def listPortsByNetworkID(self, networkid):
        url = self.NETWORK_URL + '/v2.0/ports?network_id=' + networkid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)


class KeystoneClient(BaseClient):


    def listTenants(self):
        url = self.IDENTITY_URL + '/tenants'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getTenantByName(self, tenantname):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary:
        @param tenantname: 테넌트명
        @rtype: dict
        @return: {"tenant": {"enabled": true, "description": "", "name": "daemon", "id": "7b60dfe6d426412a86c4202fccdabe2a"}}
        """
        url = self.IDENTITY_URL + '/tenants?name=' + tenantname
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createTenant(self, tenantname):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: 테넌트(프로젝트) 생성
        @param tenantname:
        @rtype: dict
        @return: {"tenant": {"enabled": true, "description": "", "name": "daemon", "id": "7b60dfe6d426412a86c4202fccdabe2a"}}
        """
        url = self.IDENTITY_URL + '/tenants'
        reqbody = {'tenant':{'name':tenantname, 'enabled':True}}
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteTenant(self, tenantid):
        url = self.IDENTITY_URL + '/tenants/' + tenantid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createUser(self, tenantuuid, username, password):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: user 생성
        @param tenantuuid: 테넌트 uuid
        @param username: 사용자명
        @param password: 비밀번호
        @rtype: dict
        @return: {"user": {"username": "daemon", "tenantId": "7b60dfe6d426412a86c4202fccdabe2a", "enabled": true, "name": "daemon", "id": "4707a2c250e345c9bae10a48ff989931"}}
        """
        url = self.IDENTITY_URL + '/users'
#         print url
        reqbody = {'user':{'tenantId':tenantuuid, 'name':username, 'enabled':True, 'OS-KSADM:password':password}}
#         print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteUser(self, userid):
        url = self.IDENTITY_URL + '/users/' + userid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def addRoleToUser(self, tenantuuid, useruuid, roleuuid):
        url = self.IDENTITY_URL + '/tenants/' + tenantuuid + '/users/' + useruuid + '/roles/OS-KSADM/' + roleuuid
        print url
        return self.sendData(url, 'put', '', tokenid=self.TOKEN_ID)

    def listRoles(self):
        url = self.IDENTITY_URL + '/OS-KSADM/roles'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getRoleByRoleName(self, rolename):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: role 정보 가져오기
        @param rolename: 롤이름
        @rtype: dict
        @return: {"id": "1456a09e26c943f7ac653be26542c4cf", "name": "member"}
        """
        url = self.IDENTITY_URL + '/OS-KSADM/roles'
        roles = self.sendData(url, 'get', tokenid=self.TOKEN_ID)
#         print type(roles)
#         print roles
        result = {}
        for role in roles['roles']:
#             print type(role)
#             print role
            if role['name'] == rolename:
                result = role

        return result

    def getUserByName(self, username):
        url = self.IDENTITY_URL + '/users?name=' + username
        print url
        result = self.sendData(url, 'get', tokenid=self.TOKEN_ID)
        return result



class NovaClient(BaseClient):
    def getKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs/' + keypairName
        print url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs'
        reqbody = {'keypair':{'name':keypairName}}
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs/' + keypairName
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def listSecurityGroups(self):
        url = self.NETWORK_URL + '/v2.0/security-groups'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def isSecurityGroupByName(self, sgname):
        securityGroups = self.listSecurityGroups()
        result = False
        for sg in securityGroups['security_groups']:
            print json.dumps(sg)
            if sg['name'] == sgname:
                result = True
                break
        return result

    def createSecurityGroup(self, sgname, sgdesc):
        url = self.NETWORK_URL + '/v2.0/security-groups'
        print url
        reqbody = {'security_group':{'name':sgname, 'description':sgdesc}}
        print "reqbody --> %s" %json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def createSecurityGroupRule(self, direction, portRangeMin, portRangeMax, ethertype, protocol, sgid):
        url = self.NETWORK_URL + '/v2.0/security-group-rules'
        print url
        reqbody = {'security_group_rule':{
                                          'direction':direction, 'ethertype':ethertype,
                                          'port_range_min':portRangeMin, 'port_range_max':portRangeMax,
                                          'protocol':protocol,
                                          'security_group_id':sgid
                                          }
                   }
        print "reqbody --> %s" %json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def createSecurityGroupRulesForUTM(self, sgid, rules):
        url = self.NETWORK_URL + '/v2.0/security-group-rules'

        for myrule in rules:
            myrule['security_group_rule']['security_group_id'] = sgid
            self.sendData(url, 'post', json.dumps(myrule), tokenid=self.TOKEN_ID)



    def listServers(self):
        url = self.COMPUTE_URL + '/servers'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getServer(self, instanceid):
        url = self.COMPUTE_URL + '/servers/' + instanceid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createServer(self, servername, keyname, securityGroups, networks, imageRef, flavorRef, availabilityZone):
        url = self.COMPUTE_URL + '/servers'
        server = {}
        server['name'] = servername
        server['key_name'] = keyname
        server['availability_zone'] = availabilityZone
        server["security_groups"] = securityGroups
        server["networks"] = networks
        server["imageRef"] = imageRef
        server["flavorRef"] = flavorRef
        reqbody = {}
        reqbody["server"] = server
        print reqbody
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteServer(self, serverid):
        url = self.COMPUTE_URL + '/servers/' + serverid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createUTM(self, provisionseq, utm, utmname, basic_networks, networks, availabilityZone, security_group_rules):
        '''
        UTM instance 생성 수행.
        주의사항 - networks list 파라미터에 들어가는 network는 순서가 틀리면 접속이 안됨(global mgmt -> red 를 꼭 먼저 넣어야 함)

        '''
        url = self.COMPUTE_URL + '/servers'
        print "createUTM -> %s"%url

        #keypair가 있는지 검사하고 없다면 생성
        keypair = self.getKeypair(self.username + '-keypair')
        if keypair.has_key('keypair') == True:
            print "keypair --> %s" %json.dumps(keypair)
        else :
            keypair = self.createKeypair(self.username + '-keypair')

        print "key_name : %s" %keypair['keypair']['name']

#         print "test %s"%json.dumps(utm)

        securitygroupname = "EndianAccessGroup" + "_" + str(provisionseq)
        #SecurityGroup이 있는지 검사하고 없다면 생성
        if self.isSecurityGroupByName(securitygroupname) == False:
            mysg = self.createSecurityGroup(securitygroupname, 'Permits SSH and UTM')           
            self.createSecurityGroupRulesForUTM(mysg['security_group']['id'], security_group_rules)
        else:
            del utm[0]['defaultconf']['security_groups'][0]['rules']

        server = utm[0]['defaultconf']
        server['name'] = utmname
        server['key_name'] = keypair['keypair']['name']
        server['availability_zone'] = availabilityZone
#         server["networks"] = networks + self.BASIC_NETWORKS
        server["networks"] = basic_networks + networks
        reqbody = {}
        reqbody["server"] = server

        print json.dumps(reqbody)
        result = self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)
        return reqbody, result

    def excuteServer(self, instanceid, action):
        """
        Created on 2015. 2. 14.
        @author: 진수
        @summary: 서버의 중지/재시작
        @param instanceid: 생성된 vm의 uuid
        @param action: vm의 제어명령문자열("pause"/"unpause", "suspend"/"resume")
        @rtype: None
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {action:None}
        print reqbody
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def rebootServer(self, instanceid, reboottype):
        """
        Created on 2015. 2. 14.
        @author: 진수
        @summary: 서버 리부팅
        @param instanceid: 생성된 vm의 uuid
        @param reboottype: vm reboot 종류.("soft")
        @rtype: None
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {'reboot':{'type':reboottype}}
        print reqbody
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def getVNCConsole(self, instanceid):
        """
        Created on 2015. 2. 24.
        @author: 진수
        @summary:
        @param instanceid: 생성된 vm의 uuid
        @rtype: dict
        @return: {
                    "console": {
                        "type": "novnc",
                        "url": "http://127.0.0.1:6080/vnc_auto.html?token=191996c3-7b0f-42f3-95a7-f1839f2da6ed"
                    }
                }
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        print url
        reqbody = {'os-getVNCConsole':{'type':'novnc'}}
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def listFloatingIPPools(self):
        url = self.COMPUTE_URL + '/os-floating-ip-pools'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createFloatingIP(self, pool):
        """
        Created on 2015. 3. 19.
        @author: 진수
        @summary: create floating-ip
        @param str pool: IP Pool
        @rtype: dict
                {
                    "floating_ip": {
                        "fixed_ip": null,
                        "id": 1,
                        "instance_id": null,
                        "ip": "10.10.10.1",
                        "pool": "public_net"
                    }
                }
        """
        url = self.COMPUTE_URL + '/os-floating-ips'
        reqbody = {'pool':pool}
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def addFloatingIp(self, instanceid, address):
        """
        Created on 2015. 3. 19.
        @author: 진수
        @summary:
        @param param1:
        @rtype:
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {'addFloatingIp':{'address':address}}
        print "addFloating url -> %s"%url
        print "addFloating body -> %s"%json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def removeFloatingIp(self, instanceid, address):
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {'removeFloatingIp':{'address':address}}
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deallocateFloatingIp(self, ipid):
        url = self.COMPUTE_URL + '/os-floating-ips/' + ipid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)


if __name__ == '__main__':
#     pass

#     #keyston test
#     keystone = KeystoneClient()
#     keystone.authenticate('admin', 'admin', 'ohhberry3333', keystone.TOKEN_URL)
#     print json.dumps(keystone.getRoleByRoleName('member'))
#
#
#     tenant = keystone.createTenant('daemon')
#     print json.dumps(tenant)
#     user = keystone.createUser(tenant['tenant']['id'],'daemon', 'daemon!234')
#     print json.dumps(user)
#     role = keystone.getRoleByRoleName('member')
#     print json.dumps(role)
#     print keystone.addRoleToUser(tenant['tenant']['id'], user['user']['id'], role['id'])
#     print keystone.addRoleToUser("daemon", "daemon", '1456a09e26c943f7ac653be26542c4cf')
#
#     print json.dumps(keystone.getUserByName('wooni'))
#     tenant = keystone.getTenantByName('wooni')
#     print json.dumps(keystone.getTenantByName('star'))
#     print keystone.listTenants()
#     print json.dumps(keystone.listRoles())
#     print json.dumps(keystone.getRoleByRoleName('member'))
#     print keystone.getUserByName('jini')


#     #neutron test
#     neutron = NeutronClient()
#     neutron.authenticate('admin', 'admin', 'ohhberry3333', neutron.TOKEN_URL)
#     neutron.authenticate('star', 'star', 'star!234', neutron.TOKEN_URL)
#     green_network = neutron.createNetwork(tenant['tenant']['id'], "jini_GREEN_NET", "vlan", "physnet_hybrid", '11', True)
#     print "createNetwork green response --> %s" %green_network
#     orange_network = neutron.createNetwork(tenant['tenant']['id'], "jini_ORANGE_NET", "vlan", "physnet_hybrid", '12', True)
#     print "createNetwork orange response --> %s" %orange_network
#     print json.dumps(network)
#     print type(network)
#     sleep(1)
#     green_subnet = neutron.createSubnet(tenant['tenant']['id'], green_network['network']['name']+'_subnet', green_network['network']['id'], '4', '192.168.5.0/24', False)
#     print "createSubnet response --> %s" %green_subnet
#     orange_subnet = neutron.createSubnet(tenant['tenant']['id'], orange_network['network']['name']+'_subnet', orange_network['network']['id'], '4', '192.168.5.0/24', False)
#     print "createSubnet response --> %s" %orange_subnet

#     sleep(1)
#     neutron.deleteSubnet(subnet['subnet']['id'])
#     sleep(1)
#     neutron.deleteNetwork(network['network']['id'])
#     sg = neutron.createSecurityGroup("test", "test desc")
#     print sg
#     neutron.createSecurityGroupRulesForUTM(sg['security_group']['id'])


#    #nova test
#     nova = NovaClient()
#     nova.authenticate('admin', 'admin', 'ohhberry3333', nova.TOKEN_URL)
#     print json.dumps(nova.listSecurityGroups())
#     nova.authenticate('wooni', 'wooni', 'wooni!234', nova.TOKEN_URL)
#     print nova.listServers()
#     print nova.getServer("cffc3cc7-66ee-4bde-ae11-5806a00751b7")

#     print json.dumps(nova.listSecurityGroups())
#     print json.dumps(nova.getSecurityGroupByName("default"))
#     print nova.isSecurityGroupByName("Endian Access Group")
#     aaa = type(nova.getSecurityGroupByName("aaaa"))
#     print nova.createSecurityGroup("testSG", "sd desc")
#
#     keypair = nova.getKeypair('wooni-keypair')
#     if keypair.has_key('keypair') == True:
#         print keypair
#     else :
#         print 'fail'
#     networks = []
#     networks = [{"uuid":green_network['network']['id']}, {"uuid":orange_network['network']['id']}]
#     print nova.createUTM("jinivm", networks, "seocho-az:cnode02")


#     #createUTM
#     keystone = KeystoneClient()
#     keystone.authenticate('admin', 'admin', 'ohhberry3333', keystone.TOKEN_URL)
#     tenant = keystone.getTenantByName('wooni')
#     tenant_uuid = tenant['tenant']['id']
#     neutron = NeutronClient()
#     neutron.authenticate('admin', 'admin', 'ohhberry3333', neutron.TOKEN_URL)
#     green_network = neutron.createNetwork(tenant_uuid, "jini_GREEN_NET", "vlan", "physnet_hybrid", '13', False, False)
#     print "createNetwork green response --> %s" %green_network
#     orange_network = neutron.createNetwork(tenant_uuid, "jini_ORANGE_NET", "vlan", "physnet_hybrid", '14', False, False)
#     print "createNetwork orange response --> %s" %orange_network
#     green_subnet = neutron.createSubnet(tenant_uuid, green_network['network']['name']+'_subnet', green_network['network']['id'], '4', '192.168.5.0/24', False)
#     print "createSubnet response --> %s" %green_subnet
#     orange_subnet = neutron.createSubnet(tenant_uuid, orange_network['network']['name']+'_subnet', orange_network['network']['id'], '4', '192.168.6.0/24', False)
#     print "createSubnet response --> %s" %orange_subnet
#     nova = NovaClient()
#     nova.authenticate('wooni', 'wooni', 'wooni!234', nova.TOKEN_URL)
#     networks = [{"uuid":green_network['network']['id']}, {"uuid":orange_network['network']['id']}]
#     print nova.createUTM("jinivm", networks, "seocho-az:cnode02")


#     nova = NovaClient()
#     nova.authenticate('wooni', 'wooni', 'wooni!234', nova.TOKEN_URL)
# #     nova.excuteServer('41133c71-5f6c-4d1f-89e0-d9a6b981b659', 'pause')
#     nova.rebootServer('41133c71-5f6c-4d1f-89e0-d9a6b981b659', 'SOFT')


#     nova = NovaClient()
#     nova.authenticate('admin', 'admin', 'ohhberry3333', nova.TOKEN_URL)
# #     print json.dumps(nova.listSecurityGroups())
# #     nova.authenticate('wooni', 'wooni', 'wooni!234', nova.TOKEN_URL)
# #     print nova.listServers()
#     print json.dumps(nova.getServer("a92511cf-81b8-4c57-b2da-5682de51ad5d"))


#     neutron = NeutronClient()
# #     neutron.authenticate('221.151.188.15', 'admin', 'admin', 'ohhberry3333', 'http://221.151.188.15:5000/v2.0/tokens')
#     neutron.authenticate('221.151.188.15', 'wooni', 'wooni', 'wooni!234', 'http://221.151.188.15:5000/v2.0/tokens')
#     print json.dumps(neutron.listNetworks())


    neutron = NeutronClient()
    neutron.authenticate('221.151.188.15', 'wooni', 'wooni', 'wooni!234', 'http://221.151.188.15:5000/v2.0/tokens')
#     neutron.createSubnetExt('tenantid', 'subnetname', 'networkid', 'ipversion', 'cidr', 'enabledhcp', 'allocationpools')

#     seocho_openstack = {
#     'security_groups' : [
#                          {"name":"Endian Access Group"}
#                          ]
#                         }
#     print seocho_openstack['security_groups'][0]['name']
