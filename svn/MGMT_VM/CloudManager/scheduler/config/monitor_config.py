# -*- coding: utf-8 -*-

###################################################################
#
#   현재는 구성파일을 복잡하게 할 수 있도록 파이썬의 dict 이용
#   추후, yaml을 사용하는 방안도 고려...
#   2015.02.06 iscsid, tgtd 수집 대상에서 삭제함
###################################################################

#국사 설정
org = {"orgseq":11} #대전연구소
# org = {"orgseq":12} #테스트용국사


# instance mgmt_net resource 임계치 설정
mgmt_net_alarm = {
    'cpu' : 80,
    'memory': 80,
    'disk':80
}
# 감시대상 호스트와 호스트 내부의 콤포넌트 정보
monitor = {
    # 감시대상 호스트 리스트
    'host_list': ['anode'],

    # 감시대상 호스트 정보
    # prefix: m: management, n: network, c: compute, s: storage
    #         mn: mgmt & network -> controller와 network 기능을 하나의 서버에 설치(비용이슈)
    'anode'    : {
        'ip'   : 'VshqBMH+p2+ThUdno87fi92OGxpk8rRG2s1pi/5e1d0=',
        'port' : 22,
        'id'   : '+3jaIdxOYE5FC8GHS/N1dhUTRwtuoUNDFg6fmalKEEM=',
        'pw'   : 'UbyIfwiYcpsVTrUESh9dMz+HOPbx2l9cAMEnNPZvLrQ='
    },
    'anode_components' : {
        'nova'          : ['nova-api','nova-conductor','nova-scheduler','nova-cert',
                           'nova-consoleauth','nova-novncproxy'],
        'neutron'       : ['neutron-server','neutron-openvswitch-agent',
                           'neutron-dhcp-agent','neutron-l3-agent',
                           'neutron-metadata-agent','neutron-ns-metadata-proxy'],
        'cinder'        : ['cinder-api', 'cinder-scheduler','cinder-volume'],
        'glance'        : ['glance-api', 'glance-registry'],
        'keystone'      : ['keystone-all'],
        'cinder_utils'  : ['iscsid', 'tgtd'],
        'horizon_utils' : ['apache2','memcached'],
        'message_utils' : ['epmd', 'beam.smp'],
        'db_utils'      : ['mysqld'],
        'neutron_utils' : ['dnsmasq','ovsdb-client','ovsdb-server','ovs-vswitchd']
    },

}

