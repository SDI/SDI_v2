#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: umisue
@summary: 수집모듈.
        openstack(mn_node, cnode01, cnode02) 관련 상태정보 수집
        switch(aggr, tor) 관련 상태정보 수집
        openstack instance 상태정보 수집

Modified on 2015. 01. 28 모니터링 모듈 추가
                         tb_openstack_state_change 변경내용 저장
Modified on 2015. 03. 03 모니터링 모듈 추가
                         tb_customer_servier_state_info 수집 대상 추가                         
'''
import collections, datetime, json, os, sys 

from apscheduler.schedulers.blocking import BlockingScheduler
import paramiko
import psycopg2
import ssl
from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders
from websocket import create_connection

from config.connect_config import db_conn_info, websocket_conn_info, vm_manage_info, AESCipherKey
from config.monitor_config import monitor, org
from helper.logHelper import myLogger
from helper.paramikoHelper  import myParamiko
from sql.monitorcollectorsql import OpenstackHostStateInfoSql, CustomerServerStateInfoSql, SdnApplianceStateInfoSql, CloudServerStateInfoSql, SwitchStateInfoSql, SdnSwitchStateInfoSql, SdnIPmappingStateInfoSql, CollectorProcessStateInfoSql
from sql.provisioningsql import Provisioningsql
from util.aescipher import AESCipher
from util.openstackclient import NovaClient


org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='nestatusmonitor', logdir='./log', loglevel='debug', logConsole=True).get_instance()

orgseq = org["orgseq"]

class OpenStackModuleMonitor(object):
    """
        클라우드를 구성하는 개별 노드정보와 노드의  감시대상 콤포넌트를 구성파일에 읽어와서 그와 관련된 정보를 수집한다.
    """
    global dic_host
    dic_host =[]
    d_host = collections.OrderedDict()

    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.remote_shell_connect()
        self.monitorsql = OpenstackHostStateInfoSql()

    def init_config_from_dict_config_file(self):
        # 데이터베이스 관련 정보
        aes = AESCipher(AESCipherKey['key'])

        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        # 감시대상 호스트 리스트
        self.host_list = monitor['host_list']
        # 감시대상 호스트 정보 dictionary
        # prefix: m: management, n: network, c: compute, s: storage
        #         mn: mgmt & network -> controller와 network 기능을 하나의 서버에 설치(비용이슈)
#         self.node    = monitor['node']
#         components_name = "%s"
#         self.node_components = monitor['node_components']

    def database_connect(self):
        self.conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)

    def remote_shell_connect(self):
        self.rssh = myParamiko()
        self.rssh.timeout = 10
        self.rssh.debug = True
        aes = AESCipher(AESCipherKey['key'])
        for host in self.host_list:
            # 문자열로 된 속성이름을 참조하기 위해 getattr 사용
#             node = getattr(self, host)
            node = monitor[host]
            ip = aes.decrypt(node['ip'])
            port = node['port']
            id = aes.decrypt(node['id'])
            pw =  aes.decrypt(node['pw'])
            self.rssh.register( host, ip, port, id, pw )
            self.rssh.connect(host)

    def check_openstack_component(self, cur, tag, mon_components, recs_ref):
        try:
            serv_info = self.rssh.getservinfo(tag)[0]
                # NGKIM
#             self.save_pslist(tag)   
            for key, val in mon_components.iteritems():
    #             log.info("")
#                 log.info("# [%s] list ::" % (key))
                for component in val:
                    cmd = """ps -ef | grep '%s' | grep -v grep | wc -l """ % (component)
                    #JINI - direct call. paramiko use.
                    running_num = self.rssh.run_with_result(tag, cmd)
                    d = collections.OrderedDict()
                    d['host']       = tag
                    d['service']    = key
                    d['component']  = component
                    d['num']        = running_num
                    recs_ref.append(d)
                    try:
                        myquery = self.monitorsql.insertOpenstackHostStateInfo(self.invokeTime, orgseq, tag, serv_info, key, component, int(running_num))
                        cur.execute(myquery)
#                         log.debug("#"*100)
#                         log.debug("host db insert")
                        '''
                        * 프로세스카운트 테스트를 위한 테스트 데이터
                        '''
    #                     running_num = str(random.randint(0,5))
                        '''
                        * 프로세스카운트 테스트를 위한 테스트 데이터
                        '''
    
                        if int(running_num) == 0:
                            alarm_type = "1"
                        else:
                            alarm_type = "0"

                        if alarm_type == "1" :#and usertype == "1":
                            selectquery = self.monitorsql.selectsmsdata(orgseq, tag, 'OPENSTACK', component, running_num)
                            cur.execute(selectquery)
                            rows = cur.fetchone()
                            smscount = rows[0]
                            log.debug("smscount %s"%smscount)
                            if smscount == 0:
                                myquery = self.monitorsql.insertsmsdata(self.invokeTime, orgseq, tag, 'OPENSTACK', component, running_num)
                                cur.execute(myquery)
                                                    
                        result_host, checkData = self.checkHostStatus(self.invokeTime, orgseq, tag, serv_info, key, component, int(running_num))
                        log.debug("result_host %s component %s"%(result_host, component))
                        if ((result_host <> "EXIST") or (alarm_type == "1")) :   
                            if result_host == "CHANGE":
                                data_type = "C"
                            elif result_host == "FIRST":
                                data_type = "I"
                            else:
                                data_type = "N"
#                             changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 1, data_type, "N", checkData)
#                             cur.execute(changequery)    
                            sendData = {"command":"notifyAll", "data_type":data_type,"alarm_type":alarm_type, "alarm_detail":running_num, "create_time":self.invokeTime, "orgseq": orgseq, "infotype":1, "collect_data":checkData}
                            sendData_json = json.dumps(sendData)
                            self.sendBroadcasting(sendData_json)
                    except Exception, e:
                        log.error('Error %s' % e)
        except Exception,e:
            log.error("OpenStackModuleMonitor exception error %s"%e.message)

    def save_pslist(self, tag):
        filename=self.get_pslist_file_name(tag)
        cmd = "ps -ef | grep -v grep "
        ch = self.rssh.run(tag, cmd)
        fo = open(filename, "w")
        for line in ch.read().splitlines():
            fo.write(line+'\n')
        fo.close()

    def get_ps_count(self, tag, component):
        cmd = """cat %s | grep '%s' | wc -l """ % (self.get_pslist_file_name(tag), component)
        import subprocess
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        (output, err) = p.communicate()
        return output.strip()

    def get_pslist_file_name(self, tag):
        return "/tmp/processes.%s.log" % tag

    def run(self, invokeTime):
#         print 'OpenStackMonitor() 작업시작'
        try:
            recs_ref = []
            self.invokeTime = invokeTime
    
#             log.debug("###### %s" %self.host_list)
            
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()
                
            for host in self.host_list:

#                 node = getattr(self, host)
#                 log.debug("%-8s [%s] node connect" % (host, node['ip']) )

#                 if host == "anode":
                componentname = host+"_components"
                monitor_components = monitor[componentname]
                self.check_openstack_component(cur, host, monitor_components, recs_ref)
                """

                self.check_ctrlnnode_openstack_component(self.ctrl_tag, recs_ref)
                recs_json = json.dumps(recs_ref)
                print recs_json
                """
                   
    
            data_len = len(dic_host)
            for index in range(0, data_len) :
                if dic_host[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":1, "collect_data":dic_host[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 3, "D", "N", json.dumps(dic_host[data_len-(index + 1)]))
#                     cur.execute(deletedequery)
                    dic_host.pop(data_len-(index + 1))
                    self.sendBroadcasting(send_delete_data_json)
    
            self.checkResetHost()   
            
            conn.commit()
            
        except Exception,e:
            log.error("OpenStackModuleMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            self.rssh.close(host)
            if conn is not None:
                conn.close()

    def checkHostStatus(self, create_time, orgseq, host, hostip, service, component, pcount):
        count = 0
        for row in dic_host:
            if row["orgseq"] == orgseq and row["host"] == host and row["service"] == service and row["component"] == component:
                if row["pcount"] == pcount :
                    row["check"] = "T"
                    return "EXIST",json.dumps(dic_host[count])
                else:
                    row["pcount"] = pcount
                    row["check"] = "T"
                    return "CHANGE",json.dumps(dic_host[count])
            count = count + 1
        d_host = {"create_time": create_time,"orgseq":orgseq,"host":host, "hostip":hostip, "service": service, "component": component, "pcount":pcount, "check":"T"}
        dic_host.append(d_host)
        return "FIRST",json.dumps(d_host)

    def checkResetHost(self):
        for row in dic_host:
            row["check"] = "F"

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("Host send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

    def initdicHost(self):
        log.info("#"*100)
        conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
        conn.autocommit = True
        cur = conn.cursor()
        try:
            myquery = self.monitorsql.selectOpenstackHostStateInfoMnnodeExt(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_host = dict(zip(columns, row))
                dic_host.append(d_host)
            log.info("initdicHost %s" %len(dic_host))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)

class SwitchMonitor(object):
    global dic_switchm
    dic_switchm = []
    d_switchm = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.customersql = CustomerServerStateInfoSql()
        self.switchsql = SwitchStateInfoSql()

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicSwitch(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.switchsql.selectSwitchStateInfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_switchm = dict(zip(columns, row))
                dic_switchm.append(d_switchm)
            log.info("initdicSwitch %s" %len(dic_switchm))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.switchsql.getopenstackinfra(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            switchlist = []
            for row in rows:
                switchlist.append(dict(zip(columns, row)))
            for  switch in switchlist[0]["infraprops"]["switch_list"]:
                print json.dumps(switch)
                switchname =  switch["name"]
                switchip =  switch["ip"]

                status = self.checkPing(switchip)
                if(status == "ACTIVE"):
                    alarm_type = "0"
                else:
                    alarm_type = "1"
                myquery = self.switchsql.insertOpenStackSwitchStateInfo(self.invokeTime, orgseq, switchname, switchip, status)
                cur.execute(myquery)
                result_switchm, checkData = self.checkSwitchStatus(self.invokeTime, orgseq, switchname, switchip, status)
#                 log.debug("[%s] servername %s serverip %s status %s"%(result_switchm, servername, serverip, status))
                if ((result_switchm <> "EXIST") or (alarm_type == "1"))  :
                    if result_switchm == "CHANGE":
                        data_type = "C"
                    elif result_switchm == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":2, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)                       
            log.info("initdicCloud %s" %len(dic_switchm))           
            data_len = len(dic_switchm)
            for index in range(0, data_len) :
                if dic_switchm[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":2, "collect_data":dic_switchm[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_switchm.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_switchm[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("SwitchMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetCustomer(self):
        for row in dic_switchm:
            row["check"] = "F"
            
    def checkSwitchStatus(self, create_time, orgseq, switchname, switchip, status):
        count = 0
        for row in dic_switchm:
            if row["orgseq"] == orgseq and row["switchip"] == switchip:
                row["check"] = "T"
                if row["status"] == status :
                    return "EXIST", json.dumps(dic_switchm[count])
                else:
                    row["status"] = status
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_switchm[count])
            count = count + 1

        d_switchm = {"create_time": create_time,"orgseq":orgseq,"switchname":switchname, "switchip":switchip, "status": status, "check":"T"}
        dic_switchm.append(d_switchm)
        return "FIRST",json.dumps(d_switchm)
    
    def checkPing(self, addr):
        response = os.system("ping -c 1 " + addr)
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        return result

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("Switch send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

class OpenstackInstanceMonitor(object):
    global dic_instance
    dic_instance = []
    d_instance = collections.OrderedDict()    
    """
    openstack api를 사용하여 해당노드의 인스턴스 정보 모니터링
    """
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.monitorsql = OpenstackHostStateInfoSql()


    def __del__(self):
        pass

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        sql = Provisioningsql()
        conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
        conn.autocommit = True
        cur = conn.cursor()
        query = sql.getopenstackinfra(orgseq)
        cur.execute(query)
        rows = cur.fetchone()
        self.tenathost = aes.decrypt(rows[0]["openstack_connect"]["host"])    
        self.tenantname = aes.decrypt(rows[0]["openstack_connect"]["tenantid"])
        self.tenantusername = aes.decrypt(rows[0]["openstack_connect"]["userid"])
        self.tenantpassword = aes.decrypt(rows[0]["openstack_connect"]["password"])
        self.tenantauthurl = aes.decrypt(rows[0]["openstack_connect"]["token_url"])
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            tokeninfo = self.getOpenstackToken()
            log.debug("listServersDetail start")
            serverinfo = self.listServersDetail(tokeninfo['novaurl'], tokeninfo['tenantid'], tokeninfo['tokenid'])           
            servers = serverinfo['servers']
            log.debug("listServersDetail servers %s"%json.dumps(servers))
            log.debug("listServersDetail end")
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()
            server_count = 0
            for server in servers:
                status = server['status']
                vmname = server['name']
                vmid = server['id']
                hostname = server['OS-EXT-SRV-ATTR:host']
                conntracklist = ""
                selectquery = self.monitorsql.selectNeUertypeInfo(vmid)
                cur.execute(selectquery)
                rows = cur.fetchone()
                usertype = str(rows[0])
                log.debug("vmname %s"%vmname)
                if usertype == "0":
                    prdname = ""
                else:
                    prdnamequery = self.monitorsql.selectProductname(vmid)
                    cur.execute(prdnamequery)
                    rows = cur.fetchone()
                    prdname = str(rows[0])
                
                log.debug("prdname %s"%prdname)
                
                if(status == "ACTIVE"):                   
                    if usertype == "0":
                        alarm_type = "0"
                        resource = ["0","0","0","0"]
                    else:
                        try:
                            resource = self.getServerResource(vmid)      
                        except Exception,e:
                            log.error("getServerResource error %s"%e.message)
                            continue          
                                         
                        if usertype == "1" and resource == []:
                            alarm_type = "1"
                            resource = ["0","0","0","0"]
                        elif usertype == "1" and resource[3] == "0":
                            alarm_type = "1"
                        elif usertype == "1" and resource[3] <> "0":
                            alarm_type = "0"
                            try :
                                conntracklist = self.getServerConntrack(vmid)
                            except Exception,e:
                                log.error("getServerConntrack error %s"%e.message)
                                continue                                      
                else:
                    alarm_type = "1"
                    resource = ["0","0","0","0"]
                log.debug("usertype %s"%usertype)
                    
                log.debug("#"*100)
                myquery = self.monitorsql.insertOpenstackInstanceStateInfo(self.invokeTime, orgseq, vmid, status, vmname, hostname, resource, usertype, prdname, conntracklist)
                cur.execute(myquery)
                log.debug("instacnce db insert")
                result_instance, checkData = self.checkInstanceStatus(self.invokeTime, orgseq, vmid, status, vmname, hostname, resource, prdname)
                log.debug("[%s] hostname %s vmname %s status %s resource %s" %(result_instance, hostname, vmname, status, resource))
                if ((result_instance <> "EXIST") or (alarm_type == "1"))  :
                    if result_instance == "CHANGE":
                        data_type = "C"
                    elif result_instance == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 3, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":usertype, "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":resource, "create_time":self.invokeTime, "orgseq": orgseq, "infotype":3, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)
                server_count = server_count + 1
            data_len = len(dic_instance)
            for index in range(0, data_len) :
#                 log.debug("#"*100)
#                 log.debug("Deleted VMID check %s"%dic_instance[data_len-(index + 1)])
                if dic_instance[data_len-(index + 1)]["check"] == "F":
                    print len(dic_instance[data_len-(index + 1)])
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":3, "collect_data":dic_instance[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_instance.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 3, "D", "N", json.dumps(dic_instance[data_len-(index + 1)]))
#                     cur.execute(deletedequery)
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetInstance()
            conn.commit()
        except Exception, e:
            log.error("OpenstackInstanceMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()

    def listServersDetail(self, url, tenantid, tokenid):
        url = url + '/servers/detail?all_tenants=1'
        httpHeaders = {"Content-Type":"application/json;charset=UTF-8", "accept":"application/json", "X-Auth-Token":tokenid}
        response = json.loads(self.sendData(url, 'get', httpHeaders, None))
        return response

    def getOpenstackToken(self):
        """
        openstack에서 토큰아이디와 tenantid, nova접근url을 dict으로 리턴
        """
#         url = 'http://'+self.anode['ip'] + ':5000/v2.0/tokens'
        url = self.tenantauthurl
        httpHeaders = {"content-type":"application/json;charset=UTF-8", "accept":"application/json"}
        reqBody = {'auth':{'tenantName':self.tenantname, 'passwordCredentials':{'username':self.tenantusername,'password':self.tenantpassword}}}
        response = json.loads(self.sendData(url, 'post', httpHeaders, json.dumps(reqBody)))
        result = {}
        result['tokenid'] = response['access']['token']['id']
        result['tenantid'] = response['access']['token']['tenant']['id']
        result['novaurl'] = response['access']['serviceCatalog'][0]['endpoints'][0]['publicURL'].replace("controller",self.tenathost)
        return result

    def sendData(self, url, httpMethod, httpHeaders, reqBody):
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders(httpHeaders)
        if httpMethod == "post":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "delete" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(),  body=None)
        elif httpMethod == "get" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(),  body=None)
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            return response.body
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result

    def checkInstanceStatus(self, create_time, orgseq, vmid, status, vmname, host, resource, prdname):
        count = 0
        for row in dic_instance:
            if row["orgseq"] == orgseq and row["vmid"] == vmid:      
                row["check"] = "T"          
                row["cpu"] = resource[0]
                row["memory"] = resource[1]
                row["disk"] = resource[2]
                if row["status"] == status and row["conntrack"] == resource[3]:
                    return "EXIST", json.dumps(dic_instance[count])
                else:
                    row["status"] = status
                    row["conntrack"] = resource[3]
                    return "CHANGE",json.dumps(dic_instance[count])
            count = count + 1
        d_instance = {"create_time": create_time,"orgseq":orgseq,"vmid":vmid, "status":status, "vmname": vmname, "host": host, "prdname":prdname, "cpu":resource[0], "memory":resource[1], "disk":resource[2], "conntrack":resource[3], "check":"T"}
        dic_instance.append(d_instance)
        return "FIRST",json.dumps(d_instance)
#
    def checkResetInstance(self):
        for row in dic_instance:
            row["check"] = "F"

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("Instance send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)
            
    def getServerResource(self, vmid):      
        nova = NovaClient()
        nova.authenticate(self.tenathost, self.tenantname, self.tenantusername, self.tenantpassword, self.tenantauthurl)
        server = nova.getServer(vmid)
        for row in server['server']['addresses']['global_mgmt_net']:
            if row['OS-EXT-IPS:type'] == "fixed":
                addr = row['addr']
        cmd_cpu = "grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage}'"
        cmd_mem = "free -m |grep -i Mem: |awk '{print ($3/$2)*100}'"
        cmd_dis = "df -m /home |grep -v Use |awk '{print $5}'|tr -d \"%\""
        cmd_conntrack = "conntrack -L | wc -l "
#         resource = []
        resource = self.sendCommandlist(addr, cmd_cpu, cmd_mem, cmd_dis, cmd_conntrack)
        return resource

    def getServerConntrack(self, vmid):      
        nova = NovaClient()
        nova.authenticate(self.tenathost, self.tenantname, self.tenantusername, self.tenantpassword, self.tenantauthurl)
        server = nova.getServer(vmid)
        for row in server['server']['addresses']['global_mgmt_net']:
            if row['OS-EXT-IPS:type'] == "fixed":
                addr = row['addr']
#         cmd_conntrackcnt = "conntrack -L | wc -l "
        cmd_conntracklist = "conntrack -L "
#         resource = []
        resource = self.sendCommand(addr, cmd_conntracklist)
        if len(resource) > 5000:
            log.debug("length %s"%len(resource))
            resource = resource[1:4999]
        return resource

    def sendCommand(self, ipAddress, cmd):
        client = paramiko.SSHClient()
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(vm_manage_info['user'])
            password = aes.decrypt(vm_manage_info['password'])
            client.connect(hostname=ipAddress, username=user, password=password, timeout=3)
            stdin, stdout, stderr = client.exec_command(cmd)
            result = ""
            for line in stdout:
                result = result + line.replace('\n','')
#             print "***"*50
#             print "result %s"%result                
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(ipAddress,str(e)))
            sys.exit()
        finally:
            client.close()
            return result
            
    def sendCommandlist(self, ipAddress, cmd_cpu, cmd_mem, cmd_dis, cmd_conntrack):
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(vm_manage_info['user'])
            password = aes.decrypt(vm_manage_info['password'])
            client.connect(hostname=ipAddress, username=user, password=password, timeout=3)
            cmd_list = [cmd_cpu,cmd_mem,cmd_dis,cmd_conntrack]
            req = ';'.join(cmd_list)
            stdin, stdout, stderr = client.exec_command(req)
            for line in stdout:
                result.append(line.replace('\n',''))       
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(ipAddress,str(e)))
            sys.exit()
        finally:
            client.close()
            return result

    def initdicInstance(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.monitorsql.selectOpenstackInstanceStateInfoExt(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_instance = dict(zip(columns, row))
                dic_instance.append(d_instance)
            log.info("initdicInstance %s" %len(dic_instance))
        except psycopg2.DatabaseError, e:
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)

class CustomerServerMonitor(object):
    global dic_customer
    dic_customer = []
    d_customer = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.monitorsql = OpenstackHostStateInfoSql()
        self.customersql = CustomerServerStateInfoSql()

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicCustomer(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.customersql.selectCustomerServerStateInfoExt(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_customer = dict(zip(columns, row))
                dic_customer.append(d_customer)
            log.info("initdicCustomer %s" %len(dic_customer))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.customersql.selectCustomerServerList(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            customers = []
            for row in rows:
                customers.append(dict(zip(columns, row)))
            for customer in customers:
                customername = customer["customername"]
                servername = customer["servername"]
                serverip = customer["serverip"]
                servertype = customer["servertype"]
                status = self.checkPing(serverip)
                if(status == "ACTIVE"):
                    alarm_type = "0"
                else:
                    alarm_type = "1"
                myquery = self.customersql.insertCustomerServerStateInfo(self.invokeTime, orgseq, customername, servername, serverip, servertype, status)
                cur.execute(myquery)
                result_customer, checkData = self.checkCustomerServerStatus(self.invokeTime, orgseq, customername, servername, serverip, servertype, status)
                log.debug("[%s] servername %s serverip %s status %s"%(result_customer, servername, serverip, status))
                if ((result_customer <> "EXIST") or (alarm_type == "1"))  :
                    if result_customer == "CHANGE":
                        data_type = "C"
                    elif result_customer == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":4, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)                       
            log.info("initdicCustomer %s" %len(dic_customer))           
            data_len = len(dic_customer)
            for index in range(0, data_len) :
                if dic_customer[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":4, "collect_data":dic_customer[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_customer.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_customer[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("CustomerServerMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetCustomer(self):
        for row in dic_customer:
            row["check"] = "F"
            
    def checkCustomerServerStatus(self, create_time, orgseq, customername, servername, serverip, servertype, status):
        count = 0
        for row in dic_customer:
            if row["orgseq"] == orgseq and row["serverip"] == serverip:
                row["check"] = "T"
                if row["status"] == status :
                    return "EXIST", json.dumps(dic_customer[count])
                else:
                    row["status"] = status
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_customer[count])
            count = count + 1

        d_customer = {"create_time": create_time,"orgseq":orgseq,"customername":customername, "servername":servername, "serverip": serverip, "servertype":servertype, "status":status, "check":"T"}
        dic_customer.append(d_customer)
        return "FIRST",json.dumps(d_customer)
    
    def checkPing(self, addr):
#         cmd = "netstat -an | grep LISTEN"
#         out = self.sendCommand(addr, cmd)
#         cnt = 0
#         for line in out:
#             if ':::22' in line:
#                 cnt = cnt + 1
#             if '0.0.0.0:10443' in line:
#                 cnt = cnt + 1
#         if cnt > 1:
#             result = "ACTIVE"
#         else:
#             result = "DEACTIVE"
        response = os.system("ping -c 1 " + addr)
        # and then check the response...
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        
        return result

    def sendCommand(self, ipAddress, cmd):
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(vm_manage_info['user'])
            password = aes.decrypt(vm_manage_info['password'])
            client.connect(hostname=ipAddress, username=user, password=password, timeout=3)
            stdin, stdout, stderr = client.exec_command(cmd)
            for line in stdout:
                result.append(line.replace('\n',''))
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(ipAddress,str(e)))
            sys.exit()
        finally:
            client.close()
            return result

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)     
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("CustomerServer send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

class SdnApplianceMonitor(object):
    global dic_sdn
    dic_sdn = []
    d_sdn = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.monitorsql = OpenstackHostStateInfoSql()
        self.sdnappliacne = SdnApplianceStateInfoSql()
        
    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicSdnAppliance(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.sdnappliacne.selectSdnApplianceStateInfoExt(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_sdn = dict(zip(columns, row))
                dic_sdn.append(d_sdn)
            log.info("initdicSdn %s" %len(dic_sdn))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.sdnappliacne.selectSdnApplianceList(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            adnappliances = []
            for row in rows:
                adnappliances.append(dict(zip(columns, row)))
            for adnappliance in adnappliances:
#                 applianceid = adnappliance["applianceid"]
                appliancetype = adnappliance["appliancetype"]
                appliancename = adnappliance["appliancename"]
                ipaddr = adnappliance["ipaddr"]
#                 portnum = adnappliance["portnum"]
#                 switchdpid = adnappliance["switchdpid"]
#                 portnum = ""
#                 switchdpid = ""
#                 connecttype = ""
                companyname = ipaddr
                status = self.checkPing(ipaddr)
                if(status == "ACTIVE"):
                    alarm_type = "0"
                else:
                    alarm_type = "1"

                myquery = self.sdnappliacne.insertSdnApplianceStateInfo(self.invokeTime, orgseq, appliancetype, appliancename, ipaddr, status, companyname)
                log.debug("myquery -> %s"%myquery)
                cur.execute(myquery)

                if alarm_type == "1" :#and usertype == "1":
                    selectquery = self.monitorsql.selectsmsdata(orgseq, appliancename, 'HWUTM', ipaddr, status)
                    cur.execute(selectquery)
                    rows = cur.fetchone()
                    smscount = rows[0]
                    log.debug("smscount %s"%smscount)
                    if smscount == 0:
                        myquery = self.monitorsql.insertsmsdata(self.invokeTime, orgseq, appliancename, 'HWUTM', ipaddr, status)
                        cur.execute(myquery)
                        
                result_sdn, checkData = self.checkSdnApplianceStatus(self.invokeTime, orgseq, appliancetype, appliancename, ipaddr, status, companyname)
                log.debug("[%s] ipaddr %s status %s"%(result_sdn, ipaddr, status))

                if ((result_sdn <> "EXIST") or (alarm_type == "1"))  :
                    if result_sdn == "CHANGE":
                        data_type = "C"
                    elif result_sdn == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 5, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":5, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)
                 
            log.info("initdicSdn %s" %len(dic_sdn))           
            data_len = len(dic_sdn)
            for index in range(0, data_len) :
                if dic_sdn[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":5, "collect_data":dic_sdn[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_sdn.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 5, "D", "N", json.dumps(dic_sdn[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetSdnAppliance()            
            conn.commit()
        except Exception, e:
            log.error("SdnApplianceMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetSdnAppliance(self):
        for row in dic_sdn:
            row["check"] = "F"
            
    def checkSdnApplianceStatus(self, create_time, orgseq, appliancetype, appliancename, ipaddr, status, companyname):

        count = 0
        for row in dic_sdn:
            if row["orgseq"] == orgseq and row["ipaddr"] == ipaddr:
                row["check"] = "T"
                if row["status"] == status :
                    return "EXIST", json.dumps(dic_sdn[count])
                else:
                    row["status"] = status
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_sdn[count])
            count = count + 1

        d_sdn = {"create_time": create_time,"orgseq":orgseq, "appliancetype":appliancetype, "appliancename": appliancename, "ipaddr":ipaddr, "status":status, "companyname":companyname,"check":"T"}
        dic_sdn.append(d_sdn)
        return "FIRST",json.dumps(d_sdn)

    def checkPing(self, addr):
        response = os.system("ping -c 1 " + addr)
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        return result
        
    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("SdnAlliance send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

class CloudServerMonitor(object):
    global dic_cloud
    dic_cloud = []
    d_cloud = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.customersql = CustomerServerStateInfoSql()
        self.cloudsql = CloudServerStateInfoSql()

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicCloud(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.cloudsql.selectOpenstackServerStateInfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_cloud = dict(zip(columns, row))
                dic_cloud.append(d_cloud)
            log.info("initdicCloud %s" %len(dic_cloud))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            aes = AESCipher(AESCipherKey['key'])
            
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.cloudsql.getopenstackinfra(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            cloudservers = []
            for row in rows:
                cloudservers.append(dict(zip(columns, row)))
            for cloudserver in cloudservers[0]["infraprops"]["host_list"]:
                print json.dumps(cloudserver)
                
                hostname = aes.decrypt(cloudserver["name"])
                hostip = aes.decrypt(cloudserver["ip"])

                status = self.checkPing(hostip)
                if(status == "ACTIVE"):
                    alarm_type = "0"
                else:
                    alarm_type = "1"
                myquery = self.cloudsql.insertOpenStackServerStateInfo(self.invokeTime, orgseq, hostname, hostip, status)
                cur.execute(myquery)
                
                if alarm_type == "1" :#and usertype == "1":
                    selectquery = self.monitorsql.selectsmsdata(orgseq, hostname, 'OPENSTACK', hostip, status)
                    cur.execute(selectquery)
                    rows = cur.fetchone()
                    smscount = rows[0]
                    log.debug("smscount %s"%smscount)
                    if smscount == 0:
                        myquery = self.monitorsql.insertsmsdata(self.invokeTime, orgseq, hostname, 'OPENSTACK', hostip, status)
                        cur.execute(myquery)
                                        
                result_cloud, checkData = self.checkCloudServerStatus(self.invokeTime, orgseq, hostname, hostip, status)
                log.debug("[%s] cloud server servername %s serverip %s status %s"%(result_cloud, hostname, hostip, status))
                if ((result_cloud <> "EXIST") or (alarm_type == "1"))  :
                    if result_cloud == "CHANGE":
                        data_type = "C"
                    elif result_cloud == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":6, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)                       
            log.info("initdicCloud %s" %len(dic_cloud))           
            data_len = len(dic_cloud)
            for index in range(0, data_len) :
                if dic_cloud[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":6, "collect_data":dic_cloud[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_cloud.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_cloud[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("CloudServerMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetCustomer(self):
        for row in dic_cloud:
            row["check"] = "F"
            
    def checkCloudServerStatus(self, create_time, orgseq, hostname, hostip, status):
        count = 0
        for row in dic_cloud:
            if row["orgseq"] == orgseq and row["hostip"] == hostip:
                row["check"] = "T"
                if row["status"] == status :
                    return "EXIST", json.dumps(dic_cloud[count])
                else:
                    row["status"] = status
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_cloud[count])
            count = count + 1

        d_cloud = {"create_time": create_time,"orgseq":orgseq,"hostname":hostname, "hostip":hostip, "status": status, "check":"T"}
        dic_cloud.append(d_cloud)
        return "FIRST",json.dumps(d_cloud)
    
    def checkPing(self, addr):
        response = os.system("ping -c 1 " + addr)
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        return result

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("CloudServer send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

class SdnSwitchMonitor(object):
    global dic_sdnswitch
    dic_sdnswitch = []
    d_sdnswitch = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.sdnsql = SdnSwitchStateInfoSql()

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicSdnSwitch(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.sdnsql.selectSdnSwitchStateInfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_sdnswitch = dict(zip(columns, row))
                dic_sdnswitch.append(d_sdnswitch)
            log.info("initdicSdnSwitch %s" %len(dic_sdnswitch))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.sdnsql.getsdnswitchinfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            sdnlist = []
            for row in rows:
                sdnlist.append(dict(zip(columns, row)))
            for  sdnswitch in sdnlist:
                print json.dumps(sdnswitch)
                sdnswitchdpid = sdnswitch["switch_dpid"]
                sdnswitchname =  sdnswitch["switch_name"]
                sdnswitchip =  sdnswitch["mgmt_ip"]

                status = self.checkPing(sdnswitchip)
                if(status == "ACTIVE"):
                    alarm_type = "0"
                else:
                    alarm_type = "1"
                myquery = self.sdnsql.insertOpenStackSdnSwitchStateInfo(self.invokeTime, orgseq, sdnswitchdpid, sdnswitchname, sdnswitchip, status)
                cur.execute(myquery)
                result_sdnswitch, checkData = self.checkSdnSwitchStatus(self.invokeTime, orgseq, sdnswitchdpid, sdnswitchname, sdnswitchip, status)
#                 log.debug("[%s] servername %s serverip %s status %s"%(result_sdnswitch, servername, serverip, status))
                if ((result_sdnswitch <> "EXIST") or (alarm_type == "1"))  :
                    if result_sdnswitch == "CHANGE":
                        data_type = "C"
                    elif result_sdnswitch == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":7, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)                       
            log.info("initdicSdnSwitch %s" %len(dic_sdnswitch))           
            data_len = len(dic_sdnswitch)
            for index in range(0, data_len) :
                if dic_sdnswitch[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":7, "collect_data":dic_sdnswitch[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_sdnswitch.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_sdnswitch[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("SdnSwitchMonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetCustomer(self):
        for row in dic_sdnswitch:
            row["check"] = "F"
            
    def checkSdnSwitchStatus(self, create_time, orgseq, sdnswitchdpid, sdnswitchname, sdnswitchip, status):
        count = 0
        for row in dic_sdnswitch:
            if row["orgseq"] == orgseq and row["sdnswitchip"] == sdnswitchip:
                row["check"] = "T"
                if row["status"] == status :
                    return "EXIST", json.dumps(dic_sdnswitch[count])
                else:
                    row["status"] = status
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_sdnswitch[count])
            count = count + 1

        d_sdnswitch = {"create_time": create_time,"orgseq":orgseq, "sdnswitchdpid":sdnswitchdpid, "sdnswitchname":sdnswitchname, "sdnswitchip":sdnswitchip, "status": status, "check":"T"}
        dic_sdnswitch.append(d_sdnswitch)
        return "FIRST",json.dumps(d_sdnswitch)
    
    def checkPing(self, addr):
        response = os.system("ping -c 1 " + addr)
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        return result

    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("SdnSwich send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)

# class CollectorProcess(object):
#     global dic_process
#     dic_process = []
#     d_utmover = collections.OrderedDict()   
#         
#     def __init__(self):
#         self.org_dir= os.getcwd()
#         self.init_config_from_dict_config_file()
# 
# 
#     def init_config_from_dict_config_file(self):
#         self.collect = CollectorProcessStateInfoSql()
#         
#         aes = AESCipher(AESCipherKey['key'])
#         self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
#         self.dbdatabase = aes.decrypt(db_conn_info['db'])
#         self.dbuser = aes.decrypt(db_conn_info['id'])
#         self.dbpassword = aes.decrypt(db_conn_info['pw'])
#         self.dbhost = aes.decrypt(db_conn_info['db_host'])
#         self.dbport = aes.decrypt(db_conn_info['port'])
# 
#         conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
#         cur = conn.cursor()   
#         query = self.collect.getopenstackinfra(orgseq)
#         cur.execute(query)
#         rows = cur.fetchall()
#         columns = [desc[0] for desc in cur.description]
#         processlist = []
#         for row in rows:
#             processlist.append(dict(zip(columns, row)))
# 
#         self.host = processlist[0]["infraprops"]["mgmtvms"][0]["host"]    
#         self.userid = processlist[0]["infraprops"]["mgmtvms"][0]["userid"]
#         self.password = processlist[0]["infraprops"]["mgmtvms"][0]["password"]
# 
#                 
#     def initdicCollectorProcess(self):
#         try:
#             log.info("#"*100)
#             conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
#             conn.autocommit = True
#             cur = conn.cursor()
#             myquery = self.collect.selectCollectorProcessStateInfo(orgseq)
#             cur.execute(myquery)
#             rows = cur.fetchall()
#             columns = [desc[0] for desc in cur.description]
#             for row in rows:
#                 d_utmover = dict(zip(columns, row))
#                 dic_process.append(d_utmover)
#             log.info("initdicProcess %s" %len(dic_process))
#         except psycopg2.DatabaseError, e:
# #                 print 'Error %s' % e
#             log.error('[%s]Database ExceptionError %s' %e)
#         except Exception, e:
#             log.error('[%s]Exception Error %s' %e)
#                 
#     def run(self, invokeTime):
#         try:
#             self.invokeTime = invokeTime
#             conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
#             cur = conn.cursor()          
#             myquery = self.collect.getProcessNameinfo(orgseq)
#             cur.execute(myquery)
#             rows = cur.fetchall()
#             columns = [desc[0] for desc in cur.description]
#             processlist = []
#             for row in rows:
#                 processlist.append(dict(zip(columns, row)))
#             log.debug("processlist --> %s"%processlist)
#             for process in processlist:
#                 print json.dumps(process)
#                 process_name = process["process_name"]
#                 process_status, cmd, out = self.getProcessCheck(process_name)
#                 log.debug(" ")
#                 log.debug("getProcessCheck result -> %s"%process_status)
#                 log.debug("getProcessCheck cmd -> %s"%cmd)
#                 log.debug("getProcessCheck out -> %s"%out)
#                 
#                 if out == []:
#                     alarm_type = "1"
#                     myquery = self.collect.insertCollectorProcessStateInfo(self.invokeTime, orgseq, process_name, process_status, cmd, "")
#                 else:
#                     alarm_type = "0"
#                     myquery = self.collect.insertCollectorProcessStateInfo(self.invokeTime, orgseq, process_name, process_status, cmd, out[0]) 
#                 
#                 
#                 cur.execute(myquery)
#                 result_collector, checkData = self.checkColectorProcessStatus(self.invokeTime, orgseq, process_name, process_status)
#                 log.debug("[%s] process_name %s process_status %s"%(result_collector, process_name, process_status))
#                 if (result_collector <> "EXIST" or alarm_type == "1")  :
#                     if result_collector == "CHANGE":
#                         data_type = "C"
#                     elif result_collector == "FIRST":
#                         data_type = "I"
#                     else:
#                         data_type = "N"
# #                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
# #                     cur.execute(changequery)
#                     sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":9, "collect_data":checkData}
#                     sendData_json = json.dumps(sendData)
#                     self.sendBroadcasting(sendData_json)                       
#             log.info("initdicProcess %s" %len(dic_process))           
#             data_len = len(dic_process)
#             for index in range(0, data_len) :
#                 if dic_process[data_len-(index + 1)]["check"] == "F":
#                     send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":9, "collect_data":dic_process[data_len-(index + 1)]}
#                     send_delete_data_json = json.dumps(send_delete_data)
#                     dic_process.pop(data_len-(index + 1))
# #                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_process[data_len-(index + 1)]))
# #                     cur.execute(deletedequery)                        
#                     self.sendBroadcasting(send_delete_data_json)
#             self.checkResetCustomer()            
#             conn.commit()
#         except Exception, e:
#             log.error("Process Monitor exception error %s"%e.message)
#             if conn is not None:
#                 conn.rollback()
#         finally:
#             if conn is not None:
#                 conn.close()
# 
#     def getProcessCheck(self, process_name):      
# 
#         cmd = "ps -ef |grep %s"%process_name+" |grep -v grep"
#         result, cmd, out = self.sendCommand(cmd, process_name)
#         if result == True:
#             result = "active"
#         else:
#             result = "deactive"
#         return result, cmd, out
# 
#     def sendCommand(self, cmd, searchText):
#         client = paramiko.SSHClient()
#         try:
#             out = []
#             client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#             client.connect(hostname=self.host, username=self.userid, password=self.password, timeout=3)
#             stdin, stdout, stderr = client.exec_command(cmd)
#             result = False
#             for line in stdout:
#                 if searchText in line:
#                     result = True
#                     print "collect line %s"%line
#                     out.append(line.replace('\n','').replace('       ',''))
#                     break
#             log.debug("result %s" %result)
#             
#         except Exception, e:
#             log.error("sendCommand[%s] error %s" %(self.host,str(e)))
#             sys.exit()
#         finally:
#             client.close()
#             return result, cmd, out
#             
#     def checkResetCustomer(self):
#         for row in dic_process:
#             row["check"] = "F"
#             
#     def checkColectorProcessStatus(self, create_time, orgseq, process_name, process_status):
#         count = 0
#         for row in dic_process:
#             if row["orgseq"] == orgseq and row["process_name"] == process_name and row["process_status"] == process_status:
#                 row["check"] = "T"
#                 return "EXIST", json.dumps(dic_process[count])
#             else:
#                 row["process_status"] = process_status
# 
#                 return "CHANGE",json.dumps(dic_process[count])
#             count = count + 1
# 
#         d_process = {"create_time": create_time,"orgseq":orgseq, "process_name":process_name, "process_status":process_status, "check":"T"}
#         dic_process.append(d_process)
#         return "FIRST",json.dumps(d_process)
#     
#     def sendBroadcasting(self, sendData):
#         try:
#             print "#"*100
#             print self.WS_URL            
# #             cert_path=os.path.join(os.path.dirname(__file__), "cert")
#             settings = dict(
# #                 ssl_options = {
# #                     "certfile": os.path.join(cert_path, "server.crt"),
# #                     "keyfile": os.path.join(cert_path, "server.key")
# #                 }
#                 sslopt={"cert_reqs": ssl.CERT_NONE}
#             )            
#             ws = create_connection(self.WS_URL, **settings)
#             ws.send(sendData)
#             result = ws.recv()
#             log.debug("#"*100)
#             log.debug("Process Monitoring send(received) data:%s", result)
# #             log.debug("#"*80)
#             ws.close()
#         except Exception, e:
#             log.error("Socket Error %s" %e)
#                                    
sched = BlockingScheduler()

aes = AESCipher(AESCipherKey['key'])

dbdatabase = aes.decrypt(db_conn_info['db'])
dbuser = aes.decrypt(db_conn_info['id'])
dbpassword = aes.decrypt(db_conn_info['pw'])
dbhost = aes.decrypt(db_conn_info['db_host'])
dbport = aes.decrypt(db_conn_info['port'])

sql = Provisioningsql()
conn = psycopg2.connect(database=dbdatabase, user=dbuser, password=dbpassword, host=dbhost, port=dbport)
conn.autocommit = True
cur = conn.cursor()
query = sql.getopenstackinfra(orgseq)
cur.execute(query)
rows = cur.fetchone()

if "hostcomponentstatusmonitoring" in rows[0]["process_period"]:
    hostperiod = rows[0]["process_period"]["hostcomponentstatusmonitoring"]
else:
    hostperiod = "10"

if "customerstatusmonitoring" in rows[0]["process_period"]:
    customerperiod = rows[0]["process_period"]["customerstatusmonitoring"]
else:
    customerperiod = "10"    

if "hwutmstatusmonitoring" in rows[0]["process_period"]:
    hwutmperiod = rows[0]["process_period"]["hwutmstatusmonitoring"]
else:
    hwutmperiod = "10"   
        
if "cloudserverstatusmonitoring" in rows[0]["process_period"]:
    cloudperiod = rows[0]["process_period"]["cloudserverstatusmonitoring"]
else:
    cloudperiod = "10"   

if "sdnswitchstatusmonitoring" in rows[0]["process_period"]:
    sdnswperiod = rows[0]["process_period"]["sdnswitchstatusmonitoring"]
else:
    sdnswperiod = "10"       

# if "collectorstatusmonitoring" in rows[0]["process_period"]:
#     collectperiod = rows[0]["process_period"]["collectorstatusmonitoring"]
# else:
#     collectperiod = "10"   
        
@sched.scheduled_job('cron', second=hostperiod)
def openstack_service_monitor_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("openstack_monitor_job() start. %s period[%s]" %(starttime,hostperiod))
    log.debug("dicHostSize[%d]"%len(dic_host))
    log.debug("-"*50)
    OpenStackModuleMonitor().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicHostSize[%d]"%len(dic_host))
    log.debug("openstack_monitor_job() end. %s" %endtime)
    log.debug("-"*50)
        
#@sched.scheduled_job('cron', second=customerperiod)
#def customer_server_list_job():
#    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#    log.debug("-"*50)
#    log.debug("customer_server_list_job start. %s period[%s]" %(starttime,customerperiod))
#    log.debug("dicCustomerSize[%d]"%len(dic_customer))
#    log.debug("-"*50)
#    CustomerServerMonitor().run(starttime)
#    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#    log.debug("-"*50)
#    log.debug("dicCustomerSize[%d]"%len(dic_customer))
#    log.debug("customer_server_list_job end. %s" %endtime)
#    log.debug("-"*50)
                         
@sched.scheduled_job('cron', second=hwutmperiod)
def sdn_appliance_list_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("sdn_appliance_list_job start. %s period[%s]" %(starttime, hwutmperiod))
    log.debug("dicSDNSize[%d]"%len(dic_sdn))
    log.debug("-"*50)
    SdnApplianceMonitor().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicSDNSize[%d]"%len(dic_sdn))
    log.debug("sdn_appliance_list_job end. %s" %endtime)
    log.debug("-"*50)
                   
@sched.scheduled_job('cron', second=cloudperiod)
def cloud_host_list_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("cloud_host_list_job start. %s period[%s]" %(starttime, cloudperiod))
    log.debug("dicCloudSize[%d]"%len(dic_cloud))
    log.debug("-"*50)
    CloudServerMonitor().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicCloudSize[%d]"%len(dic_cloud))
    log.debug("cloud_host_list_job end. %s" %endtime)
    log.debug("-"*50)
                     
# @sched.scheduled_job('cron', second=sdnswperiod)
# def SdnSwitch_list_job():
#     starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     log.debug("-"*50)
#     log.debug("SdnSwitch_list_job start. %s period[%s]" %(starttime,sdnswperiod))
#     log.debug("dicSdnSwitchSize[%d]"%len(dic_sdnswitch))
#     log.debug("-"*50)
#     SdnSwitchMonitor().run(starttime)
#     endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     log.debug("-"*50)
#     log.debug("dicSdnSwitchSize[%d]"%len(dic_sdnswitch))
#     log.debug("SdnSwitch_list_job end. %s" %endtime)
#     log.debug("-"*50)
     
# @sched.scheduled_job('cron', second=collectperiod)
# def CollectorProcess_list_job():
#     starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     log.debug("-"*50)
#     log.debug("CollectorProcess_list_job start. %s period[%s]" %(starttime,collectperiod))
#     log.debug("diprocessSize[%d]"%len(dic_process))
#     log.debug("-"*50)
#     CollectorProcess().run(starttime)
#     endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
#     log.debug("-"*50)
#     log.debug("diprocessSize[%d]"%len(dic_process))
#     log.debug("CollectorProcess_list_job end. %s" %endtime)
#     log.debug("-"*50)
               
def main():
    log.info("Scheduler Start")
    OpenStackModuleMonitor().initdicHost()  
#    CustomerServerMonitor().initdicCustomer()
    SdnApplianceMonitor().initdicSdnAppliance()
    CloudServerMonitor().initdicCloud()
#     SdnSwitchMonitor().initdicSdnSwitch()
#     CollectorProcess().initdicCollectorProcess()
    sched.start()

if __name__ == '__main__':
    main()
