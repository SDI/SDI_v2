#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 11.
@summary:
@author: umisue
'''

from helper.psycopg_helper import PsycopgHelper
from sql.provisioningsql import Provisioningsql

class ProvisionTrace(PsycopgHelper):

    def settrace(self, provisionseq, userid, managegroup, logstep, stepmsg):

        trace = {"cmdtype": "", "managegroup": "", "managetype": 0, "provisionseq":"", "userid":"", "stepmsg":"", "indent": 0, "logstep":"", "msglevel": 0, "stepmsgcmd": "","stepmsgdetails": "", "svctype": None, "resourceid": ""}

        provisionsql = Provisioningsql()

        trace["provisionseq"] = provisionseq
        trace["userid"] = userid

        if managegroup == "0":
            trace["cmdtype"] = "C"
            trace["managegroup"] = "0"
            trace["managetype"] = 0
            if logstep == "0":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "0"
                trace["stepmsg"] = u"서비스 설치 시작"
                myquery = provisionsql.settraceforprovision(trace)
            self.execute_set(myquery)

        if managegroup == "1":
            trace["cmdtype"] = "C"
            trace["managegroup"] = "1"
            trace["managetype"] = 0
            if logstep == "1":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "1"
                trace["stepmsg"] = u"계정확인"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "1.1":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "1.1"
                trace["stepmsg"] = u"계정생성"
                myquery = provisionsql.settraceforprovision(trace)
            elif logstep == "1.2":
                trace["indent"] = 1
                trace["msglevel"] = 1
                trace["logstep"] = "1.2"
                trace["stepmsg"] = u"계정생성 완료"
                myquery = provisionsql.settraceforprovision(trace)
            elif logstep == "1.3":
                trace["indent"] = 1
                trace["msglevel"] = 2
                trace["logstep"] = "1.3"
                trace["stepmsg"] = u"계정생성 실패"
                myquery = provisionsql.settraceforprovision(trace)
            elif logstep == "1.4":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "1.4"
                trace["stepmsg"] = u"계정검사"
                myquery = provisionsql.settraceforprovision(trace)
            elif logstep == "1.5":
                trace["indent"] = 1
                trace["msglevel"] = 1
                trace["logstep"] = "1.5"
                trace["stepmsg"] = u"계정검사 완료"
                myquery = provisionsql.settraceforprovision(trace)

            self.execute_set(myquery)

        if managegroup == "2":
            trace["cmdtype"] = "C"
            trace["managegroup"] = "2"
            trace["managetype"] = 0
            if logstep == "2":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "2"
                trace["stepmsg"] = u"그린네트워크생성"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "2.1":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "2.1"
                trace["stepmsg"] = u"그린네트워크생성요청"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "2.1.1":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "2.1.1"
                trace["stepmsg"] = u"그린네트워크생성 완료"
                myquery = provisionsql.settraceforprovision(trace)
                self.execute_set(myquery)
            if logstep == "2.1.2":
                trace["indent"] = 2
                trace["msglevel"] = 2
                trace["logstep"] = "2.1.2"
                trace["stepmsg"] = u"그린네트워크생성 실패"
                myquery = provisionsql.settraceforprovision(trace)
                self.execute_set(myquery)
            if logstep == "2.2":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "2.2"
                trace["stepmsg"] = u"그린서브네트워크생성요청"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "2.2.1":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "2.2.1"
                trace["stepmsg"] = u"그린서브네트워크생성 완료"
                trace["svctype"] = 10
                myquery = provisionsql.settraceforprovision(trace)
                trace["svctype"] = None
            if logstep == "2.2.2":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "2.2.2"
                trace["stepmsg"] = u"그린서브네트워크생성 실패"
                myquery = provisionsql.settraceforprovision(trace)
            self.execute_set(myquery)

        if managegroup == "3":
            trace["cmdtype"] = "C"
            trace["managegroup"] = "3"
            trace["managetype"] = 0
            if logstep == "3":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "3"
                trace["stepmsg"] = u"오렌지네트워크생성"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "3.1":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "3.1"
                trace["stepmsg"] = u"오렌지네트워크생성요청"
                myquery = provisionsql.settraceforprovision(trace)

            if logstep == "3.1.1":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "3.1.1"
                trace["stepmsg"] = u"오렌지네트워크생성 완료"
                myquery = provisionsql.settraceforprovision(trace)

            if logstep == "3.2":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "3.2"
                trace["stepmsg"] = u"오렌지서브네트워크생성요청"
                myquery = provisionsql.settraceforprovision(trace)

            if logstep == "3.2.1":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "3.2.1"
                trace["stepmsg"] = u"오렌지서브네트워크생성 완료"
                trace["svctype"] = 20
                myquery = provisionsql.settraceforprovision(trace)
                trace["svctype"] = None

            self.execute_set(myquery)

        if managegroup == "4":
            trace["cmdtype"] = "C"
            trace["managegroup"] = "4"
            trace["managetype"] = 0

            if logstep == "4":
                trace["managegroup"] = "4"
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "4"
                trace["stepmsg"] = u"인스턴스생성"
                myquery = provisionsql.settraceforprovision(trace)

            if logstep == "4.1":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "4.1"
                trace["stepmsg"] = u"인스턴스생성요청"
                trace["svctype"] = 1
                myquery = provisionsql.settraceforprovision(trace)

            if logstep == "4.2":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "4.2"
                trace["stepmsg"] = u"인스턴스생성완료"
                trace["svctype"] = 1
                myquery = provisionsql.settraceforprovision(trace)
                trace["svctype"] = None

            self.execute_set(myquery)

        if managegroup == "50":
            trace["cmdtype"] = "D"
            trace["managegroup"] = "50"
            trace["managetype"] = 0
            if logstep == "50":
                trace["indent"] = 0
                trace["msglevel"] = 0
                trace["logstep"] = "50"
                trace["stepmsg"] = u"서비스 설치 실패"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1":
                trace["indent"] = 1
                trace["msglevel"] = 0
                trace["logstep"] = "50.1"
                trace["stepmsg"] = "서비스 삭제 시작"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.1":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "50.1.1"
                trace["stepmsg"] = "인스턴스 삭제"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.2":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "50.1.2"
                trace["stepmsg"] = "그린서브네트워크 삭제"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.3":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "50.1.3"
                trace["stepmsg"] = "오렌지서브네트워크 삭제"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.4":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "50.1.4"
                trace["stepmsg"] = "그린네트워크 삭제"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.5":
                trace["indent"] = 2
                trace["msglevel"] = 1
                trace["logstep"] = "50.1.5"
                trace["stepmsg"] = "오렌지네트워크 삭제"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.6":
                trace["indent"] = 2
                trace["msglevel"] = 0
                trace["logstep"] = "50.1.6"
                trace["stepmsg"] = "서비스 삭제 완료"
                myquery = provisionsql.settraceforprovision(trace)
            if logstep == "50.1.7":
                trace["indent"] = 2
                trace["msglevel"] = 0
                trace["logstep"] = "50.1.7"
                trace["stepmsg"] = "서비스 삭제 완료 실패"
                myquery = provisionsql.settraceforprovision(trace)
            self.execute_set(myquery)



