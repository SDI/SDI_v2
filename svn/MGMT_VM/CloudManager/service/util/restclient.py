# -*- coding: utf8 -*-
'''
Created on 2015. 1. 13.

@author: 진수
'''
import json

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders


class SoapClient():
    def sendData(self, url, httpMethod, reqBody):
        soapUrl = "http://211.224.204.158:8080/soap2"
        print soapUrl+url
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        if httpMethod == "post":
            request = HTTPRequest(url=soapUrl+url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=1)
        elif httpMethod == "delete" :
            request = HTTPRequest(url=soapUrl+url+reqBody, headers=h, method=httpMethod.upper(), body=None, request_timeout=3)
            print url
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            print response.body
            return response.body
        except httpclient.HTTPError as e:
#             print("Error: " + str(e))
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
#             print("Error: " + str(e))
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result


if __name__ == '__main__':
    soapclient = SoapClient()
    soapclient.sendData("http://www.google.com/", "GET", None)

#     http_client = httpclient.HTTPClient()
#     try:
#         response = http_client.fetch("http://www.google.com/")
#         print response.body
#     except httpclient.HTTPError as e:
#         # HTTPError is raised for non-200 responses; the response
#         # can be found in e.response.
#         print("Error: " + str(e))
#     except Exception as e:
#         # Other errors are possible, such as IOError.
#         print("Error: " + str(e))
#     http_client.close()