#!/bin/bash

for pid in $(ps -ef | grep nssc.py | grep -v grep | awk '{print $2}') 
do
	kill -9 $pid
done

