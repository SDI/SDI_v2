#!/bin/bash

for log in $(ls -alt ../log/*.log | awk '{print $9}')
do
	tail -fn 200 ../log/$log
	break
done

