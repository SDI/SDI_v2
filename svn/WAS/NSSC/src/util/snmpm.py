'''
Created on 2015. 4. 22.

@author: ohhara
'''

from pysnmp.entity.rfc3413.oneliner import cmdgen
from pysnmp.proto import rfc1902

import logm


def sendGetOneMsg(ip, comm, oid, port=161):
    logm.info( "SNMP Get: " + str(ip) + ":" + str(port) + ", " + "ID=" + str(comm) + ":" + str(oid))
    
    try:
        errorIndication, errorStatus, errorIndex, varBinds = cmdgen.CommandGenerator().getCmd(
            cmdgen.CommunityData( comm ),
            cmdgen.UdpTransportTarget( ( ip, port), timeout=1.5 ),
            oid,
        )

        rs = []
        # Check for errors and print out results
        if errorIndication:
            print(errorIndication)
        else:
            if errorStatus:
                logm.info('%s at %s' % (
                    errorStatus.prettyPrint(),
                    errorIndex and varBinds[int(errorIndex)-1][0] or '?'
                    )
                )
            else:
                for name, val in varBinds:
                    logm.info('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                    rs.append( ( str(name), str(val) ) )
                    return rs
    except Exception, e:
        logm.exc(e, 'SNMP_GetOne')
    
    return None


def sendSetOneMsg(ip, comm, oid, val, port=161):
    logm.info( "SNMP Set: " + str(ip) + ":" + str(port) + ", " + "ID=" + str(comm) + ":" + str(oid) + ", Val=" + str(val))
    
    try:
        errorIndication, errorStatus, errorIndex, varBinds = cmdgen.CommandGenerator().setCmd(
            cmdgen.CommunityData( comm ),
            cmdgen.UdpTransportTarget( (ip, port) ),
            (oid, rfc1902.Integer32(val)),
        )
        
        rs = []
        # Check for errors and print out results
        if errorIndication:
            print(errorIndication)
        else:
            if errorStatus:
                logm.info('%s at %s' % (
                    errorStatus.prettyPrint(),
                    errorIndex and varBinds[int(errorIndex)-1] or '?'
                    )
                )
            else:
                for name, val in varBinds:
                    logm.info('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                    rs.append( ( str(name), str(val) ) )
                    return rs
    
    except Exception, e:
        logm.exc(e, 'SNMP_SetOne')
    
    return None

def sendGetBulkMsg(ip, comm, oid, port=161):
    logm.info( "SNMP Get: " + str(ip) + ":" + str(port) + ", " + "ID=" + str(comm) + ":" + str(oid))
    
    errorIndication, errorStatus, errorIndex, varBinds = cmdgen.CommandGenerator().bulkCmd(
        cmdgen.CommunityData( comm ), 
        cmdgen.UdpTransportTarget( ( ip, port) ),
        0, 25,
        oid,
    )
    
    logm.debug(errorIndication)
    logm.debug(errorStatus)
    logm.debug(errorIndex)
    logm.debug(varBinds)
    
    rs = {}
    
    # Check for errors and print out results
    if errorIndication:
        print(errorIndication)
    else:
        if errorStatus:
            logm.info('%s at %s' % (
                errorStatus.prettyPrint(),
                errorIndex and varBinds[int(errorIndex)-1][0] or '?'
                )
            )
        else:
            for name, val in varBinds:
                logm.info('%s = %s' % (name.prettyPrint(), val.prettyPrint()))
                rs[str(name)] = str(val)
                logm.debug(val)
            return rs
    
    return True


