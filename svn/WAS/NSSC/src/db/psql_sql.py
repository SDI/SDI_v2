# -*- coding: utf-8 -*-
'''
Created on 2015. 4. 23.

@author: ohhara
'''

import json

def inqSwList(showID=0):
    idSql=""
    if showID == 1 :
        idSql = "sw_id, "
    
    sql = "SELECT " + idSql + "sw_name, sw_rcode, sw_mgmt_ip, sw_reg_dttm " \
        + "FROM orchestrator.tb_switch_info"
    param = []
    return { 'sql':sql, 'param': param }

def inqSwListByRCode(rcode, showID=0):
    idSql=""
    if showID == 1 :
        idSql = "sw_id, "

    sql = "SELECT " + idSql + "sw_name, sw_rcode, sw_mgmt_ip, sw_reg_dttm "\
        + "FROM orchestrator.tb_switch_info WHERE sw_rcode=upper(%s)"
    param = [ rcode ]
    return { 'sql':sql, 'param': param }

def inqSwListByName(name, showID=0):
    idSql=""
    if showID == 1 :
        idSql = "sw_id, "

    sql = "SELECT " + idSql + "sw_name, sw_rcode, sw_mgmt_ip, sw_reg_dttm "\
        + "FROM orchestrator.tb_switch_info WHERE sw_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }

def getSwID( name ):
    sql = "SELECT sw_id FROM orchestrator.tb_switch_info WHERE sw_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }

def getSwIP( swid ):
    sql = "SELECT sw_mgmt_ip FROM orchestrator.tb_switch_info WHERE sw_seq=%s"
    param = [ swid ]
    return { 'sql':sql, 'param': param }

def chkSW( name, ip='' ):
    sql = "SELECT * FROM "\
            + "(SELECT count(sw_id) name_cnt FROM orchestrator.tb_switch_info WHERE sw_name=%s) a, "\
            + "(SELECT count(sw_id) ip_cnt FROM orchestrator.tb_switch_info WHERE sw_mgmt_ip=%s) b"
    param = [ name, ip ]
    return { 'sql':sql, 'param': param }

def addSW( name, ip, rcode ):
    sql = "INSERT INTO orchestrator.tb_switch_info( sw_rcode, sw_name, sw_mgmt_ip ) "\
        + "VALUES(upper(%s), %s, %s)"
    param = [ rcode, name, ip ]
    return { 'sql':sql, 'param': param }

def modSW( swID, new_name, ip, rcode ):
    sql = "UPDATE orchestrator.tb_switch_info SET "
    param = []
    isAdded = False
    
    if new_name != None and new_name != '' :
        sql = sql + "sw_name=%s"
        param.append(new_name)
        isAdded = True
    
    if rcode != None and rcode != '' :
        if isAdded == True :
            sql = sql + ", "
        sql = sql + "sw_rcode=upper(%s)"
        param.append(rcode)
        isAdded = True

    if ip != None and ip != '' :
        if isAdded == True :
            sql = sql + ", "
        sql = sql + "sw_mgmt_ip=%s"
        param.append(ip)
    
    sql = sql + " WHERE sw_id=%s"
    param.append(swID)
    
    return { 'sql':sql, 'param': param }

def delSW(name):
    sql = "DELETE FROM orchestrator.tb_switch_info WHERE sw_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }



def inqUTMList(showID=0):
    idSql=""
    if showID == 1 :
        idSql = "utm_id, "

    sql = "SELECT " + idSql + "utm_name, utm_mgmt_ip, utm_type, utm_reg_dttm "\
        + "FROM orchestrator.tb_utm_info"
    param = []
    return { 'sql':sql, 'param': param }

def inqUTMListByType(utmType, showID=0):
    idSql=""
    if showID == 1 :
        idSql = "utm_id, "

    sql = "SELECT " + idSql + "utm_name, utm_mgmt_ip, utm_type, utm_reg_dttm "\
        + "FROM orchestrator.tb_utm_info WHERE utm_type=upper(%s)"
    param = [ utmType ]
    return { 'sql':sql, 'param': param }

def inqUTMListByName(name, showID=0):
    idSql=""
    if showID == 1 :
        idSql = "utm_id, "

    sql = "SELECT " + idSql + "utm_name, utm_mgmt_ip, utm_type, utm_reg_dttm "\
        + "FROM orchestrator.tb_utm_info WHERE utm_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }

def getUtmID( name ):
    sql = "SELECT utm_id FROM orchestrator.tb_utm_info WHERE utm_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }

def getUtmIdByIP( ip ):
    sql = "SELECT utm_seq, utm_name FROM orchestrator.tb_utm_info WHERE utm_mgmt_ip=%s"
    param = [ ip ]
    return { 'sql':sql, 'param': param }

def chkUTM( name, ip='' ):
    sql = "SELECT * FROM "\
            + "(SELECT count(utm_id) name_cnt FROM orchestrator.tb_utm_info WHERE utm_name=%s) a, "\
            + "(SELECT count(utm_id) ip_cnt FROM orchestrator.tb_utm_info WHERE utm_mgmt_ip=%s) b"
    param = [ name, ip ]
    return { 'sql':sql, 'param': param }

def addUTM( name, ip, utmType ):
    sql = "INSERT INTO orchestrator.tb_utm_info(utm_name, utm_mgmt_ip, utm_type) "\
        + "VALUES(%s, %s, upper(%s))"
    param = [ name, ip, utmType ]
    return { 'sql':sql, 'param': param }

def modUTM( utmID, new_name, ip, utmType ):
    sql = "UPDATE orchestrator.tb_utm_info SET "
    param = []
    isAdded = False
    
    if new_name != None and new_name != '' :
        sql = sql + "utm_name=%s"
        param.append(new_name)
        isAdded = True
    
    if ip != None and ip != '' :
        if isAdded == True :
            sql = sql + ", "
        sql = sql + "utm_mgmt_ip=%s"
        param.append(ip)
        isAdded = True

    if utmType != None and utmType != '' :
        if isAdded == True :
            sql = sql + ", "
        sql = sql + "utm_type=upper(%s)"
        param.append(utmType)
    
    sql = sql + " WHERE utm_id=%s"
    param.append(utmID)
    
    return { 'sql':sql, 'param': param }

def delUTM( name ):
    sql = "DELETE FROM orchestrator.tb_utm_info WHERE utm_name=%s"
    param = [ name ]
    return { 'sql':sql, 'param': param }



def inqConn(showID=0):
    utmIdSql=""
    swIdSql=""
    if showID == 1 :
        utmIdSql="conn.utm_id, "
        swIdSql="conn.sw_id, "
    sql = "SELECT " + utmIdSql + "utm.utm_name, utm.utm_mgmt_ip, utm.utm_type, conn.utm_link_type, "\
            + swIdSql + "sw.sw_name, conn.sw_port, sw.sw_rcode, conn.utm_conn_reg_dttm "\
        + "FROM orchestrator.tb_utm_info utm, orchestrator.tb_switch_info sw, orchestrator.tb_utm_conn_info conn "\
        + "WHERE conn.utm_id=utm.utm_id and conn.sw_id=sw.sw_id"
    param = []
    return { 'sql':sql, 'param': param }

def inqConnByRCode( rCode, showID=0 ):
    utmIdSql=""
    swIdSql=""
    if showID == 1 :
        utmIdSql="conn.utm_id, "
        swIdSql="conn.sw_id, "
    sql = "SELECT " + utmIdSql + "utm.utm_name, utm.utm_mgmt_ip, utm.utm_type, conn.utm_link_type, "\
            + swIdSql + "sw.sw_name, conn.sw_port, sw.sw_rcode, conn.utm_conn_reg_dttm "\
        + "FROM orchestrator.tb_utm_info utm, orchestrator.tb_switch_info sw, orchestrator.tb_utm_conn_info conn "\
        + "WHERE sw.sw_rcode=upper(%s) and conn.utm_id=utm.utm_id and conn.sw_id=sw.sw_id"
    param = [ rCode ]
    return { 'sql':sql, 'param': param }

def isActiveUtm( ip ):
    sql = "SELECT count(ha.utm_seq) FROM orchestrator.tb_utm_info utm, orchestrator.tb_ha_info ha "\
        + "WHERE ha.utm_seq=utm.utm_seq AND ha.ha_state='Active' AND utm.utm_mgmt_ip=%s"
    param = [ ip ]
    return { 'sql':sql, 'param': param }

def chkConnItem( utmName, swName ):
    sql = 'SELECT * '\
        + 'FROM '\
            + '(SELECT count(utm_id) u_cnt FROM orchestrator.tb_utm_info WHERE utm_name=%s) utm, '\
            + '(SELECT count(sw_id) sw_cnt FROM orchestrator.tb_switch_info WHERE sw_name=%s) sw '
    param = [ utmName, swName ]
    return { 'sql':sql, 'param': param }

def getConns( utmID ):
    sql = "SELECT sw.sw_name, sw.sw_mgmt_ip, conn.sw_port, utm.utm_seq, utm.utm_name, conn.utm_link_type "\
        + "FROM orchestrator.tb_utm_conn_info conn, "\
            + "orchestrator.tb_switch_info sw, "\
            + "orchestrator.tb_utm_info utm "\
        + "WHERE conn.utm_seq=%s AND utm.utm_seq=%s AND conn.sw_seq=sw.sw_seq"
    param = [ utmID, utmID ]
    return { 'sql':sql, 'param': param }   

def chkConn( name, conn='' ):
    sql = "SELECT count(conn.utm_id) FROM " \
            + "orchestrator.tb_utm_conn_info conn, "\
            + "orchestrator.tb_utm_info utm "\
        + "WHERE conn.utm_id=utm.utm_id AND utm.utm_name=%s AND conn.utm_link_type=upper(%s)"
    param = [ name, conn ]
    return { 'sql':sql, 'param': param }

def addConn( utmName, utmLink, swName, swPort ):
    sql = "INSERT INTO orchestrator.tb_utm_conn_info(utm_id, utm_link_type, sw_id, sw_port) "\
            + "SELECT utm.utm_id, val.link, sw.sw_id, val.port "\
                + "FROM (select utm_id from orchestrator.tb_utm_info where utm_name=%s) utm, "\
                     + "(select sw_id from orchestrator.tb_switch_info where sw_name=%s) sw, "\
                     + "(select upper(%s) as link, %s as port) val"
    param = [ utmName, swName, utmLink, int(swPort) ]
    return { 'sql':sql, 'param': param }

def modConn( utmName, utmLink, newSwName, newSwPort ):
    sql = "UPDATE orchestrator.tb_utm_conn_info SET "
    param = []
    isAdded = False
    
    if newSwName != None and newSwName != '' :
        sql = sql + "sw_id=(SELECT sw_id FROM orchestrator.tb_switch_info WHERE sw_name=%s)"
        param.append(newSwName)
        isAdded = True
    
    if newSwPort != None and newSwPort != '' :
        if isAdded == True :
            sql = sql + ", "
        sql = sql + "sw_port=%s"
        param.append(int(newSwPort))
        isAdded = True
    
    sql = sql + " WHERE utm_id=(SELECT utm_id FROM orchestrator.tb_utm_info WHERE utm_name=%s) and utm_link_type=upper(%s)"
    param.append(utmName)
    param.append(utmLink)

    return { 'sql':sql, 'param': param }

def delConn( utmName, utmLink='ALL'):
    linkSql=""
    if str(utmLink).upper() != 'ALL' :
        linkSql= "AND utm_link_type='" + utmLink + "'"
    sql = "DELETE FROM orchestrator.tb_utm_conn_info "\
        + "WHERE utm_id = ( SELECT utm_id FROM orchestrator.tb_utm_info "\
                         + "WHERE utm_name=%s" + linkSql + ")"
    param = [ utmName ]
    return { 'sql':sql, 'param': param }


def inqHaList(showID=0):
    ## 그룹, utm, utmType, 백업종류, status, 스위치, port, 국사
    utmIdSql=""
    swIdSql=""
    if showID == 1 :
        utmIdSql="hu.utm_id, "
        swIdSql="cs.sw_id, "
    
    sql = "SELECT hu.ha_gname, " + utmIdSql + "hu.utm_name, hu.utm_type, hu.is_primary, hu.ha_status, "\
            + swIdSql + "cs.sw_name, cs.sw_port, cs.sw_rcode, hu.ha_reg_dttm "\
        + "FROM (SELECT h.ha_gname , h.utm_id, u.utm_name, u.utm_type, h.is_primary, h.ha_status, h.ha_reg_dttm "\
            + "FROM orchestrator.tb_ha_info h, orchestrator.tb_utm_info u WHERE h.utm_id=u.utm_id ) hu "\
        + "LEFT OUTER JOIN "\
            + "(SELECT c.utm_id, c.sw_id, s.sw_name, c.sw_port, s.sw_rcode "\
            + "FROM orchestrator.tb_utm_conn_info c, orchestrator.tb_switch_info s WHERE c.sw_id=s.sw_id) cs "\
        + "ON hu.utm_id = cs.utm_id"
    param = []
    return { 'sql':sql, 'param': param }

def inqHaListByName(haName, showID=0):
    ## 그룹, utm, utmType, 백업종류, status, 스위치, port, 국사
    utmIdSql=""
    swIdSql=""
    if showID == 1 :
        utmIdSql="hu.utm_id, "
        swIdSql="cs.sw_id, "
    
    sql = "SELECT hu.ha_gname, " + utmIdSql + "hu.utm_name, hu.utm_type, hu.is_primary, hu.ha_status, "\
            + swIdSql + "cs.sw_name, cs.sw_port, cs.sw_rcode, hu.ha_reg_dttm "\
        + "FROM (SELECT h.ha_gname , h.utm_id, u.utm_name, u.utm_type, h.is_primary, h.ha_status, h.ha_reg_dttm "\
            + "FROM orchestrator.tb_ha_info h, orchestrator.tb_utm_info u WHERE h.utm_id=u.utm_id ) hu "\
        + "LEFT OUTER JOIN "\
            + "(SELECT c.utm_id, c.sw_id, s.sw_name, c.sw_port, s.sw_rcode "\
            + "FROM orchestrator.tb_utm_conn_info c, orchestrator.tb_switch_info s WHERE c.sw_id=s.sw_id) cs "\
        + "ON hu.utm_id = cs.utm_id "\
        + "WHERE hu.ha_gname=%s"
    param = [haName]
    return { 'sql':sql, 'param': param }

def getHaTargets( utmID ):
    sql = "SELECT utm_seq, utm_name, utm_mgmt_ip FROM orchestrator.tb_utm_info WHERE utm_seq=("\
        + "SELECT utm_seq FROM orchestrator.tb_ha_info "\
        + "WHERE "\
            + "ha_gname=(SELECT ha_gname FROM orchestrator.tb_ha_info WHERE utm_seq=%s) "\
            + "AND utm_seq!=%s AND ha_state='Standby' "\
        + "ORDER BY is_primary DESC, ha_pri LIMIT 1 )"
    param = [ utmID, utmID ]
    return { 'sql':sql, 'param': param }

def chkHA( name, utmList ):
    param = []
    
    sql = "SELECT * FROM (SELECT count(ha_gname) cnt  FROM orchestrator.tb_ha_info WHERE ha_gname=%s) H"
    param.append( name )
    
    i = 0
    for item in utmList :
        sql = sql + ",  (SELECT count(ha.utm_id) cnt  FROM orchestrator.tb_ha_info ha, orchestrator.tb_utm_info utm "
        sql = sql + "WHERE ha.utm_id=utm.utm_id AND utm.utm_name=%s) U" + str(i)
        param.append(item)
        i += 1
    
    return { 'sql':sql, 'param': param }

def addHA( haName, utmName, isPrimary, isActive, haPri=0 ):
    sql = "INSERT INTO orchestrator.tb_ha_info(utm_id, ha_gname, is_primary, ha_status, ha_pri) "\
            + "SELECT utm.utm_id, val.ha_gname, val.is_primary, val.ha_status, val.ha_pri "\
            + "FROM (select utm_id from orchestrator.tb_utm_info where utm_name=%s) utm, "\
            + "(select %s as ha_gname, %s as is_primary, upper(%s) as ha_status, %s as ha_pri) val "
    
    if isActive == True :
        haStatus = "ACT"
    else:
        haStatus = "BACK"
    
    param = [ utmName, haName, isPrimary, haStatus, haPri ]
    return { 'sql':sql, 'param': param }

def modHA( haName, utmName, isPrimary, isActive ):
    if isActive == True :
        haStatus = "ACT"
    else:
        haStatus = "BACK"
    
    sql = "UPDATE orchestrator.tb_ha_info SET is_primary=%s, ha_status=%s"\
        + "WHERE utm_id=(SELECT utm_id FROM orchestrator.tb_utm_info WHERE utm_name=%s) AND ha_gname=%s"
    param = [ isPrimary, haStatus, utmName, haName ]
    return { 'sql':sql, 'param': param }

def saveFailOverResult(utmID, state):
    sql = "UPDATE orchestrator.tb_ha_info SET ha_state=%s WHERE utm_seq=%s"
    param = [ state, utmID ]
    return { 'sql':sql, 'param': param }

def delHA(utmName):
    sql = "DELETE FROM orchestrator.tb_ha_info "\
        + "WHERE utm_id = ( SELECT utm_id FROM orchestrator.tb_utm_info "\
                         + "WHERE utm_name=%s )"
    param = [ utmName ]
    return { 'sql':sql, 'param': param }

def delHaExcept( haName, utmList ):
    sql = "DELETE FROM orchestrator.tb_ha_info WHERE ha_gname=%s"
    param = [ str(haName) ]
    i = 0
    for utm in utmList :
        if i == 0 :
            sql = sql + " AND utm_id != ( SELECT utm_id FROM orchestrator.tb_utm_info WHERE utm_name=%s )"
            param.append( str(utm) )
    return { 'sql':sql, 'param': param }





def inqHis(itemType, sDttm, eDttm):
    sql = "SELECT * FROM orchestrator.tb_his_info "
    param = []
    isAdded = False
    
    if itemType != None and itemType != '' :
        sql = sql + "WHERE item_type=upper(%s)"
        param.append( itemType )
        isAdded = True
    
    if sDttm != None and sDttm != '' :
        if isAdded == True :
            sql = sql + "AND end_dttm>=%s"
        else :
            sql = sql + "WHERE end_dttm>=%s"
        param.append( eDttm )
    
    if eDttm != None and eDttm != '' :
        if isAdded == True :
            sql = sql + "AND end_dttm<=%s"
        else :
            sql = sql + "WHERE end_dttm<=%s"
        param.append( eDttm )
    
    return { 'sql':sql, 'param': param }

def addHis(sDttm, seq, rUser, itemType, itemName, cmdType, cmdPara):
    sql = "INSERT INTO orchestrator.tb_his_info(req_dttm, req_seq, req_user, item_type, item_name, cmd_type, cmd_para) "\
        + "VALUES (%s, %s, %s, upper(%s), %s, upper(%s),  %s)"
    param = [ sDttm, seq, rUser, itemType, itemName, cmdType, json.dumps(cmdPara) ]
    return { 'sql':sql, 'param': param }

def modHis(sDttm, seq, eDttm, cmdResult, errDesc):
    sql = "UPDATE orchestrator.tb_his_info SET end_dttm=%s, cmd_result=upper(%s), err_desc=%s "\
        + "WHERE req_dttm=%s and req_seq=%s"
    param = [ eDttm, cmdResult, errDesc, sDttm, seq ]
    return { 'sql':sql, 'param': param }


