#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''

import json

import tornado.httpserver
import tornado.ioloop
import tornado.options
from tornado.web import asynchronous
import tornado.web

from manager.vimmanager import Openstackapi, VimManager
from service.gomsprovision import GomsProvision


class ReadyHandler(tornado.web.RequestHandler):
    def post(self):
        goms = GomsProvision() 
        reqdata = json.loads(self.request.body)   
        result = goms.getreadyprovisionseq(reqdata)
        print result
        self.write(result)

class CreateHandler(tornado.web.RequestHandler):
    @asynchronous
    def post(self):         
        reqdata = json.loads(self.request.body)
        goms = GomsProvision()
        result = goms.gomsprovision(reqdata)
        self.write(result) 
        self.finish()
        
class DeleteHandler(tornado.web.RequestHandler):
    def delete(self, provisionseq):
        goms = GomsProvision() 
        result = goms.gomsprovisiondelete(provisionseq)
        print result
        self.write(result)    

class VncHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.getvnc(reqdata)
        print result
        self.write(result)    
        
class RebootserverHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.setrebootserver(reqdata)
        print result
        self.write(result)    

class ExcuteserverHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.setexcuteserver(reqdata)
        print result
        self.write(result)    

class CreateuserHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        vim = VimManager() 
        result = vim.checkaccount(reqdata)
        print result
        self.write(result)                                

class CreateCheckuserHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        print json.dumps(reqdata)
        if "standbyyn" in reqdata:
            standbyyn = reqdata["standbyyn"]
        else:
            standbyyn = "N"
        vim = VimManager() 
        result = vim.createcheckaccount(reqdata)
        if result["result"] == "success" and standbyyn == "Y":
            result = vim.standbycreatecheckaccount(reqdata)
        print result
        self.write(result)   
        
class ListnetworksHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.getlistnetwork(reqdata)
        print result
        self.write(result)  

class GetsubnetworkHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.getsubnetwork(reqdata)
        print result
        self.write(result)  

class GetportslistHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        oapi = Openstackapi() 
        result = oapi.getportslist(reqdata)
        print result
        self.write(result) 

class GetAddresFromVMIDHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)        
        oapi = Openstackapi() 
        result = oapi.getaddressfromvmid(reqdata)
        print result
        self.write(result) 

class SetSecurityGroupHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)        
        oapi = Openstackapi() 
        result = oapi.setsecuritygroup(reqdata)
        print result
        self.write(result)         
                                
application = tornado.web.Application([
        (r'/soap2/service/provisions/ready', ReadyHandler),
        (r'/soap2/service/provisions', CreateHandler),
        (r'/soap2/service/provisions/(?P<provisionseq>[^\/]+)/?', DeleteHandler),
        (r'/soap2/service/checkuser', CreateuserHandler),
        (r'/soap2/service/create/checkuser', CreateCheckuserHandler),        
        (r'/soap2/openstack/vncconsole', VncHandler),
        (r'/soap2/openstack/rebootserver', RebootserverHandler),
        (r'/soap2/openstack/excuteServer', ExcuteserverHandler),
        (r'/soap2/openstack/listnetworks', ListnetworksHandler),
        (r'/soap2/openstack/getsubnetwork', GetsubnetworkHandler),
        (r'/soap2/openstack/getportslist', GetportslistHandler),
        (r'/soap2/openstack/getaddress', GetAddresFromVMIDHandler),
        (r'/soap2/openstack/changesecuritygroup', SetSecurityGroupHandler),
])

def main():
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(application)
#     http_server.listen(8080)
    http_server.bind(8080)
    http_server.start(0)
    tornado.ioloop.IOLoop.instance().start()



if __name__ == '__main__':
    main()    