#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''

class Provisioningsql(object):
    '''
    프로비저닝 관련 DB 쿼리를 집합.

    '''

    def __init__(self):
        '''
        Constructor
        '''
    def settraceforprovision(self, trace):

        print "Step %s %s"%(trace["logstep"], trace["stepmsg"])

#         print "cmdtype %s"%trace["cmdtype"]
#         print "managegroup %s"%trace["managegroup"]
#         print "managetype %s"%trace["managetype"]
#         print "provisionseq %s"%trace["provisionseq"]
#         print "userid %s"%trace["userid"]
#         print "stepmsg %s"%trace["stepmsg"]
#         print "indent %s"%trace["indent"]
#         print "logstep %s"%trace["logstep"]
#         print "msglevel %s"%trace["msglevel"]
#         print "stepmsgcmd %s"%trace["stepmsgcmd"]
#         print "stepmsgdetails %s"%trace["stepmsgdetails"]
#         print "resourceid %s"%trace["resourceid"]

        try:

            if trace["svctype"] == None:
                trace["svctype"] = 99

            result = """
                        insert into tb_provision_trace
                        (cmd_type, manage_group, manage_type, provisionseq, userid,
                        step_msg, indent, step, msg_level, step_msg_cmd,
                        step_msg_details, svc_type, resource_id, reg_dttm)
                        values('%s', '%s', %s, %s, '%s',
                        '%s', %s, '%s', %s, '%s',
                        '%s',  %s, '%s',now())
                    """%(trace["cmdtype"], trace["managegroup"], trace["managetype"], trace["provisionseq"], trace["userid"],
                         trace["stepmsg"], trace["indent"], trace["logstep"], trace["msglevel"],trace["stepmsgcmd"],
                         trace["stepmsgdetails"], trace["svctype"], trace["resourceid"])

        except Exception,e:
            print e.message

        return result

    def getuserinfofordeleteprovisioning(self, provisionseq):
        result = """
                    select b.userid as userid, a.orgseq,
                    convert_from(decrypt(decode((b.password), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.res_type as restype, a.res_id as resid, a.res_name as resname, a.floatingip_id as floatingip_id, a.backuptype as backuptype
                    from tb_openstack_resource a
                    inner join tb_user b on a.userid = b.userid
                    where a.provisionseq = %s and a.del_dttm is null
        """ %provisionseq
        return result

    def getuserinfofordeleteprovisioningofstandby(self, provisionseq):
        result = """
                    select b.userid as userid, a.orgseq,
                    convert_from(decrypt(decode((b.password), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.res_type as restype, a.res_id as resid, a.res_name as resname, a.floatingip_id as floatingip_id, a.backuptype as backuptype
                    from tb_openstack_resource a
                    inner join tb_user b on a.userid = b.userid
                    where a.provisionseq = %s and a.del_dttm is null and a.backuptype = 'active'
        """ %provisionseq
        return result
    
    def setnsprovisioning(self, provisionseq):
        result = """
                    update tb_nsprovisioning set procstatecode = '0', del_dttm = now()
                    where nsprovisioningseq = %s
        """ %provisionseq
        return result

    def setne(self, provisionseq):
        result = """
        update tb_ne set usgstatuscode = '2', del_dttm = now() where provisionseq = %s
        """ %provisionseq

        return result

    def setvlanfordeleteprovision(self, orgseq, userid):

        result = """
        update tb_vlan set allocate_yn = 'N', userid = null, type = null where orgseq = %s and userid = '%s'
        """ %(orgseq, userid)

        return result

    def setnsprovisioningforcreateprovision(self, userid, orgseq, templateseq):
        result1 = """
        insert into tb_nsprovisioning (userid, orgseq, templateseq) values ('%s', %s, %s)
        """%(userid, orgseq, templateseq)

        result2 = """
        select max(nsprovisioningseq) as nsprovisioningseq from  tb_nsprovisioning where userid = '%s' and orgseq =  %s
        """%(userid, orgseq)
        return result1, result2
  
    def setneforcreateprovision(self, provisionseq, templateseq, cloidifyid, vmid, neclsseq, necatseq, reqdata):
        result = """
        insert into tb_ne ( neclsseq, necatseq, templateseq, provisionseq, usgstatuscode, procstatecode, cloudifyid, vm_id, config_props)
        values (%s, %s, %s, %s, %s, '%s', '%s', '%s', '%s'::json)
        """%(neclsseq, necatseq, templateseq, provisionseq, 1, "7", cloidifyid, vmid, reqdata)
        return result

    def getuserinfoforcreateprovision(self, userid, orgseq):
        result = """
        select userid,
        convert_from(decrypt(decode((select password from tb_user where userid = '%s' and orgseq = %s), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
        username, userauth, orgseq, customerseq, role_id from tb_user where userid = '%s' and orgseq = %s
        """%(userid, orgseq, userid, orgseq)

        return result

    def setvlanforcreateprovision(self, orgseq, userid, vlan, type):
        result = """
        update tb_vlan set allocate_yn = 'Y', userid = '%s', type = '%s' where orgseq = %s and vlan = %s
        """%(userid, type, orgseq, vlan)
        return result

    def getvlanforcreateprovision(self, orgseq, vlan):
        result = """
        select count(*) as count
        from tb_nsprovisioning ns 
        inner join tb_vlan vlan on vlan.userid = ns.userid
        where ns.procstatecode = '1' and vlan.vlan = %s and ns.orgseq = %s 
        """%(vlan, orgseq)
        return result

    def setfirstprovisionstepforprovision(self, provisionseq, stepstatus, stepmessage):
#         print "workstep %s %s"%(stepstatus, stepmessage)
        result = """
        insert into tb_provision_step(provisionseq, step, step_message, upd_dttm)
        values(%s, %s, '%s', now())
        """%(provisionseq, stepstatus, stepmessage)
        return result

    def setchangeprovisionstepforprovision(self, provisionseq, stepstatus, stepmessage):
#         print "workstep %s %s"%(stepstatus, stepmessage)
        result = """
        update tb_provision_step set step = %s, step_message = '%s', upd_dttm = now() where provisionseq = %s
        """%(stepstatus, stepmessage, provisionseq)
        return result

    def setneprovisionforcomplete(self, provisionseq, consolurl, backuptype):
        result = """
        update tb_ne set procstatecode = '7', console_url = '%s', upd_dttm = now(), backuptype = '%s' where provisionseq = %s and backuptype is null
        """%(consolurl, backuptype, provisionseq)
        return result
    
    def setnsprovisioningforcomplete(self, provisionseq):
        result = """
        update tb_nsprovisioning set procstatecode = '1' where nsprovisioningseq = %s
        """%provisionseq
        return result
    
    def setopenstackresource(self,userid, orgseq, restype, resid, resname, provisionseq, ipid, backuptype):
        result = """
        insert into tb_openstack_resource( userid, orgseq, res_type, res_id, res_name, provisionseq, floatingip_id, backuptype) 
        values ('%s',%s,'%s','%s','%s',%s, '%s', '%s')
        """%(userid, orgseq, restype, resid, resname, provisionseq, ipid, backuptype)
        return result
    def setopenstackresourcedelete(self, provisionseq, resid, backuptype):
        result = """update tb_openstack_resource set del_dttm = now() where provisionseq = %s and res_id = '%s' and backuptype = '%s'
        """%(provisionseq, resid, backuptype)
        return result

    def getopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s
        """%orgseq
        return result    
    def getuserinfoforbackuputm(self, userid, orgseq):
        result = """ select count(*) as usercount from tb_user where userid = '%s' and orgseq = %s
        """%(userid, orgseq)
        return result
    def setuserinfoforbackuputm(self, userid, password, username, userauth, orgseq):
        result = """ insert into tb_user(userid, password, username, userauth, orgseq)
        values('%s', (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), '%s', %s, %s)
        """%(userid, password, username, userauth, orgseq)
        return result
    
    def setchangeuserinfoforbackuputm(self, userid, password, username, userauth, orgseq):
        result = """ update tb_user 
        set password = (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), username = '%s', userauth = '%s'
        where orgseq = %s and userid = '%s'
        """%(password, username, userauth, orgseq, userid)
        return result   
    
    def getutmjoblist(self):
        result = """
        select job_type, jobname from tb_utm_jobs where checkable = 'Y'
        """ 
        return result
    
    def sete2etracemanager(self, provisionseq, mainclass, subclass, message, request_cmd, request_parameter, start_dttm, end_dttm, execute_time, response, response_status, message_detail):
        if subclass == "":
            pass
        elif message.find("VIM") == 0:
            pass
        elif message.find("VNF") == 0 or message.find("VIM") == 0:
            pass
        elif message.find("TEST") == 0 or message.find("VIM") == 0:
            pass                
        else:
            message = "└───&nbsp;"+message
            
            
        result = """
        insert into tb_provisionmgr_trace(
            provisionseq, mainclass, subclass, message, request_cmd, request_parameter, 
            start_dttm, end_dttm, execute_time, response, response_status, 
            reg_dttm, message_detail)
        values(%s,'%s','%s','%s','%s','%s',
        '%s', '%s', '%s', '%s', '%s', now(), '%s')
        """%(provisionseq, mainclass, subclass, message, request_cmd, request_parameter, start_dttm, end_dttm, execute_time, response, response_status, message_detail)
        return result

    def sete2etracemanagerforend(self, provisionseq):
        result = """
        insert into tb_provisionmgr_trace(provisionseq, message, reg_dttm) values(%s,'end',now())
        """%provisionseq
        return result

    def sete2etracemanagerforenddelete(self, provisionseq):
        result = """
        delete from tb_provisionmgr_trace where provisionseq = %s and message = 'end'
        """%provisionseq
        return result
        
    def settracehistoryfirst(self, start_dttm, traceseq, orgseq, caller):
        result = """
        insert into tb_trace_history(trace_type, step_msg, start_dttm, reg_dttm, traceseq, orgseq, caller)
        values(2, 'ProvisionManager', '%s', now(), %s, %s, '%s')
        """%(start_dttm, traceseq, orgseq, caller)
        return result
       
    def settracehistorychange(self, end_dttm, traceseq, orgseq, response_status):
        if response_status == True:
            response_status = "S"
        elif response_status == False:
            response_status = "F"
                    
        result = """
        update tb_trace_history set end_dttm = '%s', response_status = '%s' where orgseq = %s and traceseq = %s 
        """%(end_dttm, response_status, orgseq, traceseq)
        return result
    
    def getcustomername(self, userid):
        result = """
        select customername from tb_customer where userid = '%s'
        """%userid
        return result
    
    def getopenstacktraceerror(self, vmname):
        result = """select traceback from nova.vw_vm_trace where vm_name = '%s' and result = 'Error' order by start_time desc limit 1        
        """%vmname
        return result
    
    def getprovisionseqfromvmid(self, vmid):
        result = """select provisionseq from tb_ne where vm_id = '%s'        
        """%vmid
        return result    