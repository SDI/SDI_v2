#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''
import sys,os, time, json, datetime
# import MySQLdb as mdb
from helper.logHelper import myLogger
from util.restclient import SoapClient
# from config.connect_config import mgmt_vm
# from util.aescipher import AESCipher
from manager.e2emanager import E2emanager
from helper.psycopg_helper import PsycopgHelper
from sql.provisioningsql import Provisioningsql
from netaddr.ip import IPNetwork
from config.connect_config import AESCipherKey
from util.aescipher import AESCipher


org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
 
log = myLogger(tag='startprovision_vnf', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class VnfManager(PsycopgHelper):

    def __init__(self):
        self.sql = Provisioningsql()
#         self.trace = Provisiontrace()
        self.e2e = E2emanager()
        self.init_config_from_dict_config_file()   
       
    def managevnf(self, orgseq, provisionseq, userid, gInstance, applicationitems):
        configinfo = {}
        try:           
            start_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", "VNF Manager 서비스 시작", "", "", start_dttm_provision, start_dttm_provision, "", "", True, "")
#             aes = AESCipher(AESCipherKey['key'])
#             self.mgmt_endpoint = aes.decrypt(mgmt_vm["MGMT_VM_ENDPOIT"])

            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
            aes = AESCipher(AESCipherKey['key'])
            
            if "backuptype" in gInstance and gInstance["backuptype"] == "standby":
                self.mgmt_endpoint = aes.decrypt(rows[0]["infraprops"]["backup_mgmtvms"][0]["endpoint"])
            else:
                self.mgmt_endpoint = aes.decrypt(rows[0]["infraprops"]["mgmtvms"][0]["endpoint"])
            
            time.sleep(1)

            log.debug("[%s][%s][%s] check port request"%(orgseq, provisionseq, userid)) 
                                 
            global_mgmt_net = gInstance["global_mgmt_net"]
#             global_mgmt_net_floating_ip = gInstance["global_mgmt_net_floating_ip"]

            result = self.checkport(provisionseq, userid, global_mgmt_net, "22")
            if result["result"] =="fail":
                log.debug("[%s][%s][%s] check port fail"%(orgseq, provisionseq, userid)) 
                raise Exception(result["description"])
            log.debug("[%s][%s][%s] check port success"%(orgseq, provisionseq, userid)) 
            result = {"result":"success"}

            log.debug("[%s][%s][%s] settings upload request"%(orgseq, provisionseq, userid)) 
            
#             red_shared_public_net = gInstance["red_shared_public_net"]
            result, configinfo = self.configinstance(provisionseq, userid, global_mgmt_net, applicationitems, gInstance)
            if result["result"] =="fail":
                log.debug("[%s][%s][%s] settings upload fail"%(orgseq, provisionseq, userid)) 
                raise Exception("self.configinstance fail ")
            log.debug("[%s][%s][%s] settings upload success"%(orgseq, provisionseq, userid)) 
            result = {"result":"success"}            

            result = self.checkport(provisionseq, userid, global_mgmt_net, "22")
            if result["result"] =="fail":
                log.debug("[%s][%s][%s] check port fail"%(orgseq, provisionseq, userid)) 
                raise Exception("self.checkport fail ")
            log.debug("[%s][%s][%s] check port success"%(orgseq, provisionseq, userid)) 
            result = {"result":"success"}

            result = self.checkproxyarp(provisionseq, userid, global_mgmt_net, applicationitems, gInstance, configinfo)
            if result["result"] =="fail":
                log.debug("[%s][%s][%s] proxyarp config fail"%(orgseq, provisionseq, userid)) 
                raise Exception("self.checkproxyarp fail ")
            log.debug("[%s][%s][%s] proxyarp config success"%(orgseq, provisionseq, userid)) 
            result = {"result":"success"}      

            result = self.checkinterface(provisionseq, userid, global_mgmt_net, gInstance)
            if result["result"] =="fail":
                log.debug("[%s][%s][%s] checkinterface fail"%(orgseq, provisionseq, userid)) 
                raise Exception("self.checkinterface fail ")
            log.debug("[%s][%s][%s] checkinterface success"%(orgseq, provisionseq, userid)) 
            result = {"result":"success"}   
                                               
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "5", u"UTM 설정을 적용하였습니다.")     
            self.execute_set(myquery)
            
            end_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", "VNF Manager 서비스 종료", "", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, "", True, "")
            
        except Exception, e:                      
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "5", u"UTM 설정을 적용이 실패되었습니다.")     
            self.execute_set(myquery)    
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", "VNF Manager 서비스 종료", "managevnf()", "", start_dttm_provision, end_dttm, end_dttm-start_dttm_provision, e.message, False, "")                    
            result =  {"result":"fail", "description": "managevnf error "+e.message}
        
        return result, configinfo
    
    def checkport(self, provisionseq, userid, global_mgmt_net, port):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()                        
        try:
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "5", u"NFV-UTM 통신채널을 점검 중입니다.")     
            self.execute_set(myquery) 
            
            for i in range(1,100,1):
                time.sleep(1)
                print "checkport %s"%i
                resultport = soap.sendData(self.mgmt_endpoint + "/check/port/"+port+"?utm_mgmt="+global_mgmt_net, "get", "")
                if i > 90:
                    log.debug("[%s] check port fail"%i)
                    end_dttm = datetime.datetime.now()  
                    cmd = "self.mgmt_endpoint+\"/check/port/"+ port
                    response = "check port fail"
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 통신채널 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                
                    result = {"result":"fail", "description": response}   
                    break                
                if resultport["result"] == "fail":
                    continue
                else:                
                    log.debug("[%s] check port success"%i)
                    end_dttm = datetime.datetime.now() 
                    cmd = resultport["cmd"]
                    response = json.dumps(resultport["detail"])                 
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 통신채널 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                  
                    result = {"result":"success"}
                    break
                  
        except Exception,e:
            response = "checkport fail " + e.message
            result = {"result":"fail", "description": "checkport fail " + e.message}    
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 통신채널 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "checkport fail " + e.message)            
        return result   
     
    def configinstance(self, provisionseq, userid, global_mgmt_net, applicationitems, gInstance):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()
        try:
            if "hostname" in gInstance:
                hostname = gInstance["hostname"]
            else:
                hostname = ""

            if "domainname" in gInstance:
                domainname = gInstance["domainname"]
            else:
                domainname = ""

            if "dns1" in gInstance:
                dns1 = gInstance["dns1"]
            else:
                dns1 = ""

            if "dns2" in gInstance:
                dns2 = gInstance["dns2"]
            else:
                dns2 = ""
                                                                
            routers = applicationitems[2]["router"]["value"]
            bridge = applicationitems[1]["bridge"]
#             ethernet = applicationitems[0]["ethernet"]

            ethernet = []
                
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth1":
                    ethernet.append(row)   
            
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth2":
                    ethernet.append(row)  
            
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth3":
                    ethernet.append(row)
            
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth4":
                    ethernet.append(row)     

            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth5":
                    ethernet.append(row)    
                    
            for i, row in enumerate(ethernet):
#                 print row.keys()[0]
                if row.keys()[0] == "eth1":
                    red_defaultgateway = row["eth1"]["defaultgateway"]["value"]
                    red_cidrs = row["eth1"]["subnetcidr"]["value"]
#                     print "[eth1] %s"%i
#                     print "red_defaultgateway %s"%red_defaultgateway
#                     print "red_cidrs %s"%red_cidrs

            ip = IPNetwork(global_mgmt_net+"/24")
            global_mgmt_netaddr = str(ip.network)

            vlan_key =  "eth1_vlan"
            red_vlan = gInstance[vlan_key]
                                              
            for index, row in enumerate(red_cidrs):
                ip = IPNetwork(row)
                if index == 0:
                    red_address = str(ip.ip)
                    red_broadcast = str(ip.broadcast)
                    red_netaddr = str(ip.network)
                    red_netmask = str(ip.netmask) 
                    red_cidr = str(ip.prefixlen)
                    red_ips = str(ip.ip)+"/"+str(ip.prefixlen)
                    red_master_address = str(ip.ip)
                else:
                    if not red_master_address == str(ip.ip):
                        red_ips = red_ips +","+str(ip.ip)+"/"+str(ip.prefixlen) 
            redinfo = {"mgmtnetaddr":global_mgmt_net, "global_mgmt_netaddr":global_mgmt_netaddr,  "red_defaultgateway":red_defaultgateway, "red_address":red_address, "red_broadcast":red_broadcast, "red_netaddr":red_netaddr, "red_netmask":red_netmask, "red_cidr":red_cidr, "red_ips":red_ips, "red_vlan":red_vlan}
            
            for row in bridge:
                if "br0" in row:
                    green_address = ""
                    green_broadcast = ""
                    green_netaddr = ""
                    green_netmask = ""
                    green_cidr = ""
                    green_ips = ""
                    green_master_address = ""
                    green_vlan = []
                    br0 = row["br0"]
                    for br_index, br_nic in enumerate(br0["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        green_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
                        vlan_key =  br_nic+"_vlan"
                        green_vlan.append(gInstance[vlan_key])
                        for index, row in enumerate(green_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                green_address = str(ip.ip)                    
                                green_broadcast = str(ip.broadcast)
                                green_netaddr = str(ip.network)
                                green_netmask = str(ip.netmask) 
                                green_cidr = str(ip.prefixlen)
                                green_ips = str(ip.ip)+"/"+str(ip.prefixlen)
                                green_master_address = str(ip.ip)
                            else:
#                                 if not green_master_address == str(ip.ip):
#                                     green_ips = green_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)
                                    
                                if green_ips.find(str(ip.ip)) == -1:
                                    green_ips = green_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)
                                     
                    greeninfo = {"green_address":green_address, "green_broadcast":green_broadcast, "green_netaddr":green_netaddr, "green_netmask":green_netmask, "green_cidr":green_cidr, "br_index":br_index,"green_ips":green_ips, "green_vlan":green_vlan}

                if "br1" in row:
                    orange_address = ""
                    orange_broadcast = ""
                    orange_netaddr = ""
                    orange_netmask = ""
                    orange_cidr = ""
                    orange_ips = ""
                    orange_master_address = ""
                    orange_vlan = []
                    br1 = row["br1"]
                    for br_index, br_nic in enumerate(br1["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        orange_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
                        vlan_key =  br_nic+"_vlan"
                        orange_vlan.append(gInstance[vlan_key])       
            
                        for index, row in enumerate(orange_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                orange_address = str(ip.ip)                    
                                orange_broadcast = str(ip.broadcast)
                                orange_netaddr = str(ip.network)
                                orange_netmask = str(ip.netmask) 
                                orange_cidr = str(ip.prefixlen)
                                orange_ips = str(ip.ip)+"/"+str(ip.prefixlen)
                                orange_master_address = str(ip.ip)
                            else:
#                                 if not orange_master_address == str(ip.ip):
#                                     orange_ips = orange_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)
                                if orange_ips.find(str(ip.ip)) == -1:
                                    orange_ips = orange_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)

                    orangeinfo = {"orange_address":orange_address, "orange_broadcast":orange_broadcast, "orange_netaddr":orange_netaddr, "orange_netmask":orange_netmask, "orange_cidr":orange_cidr, "br_index":br_index, "orange_ips":orange_ips, "orange_vlan":orange_vlan}
                        
                if "br2" in row:
                    blue_address = ""                  
                    blue_broadcast = ""
                    blue_netaddr = ""
                    blue_netmask = ""
                    blue_cidr = ""
                    blue_ips = ""
                    blue_master_address = ""
                    blue_vlan = []
                    br2 = row["br2"]
                    for br_index, br_nic in enumerate(br2["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        blue_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
                        vlan_key =  br_nic+"_vlan"
                        blue_vlan.append(gInstance[vlan_key])                                       
                        for index, row in enumerate(blue_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                blue_address = str(ip.ip)                    
                                blue_broadcast = str(ip.broadcast)
                                blue_netaddr = str(ip.network)
                                blue_netmask = str(ip.netmask) 
                                blue_cidr = str(ip.prefixlen)
                                blue_ips = str(ip.ip)+"/"+str(ip.prefixlen)
                                blue_master_address = str(ip.ip)
                            else:
#                                 if not blue_master_address == str(ip.ip):
#                                     blue_ips = blue_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)
                                if blue_ips.find(str(ip.ip)) == -1:
                                    blue_ips = blue_ips +","+str(ip.ip)+"/"+str(ip.prefixlen)

                    blueinfo = {"blue_address":blue_address, "blue_broadcast":blue_broadcast, "blue_netaddr":blue_netaddr, "blue_netmask":blue_netmask, "blue_cidr":blue_cidr, "br_index":br_index, "blue_ips":blue_ips,"blue_vlan":blue_vlan}

            """
            config_type 추가
            1. GREEN CONFIG_TYPE=2  
            2. GREEN & ORANGE CONFIG_TYPE=3 
            3. GREEN & BLUE CONFIG_TYPE=4
            4.GREEN & ORANGE & BLUE CONFIG_TYPE=5
            """
            
            if green_address <> "" and orange_address <> "" and blue_address <> "":
                config_type = "5"
            elif green_address <> "" and orange_address == ""  and blue_address <> "":
                config_type = "4"
            elif green_address <> "" and orange_address <> ""  and blue_address == "":
                config_type = "3"
            elif green_address <> "" and orange_address == ""  and blue_address == "":
                config_type = "2"
            else:
                config_type = "7"
             
            log.debug("[%s] green %s orange %s blue %s config_type -> %s"%(provisionseq, green_address, orange_address, blue_address, config_type))
                
            configinfo = {"hostname":hostname, "redinfo":redinfo, "greeninfo":greeninfo, "orangeinfo":orangeinfo, "blueinfo":blueinfo, "br0":br0, "br1":br1, "br2":br2, "routers":routers, "domainname":domainname, "dns1":dns1, "dns2":dns2, "configtype":config_type}
            senddata = {"configinfo" : configinfo}

            resultconfig = soap.sendData(self.mgmt_endpoint + "/utmconfig", "post", json.dumps(senddata))

            if resultconfig["result"] == "fail":           
                end_dttm = datetime.datetime.now()      
                response = resultconfig["description"]
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 룰 변경요청", self.mgmt_endpoint+"/utmconfig", "", start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                result = {"result":"fail", "description": response}
            else:
                end_dttm = datetime.datetime.now()   
  
                log.debug("[%s]resultconfig --> %s"%(provisionseq, json.dumps(resultconfig)))
  
                mainConfingFile = resultconfig["mainconfig"]
                ethernetConfigFile = resultconfig["ethernetconfig"]
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 레드 룰 설정", self.mgmt_endpoint+"/utmconfig", "/var/efw/uplinks/main/settings upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , mainConfingFile)                
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 고객및서버네트워크 룰 설정", self.mgmt_endpoint+"/utmconfig", "/var/efw/ethernet/settings upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , ethernetConfigFile)
                br0 = resultconfig["br0"]
                bridgerule = ""
                for row in br0:
                    bridgerule = bridgerule + row +" "
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 그린룰 설정(br0)", self.mgmt_endpoint+"/utmconfig", "/var/efw/ethernet/br0 upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , bridgerule)
                br1 = resultconfig["br1"]
                bridgerule = ""
                for row in br1:
                    bridgerule = bridgerule + row +" "                
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 오렌지룰 설정(br1)", self.mgmt_endpoint+"/utmconfig", "/var/efw/ethernet/br1 upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , bridgerule)
                br2 = resultconfig["br2"]
                bridgerule = ""
                for row in br2:
                    bridgerule = bridgerule + row +" "                    
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 블루룰 설정(br2)", self.mgmt_endpoint+"/utmconfig", "/var/efw/ethernet/br2 upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , bridgerule)

                router = resultconfig["router"]
                routersinfo = ""
                for row in router:
                    routersinfo = routersinfo + row + " "
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 라우터 룰 설정", self.mgmt_endpoint+"/utmconfig", "/var/efw/routing/config upload", start_dttm, end_dttm, end_dttm-start_dttm, resultconfig["result"], True , routersinfo)

                if len(resultconfig["result_red_settings_file_type"][0]) == 0:
                    result_red_settings_file_type = ""
                else:
                    result_red_settings_file_type = resultconfig["result_red_settings_file_type"][0]
                
                if len(resultconfig["result_updatelinks"][0]) == 0:
                    result_updatelinks = ""
                else:
                    result_updatelinks = resultconfig["result_updatelinks"][0]
                
                if len(resultconfig["result_ethernet_settings_file_type"][0]) == 0:
                    result_ethernet_settings_file_type = ""
                else:
                    result_ethernet_settings_file_type = resultconfig["result_ethernet_settings_file_type"][0]
                
                if len(resultconfig["result_networkupdate"][0]) == 0:
                    result_networkupdate = ""
                else:
                    result_networkupdate = resultconfig["result_networkupdate"][0]
                                        
                settings_result_uplink = result_red_settings_file_type + "<br>" + str(result_updatelinks).replace("'", "")               
                settings_result_networkd = result_ethernet_settings_file_type + "<br>" + str(result_networkupdate).replace("'", "")
                                             
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 레드 룰 설정 적용", self.mgmt_endpoint+"/utmconfig", "jobcontrol request uplinksdaemonjob.updatewizard", start_dttm, end_dttm, end_dttm-start_dttm, settings_result_uplink, True , "")
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 고객및서버네트워크 룰  설정 적용", self.mgmt_endpoint+"/utmconfig", "jobcontrol request network.updatewizard", start_dttm, end_dttm, end_dttm-start_dttm, settings_result_networkd, True , "")
            
            result = {"result":"success", "description": ""}
           
        except Exception,e:
            log.error("vnf manager error %s"%e.message)
#             print e.message
            result = {"result":"fail", "description": "vnf manager configinstance error "+e.message}
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM 네트워크 룰  설정 적용", self.mgmt_endpoint+"/utmconfig", "jobcontrol request uplinksdaemon/network.updatewizard", start_dttm, end_dttm, end_dttm-start_dttm, "", False , "vnf manager configinstance error "+e.message)            
        return result, configinfo

    def checkproxyarp(self, provisionseq, userid, global_mgmt_net, applicationitems, gInstance, configinfo):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()
        try:
#             print "---------------------------check proxy arp"
            bridge = applicationitems[1]["bridge"]
            
            ethernet = []
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth1":
                    ethernet.append(row)   
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth2":
                    ethernet.append(row)  
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth3":
                    ethernet.append(row)
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth4":
                    ethernet.append(row)
            for row in applicationitems[0]["ethernet"]:
                if row.keys()[0] == "eth5":
                    ethernet.append(row)                                           
            ebtable_cmd = []                      
            green_ips = configinfo["greeninfo"]["green_ips"]
            orange_ips = configinfo["orangeinfo"]["orange_ips"]
            blue_ips = configinfo["blueinfo"]["blue_ips"]

            for row in bridge:
                if "br0" in row:
                    if len(row["br0"]["proxy_arp"]) == 0:
                        continue
                    br0 = row["br0"]
                    for br_index, br_nic in enumerate(br0["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        green_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
                        for index, row in enumerate(green_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                cmd = "ebtables -t nat -N CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                cmd = "ebtables -t nat -A PREROUTING -j CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                green_address = str(ip.ip)
                                cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%green_address
                                ebtable_cmd.append(cmd)
#                                 print "cmd : %s"%cmd
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)                                
                                ebtable_cmd.append(cmd)
                            else:
                                if not green_address == str(ip.ip):
                                    green_address = str(ip.ip)
                                    cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%green_address
                                    ebtable_cmd.append(cmd)                                
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)
                                ebtable_cmd.append(cmd)
                                green_ips = green_ips +","+str(ip.cidr)                   
                if "br1" in row:
                    if len(row["br1"]["proxy_arp"]) == 0:
                        continue
                    br1 = row["br1"]
                    for br_index, br_nic in enumerate(br1["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        orange_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
#                         print "orange_cidrs: %s"%orange_cidrs
                        for index, row in enumerate(orange_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                
                                cmd = "ebtables -t nat -N CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                cmd = "ebtables -t nat -A PREROUTING -j CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                                                
                                orange_address = str(ip.ip)
                                cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%orange_address
                                ebtable_cmd.append(cmd)
#                                 print "cmd : %s"%cmd
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)                                
                                ebtable_cmd.append(cmd)
                            else:
                                if not orange_address == str(ip.ip):
                                    orange_address = str(ip.ip)
                                    cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%orange_address
                                    ebtable_cmd.append(cmd)                                   
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)
                                ebtable_cmd.append(cmd)
                                orange_ips = orange_ips +","+str(ip.cidr)      

                if "br2" in row:
                    if len(row["br2"]["proxy_arp"]) == 0:
                        continue
                    br1 = row["br2"]
                    for br_index, br_nic in enumerate(br1["value"]):
                        nic_index = 0
                        for i, nic in enumerate(ethernet):
                            if br_nic == ethernet[i].keys()[0]:
                                nic_index = i 
                        blue_cidrs = ethernet[nic_index][br_nic]["subnetcidr"]["value"]
#                         print "blue_cidrs: %s"%blue_cidrs
                        for index, row in enumerate(blue_cidrs):
                            ip = IPNetwork(row)
                            if br_index == 0 and index == 0:
                                cmd = "ebtables -t nat -N CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                cmd = "ebtables -t nat -A PREROUTING -j CUSTOM_PROXY_ARP"
                                ebtable_cmd.append(cmd)
                                                                
                                blue_address = str(ip.ip)
                                cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%blue_address
                                ebtable_cmd.append(cmd)
#                                 print "cmd : %s"%cmd
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)                                
                                ebtable_cmd.append(cmd)
                            else:
                                if not blue_address == str(ip.ip):
                                    blue_address = str(ip.ip)
                                    cmd = u"ebtables -t nat -I CUSTOM_PROXY_ARP -p arp --arp-opcode Request --arp-ip-dst %s -j ACCEPT"%blue_address
                                    ebtable_cmd.append(cmd)                                     
                                key = "network_%s_mac"%br_nic
                                cmd = u"ebtables -t nat -A CUSTOM_PROXY_ARP -p arp --arp-opcode Request -i %s --arp-ip-dst %s ! --arp-gratuitous -j arpreply --arpreply-mac %s"%(br_nic, str(ip.cidr), gInstance[key])
#                                 print "cmd %s : %s"%(br_index, cmd)
                                ebtable_cmd.append(cmd)
                                blue_ips = blue_ips +","+str(ip.cidr)     
                                                                                                                       
            senddata = {"utm_mgmt":global_mgmt_net, "cmd":ebtable_cmd}
            log.debug("senddata %s"%senddata)

            if not len(ebtable_cmd) == 0:
                resultconfig = soap.sendData(self.mgmt_endpoint + "/proxyarp", "post", json.dumps(senddata))
                log.debug("/proxyarp result -> %s"%resultconfig)
                if resultconfig["result"] == "fail":           
                    end_dttm = datetime.datetime.now()      
                    response = resultconfig["message"]
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM Proxy arp 설정", self.mgmt_endpoint+"/proxyarp", "", start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
    
                else:
                    ebtable = ""
                    end_dttm = datetime.datetime.now()   
                    response = resultconfig["message"]
                    cmdlist = resultconfig["cmd"]
                    detail = resultconfig["detail"]
                    log.debug("resultconfig detail->  %s"%detail)
                    log.debug("resultconfig cmd->  %s"%cmdlist)
                    for cmd in cmdlist:
    #                     print cmd
                        ebtable = ebtable + cmd + "<br>"
                    log.debug("ebtable --> %s"%ebtable)
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM Proxy arp 설정", self.mgmt_endpoint+"/proxyarp", ebtable, start_dttm, end_dttm, end_dttm-start_dttm, response, True , detail)
                
#             print "---------------------------check proxy arp end %s"%resultconfig
            result = {"result":"success", "description": ""}
           
        except Exception,e:
            log.error("vnf manager error %s"%e.message)
#             print e.message
            result = {"result":"fail", "description": "vnf manager checkproxyarp error "+e.message}
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM Proxy arp 설정", self.mgmt_endpoint+"/proxyarp", "", start_dttm, end_dttm, end_dttm-start_dttm, "", False , "vnf manager checkproxyarp error "+e.message)
        return result

    def checkinterface(self, provisionseq, userid, global_mgmt_net, gInstance):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()
        try:

            interfacestatusoff = []
            
            if "eth1_vlan_status" in gInstance and gInstance["eth1_vlan_status"] == "off":
                interfacestatusoff.append("ifconfig eth1 down")
            if "eth2_vlan_status" in gInstance and gInstance["eth2_vlan_status"] == "off":
                interfacestatusoff.append("ifconfig eth2 down")
            if "eth3_vlan_status" in gInstance and gInstance["eth3_vlan_status"] == "off":
                interfacestatusoff.append("ifconfig eth3 down")
            if "eth4_vlan_status" in gInstance and gInstance["eth4_vlan_status"] == "off":
                interfacestatusoff.append("ifconfig eth4 down")
            if "eth5_vlan_status" in gInstance and gInstance["eth5_vlan_status"] == "off":
                interfacestatusoff.append("ifconfig eth5 down")
            if "br0_status" in gInstance and gInstance["br0_status"] == "off":
                interfacestatusoff.append("ifconfig br0 down")
            if "br1_status" in gInstance and gInstance["br1_status"] == "off":
                interfacestatusoff.append("ifconfig br1 down")
            if "br2_status" in gInstance and gInstance["br2_status"] == "off":
                interfacestatusoff.append("ifconfig br2 down")
            
            log.debug("checkinterface --> %s"%interfacestatusoff)
            
            if len(interfacestatusoff) == 0:
                return {"result":"success", "description": ""}
                                                                                                                                                            
            senddata = {"utm_mgmt":global_mgmt_net, "status":interfacestatusoff}
            resultconfig = soap.sendData(self.mgmt_endpoint + "/interfacestatus", "post", json.dumps(senddata))
            log.debug("/interfacestatus result -> %s"%resultconfig)
            if resultconfig["result"] == "fail":           
                end_dttm = datetime.datetime.now()      
                response = resultconfig["message"]
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM interface status 설정", self.mgmt_endpoint+"/interfacestatus", "", start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            else:
                end_dttm = datetime.datetime.now()      
                response = resultconfig["message"]
                interfacecmd = ""
                for row in interfacestatusoff:
                    interfacecmd = interfacecmd + " "+ row
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM interface status 설정", self.mgmt_endpoint+"/interfacestatus", interfacecmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
#             print "---------------------------check proxy arp end %s"%resultconfig
            result = {"result":"success", "description": ""}
           
        except Exception,e:
            log.error("vnf manager error %s"%e.message)
#             print e.message
            result = {"result":"fail", "description": "vnf manager interfacestatus error "+e.message}
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VNF-mgr", u"NFV-UTM interfacestatus 설정", self.mgmt_endpoint+"/interfacestatus", "", start_dttm, end_dttm, end_dttm-start_dttm, "", False , "vnf manager interfacestatus error "+e.message)
        return result

                       