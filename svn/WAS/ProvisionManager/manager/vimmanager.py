#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''

import sys,os, time, datetime

import json
import collections

import MySQLdb as mdb
# from service.provisiontrace import Provisiontrace
from manager.e2emanager import E2emanager
from util.openstackclient import NeutronClient, NovaClient, KeystoneClient
from sql.provisioningsql import Provisioningsql
from helper.psycopg_helper import PsycopgHelper
from helper.logHelper import myLogger
from util.aescipher import AESCipher
from config.connect_config import AESCipherKey

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='startprovision_vim', logdir='./log', loglevel='debug', logConsole=True).get_instance()


class VimManager(PsycopgHelper):
    def __init__(self):
        self.sql = Provisioningsql()
#         self.trace = Provisiontrace()
        self.e2e = E2emanager()
        self.init_config_from_dict_config_file()      
        self.gInstance = collections.OrderedDict()
             
    def init_openstack_client(self, orgseq):     
        log.debug("backuptype->%s"%self.backuptype)
        query = self.sql.getopenstackinfra(orgseq)
        rows = self.execute_getone(query)
        
        aes = AESCipher(AESCipherKey['key'])
        
        if not self.backuptype == "standby":
            self.host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"])    
            self.tenantname = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["tenantid"])
            self.username = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["userid"])
            self.password = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["password"])
            self.authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
            self.utm =  rows[0]["infraprops"]["nfv_products"]
            self.basic_networks = rows[0]["infraprops"]["shared_networks"]
    
            self.openstackdbdatabase = aes.decrypt(rows[0]["infraprops"]["openstack_db_connect"]["database"])
            self.openstackdbhost = aes.decrypt(rows[0]["infraprops"]["openstack_db_connect"]["host"])
            self.openstackdbuserid = aes.decrypt(rows[0]["infraprops"]["openstack_db_connect"]["userid"])
            self.openstackdbpassword = aes.decrypt(rows[0]["infraprops"]["openstack_db_connect"]["password"])
            self.openstackdbport = aes.decrypt(rows[0]["infraprops"]["openstack_db_connect"]["port"])
        else:
            self.host = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["host"])    
            self.tenantname = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["tenantid"])
            self.username = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["userid"])
            self.password = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["password"])
            self.authurl = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["token_url"])
            self.utm =  rows[0]["infraprops"]["backup_nfv_products"]
            self.basic_networks = rows[0]["infraprops"]["backup_shared_networks"]
    
            self.openstackdbdatabase = aes.decrypt(rows[0]["infraprops"]["backup_openstack_db_connect"]["database"])
            self.openstackdbhost = aes.decrypt(rows[0]["infraprops"]["backup_openstack_db_connect"]["host"])
            self.openstackdbuserid = aes.decrypt(rows[0]["infraprops"]["backup_openstack_db_connect"]["userid"])
            self.openstackdbpassword = aes.decrypt(rows[0]["infraprops"]["backup_openstack_db_connect"]["password"])
            self.openstackdbport = aes.decrypt(rows[0]["infraprops"]["backup_openstack_db_connect"]["port"])
                        
        for row in self.utm:
            if row["name"] == "utm":
                self.availability_zone = row["defaultconf"]["availability_zone"]
        self.keystone = KeystoneClient()
        self.keystone.authenticate(self.host, self.tenantname, self.username, self.password, self.authurl)                     
        self.neutron = NeutronClient()
        self.neutron.authenticate(self.host, self.tenantname, self.username, self.password, self.authurl)       
        self.nova = NovaClient() 
    
    def managevim(self, index, provisionseq, reqdata):
        try:      
                                 
            log.debug("-"*100)
            log.debug("index -> %s"%index)
            
            self.gInstance = {}
                                                
            userid = reqdata["provision"]["userid"]
            orgseq = reqdata["provision"]["orgseq"]
            
            if "hostname" in reqdata["provision"]:
                hostname = reqdata["provision"]["hostname"]
                self.gInstance["hostname"] = hostname

            if "domainname" in reqdata["provision"]:
                domainname = reqdata["provision"]["domainname"]
                self.gInstance["domainname"] = domainname                

            if "dns1" in reqdata["provision"]:
                dns1 = reqdata["provision"]["dns1"]
                self.gInstance["dns1"] = dns1     

            if "dns2" in reqdata["provision"]:
                dns2 = reqdata["provision"]["dns2"]
                self.gInstance["dns2"] = dns2     
                                            
            if 'syncseq' in reqdata["provision"]:
                caller = reqdata["provision"]["syncseq"]
                self.gInstance["syncseq"] = caller     
            else:
                caller = ""
                self.gInstance["syncseq"] = "0"
                
            if "floatingIP" in reqdata["provision"]:
                floatingIP = reqdata["provision"]["floatingIP"]
            else:
                floatingIP = "public_net"   

            if not "backuptype" in reqdata["provision"]["application"][index]:
                self.backuptype = ""
            elif reqdata["provision"]["application"][index]["backuptype"] == "standby":
                self.backuptype = reqdata["provision"]["application"][index]["backuptype"]
                self.gInstance["backuptype"] = self.backuptype
            else:
                self.backuptype = "active"
                self.gInstance["backuptype"] = self.backuptype

            log.debug("backuptype --> %s"%self.backuptype)

            start_dttm_provision = datetime.datetime.now()
            
            if not self.backuptype == "standby":
                myquery = self.sql.settracehistoryfirst(start_dttm_provision, provisionseq, orgseq, caller)
                self.execute_set(myquery)
            
            message = "VIM Manager 서비스 시작(%s)"%self.backuptype
            
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_provision, start_dttm_provision, "", "", True, "")              
               
            self.init_openstack_client(orgseq)
            self.gInstance["orgseq"] = orgseq
            self.gInstance["openstackhost"] = self.host
            self.gInstance["openstacktokenurl"] = self.authurl

            ethernet = []
                
            for row in reqdata["provision"]["application"][index]["service"][0]["item"][0]["ethernet"]:
                if row.keys()[0] == "eth1":
                    ethernet.append(row)   
            
            for row in reqdata["provision"]["application"][index]["service"][0]["item"][0]["ethernet"]:
                if row.keys()[0] == "eth2":
                    ethernet.append(row)  
            
            for row in reqdata["provision"]["application"][index]["service"][0]["item"][0]["ethernet"]:
                if row.keys()[0] == "eth3":
                    ethernet.append(row)
            
            for row in reqdata["provision"]["application"][index]["service"][0]["item"][0]["ethernet"]:
                if row.keys()[0] == "eth4":
                    ethernet.append(row)     

            for row in reqdata["provision"]["application"][index]["service"][0]["item"][0]["ethernet"]:
                if row.keys()[0] == "eth5":
                    ethernet.append(row)     
#             ethernet = reqdata["provision"]["application"][0]["service"][0]["item"][0]["ethernet"]

            vlans = []
#             vlancount = 1    
            for row in ethernet:     
                vlantype = ""
                if "eth1" in row:
                    vlan = row["eth1"]["vlan"]["value"]
                    vlans.append(vlan)
                    self.gInstance["eth1_vlan"] = vlan
                    if "type" in row["eth1"]["vlan"]:
                        vlantype = row["eth1"]["vlan"]["type"]
                    if "status" in row["eth1"]["vlan"]:
                        self.gInstance["eth1_vlan_status"] = row["eth1"]["vlan"]["status"]
                    else:
                        self.gInstance["eth1_vlan_status"] = "on"
                    myquery = self.sql.getvlanforcreateprovision(orgseq, vlan)
                    row = self.execute_getone(myquery)
                    log.debug("vlan[%s] eth1 used -> [%s]"%(vlan,row[0]["count"]))
                    if row[0]["count"] == 0:
                        myquery = self.sql.setvlanforcreateprovision(orgseq, userid, vlan, vlantype)
                        self.execute_set(myquery)
                        log.debug("set vlan %s complete"%vlan)
                    else:
                        log.debug("vlan %s conflict"%vlan)
                        message = "vlan %s conflict"%vlan
                        raise Exception(message)
                                    
                if "eth2" in row:    
                    vlan = row["eth2"]["vlan"]["value"]
                    vlans.append(vlan)
                    self.gInstance["eth2_vlan"] = vlan
                    if "type" in row["eth2"]["vlan"]:
                        vlantype = row["eth2"]["vlan"]["type"]
                    if "status" in row["eth2"]["vlan"]:
                        self.gInstance["eth2_vlan_status"] = row["eth2"]["vlan"]["status"]
                    else:
                        self.gInstance["eth2_vlan_status"] = "on"                        
                    myquery = self.sql.getvlanforcreateprovision(orgseq, vlan)
                    row = self.execute_getone(myquery)
                    log.debug("vlan[%s] eth2 used -> [%s]"%(vlan,row[0]["count"]))
                    if row[0]["count"] == 0:                    
                        myquery = self.sql.setvlanforcreateprovision(orgseq, userid, vlan, vlantype)
                        self.execute_set(myquery)
                        log.debug("set vlan %s complete"%vlan)                   
                    else:
                        log.debug("vlan %s conflict"%vlan)
                        message = "vlan %s conflict"%vlan
                        raise Exception(message)                                  
                        
                if "eth3" in row:    
                    vlan = row["eth3"]["vlan"]["value"]
                    vlans.append(vlan)
                    self.gInstance["eth3_vlan"] = vlan
                    if "type" in row["eth3"]["vlan"]:
                        vlantype = row["eth3"]["vlan"]["type"]
                    if "status" in row["eth3"]["vlan"]:
                        self.gInstance["eth3_vlan_status"] = row["eth3"]["vlan"]["status"]
                    else:
                        self.gInstance["eth3_vlan_status"] = "on"                        
                    myquery = self.sql.getvlanforcreateprovision(orgseq, vlan)
                    row = self.execute_getone(myquery)
                    log.debug("vlan[%s] eth3 used -> [%s]"%(vlan,row[0]["count"]))
                    if row[0]["count"] == 0:                       
                        myquery = self.sql.setvlanforcreateprovision(orgseq, userid, vlan, vlantype)
                        self.execute_set(myquery)
                        log.debug("set vlan %s complete"%vlan)
                    else:
                        log.debug("vlan %s conflict"%vlan)
                        message = "vlan %s conflict"%vlan
                        raise Exception(message)                        
                            
                if "eth4" in row:    
                    vlan = row["eth4"]["vlan"]["value"]
                    vlans.append(vlan)
                    self.gInstance["eth4_vlan"] = vlan
                    if "type" in row["eth4"]["vlan"]:
                        vlantype = row["eth4"]["vlan"]["type"]
                    if "status" in row["eth4"]["vlan"]:
                        self.gInstance["eth4_vlan_status"] = row["eth4"]["vlan"]["status"]
                    else:
                        self.gInstance["eth4_vlan_status"] = "on"
                            
                    myquery = self.sql.getvlanforcreateprovision(orgseq, vlan)
                    row = self.execute_getone(myquery)
                    log.debug("vlan[%s] eth4 used -> [%s]"%(vlan,row[0]["count"]))
                    if row[0]["count"] == 0:                       
                        myquery = self.sql.setvlanforcreateprovision(orgseq, userid, vlan, vlantype)
                        self.execute_set(myquery)
                        log.debug("set vlan %s complete"%vlan)
                    else:
                        log.debug("vlan %s conflict"%vlan)
                        message = "vlan %s conflict"%vlan
                        raise Exception(message)                        
                        
                if "eth5" in row:    
                    vlan = row["eth5"]["vlan"]["value"]
                    vlans.append(vlan)
                    self.gInstance["eth5_vlan"] = vlan
                    if "type" in row["eth5"]["vlan"]:
                        vlantype = row["eth5"]["vlan"]["type"]
                    if "status" in row["eth5"]["vlan"]:
                        self.gInstance["eth5_vlan_status"] = row["eth5"]["vlan"]["status"]
                    else:
                        self.gInstance["eth5_vlan_status"] = "on"    
                        
                    myquery = self.sql.getvlanforcreateprovision(orgseq, vlan)
                    row = self.execute_getone(myquery)
                    log.debug("vlan[%s] eth5 used -> [%s]"%(vlan,row[0]["count"]))
                    if row[0]["count"] == 0:                       
                        myquery = self.sql.setvlanforcreateprovision(orgseq, userid, vlan, vlantype)
                        self.execute_set(myquery)
                        log.debug("set vlan %s complete"%vlan)                 
                    else:
                        log.debug("vlan %s conflict"%vlan)
                        message = "vlan %s conflict"%vlan
                        raise Exception(message)                        
                        
            if not "eth1_vlan" in self.gInstance:
                raise Exception("eth1 vlan not exist")    
            if not "eth2_vlan" in self.gInstance:
                raise Exception("eth2 vlan not exist")       
                       
            for row in reqdata["provision"]["application"][index]["service"][0]["item"][1]["bridge"]:
                if "br0" in row:
                    if "status" in row["br0"]:
                        self.gInstance["br0_status"]  = row["br0"]["status"]
                    else:
                        self.gInstance["br0_status"]  = "on"
                if "br1" in row:
                    if "status" in row["br1"]:
                        self.gInstance["br1_status"]  = row["br1"]["status"]
                    else:
                        self.gInstance["br1_status"]  = "on"                
                if "br2" in row:
                    if "status" in row["br2"]:
                        self.gInstance["br2_status"]  = row["br2"]["status"]
                    else:
                        self.gInstance["br2_status"]  = "on"
                                 
            """        
            keypair 검사
            """
            if self.backuptype == "standby":
                myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "0", u"서비스 설치를 시작합니다.")
                self.execute_set(myquery)
            else:
                myquery = self.sql.setfirstprovisionstepforprovision(provisionseq, "0", u"서비스 설치를 시작합니다.")
                self.execute_set(myquery)
            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정을 확인합니다.")
            self.execute_set(myquery)

            myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
            userinfo = self.execute_getone(myquery)
            
            self.gInstance["userinfo"] = userinfo
            user = self.keystone.getUserByName(userid)

            start_dttm = datetime.datetime.now()
            if ("result" in user and user["result"] == "fail"):
                tenant, keypair = self.createaccount(provisionseq, userinfo)      
                self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl)            
                if ("result" in tenant and tenant["result"] == "fail"):
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정생성이 실패되었습니다.")
                    self.execute_set(myquery)
                    end_dttm = datetime.datetime.now()
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"계정생성", "createaccount()", userid, start_dttm, end_dttm, end_dttm-start_dttm, json.dumps(tenant), False, "")
                    if not keypair == {}:
                        self.nova.deleteKeypair(userid+"-keypair")
                    if not user == {}:
                        self.keystone.deleteUser(userid)
                    if not tenant == {}:
                        self.keystone.deleteTenant(tenant["tenant"]["id"])                                            
                    raise Exception("create account fail")
                else:
                    end_dttm = datetime.datetime.now()
#                     response =  json.dumps(tenant["tenant"], indent=4, sort_keys=True)
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"계정생성", "createaccount()", userid, start_dttm, end_dttm, end_dttm-start_dttm, json.dumps(tenant), True, "")
                tenantid = tenant["tenant"]["id"]
            else:
                """
                존재여부 확인
                """
                self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl)      
                keypair = self.nova.getKeypair(userid+"-keypair")
                response = json.dumps(keypair)
                if ("result" in keypair and keypair["result"] == "fail"):
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정확인 실패했습니다.")
                    self.execute_set(myquery)                     
                    end_dttm = datetime.datetime.now()
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"계정확인", "nova.getKeypair()", userid+"-keypair", start_dttm, end_dttm, end_dttm-start_dttm, response, False, "")                          
                    raise Exception("keypair is null")    
                else:
#                     print "keypair %s"%keypair
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정확인 완료되었습니다.")
                    self.execute_set(myquery)
                                     
                    myquery = self.sql.getcustomername(userid)
                    rows = self.execute_getone(myquery)
                    message_detail = ""
#                     if not rows == []:
#                         customername = rows[0]["customername"]
#                         message_detail = u"고객사명 :"+customername.decode('utf-8')
                    end_dttm = datetime.datetime.now()
#                     message_detail = {"name":keypair["keypair"]["name"],"created_at":keypair["keypair"]["created_at"]}
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"계정확인", "nova.getKeypair()", userid+"-keypair", start_dttm, end_dttm, end_dttm-start_dttm, response, True, message_detail)
                tenantid = user["user"]["tenantId"]

            """
            Network 생성
            """
            networks = []
            nics = []
            subnets = []          

            for row in ethernet:  
                nic = row.keys()
                nics.append(nic[0])
                result, network = self.createnetwork(nic[0], tenantid, provisionseq, userid, userinfo, row[nic[0]]["vlan"]["value"])
                if ("result" in network and network["result"] == "fail"):
                    log.debug("[%s][%s][%s] network_%s fail"%(orgseq, provisionseq, userid, nic[0])) 
                    raise Exception("network "+network["description"])                   
                networks.append({"uuid":network["network"]["id"]})
                subnet_cidr = row[nic[0]]["subnetcidr"]["value"]
                for index, cidr in enumerate(subnet_cidr):
                    result, subnetwork = self.createsubnetwork(index, nic[0], tenantid, provisionseq, userid, userinfo, cidr, network["network"]["id"])
                    if ("result" in subnetwork and subnetwork["result"] == "fail"):
                        log.debug("[%s][%s][%s] subnetwork_%s fail"%(orgseq, provisionseq, userid, nic[0])) 
                        raise Exception("subnetwork "+subnetwork["description"])
                    log.debug("[%s][%s][%s] network success"%(orgseq, provisionseq, userid))
                subnet = {nic[0]:index}
                subnets.append(subnet)               
            self.gInstance["nic"] = nics
            self.gInstance["subnets"] = subnets
#             print self.gInstance["nic"]
#             print self.gInstance["subnets"]  
                                                                        
            """
            Instance 생성
            """
            log.debug("#"*100)
            log.debug("security_group_rules start")  
            
            for row in self.utm:
                if row["name"] == "utm":
                    row["defaultconf"]["security_groups"][0]["name"] = "NfvAccessGroup" + "_"+str(provisionseq)
                    security_group_rules = row["defaultconf"]["security_groups"][0]["rules"]
                    
            """
            security_ports = reqdata["provision"]["application"][0]["service"][0]["item"][3]["security_port"]["value"]
            dhcpyn = reqdata["provision"]["application"][0]["service"][0]["item"][4]["dhcp"]["value"]

            if len(security_ports) > 0:
                for security_port in security_ports:
                    security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': security_port, 'port_range_min': security_port}}
                    security_group_rules.append(security_group_rule)
            if dhcpyn == "true":
                dhcp_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': '67', 'port_range_min': '67'}}
                security_group_rules.append(dhcp_rule)
                    
            log.debug("#"*100)
            log.debug("security_group_rules -> %s"%json.dumps(security_group_rules))        
            """                      
            log.debug("[%s][%s][%s] instance request"%(orgseq, provisionseq, userid))
            result, server = self.createinstance(provisionseq, userid, networks, userinfo, security_group_rules, floatingIP)
            if not result:
                log.debug("[%s][%s][%s] instance fail"%(orgseq, provisionseq, userid))
                raise Exception("instance create fail")    
            
            vmid = server["server"]["id"]    
            log.debug("[%s][%s][%s] instance success %s"%(orgseq, provisionseq, userid, vmid))
            
#             raise Exception("instance create test")    

            """
            --인스턴스설정
            """
#             log.debug("[%s][%s][%s] instance settings upload request"%(orgseq, provisionseq, userid))
#             
#             result = self.confirminstance(provisionseq, userid, vmid)
#            
#             if result["result"] == "fail":
#                 log.debug("[%s][%s][%s] instance settings upload fail"%(orgseq, provisionseq, userid))
#                 raise Exception(result["description"])
#             log.debug("[%s][%s][%s] instance settings upload success"%(orgseq, provisionseq, userid))
            message = "VIM Manager 서비스 종료(%s)"%self.backuptype
            end_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, "", True, "")                        
            
            return {"result":"success", "gInstance":self.gInstance}
        
        except Exception, e:
            log.error("vim manager error %s"%e.message)
#             print e.message
#             print self.gInstance
            
#             time.sleep(100)
#             end_dttm_provision = datetime.datetime.now()
#             self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", "VIM Manager 서비스 종료", "", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, "", False, "vim manager error " +e.message)
            """
            rollback
            """
            backuptype = self.backuptype
            if backuptype == "standby":
                self.deleteInstanceforstandby(provisionseq, "active")
                
            if "serverid" in self.gInstance:
                start_dttm = datetime.datetime.now()
                myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "51", u"NFV-UTM을 삭제합니다.")
                self.execute_set(myquery)                    
#                 self.nova.deleteServer(self.gInstance["serverid"])
                self.nova.deleteUTM(provisionseq, self.gInstance["serverid"])
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 삭제", "nova.deleteServer", self.gInstance["serverid"], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")           
                
                time.sleep(5)

            if "floatingipid" in self.gInstance:
#                 print "floatingipid -> %s"%self.gInstance["floatingipid"]
                result_deallocate = self.nova.deallocateFloatingIp(self.gInstance["floatingipid"])   
                log.debug("deallocateFloatingIp -> %s"%result_deallocate) 
#                 print "result_deallocate -> %s"%result_deallocate
#                 print "global_mgmt_net_floating_ip -> %s"%self.gInstance["global_mgmt_net_floating_ip"]
#                 result_remove = self.nova.removeFloatingIp(self.gInstance["serverid"], self.gInstance["global_mgmt_net_floating_ip"])
#                 print "result_remove -> %s"%result_remove
                            
            if "nic" in self.gInstance:
                for i, nic in enumerate(self.gInstance["nic"]):
#                     print self.gInstance["subnets"][i]
#                     print self.gInstance["subnets"][i][nic]
                    for index in range(0, self.gInstance["subnets"][i][nic]+1) :
                        key_id = "subnetwork_"+nic+"_"+str(index)+"_id"
                        if key_id in self.gInstance:
                            start_dttm = datetime.datetime.now()
                            message = u"%s 서브네트워크를 삭제합니다."%nic
                            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "52", message)
                            self.execute_set(myquery)
                            self.neutron.deleteSubnet(self.gInstance[key_id])
                            end_dttm = datetime.datetime.now()
                            message = u"%s 서브네트워크 삭제"%nic
                            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "nova.deleteSubnet", self.gInstance[key_id], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                  
                            time.sleep(1)
    
                for nic in self.gInstance["nic"]:
                    start_dttm = datetime.datetime.now()
                    message = u"%s 네트워크를 삭제합니다."%nic
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "52", message)
                    self.execute_set(myquery)
                    key_id = "network_"+nic+"_id"
                    self.neutron.deleteNetwork(self.gInstance[key_id])
                    end_dttm = datetime.datetime.now()
                    message = u"%s 네트워크 삭제"%nic
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "nova.deleteSubnet", self.gInstance[key_id], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                  
                    time.sleep(1)
            
            result = self.updatedbfordeleteprovision(provisionseq, orgseq, userid)   

            end_dttm_provision = datetime.datetime.now()
            message = "VIM Manager 서비스 종료(%s)"%backuptype
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, e.message, False , "")
                                                              
            return {"result":"fail","description": e.message}

    def confirminstance(self, provisionseq, userid, vmid):
        try:
            start_dttm = datetime.datetime.now()
            """
            인스턴스 조회 getserver()
            """
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "5", u"생성된 네트워크 상태를 확인합니다.")
            self.execute_set(myquery)
                    
            networks = self.neutron.listNetworks()

            response = json.dumps(networks)
            if ("result" in networks and networks["result"] == "fail"):
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"네트워크 상태  확인", "neutron.listNetworks()", "", start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                raise Exception(u"네트워크조회 실패")
            else:
                greennetworkname = self.neutron.getNetwork(self.gInstance["greennetworkid"])["network"]["name"]
                orangenetworkname = self.neutron.getNetwork(self.gInstance["orangenetworkid"])["network"]["name"]
                red = 0
                glob = 0
                green = 0
                orange = 0
                
                for row in networks["networks"]:
                    if row["name"] == "red_shared_public_net":
                        red = 1
                for row in networks["networks"]:
                    if row["name"] == "global_mgmt_net":
                        glob = 1                 
                for row in networks["networks"]:
                    if row["name"] == greennetworkname:
                        green = 1
                for row in networks["networks"]:
                    if row["name"] == orangenetworkname:
                        orange = 1                                                   
                if red == 0:
                    raise Exception(u"red_shared_public_net 조회 실패")       
                elif glob == 0:                    
                    raise Exception(u"global_mgmt_net 조회 실패")     
                elif green == 0:
                    raise Exception(u"그린네트워크 조회 실패")                          
                elif orange == 0:
                    raise Exception(u"오렌지네트워크 조회 실패")       
            
                end_dttm = datetime.datetime.now()
                
                message_detail = u"인터넷IP주소:"+self.gInstance["red_shared_public_net"] +u" 고객IP주소:"+self.gInstance["green_net"]+u" 서버IP주소"+self.gInstance["orange_net"]
                       
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"네트워크 상태확인", "neutron.listNetworks()", "", start_dttm, end_dttm, end_dttm-start_dttm, "", True , message_detail)
            result = {"result": "success", "description":""}

            
        except Exception,e:
#             print e.message
            log.error("vim manager-confirminstance error %s"%e.message)
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "5", u"생성된 네트워크 상태확인이 실패되었습니다.")
            self.execute_set(myquery)
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"네트워크 상태확인", "neutron.listNetworks()", "", start_dttm, end_dttm, end_dttm-start_dttm, e.message, False , "")                        
            result = {"result": "fail", "description": "vim manager-confirminstance error "+e.message}
        
        return result
                    
    def checkaccount(self, reqdata):
        try:
            orgseq = reqdata["orgseq"]
            userid = reqdata["userid"]
            username = reqdata["username"]
            password = reqdata["password"]
            backuputmseq = reqdata["backuputmseq"]
                    
            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
    
            aes = AESCipher(AESCipherKey['key'])
            
            host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"])    
            tenantname = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["tenantid"])
            tenantusername = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["userid"])
            tenantpassword = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["password"])
            authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
                               
            myquery = self.sql.getuserinfoforbackuputm(userid, orgseq)
            row = self.execute_getone(myquery)
            usercount = row[0]["usercount"]
            
            if usercount == 0:            
                myquery = self.sql.setuserinfoforbackuputm(userid, password, username, 3, orgseq)
            else:
                myquery = self.sql.setchangeuserinfoforbackuputm(userid, password, username, 3, orgseq)
            self.execute_set(myquery)
            userinfo = collections.OrderedDict()
            userinfo["userid"] = userid
            userinfo["password"] = password
    
            keystone = KeystoneClient()
            keystone.authenticate(host, tenantname, tenantusername, tenantpassword, authurl)  
            user = keystone.getUserByName(userid)        
#             print "checkcount user result %s"%user
            if ("result" in user and user["result"] == "fail"):
                result = self.createaccountforbackuputm(backuputmseq, orgseq, userinfo)
    
            else:
                nova = NovaClient()
                nova.authenticate(host, userid, userid, password, authurl)      
                keypair = nova.getKeypair(userid+"-keypair")        
#                 print "checkcount keypair result %s"%keypair    
                if ("result" in keypair and keypair["result"] == "fail"):
                    role = keystone.getRoleByRoleName("member")
                    if("result" in role and role["result"] == "fail"):
                        raise Exception("fail to get role")
                    print "get role %s"%role
                    keystone.addRoleToUser(user['user']['tenantId'], user["user"]["id"], role["id"])
                    nova = NovaClient()
                    nova.authenticate(host, userinfo["userid"], userinfo["userid"], userinfo["password"], authurl)
                    keypair = nova.createKeypair(userinfo["userid"]+"-keypair")
                    if("result" in keypair and keypair["result"] == "fail"):
                        raise Exception("fail to create keypair")
#                     print "created keypair %s"%keypair
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}
                else:
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}

        except Exception, e:
            result = e.message
            result = {"result":"fail", "description": "checkaccount fail "+e.message}
        finally:
            return result

    def createcheckaccount(self, reqdata):
        try:
            orgseq = reqdata["orgseq"]
            userid = reqdata["userid"]
            username = reqdata["username"]
            password = reqdata["password"]
            backuputmseq = reqdata["backuputmseq"]
                    
            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
    
            aes = AESCipher(AESCipherKey['key'])
            
            host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"])    
            tenantname = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["tenantid"])
            tenantusername = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["userid"])
            tenantpassword = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["password"])
            authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
                               
            myquery = self.sql.getuserinfoforbackuputm(userid, orgseq)
            row = self.execute_getone(myquery)
            usercount = row[0]["usercount"]
            
            if usercount == 0:            
                myquery = self.sql.setuserinfoforbackuputm(userid, password, username, 2, orgseq)
            else:
                myquery = self.sql.setchangeuserinfoforbackuputm(userid, password, username, 2, orgseq)
            self.execute_set(myquery)
            userinfo = collections.OrderedDict()
            userinfo["userid"] = userid
            userinfo["password"] = password
    
            keystone = KeystoneClient()
            keystone.authenticate(host, tenantname, tenantusername, tenantpassword, authurl)  
            user = keystone.getUserByName(userid)        
#             print "checkcount user result %s"%user
            if ("result" in user and user["result"] == "fail"):
                result = self.createaccountforbackuputm(backuputmseq, orgseq, userinfo)
    
            else:
                nova = NovaClient()
                nova.authenticate(host, userid, userid, password, authurl)      
                keypair = nova.getKeypair(userid+"-keypair")        
#                 print "checkcount keypair result %s"%keypair    
                if ("result" in keypair and keypair["result"] == "fail"):
                    role = keystone.getRoleByRoleName("member")
                    if("result" in role and role["result"] == "fail"):
                        raise Exception("fail to get role")
                    print "get role %s"%role
                    keystone.addRoleToUser(user['user']['tenantId'], user["user"]["id"], role["id"])
                    nova = NovaClient()
                    nova.authenticate(host, userinfo["userid"], userinfo["userid"], userinfo["password"], authurl)
                    keypair = nova.createKeypair(userinfo["userid"]+"-keypair")
                    if("result" in keypair and keypair["result"] == "fail"):
                        raise Exception("fail to create keypair")
#                     print "created keypair %s"%keypair
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}
                else:
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}

            log.debug("")
            log.debug("createcheckaccount -> %s"%json.dumps(result))
            log.debug("")

        except Exception, e:
            result = e.message
            result = {"result":"fail", "description": "checkaccount fail "+e.message}
        finally:
            return result
 
    def standbycreatecheckaccount(self, reqdata):
        try:
            orgseq = reqdata["orgseq"]
            userid = reqdata["userid"]
            password = reqdata["password"]
            backuputmseq = reqdata["backuputmseq"]
                    
            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
    
            aes = AESCipher(AESCipherKey['key'])
            
            host = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["host"])    
            tenantname = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["tenantid"])
            tenantusername = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["userid"])
            tenantpassword = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["password"])
            authurl = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["token_url"])

            userinfo = collections.OrderedDict()
            userinfo["userid"] = userid
            userinfo["password"] = password
    
            keystone = KeystoneClient()
            keystone.authenticate(host, tenantname, tenantusername, tenantpassword, authurl)  
            user = keystone.getUserByName(userid)        
#             print "checkcount user result %s"%user
            if ("result" in user and user["result"] == "fail"):
                result = self.standbycreateaccountforbackuputm(backuputmseq, orgseq, userinfo)
    
            else:
                nova = NovaClient()
                nova.authenticate(host, userid, userid, password, authurl)      
                keypair = nova.getKeypair(userid+"-keypair")        
#                 print "checkcount keypair result %s"%keypair    
                if ("result" in keypair and keypair["result"] == "fail"):
                    role = keystone.getRoleByRoleName("member")
                    if("result" in role and role["result"] == "fail"):
                        raise Exception("fail to get role")
                    print "get role %s"%role
                    keystone.addRoleToUser(user['user']['tenantId'], user["user"]["id"], role["id"])
                    nova = NovaClient()
                    nova.authenticate(host, userinfo["userid"], userinfo["userid"], userinfo["password"], authurl)
                    keypair = nova.createKeypair(userinfo["userid"]+"-keypair")
                    if("result" in keypair and keypair["result"] == "fail"):
                        raise Exception("fail to create keypair")
#                     print "created keypair %s"%keypair
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}
                else:
                    result = {"result":"success", "description":u"계정확인완료되었습니다."}

            log.debug("")
            log.debug("standbycreatecheckaccount -> %s"%json.dumps(result))
            log.debug("")
            
        except Exception, e:
            result = e.message
            result = {"result":"fail", "description": "checkaccount fail "+e.message}
        finally:
            return result
      
             
    def createaccountforbackuputm(self, backuputmseq, orgseq, userinfo):
        try:              
            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
            aes = AESCipher(AESCipherKey['key'])
            
            host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"] )   
            tenantname = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["tenantid"])
            username = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["userid"])
            password = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["password"])
            authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
        
            keystone = KeystoneClient()
            keystone.authenticate(host, tenantname, username, password, authurl)    
                
            tenant = keystone.createTenant(userinfo["userid"])
            if("result" in tenant and tenant["result"] == "fail"):
#                 print "fail to create tenant"
                raise Exception(tenant["description"])
            print "created Tenant %s"%tenant
            
            user = keystone.createUser(tenant["tenant"]["id"], userinfo["userid"], userinfo["password"])
            if("result" in user and user["result"] == "fail"):
#                 print "fail to create user"
                raise Exception(tenant["description"])
            print "created user %s"%user
            
            role = keystone.getRoleByRoleName("member")
            if("result" in role and role["result"] == "fail"):
#                 print "fail to get role"
                raise Exception(tenant["description"])
            print "get role %s"%role
                        
            keystone.addRoleToUser(tenant['tenant']['id'], user["user"]["id"], role["id"])

            nova = NovaClient()
            nova.authenticate(host, userinfo["userid"], userinfo["userid"], userinfo["password"], authurl)
                            
            keypair = nova.createKeypair(userinfo["userid"]+"-keypair")
            if("result" in keypair and keypair["result"] == "fail"):
#                 print "fail to create keypair"
                raise Exception("fail to create keypair")
            print "created keypair %s"%keypair
        
            result = {"result":"success", "description":u"계정생성완료되었습니다."}
            
        except Exception, e:
            if not keypair == {}:
                nova.deleteKeypair(userinfo["userid"]+"-keypair")
            if not user == {}:
                keystone.deleteUser(userinfo["userid"])
            if not tenant == {}:
                keystone.deleteTenant(tenant["tenant"]["id"])
         
            print e.message
            result = {"result":"fail", "description":e.message}
        
        finally:
            return result

    def standbycreateaccountforbackuputm(self, backuputmseq, orgseq, userinfo):
        try:              
            query = self.sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
            aes = AESCipher(AESCipherKey['key'])
            
            host = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["host"] )   
            tenantname = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["tenantid"])
            username = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["userid"])
            password = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["password"])
            authurl = aes.decrypt(rows[0]["infraprops"]["backup_openstack_connect"]["token_url"])
        
            keystone = KeystoneClient()
            keystone.authenticate(host, tenantname, username, password, authurl)    
                
            tenant = keystone.createTenant(userinfo["userid"])
            if("result" in tenant and tenant["result"] == "fail"):
#                 print "fail to create tenant"
                raise Exception(tenant["description"])
            print "created Tenant %s"%tenant
            
            user = keystone.createUser(tenant["tenant"]["id"], userinfo["userid"], userinfo["password"])
            if("result" in user and user["result"] == "fail"):
#                 print "fail to create user"
                raise Exception(tenant["description"])
            print "created user %s"%user
            
            role = keystone.getRoleByRoleName("member")
            if("result" in role and role["result"] == "fail"):
#                 print "fail to get role"
                raise Exception(tenant["description"])
            print "get role %s"%role
                        
            keystone.addRoleToUser(tenant['tenant']['id'], user["user"]["id"], role["id"])

            nova = NovaClient()
            nova.authenticate(host, userinfo["userid"], userinfo["userid"], userinfo["password"], authurl)
                            
            keypair = nova.createKeypair(userinfo["userid"]+"-keypair")
            if("result" in keypair and keypair["result"] == "fail"):
#                 print "fail to create keypair"
                raise Exception("fail to create keypair")
            print "created keypair %s"%keypair
        
            result = {"result":"success", "description":u"계정생성완료되었습니다."}
            
        except Exception, e:
            if not keypair == {}:
                nova.deleteKeypair(userinfo["userid"]+"-keypair")
            if not user == {}:
                keystone.deleteUser(userinfo["userid"])
            if not tenant == {}:
                keystone.deleteTenant(tenant["tenant"]["id"])
         
            print e.message
            result = {"result":"fail", "description":e.message}
        
        finally:
            return result
                
        
    def createaccount(self, provisionseq, userinfo):
        try:            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정 생성중입니다.")
            self.execute_set(myquery)
    
            tenant = self.keystone.createTenant(userinfo[0]["userid"])
            if("result" in tenant and tenant["result"] == "fail"):
#                 print "fail to create tenant"
                raise Exception("fail to create tenant")
#             print "created Tenant %s"%tenant
            
            user = self.keystone.createUser(tenant["tenant"]["id"], userinfo[0]["userid"], userinfo[0]["password"])
            if("result" in user and user["result"] == "fail"):
#                 print "fail to create user"
                raise Exception("fail to create user")
#             print "created user %s"%user
            
            role = self.keystone.getRoleByRoleName("member")
            if("result" in role and role["result"] == "fail"):
#                 print "fail to get role"
                raise Exception("fail to get role")
#             print "get role %s"%role
                        
            self.keystone.addRoleToUser(tenant['tenant']['id'],  user["user"]["id"], role["id"])
            
            self.nova.authenticate(self.host, userinfo[0]["userid"], userinfo[0]["userid"], userinfo[0]["password"], self.authurl)
                            
            keypair = self.nova.createKeypair(userinfo[0]["userid"]+"-keypair")
            if("result" in keypair and keypair["result"] == "fail"):
#                 print "fail to create keypair"
                raise Exception("fail to create keypair")
#             print "created keypair %s"%keypair
                
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "1", u"계정 생성 완료되었습니다.")
            self.execute_set(myquery)
            
        except Exception, e:
            print e.message
        
        return tenant, keypair  

    def createnetwork(self, nic, tenantid, provisionseq, userid, userinfo, vlan):
        try:
            result = False
            start_dttm = datetime.datetime.now()
            message = u"%s 네트워크 생성중입니다."%nic
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "2", message)
            self.execute_set(myquery)
#             greennetworkname = userid+"_GREEN_NET_"+ str(uuid.uuid4())
            networkname = userid+"_"+ nic +"_net_"+ str(provisionseq)
#             self.neutron.authenticate(userid, userid, userinfo[0]["password"], self.nova.TOKEN_URL)  
#             request, network = self.neutron.createNetwork(tenantid, networkname, "vlan", "physnet_hybrid", vlan, True)
            if nic == "eth1":
                vlan_type = "physnet_wan"
            else:
                vlan_type = "physnet_lan"
            request, network = self.neutron.createNetwork(tenantid, networkname, "vlan", vlan_type, vlan, True)
#             print "network %s"%network
            
            cmd = json.dumps(request)
            response = json.dumps(network)
            message = u"%s 네트워크생성"%nic
            if ("result" in network and network["result"] == "fail"):                
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "neutron.createNetwork()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                raise Exception(network["description"])     
            else:
                end_dttm = datetime.datetime.now()
                key_id = "network_"+nic+"_id"
                key_name = "network_"+nic+"_name"
                self.gInstance[key_id] = network["network"]["id"]
                self.gInstance[key_name] = networkname             
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "neutron.createNetwork()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
            message = u"%s 네트워크 생성되었습니다."%nic
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "2", message)     
            self.execute_set(myquery)
            result = True
                                
        except Exception, e:
#             print e
#             print e.message
            result = False
                    
        return result, network   

    def createsubnetwork(self, index, nic, tenantid, provisionseq, userid, userinfo, cidr, networkid):
        try:
            result = False
            start_dttm = datetime.datetime.now()
#             greensubnetworkname = userid +"_GREEN_SUBNET_"+ str(uuid.uuid4())
            subnetworkname = userid +"_"+nic+"_subnet_"+ str(index)+ "_"+str(provisionseq)
            request, subnetwork = self.neutron.createSubnet(tenantid, subnetworkname, networkid, "4", cidr, False)
#             print "subnetwork %s"%subnetworkname
            cmd = json.dumps(request)   
            response = json.dumps(subnetwork)
            message = u"%s 서브네트워크생성"%nic
            if ("result" in subnetwork and subnetwork["result"] == "fail"):                
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "neutron.createSubnet()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                
                raise Exception(subnetwork)                            
            else:
                end_dttm = datetime.datetime.now()
                key_id = "subnetwork_"+nic+"_"+str(index)+"_id"
                key_name = "subnetwork_"+nic+"_"+str(index)+"_name"
                self.gInstance[key_id] = subnetwork["subnet"]["id"]
                self.gInstance[key_name] = subnetworkname                     
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "neutron.createSubnet()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                
            message = u"%s 네트워크 생성되었습니다."%nic
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "2", message)     
            self.execute_set(myquery)
            result = True
                                
        except Exception, e:
#             print e
#             print e.message
            result = False
                    
        return result, subnetwork   

    def createinstance(self, provisionseq, userid, networks, userinfo, security_group_rules, floatingIP):
        try:
            start_dttm = datetime.datetime.now()
            self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl)
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "4", u"NFV-UTM을 생성중입니다.")
            self.execute_set(myquery)
            servername = userid + "_utm_" + str(provisionseq)
            availabilityZone =  self.availability_zone

            request, server = self.nova.createUTM(provisionseq, self.utm, servername, self.basic_networks, networks, availabilityZone, security_group_rules)
             
            cmd = json.dumps(request)
            response = json.dumps(server)
            if ("result" in server and server["result"] == "fail"):
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 생성", "nova.createUTM()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                raise Exception(u"인스턴스생성완료 실패")
            else:
                self.gInstance["serverid"] = server["server"]["id"]
                self.gInstance["servername"] = servername
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 생성", "nova.createUTM()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                
            
            start_dttm = datetime.datetime.now()
            
            for i in range(1,100,1):
                time.sleep(1)
                serverinfo = self.nova.getServer(server["server"]["id"])
                if "result" in serverinfo and serverinfo["result"] == "fail":
                    continue
                else:
                    if serverinfo["server"]["status"] == "ACTIVE":     
                        end_dttm = datetime.datetime.now()
                        parameter = server["server"]["id"]
                        response = json.dumps(serverinfo)
                        self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 상태확인", "nova.getServer()", parameter, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                                 
                        break
                    else:
                        continue
                if i > 90:
                    end_dttm = datetime.datetime.now()
                    parameter = server["server"]["id"]

                    response = json.dumps(serverinfo)
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 상태확인", "nova.getServer()", parameter, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                    
                    raise Exception("instance alive check fail")
            
            start_dttm = datetime.datetime.now()  
            self.gInstance["global_mgmt_net"] = serverinfo["server"]["addresses"]["global_mgmt_net"][0]["addr"]
#             self.gInstance["red_shared_public_net"] = serverinfo["server"]["addresses"]["red_shared_public_net"][0]["addr"]
            
            print self.gInstance
            if "network_eth1_name" in self.gInstance:
                self.gInstance["network_eth1_ip"] = serverinfo["server"]["addresses"][self.gInstance["network_eth1_name"]][0]["addr"]
                self.gInstance["network_eth1_mac"] = serverinfo["server"]["addresses"][self.gInstance["network_eth1_name"]][0]["OS-EXT-IPS-MAC:mac_addr"]            
            if "network_eth2_name" in self.gInstance:
                self.gInstance["network_eth2_ip"] = serverinfo["server"]["addresses"][self.gInstance["network_eth2_name"]][0]["addr"]
                self.gInstance["network_eth2_mac"] = serverinfo["server"]["addresses"][self.gInstance["network_eth2_name"]][0]["OS-EXT-IPS-MAC:mac_addr"]
            if "network_eth3_name" in self.gInstance:
                self.gInstance["network_eth3_ip"] = serverinfo["server"]["addresses"][self.gInstance["network_eth3_name"]][0]["addr"]
                self.gInstance["network_eth3_mac"] = serverinfo["server"]["addresses"][self.gInstance["network_eth3_name"]][0]["OS-EXT-IPS-MAC:mac_addr"]
            if "network_eth4_name" in self.gInstance:
                self.gInstance["network_eth4_ip"] = serverinfo["server"]["addresses"][self.gInstance["network_eth4_name"]][0]["addr"]
                self.gInstance["network_eth4_mac"] = serverinfo["server"]["addresses"][self.gInstance["network_eth4_name"]][0]["OS-EXT-IPS-MAC:mac_addr"]     
            if "network_eth5_name" in self.gInstance:
                self.gInstance["network_eth5_ip"] = serverinfo["server"]["addresses"][self.gInstance["network_eth5_name"]][0]["addr"]
                self.gInstance["network_eth5_mac"] = serverinfo["server"]["addresses"][self.gInstance["network_eth5_name"]][0]["OS-EXT-IPS-MAC:mac_addr"]

            print "#"*100
            print json.dumps(serverinfo)

            floatingipresult = self.nova.createFloatingIP(floatingIP)
            print "floating IP create result %s"%floatingipresult
            if "result" in floatingipresult and floatingipresult["result"] == "fail":
                raise Exception("floating IP Create fail %s"%floatingipresult["description"])
            else:
                print "after floating IP %s %s"%(server["server"]["id"],floatingipresult["floating_ip"]["ip"])
                result = self.nova.addFloatingIp(server["server"]["id"], floatingipresult["floating_ip"]["ip"])           
                print "floating IP add result %s"%result
#                 self.gInstance["global_mgmt_net"] = floatingipresult["floating_ip"]["ip"]
                self.gInstance["global_mgmt_net_floating_ip"] = floatingipresult["floating_ip"]["ip"]
                self.gInstance["floatingipid"] = floatingipresult["floating_ip"]["id"]
                end_dttm = datetime.datetime.now()  
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 플로팅IP 생성", "nova.createFloatingIP(\"public_net\")", "", start_dttm, end_dttm, end_dttm-start_dttm, json.dumps(floatingipresult), True , "")
                  
#             result_deallocate = self.nova.deallocateFloatingIp(self.gInstance["floatingipid"])    
#             print "result_deallocate -> %s"%result_deallocate
#             result_remove = self.nova.removeFloatingIp(server["server"]["id"], self.gInstance["global_mgmt_net_floating_ip"])
#             print "result_remove -> %s"%result_remove
#              
            
            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "4", u"NFV-UTM이 생성되었습니다.")
            self.execute_set(myquery)        
            result= True    
                                
        except Exception, e:
            end_dttm = datetime.datetime.now()
            con = mdb.connect(self.openstackdbhost, self.openstackdbuserid, self.openstackdbpassword, 'nova')
            cur = con.cursor()
            myquery = self.sql.getopenstacktraceerror(servername)
            cur.execute(myquery)
            traceback = cur.fetchone()
            if traceback == None:
                traceback = ""                   
            if ("result" in server and server["result"] == "fail"):
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 생성", "nova.createUTM()", cmd, start_dttm, end_dttm, end_dttm-start_dttm, e.message, False , traceback)
            else:    
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 상태확인", "nova.getServer()", server["server"]["id"], start_dttm, end_dttm, end_dttm-start_dttm, e.message, False , traceback)
                                                
            print e.message
            result = False
        return result, server

    def updatedbfordeleteprovision(self, provisionseq, orgseq, userid):
        try:
            
            '''
            tb_nsprovisioning 업데이트
            tb_ne 업데이트
            tb_vlan 업데이트
            '''
            myquery = self.sql.setnsprovisioning(provisionseq)
            self.execute_set(myquery)
#             log.info("[%s] tb_nsprovisioning 업데이트완료"%provisionseq)
            myquery = self.sql.setne(provisionseq)
            self.execute_set(myquery)
#             log.info("[%s] tb_ne 업데이트완료 "%provisionseq)
            myquery = self.sql.setvlanfordeleteprovision(orgseq, userid)
            self.execute_set(myquery)
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "50", u"서비스설치가 실패하였습니다.")
            self.execute_set(myquery)
#             log.info("[%s] 서비스 삭제 완료"%provisionseq)
            result = True
        except Exception,e:
            log.error("vim manager error %s"%e.message)
            print e.message
            result = False
        return result

    def deleteInstance(self, provisionseq):
        try: 
            myquery = self.sql.sete2etracemanagerforenddelete(provisionseq)  
            self.execute_set(myquery)
             
            start_dttm_delete = datetime.datetime.now()
            myquery = self.sql.getuserinfofordeleteprovisioning(provisionseq)
            result = self.execute_get(myquery)
            if result["rows"] == []:
                return {"result":"success", "message": u"삭제할 대상이 없습니다."}
            dic = []
            for row in result["rows"]:
                d = dict(zip(result["columns"], row))
                dic.append(d)
            userid = dic[0]["userid"]
            orgseq = dic[0]["orgseq"]
            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "50", u"NFV-UTM 서비스 삭제를 시작합니다.")
            self.execute_set(myquery)   
                            
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"VIM Manager서비스 삭제 시작", "", "", start_dttm_delete, start_dttm_delete, "", "", True , "")

            # 수집프로세스의 utm접속도중 삭제 될 경우 오류 발생으로 인하여 사전 차단의 의미로 DB에 먼저 기록함..
            myquery = self.sql.setnsprovisioning(provisionseq)
            self.execute_set(myquery)
                        
            myquery = self.sql.setne(provisionseq)
            self.execute_set(myquery)
            time.sleep(3)
                         
            for row in dic:
                self.backuptype = row["backuptype"]          
                self.init_openstack_client(orgseq)                 
                if row["restype"] == "instance":        
                    start_dttm = datetime.datetime.now()      
                    self.nova.authenticate(self.host, row["userid"], row["userid"], row["password"], self.authurl)
#                     self.nova.deleteServer(row["resid"])
                    print "resid -> %s"%row["resid"]
                    result = self.nova.deleteUTM(provisionseq, row["resid"])
                    log.debug("delete utm result -> %s"%result)
                    
                    for i in range(1,100,1):
                        result = self.nova.getServer(row["resid"])
                        log.debug("result -> %s"%json.dumps(result))
                        if "result" in result and result["result"] == "success":
                            log.debug("fail status-> %s"%result["server"]["status"])
                            continue
                        else:
                            break
                        if i > 20:
                            log.debug("last status-> %s"%result["server"]["status"])     
                                       
                    end_dttm = datetime.datetime.now()
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 삭제", "nova.deleteServer()", row["resid"], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                    
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "50", u"NFV-UTM 삭제하였습니다.")
                    self.execute_set(myquery)                       
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery)                     
                    
                if len(str(row["floatingip_id"])) > 0:
                    print "floatingipid -> %s"%row["floatingip_id"]
                    result_deallocate = self.nova.deallocateFloatingIp(row["floatingip_id"])    
                    print "result_deallocate -> %s"%result_deallocate
#                     print "global_mgmt_net_floating_ip -> %s"%row["floatingip"]
#                     result_remove = self.nova.removeFloatingIp(row["resid"], row["floatingip"])
#                     print "result_remove -> %s"%result_remove
                            
            time.sleep(5)
            delete_subnetworks = ""
            for row in dic:
                self.backuptype = row["backuptype"]          
                self.init_openstack_client(orgseq)                  
                if row["restype"] == "subnet":
                    start_dttm = datetime.datetime.now()           
                    self.neutron.deleteSubnet(row["resid"])
#                     log.info("[%s] 서브네트워크 삭제 %s"%(provisionseq, row["resid"]))                
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery)
                    delete_subnetworks = delete_subnetworks + row["resname"] + " : "+ row["resid"] + "<br>"       
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"서브네트워크 삭제", "neutron.deleteSubnet()", delete_subnetworks, start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "51", u"서브네트워크를 삭제하였습니다.")
            self.execute_set(myquery)                       
            time.sleep(1)    
            delete_networks = ""
            for row in dic:
                self.backuptype = row["backuptype"]          
                self.init_openstack_client(orgseq)                  
                if row["restype"] == "network":
                    start_dttm = datetime.datetime.now()           
                    self.neutron.deleteNetwork(row["resid"])
#                     log.info("[%s] 네트워크 삭제 %s"%(provisionseq, row["resid"]))                    
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery) 
                    delete_networks = delete_networks + row["resname"] + " : "+ row["resid"] + "<br>"      
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"네트워크 삭제", "neutron.deleteNetwork()", delete_networks, start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "53", u"네트워크를 삭제하였습니다.")
            self.execute_set(myquery)                                  
            time.sleep(1)

                           
            '''
            tb_nsprovisioning 업데이트
            tb_ne 업데이트
            tb_vlan 업데이트
            '''
#             myquery = self.sql.setnsprovisioning(provisionseq)
#             self.execute_set(myquery)
#             print "tb_nsprovisioning 업데이트완료"
#             log.info("[%s] tb_nsprovisioning 업데이트완료"%provisionseq)

#             myquery = self.sql.setne(provisionseq)
#             self.execute_set(myquery)
#             print "tb_ne 업데이트완료"
#             log.info("[%s] tb_ne 업데이트완료 업데이트완료"%provisionseq)

            myquery = self.sql.setvlanfordeleteprovision(dic[0]["orgseq"], userid)
#             print "setvlan query %s"%myquery
            self.execute_set(myquery)
#             print "tb_vlan 업데이트완료"
           
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "55", u"서비스 삭제를 완료하였습니다.")
            self.execute_set(myquery)
#             log.info("[%s] 서비스 삭제 완료"%provisionseq)

            end_dttm_delete = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"VIM Manager서비스 삭제 종료", "", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, "", True , "")

#             end_dttm = datetime.datetime.now()
#             myquery = self.sql.settracehistorychange(end_dttm, provisionseq, orgseq)
#             self.execute_set(myquery)
            
            myquery = self.sql.sete2etracemanagerforend(provisionseq)
            self.execute_set(myquery)
               
            return {"response":{"result":"success", "description":"appliance is deleted."}}
        
        except Exception,e:
            log.error("vim manager error %s"%e.message)
            print e.message
            end_dttm_delete = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"VIM Manager서비스 삭제 종료", "deleteInstance()", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, e.message, False , "")
            result = "fail"
#             result_detail = "resource delete error. %s"%e.message
#             end_dttm = datetime.datetime.now()
#             myquery = self.sql.settracehistorychange(end_dttm, provisionseq, orgseq, False)
#             self.execute_set(myquery)
                            
            return {"response":{"result":"fail", "description":e.message}}

    def deleteInstanceforstandby(self, provisionseq, backuptype):
        try: 
            myquery = self.sql.sete2etracemanagerforenddelete(provisionseq)  
            self.execute_set(myquery)
             
            start_dttm_delete = datetime.datetime.now()
            myquery = self.sql.getuserinfofordeleteprovisioningofstandby(provisionseq)
            result = self.execute_get(myquery)
            if result["rows"] == []:
                return {"result":"success", "message": u"삭제할 대상이 없습니다."}
            dic = []
            for row in result["rows"]:
                d = dict(zip(result["columns"], row))
                dic.append(d)
            userid = dic[0]["userid"]
            orgseq = dic[0]["orgseq"]
            
            self.backuptype = backuptype
            self.init_openstack_client(orgseq) 

            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "50", u"NFV-UTM 서비스 삭제를 시작합니다.")
            self.execute_set(myquery)   
            
            message = "VIM Manager서비스 삭제 시작 %s"%backuptype
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_delete, start_dttm_delete, "", "", True , "")

            # 수집프로세스의 utm접속도중 삭제 될 경우 오류 발생으로 인하여 사전 차단의 의미로 DB에 먼저 기록함..
            myquery = self.sql.setnsprovisioning(provisionseq)
            self.execute_set(myquery)
                        
            myquery = self.sql.setne(provisionseq)
            self.execute_set(myquery)
            time.sleep(3)
                         
            for row in dic:
                if row["restype"] == "instance":        
                    start_dttm = datetime.datetime.now()      
                    self.nova.authenticate(self.host, row["userid"], row["userid"], row["password"], self.authurl)
#                     self.nova.deleteServer(row["resid"])
                    print "resid -> %s"%row["resid"]
                    result = self.nova.deleteUTM(provisionseq, row["resid"])
                    log.debug("delete utm result -> %s"%result)
                    end_dttm = datetime.datetime.now()
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 삭제", "nova.deleteServer()", row["resid"], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                    
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "50", u"NFV-UTM 삭제하였습니다.")
                    self.execute_set(myquery)                       
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery)                     
                    
                if len(str(row["floatingip_id"])) > 0:
                    print "floatingipid -> %s"%row["floatingip_id"]
                    result_deallocate = self.nova.deallocateFloatingIp(row["floatingip_id"])    
                    print "result_deallocate -> %s"%result_deallocate
#                     print "global_mgmt_net_floating_ip -> %s"%row["floatingip"]
#                     result_remove = self.nova.removeFloatingIp(row["resid"], row["floatingip"])
#                     print "result_remove -> %s"%result_remove
                            
            time.sleep(5)
            delete_subnetworks = ""
            for row in dic:
                if row["restype"] == "subnet":
                    start_dttm = datetime.datetime.now()           
                    self.neutron.deleteSubnet(row["resid"])
#                     log.info("[%s] 서브네트워크 삭제 %s"%(provisionseq, row["resid"]))                
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery)
                    delete_subnetworks = delete_subnetworks + row["resname"] + " : "+ row["resid"] + "<br>"       
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"서브네트워크 삭제", "neutron.deleteSubnet()", delete_subnetworks, start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "51", u"서브네트워크를 삭제하였습니다.")
            self.execute_set(myquery)                       
            time.sleep(1)    
            delete_networks = ""
            for row in dic:
                if row["restype"] == "network":
                    start_dttm = datetime.datetime.now()           
                    self.neutron.deleteNetwork(row["resid"])
#                     log.info("[%s] 네트워크 삭제 %s"%(provisionseq, row["resid"]))                    
                    myquery = self.sql.setopenstackresourcedelete(provisionseq, row["resid"], self.backuptype)
                    self.execute_set(myquery) 
                    delete_networks = delete_networks + row["resname"] + " : "+ row["resid"] + "<br>"      
            end_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"네트워크 삭제", "neutron.deleteNetwork()", delete_networks, start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "53", u"네트워크를 삭제하였습니다.")
            self.execute_set(myquery)                                  
            time.sleep(1)

                           
            '''
            tb_nsprovisioning 업데이트
            tb_ne 업데이트
            tb_vlan 업데이트
            '''
#             myquery = self.sql.setnsprovisioning(provisionseq)
#             self.execute_set(myquery)
#             print "tb_nsprovisioning 업데이트완료"
#             log.info("[%s] tb_nsprovisioning 업데이트완료"%provisionseq)

#             myquery = self.sql.setne(provisionseq)
#             self.execute_set(myquery)
#             print "tb_ne 업데이트완료"
#             log.info("[%s] tb_ne 업데이트완료 업데이트완료"%provisionseq)

            myquery = self.sql.setvlanfordeleteprovision(dic[0]["orgseq"], userid)
#             print "setvlan query %s"%myquery
            self.execute_set(myquery)
#             print "tb_vlan 업데이트완료"
           
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "55", u"서비스 삭제를 완료하였습니다.")
            self.execute_set(myquery)
#             log.info("[%s] 서비스 삭제 완료"%provisionseq)
            message = "VIM Manager서비스 삭제 종료 %s"%backuptype
            end_dttm_delete = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, "", True , "")

#             end_dttm = datetime.datetime.now()
#             myquery = self.sql.settracehistorychange(end_dttm, provisionseq, orgseq)
#             self.execute_set(myquery)
            if backuptype == "standby" or backuptype == "":
                myquery = self.sql.sete2etracemanagerforend(provisionseq)
                self.execute_set(myquery)
               
            return {"response":{"result":"success", "description":"appliance is deleted."}}
        
        except Exception,e:
            log.error("vim manager error %s"%e.message)
            message = "VIM Manager서비스 삭제 종료 %s"%backuptype
            end_dttm_delete = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "deleteInstance()", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, e.message, False , "")
            result = "fail"
#             result_detail = "resource delete error. %s"%e.message
#             end_dttm = datetime.datetime.now()
#             myquery = self.sql.settracehistorychange(end_dttm, provisionseq, orgseq, False)
#             self.execute_set(myquery)
                            
            return {"response":{"result":"fail", "description":e.message}}                       
                       
    def deleteInstanceforrollback(self, provisionseq, gInstance):
        try:
            log.debug("#"*100)            
            myquery = self.sql.sete2etracemanagerforenddelete(provisionseq)  
            self.execute_set(myquery)
            
            if "backuptype" in gInstance:
                backuptype = gInstance["backuptype"]
            else:
                backuptype = ""
            
            if backuptype == "standby":
                log.debug("deleteInstanceforstandby[%s]"%backuptype)
                self.deleteInstanceforstandby(provisionseq, "active")
            
            log.debug("deleteInstanceforrollback[%s]"%backuptype)            
                        
            start_dttm_delete = datetime.datetime.now()
            
            userid = gInstance["userinfo"][0]["userid"]
            orgseq = gInstance["userinfo"][0]["orgseq"]
            password = gInstance["userinfo"][0]["password"]
            
            self.backuptype = backuptype
            self.init_openstack_client(orgseq) 
            message = "VIM Manager서비스 삭제 시작 %s"%backuptype    
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_delete, start_dttm_delete, "", "", True , "")
            
            if "serverid" in self.gInstance:
                start_dttm = datetime.datetime.now()
                myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "51", u"NFV-UTM을 삭제합니다.")
                self.execute_set(myquery)                    
                self.nova.authenticate(self.host, userid, userid, password, self.authurl)
#                 self.nova.deleteServer(self.gInstance["serverid"])
                self.nova.deleteUTM(provisionseq, self.gInstance["serverid"])
                end_dttm = datetime.datetime.now()
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", u"NFV-UTM 삭제", "nova.deleteServer", self.gInstance["serverid"], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")           
                time.sleep(1)
            if "floatingipid" in self.gInstance:    
                self.nova.deallocateFloatingIp(self.gInstance["floatingipid"])
            print "3"                    
            if "nic" in self.gInstance:
                for i, nic in enumerate(self.gInstance["nic"]):
                    for index in range(0, self.gInstance["subnets"][i][nic]+1) :
                        key_id = "subnetwork_"+nic+"_"+str(index)+"_id"
                        if key_id in self.gInstance:
                            start_dttm = datetime.datetime.now()
                            message = u"%s 서브네트워크를 삭제합니다."%nic
                            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "52", message)
                            self.execute_set(myquery)
                            self.neutron.deleteSubnet(self.gInstance[key_id])
                            end_dttm = datetime.datetime.now()
                            message = u"%s 서브네트워크 삭제"%nic
                            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "nova.deleteSubnet", self.gInstance[key_id], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                  
                            time.sleep(1)
    
                for nic in self.gInstance["nic"]:
                    start_dttm = datetime.datetime.now()
                    message = u"%s 네트워크를 삭제합니다."%nic
                    myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "52", message)
                    self.execute_set(myquery)
                    key_id = "network_"+nic+"_id"
                    self.neutron.deleteNetwork(self.gInstance[key_id])
                    end_dttm = datetime.datetime.now()
                    message = u"%s 네트워크 삭제"%nic
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "nova.deleteSubnet", self.gInstance[key_id], start_dttm, end_dttm, end_dttm-start_dttm, "", True , "")                  
                    time.sleep(1) 
   
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "55", u"서비스 삭제를 완료하였습니다.")
            self.execute_set(myquery)
            if backuptype == "standby":
                self.updatedbfordeleteprovision(provisionseq, orgseq, userid)  
            end_dttm_delete = datetime.datetime.now()
            message = "VIM Manager서비스 삭제 완료 %s"%backuptype    
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, "", True , "")
            return {"response":{"result":"success", "description":"appliance is deleted."}}
        
        except Exception,e:
            log.error("vim manager error %s"%e.message)
            print e.message
            end_dttm_delete = datetime.datetime.now()
            message = "VIM Manager서비스 삭제 완료 %S"%backuptype    
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "VIM-mgr", message, "deleteInstanceforrollback()", "", start_dttm_delete, end_dttm_delete, end_dttm_delete-start_dttm_delete, e.message, False , "")
            return {"response":{"result":"fail", "description":e.message}}
              
                                         
    def getRequestItemValue(self, itemName, reqdata):
        for row in reqdata:
            if row["name"] == itemName:
                return row["value"]        
        
    def getvnc(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
                
        self.init_openstack_client(orgseq)
        myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
        userinfo = self.execute_getone(myquery)        
        self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl)
        result = self.nova.getVNCConsole(vmid)
        vncurl = result["console"]["url"] 
        return {"result":"success","description":vncurl}
    
    def setexcuteserver(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        action = reqdata["action"]
        
        self.init_openstack_client(orgseq)
        myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
        userinfo = self.execute_getone(myquery)        
        self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl) 
        self.nova.excuteServer(vmid, action)
    
        return {"result":"success","description":""}
        
    def setrebootserver(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        
        self.init_openstack_client(orgseq)
        myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
        userinfo = self.execute_getone(myquery)        
        self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl) 
        self.nova.rebootServer(vmid, "SOFT")  
    
        return {"result":"success","description":""}

    def getlistnetwork(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
#         vmid = reqdata["vmid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        networks = self.neutron.listNetworks()  
    
        return json.dumps(networks)    

    def getsubnetwork(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
        subnetworkid = reqdata["subnetworkid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        network = self.neutron.getSubnet(subnetworkid)
    
        return json.dumps(network)    

    def getportslist(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
        networkid = reqdata["networkid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        ports = self.neutron.listPorts(networkid)
    
        return json.dumps(ports)       

class Openstackapi(PsycopgHelper):

    def __init__(self):
        self.sql = Provisioningsql()
        self.init_config_from_dict_config_file()      
        
    def init_openstack_client(self, orgseq):     
        query = self.sql.getopenstackinfra(orgseq)
        rows = self.execute_getone(query)
        aes = AESCipher(AESCipherKey['key'])
        
        self.host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"])
        self.tenantname = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["tenantid"])
        self.username = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["userid"])
        self.password = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["password"])
        self.authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
        self.utm =  rows[0]["infraprops"]["nfv_products"]
        self.basic_networks = rows[0]["infraprops"]["shared_networks"]

        for row in self.utm:
            if row["name"] == "utm":
                self.availability_zone = row["defaultconf"]["availability_zone"]
        self.keystone = KeystoneClient()
        self.keystone.authenticate(self.host, self.tenantname, self.username, self.password, self.authurl)                     
        self.neutron = NeutronClient()
        self.neutron.authenticate(self.host, self.tenantname, self.username, self.password, self.authurl)       
        self.nova = NovaClient() 
    
                  
    def getvnc(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        
        if "vmtype" in reqdata:
            password = reqdata["password"]
            vmtype = reqdata["vmtype"]
        else:
            vmtype = ""
                
        self.init_openstack_client(orgseq)
        if vmtype == "mgmtvm":        
            self.nova.authenticate(self.host, userid, userid, password, self.authurl)
        else:
            myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
            userinfo = self.execute_getone(myquery)            
            self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl)
        result = self.nova.getVNCConsole(vmid)
        log.debug("getvnc result %s"%result)
        vncurl = result["console"]["url"] 
        return {"result":"success","description":vncurl}
    
    def setexcuteserver(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        action = reqdata["action"]

        self.init_openstack_client(orgseq)
        
        if userid == "admin":
            password = self.password
        else:            
            
            myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
            userinfo = self.execute_getone(myquery)        
            password = userinfo[0]["password"]
            
        self.nova.authenticate(self.host, userid, userid, password, self.authurl) 
        self.nova.excuteServer(vmid, action)
    
        return {"result":"success","description":""}
        
    def setrebootserver(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        
        self.init_openstack_client(orgseq)

        if userid == "admin":
            password = self.password
        else:            
            
            myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
            userinfo = self.execute_getone(myquery)        
            password = userinfo[0]["password"]
                    
        self.nova.authenticate(self.host, userid, userid, password, self.authurl) 
        self.nova.rebootServer(vmid, "SOFT")  
    
        return {"result":"success","description":""}

    def getlistnetwork(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
#         vmid = reqdata["vmid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        networks = self.neutron.listNetworks()  
    
        return json.dumps(networks)    

    def getsubnetwork(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
        networkid = reqdata["networkid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        network = self.neutron.listSubnetsByNetworkID(networkid)
    
        return json.dumps(network)    

    def getportslist(self, reqdata):
        orgseq = reqdata["orgseq"]
#         userid = reqdata["userid"]
        networkid = reqdata["networkid"]
        
        self.init_openstack_client(orgseq)
#         myquery = self.sql.getuserinfoforcreateprovision(userid)
#         userinfo = self.execute_getone(myquery)        
        
        ports = self.neutron.listPortsByNetworkID(networkid)
    
        return json.dumps(ports)       
    
    def getaddressfromvmid(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        
        self.init_openstack_client(orgseq)
        myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
        userinfo = self.execute_getone(myquery)        
        self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl) 

        server = self.nova.getServer(vmid)
        
#         print " "
#         print json.dumps(server)    
            
        ipAddress = server["server"]["addresses"]["global_mgmt_net"][0]["addr"]
        
        for row in server["server"]["addresses"]["global_mgmt_net"]:
            if row["OS-EXT-IPS:type"] == "floating":
                ipAddress = row["addr"]
                print "%s"%ipAddress
                
        return ipAddress

    def setsecuritygroup(self, reqdata):
        orgseq = reqdata["orgseq"]
        userid = reqdata["userid"]
        vmid = reqdata["vmid"]
        ingress_security_ports = reqdata["ingress_security_port"]
        egress_security_ports = reqdata["egress_security_port"]
               
        self.init_openstack_client(orgseq)

        myquery = self.sql.getprovisionseqfromvmid(vmid)
        provisionseq = self.execute_getone(myquery)        

        if provisionseq == []:
            return {"result":"fail","description":u"vmid가 존재 하지 않습니다."}
        print "provisionseq %s"%provisionseq[0]["provisionseq"]
        
        securitygroupname = "NfvAccessGroup" + "_"+str(provisionseq[0]["provisionseq"])
        print "securitygroupname %s"%securitygroupname
        
        myquery = self.sql.getuserinfoforcreateprovision(userid, orgseq)
        userinfo = self.execute_getone(myquery)        
        self.nova.authenticate(self.host, userid, userid, userinfo[0]["password"], self.authurl) 
                       
        if self.nova.isSecurityGroupByName(securitygroupname) == True:
            securitygroupid = self.nova.getSecurityGroupByName(securitygroupname)
            print "securitygroup %s"%securitygroupid
            sgid = securitygroupid['id']

            self.nova.deleteSecurityGroupRules(sgid)
        else:
            return {"result":"fail","description":u"securityGroup ID가 없습니다."}
        
        """
        create rule in securitygroup
        """
        for row in self.utm:
            if row["name"] == "utm":
                row["defaultconf"]["security_groups"][0]["name"] = securitygroupname
                security_group_rules = row["defaultconf"]["security_groups"][0]["rules"]

        print " "
        print "default security_group_rules %s"%json.dumps(security_group_rules)
                           
        if len(ingress_security_ports) > 0:
            if "any" in ingress_security_ports:
                for portnum in ingress_security_ports["any"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)                        
                    else:
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)
                
            if "tcp" in ingress_security_ports:
                for portnum in ingress_security_ports["tcp"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                    else:
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)
#                         print json.dumps(security_group_rules)  
            if "udp" in ingress_security_ports:
                for portnum in ingress_security_ports["udp"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                    else:
                        security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)                
              
        if len(egress_security_ports) > 0:
            if "any" in egress_security_ports:
                for portnum in egress_security_ports["any"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                    else:                                            
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)            
            if "tcp" in egress_security_ports:
                for portnum in egress_security_ports["tcp"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                    else:
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)
#                         print json.dumps(security_group_rules)  
            if "udp" in egress_security_ports:
                for portnum in egress_security_ports["udp"]:
                    if portnum == "any":
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': "65535", 'port_range_min': "1"}}
                        security_group_rules.append(security_group_rule)
                    else:
                        security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': portnum, 'port_range_min': portnum}}
                        security_group_rules.append(security_group_rule)                     
                
#         print " "
        log.debug("changed --> %s"%json.dumps(security_group_rules))
                
                
#                 if ingress_security_port == "67" or ingress_security_port == "68":
#                     security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': ingress_security_port, 'port_range_min': ingress_security_port}}
#                     security_group_rules.append(security_group_rule)
#                 else:
#                     security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': ingress_security_port, 'port_range_min': ingress_security_port}}
#                     security_group_rules.append(security_group_rule)
#                 
#         if len(egress_security_ports) > 0:
#             for egress_security_port in egress_security_ports:
#                 if ingress_security_port == "67" or ingress_security_port == "68":
#                     security_group_rule = {'security_group_rule': {'direction': 'ingress','protocol': 'udp', 'ethertype': 'IPv4', 'port_range_max': ingress_security_port, 'port_range_min': ingress_security_port}}
#                     security_group_rules.append(security_group_rule)
#                 else:
#                     security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': egress_security_port, 'port_range_min': egress_security_port}}
#                     security_group_rules.append(security_group_rule)

#         security_group_rule = {'security_group_rule': {'direction': 'egress','protocol': 'tcp', 'ethertype': 'IPv4', 'port_range_max': '65535', 'port_range_min': '1'}}
#         security_group_rules.append(security_group_rule)

        self.nova.createSecurityGroupRulesForUTM(securitygroupid['id'], security_group_rules)            
                                          
        return {"result":"success","description":""}     