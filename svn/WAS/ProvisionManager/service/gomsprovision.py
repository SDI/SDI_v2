#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 3. 17.
@summary: 네트워크 추가(6개의 nic)로 인한 연동 규격 변경 및 수정
@author: umisue
'''
import sys,os,json, datetime

from manager.e2emanager import E2emanager
from manager.vimmanager import VimManager
from manager.vnfmanager import VnfManager
from manager.testmanager import TestManager

from sql.provisioningsql import Provisioningsql
from helper.psycopg_helper import PsycopgHelper
from helper.logHelper import myLogger

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='startprovision', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class GomsProvision(PsycopgHelper):

    def __init__(self):
        self.sql = Provisioningsql()
        self.e2e = E2emanager()
        self.init_config_from_dict_config_file()      
        
    def gomsprovision(self, reqdata):
        vim = VimManager()
        vnf = VnfManager()
        test = TestManager()
        
        log.debug("reqdata[%s]-> %s"%(len(json.dumps(reqdata)),json.dumps(reqdata)))
        log.debug("#"*80)
        
        ncheckRollback = 1
        
        try:
            provisionseq = reqdata["provision"]["provisionseq"]
            templateseq = reqdata["provision"]["templateseq"]
            application = reqdata["provision"]["application"]
            orgseq = reqdata["provision"]["orgseq"]
            userid = reqdata["provision"]["userid"]

            myquery = self.sql.getcustomername(userid)
            rows = self.execute_getone(myquery)
            if not rows == []:
                customername = rows[0]["customername"]
            else:
                customername = ""
    
            start_dttm_provision = datetime.datetime.now()
            message = u"%s %s 프로비저닝."%(customername,userid)
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "", message, "", json.dumps(reqdata), start_dttm_provision, start_dttm_provision, "", "", True, userid)
#             self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "", "provisioning을 종료합니다", "", "", start_dttm_provision, time_dttm, time_dttm-start_dttm_provision, "", True, "")               
            if provisionseq == 0:
                myquery, resultquery = self.sql.setnsprovisioningforcreateprovision(userid, orgseq, templateseq)
                self.execute_set(myquery)
                result = self.execute_getone(resultquery)
                provisionseq = result[0]["nsprovisioningseq"]

                for index, row in enumerate(application):
                    log.debug("##############################################")      
                    log.debug("application[%s] --> %s"%(index, json.dumps(row)))
                    # virtual Firewall UTM
                    if row["neclsseq"] == 32:
                        """
                        inframanager
                        """
                        infraresult = vim.managevim(index, provisionseq, reqdata)                     
                        if infraresult["result"] == "fail":
                            raise Exception(infraresult["description"])
                        myquery = self.sql.setneforcreateprovision(provisionseq, templateseq, infraresult["gInstance"]["serverid"], infraresult["gInstance"]["serverid"], application[index]["neclsseq"], application[index]["necatseq"], json.dumps(reqdata["provision"]["application"][index]))
                        self.execute_set(myquery)  
                        vnfresult, configinfo = vnf.managevnf(orgseq, provisionseq, userid, infraresult["gInstance"], application[index]["service"][0]["item"])
                        if vnfresult["result"] == "fail":
                            """
                            rollback
                            """
                            vim.deleteInstanceforrollback(provisionseq, infraresult["gInstance"])
                            ncheckRollback = 0
                            raise Exception(result["description"])
                        testresult = test.managetest(provisionseq, orgseq, userid, infraresult["gInstance"], configinfo)
                        if testresult["result"] == "fail":
                            """
                            rollback
                            """
                            vim.deleteInstanceforrollback(provisionseq, infraresult["gInstance"])
                            ncheckRollback = 0
                            raise Exception(testresult["description"])     
                    if row["neclsseq"] < 11 or row["neclsseq"] > 16:
                        ncheckRollback = 1
                        log.debug("updatedb")   
                        if "backuptype" in application[index]:
                            backuptype = application[index]["backuptype"]
                        else:
                            backuptype = ""
                        self.updatedb(orgseq, provisionseq, userid, templateseq, application, reqdata, infraresult, backuptype)         
                        log.debug("updatedb end")      
                    log.debug("##############################################")      
            else:
               
                for index, row in enumerate(application):
                    log.debug("##############################################")      
                    log.debug("application[%s] --> %s"%(index, json.dumps(row)))
                    # virtual Firewall UTM
                    if row["neclsseq"] == 32:
                        infraresult = vim.managevim(index, provisionseq, reqdata)
                        if infraresult["result"] == "fail":
                            raise Exception(infraresult["description"])
                        myquery = self.sql.setneforcreateprovision(provisionseq, templateseq, infraresult["gInstance"]["serverid"], infraresult["gInstance"]["serverid"], application[index]["neclsseq"], application[index]["necatseq"], json.dumps(reqdata["provision"]["application"][index]))
                        self.execute_set(myquery)  
                        vnfresult, configinfo = vnf.managevnf(orgseq, provisionseq, userid, infraresult["gInstance"], application[index]["service"][0]["item"])
                        print "%s"%vnfresult
                        if vnfresult["result"] == "fail":
                            vim.deleteInstanceforrollback(provisionseq, infraresult["gInstance"])
                            ncheckRollback = 0
                            raise Exception(vnfresult["description"])
                        testresult = test.managetest(provisionseq, orgseq, userid, infraresult["gInstance"], configinfo)
                        if testresult["result"] == "fail":
                            vim.deleteInstanceforrollback(provisionseq, infraresult["gInstance"])
                            ncheckRollback = 0
                            raise Exception(testresult["description"])       
                    if row["neclsseq"] < 11 or row["neclsseq"] > 16:
                        ncheckRollback = 1
                        log.debug("backuptype check")   
                        if "backuptype" in application[index]:
                            backuptype = application[index]["backuptype"]
                        else:
                            backuptype = ""
                        log.debug("updatedb")                   
                        self.updatedb(orgseq, provisionseq, userid, templateseq, application, reqdata, infraresult, backuptype)
                        log.debug("updatedb end")      
                    log.debug("##############################################")      
            response_status = True
            result = {"response":{"result":"success", "provisionseq":provisionseq}}
#             raise Exception("test")
        except Exception, e:
            print e.message    
            response_status = False
            if ncheckRollback == 1 and "gInstance" in infraresult:
                print infraresult["gInstance"]
                vim.deleteInstanceforrollback(provisionseq, infraresult["gInstance"])       
            result = {"response":{"result":"fail", "description": e.message, "provisionseq":provisionseq}}
        
        finally:
            time_dttm = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "", "프로비저닝을 종료합니다", "", "", start_dttm_provision, time_dttm, time_dttm-start_dttm_provision, "", True, "")
            myquery = self.sql.settracehistorychange(time_dttm, provisionseq, orgseq, response_status)
            self.execute_set(myquery)
            
            myquery = self.sql.sete2etracemanagerforend(provisionseq)
            self.execute_set(myquery)
            
            return result   

    def gomsprovisiondelete(self, provisionseq):
        vim = VimManager()
        
        result = vim.deleteInstance(provisionseq)
        
        return result    
    def getreadyprovisionseq(self, reqdata):
        try:
            templateseq = reqdata["templateseq"]
            orgseq = reqdata["orgseq"]
            userid = reqdata["userid"]  
                
            myquery, resultquery = self.sql.setnsprovisioningforcreateprovision(userid, orgseq, templateseq)
            self.execute_set(myquery)
            result = self.execute_getone(resultquery)
            provisionseq = result[0]["nsprovisioningseq"] 
        
            result = {"response":{"result":"success", "provisionseq":provisionseq, "description": "your request is success"}}
        except Exception, e:
            print e.message
            result = {"response":{"result":"fail", "description": e.message}}
            
        return result          
    
    def updatedb(self, orgseq, provisionseq, userid, templateseq, application, reqdata, infraresult, backuptype):
#         vmid = infraresult["gInstance"]["vmid"]
#         neclsseq = application[0]["neclsseq"]
#         necatseq = application[0]["necatseq"]
        
#         myquery = self.sql.setneforcreateprovision(provisionseq, templateseq, vmid, vmid, neclsseq, necatseq, json.dumps(reqdata["provision"]["application"][0]))
#         self.execute_set(myquery)
        if "backuptype" in infraresult["gInstance"] and infraresult["gInstance"]["backuptype"] == "standby":
            backuptype = infraresult["gInstance"]["backuptype"]
        elif "backuptype" in infraresult["gInstance"] and infraresult["gInstance"]["backuptype"] == "active":
            backuptype = infraresult["gInstance"]["backuptype"]
        else:
            backuptype = ""
        
        if "serverid" in infraresult["gInstance"]:
            myquery = self.sql.setopenstackresource(userid, orgseq, "instance", infraresult["gInstance"]["serverid"], infraresult["gInstance"]["servername"], provisionseq, infraresult["gInstance"]["floatingipid"], backuptype)
            self.execute_set(myquery)
        if "nic" in infraresult["gInstance"]:
            for i, nic in enumerate(infraresult["gInstance"]["nic"]):
                for index in range(0, infraresult["gInstance"]["subnets"][i][nic]+1) :
                    key_id = "subnetwork_"+nic+"_"+str(index)+"_id"
                    key_name = "subnetwork_"+nic+"_"+str(index)+"_name"
                    if key_id in infraresult["gInstance"]:
                        myquery = self.sql.setopenstackresource(userid, orgseq, "subnet", infraresult["gInstance"][key_id], infraresult["gInstance"][key_name], provisionseq, "", backuptype)
                        self.execute_set(myquery)
            for nic in infraresult["gInstance"]["nic"]:
                key_id = "network_"+nic+"_id"
                key_name = "network_"+nic+"_name"
                myquery = self.sql.setopenstackresource(userid, orgseq, "network", infraresult["gInstance"][key_id], infraresult["gInstance"][key_name], provisionseq, "", backuptype)
                self.execute_set(myquery)            
#         consolurl = "https://"+infraresult["gInstance"]["global_mgmt_net"]+":10443"
        consolurl = "https://"+infraresult["gInstance"]["global_mgmt_net_floating_ip"]+":10443"
#         consolurl = "https://"+infraresult["gInstance"]["global_mgmt_net_floating_ip"]+":10443"
        myquery = self.sql.setneprovisionforcomplete(provisionseq, consolurl, backuptype)
        print myquery
        self.execute_set(myquery)
        myquery = self.sql.setnsprovisioningforcomplete(provisionseq)
        self.execute_set(myquery)        
        