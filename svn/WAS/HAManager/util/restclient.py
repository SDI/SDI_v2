# -*- coding: utf8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 3. 2.
@summary:
@author: 최재복
'''
import json

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders


class SoapClient():
    def senddata(self, url, httpmethod, reqbody):
        print url
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        if httpmethod == "post":
            request = HTTPRequest(url=url, headers=h, method=httpmethod.upper(), body=reqbody, validate_cert=False, request_timeout=30)
        elif httpmethod == "delete" :
            request = HTTPRequest(url=url+reqbody, headers=h, method=httpmethod.upper(), body=None)
        elif httpmethod == "get" :
            request = HTTPRequest(url=url, headers=h, method=httpmethod.upper(),  body=None, validate_cert=False, request_timeout=30)            
            print url
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            print response.body
            return json.loads(response.body)

        except httpclient.HTTPError as e:
#             print("Error: " + str(e))
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse
        except Exception as e:
#             print("Error: " + str(e))
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse


# if __name__ == '__main__':
#     soapclient = SoapClient()
#     soapclient.sendData("http://www.google.com/", "GET", None)

#     http_client = httpclient.HTTPClient()
#     try:
#         response = http_client.fetch("http://www.google.com/")
#         print response.body
#     except httpclient.HTTPError as e:
#         # HTTPError is raised for non-200 responses; the response
#         # can be found in e.response.
#         print("Error: " + str(e))
#     except Exception as e:
#         # Other errors are possible, such as IOError.
#         print("Error: " + str(e))
#     http_client.close()
