# -*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 3. 4.
@summary:
@author: 최재복
'''
import datetime
import sys, os, time, json

from helper.logHelper import myLogger
from helper.psycopg_helper import PsycopgHelper
from sql.hamanagersql import HaMnagerSql
from datetime import datetime

org_dir = os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)

log = myLogger(tag='tracemanager', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class TraceManager(PsycopgHelper):

    def __init__(self):
        '''
        Constructor
        '''
        self.sql = HaMnagerSql()
        self.init_config_from_dict_config_file()

    def tracehistory(self, start_dttm, end_dttm, traceseq, officecode):
        try:

            orgseqsql = self.sql.get_org_seq(officecode)
            rows, rowcount = self.execute_getone(orgseqsql)
            orgseq = None

            if rowcount == 0:
                orgseq = ""
            else:
                orgseq = rows[0]["orgseq"]

            resultquery = self.sql.set_trace_history(start_dttm, end_dttm, traceseq, orgseq)
            self.execute_set(resultquery)

        except Exception, e:
            log.error("tracehistory Exception[%s][%s][%s][%s][%s]" % (start_dttm, end_dttm, traceseq, officecode, e.message))

    def insert_tracehistory(self, stepmsg, start_dttm, traceseq, officecode):
        try:

            orgseqsql = self.sql.get_org_seq(officecode)
            rows, rowcount = self.execute_getone(orgseqsql)
            orgseq = None

            if rowcount == 0:
                orgseq = ""
            else:
                orgseq = rows[0]["orgseq"]

            resultquery = self.sql.insert_trace_history(stepmsg, start_dttm, traceseq, orgseq)
            print resultquery
            result = self.insert_one(resultquery)
            return result[0][0]

        except Exception, e:
            log.error("tracehistory Exception[%s][%s][%s][%s][%s]" % (stepmsg, start_dttm, traceseq, officecode, e.message))

    def update_tracehistory(self, histseq, responsestatus):
        try:
            resultquery = self.sql.update_trace_history(histseq, responsestatus, datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            self.execute_getone(resultquery)
        except Exception, e:
            log.error("tracehistory Exception[%s][%s][%s][%s]" % (histseq, responsestatus, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), e.message))

    def haseq_max(self):
        try:

            resultquery = self.sql.get_ha_seq_max()
            rows, rowcount = self.execute_getone(resultquery)
            haseq = None

            if rowcount == 0:
                haseq = 1
            else:
                haseq = int(rows[0]["haseq"]) + 1

        except Exception, e:
            log.error("haseq_max Exception[%s]" % (e.message))
            haseq = 1

        return haseq

    def hamgr_trace(self, haseq, mainclass, subclass, message, request_cmd, request_parameter, response, response_status, start_dttm, end_dttm, message_detail):
        try:
            resultquery = self.sql.set_hamgr_trace(haseq, mainclass, subclass, message, request_cmd, request_parameter, response, response_status, start_dttm, end_dttm, message_detail)
            self.execute_set(resultquery)

        except Exception, e:
            log.error("hamgr_trace Exception[%s][%s][%s][%s][%s][%s][%s][%s][%s][%s][%s][%s]" % (haseq, mainclass, subclass, message, request_cmd, request_parameter, response, response_status, start_dttm, end_dttm, message_detail, e.message))

