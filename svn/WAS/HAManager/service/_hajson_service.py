#-*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 27.
@summary:
@author: 최재복
'''
import sys,os,json
import time, datetime
import paramiko

from helper.logHelper import myLogger
from manager.hamanager import HaManager
from manager.tracemanager import TraceManager
from sql.hamanagersql import HaMnagerSql
from helper.psycopg_helper import PsycopgHelper
from datetime import datetime
from util.restclient import SoapClient
from config.connect_config import mss
from util.aescipher import AESCipher

from tornado import httpclient
from tornado.httpclient import HTTPRequest


org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)

log = myLogger(tag='haJsonService', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class HaJsonService(PsycopgHelper):

    def __init__(self):
        self.sql = HaMnagerSql()
        self.init_config_from_dict_config_file()
        self.trace = TraceManager();

    def mssfailover(self, reqdata):
        soap = SoapClient()   
        aes = AESCipher('Qfkrkstkghkwhgdk')         

        officecode = reqdata["failover"]["officecode"]
        axgateip = reqdata["failover"]["axgateip"]
        failmsg = reqdata["failover"]["failmsg"]
        mss_userid = reqdata["failover"]["userid"]        
        
        if reqdata["failover"]["axgateip"] == "211.224.204.160":
            axgateip = "10.0.0.100"
        
        try:
            start_dttm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")           
            #traceseq 추출
            traceseq = self.trace.haseq_max()
            #print json.dumps(reqdata)
            histseq = self.trace.insert_tracehistory("HA Manager(절체)", datetime.now().strftime("%Y-%m-%d %H:%M:%S"), traceseq, officecode)
            print histseq
            self.trace.hamgr_trace(traceseq, "HA-mgr", "failover", "MSS 절체 요청 시작", "ham.mssfailover()", json.dumps(reqdata), "", "True", datetime.now().strftime("%Y-%m-%d %H:%M:%S"), datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")

            if not "failover" in reqdata:
                raise Exception("failover message error")

            log.debug("mssFailOver [%s][%s][%s][%s] instance success"%(officecode, axgateip, failmsg, mss_userid))

            #manager 호출하여 sdn ctrl 호출할 데이터 추출
            hamanager = HaManager();
          
            result_sdn = hamanager.receive_failover('1', officecode, axgateip, traceseq)

            result_wan = False
            if result_sdn["response"]["result"] == "success":
                wan_start_dttm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                result_wan = self.setHostServer(officecode, "failover")
                self.trace.hamgr_trace(traceseq, "HA-mgr", "failover", "WAN 절체", "ifconfig p1p1 up <br> ifconfig p1p2 up", "", "", result_wan, wan_start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
            
            log.debug("sdn response --> %s"%result_sdn)    
            log.debug("sdn result --> %s"%result_sdn["response"]["result"])
            log.debug("cloud result --> %s"%result_wan)
                
            #DB 결과 저장
            if result_sdn["response"]["result"] == "success" and result_wan == True:
                update_sql = self.sql.update_sdn_ip_mapping(axgateip, officecode, '0', '1')
                print update_sql
                self.execute_set(update_sql)
                resultquery = self.sql.insertfailoverinfo("1", officecode, axgateip, failmsg, mss_userid, "T", "")
                self.trace.hamgr_trace(traceseq, "HA-mgr", "failover", "MSS 절체 요청 종료", "", "", json.dumps(result_sdn), "True", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
                tracehissql = self.sql.update_trace_history(histseq, 'S', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
                result = "success"
                description = "success"
            else:
                resultquery = self.sql.insertfailoverinfo("1", officecode, axgateip, failmsg, mss_userid, "F", result_sdn["response"]["description"])
                self.trace.hamgr_trace(traceseq, "HA-mgr", "failover", "MSS 절체 요청 종료", "", "", json.dumps(result_sdn), "False", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
                tracehissql = self.sql.update_trace_history(histseq, 'F', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
                result = "fail"
                print"1"
                description = result_sdn["response"]["description"]        

            self.execute_set(resultquery)

            if axgateip == "10.0.0.100":
                axgateip = "211.224.204.160"       
                               
            url = aes.decrypt(mss["MSS_ENDPOIT"]) + "/api/softutm/set_host_utm_status?axgateip="+axgateip+"&result="+result+"&description="+description
            log.debug("mss url -> %s"%url)
            resultport = soap.senddata(url, "get", "")
            log.debug("mss response -> %s"%resultport)
#             http_client = httpclient.HTTPClient()
#             request = HTTPRequest(url=url, method='GET', body=None, validate_cert=False)    
#             try:
#                 response = http_client.fetch(request=request)
#                 http_client.close()
#                 print response.body
#                 log.debug("response -> %s"%response)
#     
#             except httpclient.HTTPError as e:
#     #             print("Error: " + str(e))
#                 errResponse = {"result":"fail", "description":str(e)}
#     #             result = json.dumps(errResponse)
#                 print errResponse
#             except Exception as e:
#     #             print("Error: " + str(e))
#                 errResponse = {"result":"fail", "description":str(e)}
#     #             result = json.dumps(errResponse)
#                 print errResponse                
# #             resultport = soap.senddata(url, "post", "")
# #             log.debug("response -> %s"%response)
           
        except Exception, e:
            log.error("mssFailOver Exception[%s][%s][%s][%s][%s]"%(officecode, axgateip, failmsg, mss_userid, e.message))

            try:
                #DB 결과 저장
                resultquery = self.sql.insertfailoverinfo("1", officecode, axgateip, failmsg, mss_userid, "F", e.message)
                self.execute_set(resultquery)
                tracehissql = self.sql.update_trace_history(histseq, 'F', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
            except Exception, e:
                log.error("mssFailBack SQLException[%s][%s][%s][%s][%s]"%(officecode, axgateip, failmsg, mss_userid, e.message))

            result = {"response":{"result":"fail", "description": e.message}}

            if axgateip == "10.0.0.100":
                axgateip = "211.224.204.160"   
                            
            url = aes.decrypt(mss["MSS_ENDPOIT"]) + "/api/softutm/set_host_utm_status?axgateip="+axgateip+"&result="+"fail"+"&description="+ e.message
            log.debug("error mss url -> %s"%url)
            resultport = soap.senddata(url, "get", "")
            log.debug("error mss response -> %s"%resultport)
#         finally:
#             dic, rowcount = self.trace.tracehistory(start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), traceseq, officecode)

        return result

    def mssfailback(self, reqdata):
        soap = SoapClient()   
        aes = AESCipher('Qfkrkstkghkwhgdk')

        officecode = reqdata["failback"]["officecode"]
        axgateip = reqdata["failback"]["axgateip"]
        failmsg = reqdata["failback"]["failmsg"]
        mss_userid = reqdata["failback"]["userid"]

        if reqdata["failback"]["axgateip"] == "211.224.204.160":
            axgateip = "10.0.0.100"                          
        try:
            start_dttm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            log.debug("Step1")
            #traceseq 추출
            traceseq = self.trace.haseq_max()

            histseq = self.trace.insert_tracehistory("HA Manager(복구)", datetime.now().strftime("%Y-%m-%d %H:%M:%S"), traceseq, officecode)

            log.debug("Step2")
            self.trace.hamgr_trace(traceseq, "HA-mgr", "failback", "MSS 절체 복구 시작", "ham.mssfailback()", json.dumps(reqdata), "", "True", datetime.now().strftime("%Y-%m-%d %H:%M:%S"), datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
            log.debug("Step3")

            log.debug("mssFailBack [%s][%s][%s][%s] instance success"%(officecode, axgateip, failmsg, mss_userid))

            #manager 호출하여 sdn ctrl 호출할 데이터 추출
            hamanager = HaManager();
#             result = hamanager.failovermanager("2", officecode, axgateip, traceseq)
            result_sdn = hamanager.receive_failover("2", officecode, axgateip, traceseq)
            log.debug("Step4")

            result_wan = False
            
            print "1"
            
            if result_sdn["response"]["result"] == "success":
                wan_start_dttm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                result_wan = self.setHostServer(officecode, "failback")
                if result_wan == True:
                    self.trace.hamgr_trace(traceseq, "HA-mgr", "failback", "WAN 절체 복구", "ifconfig p1p1 down <br> ifconfig p1p2 down", "", "", result_wan, wan_start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
            
            print "2"        
            log.debug("sdn response --> %s"%result_sdn)    
            log.debug("sdn result --> %s"%result_sdn["response"]["result"])
            log.debug("cloud result --> %s"%result_wan)
                

            #DB 결과 저장
            if result_sdn["response"]["result"] == "success" and result_wan == True:
                update_sql = self.sql.update_sdn_ip_mapping(axgateip, officecode, '1', '0')
                print update_sql
                self.execute_set(update_sql)
                resultquery = self.sql.insertfailoverinfo("2", officecode, axgateip, failmsg, mss_userid, "T", "")
                self.trace.hamgr_trace(traceseq, "HA-mgr", "failback", "MSS 절체 복구 종료", "", "", json.dumps(result_sdn), "True", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
                tracehissql = self.sql.update_trace_history(histseq, 'S', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
                result = "success"
                description = "success"                
            else:
                resultquery = self.sql.insertfailoverinfo("2", officecode, axgateip, failmsg, mss_userid, "F", result_sdn["response"]["description"])
                self.trace.hamgr_trace(traceseq, "HA-mgr", "failback", "MSS 절체 복구 종료", "", "", json.dumps(result_sdn), "False", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
                tracehissql = self.sql.update_trace_history(histseq, 'F', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
                result = "fail"
                description = result_sdn["response"]["description"]                 

            log.debug("Step5")
            self.execute_set(resultquery)
            log.debug("Step6")

            if axgateip == "10.0.0.100":
                axgateip = "211.224.204.160"         
            
            url = aes.decrypt(mss["MSS_ENDPOIT"]) + "/api/softutm/set_host_utm_status?axgateip="+axgateip+"&result="+result+"&description="+description
            log.debug("mss url -> %s"%url)
            resultport = soap.senddata(url, "get", "")
            log.debug("mss response -> %s"%resultport)
            
        except Exception, e:
            log.error("mssFailBack Exception[%s][%s][%s][%s][%s]"%(officecode, axgateip, failmsg, mss_userid, e.message))

            try:
                #DB 결과 저장
                resultquery = self.sql.insertfailoverinfo("2", officecode, axgateip, failmsg, mss_userid, "F", e.message)
                self.execute_set(resultquery)
                tracehissql = self.sql.update_trace_history(histseq, 'F', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
                self.trace.execute_set(tracehissql)
            except Exception, e:
                log.error("mssFailBack SQLException[%s][%s][%s][%s][%s]"%(officecode, axgateip, failmsg, mss_userid, e.message))

            result = {"response":{"result":"fail", "description": e.message}}
#         finally:
#             self.trace.tracehistory(start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), traceseq, officecode)

            if axgateip == "10.0.0.100":
                axgateip = "211.224.204.160"       
            
            url = aes.decrypt(mss["MSS_ENDPOIT"]) + "/api/softutm/set_host_utm_status?axgateip="+axgateip+"&result="+"fail"+"&description="+ e.message.replace(' ','')
            log.debug("error mss url -> %s"%url)
            resultport = soap.senddata(url, "get", "")
            log.debug("error mss response -> %s"%resultport)
            
        return result

    def setswitchs(self, reqdata):
        try:
            command = reqdata["command"]
            officecode = reqdata["rcode"]
            mgmt_ip = reqdata["mgmt_ip"]
            switch_dpid = reqdata["switch_dpid"]
            switch_name = reqdata["name"]

            print command, officecode, mgmt_ip, switch_dpid, switch_name

            log.debug("setSwitchs [%s][%s][%s][%s][%s] instance success"%(command, officecode, mgmt_ip, switch_dpid, switch_name))

            #DB 저장
            if command == "add":
                resultquery = self.sql.insertsdnswitch(officecode, mgmt_ip, switch_dpid, switch_name)
            elif command == "update":
                resultquery = self.sql.updatesdnswitch(officecode, mgmt_ip, switch_dpid, switch_name)
            elif command == "delete":
                resultquery = self.sql.deletesdnswitch(switch_dpid)

            queryResult = self.execute_set(resultquery)

            if queryResult == 0:
                result = {"response":{"result":"fail","resultmsg":"Database processing result 0"}}
            else:
                result = {"response":{"result":"success"}}

        except Exception, e:
            log.error("setSwitchs Exception[%s][%s][%s][%s][%s]"%(command, officecode, mgmt_ip, switch_dpid, switch_name))

            result = {"response":{"result":"fail","resultmsg":e.message}}

        return result

    def setappliance(self, reqdata):
        try:
            command = reqdata["command"]
            appliance_id = reqdata["appliance_id"]
            appliance_name = reqdata["appliance_name"]
            appliance_type = reqdata["appliance_type"]
            switch_dpid = reqdata["switch_dpid"]
            port_num = reqdata["port_num"]
            ip_addr = reqdata["ip_addr"]
            officecode = reqdata["officecode"]
            connect_type = reqdata["connect_type"]

            print command, appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type

            log.debug("postAppliance [%s][%s][%s][%s][%s][%s][%s][%s][%s] instance success"%(command, appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type))

            #DB 저장
            if command == "add":
                resultquery = self.sql.insertsdnappliance(appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type)
            elif command == "update":
                resultquery = self.sql.updatesdnappliance(appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type)
            elif command == "delete":
                resultquery = self.sql.deletesdnappliance(appliance_id, port_num)

            queryResult = self.execute_set(resultquery)

            if queryResult == 0:
                result = {"response":{"result":"fail","resultmsg":"DB 처리 결과값이 없습니다."}}
            else:
                result = {"response":{"result":"success"}}

        except Exception, e:
            log.error("postAppliance Exception[%s][%s][%s][%s][%s][%s][%s][%s][%s]"%(command, appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type))

            result = {"response":{"result":"fail","resultmsg":e.message}}

        return result

    def setapliancesmapping(self, reqdata):
        try:
            command = reqdata["command"]
            source_appliance_id = reqdata["active_appliance_id"]
            source_port_num = reqdata["active_appliance_port"]
            dest_appliance_id = reqdata["passive_appliance_id"]
            dest_port_num = reqdata["passive_appliance_port"]

            print command, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num

            log.debug("setApliancesMapping [%s][%s][%s][%s][%s] instance success"%(command, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num))

            if command == "add":
                #tb_sdn_appliance 테이블에 정보 확인
                resultquery = self.sql.getsdnappliancecount(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
                rows, rowcount = self.execute_getone(resultquery)

                if rowcount == 0:
                    return {"response":{"result":"fail","resultmsg":"Mapping 처리할 appliance 정보가 모두 없습니다."}}
                elif rowcount == 1:
                    flag = rows[0]["flag"]
                    if flag == "S":
                        return {"response":{"result":"fail","resultmsg":"Mapping 처리할 passive_appliance 정보가 없습니다."}}
                    else:
                        return {"response":{"result":"fail","resultmsg":"Mapping 처리할 active_appliance 정보가 없습니다."}}

                resultquery = self.sql.insertsdnappliancemapping(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
            elif command == "delete":
                resultquery = self.sql.deletesdnappliancemapping(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
            elif command == "update":
                return  {"response":{"result":"fail","resultmsg":"update는 처리안됩니다. delete후 add 처리해야 됩니다."}}

            queryResult = self.execute_set(resultquery)

            if queryResult == 0:
                result = {"response":{"result":"fail","resultmsg":"DB 처리 결과값이 없습니다."}}
            else:
                result = {"response":{"result":"success"}}

        except Exception, e:
            log.error("setApliancesMapping Exception[%s][%s][%s][%s][%s]"%(command, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num))

            result = {"response":{"result":"fail","resultmsg":e.message}}

        return result

    def setHostServer(self, officecode, cmdtype):     
        try:
            result = False
            if cmdtype == "failover": 
                cmd1 = "ifconfig p1p1 up "
                cmd2 = "ifconfig p1p2 up "
            else:
                cmd1 = "ifconfig p1p1 down "
                cmd2 = "ifconfig p1p2 down "                

            result = self.sendCommandlist(officecode, cmd1, cmd2)
            log.debug("setHostServer-sendCommandlist end %s %s"%(cmd1, cmd2))
        except Exception, e:
            log.error("setHostServer error %s" %e.message)
            result = False
        return result
        
    def sendCommandlist(self, officecode, cmd1, cmd2):
        client = paramiko.SSHClient()
        result = False
        try:
            query = self.sql.getopenstackinfra(officecode)
            rows = self.execute_getonedic(query)
            host = rows[0]["infraprops"]["host_list"][0]["ip"]    
            username = rows[0]["infraprops"]["host_list"][0]["id"]
            password = rows[0]["infraprops"]["host_list"][0]["password"]
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=host, username=username, password=password, timeout=3)
            cmd_list = [cmd1,cmd2]
            req = ';'.join(cmd_list)
            stdin, stdout, stderr = client.exec_command(req)
#             for line in stdout:
#                 result.append(line.replace('\n',''))
#                 print "line %s"%line
#             print "exec_command end"
            result = True
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(host, str(e)))
            sys.exit()
            result = False
        finally:
            client.close()
            return result