#-*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 27.
@summary:
@author: 최재복
'''

class HaMnagerSql(object):
    '''
    HA Manager 관련 DB 쿼리를 집합.

    '''

    def __init__(self):
        '''
        Constructor
        '''

    def insertfailoverinfo(self, failovertype, officecode, axgateip, failmsg, mss_userid, resultcode, resultmsg):

        result = """
        insert into tb_failover_info ( regdt, failovertype, officecode, axgateip, failmsg, mssuserid, resultcode, resultmsg)
        values (now(), '%s', '%s', '%s', '%s', '%s', '%s', '%s')
        """%(failovertype, officecode, axgateip, failmsg, mss_userid, resultcode, resultmsg)
        return result

    def insertsdnswitch(self, offcecode, mgmt_ip, switch_dpid, switch_name):

        result = """
        insert into tb_sdn_switch (switch_dpid, switch_name, mgmt_ip, officecode)
        values ('%s', '%s', '%s', '%s')
        """%(switch_dpid, switch_name, mgmt_ip, offcecode)
        return result

    def updatesdnswitch(self, offcecode, mgmt_ip, switch_dpid, switch_name):
        result = """
                    update tb_sdn_switch set switch_name = '%s', mgmt_ip = '%s', officecode = '%s'
                    where switch_dpid = '%s'
        """ %(switch_name, mgmt_ip, offcecode, switch_dpid)
        return result

    def deletesdnswitch(self, switch_dpid):
        result = """
                    delete from tb_sdn_switch
                    where switch_dpid = '%s'
        """ %(switch_dpid)
        return result

    def insertsdnappliance(self, appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type):
        result = """
        insert into tb_sdn_appliance (appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type)
        values ('%s', '%s', '%s', '%s', %s, '%s', '%s', '%s')
        """%(appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, officecode, connect_type)
        return result

    def updatesdnappliance(self, appliance_id, appliance_name, appliance_type, switch_dpid, port_num, ip_addr, rcode, connect_type):
        result = """
                    update tb_sdn_appliance set appliance_name = '%s', appliance_type = '%s', switch_dpid = '%s', ip_addr = '%s', officecode = '%s', connect_type = '%s', reg_dttm = now()
                    where appliance_id = '%s' and port_num = %s
        """ %(appliance_name, appliance_type, switch_dpid, ip_addr, rcode, connect_type, appliance_id, port_num)
        return result

    def deletesdnappliance(self, appliance_id, port_num):
        result = """
                    delete from tb_sdn_appliance
                    where appliance_id = '%s' and port_num = %s
        """ %(appliance_id, port_num)
        return result

    def insertsdnappliancemapping(self, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num):

        result = """
        insert into tb_sdn_appliance_mapping (source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
        values ('%s', %s, '%s', %s)
        """%(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
        return result

    def deletesdnappliancemapping(self, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num):
        result = """
                    delete from tb_sdn_appliance_mapping
                    where (source_appliance_id = '%s' and source_port_num = %s) or (dest_appliance_id = '%s' and dest_port_num = %s)
        """ %(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
        return result

    def getsdnappliancecount(self, source_appliance_id, source_port_num, dest_appliance_id, dest_port_num):
        result = """
                    select 'S' as flag  from tb_sdn_appliance
                    where appliance_id = '%s' and port_num = %s
                    union
                    select 'D' as flag from tb_sdn_appliance
                    where appliance_id = '%s' and port_num = %s
        """ %(source_appliance_id, source_port_num, dest_appliance_id, dest_port_num)
        return result

    def failoverlist(self, failovertype, officecode, axgateip):

        if failovertype == "1": #절체
            result = """
                        select a2.switch_dpid, m.source_port_num as service_port, m.dest_port_num as transfer_port, a2.connect_type
                        from tb_sdn_appliance as a1
                        inner join tb_sdn_appliance_mapping as m on a1.appliance_id = m.source_appliance_id and a1.port_num = m.source_port_num
                        inner join tb_sdn_appliance as a2 on m.dest_appliance_id = a2.appliance_id and a2.port_num = m.dest_port_num
                        where a1.ip_addr = '%s'
            """%(axgateip)
        elif failovertype == "2": #복귀
            result = """
                        select a2.switch_dpid, m.dest_port_num as service_port, m.source_port_num as transfer_port, a2.connect_type
                        from tb_sdn_appliance as a1
                        inner join tb_sdn_appliance_mapping as m on a1.appliance_id = m.source_appliance_id and a1.port_num = m.source_port_num
                        inner join tb_sdn_appliance as a2 on m.dest_appliance_id = a2.appliance_id and a2.port_num = m.dest_port_num
                        where a1.ip_addr = '%s'
            """%(axgateip)
        return result

    def set_hamgr_trace(self, haseq, mainclass, subclass, message, request_cmd, request_parameter, response, response_status, start_dttm, end_dttm, message_detail):
        result = """
                    insert into tb_hamgr_trace(haseq, mainclass, subclass, message, request_cmd, request_parameter, execute_time, response, response_status, start_dttm, end_dttm, message_detail)
                    values (%s, '%s', '%s', '%s', '%s', '%s', to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss.ms')-to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss.ms'), '%s', '%s', to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss.ms'), to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss.ms'), '%s')
        """ %(haseq, mainclass, subclass, message, request_cmd, request_parameter, end_dttm, start_dttm, response, response_status, start_dttm, end_dttm, message_detail)
        return result

    def set_trace_history(self, start_dttm, end_dttm, traceseq, orgseq):
        result = """
                    insert into tb_trace_history(trace_type, step_msg, start_dttm, end_dttm, traceseq, orgseq)
                    values (3, 'HA Manager', to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s')
        """ %(start_dttm, end_dttm, traceseq, orgseq)
        return result

    def insert_trace_history(self, stepmsg, startdttm, traceseq, orgseq):
        result = """
            INSERT INTO tb_trace_history(trace_type, step_msg, start_dttm, reg_dttm,
                                        traceseq, orgseq)
            VALUES (3, '%s', to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), now(),
                    %d, %d) returning histseq;
        """ %(stepmsg, startdttm, traceseq, orgseq)
        return result

    def update_trace_history(self, histseq, responsestatus, enddttm):
        result = """
            UPDATE tb_trace_history
                SET response_status = '%s', end_dttm = to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss')
            WHERE histseq = %d
        """ %(responsestatus, enddttm, histseq)
        return result


    def get_org_seq(self, officecode):
        result = """
                    select orgseq from tb_org where officecode = '%s'
        """ %(officecode)
        return result

    def get_ha_seq_max(self):
        result = """
                    select max(haseq) as haseq from tb_hamgr_trace
        """
        return result

    def get_sdn_ip_mapping(self, sourceip, officecode):
        result = """
            SELECT source_ip, source_ip_active, dest_ip, dest_ip_active, reg_dttm, officecode
            FROM tb_sdn_ip_mapping
            WHERE source_ip = '%s' and officecode = '%s'
        """ %(sourceip, officecode)

        return result

    def update_sdn_ip_mapping(self, sourceip, officecode, source_ip_active, dest_ip_active):
        result = """
            UPDATE tb_sdn_ip_mapping
            SET source_ip_active = '%s', dest_ip_active = '%s'
            WHERE source_ip = '%s' AND officecode = '%s'
        """ %(source_ip_active, dest_ip_active, sourceip, officecode)

        return result
    
    def getopenstackinfra(self, officecode):
        result = """select infraprops from tb_openstack_infra where orgseq = (select orgseq from tb_org where officecode = '%s')
        """%officecode
        return result    
    
    def select_switchtype(self, officecode, axgateip):
#         result = """select sw_type from tb_switch_info 
#         where orgseq = (select orgseq from tb_org where officecode = '%s') and sw_mgmt_ip = '%s'
#         """%( officecode, axgateip)

        result = """select sw_type from tb_utm_info utm
                    inner join tb_utm_conn_info utmconn on utmconn.utm_seq = utm.utm_seq 
                    inner join tb_switch_info sw on sw.sw_seq = utmconn.sw_seq 
                    where utm.utm_mgmt_ip = '%s' """%axgateip
        return result