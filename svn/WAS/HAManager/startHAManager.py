#-*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 27.
@summary:
@author: 최재복
'''

import tornado.ioloop
import json, os, sys
import threading
import pyrestful
from pyrestful.rest import post
from pyrestful import mediatypes
from helper.logHelper import myLogger

# from manager.inframanager import InfraManager
from service.hajson_service import HaJsonService

org_dir = os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)

log = myLogger(tag='ha_manager_handler', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class HaManagerHandler(pyrestful.rest.RestHandler):

    '''
    UTM 절체요청
    '''
    @post(_path="/service/failover", _produces=mediatypes.APPLICATION_JSON)
    def mssfailoverserver(self):
        reqdata = json.loads(self.request.body)
        log.debug("mssfailoverserver start. %s" %reqdata)

        #스레드 작업 할당
        th = threading.Thread(target=self.serviceswitch, args=("failover", reqdata))
                    
        #스레드 시작
        th.start()
                
#         result = self.serviceswitch("failover", reqdata)
        result = {"response":{"result":"success", "description":""}}
        
        return result

    '''
    UTM 복귀요청
    '''
    @post(_path="/service/failback", _produces=mediatypes.APPLICATION_JSON)
    def mssfailbackserver(self):
        reqdata = json.loads(self.request.body)
        log.debug("mssfailbackserver start. %s" %reqdata)

        #스레드 작업 할당
        th = threading.Thread(target=self.serviceswitch, args=("failback", reqdata))
                    
        #스레드 시작
        th.start()
                
#         result = self.serviceswitch("failback", reqdata)

        result = {"response":{"result":"success", "description":""}}                        
        return result

    '''
    집선스위치 등록
    '''
    @post(_path="/service/inventory/switchs", _produces=mediatypes.APPLICATION_JSON)
    def setswitchs(self):
        reqdata = json.loads(self.request.body)
        log.debug("setswitchs start. %s" %reqdata)

        result = self.serviceswitch("switchs", reqdata)
        return result

    '''
    H/W UTM및 NFV서버 등록 및 연결 포트 정보 등록
    '''
    @post(_path="/service/inventory/apliances", _produces=mediatypes.APPLICATION_JSON)
    def setapliances(self):
        reqdata = json.loads(self.request.body)

        result = self.serviceswitch("apliance", reqdata)
        return result

#     '''
#     H/W UTM및 NFV서버 등록 및 연결 포트 정보 조회
#     '''
#     @get(_path="/service/inventory/apliances", _produces=mediatypes.APPLICATION_JSON)
#     def getApliances(self):
#         reqdata = json.loads(self.request.body)
#
#         result = self.serviceSwitch("getapliance", reqdata)
#         return result

    '''
    H/W UTM및 NFV서버 이중화 그룹 정보 등록
    '''
    @post(_path="/service/inventory/apliancesmapping", _produces=mediatypes.APPLICATION_JSON)
    def setapliancesmapping(self):
        reqdata = json.loads(self.request.body)

        result = self.serviceswitch("mapping", reqdata)
        return result

    def serviceswitch(self, servicetype, reqdata):

        ham = HaJsonService()
        
        log.debug("reqdata --> %s"%json.dumps(reqdata))
        
        if servicetype == "failover":
            result = ham.mssfailover(reqdata)
        elif servicetype == "failback":
            result = ham.mssfailback(reqdata)
        elif servicetype == "switchs":
            result = ham.setswitchs(reqdata)
        elif servicetype == "apliance":
            result = ham.setappliance(reqdata)
        elif servicetype == "mapping":
            result = ham.setapliancesmapping(reqdata)
            
            
        return result

if __name__ == '__main__':
    try:
        print "##### Start HA Manager #####"
        log.info("##### Start HA Manager #####")
        app = pyrestful.rest.RestService([HaManagerHandler])
        app.listen(8888)
        tornado.ioloop.IOLoop.instance().start()

    except KeyboardInterrupt, e:
        print "##### End HA Manager #####"
        print e.message
        log.warn("##### End HA Manager #####")
        log.warn("KeyboardInterrupt [%s] " % (e.message))
    except Exception, e:
        print "##### End HA Manager #####"
        print e.message
        log.warn("##### End HA Manager #####")
        log.warn("Exception [%s] " % (e.message))
    finally:
        log.warn("##### End HA Manager #####")



