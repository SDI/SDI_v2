import argparse
import ConfigParser
import sys

# NGKIM (2015. 06. 06)
# command line에 --config-file 옵션 집어넣기 테스트
# argparse 모듈을 이용하여 구현

def main(argv=None):
    if argv is None:
        argv = sys.argv

    # Parse any conf_file specification
    # We make this parser with add_help=False so that
    # it doesn't parse -h and print help.
    conf_parser = argparse.ArgumentParser(
        description=__doc__,        
        formatter_class=argparse.RawDescriptionHelpFormatter,        
        add_help=False
        )
    conf_parser.add_argument("-c", "--config-file",
                        help="Specify config file", metavar="FILE")
    args, remaining_argv = conf_parser.parse_known_args()

    if args.config_file:
        config = ConfigParser.SafeConfigParser()
        config.read([args.config_file])
        db_web = dict(config.items("database_web"))
    else:
        db_web = { "db":"mysql" }

    print "DB is %s" % db_web["db"]
     
    parser = argparse.ArgumentParser(
        parents=[conf_parser]
        )
    parser.set_defaults(**db_web)
    parser.add_argument("--db")
    args = parser.parse_args(remaining_argv)
    print "DB is \"{}\"".format(args.db)
    return(0)

if __name__ == "__main__":
    sys.exit(main())
