#-*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 27.
@summary:
@author: 최재복
'''

import base64

from Crypto import Random
from Crypto.Cipher import AES


class AESCipher(object):
    """
    AES 암복호화
    """

    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        BS = 16
        pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        unpad = lambda s : s[:-ord(s[len(s)-1:])]
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))


if __name__ == '__main__':
    #키 - 빨간사과
    aes = AESCipher('Qfkrkstkghkwhgdk')

    #개발
    encstr = aes.encrypt('sdi')
    print encstr
    encstr = aes.encrypt('211.224.204.160')
    print encstr
    encstr = aes.encrypt('5432')
    print encstr
    encstr = aes.encrypt('orchestrator')
    print encstr
    encstr = aes.encrypt('https://218.144.90.62')
    print encstr

#     # 스테이징
#     encstr = aes.encrypt('sdi')
#     print encstr
#     encstr = aes.encrypt('10.0.0.72')
#     print encstr
#     encstr = aes.encrypt('5432')
#     print encstr
#     encstr = aes.encrypt('orchestrator')
#     print encstr
#     encstr = aes.encrypt('orch!234')
#     print encstr

    decstr = aes.decrypt(encstr)
    print decstr