# -*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 3. 4.
@summary:
@author: 최재복
'''
import sys, os, time, json, datetime
from helper.logHelper import myLogger
from util.restclient import SoapClient
from config.connect_config import sdnc
from helper.psycopg_helper import PsycopgHelper
from sql.hamanagersql import HaMnagerSql
from manager.tracemanager import TraceManager
from datetime import datetime
from util.aescipher import AESCipher

org_dir = os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)

log = myLogger(tag='hamanager', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class HaManager(PsycopgHelper):

    def __init__(self):
        '''
        Constructor
        '''
        self.sql = HaMnagerSql()
        self.init_config_from_dict_config_file()
        self.trace = TraceManager();

    def receive_failover(self, failovertype, officecode, axgateip, traceseq, sw_type):
        try:
            log.debug("Step1")
            log.debug("failOverManager [%s][%s][%s] instance success" % (failovertype, officecode, axgateip))

            failoverip = axgateip
            if failovertype == "1":
                log.debug("Step2.1")
                subclass = "failover"
            elif failovertype == "2":
                log.debug("Step2.2.1")
                subclass = "failback"
                query = self.sql.get_sdn_ip_mapping(axgateip, officecode)
                print query
                rows, rowcount = self.execute_getone(query)
                log.debug("Step2.2.2")

                if rowcount == 0:
                    log.debug("Step2.2.3")
                    return {"response":{"result":"fail", "description":"No Mapping DATA."}}

                for row in rows:
                    failoverip = row["dest_ip"]

            log.debug("Step3")
            failoverresult, failmsg = self.call_sdn_failover(failoverip, traceseq, subclass, sw_type)
            log.debug("Step4")
            if failoverresult != True:
                log.debug("Step5.1")
                return {"response":{"result":"fail", "description":failmsg}}

            log.debug("Step5.2")
            result = {"response":{"result":"success"}}

        except Exception, e:
            log.error("failOverManager Exception[%s][%s][%s]" % (failovertype, officecode, axgateip))
            result = {"response":{"result":"fail", "description":e.message}}

        return result

    def call_sdn_failover(self, failoverip, traceseq, subclass, sw_type):
        soap = SoapClient()
        failoverresult = False
        failmsg = None
        start_dttm = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
        if subclass == "failover":
            if sw_type == "SDN":
                message = u"SDN LAN 절체"
            else:
                message = u"Non-SDN LAN 절체"
        else:
            if sw_type == "SDN":
                message = u"SDN LAN 절체 복구"
            else:
                message = u"Non-SDN LAN 절체 복구"            
        try:
            aes = AESCipher('Qfkrkstkghkwhgdk')
            if sw_type == "SDN":
                SDNC_ENDPOIT = aes.decrypt(sdnc["SDNC_ENDPOIT"])
            else:
                SDNC_ENDPOIT = aes.decrypt(sdnc["NONSDNC_ENDPOIT"])
                
            senddata = {'failover_ip':failoverip}
            print "SDN Request URL : " + SDNC_ENDPOIT
            print "SDN Request body : %s" %json.dumps(senddata)
            sdnresult = soap.senddata(SDNC_ENDPOIT, "post", json.dumps(senddata))
            print "SDN Response : %s" %json.dumps(sdnresult)

            if sdnresult['status'] == 'success':
                failoverresult = True
                self.trace.hamgr_trace(traceseq, "HA-mgr", subclass, message, SDNC_ENDPOIT, json.dumps(senddata), json.dumps(sdnresult), "True", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")
            elif sdnresult["status"] == "fail":
                log.info("sdnFailOver [%s][%s] failover fail" % (failoverip, sdnresult["message"]))
                failoverresult = False
                failmsg = sdnresult["message"]
                self.trace.hamgr_trace(traceseq, "HA-mgr", subclass, message, SDNC_ENDPOIT, json.dumps(senddata), json.dumps(sdnresult), "False", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")

        except Exception, e:
            failoverresult = False
            failmsg = e.message
            self.trace.hamgr_trace(traceseq, "HA-mgr", subclass, message, SDNC_ENDPOIT, json.dumps(senddata), json.dumps(sdnresult), "False", start_dttm, datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "")

        return failoverresult, failmsg
