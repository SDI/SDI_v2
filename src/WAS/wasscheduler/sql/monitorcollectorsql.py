# -*- coding: utf-8 -*-
'''
Created on 2015. 1. 14.

@author: umisue
'''

class MgmtVMStateInfoSql(object):
    
    def selectMgmtVMStateInfo(self):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, process_host, process_name, process_status, 'T' as check
                    from tb_monitor_mgmt_vm_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_mgmt_vm_state_info)""" 
        return result    
   
    def insertMgmtVMStateInfo(self, createTime, orgseq, process_host, process_name, process_status):
        result = """INSERT INTO tb_monitor_mgmt_vm_state_info 
        (create_time, orgseq, process_host, process_name, process_status)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s')
        """ % ( createTime, orgseq, process_host, process_name, process_status)
        return result  

    def getopenstackinfraall(self):
        result = """select infraprops, orgseq from tb_openstack_infra"""
        return result            

    def getProcessNameinfo(self):
        result = """select process_name from tb_collector_monitor_process_list
        """
        return result   

    def selectsmsdata(self, orgseq, hostname, objecttype, objectname, status):
        result = """SELECT count(*) as count FROM tb_monitor_sms_data
        WHERE orgseq = %s and host_name = '%s' and object_type = '%s' and object_name = '%s' and status = '%s'
        and create_time > (now() - interval '60 minutes') and sms_send_yn = 'N' and sms_check_yn = 'N'
        """%(orgseq, hostname, objecttype, objectname, status)
        
        return result

    def insertsmsdata(self, createtime, orgseq, hostname, objecttype, objectname, status):
        result = """INSERT INTO tb_monitor_sms_data(create_time, orgseq, host_name, object_type, object_name, status, sms_send_yn, sms_check_yn)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s', 'N', 'N')
        """%(createtime, orgseq, hostname, objecttype, objectname, status)
    
        return result      


class SMSCheckMonitoringSql(object):
    
    def getmonitoringsmsdata(self):
        result = """select smsseq, to_char(sms.create_time, 'yyyymmddhh24miss') as create_time, sms.orgseq, org.orgname, sms.host_name, sms.object_type, sms.object_name, sms.status from tb_monitor_sms_data sms inner join tb_org org on org.orgseq = sms.orgseq
        where sms.sms_send_yn = 'N' and sms.sms_check_yn = 'N' and  create_time > (now() - interval '60 minutes')
        """
        return result

#     def selectsmsuser(self, orgseq):
#         result = """select concat(sms.username, '^', sms.hp_num) as call_info 
#         from tb_user_failsms sms 
#         inner join tb_user us on us.userid = sms.userid and us.userauth in (0, 1) and us.orgseq in (0, %s)
#         """%orgseq
#         return result
    
    def selectsmsdata(self, orgseq, host_name, object_type, object_name):
        result = """select smsseq from tb_monitor_sms_data
        where sms_send_yn = 'Y' and orgseq = %s and host_name = '%s' and object_type = '%s' and object_name = '%s' and create_time > (now() - interval '60 minutes')
        """%(orgseq, host_name, object_type, object_name)
        
        return result
        
    
    def selectsmsuser(self, orgseq, object_type):
        object = "%"+object_type+"%"
        result = """select concat(username, '^', hp_num) as call_info 
        from tb_user_failsms where orgseq in (0, %s) and alarmtype like ('%s')
        """%(orgseq, object)
        return result

    def updatesmsuser(self, smssendyn):
        result = """update tb_user_failsms set sms_yn = '%s', sms_check_yn = 'Y' 
        where create_time = ? and vm_id = ? and vm_process1_name = ?", [smsYn, 'Y', failInfo.create_time, failInfo.vm_id, failInfo.vm_process1_name] 
        """
        return result
    
    def setmonitoringsmsdata(self, sms_send_yn, smscheck_yn, smsseq):
        result = """update tb_monitor_sms_data set sms_send_yn = '%s', sms_check_yn = '%s'
        where smsseq = %s
        """%(sms_send_yn, smscheck_yn, smsseq)
        return result
        
    def insertsdksmssend(self, create_time, dest_info, message):
        result = """INSERT INTO SDK_SMS_SEND ( USER_ID, SUBJECT, SMS_MSG, CALLBACK_URL, NOW_DATE, SEND_DATE,CALLBACK,DEST_INFO,CDR_ID)
                    VALUES('NFV', 'Alarm', '%s', '', '%s', '%s', '99999999', '%s', 'NFV')
        """%(message, create_time, create_time, dest_info)
        return result
          
if __name__ == '__main__':
    pass