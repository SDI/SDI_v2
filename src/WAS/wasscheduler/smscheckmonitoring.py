#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: umisue
@summary: mgmt_vm 감시 모듈.
                 
'''
import datetime, os, sys 
from apscheduler.schedulers.blocking import BlockingScheduler
import psycopg2
import MySQLdb
from config.connect_config import db_conn_info, db_conn_info_sms, AESCipherKey
from helper.logHelper import myLogger

from sql.monitorcollectorsql import SMSCheckMonitoringSql
from util.aescipher import AESCipher
from config.monitor_config import sms_check_period

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='smscheckmonitoring', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class SmsCheckMonitoringProcess(object):
        
    def __init__(self):
        self.init_config_from_dict_config_file()


    def init_config_from_dict_config_file(self):
        self.smschecksql = SMSCheckMonitoringSql()
        
        aes = AESCipher(AESCipherKey['key'])

        self.dbdatabase = aes.decrypt(db_conn_info['database'])
        self.dbuser = aes.decrypt(db_conn_info['user'])
        self.dbpassword = aes.decrypt(db_conn_info['password'])
        self.dbhost = aes.decrypt(db_conn_info['host'])
        self.dbport = aes.decrypt(db_conn_info['port'])

        self.smsdbdatabase = aes.decrypt(db_conn_info_sms['database'])
        self.smsdbuser = aes.decrypt(db_conn_info_sms['user'])
        self.smsdbpassword = aes.decrypt(db_conn_info_sms['password'])
        self.smsdbhost = aes.decrypt(db_conn_info_sms['host'])
        self.smsdbport = aes.decrypt(db_conn_info_sms['port'])
                
    def run(self, invokeTime):
        try:                
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          

            db = MySQLdb.connect(host = self.smsdbhost, user=self.smsdbuser, passwd=self.smsdbpassword, db=self.smsdbdatabase)
            cursms = db.cursor()

            query = self.smschecksql.getmonitoringsmsdata()
            cur.execute(query)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            smscheckdatalist = []
            for row in rows:
                smscheckdatalist.append(dict(zip(columns, row)))
                                   
            if len(smscheckdatalist) > 0:
                log.debug("smscheckdatalist[%s]"%len(smscheckdatalist))
                
                for smsdata in smscheckdatalist:
                    smsseq = smsdata["smsseq"]
                    create_time = smsdata["create_time"]
                    orgseq = smsdata["orgseq"]
                    orgname = smsdata["orgname"]
                    host_name = smsdata["host_name"]
                    object_type = smsdata["object_type"]
                    object_name = smsdata["object_name"]
                    status = smsdata["status"]
                    
                    query = self.smschecksql.selectsmsdata(orgseq, host_name, object_type, object_name)
                    cur.execute(query)
                    rows = cur.fetchall()
                    columns = [desc[0] for desc in cur.description]
                    smsdata = []
                    for row in rows:
                        smsdata.append(dict(zip(columns, row)))

                    
                    if len(smsdata) > 0:
                        log.debug("no send sms data --> %s"%smsseq)
                        myquery = self.smschecksql.setmonitoringsmsdata('N','Y',smsseq)
                        print myquery
                        cur.execute(myquery)
                    else:
                        log.debug("send sms data --> %s"%smsseq)
                        
                        log.debug("orgseq -> %s"%orgseq)
                        log.debug("object_type -> %s"%object_type)
                        
                        query = self.smschecksql.selectsmsuser(orgseq, object_type)
                        log.debug("query-> %s"%query)
                        cur.execute(query)
                        rows = cur.fetchall()
                        columns = [desc[0] for desc in cur.description]
                        smscheckuserlist = []
                        for row in rows:
                            smscheckuserlist.append(dict(zip(columns, row)))
                        log.debug("send sms user count[%s]"%len(smscheckuserlist))
                                               
                        userlist = ""
                        for index, row in enumerate(smscheckuserlist):                           

                            if len(smscheckuserlist)-1 == index:
                                userlist = userlist + row["call_info"]
                            else:
                                userlist = userlist + row["call_info"] + "|"

                        message = "[NFV]%s가 다운. [%s]/[%s]/[%s]/[%s]"%(object_type, object_name, host_name, create_time, orgname)
                        print type(message)

                        myquery = self.smschecksql.insertsdksmssend(create_time, userlist, message)                           
                        cursms.execute(myquery)
                        
                        myquery = self.smschecksql.setmonitoringsmsdata('Y','Y',smsseq)
                        print myquery
                        cur.execute(myquery)
                    
                    db.commit()
                    conn.commit()
            else:
                log.debug("대상없음.")    
        except Exception, e:
            log.error("Process smscheckmonitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
            if db is not None:
                db.rollback()                
        finally:
            if conn is not None:
                conn.close()
            if db is not None:
                db.close()
                                  
sched = BlockingScheduler()

smsperiod = sms_check_period["period"]
     
@sched.scheduled_job('cron', second=smsperiod)
def Process_list_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("SmsCheck_Monitoring_list_job start. %s period[%s]" %(starttime,smsperiod))
    log.debug("-"*50)
    SmsCheckMonitoringProcess().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("SmsCheck_Monitoring_list_job end. %s" %endtime)
    log.debug("-"*50)
               
def main():
    log.info("Scheduler Start")
    sched.start()

if __name__ == '__main__':
    main()
