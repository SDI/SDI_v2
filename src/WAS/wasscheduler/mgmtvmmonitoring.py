#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: umisue
@summary: mgmt_vm 감시 모듈.
                 
'''
import collections, datetime, json, os, sys 
import ssl
from apscheduler.schedulers.blocking import BlockingScheduler
import paramiko
import psycopg2
from websocket import create_connection

from config.connect_config import db_conn_info, websocket_conn_info, AESCipherKey
from helper.logHelper import myLogger

from sql.monitorcollectorsql import MgmtVMStateInfoSql
from util.aescipher import AESCipher
from config.monitor_config import mgmt_vm_period

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='mgmtvmmonitoring', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class MgmtVMStateMonitoringProcess(object):
    global dic_mgmtvm
    dic_mgmtvm = []
    d_mgmtvm = collections.OrderedDict()   
        
    def __init__(self):
        self.init_config_from_dict_config_file()


    def init_config_from_dict_config_file(self):
        self.mgmtvmsql = MgmtVMStateInfoSql()
        
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['database'])
        self.dbuser = aes.decrypt(db_conn_info['user'])
        self.dbpassword = aes.decrypt(db_conn_info['password'])
        self.dbhost = aes.decrypt(db_conn_info['host'])
        self.dbport = aes.decrypt(db_conn_info['port'])

        conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
        cur = conn.cursor()
        query = self.mgmtvmsql.getopenstackinfraall()
        cur.execute(query)
        rows = cur.fetchall()
        columns = [desc[0] for desc in cur.description]
        self.mgmtvmlist = []
        for row in rows:
            self.mgmtvmlist.append(dict(zip(columns, row)))
        
    def initdicMgmtVMProcess(self):
        try:
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.mgmtvmsql.selectMgmtVMStateInfo()
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_mgmtvm = dict(zip(columns, row))
                dic_mgmtvm.append(d_mgmtvm)
            log.info("initdicMgmtVM %s" %len(dic_mgmtvm))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:                
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            aes = AESCipher(AESCipherKey['key'])
            for mgmt_vm in self.mgmtvmlist:
                orgseq = mgmt_vm["orgseq"]
                log.debug("orgseq -> %s"%orgseq)
                infraprops = mgmt_vm["infraprops"]
                print infraprops["mgmtvms"][0]["host"]
                host = aes.decrypt(infraprops["mgmtvms"][0]["host"])  
                name = infraprops["mgmtvms"][0]["name"]          
                myquery = self.mgmtvmsql.getProcessNameinfo()
                cur.execute(myquery)
                rows = cur.fetchall()
                columns = [desc[0] for desc in cur.description]
                processlist = []
                for row in rows:
                    processlist.append(dict(zip(columns, row)))
                for process in processlist:
                    process_name = process["process_name"]
                    status, cmd, out = self.getProcessCheck(infraprops, process_name)
                    log.debug(" ")
                    log.debug("getProcessCheck result -> %s"%status)
                    log.debug("getProcessCheck cmd -> %s"%cmd)
                    log.debug("getProcessCheck out -> %s"%out)

                    if out == []:
                        alarm_type = "1"
                    else:
                        alarm_type = "0"
                        
                    myquery = self.mgmtvmsql.insertMgmtVMStateInfo(self.invokeTime, orgseq, host, process_name, status)
                    cur.execute(myquery)

                    if alarm_type == "1" :#and usertype == "1":
                        selectquery = self.mgmtvmsql.selectsmsdata(orgseq, host, 'MGMT_VM', process_name, status)
                        cur.execute(selectquery)
                        rows = cur.fetchone()
                        smscount = rows[0]
                        log.debug("smscount %s"%smscount)
                        if smscount == 0:
                            myquery = self.mgmtvmsql.insertsmsdata(self.invokeTime, orgseq, host, 'MGMT_VM', process_name, status)
                            cur.execute(myquery)
                                            
                    result_mgmtvm, checkData = self.checkMgmtVMStatus(self.invokeTime, orgseq, host, process_name, status)
                    log.debug("[%s] host %s process_name %s process_status %s"%(result_mgmtvm, host, process_name, status))
                    if (result_mgmtvm <> "EXIST" or alarm_type == "1")  :
                        if result_mgmtvm == "CHANGE":
                            data_type = "C"
                        elif result_mgmtvm == "FIRST":
                            data_type = "I"
                        else:
                            data_type = "N"
                        sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":alarm_type, "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":9, "collect_data":checkData}
                        sendData_json = json.dumps(sendData)
                        self.sendBroadcasting(sendData_json)                       
            log.info("initdicmgmtvm %s" %len(dic_mgmtvm))           
            data_len = len(dic_mgmtvm)
            for index in range(0, data_len) :
                if dic_mgmtvm[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":9, "collect_data":dic_mgmtvm[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_mgmtvm.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_mgmtvm[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("Process Monitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()

    def getProcessCheck(self, infraprops, process_name):      

        cmd = "ps -ef |grep %s"%process_name+" |grep -v grep"
        result, cmd, out = self.sendCommand(infraprops, cmd, process_name)
        if result == True:
            result = "active"
        else:
            result = "deactive"
        return result, cmd, out

    def checkPing(self, addr):
        response = os.system("ping -c 1 " + addr)
        if response == 0:
            result = "ACTIVE"
        else:
            result = "DEACTIVE"
        return result
    
    def sendCommand(self, infraprops, cmd, searchText):
        client = paramiko.SSHClient()
        result = False
        aes = AESCipher(AESCipherKey['key'])
        host = aes.decrypt(infraprops["mgmtvms"][0]["host"])
        userid = aes.decrypt(infraprops["mgmtvms"][0]["userid"])
        passwd = aes.decrypt(infraprops["mgmtvms"][0]["password"] )       
        try:
            out = []
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(hostname=host, username=userid, password=passwd, timeout=3)
            stdin, stdout, stderr = client.exec_command(cmd)
            for line in stdout:
                if searchText in line:
                    result = True
                    print "process line %s"%line
                    out.append(line.replace('\n','').replace('       ',''))
                    break
            log.debug("result %s" %result)
            
        except Exception, e:
            log.error("sendCommand[%s] error %s" %(self.host,str(e)))
            sys.exit()
        finally:
            client.close()
            return result, cmd, out
            
    def checkResetCustomer(self):
        for row in dic_mgmtvm:
            row["check"] = "F"
            
    def checkMgmtVMStatus(self, create_time,  orgseq, host, name, status):
        count = 0
        for row in dic_mgmtvm:
            if row["orgseq"] == orgseq and row["process_host"] == host and row["process_name"] == name and row["process_status"] == status:
                row["check"] = "T"
                return "EXIST", json.dumps(dic_mgmtvm[count])
            elif row["orgseq"] == orgseq and row["process_host"] == host and row["process_name"] == name:
                row["process_status"] = status

                return "CHANGE",json.dumps(dic_mgmtvm[count])
            count = count + 1

        d_process = {"create_time": create_time,"orgseq":orgseq,  "process_host":host, "process_name":name, "process_status":status, "check":"T"}
        dic_mgmtvm.append(d_process)
        return "FIRST",json.dumps(d_process)
    
    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("mgmtVM Monitoring send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)
                                   
sched = BlockingScheduler()

mgmtvmperiod = mgmt_vm_period["period"]
     
@sched.scheduled_job('cron', second=mgmtvmperiod)
def CollectorProcess_list_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("MgmtVM_Monitoring_list_job start. %s period[%s]" %(starttime,mgmtvmperiod))
    log.debug("dicSize[%d]"%len(dic_mgmtvm))
    log.debug("-"*50)
    MgmtVMStateMonitoringProcess().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicSize[%d]"%len(dic_mgmtvm))
    log.debug("MgmtVM_Monitoring_list_job end. %s" %endtime)
    log.debug("-"*50)
               
def main():
    log.info("Scheduler Start")
    MgmtVMStateMonitoringProcess().initdicMgmtVMProcess()
    sched.start()

if __name__ == '__main__':
    main()
