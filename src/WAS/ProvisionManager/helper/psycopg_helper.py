#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 4.
@summary: DB 관련 작업 베이스 클래스
@author: 진수
'''
import psycopg2

from config.connect_config import db_conn_info
from util.aescipher import AESCipher

class PsycopgHelper(object):
    def __init__(self):
        self.init_config_from_dict_config_file()

    def init_config_from_dict_config_file(self):
        aes = AESCipher('Qfkrkstkghkwhgdk')
        self.conn = psycopg2.connect(database=aes.decrypt(db_conn_info['database']),
                                     user=aes.decrypt(db_conn_info['user']),
                                     password=aes.decrypt(db_conn_info['password']),
                                     host=aes.decrypt(db_conn_info['host']),
                                     port=aes.decrypt(db_conn_info['port']))
                
        self.conn.autocommit = True
    def execute_get(self, query):
        """
        Created on 2015. 2. 5.
        @author: 진수
        @summary: db 처리 수행, 컬럼명과 rows를 리턴
        @param query: 데이터베이스 쿼리 문자열
        @rtype: dict
        """
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            columns = [desc[0] for desc in cur.description]
            rows = cur.fetchall()
        except psycopg2.DatabaseError, e:
            raise e

        return {'columns':columns, 'rows':rows}

    def execute_set(self, query):
        try:
            cur = self.conn.cursor()
            cur.execute(query)
        except psycopg2.DatabaseError, e:
            raise e

    def execute_getone(self,query):
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            columns = [desc[0] for desc in cur.description]
            rows = cur.fetchall()
            dic = []
            for row in rows:
                d = dict(zip(columns, row))
                dic.append(d)
        except psycopg2.DatabaseError, e:
            raise e

        return dic

    def __del__(self):
        if self.conn :
            self.conn.close()

if __name__ == '__main__':
    pass