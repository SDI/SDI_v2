# #-*- coding: utf-8 -*-
# '''
# @project: Service Orchestrator V2.0
# @copyright:  © 2015 kt corp. All rights reserved.
# 
# @created: 2015. 2. 11.
# @summary:
# @author: umisue
# '''
# 
# from helper.psycopg_helper import PsycopgHelper
# from sql.provisioningsql import Provisioningsql
# 
# class Provisiontrace(PsycopgHelper):
#     
#     def __init__(self):
#         self.trace = {"cmdtype": "", "managegroup": "", "managetype": 0, "provisionseq":"", "userid":"", "stepmsg":"", "indent": 0, "logstep":"", "msglevel": 0, "stepmsgcmd": "","stepmsgdetails": "", "svctype": None, "resourceid": ""}
#         self.init_config_from_dict_config_file()
#               
#     def settraceforinfra(self, provisionseq, userid, managegroup, logstep, stepmsg):      
# 
#         provisionsql = Provisioningsql()
# 
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#                 
#         if managegroup == "0":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "0"
#             self.trace["managetype"] = 0
#             if logstep == "0":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "0"
#                 self.trace["stepmsg"] = u"서비스 설치 시작"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#         if managegroup == "1":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "1"
#             self.trace["managetype"] = 0
#             if logstep == "1":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "1"
#                 self.trace["stepmsg"] = u"계정확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "1.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "1.1"
#                 self.trace["stepmsg"] = u"계정생성"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             elif logstep == "1.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "1.2"
#                 self.trace["stepmsg"] = u"계정생성 완료"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             elif logstep == "1.3":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "1.3"
#                 self.trace["stepmsg"] = u"계정생성 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             elif logstep == "1.4":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "1.4"
#                 self.trace["stepmsg"] = u"계정검사"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             elif logstep == "1.5":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "1.5"
#                 self.trace["stepmsg"] = u"계정검사 완료"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             elif logstep == "1.6":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "1.6"
#                 self.trace["stepmsg"] = u"계정검사 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 
#             self.execute_set(myquery)
# 
#         if managegroup == "2":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "2"
#             self.trace["managetype"] = 0
#             if logstep == "2":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "2"
#                 self.trace["stepmsg"] = u"그린네트워크생성"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "2.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "2.1"
#                 self.trace["stepmsg"] = u"그린네트워크생성요청"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "2.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "2.1.1"
#                 self.trace["stepmsg"] = u"그린네트워크생성 완료"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.execute_set(myquery)
#             if logstep == "2.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "2.1.2"
#                 self.trace["stepmsg"] = u"그린네트워크생성 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.execute_set(myquery)
#             if logstep == "2.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "2.2"
#                 self.trace["stepmsg"] = u"그린서브네트워크생성요청"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "2.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "2.2.1"
#                 self.trace["stepmsg"] = u"그린서브네트워크생성 완료"
#                 self.trace["svctype"] = 10
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "2.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "2.2.2"
#                 self.trace["stepmsg"] = u"그린서브네트워크생성 실패"        
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#         if managegroup == "3":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "3"
#             self.trace["managetype"] = 0
#             if logstep == "3":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "3"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "3.1"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성요청"
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "3.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "3.1.1"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성 완료"
#            
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "3.1.2"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성 실패"
#              
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "3.2"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크생성요청"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "3.2.1"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크생성 완료"
#                 self.trace["svctype"] = 20
#              
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "3.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "3.2.2"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크생성 실패"
#               
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#         if managegroup == "4":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "4"
#             self.trace["managetype"] = 0
#             if logstep == "4":
#                 self.trace["managegroup"] = "4"
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "4"
#                 self.trace["stepmsg"] = u"인스턴스생성"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "4.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "4.1"
#                 self.trace["stepmsg"] = u"인스턴스생성 요청"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "4.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "4.2"
#                 self.trace["stepmsg"] = u"인스턴스생성 완료"
#                 self.trace["svctype"] = 1
#               
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "4.3":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "4.3"
#                 self.trace["stepmsg"] = u"인스턴스생성 실패"
#      
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#         if managegroup == "5":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "5"
#             self.trace["managetype"] = 0
#             if logstep == "5":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5"
#                 self.trace["stepmsg"] = u"생성된 서버 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 
#             if logstep == "5.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.1"
#                 self.trace["stepmsg"] = u"인스턴스 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)     
#             if logstep == "5.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.1.1"
#                 self.trace["stepmsg"] = u"인스턴스 조회 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.1.2"
#                 self.trace["stepmsg"] = u"인스턴스 조회 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                                               
#             if logstep == "5.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.2"
#                 self.trace["stepmsg"] = u"red_shared_public_net 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.2.1"
#                 self.trace["stepmsg"] = u"red_shared_public_net 조회 성공"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.2.2"
#                 self.trace["stepmsg"] = u"red_shared_public_net 조회 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                    
#             if logstep == "5.3":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.3"
#                 self.trace["stepmsg"] = u"global_mgmt_net 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.3.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.3.1"
#                 self.trace["stepmsg"] = u"global_mgmt_net 조회 성공"
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.3.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.3.2"
#                 self.trace["stepmsg"] = u"global_mgmt_net 조회 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                    
#                                 
#             if logstep == "5.4":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.4"
#                 self.trace["stepmsg"] = u"그린네트워크 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#             if logstep == "5.4.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.4.1"
#                 self.trace["stepmsg"] = u"그린네트워크 조회 성공"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#             if logstep == "5.4.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.4.2"
#                 self.trace["stepmsg"] = u"그린네트워크 조회 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#                                                   
#             if logstep == "5.5":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.5"
#                 self.trace["stepmsg"] = u"오렌지네트워크 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                   
#             if logstep == "5.5.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.5.1"
#                 self.trace["stepmsg"] = u"오렌지네트워크 조회 성공"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.5.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.5.2"
#                 self.trace["stepmsg"] = u"오렌지네트워크 조회 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)       
#             if logstep == "5.6":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.6"
#                 self.trace["stepmsg"] = u"서버 상태 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)         
# 
#             self.execute_set(myquery)
# #             if logstep == "5.7":
# #                 self.trace["indent"] = 1
# #                 self.trace["msglevel"] = 1
# #                 self.trace["logstep"] = "5.7"
# #                 self.trace["stepmsg"] = u"접속 포트 확인"
# #                 myquery = provisionsql.settraceforprovision(self.trace)  
# # 
# #             if logstep == "5.7.1":
# #                 self.trace["indent"] = 2
# #                 self.trace["msglevel"] = 1
# #                 self.trace["logstep"] = "5.7.1"
# #                 self.trace["stepmsg"] = u"접속 포트 확인 성공"
# #                 myquery = provisionsql.settraceforprovision(self.trace) 
# # 
# #             if logstep == "5.8":
# #                 self.trace["indent"] = 2
# #                 self.trace["msglevel"] = 2
# #                 self.trace["logstep"] = "5.8"
# #                 self.trace["stepmsg"] = u"인스턴스 설정 "
# #                 myquery = provisionsql.settraceforprovision(self.trace)                                 
# # 
# #             if logstep == "5.8.1":
# #                 self.trace["indent"] = 2
# #                 self.trace["msglevel"] = 1
# #                 self.trace["logstep"] = "5.8.1"
# #                 self.trace["stepmsg"] = u"인스턴스 설정 성공"
# #                 myquery = provisionsql.settraceforprovision(self.trace) 
# # 
# #             if logstep == "5.8.2":
# #                 self.trace["indent"] = 2
# #                 self.trace["msglevel"] = 2
# #                 self.trace["logstep"] = "5.8.2"
# #                 self.trace["stepmsg"] = u"인스턴스 설정 실패"
# #                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                                 
#         if managegroup == "50":
#             self.trace["cmdtype"] = "D"
#             self.trace["managegroup"] = "50"
#             self.trace["managetype"] = 0
#             if logstep == "50":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "50"
#                 self.trace["stepmsg"] = u"서비스 설치 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "50.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "50.1"
#                 self.trace["stepmsg"] = u"서비스 삭제 시작"
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "50.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "50.1.1"
#                 self.trace["stepmsg"] = u"인스턴스 삭제"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                             
#             if logstep == "50.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "50.1.2"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크 삭제"
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "50.1.4":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "50.1.4"
#                 self.trace["stepmsg"] = u"오렌지네트워크 삭제"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                                             
#             if logstep == "50.1.3":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "50.1.3"
#                 self.trace["stepmsg"] = u"그린서브네트워크 삭제"
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "50.1.5":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "50.1.5"
#                 self.trace["stepmsg"] = u"그린네트워크 삭제"
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "50.1.6":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "50.1.6"
#                 self.trace["stepmsg"] = u"서비스 삭제 완료"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "50.1.7":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "50.1.7"
#                 self.trace["stepmsg"] = u"서비스 삭제 완료 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#     def settraceforinfraresult(self, provisionseq, userid, managegroup, logstep, stepmsg, cmd, cmddetail, resourceid):
#         provisionsql = Provisioningsql()
# 
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#         
#         if managegroup == "2":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "2"
#             self.trace["managetype"] = 0
#             if logstep == "2.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "2.1.1"
#                 self.trace["stepmsg"] = u"그린네트워크생성 완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "2.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "2.1.2"
#                 self.trace["stepmsg"] = u"그린네트워크생성 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "2.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "2.2.1"
#                 self.trace["stepmsg"] = u"그린서브네트워크생성 완료"
#                 self.trace["svctype"] = 10
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "2.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "2.2.2"
#                 self.trace["stepmsg"] = u"그린서브네트워크생성 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
#         if managegroup == "3":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "3"
#             self.trace["managetype"] = 0
#             if logstep == "3.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "3.1.1"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성 완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "3.1.2"
#                 self.trace["stepmsg"] = u"오렌지네트워크생성 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "3.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "3.2.1"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크생성 완료"
#                 self.trace["svctype"] = 20
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "3.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "3.2.2"
#                 self.trace["stepmsg"] = u"오렌지서브네트워크생성 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
# 
#         if managegroup == "4":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "4"
#             self.trace["managetype"] = 0
#             if logstep == "4.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "4.2"
#                 self.trace["stepmsg"] = u"인스턴스생성 완료"
#                 self.trace["svctype"] = 1
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#                 self.trace["svctype"] = None
#             if logstep == "4.3":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "4.3"
#                 self.trace["stepmsg"] = u"인스턴스생성 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 self.trace["resourceid"] = resourceid                
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             self.execute_set(myquery)
#         if managegroup == "5":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "5"
#             self.trace["managetype"] = 0
#             if logstep == "5.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.1.1"
#                 self.trace["stepmsg"] = u"인스턴스 조회 확인"
#                 self.trace["stepmsgcmd"] = cmd  
#                 self.trace["stepmsgdetails"] = cmddetail
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.1.2"
#                 self.trace["stepmsg"] = u"인스턴스 조회 실패"
#                 self.trace["stepmsgcmd"] = cmd  
#                 self.trace["stepmsgdetails"] = cmddetail     
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.2.1"
#                 self.trace["stepmsg"] = u"red_shared_public_net 조회 성공"
#                 self.trace["stepmsgcmd"] = cmd  
#                 self.trace["stepmsgdetails"] = cmddetail    
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.2.2"
#                 self.trace["stepmsg"] = u"red_shared_public_net 조회 실패"
#                 self.trace["stepmsgcmd"] = cmd  
#                 self.trace["stepmsgdetails"] = cmddetail     
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.3.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.3.1"
#                 self.trace["stepmsg"] = u"global_mgmt_net 조회 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail           
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.3.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.3.2"
#                 self.trace["stepmsg"] = u"global_mgmt_net 조회 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail        
#                 myquery = provisionsql.settraceforprovision(self.trace)                                    
#             if logstep == "5.4.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.4.1"
#                 self.trace["stepmsg"] = u"그린네트워크 조회 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail             
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#             if logstep == "5.4.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.4.2"
#                 self.trace["stepmsg"] = u"그린네트워크 조회 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail              
#                 myquery = provisionsql.settraceforprovision(self.trace)                              
#             if logstep == "5.5.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.5.1"
#                 self.trace["stepmsg"] = u"오렌지네트워크 조회 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail         
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.5.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.5.2"
#                 self.trace["stepmsg"] = u"오렌지네트워크 조회 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail         
#                 myquery = provisionsql.settraceforprovision(self.trace)       
#             if logstep == "5.6":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "5.6"
#                 self.trace["stepmsg"] = u"서버 상태 조회"
#                 myquery = provisionsql.settraceforprovision(self.trace)         
#             self.execute_set(myquery)
#                                                 
#     def settraceforvnfresult(self, provisionseq, userid, managegroup, logstep, stepmsg, cmd, cmddetail):
#         provisionsql = Provisioningsql()
# 
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#                         
#         if managegroup == "5":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "5"
#             self.trace["managetype"] = 1
#             if logstep == "5.7.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.7.1"
#                 self.trace["stepmsg"] = u"접속 포트 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 myquery = provisionsql.settraceforprovision(self.trace) 
# 
#             if logstep == "5.7.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.7.2"
#                 self.trace["stepmsg"] = u"접속 포트 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail
#                 myquery = provisionsql.settraceforprovision(self.trace)                          
# 
#             if logstep == "5.8.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.8.1"
#                 self.trace["stepmsg"] = u"UTM WAN 설정완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.8.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.8.2"
#                 self.trace["stepmsg"] = u"UTM LAN 설정완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.8.3":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.8.3"
#                 self.trace["stepmsg"] = u"UTM RULE 적용완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#             if logstep == "5.8.4":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.8.5"
#                 self.trace["stepmsg"] = u"UTM RULE 적용완료"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                                 
#             if logstep == "5.8.5":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "5.8.5"
#                 self.trace["stepmsg"] = u"인스턴스 설정 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                 
#             self.execute_set(myquery)            
# 
#     def settraceforvnf(self, provisionseq, userid, managegroup, logstep, stepmsg):
#         provisionsql = Provisioningsql()
# 
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#                 
#         if managegroup == "5":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "5"
#             self.trace["managetype"] = 1
#             if logstep == "5.6":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.7"
#                 self.trace["stepmsg"] = u"인스턴스 설정 "
#                 myquery = provisionsql.settraceforprovision(self.trace)    
#             if logstep == "5.7":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.6"
#                 self.trace["stepmsg"] = u"접속 포트 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#                 
#             if logstep == "5.8":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "5.7"
#                 self.trace["stepmsg"] = u"인스턴스 설정 "
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
#                 
#             self.execute_set(myquery)     
#             
#     def settracefortetst(self, provisionseq, userid, managegroup, logstep, stepmsg):
#         provisionsql = Provisioningsql()
#          
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#                 
#         if managegroup == "6":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "6"
#             self.trace["managetype"] = 2
#             if logstep == "6":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6"
#                 self.trace["stepmsg"] = u"인스턴스 테스트"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
# 
#             if logstep == "6.1":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.1"
#                 self.trace["stepmsg"] = u"인스턴스 리스타트"
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                 
#             if logstep == "6.2":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.2"
#                 self.trace["stepmsg"] = u"22번 접속 포트 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
# 
#             if logstep == "6.3":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.3"
#                 self.trace["stepmsg"] = u"10443번 접속 포트 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
# 
#             if logstep == "6.4":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.4"
#                 self.trace["stepmsg"] = u"네트워크 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
#                 
#             if logstep == "6.5":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.5"
#                 self.trace["stepmsg"] = u"라우트 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                             
# 
#             if logstep == "6.6":
#                 self.trace["indent"] = 1
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "6.6"
#                 self.trace["stepmsg"] = u"JOB 확인"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                             
#                
#             self.execute_set(myquery)  
#                     
#         if managegroup == "7":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "7"
#             self.trace["managetype"] = 2
#             if logstep == "7":
#                 self.trace["indent"] = 0
#                 self.trace["msglevel"] = 0
#                 self.trace["logstep"] = "7"
#                 self.trace["stepmsg"] = u"서비스 설치 완료"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
#             self.execute_set(myquery)
#                  
#     def settracefortetstresult(self, provisionseq, userid, managegroup, logstep, stepmsg, cmd, cmddetail):
#         provisionsql = Provisioningsql()
#          
#         self.trace["provisionseq"] = provisionseq
#         self.trace["userid"] = userid
#         self.trace["stepmsgcmd"] = ""
#         self.trace["stepmsgdetails"] = ""
#         self.trace["resourceid"] = ""
#                 
#         if managegroup == "6":
#             self.trace["cmdtype"] = "C"
#             self.trace["managegroup"] = "6"
#             self.trace["managetype"] = 2
# 
#             if logstep == "6.1.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.1.1"
#                 self.trace["stepmsg"] = u"인스턴스 리스타트 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
#             if logstep == "6.1.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.1.2"
#                 self.trace["stepmsg"] = u"인스턴스 리스타트 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                                
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                                 
#             if logstep == "6.2.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.2.1"
#                 self.trace["stepmsg"] = u"22번 접속 포트 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                                
#                 myquery = provisionsql.settraceforprovision(self.trace)                                 
#             if logstep == "6.2.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.2.2"
#                 self.trace["stepmsg"] = u"22번 접속 포트 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                 
#             if logstep == "6.3.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.3.1"
#                 self.trace["stepmsg"] = u"10443번 접속 포트 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)       
#                                           
#             if logstep == "6.3.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.3.2"
#                 self.trace["stepmsg"] = u"10443번 접속 포트 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace) 
#                 
#             if logstep == "6.4.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.4.1"
#                 self.trace["stepmsg"] = u"네트워크(eth0) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "6.4.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.4.2"
#                 self.trace["stepmsg"] = u"네트워크(eth0) 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "6.4.3":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.4.3"
#                 self.trace["stepmsg"] = u"네트워크(eth1) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "6.4.4":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.4.4"
#                 self.trace["stepmsg"] = u"네트워크(eth1) 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "6.4.5":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.4.5"
#                 self.trace["stepmsg"] = u"네트워크(br0) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "6.3.6":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.3.6"
#                 self.trace["stepmsg"] = u"네트워크(br0) 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
# 
#             if logstep == "6.4.7":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.4.7"
#                 self.trace["stepmsg"] = u"네트워크(br1) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)
#             if logstep == "6.4.8":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.4.8"
#                 self.trace["stepmsg"] = u"네트워크(br1) 확인 실패"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)                                                                                                 
# 
#                                
#             if logstep == "6.5.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.5.1"
#                 self.trace["stepmsg"] = u"라우트(mgmt-net) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)          
#             if logstep == "6.5.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.5.2"
#                 self.trace["stepmsg"] = u"라우트(mgmt-net) 확인 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)                                                      
# 
#             if logstep == "6.5.3":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.5.3"
#                 self.trace["stepmsg"] = u"라우트(red-net) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)          
#             if logstep == "6.5.4":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.5.4"
#                 self.trace["stepmsg"] = u"라우트(red-net) 확인 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)  
# 
#             if logstep == "6.5.5":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.5.5"
#                 self.trace["stepmsg"] = u"라우트(green-net) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)          
#             if logstep == "6.5.6":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.5.6"
#                 self.trace["stepmsg"] = u"라우트(green-net) 확인 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)       
# 
#             if logstep == "6.5.7":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.5.7"
#                 self.trace["stepmsg"] = u"라우트(orange-net) 확인 성공"
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)          
#                 
#             if logstep == "6.5.8":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.5.8"
#                 self.trace["stepmsg"] = u"라우트(orange-net) 확인 실패"
#                 myquery = provisionsql.settraceforprovision(self.trace)    
# 
#             if logstep == "6.6.1":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 1
#                 self.trace["logstep"] = "6.6.1"
#                 self.trace["stepmsg"] = stepmsg
#                 self.trace["stepmsgcmd"] = cmd
#                 self.trace["stepmsgdetails"] = cmddetail.replace('"','')                   
#                 myquery = provisionsql.settraceforprovision(self.trace)          
#                 
#             if logstep == "6.6.2":
#                 self.trace["indent"] = 2
#                 self.trace["msglevel"] = 2
#                 self.trace["logstep"] = "6.6.2"
#                 self.trace["stepmsg"] = stepmsg
#                 myquery = provisionsql.settraceforprovision(self.trace)                                                           
#             self.execute_set(myquery)             