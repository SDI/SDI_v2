#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 12.
@summary:
@author: 진수
'''

import base64

from Crypto import Random
from Crypto.Cipher import AES


class AESCipher(object):
    """
    AES 암복호화
    """

    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        BS = 16
        pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        unpad = lambda s : s[:-ord(s[len(s)-1:])]
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))


if __name__ == '__main__':
    #키 - 빨간사과
    aes = AESCipher('Qfkrkstkghkwhgdk')
#     encstr = aes.encrypt('sdi')
#     print encstr
#     encstr = aes.encrypt('211.224.204.160')
#     print encstr
#     encstr = aes.encrypt('5432')
#     print encstr
#     encstr = aes.encrypt('orchestrator')
#     print encstr
#     encstr = aes.encrypt('orch!234')
#     print encstr
# 
    encstr = aes.encrypt('10.0.0.102')
    print encstr

    encstr = aes.encrypt('https://10.0.0.61/failover/request_utm_failover')
    print encstr
        
#     encstr = aes.encrypt('root')
#     print encstr
#     
    decstr = aes.decrypt("Bye0fWGJiNHpFDQyJY676O1+PIrpb5NvOZd/mF8KfMU=")
    print decstr

#     encstr = aes.encrypt('ws://211.224.204.158:9999/ws')
#     print encstr

#     decstr = aes.decrypt(encstr)
#     print decstr