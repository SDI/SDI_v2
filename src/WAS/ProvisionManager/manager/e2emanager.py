#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 11.
@summary:
@author: umisue
'''
from helper.psycopg_helper import PsycopgHelper
from sql.provisioningsql import Provisioningsql

class E2emanager(PsycopgHelper):

    def __init__(self):
        self.sql = Provisioningsql()
        self.init_config_from_dict_config_file()
        
    def sete2etraceforprovisioning(self, provisionseq, mainclass, subclass, message, request_cmd, request_parameter, start_dttm, end_dttm, execute_time, response, response_status, message_detail):
        myquery = self.sql.sete2etracemanager(provisionseq, mainclass, subclass, message, request_cmd, request_parameter, start_dttm, end_dttm, execute_time, response, response_status, message_detail)
        self.execute_set(myquery) 