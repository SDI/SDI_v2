#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''

import sys,os, time, json, datetime
from helper.logHelper import myLogger
from util.restclient import SoapClient
# from config.connect_config import mgmt_vm
# from util.aescipher import AESCipher
from manager.e2emanager import E2emanager
from util.openstackclient import NovaClient
from helper.psycopg_helper import PsycopgHelper
from sql.provisioningsql import Provisioningsql
from util.aescipher import AESCipher
from config.connect_config import AESCipherKey

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
 
log = myLogger(tag='startprovision_test', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class TestManager(PsycopgHelper):

    def __init__(self):
        '''
        Constructor
        '''
        self.sql = Provisioningsql()
#         self.trace = Provisiontrace()
        self.e2e = E2emanager()
        self.init_config_from_dict_config_file() 
        self.nova = NovaClient()
#         aes = AESCipher(AESCipherKey['key'])
#         self.mgmt_endpoint = aes.decrypt(mgmt_vm["MGMT_VM_ENDPOIT"])
      
                
    def managetest(self, provisionseq, orgseq, userid, gInstance, configinfo):
        
        query = self.sql.getopenstackinfra(orgseq)
        rows = self.execute_getone(query)
        aes = AESCipher(AESCipherKey['key'])

        if "backuptype" in gInstance and gInstance["backuptype"] == "standby":
            self.mgmt_endpoint = aes.decrypt(rows[0]["infraprops"]["backup_mgmtvms"][0]["endpoint"])        
        else:
            self.mgmt_endpoint = aes.decrypt(rows[0]["infraprops"]["mgmtvms"][0]["endpoint"])
        
#         self.mgmt_endpoint = aes.decrypt(rows[0]["infraprops"]["mgmtvms"][0]["endpoint"])    
        
        try:
            start_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", "TEST Manager 서비스 시작", "", "", start_dttm_provision, start_dttm_provision, "", "", True, "")
                        
            myquery= self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 점검을 시작합니다.")
            self.execute_set(myquery)
                  
            global_mgmt_net = gInstance["global_mgmt_net"]
#             global_mgmt_net_floating_ip = gInstance["global_mgmt_net_floating_ip"]
            
            global_mgmt_netaddr = configinfo["redinfo"]["global_mgmt_netaddr"]
            
            green_test = 0
            orange_test = 0
            blue_test = 0

            red_address = configinfo["redinfo"]["red_address"]
            red_netaddr = configinfo["redinfo"]["red_netaddr"]
            
            if "green_address" in configinfo["greeninfo"]:
                green_address = configinfo["greeninfo"]["green_address"]
                green_netaddr = configinfo["greeninfo"]["green_netaddr"]

                if not green_address == "":
                    green_test = 1
            if "orange_address" in configinfo["orangeinfo"]:                
                orange_address = configinfo["orangeinfo"]["orange_address"]
                orange_netaddr = configinfo["orangeinfo"]["orange_netaddr"]                
                if not orange_address == "":
                    orange_test = 1
            if "blue_address" in configinfo["blueinfo"]:      
                blue_address = configinfo["blueinfo"]["blue_address"]
                blue_netaddr = configinfo["blueinfo"]["blue_netaddr"]             
                if not blue_address == "":
                    blue_test = 1

#             print "global_mgmt_net %s"%global_mgmt_net
#             print "global_mgmt_net_floating_ip %s"%global_mgmt_net_floating_ip
#             print "red_address %s"%red_address
#             print "green_address %s"%green_address
#             print "orange_address %s"%orange_address
#             print "blue_address %s"%blue_address
#             
#             print "global_mgmt_netaddr %s"%global_mgmt_netaddr
#             print "red_netaddr %s"%red_netaddr
#             print "green_netaddr %s"%green_netaddr
#             print "orange_netaddr %s"%orange_netaddr
#             print "blue_netaddr %s"%blue_netaddr
                        
            """
            Port검사
            """            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM SSH Port를 점검 중입니다.")     
            self.execute_set(myquery)                
            
#             print "checkport 22 mgmt_net %s"%global_mgmt_net

            log.debug("[%s][%s] checkport request"%(provisionseq, userid)) 
            result = self.checkport(provisionseq, userid, global_mgmt_net, "22")
            if result["result"] =="fail":
                log.debug("[%s][%s] checkport fail"%(provisionseq, userid)) 
                raise Exception(result["description"])
            log.debug("[%s][%s] checkport success"%(provisionseq, userid)) 

#             print "checkport 10443 %s"%global_mgmt_net
            
            result = self.checkport(provisionseq, userid, global_mgmt_net, "10443")
            if result["result"] =="fail":
                raise Exception(result["description"])    
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM SSH Port를 점검 완료했습니다.")     
            self.execute_set(myquery)             
            
            """
            Network검사
            """
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 네트워크 점검 중입니다.")     
            self.execute_set(myquery)                         

#             print "checknetwork mgmt_net %s"%global_mgmt_net

            log.debug("[%s][%s] network test request eth0"%(provisionseq, userid)) 
            result = self.checknetwork(provisionseq, configinfo, userid, global_mgmt_net, global_mgmt_net, "eth0")
            if result["result"] =="fail":
                log.debug("[%s][%s] network test eth0 fail"%(provisionseq, userid)) 
                raise Exception("network test eth0 fail") 
            log.debug("[%s][%s] network test eth0 success"%(provisionseq, userid)) 
            
#             print "checknetwork red_address %s"%red_address
            if "eth1_vlan_status" in gInstance and gInstance["eth1_vlan_status"] == "off":
                message_network = u"NFV-UTM 레드네트워크 상태 (down) "
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
            else:
                log.debug("[%s][%s] network test request eth1"%(provisionseq, userid)) 
                result = self.checknetwork(provisionseq, configinfo, userid, global_mgmt_net, red_address, "eth1")
                if result["result"] =="fail":
                    log.debug("[%s][%s] network test fail eth1"%(provisionseq, userid)) 
                    raise Exception("network test eth1 fail") 
                log.debug("[%s][%s] network test success eth1"%(provisionseq, userid))

#             print "checknetwork green_address %s"%green_address
            if green_test == 1:
                if "br0_status" in gInstance and gInstance["br0_status"] == "off":
                    message_network = u"NFV-UTM 그린네트워크 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:
                    log.debug("[%s][%s] network test request br0"%(provisionseq, userid))  
                    result = self.checknetwork(provisionseq, configinfo, userid, global_mgmt_net, green_address, "br0")
                    if result["result"] =="fail":
                        log.debug("[%s][%s] network test fail br0"%(provisionseq, userid))  
                        raise Exception("network test br0 fail")  
                    log.debug("[%s][%s] network test success br0"%(provisionseq, userid))
            print "0"
#             print "checknetwork orange_address %s"%orange_address
            
            if orange_test == 1:
                if "br1_status" in gInstance and gInstance["br1_status"] == "off":
                    message_network = u"NFV-UTM 오렌지네트워크 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:                
                    log.debug("[%s][%s] network test request br1"%(provisionseq, userid))    
                    result = self.checknetwork(provisionseq, configinfo, userid, global_mgmt_net, orange_address, "br1")
                    if result["result"] =="fail":
                        log.debug("[%s][%s] network test fail br1"%(provisionseq, userid))    
                        raise Exception("network test br1 fail")  
                    log.debug("[%s][%s] network test success br1"%(provisionseq, userid))    

#             print "checknetwork blue_address %s"%blue_address
            
            if blue_test == 1:
                if "br2_status" in gInstance and gInstance["br2_status"] == "off":
                    message_network = u"NFV-UTM 블루네트워크 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:                      
                    log.debug("[%s][%s] network test request br1"%(provisionseq, userid))    
                    result = self.checknetwork(provisionseq, configinfo, userid, global_mgmt_net, blue_address, "br2")
                    if result["result"] =="fail":
                        log.debug("[%s][%s] network test fail br1"%(provisionseq, userid))    
                        raise Exception("network test br2 fail")  
                    log.debug("[%s][%s] network test success br1"%(provisionseq, userid))    
            
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 네트워크 점검 완료했습니다.")     
            self.execute_set(myquery)     
 
            """
            Route검사
            """
            log.debug("[%s][%s] route test request "%(provisionseq, userid))    
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 라우트 점검 중입니다.")     
            self.execute_set(myquery) 

#             print "checkroute global_mgmt_netaddr %s"%global_mgmt_net
                                    
            result = self.checkroute(provisionseq, configinfo, userid, global_mgmt_netaddr, global_mgmt_net, "mgmt-net")
            if result["result"] =="fail":
                raise Exception("checkroute test error mgmt-net")          

#             print "checkroute red_netaddr %s"%red_netaddr

            if "eth1_vlan_status" in gInstance and gInstance["eth1_vlan_status"] == "off":
                message_network = u"NFV-UTM 레드라우트 상태(down) "
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
            else:
                result = self.checkroute(provisionseq, configinfo, userid, red_netaddr, global_mgmt_net, "red-net")
                if result["result"] =="fail":
                    raise Exception("checkroute test error red-net")              
                                    
            if green_test == 1:
                if "br0_status" in gInstance and gInstance["br0_status"] == "off":
                    message_network = u"NFV-UTM 그린라우트 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:                
                    result = self.checkroute(provisionseq, configinfo, userid, green_netaddr, global_mgmt_net, "green-net")
                    if result["result"] =="fail":
                        raise Exception("checkroute test error green-net")         
    
#                 print "checkroute orange_netaddr %s"%orange_netaddr

            if orange_test == 1:
                if "br1_status" in gInstance and gInstance["br1_status"] == "off":
                    message_network = u"NFV-UTM 오렌지라우트 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:                    
                    result = self.checkroute(provisionseq, configinfo, userid, orange_netaddr, global_mgmt_net, "orange-net")
                    if result["result"] =="fail":
                        raise Exception("checkroute test error orange-net")
            
            print "checkroute blue_netaddr %s"%blue_netaddr
                                
            if blue_test == 1:
                if "br2_status" in gInstance and gInstance["br2_status"] == "off":
                    message_network = u"NFV-UTM 블루라우트 상태(down) "
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, "", "", datetime.datetime.now(), datetime.datetime.now(), datetime.datetime.now()-datetime.datetime.now(), "", True , "")
                else:                     
                    result = self.checkroute(provisionseq, configinfo, userid, blue_netaddr, global_mgmt_net, "blue-net")
                    if result["result"] =="fail":
                        raise Exception("checkroute test error blue-net")    

            log.debug("[%s][%s] route test success "%(provisionseq, userid))  
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 라우트 점검 완료했습니다.")     
            self.execute_set(myquery) 
            
            """
            subnetip가 2개 이상인경우 
            """
#             green_ips_count = configinfo["greeninfo"]["br_index"]
#             print "green_ips_count %s"%green_ips_count
#             orange_ips_count = configinfo["orangeinfo"]["br_index"]
#             print "orange_ips_count %s"%orange_ips_count
#             blue_ips_count = configinfo["blueinfo"]["br_index"]
#             print "blue_ips_count %s"%blue_ips_count
             
            log.debug("[%s][%s] secondary ip test request "%(provisionseq, userid))    
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM Additional ip 점검 중입니다.")     
            self.execute_set(myquery) 

                
            # 각각의 IP가 2개 이상인 경우
            green_ips = configinfo["greeninfo"]["green_ips"]
            orange_ips = configinfo["orangeinfo"]["orange_ips"]
            blue_ips = configinfo["blueinfo"]["blue_ips"]               
            
#             print "green_ips %s"%green_ips
#             print "orange_ips %s"%orange_ips
#             print "blue_ips %s"%blue_ips
            
            
            if len(green_ips.split(",")) > 1:
                result = self.checksecondip(provisionseq, green_ips, global_mgmt_net, "br0")
                if result["result"] =="fail":
                    raise Exception("checksecondip test error br0")                      
            
            
            if len(orange_ips.split(",")) > 1:
                result = self.checksecondip(provisionseq, orange_ips, global_mgmt_net, "br1")
                if result["result"] =="fail":
                    raise Exception("checksecondip test error br1")   
            
                              
            if len(blue_ips.split(",")) > 1:
                result = self.checksecondip(provisionseq, blue_ips, global_mgmt_net, "br2")
                if result["result"] =="fail":
                    raise Exception("checksecondip test error br2")        
                
            """
            JOB테스트 
            """     
            log.debug("[%s][%s] route test request "%(provisionseq, userid))    
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM JOB 프로세스 점검 중입니다.")     
            self.execute_set(myquery) 
                            
            result = self.checkjobs(provisionseq, userid, global_mgmt_net, gInstance["syncseq"])
            if result["result"] =="fail":
                raise Exception("checkjobs test error ")  

            log.debug("[%s][%s] route test success "%(provisionseq, userid))  
            myquery = self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM JOB 프로세스 점검 완료했습니다.")     
            self.execute_set(myquery)           

            myquery= self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 점검을 완료했습니다.")
            self.execute_set(myquery)

            myquery= self.sql.setchangeprovisionstepforprovision(provisionseq, "7", u"서비스 설치가 완료되었습니다.")
            self.execute_set(myquery)

            end_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", "TEST Manager 서비스 종료", "", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, "", True, "")

#             myquery = self.sql.settracehistorychange(end_dttm_provision, provisionseq, gInstance["orgseq"])
#             self.execute_set(myquery)

            
            result = {"result":"success"}

        except Exception,e:
            log.error("test manager error %s"%e.message)
#             print e.message
            myquery= self.sql.setchangeprovisionstepforprovision(provisionseq, "6", u"NFV-UTM 점검을 실패했습니다.")
            self.execute_set(myquery)
            end_dttm_provision = datetime.datetime.now()
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", "TEST Manager 서비스 종료", "managetest()", "", start_dttm_provision, end_dttm_provision, end_dttm_provision-start_dttm_provision, "", False, "test manager error " +e.message)
                                       
            result = {"result":"fail", "description": "test manager error " +e.message}
            
        return result                    
    
    def checkport(self, provisionseq, userid, global_mgmt_net, port):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()                        

        print "checkport --> %s"%self.mgmt_endpoint
        for i in range(1,100,1):
            time.sleep(1)
            resultport = soap.sendData(self.mgmt_endpoint + "/check/port/"+port+"?utm_mgmt="+global_mgmt_net, "get", "")                    
#             resultport = json.loads(result)
            if i > 90:
                end_dttm = datetime.datetime.now() 
                log.debug("[%s] checkport fail"%i)
                cmd = resultport["cmd"]
                response = "check port fail"
                if port == "22":            
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 통신ssh 포트(22) 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")  
                elif port == "10443":
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 웹콘솔 포트(10443) 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
#                 self.trace.settracefortetstresult(provisionseq, userid, "6", "6.1.2", u"접속 포트 확인 실패", cmd, json.dumps(cmddetail))
                result = {"result":"fail", "description": response}     
                break    
            
            if resultport["result"] == "fail":
                log.debug("[%s] checkport reconnect"%i) 
                continue
            else:
                end_dttm = datetime.datetime.now() 
                log.debug("[%s] checkport success"%i)
                cmd = resultport["cmd"]
                response = json.dumps(resultport["detail"])                    
                if port == "22":            
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 통신ssh 포트(22) 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")  
                elif port == "10443":
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 웹콘솔 포트(10443) 점검", self.mgmt_endpoint+"/check/port/"+port, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                result = {"result":"success"}  
                break           
#                     raise Exception("%s time out "%global_mgmt_net)
                
        
#             result = {"result":"success"}        
                                                    
#         except Exception,e:
#             print e.message
#             cmd = "/check/port/22?utm_mgmt="+global_mgmt_net
#             cmddetail =e.message
#             self.trace.settracefortetstresult(provisionseq, userid, "6", "6.1.2", u"접속 포트 확인 실패", cmd, cmddetail)
#             result = {"result":"fail", "description": e.message}
        
        return result           
    
    def checknetwork(self, provisionseq, configinfo, userid, global_mgmt_net, searchip, network):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()  
        try:
#             for i in range(1,100,1): 
#                 time.sleep(1)   
#                 resultnetwork = soap.sendData(self.mgmt_endpoint + "/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, "get", "")       
#     #             resultnetwork= json.loads(result)
#                 if resultnetwork["result"] == "fail":
# #                     cmd = resultnetwork["cmd"]
# #                     cmddetail = resultnetwork["detail"]   
# #                     result = {"result":"fail", "description": cmddetail}
#                     continue
#                 else:
#                     end_dttm = datetime.datetime.now()
#                     cmd = resultnetwork["cmd"]
#                     response = json.dumps(resultnetwork["detail"])   
#                     if network == "eth0":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
#                     elif network == "eth1":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 인터넷네트워크  상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
#                     elif network == "br0":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 그린네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
#                     elif network == "br1":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 오렌지네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
#                     elif network == "br2":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 블루네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                                                                
#                     result = {"result":"success", "cmd":cmd, "detail":response}           
#                     break
#                 if i > 100:
#                     cmd = resultnetwork["cmd"]   
#                     response = json.dumps(resultnetwork["detail"])  
#                     if network == "eth0":        
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
#                     elif network == "eth1":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 인터넷네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
#                     elif network == "br0":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 그린네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
#                     elif network == "br1":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 오렌지네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
#                     elif network == "br2":
#                         self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 블루네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                        
#                     result = {"result":"fail", "cmd":cmd, "detail":response}          
            for i in range(1,30,1):
                time.sleep(1)
                resultnetwork = soap.sendData(self.mgmt_endpoint + "/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, "get", "")       
                if resultnetwork["result"] == "fail":
                    continue
                else:
                    break
            vlan = ""        
            if resultnetwork["result"] == "fail":
                end_dttm = datetime.datetime.now()
                cmd = resultnetwork["cmd"]   
                response = json.dumps(resultnetwork["detail"])  
                if network == "eth0":        
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                elif network == "eth1":
                    message_network = u"NFV-UTM 레드네트워크 상태 점검(vlan:%s)"%configinfo["redinfo"]["red_vlan"]             
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                elif network == "br0":                  
                    for row in configinfo["greeninfo"]["green_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 그린네트워크 상태 점검(vlan:%s)"%vlan
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                elif network == "br1":
                    for row in configinfo["orangeinfo"]["orange_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 오렌지네트워크 상태 점검(vlan:%s)"%vlan                          
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                elif network == "br2":
                    for row in configinfo["blueinfo"]["blue_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 블루네트워크 상태 점검(vlan:%s)"%vlan                        
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                        
                result = {"result":"fail", "cmd":cmd, "detail":response}   
            else:
                end_dttm = datetime.datetime.now()
                cmd = resultnetwork["cmd"]
                response = json.dumps(resultnetwork["detail"])   
                if network == "eth0":
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif network == "eth1":
                    message_network = u"NFV-UTM 레드네트워크 상태 점검(vlan:%s)"%configinfo["redinfo"]["red_vlan"]                           
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif network == "br0":
                    for row in configinfo["greeninfo"]["green_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 그린네트워크 상태 점검(vlan:%s)"%vlan                    
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif network == "br1":
                    for row in configinfo["orangeinfo"]["orange_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 오렌지네트워크 상태 점검(vlan:%s)"%vlan                                
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif network == "br2":
                    for row in configinfo["blueinfo"]["blue_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 블루네트워크 상태 점검(vlan:%s)"%vlan                       
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network+"?utm_mgmt="+global_mgmt_net+"&searchip="+searchip, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                                                                
                result = {"result":"success", "cmd":cmd, "detail":response}           
                                            
        except Exception,e:    
            print e.message
            log.error("test manager error %s"%e.message)
            end_dttm = datetime.datetime.now()
            cmd = self.mgmt_endpoint+"/check/net/"+network
            response =e.message + "-checknetwork()"
            if network == "eth0":        
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리네트워크 상태 점검", self.mgmt_endpoint+"/check/net/"+network, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif network == "eth1":
                message_network = u"NFV-UTM 레드네트워크 상태 점검(vlan:%s)"%configinfo["redinfo"]["red_vlan"]   
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif network == "br0":
                for row in configinfo["greeninfo"]["green_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 그린네트워크 상태 점검(vlan:%s)"%vlan                           
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif network == "br1":
                for row in configinfo["orangeinfo"]["orange_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 오렌지네트워크 상태 점검(vlan:%s)"%vlan                       
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif network == "br2":
                for row in configinfo["blueinfo"]["blue_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 블루네트워크 상태 점검(vlan:%s)"%vlan                    
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/net/"+network, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                  
            result = {"result":"fail", "description": e.message}
            
        return result    

    def checkroute(self, provisionseq, configinfo, userid, addr, global_mgmt_net, route):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()  
        try:
            vlan = ""
            resultroute = soap.sendData(self.mgmt_endpoint + "/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, "get", "")       
#             resultroute= json.loads(result)
            if resultroute["result"] == "fail":
                end_dttm = datetime.datetime.now()
                cmd = resultroute["cmd"]
                response = json.dumps(resultroute["detail"])   
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 라우트 상태 점검", self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
                result = {"result":"fail", "description": ""}        
            else:
                end_dttm = datetime.datetime.now()
                cmd = resultroute["cmd"]
                response = json.dumps(resultroute["detail"])     
                if route == "mgmt-net":        
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리라우트 상태 점검", self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif route == "red-net":
                    message_network = u"NFV-UTM 레드라우트 상태 점검(vlan:%s)"%configinfo["redinfo"]["red_vlan"]        
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif route == "green-net":
                    for row in configinfo["greeninfo"]["green_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 그린라우트 상태 점검(vlan:%s)"%vlan                           
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")
                elif route == "orange-net":
                    for row in configinfo["orangeinfo"]["orange_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 오렌지라우트 상태 점검(vlan:%s)"%vlan                         
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                  
                elif route == "blue-net":
                    for row in configinfo["blueinfo"]["blue_vlan"]:
                        vlan = vlan + row + " "
                    message_network = u"NFV-UTM 블루라우트 상태 점검(vlan:%s)"%vlan                        
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                
            result = {"result":"success"}           
            
        except Exception,e:    
            print e.message
            log.error("test manager error %s"%e.message)
            end_dttm = datetime.datetime.now()
            cmd = "/check/route/"+addr+"?utm_mgmt="+global_mgmt_net
            response = e.message +"-checkroute()"
            if route == "mgmt-net":        
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", u"NFV-UTM 관리라우트 상태 점검", self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif route == "red-net":
                message_network = u"NFV-UTM 레드라우트 상태 점검(vlan:%s)"%configinfo["redinfo"]["red_vlan"]                        
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif route == "green-net":
                for row in configinfo["greeninfo"]["green_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 그린라우트 상태 점검(vlan:%s)"%vlan                         
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            elif route == "orange-net":
                for row in configinfo["orangeinfo"]["orange_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 오렌지라우트 상태 점검(vlan:%s)"%vlan                   
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                  
            elif route == "orange-net":
                for row in configinfo["blueinfo"]["blue_vlan"]:
                    vlan = vlan + row + " "
                message_network = u"NFV-UTM 블루라우트 상태 점검(vlan:%s)"%vlan                      
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message_network, self.mgmt_endpoint+"/check/route/"+addr+"?utm_mgmt="+global_mgmt_net, cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")                  
                                
            result = {"result":"fail", "description": e.message}
            
        return result   
     
    def checkjobs(self, provisionseq, userid, global_mgmt_net, syncseq):
        soap = SoapClient()  
        try:
            myquery= self.sql.getutmjoblist()
            rows = self.execute_getone(myquery)
            check_dhcp = 0
            check_nat = 0
            check_firewall = 0
            check_vpn = 0
            check_route = 0
            check_ntp = 0
            cmd = ""
            response = ""
            start_dttm = datetime.datetime.now()
            for row in rows:
                if row["job_type"] == "DHCP":
                    self.jobtype = row["job_type"]        
                    self.job = row["jobname"]      
                    searchtext = "ok/"             
                    senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                    resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                    
                    if resultjob["result"] == "fail":
                        check_dhcp = check_dhcp + 1
                        end_dttm = datetime.datetime.now()
                    else:
                        end_dttm = datetime.datetime.now()  
                    
                    cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                    response = response + json.dumps(resultjob["detail"]) +"<br>"
                    message = u"NFV-UTM Job %s 상태 점검"%self.jobtype
            if check_dhcp == 0:
#                 cmd = resultjob["cmd"]
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                       
            else:
#                 cmd = " jobcontrol list|grep %s"%self.job
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "") 

            cmd = ""
            response = ""
            start_dttm = datetime.datetime.now()
            for row in rows:
                if row["job_type"] == "NAT":
                    self.jobtype = row["job_type"]        
                    self.job = row["jobname"]      
                    searchtext = "ok/"             
                    senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                    resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                
                    if resultjob["result"] == "fail":
                        check_nat = check_nat + 1
                        end_dttm = datetime.datetime.now()
                    else:
                        end_dttm = datetime.datetime.now()  

                    cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                    response = response + json.dumps(resultjob["detail"]) +"<br>"
                    message = u"NFV-UTM Job %s 상태 점검"%self.jobtype
                                        
            if check_nat == 0:
#                 cmd = resultjob["cmd"]
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                  
            else:
#                 cmd = " jobcontrol list|grep %s"%self.job
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            
            cmd = ""
            response = ""
            start_dttm = datetime.datetime.now()
            for row in rows:
                if row["job_type"] == "FIREWALL":
                    self.jobtype = row["job_type"]        
                    self.job = row["jobname"]      
                    searchtext = "ok/"             
                    senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                    resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                    if resultjob["result"] == "fail":
                        check_firewall = check_firewall + 1
                        end_dttm = datetime.datetime.now()
                    else:
                        end_dttm = datetime.datetime.now()  
                    cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                    response = response + json.dumps(resultjob["detail"]) +"<br>"
                    message = u"NFV-UTM Job %s 상태 점검"%self.jobtype
            if check_firewall == 0:
#                 cmd = resultjob["cmd"]
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "")                       
            else:
#                 cmd = " jobcontrol list|grep %s"%self.job
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")

            if  syncseq == "0":
                cmd = ""
                response = ""
                start_dttm = datetime.datetime.now()
                for row in rows:
                    if row["job_type"] == "VPN":
                        self.jobtype = row["job_type"]        
                        self.job = row["jobname"]      
                        searchtext = "ok/"             
                        senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                        resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                        if resultjob["result"] == "fail":
                            check_vpn = check_vpn + 1
                            end_dttm = datetime.datetime.now()
                        else:
                            end_dttm = datetime.datetime.now()  
                        cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                        response = response + json.dumps(resultjob["detail"]) +"<br>"
                        message = u"NFV-UTM Job %s 상태 점검"%self.jobtype            
                if check_vpn == 0:
    #                 cmd = resultjob["cmd"]
    #                 response = json.dumps(resultjob["detail"])  
    #                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "") 
                else:
    #                 cmd = " jobcontrol list|grep %s"%self.job
    #                 response = json.dumps(resultjob["detail"])  
    #                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                    self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
     
    #             if check_dhcp + check_nat + check_firewall + check_vpn + check_ntp== 0:
    #                 result = {"result":"success"} 
    #             else:
    #                 result = {"result":"fail", "description": ""}   

            cmd = ""
            response = ""
            start_dttm = datetime.datetime.now()
            for row in rows:
                if row["job_type"] == "ROUTING":
                    self.jobtype = row["job_type"]        
                    self.job = row["jobname"]      
                    searchtext = "ok/"             
                    senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                    resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                    if resultjob["result"] == "fail":
                        check_route = check_route + 1
                        end_dttm = datetime.datetime.now()
                    else:
                        end_dttm = datetime.datetime.now()  
                    cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                    response = response + json.dumps(resultjob["detail"]) +"<br>"
                    message = u"NFV-UTM Job %s 상태 점검"%self.jobtype            
            if check_vpn == 0:
#                 cmd = resultjob["cmd"]
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "") 
            else:
#                 cmd = " jobcontrol list|grep %s"%self.job
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")

#             if check_dhcp + check_nat + check_firewall + check_vpn + check_ntp== 0:
#                 result = {"result":"success"} 
#             else:
#                 result = {"result":"fail", "description": ""}   

            cmd = ""
            response = ""
            start_dttm = datetime.datetime.now()
            for row in rows:
                if row["job_type"] == "NTP":
                    self.jobtype = row["job_type"]        
                    self.job = row["jobname"]      
                    searchtext = "ok/"             
                    senddata = {"mgmtaddr" : global_mgmt_net, "job":self.job, "searchtext":searchtext}       
                    resultjob = soap.sendData(self.mgmt_endpoint + "/check/jobs", "post", json.dumps(senddata))
                    if resultjob["result"] == "fail":
                        check_ntp = check_ntp + 1
                        end_dttm = datetime.datetime.now()
                    else:
                        end_dttm = datetime.datetime.now()  
                    cmd = cmd + " jobcontrol list|grep %s"%self.job + "<br>"
                    response = response + json.dumps(resultjob["detail"]) +"<br>"
                    message = u"NFV-UTM Job %s 상태 점검"%self.jobtype            
            if check_ntp == 0:
#                 cmd = resultjob["cmd"]
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , "") 
            else:
#                 cmd = " jobcontrol list|grep %s"%self.job
#                 response = json.dumps(resultjob["detail"])  
#                 message = u"NFV-UTM %s(%s) 상태 점검"%(self.jobtype, self.job)
                self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")

            if check_dhcp + check_nat + check_firewall + check_vpn + check_ntp == 0:
                result = {"result":"success"} 
            else:
                result = {"result":"fail", "description": ""}   
                                                                                            
        except Exception,e:    
            log.error("test manager error %s"%e.message)
            print e.message
            end_dttm = datetime.datetime.now()
            cmd = "/check/jobs/utm_mgmt="+global_mgmt_net
            response = e.message +"-checkroute()"
            message = u"NFV-UTM Job %s(%s) 상태 점검"%(self.jobtype, self.job)
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/jobs", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            result = {"result":"fail", "description": e.message}
            
        return result    

    def checksecondip(self, provisionseq, ips, global_mgmt_net, nic):
        start_dttm = datetime.datetime.now()
        soap = SoapClient()  
        try:
            cmdlist = []
            responselist = []
            for addr in ips.split(","):
                senddata = {"utm_mgmt":global_mgmt_net, "searchip":addr, "nic":nic} 
                resultsecondip = soap.sendData(self.mgmt_endpoint + "/check/secondary", "post", json.dumps(senddata))                     
    #             resultroute= json.loads(result)
                if resultsecondip["result"] == "fail":
                    end_dttm = datetime.datetime.now()
                    cmdlist.append(resultsecondip["cmd"])
                    responselist.append(json.dumps(resultsecondip["detail"]))   
#                     message = u"NFV-UTM %s secondary ip 점검"%nic
#                     self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/secondary", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , ips)
                    result = {"result":"fail", "description": ""}        
                else:
                    end_dttm = datetime.datetime.now()
                    cmdlist.append(resultsecondip["cmd"])
                    responselist.append(json.dumps(resultsecondip["detail"]))   
#                     message = u"NFV-UTM %s secondary ip 점검"%nic
#                     self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/secondary", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , ips)
        
                
            cmd = ""
            for row in cmdlist:
                cmd = cmd + row + "<br>"

            response = ""
            for row in responselist:
                response = response + row + "<br>"
                                
            response = json.dumps(response)
            if nic == "br0":
                message = u"NFV-UTM 그린 Additional ip 점검"
            if nic == "br1":
                message = u"NFV-UTM 오렌지 Additional ip 점검"
            if nic == "br2":
                message = u"NFV-UTM 블루 Additional ip 점검"
                    
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/secondary", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, True , ips)
    
                    
            result = {"result":"success"}           
            
        except Exception,e:    
            print e.message
            log.error("test manager error %s"%e.message)
            end_dttm = datetime.datetime.now()
            cmd = "/check/secondary/"+addr+"?utm_mgmt="+global_mgmt_net
            response = e.message +"-checkroute()"
            message = u"NFV-UTM %s secondary ip 점검"%nic
            self.e2e.sete2etraceforprovisioning(provisionseq, "Prov-mgr", "TEST-mgr", message, self.mgmt_endpoint+"/check/secondary", cmd, start_dttm, end_dttm, end_dttm-start_dttm, response, False , "")
            
            result = {"result":"fail", "description": e.message}
            
        return result   

           
# if __name__ == '__main__':
#     
#     test = TestManager()
#     
#     test.checkjobs(4, "wooni", "221.151.188.156")