# -*- coding: utf-8 -*-

import ConfigParser

class SDI_Config:
	
	def __init__( self, config_file ):
		self.conf = ConfigParser.ConfigParser()
		self.conf.read(config_file)
	
	def get(self, section, option):
		return self.conf.get(section, option)

