#-*- coding: utf-8 -*-
'''
@project: NFV HA Manager
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 27.
@summary:
@author: 최재복
'''

import sys
import base64

from Crypto import Random
from Crypto.Cipher import AES


class AESCipher(object):
    """
    AES 암복호화
    """

    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        BS = 16
        pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        unpad = lambda s : s[:-ord(s[len(s)-1:])]
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))


if __name__ == '__main__':

    if len(sys.argv) != 3:
        print "Invalid Input!!!"
        exit(-1)

    if ( sys.argv[1] != "enc" ) and ( sys.argv[1] != "dec" ):
        print "Invalid Type"
        exit(-1)
    

    aes = AESCipher('Qfkrkstkghkwhgdk')

    if( sys.argv[1] == "enc" ):
        encstr = aes.encrypt(sys.argv[2])
        print encstr
    else:
        decstr = aes.decrypt(sys.argv[2])
        print decstr

