# -*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: ohhara
'''

import json
import os, sys

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

org_dir= os.getcwd()
sys.path.append(org_dir)

from config import apiCfg, logCfg
from util import logm
from ctrl import swc


class SWCtl(tornado.web.RequestHandler):
    def post(self):
        try:
            reqdata = json.loads(self.request.body)
            logm.info( 'API.Recv > SWCtl : ' + str(reqdata) )
            
            result = swc.execute('FO', reqdata)
            self.write(result)
            logm.info( 'API.Send > SWCtl : ' + str(result) )
        except Exception, e:
            logm.exc( e, 'SWCtl API' )
            self.write( {"status": "fail", "message": "POST API Fail(Exception)", "data": reqdata} )


def runApi():
    app = tornado.web.Application([
            (apiCfg['url']['swc'], SWCtl),
    ])
    tornado.options.parse_command_line()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.bind( apiCfg['port'] )
    http_server.start(0)
    tornado.ioloop.IOLoop.instance().start()


def main():
    logm.info( '========================== START NSSM ==========================' )

    logm.info( 'Api-Listener Run' )
    runApi()


if __name__ == '__main__':
    if not os.path.isdir(logCfg['dir']) :
        os.mkdir(logCfg['dir'])
    
    logm.init(logCfg['name'], logCfg['dir'], logCfg['file'], logCfg['fileSize'], logCfg['fileCnt'], logCfg['level'])
    main()
