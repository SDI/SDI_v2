# -*- coding: utf-8 -*-
'''
Created on 2015. 4. 20.

@author: ohhara
'''

import logging


apiCfg = {  'port': 8889,
            'url': dict( hism=  '/soap2/service/nssm/HISMng',
                         swc=  '/soap2/service/nssm/SWCtl',
                         cli=  '/soap2/service/nssm/CliMng'
                        )
         }

dbCfg = {   'name': 'so51LMzI33dz3OHmWtVq/SUdNOu64aJbV+9/wxY/pDI=',
            'user': 'mJmSIjRTojiVDWgGIaWXdh9dgHaf6ugqFYaevZ+/H4c=',
            'pass': 'WZyGjRJwBROD5kXB3rlXDVtMSQgNiTRWUK3FsBzuogU=',
            'host': 'HXTntE71gjWCFSmKy/kbRNbMAPljVQmmKkDjlg1+e90=',
            'port': 'Rvx96C0lpxO9/ihxVG4txgatjcyu8B0hQcMnnxyXKe8='}

snmpCfg = { 'port': 'CEFU2dCRWBTlSYiIiVmbZdAQVX7rbg+s/2f2fXEbGIg=',
            'comm': 'lqJ0v6EQOvfdAJtAkfbZiXfCB57B4UPqSJk2tzvpqnM=',
            'portOID': 'CasYaZ0C9si5psdZPTKJxwMcM9ptekp/KvZ6I1CEh5zhexyLvi4Z2HrV3w22swpb'}

logCfg = { 'name': 'nsscLogger',
           'dir' : '../log',
           'file': 'nssc.log',
           'fileSize': 1024*1024*2,
           'fileCnt': 10,
           'level': logging.INFO}

