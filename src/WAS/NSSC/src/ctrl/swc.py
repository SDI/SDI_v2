# -*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: ohhara
'''

import copy

import util.logm as logm
import db.psql_sql as psql_sql
from config import dbCfg, snmpCfg
from db.psql_db import PsqlDb
from util import snmpm
from util.aescipher import AESCipher

def execute( cmdType, params):
    logm.debug( "SWCtl execute")
    if str(cmdType) == 'FO' :
        return FailOver().run(params)
    if str(cmdType) == 'CA' :
        return ConfigApply().run(params)
    #if str(cmdType) == 'SS' :
    #    return ShowSwitch
    else :
        logm.warn( "Invalid SWCtl Type -> " + cmdType )
        return {"status": "fail", "message": "Invalid Operation", "data": params}
    



class FailOver():
    
    def __init__(self):
        self.aes = AESCipher('Qfkrkstkghkwhgdk')
        self.db = PsqlDb(self.aes.decrypt(dbCfg['name']), 
                         self.aes.decrypt(dbCfg['host']), self.aes.decrypt(dbCfg['port']), 
                         self.aes.decrypt(dbCfg['user']), self.aes.decrypt(dbCfg['pass']))
        self.db.connect()

    def __del__(self):
        self.db.release()

    def run(self, params):
        #{“failover_ip”: ”10.0.0.101”}
        oriIP = params.get('failover_ip')
        logm.info( "REQ > FailOver [oriIP=" + str(oriIP) + "]" )
        
        try:
            
            # Active 인지 확인
            sql = psql_sql.isActiveUtm( oriIP )
            ret = self.db.select( sql['sql'], sql['param'] )
            logm.debug( str(ret) )
            if ret[0][0] <= 0 :
                return self.returnFail( "Not ACTIVE UTM(" + oriIP + ")", params )
            
            # 장애 UTM ID, name 가져오기
            sql = psql_sql.getUtmIdByIP( oriIP )
            ret = self.db.select( sql['sql'], sql['param'] )
            logm.debug( str(ret) )
            if len(ret) <= 0 :
                return self.returnFail( "Unknown UTM(ip=" + str(oriIP) + ")", params )
            prvUtmID = ret[0][0]
            prvUtmName = ret[0][1]
            logm.info("FO : Fail_UtmID=" + str(prvUtmID) )
            
            # 장애 UTM 연결정보 가져오기(스위치이름, 스위치 ip, 포트, utmid, utm 이름, link)
            sql = psql_sql.getConns(prvUtmID)
            ret = self.db.select( sql['sql'], sql['param'] )
            logm.debug( str(ret) )
            if len(ret) <= 0 :
                return self.returnFail( "No FailOver_UTM Connection Info", params )
            prevConns = copy.copy(ret)
            logm.info( "FO : Fail_UtmConnInfo=" + str(prevConns) )
            
            # 절체할 UTM ID, name, ip 가져오기
            sql = psql_sql.getHaTargets(prvUtmID)
            ret = self.db.select( sql['sql'], sql['param'] )
            logm.debug( str(ret) )
            if len(ret) <= 0 :
                return self.returnFail( "No FailOver_Target_UTM ", params )
            tarUtm = ret[0][0]
            tarUtmName = ret[0][1]
            tarIP = ret[0][2]
            logm.info("FO : FailOver_Target_UtmID=" + str(prvUtmID) )

            # 절체할 UTM 연결정보 가져오기(스위치이름, 스위치 ip, 포트, utmid, utm 이름, link)
            sql = psql_sql.getConns(tarUtm)
            ret = self.db.select( sql['sql'], sql['param'] )
            logm.debug( str(ret) )
            if len(ret) <= 0 :
                return self.returnFail( "No FailOver_Target_UTM Connection Info", params )
            tarConns = copy.copy(ret)
            logm.info( "FO : FailOver_Target_UTMConnectionInfo=" + str(tarConns) )
            
            # 스위치로 절체 요청 및 결과 저장하기(UP한 후 DOWN)snmpCfg['comm'], snmpCfg['ip'], snmpCfg['port']
            #[('Sw1', '10.0.0.100', 10, 1, 'Utm1', 'LAN')]
            portOid = self.aes.decrypt(snmpCfg['portOID'])
            comm = self.aes.decrypt(snmpCfg['comm'])
            snmpPort = int(self.aes.decrypt(snmpCfg['port']))
            
            tarUnSetList = []
            tarLanPort = -1
            for targC in tarConns :
                if targC[5] == 'LAN' :
                    tarLanPort = targC[2]
                
                oid = portOid + ".1" + str(targC[2]).zfill(2)
                ret = snmpm.sendSetOneMsg( targC[1], comm, oid, 1, snmpPort )
                logm.debug(ret)
                if not self.chkSnmpRes(ret) : 
                    oid = portOid + ".10" + str(targC[2]).zfill(2)
                    ret = snmpm.sendSetOneMsg( targC[1], comm, oid, 1, snmpPort )
                    logm.debug(ret)
                if not self.chkSnmpRes(ret) : 
                    tarUnSetList.append( ( targC[0], targC[1], targC[2], targC[4], targC[5] ) )
                    logm.err( "Fail to Set(UP) switch(" + str(targC[0]) + ", " + str(targC[1])\
                               + ") - Connected UTM(" + str(targC[4]) + ", " + str(targC[5]) + ")" )
                else :
                    self.saveFailOver(targC[3], 'Active')
            
            prvUnSetList = []
            prvLanPort = -1
            
            for prevC in prevConns :
                if prevC[5] == 'LAN' :
                    prvLanPort = prevC[2]
                
                oid = portOid + ".1" + str(prevC[2]).zfill(2)
                ret = snmpm.sendSetOneMsg( prevC[1], comm, oid, 2, snmpPort )
                logm.debug(ret)
                if not self.chkSnmpRes(ret) : 
                    oid = portOid + ".10" + str(prevC[2]).zfill(2)
                    ret = snmpm.sendSetOneMsg( prevC[1], comm, oid, 2, snmpPort )
                    logm.debug(ret)
                if not self.chkSnmpRes(ret) : 
                    prvUnSetList.append( ( prevC[0], prevC[1], prevC[2], prevC[4], prevC[5] ) )
                    logm.err( "Fail to Set(DOWN) switch(" + str(prevC[0]) + ", " + str(prevC[1])\
                               + ") - Connected UTM(" + str(prevC[4]) + ", " + str(prevC[5]) + ")" )
                else :
                    self.saveFailOver(prevC[3], 'Standby')
            
            if len(tarUnSetList) <= 0 and len(prvUnSetList) <= 0 :
                desc = str(prvUtmName) + '(' + str(oriIP) + ') -> ' + str(tarUtmName) + '(' + str(tarIP) + ')'
                return {"status": "success", "data": {"active_appliance_ip": str(tarIP), "active_port": tarLanPort, "standby_appliance_ip": str(oriIP), "standby_port": prvLanPort}}
            else :
                desc = "Unable to Set Switch\ntoUP=" + str(tarUnSetList) + "\ntoDOWN=" + str(prvUnSetList)
                return self.returnFail(desc, params)

        except Exception, e:
            logm.exc(e, "FailOver" )
            return {"status": "fail", "message": "Operation Fail", "data": params}

    def returnFail(self, err, params):
        logm.err( "Fail FO : " + err )
        return {"status": "fail", "message": err, "data": params}

    def saveFailOver(self, utmID, state):
        sql = psql_sql.saveFailOverResult(utmID, state)
        ret = self.db.execSql(sql['sql'], sql['param'])
        if ret <= 0 :
            return utmID
        return None

    def chkSnmpRes(self, ret):
        if ret == None or len(ret) <= 0 or ret[0][1] == None or ret[0][1] == '' :
            return False
        return True



class ConfigApply():
    
    def __init__(self):
        self.db = PsqlDb(dbCfg['name'], dbCfg['host'], dbCfg['port'], dbCfg['user'], dbCfg['pass'])
        self.db.connect()

    def __del__(self):
        self.db.release()

    def run(self, params):
        #{ "user": "admin", "cmd":"apply", "to": "sw", "utm_list": [ "utmtmp1", "utmtmp2" ] }
        
        pass


