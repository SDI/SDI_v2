# -*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: ohhara
'''
import psycopg2
import threading

from util import logm


class PsqlDb():
    
    def __init__(self, dbName, host, port, user, passwd, autocommit=True):
        self.__dbName = dbName
        self.__host = host
        self.__port = port
        self.__user = user
        self.__passwd = passwd
        self.__autocommit = autocommit
        self.__lock = threading.Lock()
    
    def connect(self):
        self.__lock.acquire()
        try:
            self.__conn = psycopg2.connect(database = self.__dbName, 
                                           user = self.__user, password = self.__passwd, 
                                           host = self.__host, port = self.__port)
            logm.info( 'DB : Connected Postresql : ' + self.__dbName )
            self.__conn.autocommit = self.__autocommit
        except Exception, e:
            logm.cri( "DBFail : psycopg2.connect " )
            logm.exc( e )
            self.__lock.release()
            return False
        
        try:
            self.__cursor = self.__conn.cursor()
            logm.info( 'DB : Get cursor' )
        except:
            logm.cri( "DBFail : get Cursor" )
            logm.exc( e )
            return False
        finally:
            self.__lock.release()

        return True
    
    def release(self):
        try:
            self.__lock.acquire()
            self.__cursor.close()
            self.__conn.close()
            logm.info( 'DB : Close Postresql : ' + self.__dbName )
        except Exception, e:
            logm.cri( "DBFail : DB close" )
            logm.exc(e)
        finally:
            self.__lock.release()

    def select(self, sql, params):
        logm.debug( 'DB : ' + sql + ":" + str(params) )
        rs = []
        try:
            self.__lock.acquire()
            self.__cursor.execute(sql, params)
            for row in self.__cursor:
                rs.append( row )
            return rs
        except Exception, e:
            logm.err( 'DBFail : ' + sql + ', ' + str(params) )
            raise e
        finally:
            self.__lock.release()
            
    def execSql(self, sql, params):
        logm.debug( 'DB : ' + sql + ":" + str(params) )
        self.__lock.acquire()
        try:
            self.__cursor.execute(sql, params)
            return self.__cursor.rowcount
        except Exception, e:
            logm.err( 'DBFail : ' + sql + ', ' + str(params) )
            raise e
        finally:
            self.__lock.release()
    
    def isConn(self):
        return self.__conn



