#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 4.
@summary: DB 관련 작업 베이스 클래스
@author: 진수
'''
import psycopg2

from config.monitor_config import postgresql_db_info, key
from util.aescipher import AESCipher
from helper.logHelper import myLogger

class PsycopgHelper(object):
    def __init__(self):
        self.init_config_from_dict_config_file()

    def init_config_from_dict_config_file(self):
        #복호화를 위한 키값 불러오기
        aes = AESCipher(key['key'])
        #암호화된 DB정보 복호화
        self.conn = psycopg2.connect(database=aes.decrypt(postgresql_db_info['db']),
                                     user=aes.decrypt(postgresql_db_info['id']),
                                     password=aes.decrypt(postgresql_db_info['pw']),
                                     host=aes.decrypt(postgresql_db_info['db_host']),
                                     port=aes.decrypt(postgresql_db_info['port']))
        
    def execute(self, query):
        """
        Created on 2015. 2. 5.
        @author: 진수
        @summary: db 처리 수행, 컬럼명과 rows를 리턴
        @param query: 데이터베이스 쿼리 문자열
        @rtype: dict
        """
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            columns = [desc[0] for desc in cur.description]
            rows = cur.fetchall()
        except psycopg2.DatabaseError, e:
            raise e

        return {'columns':columns, 'rows':rows}

    def execute_commit(self, query):
        """
        Created on 2015. 2. 12.
        @author: 세민
        @summary: db 처리 수행, insert, update, delete들의 commit 처리
        @param query: 데이터베이스 쿼리 문자열
        """
        try:
            cur = self.conn.cursor()
            cur.execute(query)
            self.conn.commit()
        except psycopg2.DatabaseError, e:
            raise e

    def __del__(self):
        if self.conn :
            self.conn.close()

if __name__ == '__main__':
    aaa = PsycopgHelper()
    result = aaa.execute('select * from tb_user')
    print result['rows']
