#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 27.

@author: SM
'''
class InfraSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getInfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s""" % orgseq
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result

    def setInfra(self, orgseq, props):
        result = """insert into tb_openstack_infra(orgseq, infraprops) values('%s', '%s')""" % (orgseq, props)
        return result

    def updateInfra(self, orgseq, props):
        result = """update tb_openstack_infra set infraprops = '%s' where orgseq = '%s'""" % (props, orgseq)
        return result