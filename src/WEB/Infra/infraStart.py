#-*- coding: utf-8 -*-
'''
Created on 2015. 3. 16.

@author: SM
'''
import tornado.httpserver
import tornado.ioloop
import tornado.web
import json
import os

from tornado.options import define, options
from helper.psycopg_helper import PsycopgHelper

import psycopg2
from infrasql import InfraSql
from config.monitor_config import key
from util.aescipher import AESCipher

define("port", default=9000, help="run on the given port", type=int)

class InfraManageHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('infra.html')

class InfraListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = int(self.get_argument('orgseq', '0'))

        infrasql = InfraSql()
        aes = AESCipher(key['key'])

        orgs = self.execute(infrasql.getOrgs())
        result = []

        for row in orgs['rows']:
            row = dict(zip(orgs['columns'], row))
            result.append(row)

        orgs = json.dumps(result)

        props = self.execute(infrasql.getInfra(orgseq))['rows']

        if not props :
                props = json.dumps([{"is_success": "f"}])
        else :
            props = props[0][0]

            props['openstack_connect']['token_url'] = aes.decrypt(props['openstack_connect']['token_url'])
            props['openstack_connect']['host'] = aes.decrypt(props['openstack_connect']['host'])
            props['openstack_connect']['horizon'] = aes.decrypt(props['openstack_connect']['horizon'])
            props['openstack_connect']['password'] = aes.decrypt(props['openstack_connect']['password'])
            props['openstack_connect']['userid'] = aes.decrypt(props['openstack_connect']['userid'])
            props['openstack_connect']['tenantid'] = aes.decrypt(props['openstack_connect']['tenantid'])

            props['openstack_db_connect']['database'] = aes.decrypt(props['openstack_db_connect']['database'])
            props['openstack_db_connect']['host'] = aes.decrypt(props['openstack_db_connect']['host'])
            props['openstack_db_connect']['password'] = aes.decrypt(props['openstack_db_connect']['password'])
            props['openstack_db_connect']['userid'] = aes.decrypt(props['openstack_db_connect']['userid'])
            props['openstack_db_connect']['port'] = aes.decrypt(props['openstack_db_connect']['port'])

            props['host_list'][0]['name'] = aes.decrypt(props['host_list'][0]['name'])
            props['host_list'][0]['ip'] = aes.decrypt(props['host_list'][0]['ip'])
            props['host_list'][0]['id'] = aes.decrypt(props['host_list'][0]['id'])
            props['host_list'][0]['password'] = aes.decrypt(props['host_list'][0]['password'])

            props['mgmtvms'][0]['endpoint'] = aes.decrypt(props['mgmtvms'][0]['endpoint'])
            props['mgmtvms'][0]['host'] = aes.decrypt(props['mgmtvms'][0]['host'])
#             props['mgmtvms'][0]['id'] = aes.decrypt(props['mgmtvms'][0]['id'])
            props['mgmtvms'][0]['userid'] = aes.decrypt(props['mgmtvms'][0]['userid'])
            props['mgmtvms'][0]['password'] = aes.decrypt(props['mgmtvms'][0]['password'])

        self.write({"props" : props, "orgs" : orgs})

class InfraInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        props = self.get_argument('props')
        props = json.loads(props)
        data = 0

        infrasql = InfraSql()
        aes = AESCipher(key['key'])

        try:
            props['openstack_connect']['token_url'] = aes.encrypt(props['openstack_connect']['token_url'])
            props['openstack_connect']['host'] = aes.encrypt(props['openstack_connect']['host'])
            props['openstack_connect']['horizon'] = aes.encrypt(props['openstack_connect']['horizon'])
            props['openstack_connect']['password'] = aes.encrypt(props['openstack_connect']['password'])
            props['openstack_connect']['userid'] = aes.encrypt(props['openstack_connect']['userid'])
            props['openstack_connect']['tenantid'] = aes.encrypt(props['openstack_connect']['tenantid'])

            props['openstack_db_connect']['database'] = aes.encrypt(props['openstack_db_connect']['database'])
            props['openstack_db_connect']['host'] = aes.encrypt(props['openstack_db_connect']['host'])
            props['openstack_db_connect']['password'] = aes.encrypt(props['openstack_db_connect']['password'])
            props['openstack_db_connect']['userid'] = aes.encrypt(props['openstack_db_connect']['userid'])
            props['openstack_db_connect']['port'] = aes.encrypt(props['openstack_db_connect']['port'])

            props['host_list'][0]['name'] = aes.encrypt(props['host_list'][0]['name'])
            props['host_list'][0]['ip'] = aes.encrypt(props['host_list'][0]['ip'])
            props['host_list'][0]['id'] = aes.encrypt(props['host_list'][0]['id'])
            props['host_list'][0]['password'] = aes.encrypt(props['host_list'][0]['password'])

            props['mgmtvms'][0]['endpoint'] = aes.encrypt(props['mgmtvms'][0]['endpoint'])
            props['mgmtvms'][0]['host'] = aes.encrypt(props['mgmtvms'][0]['host'])
#             props['mgmtvms'][0]['id'] = aes.encrypt(props['mgmtvms'][0]['id'])
            props['mgmtvms'][0]['userid'] = aes.encrypt(props['mgmtvms'][0]['userid'])
            props['mgmtvms'][0]['password'] = aes.encrypt(props['mgmtvms'][0]['password'])

            props = json.dumps(props)

            self.execute_commit(infrasql.setInfra(orgseq, props))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class InfraModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument('orgseq')
        props = self.get_argument('props')
        props = json.loads(props)
        data = 0

        infrasql = InfraSql()
        aes = AESCipher(key['key'])

        try:
            props['openstack_connect']['token_url'] = aes.encrypt(props['openstack_connect']['token_url'])
            props['openstack_connect']['host'] = aes.encrypt(props['openstack_connect']['host'])
            props['openstack_connect']['horizon'] = aes.encrypt(props['openstack_connect']['horizon'])
            props['openstack_connect']['password'] = aes.encrypt(props['openstack_connect']['password'])
            props['openstack_connect']['userid'] = aes.encrypt(props['openstack_connect']['userid'])
            props['openstack_connect']['tenantid'] = aes.encrypt(props['openstack_connect']['tenantid'])

            props['openstack_db_connect']['database'] = aes.encrypt(props['openstack_db_connect']['database'])
            props['openstack_db_connect']['host'] = aes.encrypt(props['openstack_db_connect']['host'])
            props['openstack_db_connect']['password'] = aes.encrypt(props['openstack_db_connect']['password'])
            props['openstack_db_connect']['userid'] = aes.encrypt(props['openstack_db_connect']['userid'])
            props['openstack_db_connect']['port'] = aes.encrypt(props['openstack_db_connect']['port'])

            props['host_list'][0]['name'] = aes.encrypt(props['host_list'][0]['name'])
            props['host_list'][0]['ip'] = aes.encrypt(props['host_list'][0]['ip'])
            props['host_list'][0]['id'] = aes.encrypt(props['host_list'][0]['id'])
            props['host_list'][0]['password'] = aes.encrypt(props['host_list'][0]['password'])

            props['mgmtvms'][0]['endpoint'] = aes.encrypt(props['mgmtvms'][0]['endpoint'])
            props['mgmtvms'][0]['host'] = aes.encrypt(props['mgmtvms'][0]['host'])
#             props['mgmtvms'][0]['id'] = aes.encrypt(props['mgmtvms'][0]['id'])
            props['mgmtvms'][0]['userid'] = aes.encrypt(props['mgmtvms'][0]['userid'])
            props['mgmtvms'][0]['password'] = aes.encrypt(props['mgmtvms'][0]['password'])

            props = json.dumps(props)

            self.execute_commit(infrasql.updateInfra(orgseq, props))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class CryptHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('crypt.html')

class EncryptHandler(tornado.web.RequestHandler):
    def post(self):
        encrypt = self.get_argument('encrypt')
        aes = AESCipher(key['key'])

        result = aes.encrypt(encrypt)

        self.write({"result" : result})

class DecryptHandler(tornado.web.RequestHandler):
    def post(self):
        decrypt = self.get_argument('decrypt')
        aes = AESCipher(key['key'])

        result = aes.decrypt(decrypt)

        self.write({"result" : result})

if __name__ == '__main__':
    tornado.options.parse_command_line()

    app = tornado.web.Application(
        handlers = [
                    (r'/', InfraManageHandler),
                    (r'/infra/infraList', InfraListHandler),
                    (r'/infra/insertInfra', InfraInsertHandler),
                    (r'/infra/updateInfra', InfraModifyHandler),
                    (r'/crypt', CryptHandler),
                    (r'/encrypt', EncryptHandler),
                    (r'/decrypt', DecryptHandler)
                   ],
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        data_dir=os.path.join(os.path.dirname(__file__), "cert"),
        cookie_secret="fggakfdjdlakjf;klja;lfj;lkdfgf",
        debug=True
    )
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
#     http_server.listen(11443)
    tornado.ioloop.IOLoop.instance().start()