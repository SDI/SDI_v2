#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 1. 28.
@summary:
@author: 진수
'''
import json
from string import strip
from time import sleep

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders

import config.openstack_config  # config 정보파일을 import

class BaseClient:
    """
    openstack rest 호출을 수행하는 class의 baseclass.
    token, endpoint 처리 수행
    """
    def __init__(self):
        self.init_config_from_dict_config_file()

    def init_config_from_dict_config_file(self):
        self.TOKEN_URL = config.openstack_config.seocho_openstack['connect']['token_url']
        self.BASIC_NETWORKS = config.openstack_config.seocho_openstack['networks']
        self.UTM = config.openstack_config.seocho_openstack['utm']

    def sendData(self, url, httpMethod, reqBody=None, tokenid=None):
        """
        @summary: openstack rest api 호출 수행
        @param url: openstack rest api URL
        @param httpMethod: HTTPMETHOD(get/post/put/delete)
        @param reqBody: post요청 시 , requestbody(json)
        @param tokenid: openstack api를 호출하기 위한 인증 토큰 아이디
        @rtype: dictionary
        """
        http_client = httpclient.HTTPClient()
        dict_h = {"content-type":"application/json;charset=UTF-8", "accept":"application/json"}
        if tokenid is not None:
            dict_h['X-Auth-Token'] =  tokenid

        print "request header --> %s" %dict_h
        h = HTTPHeaders(dict_h)
        if httpMethod == "post":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=1)
        elif httpMethod == "delete" :
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=None, request_timeout=3)
        elif httpMethod == "get":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=3)
        elif httpMethod == "put":
            request = HTTPRequest(url=url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=3)


        try:
            response = http_client.fetch(request=request)
            http_client.close()
#             print type(response.body)
#             print response.body
#             return response.body
            if httpMethod == "get" :
                return json.loads(response.body)
            elif httpMethod == "post" :
                return json.loads(response.body)
            else:
                return []
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
#             result = json.dumps(errResponse)
            return errResponse
        finally:
            if http_client:
                http_client.close()

    def set_openstack_config(self, authinfo):
        """
        openstack에 access한 후, token, endpoint 정보를 저장한다.
        keyston url만 adminURL로 저장한다.(publicURL과 주소 다르다. keystone에 명령은 대부분 admin 권한만이 수행가능)
        """
#         authinfo = json.loads(auth_result)
        print authinfo
        self.TOKEN_ID = authinfo['access']['token']['id']
        for serviceCatalog in authinfo['access']['serviceCatalog']:
            if serviceCatalog['type'] == 'compute':
                self.COMPUTE_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", config.openstack_config.seocho_openstack['connect']['host'] )
                print "COMPUTE_URL --> %s" %self.COMPUTE_URL
            elif serviceCatalog['type'] == 'network':
                self.NETWORK_URL = serviceCatalog['endpoints'][0]['publicURL'].replace("controller", config.openstack_config.seocho_openstack['connect']['host']  )
                print "NETWORK_URL --> %s" %self.NETWORK_URL
            elif serviceCatalog['type'] == 'identity':
                self.IDENTITY_URL = serviceCatalog['endpoints'][0]['adminURL'].replace("controller", config.openstack_config.seocho_openstack['connect']['host']  )
                print "IDENTITY_URL --> %s" %self.IDENTITY_URL
            else:
                pass

    def authenticate(self, tenantname, username, password, authurl):
        self.tenantname = tenantname
        self.username = username
        self.password = password
        self.authurl = authurl

        request = {"auth":{"tenantName":tenantname,"passwordCredentials":{"username":username,"password":password}}}
        self.set_openstack_config(self.sendData(authurl, 'post', json.dumps(request)))


class NeutronClient(BaseClient):
    def listNetworks(self):
        url = self.NETWORK_URL + '/v2.0/networks'
        print "request url --> %s" %url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getNetwork(self, networkid):
        url = self.NETWORK_URL + '/v2.0/networks/' + networkid
        print "request url --> %s" %url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createNetwork(self, tenantid, networkname, networkType, physicalNetwork, segmentationId, adminStateUp, routerExternal=False):
        """
        network를 생성 수행.
        - 이 함수는 "Networking API v2.0 extensions"을 사용하기 때문에, admin 권한의 계정만이 수행할 수 있음.
        """
        url = self.NETWORK_URL + '/v2.0/networks'
        print url
        requestbody = {"network":{"name":networkname,"tenant_id":tenantid, "provider:network_type":networkType,
                                  "provider:physical_network":physicalNetwork,"provider:segmentation_id":segmentationId,
                                  "admin_state_up":adminStateUp, "router:external":routerExternal
                                  }
                       }
        print "request url --> %s" %url
        print "request body --> %s" %requestbody
        return self.sendData(url, 'post', json.dumps(requestbody), tokenid=self.TOKEN_ID)

    def deleteNetwork(self, networkid):
        url = self.NETWORK_URL + '/v2.0/networks/' + networkid
        print "request url --> %s" %url
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createSubnet(self, tenantid, subnetname, networkid, ipversion, cidr, enabledhcp):
        url = self.NETWORK_URL + '/v2.0/subnets'
        requestbody = {"subnet":{
                                 "tenant_id":tenantid,
                                 "name":subnetname,"network_id":networkid,
                                 "ip_version":ipversion, "cidr":cidr,
                                 "enable_dhcp":enabledhcp
                                 }
                       }
        print "request url --> %s" %url
        print "request body --> %s" %json.dumps(requestbody)
        return self.sendData(url, 'post', json.dumps(requestbody), tokenid=self.TOKEN_ID)

    def deleteSubnet(self, subnetid):
        url = self.NETWORK_URL + '/v2.0/subnets/' + subnetid
        print "request url --> %s" %url
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)


class KeystoneClient(BaseClient):


    def listTenants(self):
        url = self.IDENTITY_URL + '/tenants'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getTenantByName(self, tenantname):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary:
        @param tenantname: 테넌트명
        @rtype: dict
        @return: {"tenant": {"enabled": true, "description": "", "name": "daemon", "id": "7b60dfe6d426412a86c4202fccdabe2a"}}
        """
        url = self.IDENTITY_URL + '/tenants?name=' + tenantname
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createTenant(self, tenantname):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: 테넌트(프로젝트) 생성
        @param tenantname:
        @rtype: dict
        @return: {"tenant": {"enabled": true, "description": "", "name": "daemon", "id": "7b60dfe6d426412a86c4202fccdabe2a"}}
        """
        url = self.IDENTITY_URL + '/tenants'
        reqbody = {'tenant':{'name':tenantname, 'enabled':True}}
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteTenant(self, tenantid):
        url = self.IDENTITY_URL + '/tenants/' + tenantid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createUser(self, tenantuuid, username, password):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: user 생성
        @param tenantuuid: 테넌트 uuid
        @param username: 사용자명
        @param password: 비밀번호
        @rtype: dict
        @return: {"user": {"username": "daemon", "tenantId": "7b60dfe6d426412a86c4202fccdabe2a", "enabled": true, "name": "daemon", "id": "4707a2c250e345c9bae10a48ff989931"}}
        """
        url = self.IDENTITY_URL + '/users'
#         print url
        reqbody = {'user':{'tenantId':tenantuuid, 'name':username, 'enabled':True, 'OS-KSADM:password':password}}
#         print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteUser(self, userid):
        url = self.IDENTITY_URL + '/users/' + userid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def addRoleToUser(self, tenantuuid, useruuid, roleuuid):
        url = self.IDENTITY_URL + '/tenants/' + tenantuuid + '/users/' + useruuid + '/roles/OS-KSADM/' + roleuuid
        print url
        return self.sendData(url, 'put', '', tokenid=self.TOKEN_ID)

    def listRoles(self):
        url = self.IDENTITY_URL + '/OS-KSADM/roles'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getRoleByRoleName(self, rolename):
        """
        Created on 2015. 2. 12.
        @author: 진수
        @summary: role 정보 가져오기
        @param rolename: 롤이름
        @rtype: dict
        @return: {"id": "1456a09e26c943f7ac653be26542c4cf", "name": "member"}
        """
        url = self.IDENTITY_URL + '/OS-KSADM/roles'
        roles = self.sendData(url, 'get', tokenid=self.TOKEN_ID)
#         print type(roles)
#         print roles
        result = {}
        for role in roles['roles']:
#             print type(role)
#             print role
            if role['name'] == rolename:
                result = role

        return result

    def getUserByName(self, username):
        url = self.IDENTITY_URL + '/users?name=' + username
        print url
        jsonresult = self.sendData(url, 'get', tokenid=self.TOKEN_ID)
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)



class NovaClient(BaseClient):
    def getKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs/' + keypairName
        print url
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs'
        reqbody = {'keypair':{'name':keypairName}}
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteKeypair(self, keypairName):
        url = self.COMPUTE_URL + '/os-keypairs/' + keypairName
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def listSecurityGroups(self):
        url = self.NETWORK_URL + '/v2.0/security-groups'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def isSecurityGroupByName(self, sgname):
        securityGroups = self.listSecurityGroups()
        result = False
        for sg in securityGroups['security_groups']:
            print json.dumps(sg)
            if sg['name'] == sgname:
                result = True
                break
        return result

    def createSecurityGroup(self, sgname, sgdesc):
        url = self.NETWORK_URL + '/v2.0/security-groups'
        print url
        reqbody = {'security_group':{'name':sgname, 'description':sgdesc}}
        print "reqbody --> %s" %json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def createSecurityGroupRule(self, direction, portRangeMin, portRangeMax, ethertype, protocol, sgid):
        url = self.NETWORK_URL + '/v2.0/security-group-rules'
        print url
        reqbody = {'security_group_rule':{
                                          'direction':direction, 'ethertype':ethertype,
                                          'port_range_min':portRangeMin, 'port_range_max':portRangeMax,
                                          'protocol':protocol,
                                          'security_group_id':sgid
                                          }
                   }
        print "reqbody --> %s" %json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def createSecurityGroupRulesForUTM(self, sgid):
        url = self.NETWORK_URL + '/v2.0/security-group-rules'
        reqrules = []
        reqrule = {'security_group_rule':{
                                          'direction':'ingress', 'ethertype':'IPv4',
                                          'port_range_min':'80', 'port_range_max':'80',
                                          'protocol':'tcp',
                                          'security_group_id':sgid
                                          }
                   }
        reqrules.append(reqrule)
        reqrule = {'security_group_rule':{
                                          'direction':'ingress', 'ethertype':'IPv4',
                                          'port_range_min':'443', 'port_range_max':'443',
                                          'protocol':'tcp',
                                          'security_group_id':sgid
                                          }
                   }
        reqrules.append(reqrule)
        reqrule = {'security_group_rule':{
                                          'direction':'ingress', 'ethertype':'IPv4',
                                          'port_range_min':'10443', 'port_range_max':'10443',
                                          'protocol':'tcp',
                                          'security_group_id':sgid
                                          }
                   }
        reqrules.append(reqrule)
        reqrule = {'security_group_rule':{
                                          'direction':'ingress', 'ethertype':'IPv4',
                                          'port_range_min':'22', 'port_range_max':'22',
                                          'protocol':'tcp',
                                          'security_group_id':sgid
                                          }
                   }
        reqrules.append(reqrule)
        reqrule = {'security_group_rule':{
                                          'direction':'ingress', 'ethertype':'IPv4',
                                          'port_range_min':'-1', 'port_range_max':'-1',
                                          'protocol':'icmp',
                                          'security_group_id':sgid
                                          }
                   }
        reqrules.append(reqrule)
        for myrule in reqrules:
            self.sendData(url, 'post', json.dumps(myrule), tokenid=self.TOKEN_ID)



    def listServers(self):
        url = self.COMPUTE_URL + '/servers'
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def getServer(self, instanceid):
        url = self.COMPUTE_URL + '/servers/' + instanceid
        return self.sendData(url, 'get', tokenid=self.TOKEN_ID)

    def createServer(self, servername, keyname, securityGroups, networks, imageRef, flavorRef, availabilityZone):
        url = self.COMPUTE_URL + '/servers'
        server = {}
        server['name'] = servername
        server['key_name'] = keyname
        server['availability_zone'] = availabilityZone
        server["security_groups"] = securityGroups
        server["networks"] = networks
        server["imageRef"] = imageRef
        server["flavorRef"] = flavorRef
        reqbody = {}
        reqbody["server"] = server
        print reqbody
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def deleteServer(self, serverid):
        url = self.COMPUTE_URL + '/servers/' + serverid
        return self.sendData(url, 'delete', tokenid=self.TOKEN_ID)

    def createUTM(self, utmname, networks, availabilityZone):
        '''
        UTM instance 생성 수행.
        주의사항 - networks list 파라미터에 들어가는 network는 순서가 틀리면 접속이 안됨(global mgmt -> red 를 꼭 먼저 넣어야 함)

        '''
        url = self.COMPUTE_URL + '/servers'

        #keypair가 있는지 검사하고 없다면 생성
        keypair = self.getKeypair(self.username + '-keypair')
        if keypair.has_key('keypair') == True:
            print "keypair --> %s" %json.dumps(keypair)
        else :
            keypair = self.createKeypair(self.username + '-keypair')

        print "key_name : %s" %keypair['keypair']['name']

        #SecurityGroup이 있는지 검사하고 없다면 생성
        if self.isSecurityGroupByName(self.UTM['security_groups'][0]['name']) == False:
            mysg = self.createSecurityGroup('Endian Access Group', 'Permits SSH and UTM')
            self.createSecurityGroupRulesForUTM(mysg['security_group']['id'])

        server = self.UTM
        server['name'] = utmname
        server['key_name'] = keypair['keypair']['name']
        server['availability_zone'] = availabilityZone
#         server["networks"] = networks + self.BASIC_NETWORKS
        server["networks"] = self.BASIC_NETWORKS + networks
        reqbody = {}
        reqbody["server"] = server

        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def excuteServer(self, instanceid, action):
        """
        Created on 2015. 2. 14.
        @author: 진수
        @summary: 서버의 중지/재시작
        @param instanceid: 생성된 vm의 uuid
        @param action: vm의 제어명령문자열("pause"/"unpause", "suspend"/"resume")
        @rtype: None
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {action:None}
        print reqbody
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def rebootServer(self, instanceid, reboottype):
        """
        Created on 2015. 2. 14.
        @author: 진수
        @summary: 서버 리부팅
        @param instanceid: 생성된 vm의 uuid
        @param reboottype: vm reboot 종류.("soft")
        @rtype: None
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        reqbody = {'reboot':{'type':reboottype}}
        print reqbody
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)

    def getVNCConsole(self, instanceid):
        """
        Created on 2015. 2. 24.
        @author: 진수
        @summary:
        @param instanceid: 생성된 vm의 uuid
        @rtype: dict
        @return: {
                    "console": {
                        "type": "novnc",
                        "url": "http://127.0.0.1:6080/vnc_auto.html?token=191996c3-7b0f-42f3-95a7-f1839f2da6ed"
                    }
                }
        """
        url = self.COMPUTE_URL + '/servers/' + instanceid + '/action'
        print url
        reqbody = {'os-getVNCConsole':{'type':'novnc'}}
        print json.dumps(reqbody)
        return self.sendData(url, 'post', json.dumps(reqbody), tokenid=self.TOKEN_ID)





if __name__ == '__main__':
    pass