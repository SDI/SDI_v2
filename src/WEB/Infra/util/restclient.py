# -*- coding: utf8 -*-
'''
Created on 2015. 1. 13.

@author: 진수
'''
import json

from tornado import httpclient
from tornado.httpclient import HTTPRequest
from tornado.httputil import HTTPHeaders
from config.monitor_config import ap_url_info
from util.aescipher import AESCipher

class SoapClient():
    def sendData(self, url, httpMethod, reqBody):
        aes = AESCipher('Qfkrkstkghkwhgdk')
        soapUrl = aes.decrypt(ap_url_info['url'])
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        if httpMethod == "post":
            create_url = soapUrl+url
            request = HTTPRequest(create_url, headers=h, method=httpMethod.upper(), body=reqBody, request_timeout=1)
        elif httpMethod == "delete" :
            delete_url=soapUrl+url+reqBody
            request = HTTPRequest(delete_url, headers=h, method=httpMethod.upper(), body=None, request_timeout=3)
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            return response.body
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result

    def test_send_data(self, url, httpMethod, reqBody):
        soapUrl = 'http://10.0.0.71:8888'
#         soapUrl = 'http://211.224.204.173:8888'
        http_client = httpclient.HTTPClient()
        h = HTTPHeaders({"content-type":"application/json;charset=UTF-8", "accept":"application/json"})
        if httpMethod == "post":
            create_url = soapUrl+url
            request = HTTPRequest(create_url, headers=h, method=httpMethod.upper(), body=reqBody)
        elif httpMethod == "delete" :
            delete_url=soapUrl+url+reqBody
            request = HTTPRequest(delete_url, headers=h, method=httpMethod.upper(), body=None, request_timeout=3)
        try:
            response = http_client.fetch(request=request)
            http_client.close()
            return response.body
        except httpclient.HTTPError as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result
        except Exception as e:
            errResponse = {"result":"fail", "description":str(e)}
            result = json.dumps(errResponse)
            return result


if __name__ == '__main__':
    pass