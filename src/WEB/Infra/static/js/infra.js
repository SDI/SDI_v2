function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("infraService",function($scope,$timeout) {
		$scope.orgseq = "0";
		$scope.init = function(){
			$scope.submit = false;
			$scope.modify = false;

			$scope.props = {};
    		$scope.sharedNetworkUuid = '';

    		$scope.openstackConnectTokenUrl = '';
    		$scope.openstackConnectHost = '';
    		$scope.openstackConnectHorizon = '';
    		$scope.openstackConnectPassword = '';
    		$scope.openstackConnectUserid = '';
    		$scope.openstackConnectTenantid = '';

    		$scope.openstackDbConnectDatabase = '';
    		$scope.openstackDbConnectHost = '';
    		$scope.openstackDbConnectPassword = '';
    		$scope.openstackDbConnectUserid = '';
    		$scope.openstackDbConnectPort = '';

    		$scope.hostListName = '';
    		$scope.hostListIp = '';
    		$scope.hostListPort = '';
    		$scope.hostListId = '';
    		$scope.hostListPassword = '';

    		$scope.mgmtvmsName = '';
    		$scope.mgmtvmsEndpoint = '';
    		$scope.mgmtvmsHost = '';
    		$scope.mgmtvmsId = '';
    		$scope.mgmtvmsUserid = '';
    		$scope.mgmtvmsPassword = '';
    		$scope.mgmtvmsAvailabilityZone = '';

    		$scope.nfvProductsName = '';
    		$scope.nfvProductsDefaultconfAvailabilityZone = '';
    		$scope.nfvProductsDefaultconfFlavorRef = '';
    		$scope.nfvProductsDefaultconfImageRef = '';

			$scope.getList();
		};

		$scope.getList = function() {
			$.ajax({
		        type:'post',
		        data : {
		        	orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/infra/infraList',
		        success: function(data) {
		        	$scope.orgList = JSON.parse(data.orgs);
		        	if (data.props.constructor == Object) {
		        		$scope.props = data.props;
		        		$scope.sharedNetworkUuid = $scope.props.shared_networks[0].uuid;

		        		$scope.openstackConnectTokenUrl = $scope.props.openstack_connect.token_url;
		        		$scope.openstackConnectHost = $scope.props.openstack_connect.host;
		        		$scope.openstackConnectHorizon = $scope.props.openstack_connect.horizon;
		        		$scope.openstackConnectPassword = $scope.props.openstack_connect.password;
		        		$scope.openstackConnectUserid = $scope.props.openstack_connect.userid;
		        		$scope.openstackConnectTenantid = $scope.props.openstack_connect.tenantid;

		        		$scope.openstackDbConnectDatabase = $scope.props.openstack_db_connect.database;
		        		$scope.openstackDbConnectHost = $scope.props.openstack_db_connect.host;
		        		$scope.openstackDbConnectPassword = $scope.props.openstack_db_connect.password;
		        		$scope.openstackDbConnectUserid = $scope.props.openstack_db_connect.userid;
		        		$scope.openstackDbConnectPort = $scope.props.openstack_db_connect.port;

		        		$scope.hostListName = $scope.props.host_list[0].name;
		        		$scope.hostListIp = $scope.props.host_list[0].ip;
		        		$scope.hostListPort = $scope.props.host_list[0].port;
		        		$scope.hostListId = $scope.props.host_list[0].id;
		        		$scope.hostListPassword = $scope.props.host_list[0].password;

		        		$scope.mgmtvmsName = $scope.props.mgmtvms[0].name;
		        		$scope.mgmtvmsEndpoint = $scope.props.mgmtvms[0].endpoint;
		        		$scope.mgmtvmsHost = $scope.props.mgmtvms[0].host;
		        		$scope.mgmtvmsId = $scope.props.mgmtvms[0].id;
		        		$scope.mgmtvmsUserid = $scope.props.mgmtvms[0].userid;
		        		$scope.mgmtvmsPassword = $scope.props.mgmtvms[0].password;
		        		$scope.mgmtvmsAvailabilityZone = $scope.props.mgmtvms[0].availability_zone;

		        		$scope.nfvProductsName = $scope.props.nfv_products[0].name;
		        		$scope.nfvProductsDefaultconfAvailabilityZone = $scope.props.nfv_products[0].defaultconf.availability_zone;
		        		$scope.nfvProductsDefaultconfFlavorRef = $scope.props.nfv_products[0].defaultconf.flavorRef;
		        		$scope.nfvProductsDefaultconfImageRef = $scope.props.nfv_products[0].defaultconf.imageRef;

		        		$scope.submit = false;
		    			$scope.modify = true;
		        	} else {
		        		$scope.sharedNetworkUuid = '';

		        		$scope.openstackConnectTokenUrl = '';
		        		$scope.openstackConnectHost = '';
		        		$scope.openstackConnectHorizon = '';
		        		$scope.openstackConnectPassword = '';
		        		$scope.openstackConnectUserid = '';
		        		$scope.openstackConnectTenantid = '';

		        		$scope.openstackDbConnectDatabase = '';
		        		$scope.openstackDbConnectHost = '';
		        		$scope.openstackDbConnectPassword = '';
		        		$scope.openstackDbConnectUserid = '';
		        		$scope.openstackDbConnectPort = '';

		        		$scope.hostListName = '';
		        		$scope.hostListIp = '';
		        		$scope.hostListPort = '';
		        		$scope.hostListId = '';
		        		$scope.hostListPassword = '';

		        		$scope.mgmtvmsName = '';
		        		$scope.mgmtvmsEndpoint = '';
		        		$scope.mgmtvmsHost = '';
		        		$scope.mgmtvmsId = '';
		        		$scope.mgmtvmsUserid = '';
		        		$scope.mgmtvmsPassword = '';
		        		$scope.mgmtvmsAvailabilityZone = '';

		        		$scope.nfvProductsName = '';
		        		$scope.nfvProductsDefaultconfAvailabilityZone = '';
		        		$scope.nfvProductsDefaultconfFlavorRef = '';
		        		$scope.nfvProductsDefaultconfImageRef = '';
		        		$scope.submit = true;
		    			$scope.modify = false;

		    			if ($scope.orgseq == '0') {
		    				$scope.submit = false;
		    			}
		        	}
		        	console.log(data);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.infraSubmit = function() {
			$scope.props = {"openstack_connect":{"token_url":"http://10.0.0.110:5000/v2.0/tokens","userid":"admin","tenantid":"admin","host":"10.0.0.101","horizon":"211.224.204.160","password":"ohhberry3333"},"process_period":{"utmswitchovermonitoring":"*/2","customerstatusmonitoring":"*/30","hwutmstatusmonitoring":"*/30","collectorstatusmonitoring":"*/30","hostcomponentstatusmonitoring":"*/30","utmstatusmonitoring":"*/10","sdnswitchstatusmonitoring":"*/30","cloudserverstatusmonitoring":"*/30"},"host_list":[{"id":"root","ip":"10.0.0.101","password":"ohhberry3333","name":"anode","port":"22"}],"openstack_db_connect":{"host":"10.0.0.72","password":"ohhberry3333","userid":"root","port":"3306","database":"nova"},"mgmtvms":[{"endpoint":"http://10.0.0.201:80","name":"gmgmt_vm","availability_zone":"daejeon-az","userid":"root","host":"10.0.0.201","password":"ohhberry3333","id":"a1763a1b-1070-4148-b25b-97fcd8be1eef"}],"shared_networks":[{"uuid":"xx47abc1-cafb-4234-9801-9daa49bed8a2"}],"switch_list":[{"ip":"221.151.188.9","name":"aggr_sw"},{"ip":"221.151.188.19","name":"tor_sw"}],"nfv_products":[{"name":"utm","defaultconf":{"flavorRef":"3","security_groups":[{"rules":[{"security_group_rule":{"ethertype":"IPv4","direction":"ingress","port_range_min":"80","protocol":"tcp","port_range_max":"80"}},{"security_group_rule":{"ethertype":"IPv4","direction":"ingress","port_range_min":"22","protocol":"tcp","port_range_max":"22"}},{"security_group_rule":{"ethertype":"IPv4","direction":"ingress","port_range_min":"10443","protocol":"tcp","port_range_max":"10443"}},{"security_group_rule":{"ethertype":"IPv4","direction":"ingress","port_range_min":"443","protocol":"tcp","port_range_max":"443"}},{"security_group_rule":{"direction":"ingress","protocol":"icmp","remote_ip_prefix":"0.0.0.0/0","port_range_max":null,"port_range_min":null,"ethertype":"IPv4"}},{"security_group_rule":{"direction":"egress","protocol":"icmp","remote_ip_prefix":"0.0.0.0/0","port_range_max":null,"port_range_min":null,"ethertype":"IPv4"}}],"name":"NfvAccessGroup"}],"imageRef":"7f6f6ebf-1fc6-4b6c-a51b-3ebdb3e10799","availability_zone":"daejeon-az"}},{"name":"ippbx","defaultconf":{"flavorRef":"3","security_groups":[{"rules":[],"name":"IPPBX Access Group"}],"imageRef":"aaaaaaaa-eae2-46cc-9af8-123456789abc","availability_zone":"seacho-cnode1"}}]}
			$scope.props.shared_networks[0].uuid = $scope.sharedNetworkUuid;

    		$scope.props.openstack_connect.token_url = $scope.openstackConnectTokenUrl;
    		$scope.props.openstack_connect.host = $scope.openstackConnectHost;
    		$scope.props.openstack_connect.horizon = $scope.openstackConnectHorizon;
    		$scope.props.openstack_connect.password = $scope.openstackConnectPassword;
    		$scope.props.openstack_connect.userid = $scope.openstackConnectUserid;
    		$scope.props.openstack_connect.tenantid = $scope.openstackConnectTenantid;

    		$scope.props.openstack_db_connect.database = $scope.openstackDbConnectDatabase;
    		$scope.props.openstack_db_connect.host = $scope.openstackDbConnectHost;
    		$scope.props.openstack_db_connect.password = $scope.openstackDbConnectPassword;
    		$scope.props.openstack_db_connect.userid = $scope.openstackDbConnectUserid;
    		$scope.props.openstack_db_connect.port = $scope.openstackDbConnectPort;

    		$scope.props.host_list[0].name = $scope.hostListName;
    		$scope.props.host_list[0].ip = $scope.hostListIp;
    		$scope.props.host_list[0].port = $scope.hostListPort;
    		$scope.props.host_list[0].id = $scope.hostListId;
    		$scope.props.host_list[0].password = $scope.hostListPassword;

    		$scope.props.mgmtvms[0].name = $scope.mgmtvmsName;
    		$scope.props.mgmtvms[0].endpoint = $scope.mgmtvmsEndpoint;
    		$scope.props.mgmtvms[0].host = $scope.mgmtvmsHost;
    		$scope.props.mgmtvms[0].id = $scope.mgmtvmsId;
    		$scope.props.mgmtvms[0].userid = $scope.mgmtvmsUserid;
    		$scope.props.mgmtvms[0].password = $scope.mgmtvmsPassword;
    		$scope.props.mgmtvms[0].availability_zone = $scope.mgmtvmsAvailabilityZone;

    		$scope.props.nfv_products[0].name = $scope.nfvProductsName;
    		$scope.props.nfv_products[0].defaultconf.availability_zone = $scope.nfvProductsDefaultconfAvailabilityZone;
    		$scope.props.nfv_products[0].defaultconf.flavorRef = $scope.nfvProductsDefaultconfFlavorRef;
    		$scope.props.nfv_products[0].defaultconf.imageRef = $scope.nfvProductsDefaultconfImageRef;

			$.ajax({
		        type:'post',
		        data : {
		        	props : JSON.stringify($scope.props),
		        	orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/infra/insertInfra',
		        success: function(data) {
		        	$scope.init();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.infraModify = function() {
			$scope.props.shared_networks[0].uuid = $scope.sharedNetworkUuid;

    		$scope.props.openstack_connect.token_url = $scope.openstackConnectTokenUrl;
    		$scope.props.openstack_connect.host = $scope.openstackConnectHost;
    		$scope.props.openstack_connect.horizon = $scope.openstackConnectHorizon;
    		$scope.props.openstack_connect.password = $scope.openstackConnectPassword;
    		$scope.props.openstack_connect.userid = $scope.openstackConnectUserid;
    		$scope.props.openstack_connect.tenantid = $scope.openstackConnectTenantid;

    		$scope.props.openstack_db_connect.database = $scope.openstackDbConnectDatabase;
    		$scope.props.openstack_db_connect.host = $scope.openstackDbConnectHost;
    		$scope.props.openstack_db_connect.password = $scope.openstackDbConnectPassword;
    		$scope.props.openstack_db_connect.userid = $scope.openstackDbConnectUserid;
    		$scope.props.openstack_db_connect.port = $scope.openstackDbConnectPort;

    		$scope.props.host_list[0].name = $scope.hostListName;
    		$scope.props.host_list[0].ip = $scope.hostListIp;
    		$scope.props.host_list[0].port = $scope.hostListPort;
    		$scope.props.host_list[0].id = $scope.hostListId;
    		$scope.props.host_list[0].password = $scope.hostListPassword;

    		$scope.props.mgmtvms[0].name = $scope.mgmtvmsName;
    		$scope.props.mgmtvms[0].endpoint = $scope.mgmtvmsEndpoint;
    		$scope.props.mgmtvms[0].host = $scope.mgmtvmsHost;
    		$scope.props.mgmtvms[0].id = $scope.mgmtvmsId;
    		$scope.props.mgmtvms[0].userid = $scope.mgmtvmsUserid;
    		$scope.props.mgmtvms[0].password = $scope.mgmtvmsPassword;
    		$scope.props.mgmtvms[0].availability_zone = $scope.mgmtvmsAvailabilityZone;

    		$scope.props.nfv_products[0].name = $scope.nfvProductsName;
    		$scope.props.nfv_products[0].defaultconf.availability_zone = $scope.nfvProductsDefaultconfAvailabilityZone;
    		$scope.props.nfv_products[0].defaultconf.flavorRef = $scope.nfvProductsDefaultconfFlavorRef;
    		$scope.props.nfv_products[0].defaultconf.imageRef = $scope.nfvProductsDefaultconfImageRef;

			$.ajax({
		        type:'post',
		        data : {
		        	props : JSON.stringify($scope.props),
		        	orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/infra/updateInfra',
		        success: function(data) {
		        	$scope.init();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};
	});
	$(document).ready(function() {
	});
