function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("cryptService",function($scope,$timeout) {
		$scope.orgseq = "0";
		$scope.init = function(){
			$scope.encrypt = false;
			$scope.decrypt = false;

			$scope.encryptValue = '';
    		$scope.encryptResult = '';

    		$scope.decryptValue = '';
    		$scope.decryptResult = '';
		};

		$scope.encryptCommit = function() {
			if ($scope.encryptValue != '' && $scope.encryptValue != null) {
				$scope.encrypt = true;
			}
		};

		$scope.decryptCommit = function() {
			if ($scope.decryptValue != '' && $scope.decryptValue != null) {
				$scope.decrypt = true;
			}
		};

		$scope.encryptAccept = function() {
			$.ajax({
		        type:'post',
		        data : {
		        	encrypt : $scope.encryptValue
		        },
		        dataType:'json',
		        url: '/encrypt',
		        success: function(data) {
		        	$scope.encryptResult = data.result;
		        	$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.decryptAccept = function() {
			$.ajax({
		        type:'post',
		        data : {
		        	decrypt : $scope.decryptValue
		        },
		        dataType:'json',
		        url: '/decrypt',
		        success: function(data) {
		        	$scope.decryptResult = data.result;
		        	$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};
	});
	$(document).ready(function() {
	});
