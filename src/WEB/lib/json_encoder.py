# -*- coding: utf-8 -*-

import json

class Object:
	
	def to_JSON(self):
		return json.dumps(self, default=lambda o: o.__dict__, 
			sort_keys=True, indent=4)
