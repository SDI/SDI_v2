# -*- coding: utf-8 -*-

import ConfigParser

class SDI_Config:
	
	def __init__( self, config_file):
		self.conf = ConfigParser.ConfigParser()
		self.conf.read(config_file)
	
	def get(self, section, option):
		value=""
		try: 
			value = self.conf.get(section,option)
		except:
			value = ""
		return value

class SDI_Web_Config(SDI_Config):
	
	def __init__( self, config_file = "config/web.conf"):
		SDI_Config.__init__(self, config_file)

class SDI_WAS_Config(SDI_Config):
	
	def __init__( self, config_file = "config/was.conf"):
		SDI_Config.__init__(self, config_file)
	