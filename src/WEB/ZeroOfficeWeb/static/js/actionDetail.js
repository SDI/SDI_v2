/**
 * 작업이력 상세정보 팝업
 */

var col = ["사용자", "실행시간", "url", "명령", "파라미터"];

function actions() {
	Ext.define('Grid_Action_Model',{ // 데이터 모델
	    extend: 'Ext.data.Model',
	    fields: [ "col", "msg", "idx" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저장소
		model: 'Grid_Action_Model',
		storeId : 'grid_action_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	function error_check(val, meta, rec) { // 줄 바꿈 처리
		return '<pre style="white-space: pre-line;">' + val + '</pre>';
	}

	action_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'action_detail_panel',
		renderTo: Ext.getBody(),
		maxHeight: 690,
	    store: Ext.data.StoreManager.lookup('grid_action_store'),
	    columns: [
	        {text: "", width: 120, dataIndex: 'col', sortable: true, tdCls: 'title-column'},
	        {text: "", width: 280, renderer : error_check, dataIndex: 'msg', sortable: true, tdCls: 'content-column', flex: 1}
	    ],
        listeners: {
			itemdblclick: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            },
        },
        buttons : [{
			text : '닫기',
			handler : function() {self.close();}
		}],
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// 팝업 창 크기 조절에 따른 내부 크기 조절
Ext.EventManager.onWindowResize(function(w, h){
	action_grid_panel.setSize(w, h);
});

// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

Ext.onReady(function() {
	// grid panel 초기화
	actions();

	// 정렬
	var actions_stores = Ext.data.StoreManager.lookup('grid_action_store');
	actions_stores.sort([ { property: 'idx', direction: 'ASC' } ]);

	// 작업이력 상세정보 조회
	Ext.Ajax.request({
		url : '/action/actionDetail',
		type : 'POST',
		dataType : 'json',
		params : {seq : seq, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			action = JSON.parse(data.responseText).result;

			for (var i=1; i<action[0].length; i++) {
				actions_stores.insert(i, {
					"idx" : i,
					"col" : col[i-1],
					"msg" : action[0][i]
				});
			}

			$('#viewLoading').hide(); // 로딩 이미지 숨김
		}
	});
});