		/**
		 * 사용안함
		 */
		/*
		 * VARIABLES
		 * Description: All Global Vars
		 */
		// Impacts the responce rate of some of the responsive elements (lower value affects CPU but improves speed)
		$.throttle_delay = 350;

		// The rate at which the menu expands revealing child elements on click
		$.menu_speed = 235;

		// Note: You will also need to change this variable in the "variable.less" file.
		$.navbar_height = 49;

		/*
		 * APP DOM REFERENCES
		 * Description: Obj DOM reference, please try to avoid changing these
		 */
		$.root_ = $('body');
		$.left_panel = $('#left-panel');
		$.shortcut_dropdown = $('#shortcut');
		$.bread_crumb = $('#ribbon ol.breadcrumb');

	    // desktop or mobile
	    $.device = null;

		/*
		 * APP CONFIGURATION
		 * Description: Enable / disable certain theme features here
		 */
		$.navAsAjax = false; // Your left nav in your app will no longer fire ajax calls

		// Please make sure you have included "jarvis.widget.js" for this below feature to work
		$.enableJarvisWidgets = true;

		// Warning: Enabling mobile widgets could potentially crash your webApp if you have too many
		// 			widgets running at once (must have $.enableJarvisWidgets = true)
		$.enableMobileWidgets = false;


		/*
		 * DETECT MOBILE DEVICES
		 * Description: Detects mobile device - if any of the listed device is detected
		 * a class is inserted to $.root_ and the variable $.device is decleard.
		 */

		/* so far this is covering most hand held devices */
		var ismobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));

		if (!ismobile) {
			// Desktop
			$.root_.addClass("desktop-detected");
			$.device = "desktop";
		} else {
			// Mobile
			$.root_.addClass("mobile-detected");
			$.device = "mobile";

			// Removes the tap delay in idevices
			// dependency: js/plugin/fastclick/fastclick.js
			//FastClick.attach(document.body);
		}
		$(document).ready(function() {
			if ($("[rel=tooltip]").length) {
				$("[rel=tooltip]").tooltip();
			}

			//TODO: was moved from window.load due to IE not firing consist
			nav_page_height()

			// INITIALIZE LEFT NAV
			if (!null) {
				$('nav ul').jarvismenu({
					accordion : true,
					speed : $.menu_speed,
					closedSign : '<em class="fa fa-expand-o"></em>',
					openedSign : '<em class="fa fa-collapse-o"></em>'
				});
			} else {
				alert("Error - menu anchor does not exist");
			}

			// COLLAPSE LEFT NAV
			$('.minifyme').click(function(e) {
				$('body').toggleClass("minified");
				$(this).effect("highlight", {}, 500);
				e.preventDefault();
			});

			// HIDE MENU
			$('#hide-menu >:first-child > a').click(function(e) {
				$('body').toggleClass("hidden-menu");
				e.preventDefault();
			});

			$('#show-shortcut').click(function(e) {
				if ($.shortcut_dropdown.is(":visible")) {
					shortcut_buttons_hide();
				} else {
					shortcut_buttons_show();
				}
				e.preventDefault();
			});

			// SHOW & HIDE MOBILE SEARCH FIELD
			$('#search-mobile').click(function() {
				$.root_.addClass('search-mobile');
			});

			$('#cancel-search-js').click(function() {
				$.root_.removeClass('search-mobile');
			});

			// ACTIVITY
			// ajax drop
			$('#activity').click(function(e) {
				var $this = $(this);

				if ($this.find('.badge').hasClass('bg-color-red')) {
					$this.find('.badge').removeClassPrefix('bg-color-');
					$this.find('.badge').text("0");
					// console.log("Ajax call for activity")
				}

				if (!$this.next('.ajax-dropdown').is(':visible')) {
					$this.next('.ajax-dropdown').fadeIn(150);
					$this.addClass('active');
				} else {
					$this.next('.ajax-dropdown').fadeOut(150);
					$this.removeClass('active')
				}

				var mytest = $this.next('.ajax-dropdown').find('.btn-group > .active > input').attr('id');
				//console.log(mytest)

				e.preventDefault();
			});

			$('input[name="activity"]').change(function() {
				//alert($(this).val())
				var $this = $(this);

				url = $this.attr('id');
				container = $('.ajax-notifications');

				loadURL(url, container);

			});

			$(document).mouseup(function(e) {
				if (!$('.ajax-dropdown').is(e.target)// if the target of the click isn't the container...
				&& $('.ajax-dropdown').has(e.target).length === 0) {
					$('.ajax-dropdown').fadeOut(150);
					$('.ajax-dropdown').prev().removeClass("active")
				}
			});

			$('button[data-loading-text]').on('click', function() {
				var btn = $(this)
				btn.button('loading')
				setTimeout(function() {
					btn.button('reset')
				}, 3000)
			});

			// NOTIFICATION IS PRESENT

			function notification_check() {
				$this = $('#activity > .badge');

				if (parseInt($this.text()) > 0) {
					$this.addClass("bg-color-red bounceIn animated")
				}
			}

			notification_check();

			// RESET WIDGETS
			$('#refresh').click(function(e) {
				$.SmartMessageBox({
					title : "<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",
					content : "Would you like to RESET all your saved widgets and clear LocalStorage?",
					buttons : '[No][Yes]'
				}, function(ButtonPressed) {
					if (ButtonPressed == "Yes" && localStorage) {
						localStorage.clear();
						location.reload();
					}

				});
				e.preventDefault();
			});

			// LOGOUT BUTTON
			$('#logout a').click(function(e) {
				//get the link
				var $this = $(this);
				$.loginURL = $this.attr('href');
				$.logoutMSG = $this.data('logout-msg');

				// ask verification
				$.SmartMessageBox({
					title : "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>" + $('#show-shortcut').text() + "</strong></span> ?",
					content : $.logoutMSG || "You can improve your security further after logging out by closing this opened browser",
					buttons : '[No][Yes]'

				}, function(ButtonPressed) {
					if (ButtonPressed == "Yes") {
						$.root_.addClass('animated fadeOutUp');
						setTimeout(logout, 1000)
					}

				});
				e.preventDefault();
			});

			/*
			 * LOGOUT ACTION
			 */

			function logout() {
				window.location = $.loginURL;
			}

			/*
			* SHORTCUTS
			*/

			// SHORT CUT (buttons that appear when clicked on user name)
			$.shortcut_dropdown.find('a').click(function(e) {

				e.preventDefault();

				window.location = $(this).attr('href');
				setTimeout(shortcut_buttons_hide, 300);

			});

			// SHORTCUT buttons goes away if mouse is clicked outside of the area
			$(document).mouseup(function(e) {
				if (!$.shortcut_dropdown.is(e.target)// if the target of the click isn't the container...
				&& $.shortcut_dropdown.has(e.target).length === 0) {
					shortcut_buttons_hide()
				}
			});

			// SHORTCUT ANIMATE HIDE
			function shortcut_buttons_hide() {
				$.shortcut_dropdown.animate({
					height : "hide"
				}, 300, "easeOutCirc");
				$.root_.removeClass('shortcut-on');

			}

			// SHORTCUT ANIMATE SHOW
			function shortcut_buttons_show() {
				$.shortcut_dropdown.animate({
					height : "show"
				}, 200, "easeOutCirc")
				$.root_.addClass('shortcut-on');
			}
		});
		function nav_page_height() {
			var setHeight = $('#main').height();
			//menuHeight = $.left_panel.height();

			var windowHeight = $(window).height() - $.navbar_height;
			//set height

			if (setHeight > windowHeight) {// if content height exceedes actual window height and menuHeight
				$.left_panel.css('min-height', setHeight + 'px');
				$.root_.css('min-height', setHeight + $.navbar_height + 'px');

			} else {
				$.left_panel.css('min-height', windowHeight + 'px');
				$.root_.css('min-height', windowHeight + 'px');
			}
		}

		$('#main').resize(function() {
			nav_page_height();
			check_if_mobile_width();
		})

		$('nav').resize(function() {
			nav_page_height();
		})

		function check_if_mobile_width() {
			if ($(window).width() < 979) {
				$.root_.addClass('mobile-view-activated')
			} else if ($.root_.hasClass('mobile-view-activated')) {
				$.root_.removeClass('mobile-view-activated');
			}
		}
		$.fn.extend({

			//pass the options variable to the function
			jarvismenu : function(options) {

				var defaults = {
					accordion : 'true',
					speed : 200,
					closedSign : '[+]',
					openedSign : '[-]'
				};

				// Extend our default options with those provided.
				var opts = $.extend(defaults, options);
				//Assign current element to variable, in this case is UL element
				var $this = $(this);

				//add a mark [+] to a multilevel menu
				$this.find("li").each(function() {
					if ($(this).find("ul").size() != 0) {
						//add the multilevel sign next to the link
						$(this).find("a:first").append("<b class='collapse-sign'>" + opts.closedSign + "</b>");

						//avoid jumping to the top of the page when the href is an #
						if ($(this).find("a:first").attr('href') == "#") {
							$(this).find("a:first").click(function() {
								return false;
							});
						}
					}
				});

				//open active level
				$this.find("li.active").each(function() {
					$(this).parents("ul").slideDown(opts.speed);
					$(this).parents("ul").parent("li").find("b:first").html(opts.openedSign);
					$(this).parents("ul").parent("li").addClass("open")
				});

				$this.find("li a").click(function() {

					if ($(this).parent().find("ul").size() != 0) {

						if (opts.accordion) {
							//Do nothing when the list is open
							if (!$(this).parent().find("ul").is(':visible')) {
								parents = $(this).parent().parents("ul");
								visible = $this.find("ul:visible");
								visible.each(function(visibleIndex) {
									var close = true;
									parents.each(function(parentIndex) {
										if (parents[parentIndex] == visible[visibleIndex]) {
											close = false;
											return false;
										}
									});
									if (close) {
										if ($(this).parent().find("ul") != visible[visibleIndex]) {
											$(visible[visibleIndex]).slideUp(opts.speed, function() {
												$(this).parent("li").find("b:first").html(opts.closedSign);
												$(this).parent("li").removeClass("open");
											});

										}
									}
								});
							}
						}// end if
						if ($(this).parent().find("ul:first").is(":visible") && !$(this).parent().find("ul:first").hasClass("active")) {
							$(this).parent().find("ul:first").slideUp(opts.speed, function() {
								$(this).parent("li").removeClass("open");
								$(this).parent("li").find("b:first").delay(opts.speed).html(opts.closedSign);
							});

						} else {
							$(this).parent().find("ul:first").slideDown(opts.speed, function() {
								/*$(this).effect("highlight", {color : '#616161'}, 500); - disabled due to CPU clocking on phones*/
								$(this).parent("li").addClass("open");
								$(this).parent("li").find("b:first").delay(opts.speed).html(opts.openedSign);
							});
						} // end else
					} // end if
				});
			} // end function
		});