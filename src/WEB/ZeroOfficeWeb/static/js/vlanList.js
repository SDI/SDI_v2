function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("vlanService",function($scope,$http,$window,$timeout) {
		$scope.init = function(){
			$scope.userDel = false;
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUserBtn = true;
			$scope.checked = false;
			$scope.searchUserId = "";
			$scope.searchUserName = "";
			$scope.searchOrgName = "";
			$scope.modal = "modal";
			$scope.orgseq = 0;
			$scope.orgname = 0;
			$scope.allocateYn = "0";
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			if($scope.searchVlan ==null || $scope.searchVlan==""){
				$scope.searchVlan ="";
			}

			if($scope.searchUserId ==null || $scope.searchUserId==""){
				searchUserId ="";
			} else {
				searchUserId = "%"+$scope.searchUserId+"%";
			}

			$.ajax({
		        type:'post',
		        data : {
		        	vlan : $scope.searchVlan,
		        	userid : searchUserId,
		        	orgseq : $scope.orgseq,
		        	page : page,
		        	allocateYn : $scope.allocateYn, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/vlan/vlanList',
		        success: function(data) {
		        	$scope.vlanList = JSON.parse(data.vlan)
		        	$scope.orgList = JSON.parse(data.orgs);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];

					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({
							page:i
						});
					}
					$("#user").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addVlan = function(){
			$scope.startVlan = "";
			$scope.endVlan = "";
			$scope.insertOrg = "1";
		};

		$scope.vlanSubmit = function(){
			var re = /^[0-9]+$/;
			var result = true;
	        if(!re.test($scope.startVlan) || !re.test($scope.endVlan)) {
	        	$.SmartMessageBox({
					title : "VLan을 등록 할 수가 없습니다",
					content : "숫자를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
	        }
	        if($scope.insertOrg == ""){
	        	$.SmartMessageBox({
					title : "VLan을 등록 할 수가 없습니다",
					content : "국사를 선택해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
	        	result = false;
	        }
	        if(result){
	        	if(Number($scope.startVlan) > Number($scope.endVlan)){
	        		$.SmartMessageBox({
						title : "VLan을 등록 할 수가 없습니다",
						content : "StartVLan 이 EndVLan 보다 클수가 없습니다.",
						buttons : '[닫기]'
					});
	        	} else {
	        		var vlans = [];
	        		for(var i = Number($scope.startVlan); i <= Number($scope.endVlan); i++){
	        			vlans.push({
	        				vlan : i
	        			})
	        		}
	        		$.ajax({
				        type:'post',
				        data : {
				        	orgseq : $scope.insertOrg, vlan : JSON.stringify(vlans), _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/vlan/vlanCheck',
				        success: function(data) {
				        	if(data.response == "succ"){
				        		$.ajax({
							        type:'post',
							        data : {
							        	orgseq : $scope.insertOrg,
							        	vlan : JSON.stringify(vlans), _xsrf : getCookie("_xsrf")
							        },
							        dataType:'json',
							        url: '/vlan/vlanInsert',
							        success: function(data) {
							        	$('#insertVlan').modal('hide');
							        	$scope.init();
							        },
							        error: function(jqXHR, textStatus, errorThrown) {
							            alert("데이터 실패");
							        }
							    });
				        	} else {
				        		$.SmartMessageBox({
									title : "VLan을 등록 할 수가 없습니다",
									content : "등록 되어 있는 VLan 이 있습니다.",
									buttons : '[닫기]'
								});
				        	}
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
	        	}
	        }
		};

		$scope.allocate_ch = function() {
			if ($scope.modifyAllocate == 'Y') {
				$scope.is_allocate = false;
			} else {
				$scope.is_allocate = true;
			}
		}

		$scope.vlanDetail = function(vlan, userid, orgseq, allocate, type){
			$scope.modifyVlan = vlan;
			if (userid != null) {
				$scope.modifyId = userid;
			} else {
				$scope.modifyId = '';
			}
			$scope.modifyOrg = orgseq;
			$scope.modifyAllocate = allocate;
			if (allocate == 'Y') {
				$scope.is_allocate = false;
			} else if (allocate == 'N') {
				$scope.is_allocate = true;
			}
			if (type != null) {
				$scope.modifyType = type;
			} else {
				$scope.modifyType = '';
			}

			$.ajax({
		        type:'post',
		        data : {
		        	orgseq : $scope.modifyOrg, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/vlan/getUser',
		        success: function(data) {
		        	$scope.userList = JSON.parse(data.data);
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.vlanModify = function(){
			if($scope.modifyAllocate == "Y" && ($scope.modifyId == "" && $scope.modifyId == null)){
				$.SmartMessageBox({
					title : "VLan을 수정 할 수가 없습니다",
					content : "ID를 입력해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
			} else {
				if ($scope.modifyAllocate == "N") {
					$scope.modifyId = '';
					$scope.modifyType = '';
				}
				$.ajax({
			        type:'post',
			        data : {
			        	vlan : $scope.modifyVlan,
			        	userid : $scope.modifyId,
			        	orgseq : $scope.modifyOrg,
			        	allocate : $scope.modifyAllocate,
			        	type : $scope.modifyType,
			        	_xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/vlan/vlanModify',
			        success: function(data) {
			        	$('#detailVlan').modal('hide');
			        	$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			}
		};

		$scope.vlanDelete = function(){
			$.SmartMessageBox({
				title : "VLAN 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.vlanList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								vlan : $("#tdValue"+i).html(),
								orgseq : $("#tddValue"+i).html()
							});
						}
					}
					if($scope.item == ""){
						$.SmartMessageBox({
							title : "VLan을 삭제 할 수 없습니다",
							content : "Vlan을 선택하여 주시기 바랍니다.",
							buttons : '[닫기]'
						});
					} else {
						$.ajax({
					        type:'post',
					        data : {
					        	vlan : JSON.stringify($scope.item), _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/vlan/vlanDelete',
					        success: function(data) {
					        	$.smallBox({
							        title : "삭제 완료",
							        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 2000,
							        icon : "fa fa-bell"
							    });
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertVlan').draggable();
		$('#detailVlan').draggable();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').attr('class','active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
