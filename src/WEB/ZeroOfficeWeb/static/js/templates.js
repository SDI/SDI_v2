/**
 * 사용 안함
 * @param name
 * @returns
 */

function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', ['ngDraggable']);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("templateCtrl",function($scope,$http,$window,$timeout) {
		$scope.init = function(){
			$("#content").css("min-height", "950");
			$.ajax({
		        type:'POST',
		        data: {_xsrf : getCookie("_xsrf")},
		        dataType:'json',
		        url: '/template/getTemplate',
		        success: function(data) {
		        	$scope.templateData = JSON.parse(data.templateList);
		    		$scope.templateName = JSON.parse(data.templateName);
		    		$scope.droppedObjects1 = [];
		    		$scope.droppedObjects2 = [];
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.onDropComplete1 = function(data, evt) {
			var index = $scope.droppedObjects1.indexOf(data);
			if (index == -1)
				$scope.droppedObjects1.push(data);
			if($scope.droppedObjects1.length>4){
				$("#templateWrap").css("top","95px");
			}


		}

		$scope.onDragSuccess1 = function(data, evt) {
			var index = $scope.droppedObjects1.indexOf(data);
			if (index > -1) {
				$scope.droppedObjects1.splice(index, 1);
			}
			if($scope.droppedObjects1.length<5){
				$("#templateWrap").css("top","140px");
			}
		}
		$scope.onDropComplete2 = function(data, evt) {
			var index = $scope.droppedObjects2.indexOf(data);
			if (index == -1) {
				$scope.droppedObjects2.push(data);
			}
		}

		$scope.selectedTemplate = function(){
			if($scope.templateSeq!=""){
				$.ajax({
			        type:'post',
			        data : {
			        	templateSeq : $scope.templateSeq, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/template/templateApp',
			        success: function(data) {
			        	$scope.status = JSON.parse(data.templatesApp).length;
						$scope.droppedObjects1 = JSON.parse(data.templatesApp);
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	alert("템플릿 로딩 실패");
			        }
			    });
			}else{
				$scope.droppedObjects1 = [];
			}
		};

		$scope.addTemplateNm = function(){
			$.ajax({
		        type:'post',
		        data : {
		        	templateName : $scope.newTemplateNm,
		        	username : username, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/template/addNewTemplate',
		        success: function(data) {
		        	$.smallBox({
				        title : "등록 완료",
				        content : "템플릿 등록을 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000,
				        icon : "fa fa-bell"
				    });
					$scope.newTemplateNm="";
					$scope.templateName = data.templateName;
					$scope.init();
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("템플릿 추가 실패");
		        }
		    });
		};

		$scope.setTemplate = function(){

			if($scope.templateSeq == '' || $scope.templateSeq == null){
				$.SmartMessageBox({
					title : "설정 실패",
					content : "설정 할 Template을 선택 하세요.",
					buttons : '[닫기]'
				});
			}else{

				if($scope.status == 0){
					$.ajax({
				        type:'post',
				        data : {
				        	template : JSON.stringify($scope.droppedObjects1),
							templateSeq : $scope.templateSeq, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/template/setTemplate',
				        success: function(data) {
				        	$.smallBox({
						        title : "설정 완료",
						        content : "템플릿 설정을 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
						        color : "#5384AF",
						        timeout: 2000,
						        icon : "fa fa-bell"
						    });
				        	$scope.status = 1;
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				        	alert("템플릿 설정 실패");
				        }
				    });
				}else{
					$.ajax({
				        type:'POST',
				        data : {
				        	template : JSON.stringify($scope.droppedObjects1),
							templateSeq : $scope.templateSeq, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/template/updateTemplate',
				        success: function(data) {
				        	$.smallBox({
						        title : "설정 완료",
						        content : "템플릿 설정을 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
						        color : "#5384AF",
						        timeout: 2000,
						        icon : "fa fa-bell"
						    });
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				        	alert("템플릿 설정 실패");
				        }
				    });
				}

			}
		};
		$scope.deleteBorder = function(){
			$("#templateBody").css("border","2px solid red");
		};
		$scope.defaultBorder = function(){
			$("#templateBody").css("border","none");
			$("#templateBody").css("border-top","1px solid #ddd");
		};
		$scope.defaultBorder2 = function(){
			$("#templateWindow").css("border","none");
			$("#templateWindow").css("border-top","1px solid lightgray");

		};
		$scope.addBorder = function(){
			$("#templateWindow").css("border","2px solid green");
		};

		$scope.delNsTemplate = function(){
			if($scope.templateSeq == '' || $scope.templateSeq == null){
				$.SmartMessageBox({
					title : "삭제 실패",
					content : "삭제 할 Template을 선택 하세요.",
					buttons : '[닫기]'
				});
			}else{
				$.ajax({
			        type:'POST',
			        data : {
			        	templateSeq : $scope.templateSeq, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/template/deleteNsTemplate',
			        success: function(data) {
			        	$.smallBox({
					        title : "삭제 완료",
					        content : "템플릿 삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
						$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	alert("템플릿 삭제 실패");
			        }
			    });
			}
		};
		var inArray = function(array, obj) {
			var index = array.indexOf(obj);
		}
	});

	$(document).ready(function() {
		functionMenu();
		init();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').attr('class','active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
