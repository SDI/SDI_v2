/**
 * 룰 동기화 이상 상세정보 팝업
 */

var col = ["장비명", "장치IP", "고객사", "내용", "상세내용", "CASE TYPE", "분류", "처리"];

function rules() {
	Ext.define('Grid_Rule_Model',{ // 데이터 저장소 기본 틀
	    extend: 'Ext.data.Model',
	    fields: [ "col", "msg", "idx" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저장소
		model: 'Grid_Rule_Model',
		storeId : 'grid_rule_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	// 상세내용 줄 바꿈 처리를 위한 함수
	function error_check(val, meta, rec) {
		if (rec.internalId == 5) {
			return '<pre style="white-space:pre;">' + val + '</pre>';
		}
		return val;
	}

	rule_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'rule_panel',
		renderTo: Ext.getBody(),
		maxHeight: 690,
	    store: Ext.data.StoreManager.lookup('grid_rule_store'),
	    columns: [
	        {text: "", width: 120, dataIndex: 'col', sortable: true, tdCls: 'title-column'},
	        {text: "", width: 280, renderer : error_check, dataIndex: 'msg', sortable: true, tdCls: 'content-column', flex: 1}
	    ],
        listeners: {
			itemdblclick: function(view, rec, node, index, e) {
                e.stopEvent();
                return false;
            },
        },
        buttons : [{
			text : '닫기',
			handler : function() {self.close();}
		}],
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// 팝업 창 크기 조절에 따라 내부 크기 조절
Ext.EventManager.onWindowResize(function(w, h){
	rule_grid_panel.setSize(w, h);
});

// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

Ext.onReady(function() {
	//grid panel 초기화
	rules();

	// 정렬
	var rules_stores = Ext.data.StoreManager.lookup('grid_rule_store');
	rules_stores.sort([ { property: 'idx', direction: 'ASC' } ]);

	// 룰 동기화 이상 상세정보 조회
	Ext.Ajax.request({
		url : '/monitor/ruledetail',
		type : 'POST',
		dataType : 'json',
		params : {seq : seq, reg : reg, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			rule = JSON.parse(data.responseText).result;

			for (var i=0; i<rule[0].length; i++) {
				rules_stores.insert(i, {
					"idx" : i,
					"col" : col[i],
					"msg" : rule[0][i]
				});
			}

			$('#viewLoading').hide();
		}
	});
});