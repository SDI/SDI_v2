/**
 * 웹소켓 처리를 담당하는 스크립트
 */

// singleton
Ext.define("s_Monitor", {
	singleton : true,
	// 변수
	config: {
		org_seq: "",
		vm_name: "",
		active_standby: "",
		serverfarm_total: "",
		serverfarm_error: "",
		orgname: "",
		re: "0",
		mgr : "0",
		st : "",
		et : "",
		_xsrf: "",
	},
	// DB에서 선택한 국사에 대한 상태값 조회
	get_state: function () {
		var sb = Ext.getCmp('basic-statusbar');
		$.ajax({
			url : "/monitor/openStackState",
			type : "POST",
			dataType : "json",
			data : {data : this.config.org_seq,
				_xsrf : getCookie("_xsrf")},
			success : function(req) {
				// 응답 받은 데이터 처리
				server_stores(req.node);//그래픽뷰->NFV-UTM  || 그리드뷰->고객 서비스 상태
				component_stores(req.host);//그리드뷰->인프라 동작 상태(프로세스&& mgmtVM제외)
				mgmt_stores(req.node);//그리드뷰->인프라 동작 상태->서비스 지원 인프라->mgmtVM
				serverCust_stores(req.serverCust);//고객서버팜(현재 사용 안함)
				dev_hw_customer_stores(req.devhwutm);//그래픽뷰->H/W-UTM(장비 정보)
				hw_customer_stores(req.hwutm);//그래픽뷰->H/W-UTM(고객정보)
				process_stores(req.process_state);//그리드뷰->인프라 동작 상태->프로세스

				//실행이력
				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);//국사
				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);//상태
				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);//매니저
				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);//조회일자(시작)
				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);//조회일자(끝)
				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);//조회

				//룰 동기화 이상
				Ext.data.StoreManager.lookup('grid_rule_store').getProxy().setExtraParam('org', s_Monitor.config.org_seq);//국사
				Ext.data.StoreManager.lookup('grid_rule_store').loadPage(1);//조회

				cssSetting();//그래픽뷰->토폴로지 정보 이미지 초기화

				sb.clearStatus({useDefaults:true});//하단 상태바 초기화

				// H/W-UTM과 NFV-UTM의 ACTIVE/STANDBY를 판단
				if (req.active_standby.length > 0) {
					if (req.active_standby[0].length > 0) {
						s_Monitor.config.active_standby = req.active_standby[0][0];
					}
				}

				if (s_Monitor.config.active_standby == '') {//초기화 상태
            		$('#utm_ative').attr('class', 'fa fa-gear');
            		$('#cloud_ative').attr('class', 'fa fa-gear');
            		$('#activeCloud').attr('class','hidden');
            		$('#activeHwutm').attr('class','hidden');
            		$('#nonsel').attr('class','');
            	} else if (s_Monitor.config.active_standby == '1') {// H/W-UTM ACTIVE
            		$('#utm_a').attr('class','hidden');
					$('#utm').attr('class','');
					$('#activeCloud').attr('class','hidden');
            		$('#activeHwutm').attr('class','');
            		$('#nonsel').attr('class','hidden');
					$('#utm_ative').attr('class', 'fa fa-gear fa-spin');
					$('#cloud_ative').attr('class', 'fa fa-gear');
				} else { // NFV-UTM ACTIVE
					$('#utm_a').attr('class','');
					$('#utm').attr('class','hidden');
					$('#activeCloud').attr('class','');
            		$('#activeHwutm').attr('class','hidden');
            		$('#nonsel').attr('class','hidden');
					$('#utm_ative').attr('class', 'fa fa-gear');
					$('#cloud_ative').attr('class', 'fa fa-gear fa-spin');
				}

				// LAN Switch 정보 표시(SDN||Non-SDN)
				if (req.swtichInfo.length>0) {
					$('#switchInfo').html(req.swtichInfo[0][0]);
				} else {
					$('#switchInfo').html('');
				}
			}
		});
	},
	websocket: function () {// 웹소켓
		/**
		 * data.alarm_type : 1 - 장애, 2 - 비장애
		 * data.data_type : I -신규, D - 삭제, C - 변경, N - 변화없음
		 * data.infotype : 1 - 컴포넌트, 2 - 사용안함, 3 - VM, 4 - 서버팜, 5 - H/W-UTM, 6 - NFV-UTM, 7 -사용안함, 8 - 절체/복구, 9 - 프로세스
		 */
		if("WebSocket" in window){// 브라우저의 웹소켓 지원 여부 확인
			ws = new WebSocket("wss://" + socket_url + "/ws"); // 웹소켓 연결
			ws_boolean = true;

			ws.onmessage = function(msg){ // 웹소켓 메시지 수신
				var data = JSON.parse(msg.data); // 수신 받은 데이터를 JSON형으로 형변환

				if (data.orgseq == s_Monitor.config.org_seq) { // 국사 판단

					var collect_data = JSON.parse(data.collect_data); // 수신 받은 데이터의 상세 정보를 JSON형으로 형변환

					if (data.alarm_type == '1' && data.data_type == 'C') { // 새로 발생된 장애에 대한 알림 팝업
						if (data.infotype == 1) {
							var win = window.open("/monitor/error?orgseq="+data.orgseq+"&rt="+data.create_time+"&fm="+admin_node[collect_data.component]+"&infotype="+data.infotype,"","width=490, height=210, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						} else if (data.infotype == 4) {
							var win = window.open("/monitor/error?orgseq="+data.orgseq+"&rt="+data.create_time+"&fm="+collect_data.serverip+"&infotype="+data.infotype,"","width=490, height=210, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						} else if (data.infotype == 5) {
							var win = window.open("/monitor/error?orgseq="+data.orgseq+"&rt="+data.create_time+"&fm="+collect_data.companyname+"&infotype="+data.infotype,"","width=490, height=210, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						} else if (data.infotype == 9) {
							var win = window.open("/monitor/error?orgseq="+data.orgseq+"&rt="+data.create_time+"&fm="+collect_data.process_name+"&infotype="+data.infotype,"","width=490, height=210, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						}

						beepSound(); // 장애 알림음 발생
					}

					if (data.infotype == 8) { // 절체/복구 알림 팝업
						var collect_data = JSON.parse(data.collect_data);

						if (collect_data.source_ip_active == '1') {
							s_Monitor.config.active_standby = '1';
							$('#utm_a').attr('class','hidden');
							$('#utm').attr('class','');
							$('#activeCloud').attr('class','hidden');
		            		$('#activeHwutm').attr('class','');
		            		$('#nonsel').attr('class','hidden');
							$('#utm_ative').attr('class', 'fa fa-gear fa-spin');
							$('#cloud_ative').attr('class', 'fa fa-gear');

							var win = window.open("/monitor/failover_back?active=hwutm&rt="+data.create_time,"failback","width=490, height=180, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						} else {
							s_Monitor.config.active_standby = '0';
							$('#utm_a').attr('class','');
							$('#utm').attr('class','hidden');
							$('#activeCloud').attr('class','');
		            		$('#activeHwutm').attr('class','hidden');
		            		$('#nonsel').attr('class','hidden');
							$('#cloud_ative').attr('class', 'fa fa-gear fa-spin');
							$('#utm_ative').attr('class', 'fa fa-gear');

							var win = window.open("/monitor/failover_back?active=cloud&rt="+data.create_time,"failover","width=490, height=180, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
						}
					}

					if (data.data_type == "I") { // 생성에 대한 DB조회(고객 정보, VLAN 값들 등을 위해 DB 조회), Websocket 송신 후 DB에 작성 되기 때문에 timeout 12초
						setTimeout(function() {
							call_org_state(index_menu, s_Monitor.config.org_seq);
						}, 12000);
					} else if (data.data_type == "D"){ // 삭제에 대한 DB조회, Websocket 송신 후 DB에 작성 되기 때문에 timeout 12초
						setTimeout(function() {
							call_org_state(index_menu, s_Monitor.config.org_seq);
						}, 12000);
					} else if (data.alarm_type == '1') { // 장애 발생에 대한 처리 로직

						var collect_data = JSON.parse(data.collect_data), grid_id = '';

						if (data.infotype == 1 && admin_node[collect_data.component] != null && admin_node[collect_data.component] != '') {//컴포넌트
							grid_id = collect_data.host + '_' + collect_data.component;
							$('#cloud_css_a').attr('class','');
							$('#cloud_css').attr('class','hidden');
							if (collect_data.service.indexOf('keystone') > -1) {
								Ext.data.StoreManager.lookup('grid_keystone_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_keystone_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('nova') > -1) {
								Ext.data.StoreManager.lookup('grid_nava_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_nava_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('neutron') > -1) {
								Ext.data.StoreManager.lookup('grid_neutron_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_neutron_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('glance') > -1) {
								Ext.data.StoreManager.lookup('grid_glance_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_glance_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else {
								Ext.data.StoreManager.lookup('grid_etc_infra_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_etc_infra_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							}

							grid_sorters('component');// 데이터 수정 후 재정렬 및 화면 업데이트

						} else if (data.infotype == 2 && collect_data.port <= 48) { // (구)스위치 상태 현재 사용안함
						} else if (data.infotype == 3) { // VM
							// 그래픽뷰의 이미지에 장애발생 표시
							$('#cloud_css_a').attr('class','');
							$('#cloud_css').attr('class','hidden');

							if (data.user_type == '0') {
								if (Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname) != null && Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname) != '') {
									Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname).data.is_error = data.alarm_type;
									Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname).data.state = collect_data.status;
									grid_sorters('mgmt');// 데이터 수정 후 재정렬 및 화면 업데이트
								}
							} else {
								if (Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname) != null && Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname) != '') {
									if (collect_data.conntrack == 0) {
										console.log(data);
										console.log(collect_data);
									}
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.is_error = data.alarm_type;
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.state = collect_data.status;
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.conntrackcnt = collect_data.conntrack;
									grid_sorters('server');// 데이터 수정 후 재정렬 및 화면 업데이트
								}
							}
						} else if (data.infotype == 4) { // 고객 서버팜(현재 사용안함)
							Ext.data.StoreManager.lookup('grid_serverCust_store').findRecord('server_ip', collect_data.serverip).data.is_error = data.alarm_type;
							grid_sorters('servercust');// 데이터 수정 후 재정렬 및 화면 업데이트

							$('#serverfarm_a').attr('class','');
							$('#serverfarm').attr('class','hidden');
							var total = 0;
							var errors = 0;
							Ext.data.StoreManager.lookup('grid_serverCust_store').each(function(record, index) {
								total++;
								if (record.data.status == 'DEACTIVE') {
									errors++;
								}
							});
							s_Monitor.config.serverfarm_total = total;
							s_Monitor.config.serverfarm_error = errors;
							$('#farmName').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
							$('#farmName_a').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");

						} else if (data.infotype == 5) {// H/W-UTM 상태
							// 장애가 발생한 IP에 대해서 상태값 변경
							Ext.data.StoreManager.lookup('grid_dev_hw_utm_store').each(function(record, index) {
								if (record.data.ip_addr == collect_data.ip_addr) {
									record.data.is_error = data.alarm_type;
								}
							});

							// 장애가 발생한 IP에 대해서 상태값 변경
							Ext.data.StoreManager.lookup('grid_hw_utm_store').each(function(record, index) {
								if (record.data.ip_addr == collect_data.ip_addr) {
									record.data.is_error = data.alarm_type;
								}
							});

							grid_sorters('hw_utm');// 데이터 수정 후 재정렬 및 화면 업데이트
							//그래픽뷰 이미지에 장애발생 표시
							$('#utm_a').attr('class','');
							$('#utm').attr('class','hidden');

						} else if (data.infotype == 6) {// NFV-UTM node 상태
							//그래픽뷰 이미지에 장애발생 표시
							$('#cloud_css_a').attr('class','');
							$('#cloud_css').attr('class','hidden');
							var host = JSON.parse(data.collect_data).host;
							host_array.push({name : host});
							var create_time = JSON.parse(data.collect_data).create_time;
							if(host == 'mnnode'){
								var win = window.open("/monitor/hostError?host="+host+"&create_time="+create_time,"mnnode","width=490, height=160, scrollbars=no," +
								"toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=300, top=300");
							} else if(host == 'cnode01'){
								var win = window.open("/monitor/hostError?host="+host+"&create_time="+create_time,"cnode01","width=490, height=160, scrollbars=no," +
								"toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=330, top=330");
							} else if(host == 'cnode02'){
								var win = window.open("/monitor/hostError?host="+host+"&create_time="+create_time,"cnode02","width=490, height=160, scrollbars=no," +
								"toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes, left=360, top=360");
							}

						} else if (data.infotype == 9) {// 프로세스 상태
							Ext.data.StoreManager.lookup('grid_process_store').findRecord('process_name', collect_data.process_name).data.status = collect_data.process_status.toUpperCase();
						}
					} else if (data.data_type == "C") {// 변경에 대한 처리 로직
						var svg_id = '', collect_data = JSON.parse(data.collect_data), grid_id = '', svg_border_id = '';
						if (data.infotype == 1 && admin_node[collect_data.component] != null && admin_node[collect_data.component] != '') {//컴포넌트
							grid_id = collect_data.host + '_' + collect_data.component;
							if (collect_data.service.indexOf('keystone') > -1) {
								Ext.data.StoreManager.lookup('grid_keystone_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_keystone_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('nova') > -1) {
								Ext.data.StoreManager.lookup('grid_nava_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_nava_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('neutron') > -1) {
								Ext.data.StoreManager.lookup('grid_neutron_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_neutron_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else if (collect_data.service.indexOf('glance') > -1) {
								Ext.data.StoreManager.lookup('grid_glance_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_glance_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							} else {
								Ext.data.StoreManager.lookup('grid_etc_infra_store').findRecord('id', grid_id).data.is_error = data.alarm_type;
								Ext.data.StoreManager.lookup('grid_etc_infra_store').findRecord('id', grid_id).data.pcount = collect_data.pcount;
							}

							grid_sorters('component');

						} else if (data.infotype == 2 && collect_data.port <= 48) {// (구) 스위치
						} else if (data.infotype == 3) {// VM
							var errors = 0;

							if (data.user_type == '0') {
								if (Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname) != null && Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname) != '') {
									Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname).data.is_error = data.alarm_type;
									Ext.data.StoreManager.lookup('grid_mgmt_store').findRecord('vm_name', collect_data.vmname).data.state = collect_data.status;

									grid_sorters('mgmt');
									Ext.data.StoreManager.lookup('grid_mgmt_store').each(function(record, index) {
										if (record.data.is_error == '1') {
											errors++;
										}
									});
								}
							} else {
								if (Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname) != null && Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname) != '') {
									if (collect_data.conntrack == 0) {
										console.log(data);
										console.log(collect_data);
									}
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.is_error = data.alarm_type;
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.state = collect_data.status;
									Ext.data.StoreManager.lookup('grid_server_store').findRecord('vm_name', collect_data.vmname).data.conntrackcnt = collect_data.conntrack;

									grid_sorters('server');

									Ext.data.StoreManager.lookup('grid_server_store').each(function(record, index) {
										if (record.data.is_error == '1') {
											errors++;
										}
									});
								}
							}

							// 현재 장애 수 파악(장애가 존재하면 이미지에 장애표시 없으면 제거)
							if(errors == 0){
								$('#cloud_css_a').attr('class','hidden');
								$('#cloud_css').attr('class','');
							} else {
								$('#cloud_css').attr('class','hidden');
								$('#cloud_css_a').attr('class','');
							}

						} else if (data.infotype == 4) {// 고객 서버팜(현재 사용안함)
							Ext.data.StoreManager.lookup('grid_serverCust_store').findRecord('server_ip', collect_data.serverip).data.is_error = data.alarm_type;

							grid_sorters('servercust');

							var total = 0;
							var errors = 0;
							Ext.data.StoreManager.lookup('grid_serverCust_store').each(function(record, index) {
								total++;
								if (record.data.is_error == '1') {
									errors++;
								}
							});
							s_Monitor.config.serverfarm_total = total;
							s_Monitor.config.serverfarm_error = errors;
							$('#farmName').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
							$('#farmName_a').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");

							if(errors != 0){
								$('#serverfarm_a').attr('class','');
								$('#serverfarm').attr('class','hidden');
							} else {
								$('#serverfarm').attr('class','');
								$('#serverfarm_a').attr('class','hidden');
							}

						} else if (data.infotype == 5) {// H/W-UTM
							Ext.data.StoreManager.lookup('grid_dev_hw_utm_store').each(function(record, index) {
								if (record.data.ip_addr == collect_data.ip_addr) {
									record.data.is_error = data.alarm_type;
								}
							});

							Ext.data.StoreManager.lookup('grid_hw_utm_store').each(function(record, index) {
								if (record.data.ip_addr == collect_data.ip_addr) {
									record.data.is_error = data.alarm_type;
								}
							});

							grid_sorters('hw_utm');

							var total = 0;
							var errors = 0;
							Ext.data.StoreManager.lookup('grid_hw_utm_store').each(function(record, index) {
								total++;
								if (record.data.is_error == '1') {
									errors++;
								}
							});
							if(errors != 0){
								$('#utm_a').attr('class','hidden');
								$('#utm').attr('class','');
							} else {
								$('#utm').attr('class','hidden');
								$('#utm_a').attr('class','');
							}

						} else if (data.infotype == 6) { // NFV-UTM
							grid_sorters('server');
							var host = JSON.parse(data.collect_data).host;
							if(host_array.length == 0){
								$('#cloud_css_a').attr('class','hidden');
								$('#cloud_css').attr('class','');
							} else {
								$('#cloud_css').attr('class','hidden');
								$('#cloud_css_a').attr('class','');
							}
						} else if (data.infotype == 7) {
						} else if (data.infotype == 9) { //프로세스 \
							Ext.data.StoreManager.lookup('grid_process_store').findRecord('process_name', collect_data.process_name).data.status = collect_data.process_status.toUpperCase();
						}
					}
				}
			}
		}else{
			// 브라우저에 웹소켓을 지원 안하는 경우
			alert("sorry");
		}
	}
});