// 해당 국사의 호라이즌 팝업 호출
function openHorizon() {
	// 해당 국사의 호라이즌 주소 조회
	$.ajax({
		url : "/monitor/getHorizon",
		type : "POST",
		dataType : "json",
		data : {seq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
		success : function(req) {
			var win = window.open("http://" + req.ip + "/","Horizon",'width=900, height=800, left=50, top=50, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
		},
		error:function(request,status,error){
			Ext.Msg.alert('안내','horizon에 접속할 수 없습니다.');
			console.log("request : ");
			console.log(request);
			console.log("status : " + status);
			console.log("error : " + error);
		}
	});
}

// 룰 동기화 이상 팝업 호출
function ruleCompareError(ts) {
	var winLog = window.open("/monitor/ruleCompareError?ts="+ts+"&orgs="+s_Monitor.config.org_seq,"",'width=1400, height=590, left=50, top=50,' +
	'scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// NFV-UTM E2E Trace 팝업 호출
function openE2E() {
	var winLog = window.open("/monitor/e2etrace?comm=2&seq="+pseq,"provisionSvg",'width=950, height=590, left=820, top=50,' +
	'scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// 절체/복구 알림 팝업 호출
function openFailover(){
	var win = window.open("/monitor/failover?ipaddr="+ip_addr,"failover",'width=780, height=260, left=520, top=50,' +
							'scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// VM 제어 함수
function serversendcommand(comm) {
	d3.select('#context-menu').style('display', 'none');
	context = null;
	var comm_text = '';
	if (comm == 'rebootserver') {
		comm_text = '재시작 하시겠습니까?';
	} else if (comm == 'resume') {
		comm_text = '재구동 하시겠습니까?';
	} else if (comm == 'suspend') {
		comm_text = '정지 하시겠습니까?';
	}

	if (comm == 'vncconsole') { // VNC 콘솔 접속 ip 조회 및 팝업 호출
		$.ajax({
			url : "/monitor/vmHandle",
			type : "POST",
			dataType : "json",
			data : {data : vmid, comm : 'vncconsole', orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
			success : function(req) {
				console.log(req);
				if (req.result != 'fail') {
					var win = window.open(req.description,vmid,'width=750, height=500, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
				} else {
					Ext.Msg.alert('안내','VNC를 접속할 수 없습니다.');
				}
			},
			error:function(request,status,error){
				Ext.Msg.alert('안내','VNC를 접속할 수 없습니다.');
				console.log("request : ");
				console.log(request);
				console.log("status : " + status);
				console.log("error : " + error);
			}
		});
	} else if (comm == 'hostconsole') { // SSH 접속 ip 조회 및 팝업 호출
		$.ajax({
			url : "/monitor/vmHandle",
			type : "POST",
			dataType : "json",
			data : {data : vmid, comm : 'hostconsole', orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
			success : function(req) {
				if (req.result != 'fail') {
					var win = window.open("http://" + ssh_url + "?ip="+req.description+"&title="+req.customer,"host",'width=451, height=384, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
				} else {
					Ext.Msg.alert('안내','HOST에 접속할 수 없습니다.');
				}
			},
			error:function(request,status,error){
				Ext.Msg.alert('안내','HOST에 접속할 수 없습니다.');
				console.log("request : ");
				console.log(request);
				console.log("status : " + status);
				console.log("error : " + error);
			}
		});
	} else if (comm == 'endian') { // web접속 ip 조회 및 팝업 호출
		$.ajax({
			url : "/monitor/vmHandle",
			type : "POST",
			dataType : "json",
			data : {data : vmid, comm : 'endian', orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
			success : function(req) {
				var win = window.open(req.description,"web",'width=1000, height=750, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
			},
			error:function(request,status,error){
				console.log("request : ");
				console.log(request);
				console.log("status : " + status);
				console.log("error : " + error);
			}
		});
	} else { // VM 제어(재구동/재시작/정지)
		var cls = '';

		if (comm == '') {
			cls = 'test';
		} else {
			cls = 'test2';
		}

		Ext.Msg.show({
		    title:'확인',
		    message: comm_text,
		    buttons: Ext.Msg.YESNO,
		    icon: Ext.Msg.QUESTION,
		    cls: cls,
		    fn: function(btn) {
		        if (btn === 'yes') {
		        	$.ajax({
						url : "/monitor/vmHandle",
						type : "POST",
						dataType : "json",
						data : {data : vmid, comm : comm, orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
						success : function(req) {
							console.log(req);
						},
						error:function(request,status,error){
							console.log("request : ");
							console.log(request);
							console.log("status : " + status);
							console.log("error : " + error);
						}
					});
		        }
		    }
		});
	}
}

// mgmtVM 제어
function mgmtcommand(comm) {
	d3.select('#context-menu').style('display', 'none');
	context = null;
	var comm_text = '';
	if (comm == 'rebootserver') {
		comm_text = '재시작 하시겠습니까?';
	} else if (comm == 'resume') {
		comm_text = '재구동 하시겠습니까?';
	} else if (comm == 'suspend') {
		comm_text = '정지 하시겠습니까?';
	}

	if (userauth == '99'){ // 슈퍼유저인지 권한 확인
		Ext.Msg.show({
		    title: '확인',
		    message: '시스템에 치명적인 영향을 끼칠수 있습니다.<br>비밀번호를 입력해주세요:<br><input type="password" id="superpass2" style="width: 100%"/>',
		    width: 300,
		    buttons: Ext.Msg.YESNO,
		    /*prompt: true,*/
		    cls: 'x-message-box-alert-color',
		    fn: function(btn) {
		    	if (btn === 'yes') {
		    		if (comm == 'vncconsole') { // VNC 접속
		    			$.ajax({
		    				url : "/monitor/mgmtHandle",
		    				type : "POST",
		    				dataType : "json",
		    				data : {data : vmid, pwds : $('#superpass2').val(), comm : 'vncconsole', orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
		    				success : function(req) {
		    					console.log(req);
		    					if (req.is_fail == 's') {
		    						var description = JSON.parse(req.descript);
		    						if (description.result == 'fail') {
		    							Ext.example.msg('실패', '연결에 실패하였습니다.');
		    						} else {
		    							var win = window.open(description.description,vmid,'width=750, height=500, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
		    						}
		    					} else {
		    						Ext.example.msg('실패', req.descript);
		    						Ext.Msg.alert('안내','VNC를 접속할 수 없습니다.');
		    					}
		    				},
		    				error:function(request,status,error){
		    					Ext.Msg.alert('안내','VNC를 접속할 수 없습니다.');
		    					console.log("request : ");
		    					console.log(request);
		    					console.log("status : " + status);
		    					console.log("error : " + error);
		    				}
		    			});
		    		} else { // VM 제어(재구동/재시작/정지)
		    			var cls = '';

		    			if (comm == '') {
		    				cls = 'test';
		    			} else {
		    				cls = 'test2';
		    			}

		    			Ext.Msg.show({
		    			    title:'확인',
		    			    message: comm_text,
		    			    buttons: Ext.Msg.YESNO,
		    			    icon: Ext.Msg.QUESTION,
		    			    cls: cls,
		    			    fn: function(btn) {
		    			        if (btn === 'yes') {
		    			        	$.ajax({
		    							url : "/monitor/mgmtHandle",
		    							type : "POST",
		    							dataType : "json",
		    							data : {data : vmid, pwds : text, comm : comm, orgseq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
		    							success : function(req) {
		    								if (req.is_fail == 's') {
		    									Ext.example.msg('성공', '제어 명령을 내렸습니다.');
		    								} else if (req.is_fail == 'f') {
		    									Ext.example.msg('실패', req.descript);
		    								}
		    							},
		    							error:function(request,status,error){
		    								console.log("request : ");
		    								console.log(request);
		    								console.log("status : " + status);
		    								console.log("error : " + error);
		    							}
		    						});
		    			        }
		    			    }
		    			});
		    		}
		    	}
		    }
		});
	} else {
		var comm_text = '수퍼 유저 권한입니다.<br>연라처 : 인프라연구소 PM(옥기상)';

		Ext.Msg.show({
		    title:'확인',
		    message: comm_text,
		    buttons: Ext.Msg.CANCEL,
		    cls: 'x-message-box-alert-color',
		    fn: function(btn, text) {
		    	console.log(text);
		    }
		});
	}
}

// 컴포넌트 제어
function hostsendcommand(comm) {
	d3.select('#context-menu').style('display', 'none');
	context = null;

	var comm_text = '수퍼 유저 권한입니다.<br>연라처 : 인프라연구소 PM(옥기상)';

	if (userauth == '99'){ // 권한 확인
		Ext.Msg.show({
		    title: '확인',
		    message: '시스템에 치명적인 영향을 끼칠수 있습니다.<br>비밀번호를 입력해주세요:<br><input type="password" id="superpass" style="width: 100%"/>',
		    width: 300,
		    buttons: Ext.Msg.YESNO,
		    /*prompt: true,*/
		    cls: 'x-message-box-alert-color',
		    fn: function(btn) {
		    	if (btn === 'yes') { // 컴포넌트 제어(시작/재시작/정지)
		    		$.ajax({
						url : "/hostsendcommand",
						type : "POST",
						dataType : "json",
						data : {
							node : node,
							service : service,
							cmdtype: comm,
							orgseq: s_Monitor.config.org_seq,
							pwd : $('#superpass').val(),
							_xsrf : getCookie("_xsrf")
						},
						success : function(req) {
							if (req.result == 'fail') {
								Ext.example.msg('실패', req.description);
							}
							console.log(req);
						},
						error:function(request,status,error){
							console.log("request : ");
							console.log(request);
							console.log("status : " + status);
							console.log("error : " + error);
						}
					});
		    	}
		    }
		});
	} else {
		Ext.Msg.show({
		    title:'확인',
		    message: comm_text,
		    buttons: Ext.Msg.CANCEL,
		    cls: 'x-message-box-alert-color',
		    fn: function(btn, text) {
		    	console.log(text);
		    }
		});
	}
}