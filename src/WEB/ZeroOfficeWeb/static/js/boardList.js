		CKEDITOR.replace("boardContent");

		$('#insertBoard').draggable();

		function getCookie(name) {
		    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
		    return r ? r[1] : undefined;
		}

		var module = angular.module('ngApp', ['ngSanitize']);

		module.config(function($interpolateProvider) {
		    $interpolateProvider.startSymbol('{[{');
		    $interpolateProvider.endSymbol('}]}');
		  });

		module.controller("BoardService",function($scope,$http,$timeout,$sce) {
				$scope.path = "";
				$scope.number = "";
				$scope.username = username;
				$scope.init = function() {
					if(username != ''){
						$scope.userid = username;
						$scope.userTd = true;
						$scope.userTd_in = false;
					} else {
						$scope.userid = "";
						$scope.userTd = false;
						$scope.userTd_in = true;
					}
					$scope.searchUserId = "";
					$scope.searchTitle = "";
					$scope.searchContent = "";
					$scope.boardPass = "";
					$scope.modalset = "";
					$scope.modalDel = "";
					$scope.modalSubmit = "";
					$scope.modalTitle = "등 록";
					$scope.tempFilepath = "";

					$scope.userDel = false;
					$scope.modify = false;
					$scope.submit = false;
					$scope.boardSubmitBtn = true;
					$scope.boardModifyBtn = false;
					$scope.modifySubmitBtn = false;
					$scope.boardDelBtn = false;
					$scope.titleInput = true;
					$scope.boardUpload = true;
					$scope.fileTr = false;

					$scope.modal = "modal";
					$scope.paging(1);
				};

				$scope.paging = function(page) {

					if($scope.searchUserId ==null || $scope.searchUserId==""){
						searchUserId ="";
					} else {
						searchUserId = "%"+$scope.searchUserId+"%";
					}

					if($scope.searchTitle ==null || $scope.searchTitle==""){
						searchTitle ="";
					} else {
						searchTitle = "%"+$scope.searchTitle+"%";
					}

					if($scope.searchContent ==null || $scope.searchContent==""){
						searchContent ="";
					} else {
						searchContent = "%"+$scope.searchContent+"%";
					}

					$.ajax({
				        type:'post',
				        data : {
				        	userid : searchUserId,
							title : searchTitle,
							content : searchContent,
							page : page, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/board/boardList',
				        success: function(data) {
				        	$scope.boardList = JSON.parse(data.boards);
				        	console.log(JSON.stringify($scope.boardList));
							$scope.boardPage = data.pageNum;
							$scope.path = $scope.boardList[0].filepath;// 업로드되는 폴더경로 가져오기
							$scope.start = data.startPage;
							$scope.end = data.endPage;
							$scope.pages = [];

							for(var i=$scope.start;i<=$scope.end;i++){
								$scope.pages.push({
									page:i
								});
							}
							$timeout(function(){
								$("#current"+page).addClass("active");
							},100);
							$("#order").css("visibility","visible");
				    		$scope.$apply();
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
				};
				// 등록 버튼 클릭
				$scope.addBoard = function(){
					$scope.boardSubmitBtn = true;
					$scope.boardModifyBtn = false;
					$scope.modifySubmitBtn = false;
					$scope.boardDelBtn = false;
					$scope.passInput = true;
					$scope.fileTr = false;

					$scope.titleInput = true;
					$scope.titleNoinput = false;

					$scope.detailDiv = false;
					$scope.ckeditor = true;

					$scope.boardTitle = "";
					$scope.boardPass = "";
					$scope.modalTitle = "등 록";

					$("#path1").val("");
					$scope.reset();

					$scope.boardUpload = true;
					CKEDITOR.instances.boardContent.setData("");
					$scope.$apply();
				}
				// 글 등록
				$scope.boardSubmit = function(){
					var boardContent = CKEDITOR.instances.boardContent.getData();
					$scope.tempfile = $("#file1").val();
					if($scope.boardPass != "" && $scope.boardTitle != "" && boardContent != ""){ // 제목, 비밀번호, 내용 체크
						if($scope.tempfile != ""){
							$scope.fileSize(); // 파일 사이즈 체크
						} else {
							$scope.sizeinbytes = 0;
						}
						if($scope.sizeinbytes < 20971520){ // 20MB 가 넘는지 체크
							$scope.modalSubmit = "modal";
							if($scope.sizeinbytes != 0){
								$scope.fileupload(boardContent);
							} else {
								$scope.filename = "";
								$scope.path = "";
								$scope.setBoard(boardContent);
							}
						} else {
							$scope.modalSubmit = "";
							$.SmartMessageBox({
								title : "게시글을 등록할 수 없습니다.",
								content : "첨부 파일이 20MB 이상입니다. 현재 파일 크기가 "+$scope.fSize+" 입니다.",
								buttons : '[닫기]'
							});
							$scope.boardPass = "";
						}
					} else {
						$scope.modalSubmit = "";
						$.SmartMessageBox({
							title : "게시글을 등록할 수 없습니다.",
							content : "비밀번호 & 제목 & 내용을 입력해 주십시오",
							buttons : '[닫기]'
						});
						$scope.boardPass = "";
					};
				};

				$scope.setBoard = function(boardContent){
					$.ajax({
				        type:'post',
				        data : {
				        	title : $scope.boardTitle,
							userid : $scope.userid,
							content : boardContent,
							boardpass : $scope.boardPass,
							filename : $scope.filename,
							path : $scope.path, _xsrf : getCookie("_xsrf")
				        },
				        async : true,
				        dataType:'json',
				        url: '/board/boardSubmit',
				        success: function(data) {
				        	$scope.init();
				    		$scope.$apply();
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
				}
				// 등록 취소 버튼 클릭
				$scope.modalClose = function(){
					$scope.title = "";
					$scope.boardPass = "";
					$scope.boardUpload = true;
					CKEDITOR.instances.boardContent.setData("");
					$scope.fileTr = false;
				};
				// 상세 보기
				$scope.boardDetail = function(boardseq){
					$scope.fileTr = true;
					$scope.modalTitle = "상 세 보 기";
					$scope.boardPass = "";
					$scope.boardUpload = false;
					$.ajax({
				        type:'post',
				        data : {
				        	boardseq : boardseq, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        async : false,
				        url: '/board/boardDetail',
				        success: function(data) {
				        	//console.log(JSON.stringify(data.boards));
				        	$scope.titleInput = false;
							$scope.titleNoinput = true;
							$scope.boardSubmitBtn = false;
							$scope.modifySubmitBtn = false;
							$scope.detailDiv = true;
							$scope.ckeditor = false;
							$scope.boards = JSON.parse(data.boards);
							$scope.password = $scope.boards[0].boardpass;
							$scope.boardseq = $scope.boards[0].boardseq;
							$scope.userid = $scope.boards[0].userid;
							$scope.name = ""
							if($scope.boards[0].filename){
								$scope.path = $scope.boards[0].filepath;
								$scope.filename = $scope.boards[0].filename;
								$scope.name = $scope.boards[0].filename.substring(2,$scope.filename.length)
							}
							if($scope.username == $scope.boards[0].userid){
								$scope.boardModifyBtn = true;
								$scope.passInput = true;
								$scope.boardDelBtn = true;
							} else if(userauth == "0"){
								$scope.boardModifyBtn = false;
								$scope.passInput = false;
								$scope.boardDelBtn = true;
							} else {
								$scope.boardModifyBtn = false;
								$scope.passInput = false;
								$scope.boardDelBtn = false;
							}
							$scope.boardTitle = $scope.boards[0].title;
							$scope.boardContent = $sce.trustAsHtml($scope.boards[0].content);
							$scope.$apply();
							CKEDITOR.instances.boardContent.setData($scope.boards[0].content);
							//$('#layoutFile').remove();
							$scope.reset();

				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("데이터 실패");
				        }
				    });
				};

				// 삭제 버튼
				$scope.boardDel = function(){
					$.SmartMessageBox({
						title : "게시글 삭제 안내 문구 입니다.",
						content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
						buttons : '[취소][확인]'
					},function(ButtonPressed){
						if($scope.boardPass == $scope.password){
							$scope.modalDel = "modal";
							$('#insertBoard').modal('hide');
							$scope.deleteFile();
							$.ajax({
						        type:'post',
						        data : {
						        	boardseq : $scope.boardseq, _xsrf : getCookie("_xsrf")
						        },
						        dataType:'json',
						        async : false,
						        url: '/board/boardDelete',
						        success: function(data) {
						        	$scope.init();
						        },
						        error: function(jqXHR, textStatus, errorThrown) {
						            alert("데이터 실패");
						        }
						    });
						} else {
							$.SmartMessageBox({
								title : "삭제를 할 수가 없습니다",
								content : "비밀번호를 다시 입력해 주십시오",
								buttons : '[닫기]'
							});
							$scope.boardPass = "";
						};
					});
				};
				// 수정 버튼 클릭
				$scope.boardModify = function(){
					if($scope.boardPass == $scope.password){
						$scope.titleInput = true;
						$scope.titleNoinput = false;
						$scope.fileTr = false;
						$scope.boardSubmitBtn = false;
						$scope.boardModifyBtn = false;
						$scope.modifySubmitBtn = true;
						$scope.boardDelBtn = false;

						$scope.detailDiv = false;
						$scope.ckeditor = true;
						$scope.passInput = true;

						$scope.modalTitle = "수 정";
						$scope.boardUpload = true;
						$scope.boardPass = "";

						$("#path1").val($scope.name);

					} else {
						$.SmartMessageBox({
							title : "수정을 할 수가 없습니다",
							content : "비밀번호를 다시 입력해 주십시오",
							buttons : '[닫기]'
						});
						$scope.boardPass = "";
					};

				};
				// 수정 확인 버튼 클릭
				$scope.modifySubmit = function(){
					$scope.modalset = "modal";
					var boardContent = CKEDITOR.instances.boardContent.getData();
					$scope.tempFilepath = $("#file1").val();
					if($scope.boardPass != "" && $scope.boardTitle != "" && boardContent != ""){ // 제목, 비밀번호, 내용 체크
						if($scope.tempFilepath != ""){
							$scope.fileSize(); // 파일 사이즈 체크
						} else {
							$scope.sizeinbytes = 0;
						}
						if($scope.sizeinbytes < 20971520){ // 20MB 가 넘는지 체크
							$scope.modalset = "modal";
							if($scope.tempFilepath != ""){
								$scope.deleteFile();
								$scope.fileupload();
								$scope.path = $scope.path+$scope.number;
							}
							$timeout(function(){
								$.ajax({
							        type:'post',
							        data : {
							        	boardseq : $scope.boardseq,
										title : $scope.boardTitle,
										userid : $scope.username,
										content : boardContent,
										boardpass : $scope.boardPass,
										path : $scope.path,
										filename :$scope.filename, _xsrf : getCookie("_xsrf")
							        },
							        dataType:'json',
							        url: '/board/boardModify',
							        success: function(data) {
							        	$scope.init();
							        	$scope.$apply();
							        }, error: function(jqXHR, textStatus, errorThrown) {
							            alert("provision 실패");
							        }
								});
							},500);
						} else {
							$scope.modalset = "";
							$.SmartMessageBox({
								title : "게시글을 수정할 수 없습니다.",
								content : "첨부 파일이 20MB 이상입니다. 현재 파일 크기가 "+$scope.fSize+" 입니다.",
								buttons : '[닫기]'
							});
							$scope.boardPass = "";
						}
					} else {
						$scope.modalset = "";
						$.SmartMessageBox({
							title : "게시글을 수정할 수 없습니다.",
							content : "비밀번호 & 제목 & 내용을 입력해 주십시오",
							buttons : '[닫기]'
						});
						$scope.boardPass = "";
					}
				};
				// 파일 업로드
				$scope.fileupload = function(boardContent){
					if($scope.filepath !=""){
						var form = $('form')[0];
				        var formData = new FormData(form);
			            $.ajax({
			                url: '/board/boardFileUpload',
			                processData: false,
			                contentType: false,
			                data: formData,
			                type: 'POST',
			                success: function(result){
			                	if (result.result == 'f') {
			                		alert('형식에 맞지 않는 파일입니다.');
			                	}
			                	$scope.filename = result.file;
			                	//alert($scope.filename);
			                	$scope.path = result.path;
			                	$scope.$apply();
			                	$scope.setBoard(boardContent);

			                }
			            });
					};
				};
				// 파일 사이즈 체크
				$scope.fileSize = function(){
					if(window.ActiveXObject){
				        var fso = new ActiveXObject("Scripting.FileSystemObject");
				        var filepath = document.getElementById('file1').value;
				        var thefile = fso.getFile(filepath);
				        $scope.sizeinbytes = thefile.size;
				    }else{
				    	$scope.sizeinbytes = document.getElementById('file1').files[0].size;
				    }
				};
				// 서버에 올려져 있는 파일 삭제
				$scope.deleteFile = function(){
					var path = $scope.path;
					var filepath = $scope.filename;
					$.ajax({
				        type:'post',
				        data : {
				        	path : path+"/"+filepath, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/board/boardFileDelete',
				        success: function(data) {
				        	console.log("파일삭제완료!!!");
				        }, error: function(jqXHR, textStatus, errorThrown) {
				        }
					});
				}

				$scope.reset = function(){
					$("#file1").replaceWith( $("#file1").clone(true) );
				}
			});

		module.filter('unsafe', function() {
			return function(text) {
				text = text.replace(/<br>/ig, "\n"); // <br>을 엔터로 변경
				text = text.replace(/&nbsp;/ig, " "); // 공백
				// HTML 태그제거
				text = text.replace(/<(\/)?([a-zA-Z]*)(\s[a-zA-Z]*=[^>]*)?(\s)*(\/)?>/ig, "");

				// shkim.add.
				text = text.replace(/<(no)?script[^>]*>.*?<\/(no)?script>/ig, "");
				text = text.replace(/<style[^>]*>.*<\/style>/ig, "");
				text = text.replace(/<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>/ig, "");
				text = text.replace(/<\\w+\\s+[^<]*\\s*>/ig, "");
				text = text.replace(/&[^;]+;/ig, "");
				text = text.replace(/\\s\\s+/ig, "");

				var l = 0;
				var len = 32;
				for (var i=0; i<text.length; i++) {
					l += (text.charCodeAt(i) > 128) ? 2 : 1;
					if(l > len) {
						return text.substring(0,i) + "...";
					}
				}

				return text;
			};
		});