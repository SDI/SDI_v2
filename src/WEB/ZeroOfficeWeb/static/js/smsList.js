// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	// AngularJS 사용
	var module = angular.module('ngApp', []);

	// AngularJS의 변수 사용을 위한 표현정의 => {[{변수명}]}
	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	// AngularJS 컨트롤러
	module.controller("smsService",function($scope,$timeout) {
		// 변수 초기화
		$scope.init = function(){
			$scope.modify = false;
			$scope.submit = false;
			$scope.addSmsBtn = true;
			$scope.checked = false;
			$scope.insertSmsHp = '';
			$scope.insertSmsName = '';
			$scope.searchUserName = '';
			$scope.modal = "modal";
			$scope.smsseq = '';
			$("input:checkbox[id='type0']").prop("checked", false);
			$("input:checkbox[id='type1']").prop("checked", false);
			$("input:checkbox[id='type2']").prop("checked", false);
			$scope.paging(1);
		};

		// SMS수신자 리스트 조회
		$scope.paging = function(page) {
			$.ajax({
		        type:'post',
		        data : {
					page : page, user : $scope.searchUserName, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/sms/smsList',
		        success: function(data) {
		        	$scope.smsList = JSON.parse(data.sms);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.orgList = JSON.parse(data.orgs);
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		// SMS수신자 등록을 위한 초기화
		$scope.addSms = function(){
			$scope.insertSmsHp = '';
			$scope.insertSmsName = '';
			$scope.insertSmsDep = '';
			$scope.insertOrgseq = '0';
			$("input:checkbox[id='type0']").prop("checked", false);
			$("input:checkbox[id='type1']").prop("checked", false);
			$("input:checkbox[id='type2']").prop("checked", false);
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
		};

		// SMS수신자 등록
		$scope.smsSubmit = function(){
			//핸드폰번호 정규식
			var hp_check=/^01([0|1|3|6|7|8|9]?)?([0-9]{7,8})$/;

			if	(!hp_check.test($scope.insertSmsHp)) { //핸드폰번호 형식 체크
				$.SmartMessageBox({
					title : "잘못된 핸드폰 번호입니다.",
					buttons : '[닫기]'
				});
			} else if ($scope.insertSmsName == null || $scope.insertSmsName == '') { //수신자명 입력 체크
				$.SmartMessageBox({
					title : "사용자를 입력 해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
			} else {
				var cnt = 0;
				var types = '';
				// 체크박스 선택 값 확인
				if ($("input:checkbox[id='type0']").is(":checked")) {
					cnt++;
					types = 'UTM';
				}
				if ($("input:checkbox[id='type1']").is(":checked")) {
					if (cnt == 0){
						types = 'OPENSTACK';
					} else {
						types = types + '|OPENSTACK';
					}
					cnt++;
				}
				if ($("input:checkbox[id='type2']").is(":checked")) {
					if (cnt == 0){
						types = 'MGMT_VM';
					} else {
						types = types + '|MGMT_VM';
					}
				}

				// 입력된 데이터 전송
				$.ajax({
			        type:'post',
			        data : {
			        	user : $scope.insertSmsName,
			        	dep : $scope.insertSmsDep,
						hp : $scope.insertSmsHp,
						orgs : $scope.insertOrgseq,
						types : types,
						_xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/sms/insertSms',
			        success: function(data) {
			        	$.smallBox({
					        title : "등록 완료",
					        content : "SMS사용자 등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000
					    });
						$('#insertSms').modal('hide');
						$scope.init();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			}
		};

		// SMS수신자 상세정보 조회
		$scope.SmsDetail = function(seq, user, hp, member, orgseq, alarmtype){
			$scope.smsseq = seq;
			$scope.insertSmsName = user;
			$scope.insertSmsHp = hp;
			$scope.insertSmsDep = member;
			$scope.insertOrgseq = orgseq;
			$("input:checkbox[id='type0']").prop("checked", false);
			$("input:checkbox[id='type1']").prop("checked", false);
			$("input:checkbox[id='type2']").prop("checked", false);
			if (alarmtype != null && alarmtype != '') {
				var types = alarmtype.split('|');
				for (var i=0; i<types.length; i++) {
					if (types[i] == 'UTM') {
						$("input:checkbox[id='type0']").prop("checked", true);
					}
					if (types[i] == 'OPENSTACK') {
						$("input:checkbox[id='type1']").prop("checked", true);
					}
					if (types[i] == 'MGMT_VM') {
						$("input:checkbox[id='type2']").prop("checked", true);
					}
				}
			}
			$scope.modalTitle = "수정";
			$scope.modify = true;
			$scope.submit = false;
		};

		// SMS수신자 정보 수정
		$scope.smsModify = function(){
			var cnt = 0;
			var types = '';
			if ($("input:checkbox[id='type0']").is(":checked")) {
				cnt++;
				types = 'UTM';
			}
			if ($("input:checkbox[id='type1']").is(":checked")) {
				if (cnt == 0){
					types = 'OPENSTACK';
				} else {
					types = types + '|OPENSTACK';
				}
				cnt++;
			}
			if ($("input:checkbox[id='type2']").is(":checked")) {
				if (cnt == 0){
					types = 'MGMT_VM';
				} else {
					types = types + '|MGMT_VM';
				}
			}

			// SMS수신자 정보 전송
			$.ajax({
		        type:'post',
		        data : {
		        	smsseq : $scope.smsseq,
		        	user : $scope.insertSmsName,
		        	dep : $scope.insertSmsDep,
					hp : $scope.insertSmsHp,
					orgs : $scope.insertOrgseq,
					types : types,
					_xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/sms/updateSms',
		        success: function(data) {
		        	$('#insertSms').modal('hide');
					$.smallBox({
				        title : "수정 완료",
				        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000
				    });
					$scope.init();
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("수정 실패");
		        }
		    });
			$scope.init();
    		$scope.$apply();
		};

		$scope.smsClose = function(){
		};

		// SMS수신자 삭제
		$scope.smsDelete = function(){
			$.SmartMessageBox({ // SMS수신자 삭제 확인 메시지
				title : "SMS사용자 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.smsList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){ // 체크박스로 선택된 수신자 삭제
							$scope.item.push({
								smsseq : $("#tdValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	smsseq : $scope.item[j].smsseq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/sms/deleteSms',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu(); // SMS수신자 관리 메뉴를 빨간색으로 강조
		init(); // 좌측 메뉴 트리에서 사용자 관리 메뉴 활성화
		$('#insertSms').draggable();
	});

	function init() {
		$('#lp_user_template').attr('style', 'display:block;');
		$('#wid-sms-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_user_sms').attr('class','active');
		$("#main").css("visibility","visible");
	}
