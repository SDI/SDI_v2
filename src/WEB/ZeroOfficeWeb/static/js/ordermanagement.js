function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

	var module = angular.module('ngApp', []);
	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });
	module.controller("orderService",function($scope,$http,$window,$timeout) {
		$scope.orgseq = 0;

		var start,end;

		$scope.page = function(page) {
			$scope.pagination1=true;
			$scope.pagination2=false;

			$.ajax({
		        type :'post',
		        data : {page : page, orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")},
		        dataType :'json',
		        url : '/order/orderList',
		        success : function(data) {
		        	$scope.data = JSON.parse(data.template);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.orgList = JSON.parse(data.orgs);
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$("#order").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
					$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.searchSubmit = function(page){
			$scope.pagination1=false;
			$scope.pagination2=true;

			var searchUserNm;
			var searchServiceNm;

			if($scope.searchOrgNm==null || $scope.searchOrgNm==""){
				$scope.searchOrgNm = "";
			}
			if($scope.searchUserNm == null || $scope.searchUserNm == ""){
				searchUserNm = "";
			}else{
				searchUserNm ="%"+$scope.searchUserNm+"%";
			}
			if($scope.searchServiceNm == null || $scope.searchServiceNm == ""){
				searchServiceNm = "";
			}else{
				searchServiceNm = "%"+$scope.searchServiceNm+"%";
			}
			if($scope.searchStatus == null || $scope.searchStatus == ""){
				$scope.searchStatus = "";
			}
			if($scope.fromdate == null || $scope.fromdate == ""){
				$scope.fromdate = "";
			}
			if($scope.todate == null || $scope.todate == ""){
				$scope.todate = "";
			}

			$.ajax({
		        type :'post',
		        data :{
					orgNm : $scope.searchOrgNm,
					userNm : searchUserNm,
					serviceNm : searchServiceNm,
					startdt : $scope.fromdate,
					enddt : $scope.todate,
					page: page, orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")
				},
		        dataType :'json',
		        url : '/order/orderSearch',
		        success : function(data) {
		        	$scope.data = JSON.parse(data.template);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
					$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.trClick = function(pseq,templateseq,orgseq){

			/*$window.location.href  = '/provisioning/nfvProvisioning?prov='+pseq+'&template='+templateseq+'&org='+orgseq;
			var winLog = window.open("/monitor/e2etrace?comm=2&seq="+pseq,"provisionSvg",'width=1240, height=900, left=600, top=50,' +
			'scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');*/
		};

		$scope.popProvisioning = function(pseq) {
			var winLog = window.open("/monitor/e2etrace?comm=2&seq="+pseq,"provisionSvg",'width=1040, height=900, left=600, top=50,' +
			'scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
		}

		$scope.delProvisioning = function(pseq, cname) {
			$.SmartMessageBox({
				title : "Provisioning 해지 안내 문구 입니다.",
				content : "해지를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.provisionseq = pseq;
					$scope.user = cname;
					$scope.$apply();
					$scope.provisionDel();
				}
			});
		};

		$scope.provisionDel = function(){
			$scope.templateDetail = false;
			$scope.provisionDelBtn = false;
			var appAtt = [];
			$('#del'+$scope.provisionseq).attr('class', 'hidden');
			$.smallBox({
		        title : "삭제중",
		        content : "Provisioning 삭제 중 입니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 7초후에 닫힙니다....</i>",
		        color : "#5384AF",
		        timeout: 7000
		    });
			appAtt = $scope.appAttribut;
			$.ajax({
		        type:'post',
		        data: {provisionseq : $scope.provisionseq, user : $scope.user, _xsrf : getCookie("_xsrf")},
		        dataType:'json',
		        url: '/provisioning/provisions/deleteApp',
		        success: function(data) {
		        	if (data !=null && data != '') {
			        	if (data.response != null && data.response != '') {
			        		if (data.response.result == 'success') {
			        			$.smallBox({
							        title : "삭제 완료",
							        content : "Provisioning 삭제 완료 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 5000
							    });
								$scope.page(1);
								$scope.$apply();
			        		} else {
			        			$scope.getWorkingData($scope.provisionseq);
								$scope.$apply();
			        		}
			        	} else {
			        		$scope.getWorkingData($scope.provisionseq);
							$scope.$apply();
			        	}
		        	}
		        }, error: function(jqXHR, textStatus, errorThrown) {
		        	$('#del'+$scope.provisionseq).attr('class', 'btn btn-danger');
		        	$.smallBox({
				        title : "삭제 실패",
				        content : "Provisioning 삭제 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 5000
				    });
					$scope.appAttribut = appAtt;
					$scope.provisionDelBtn = true;
					//$scope.getWorkingData($scope.provisionseq);
					//provisionProgress(7);
		        }
			});
		};

		$scope.getWorkingData = function(provisionseq){
			var num = 0;
			var tempLog = "";
			//console.log("pseq  :  "+provisionseq);
			timer = setInterval( function () {
			//----------------------------------------------------------------------------------
				$.ajax({
			        type:'post',
			        data: {provisionseq : provisionseq, _xsrf : getCookie("_xsrf")},
			        dataType:'json',
			        url: '/provisioning/provisions/step',
			        success: function(data) {
			        	$scope.neprocstatus = JSON.parse(data.step);
			        	if ($scope.neprocstatus.length > 0) {
							var step = $scope.neprocstatus[0].step;
							if (step == 55) {
								$.smallBox({
							        title : "삭제 완료",
							        content : "Provisioning 삭제 완료 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
							        color : "#5384AF",
							        timeout: 5000
							    });
								clearInterval(timer);
								$scope.page(1);
								$scope.$apply();
							}
			        	}
			        }, error: function(jqXHR, textStatus, errorThrown) {
//			            alert("provision progressbar 실패");
			            clearInterval(timer);
			        }
				});
			}, 1500); // 1초에 한번씩 받아온다.
		};
	});
$(document).ready(function() {
	functionMenu();
	init();
	// START AND FINISH DATE
	$("#fromdate").datepicker({
						changeMonth : true,
						numberOfMonths : 1,
						dateFormat : 'yy-mm-dd',
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
										$("#todate").datepicker("option","minDate",selectedDate);
						}
	});
	$("#todate").datepicker({
						changeMonth : true,
						numberOfMonths : 1,
						dateFormat : 'yy-mm-dd',
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
										$("#fromdate").datepicker("option","maxDate",selectedDate);
						}
	});

});

function init() {
	$('#lp_service_ul').attr('style', 'display:block;');
	$('#wid-utm-provision-btn_connect').hide();
};

var _gaq = _gaq || [];
_gaq.push([ '_setAccount', 'UA-XXXXXXXX-X' ]);
_gaq.push([ '_trackPageview' ]);

(function() {
	var ga = document.createElement('script');
	ga.type = 'text/javascript';
	ga.async = true;
	ga.src = ('https:' == document.location.protocol ? 'https://ssl'
			: 'http://www')
			+ '.google-analytics.com/ga.js';
	var s = document.getElementsByTagName('script')[0];
	s.parentNode.insertBefore(ga, s);
})();

function functionMenu(){
	$('#lp_service_list').attr('class','active');
	$('#lp_service_utm').removeClass('active');
	$('#lp_service_ips').removeClass('active');
	$('#lp_service_custServer').removeClass('active');
	$('#lp_service_nfv').removeClass('active');
	$("#main").css("visibility","visible");
}
