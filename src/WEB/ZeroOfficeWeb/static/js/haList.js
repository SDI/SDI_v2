function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("haService",function($scope,$timeout) {
		$scope.init = function(){
			for (var idx=1; idx<($scope.inputlines+1); idx++) {
				$('#inputData'+idx).remove();
			}
			$('#insertIsPrimary0').val('0');
			$('#insertStatus0').val('Active');
			$scope.modify = false;
			$scope.submit = false;
			$scope.removes = false;
			$scope.addHaBtn = true;
			$scope.checked = false;
			$scope.insertHaGName = "";
			$scope.utmListCnt = 0;
			$scope.inputlines = 0;
			$scope.orgseq = "0";
			$scope.modal = "modal";
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			$.ajax({
		        type:'post',
		        data : {
					orgseq : $scope.orgseq,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/ha/haList',
		        success: function(data) {
		        	$scope.haList = JSON.parse(data.has);
		        	$scope.utmList = JSON.parse(data.utms);
		        	$scope.utmListCnt = JSON.parse(data.utms).length;
		        	$scope.orgList = JSON.parse(data.orgs);
		        	$scope.orgseq = data.orgseq;
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addHa = function(){
			for (var idx=1; idx<=$scope.inputlines; idx++) {
	    		$('#inputData'+idx).remove();
			}
			$('#insertUtmseq0').val('');
			$('#insertUtmseq0').removeAttr('disabled');
			$scope.insertHaGName = "";
			$scope.inputlines = 0;
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
			$scope.paging(1);
		};

		$scope.haAdd = function() {
				if ($scope.inputlines < ($scope.utmListCnt-1)) {
				$scope.inputlines = $scope.inputlines + 1;
				var divs = '<hr><div style="margin-top:20px;" class="row" id="inputData' + $scope.inputlines + '">'
				+ '<div class="col-xs-12">'
				+ '<div class="form-group">'
				+ '<div class="col-xs-12">'
				+ '<label for="detailaddr" class="col-xs-2" style="top: 7px;">UTM:</label>'
				+ '<select style="width: 150px;" class="form-control ng-pristine ng-valid col-xs-2" id="insertUtmseq' + $scope.inputlines + '">'
				+ '<option value=""></option>';

				for (var utm in $scope.utmList) {
					divs = divs + '<option data-ng-repeat="utm in utmList" value="' + $scope.utmList[utm].utm_seq + '">' + $scope.utmList[utm].utm_name + '</option>';
				}

				divs = divs + '</select>'
				+ '</div>'
				+ '<div class="col-xs-12" style="margin-top: 10px;">'
				+ '<div class="form-inline">'
				+ '<label for="detailaddr" class="col-xs-2" style="top: 7px;">백업타입:</label>'
				+ '<select style="width: 150px;" class="form-control ng-pristine ng-valid col-xs-2" id="insertIsPrimary' + $scope.inputlines + '">'
				+ '<option value="0">Backup</option>'
				+ '<option value="1">Primary</option>'
				+ '</select>'
				+ '<label for="detailaddr" class="col-xs-2" style="margin-left:30px;top: 7px;">Status:</label>'
				+ '<select style="width: 150px;" class="form-control ng-pristine ng-valid col-xs-2" id="insertStatus' + $scope.inputlines + '">'
				+ '<option value="Active">Active</option>'
				+ '<option value="Standby">Standby</option>'
				+ '</select>'
				+ '</div>'
				+ '</div>'
				+ '</div>'
				+ '</div>'
				+ '</div>';

				$('#inputData').append(divs);
				if ($scope.inputlines > 0) {
					$scope.removes = true;
				} else {
					$scope.inputlines = false;
				}
			}
		}

		$scope.haRemove = function() {
			$('#inputData'+$scope.inputlines).remove();
			$scope.inputlines = $scope.inputlines - 1;

			if ($scope.inputlines > 0) {
				$scope.removes = true;
			} else {
				$scope.inputlines = false;
			}
		}

		$scope.haSubmit = function(){
			$scope.is_submit = false;
			$.ajax({
		        type:'post',
		        data : {
		        	_xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/ha/haMax',
		        success: function(data) {
		        	$scope.haSeq = data.data;
		        	$scope.haSubmits();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		}

		$scope.haSubmits = function() {
			for (var idx=0; idx<=$scope.inputlines; idx++) {
				$.ajax({
			        type:'post',
			        data : {
			        	haGName : $scope.insertHaGName,
			        	utmSeq : $('#insertUtmseq'+idx).val(),
						isPrimary : $('#insertIsPrimary'+idx).val(),
						status : $('#insertStatus'+idx).val(),
						haSeq : $scope.haSeq, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/ha/insertHa',
			        success: function(data) {
						$scope.init();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });

				if (idx == $scope.inputlines) {
					$scope.is_submit = true;
				}
			}

			for (var idx=1; idx<($scope.inputlines+1); idx++) {
				$('#inputData'+idx).remove();
			}

			if ($scope.is_submit) {
				$.smallBox({
			        title : "등록 완료",
			        content : "등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
			        color : "#5384AF",
			        timeout: 2000,
			        icon : "fa fa-bell"
			    });
				$('#insertHa').modal('hide');
			}
		};

		$scope.haDetail = function(ha_gname, ha_seq){
			for (var idx=1; idx<=$scope.inputlines; idx++) {
	    		$('#inputData'+idx).remove();
			}
			$scope.insertHaGName = ha_gname;
			$scope.modalTitle = "수정";
			$scope.haSeq = ha_seq;
			$.ajax({
		        type:'post',
		        data : {
		        	haSeq : ha_seq, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/ha/haDetail',
		        success: function(data) {
		        	var has = JSON.parse(data.has);
		        	$scope.utmList = has;
		        	for (var idx=1; idx<has.length; idx++) {
		        		var divs = '<hr><div style="margin-top:20px;" class="row" id="inputData' + idx + '">'
		        		+ '<div class="col-xs-12">'
						+ '<div class="form-group">'
						+ '<div class="col-xs-12">'
						+ '<label for="detailaddr" class="col-xs-2" style="top: 7px;">UTM:</label>'
						+ '<select style="width: 150px;" data-ng-disabled="1" class="form-control ng-pristine ng-valid col-xs-2" id="insertUtmseq' + idx + '">';

		    			for (var utm in $scope.utmList) {
		    				divs = divs + '<option value="' + $scope.utmList[utm].utm_seq + '">' + $scope.utmList[utm].utm_name + '</option>';
		    			}

		    			divs = divs + '</select>'
		    			+ '</div>'
						+ '<div class="col-xs-12" style="margin-top: 10px;">'
						+ '<div class="form-inline">'
						+ '<label for="detailaddr" class="col-xs-2">백업타입:</label>'
		    			+ '<select style="width: 150px;" class="form-control ng-pristine ng-valid col-xs-2" id="insertIsPrimary' + idx + '">'
		    			+ '<option value="0">Backup</option>'
		    			+ '<option value="1">Primary</option>'
		    			+ '</select>'
		    			+ '<label for="detailaddr" class="col-xs-2" style="margin-left:30px;top: 7px;">Status:</label>'
						+ '<select style="width: 150px;" class="form-control ng-pristine ng-valid col-xs-2" id="insertStatus' +idx + '">'
		    			+ '<option value="Active">Active</option>'
		    			+ '<option value="Standby">Standby</option>'
		    			+ '</select>'
		    			+ '</div>'
						+ '</div>'
						+ '</div>'
						+ '</div>'
						+ '</div>';

		    			$('#inputData').append(divs);
		        	}
		        	$scope.inputlines = has.length - 1;
					$scope.modify = true;
					$scope.submit = false;
		    		$scope.$apply();

		    		for (var idx=0; idx<has.length; idx++) {
		        		$('#insertUtmseq'+idx).val(has[idx].utm_seq);
		        		$('#insertIsPrimary'+idx).val(has[idx].is_primary);
		        		$('#insertStatus'+idx).val(has[idx].ha_state);
		        		$('#insertUtmseq'+idx).attr("disabled",true);
		        	}
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.haModify = function(){
			var leng = $scope.inputlines;
			for (var idx=0; idx<=leng; idx++) {
				$.ajax({
			        type:'post',
			        data : {
			        	haGName : $scope.insertHaGName,
			        	utmSeq : $('#insertUtmseq'+idx).val(),
						isPrimary : $('#insertIsPrimary'+idx).val(),
						haState : $('#insertStatus'+idx).val(), _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/ha/updateHa',
			        success: function(data) {
			        	$scope.init();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	alert("수정 실패");
			        }
			    });
			}
			for (var idx=1; idx<=leng; idx++) {
	    		$('#inputData'+idx).remove();
			}
			$('#insertHa').modal('hide');
			$.smallBox({
		        title : "수정 완료",
		        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
		        color : "#5384AF",
		        timeout: 2000,
		        icon : "fa fa-bell"
		    });
		};

		$scope.haClose = function(){
			for (var idx=1; idx<=($scope.inputlines); idx++) {
				$('#inputData'+idx).remove();
			}
			$scope.orgModal = false;
		};

		$scope.haDelete = function(){
			$.SmartMessageBox({
				title : "HA 그룹 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.haList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								utmSeq : $("#tdValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	utmSeq : $scope.item[j].utmSeq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/ha/deleteHa',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};

		$scope.haGDelete = function(){
			$scope.item=[];

			var msg = '삭제 하시겠습니까?\n그룹 : ';
			for(var i=0; i<$scope.haList.length; i++){
				var is_sel = false;
				if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
					for (var idx=0; idx<$scope.item.length; i++) {
						if ($scope.item[idx].haGName ==  $("#GName"+i).html()) {
							is_sel = true;
						}
					}

					if (!is_sel) {
						$scope.item.push({
							haSeq : $("#tddValue"+i).html(),
							haGName : $("#GName"+i).html()
						});
						if ($scope.item.length == 1) {
							msg = msg + $("#GName"+i).html()
						} else if ($scope.item.length > 1) {
							msg = msg + ', ' + $("#GName"+i).html()
						}
					}
				}
			}


			if (confirm(msg) == true) {
				$scope.haGDeleteOK();
			}
		};

		$scope.haGDeleteOK = function(){
			var num = 0;
			/*$scope.item=[];
			var num = 0;
			for(var i=0; i<$scope.haList.length; i++){
				if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
					$scope.item.push({
						haSeq : $("#tddValue"+i).html()
					});
				}
			}*/
			for(var j=0; j<$scope.item.length; j++){
				$.ajax({
			        type:'post',
			        data : {
			        	haSeq : $scope.item[j].haSeq, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/ha/deleteHaG',
			        success: function(data) {
			        	num = 1
						$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	alert("삭제 실패");
			        }
			    });
			}
			if(num == 1){
				$.smallBox({
			        title : "삭제 완료",
			        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
			        color : "#5384AF",
			        timeout: 2000,
			        icon : "fa fa-bell"
			    });
			}
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertHa').draggable();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').attr('class','active');
		$("#main").css("visibility","visible");
	}
