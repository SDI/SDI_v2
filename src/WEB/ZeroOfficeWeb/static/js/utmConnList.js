function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("utmConnService",function($scope,$timeout) {
		$scope.init = function(){
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUtmConnBtn = true;
			$scope.checked = false;
			$scope.insertUtmseq = "0";
			$scope.insertUtmLinkType = "LAN";
			$scope.insertSwseq = "0";
			$scope.sw_port = 0;
			$scope.orgseq = "0";
			$scope.modal = "modal";
			$scope.paging(1);
			$scope.portlimit = 0;
		};

		$scope.paging = function(page) {
			$.ajax({
		        type:'post',
		        data : {
					orgseq : $scope.orgseq,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/utmConn/utmConnList',
		        success: function(data) {
		        	$scope.utmConnList = JSON.parse(data.utmConns);
		        	$scope.utmList = JSON.parse(data.utms);
		        	$scope.switchList = JSON.parse(data.switchs);
		        	$scope.orgList = JSON.parse(data.orgs);
		        	$scope.orgseq = data.orgseq;
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.getPort = function() {
			$.ajax({
		        type:'post',
		        data : {
					swSeq : $scope.insertSwseq,
					_xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/utmConn/getPort',
		        success: function(data) {
		        	$scope.portlimit = data.port*1;
		        	$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		}

		$scope.addUtmConn = function(){
			$('#insertUtmseq').removeAttr('disabled');
			$('#insertUtmLinkType').removeAttr('disabled');
			$('#insertSwseq').removeAttr('disabled');
			$scope.insertUtmseq = "0";
			$scope.insertUtmLinkType = "LAN";
			$scope.insertSwseq = "0";
			$scope.sw_port = 0;
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
		};

		$scope.utmConnSubmit = function(){
			if ($scope.portlimit >= $scope.sw_port && $scope.sw_port >= 1) {
				$.ajax({
			        type:'post',
			        data : {
			        	utmSeq : $scope.insertUtmseq,
						utmLinkType : $scope.insertUtmLinkType,
						swSeq : $scope.insertSwseq,
						sw_port : $scope.sw_port, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/utmConn/insertUtmConn',
			        success: function(data) {
			        	$.smallBox({
					        title : "등록 완료",
					        content : "등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
						$('#insertUtmConn').modal('hide');
						$scope.init();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			} else {
				$.SmartMessageBox({
    				title : "포트 번호 오류",
    				content : "포트 번호가 잘 못 되었습니다.",
    				buttons : '[닫기]'
    			});
			}
		};

		$scope.utmConnDetail = function(utm_seq, utm_link_type, sw_seq, sw_port, portlimit){
			$scope.modalTitle = "수정";
			$scope.portlimit = portlimit*1;

			$scope.insertUtmseq = utm_seq;
			$scope.insertUtmLinkType = utm_link_type;
			$scope.sw_port = sw_port;
			$scope.insertSwseq = sw_seq;
			$scope.modify = true;
			$scope.submit = false;
    		$scope.$apply();
    		$('#insertUtmseq').val(utm_seq);
    		$('#insertSwseq').val(sw_seq);
    		$('#insertUtmseq').attr("disabled",true);
			$('#insertUtmLinkType').attr("disabled",true);
			$('#insertSwseq').attr("disabled",true);
		};

		$scope.utmConnModify = function(){
			if ($scope.portlimit >= $scope.sw_port && $scope.sw_port >= 1) {
				$.ajax({
			        type:'post',
			        data : {
			        	utmSeq : $scope.insertUtmseq,
						utmLinkType : $scope.insertUtmLinkType,
						swSeq : $scope.insertSwseq,
						sw_port : $scope.sw_port, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/utmConn/updateUtmConn',
			        success: function(data) {
			        	$('#insertUtmConn').modal('hide');
						$.smallBox({
					        title : "수정 완료",
					        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
						$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	alert("수정 실패");
			        }
			    });
			} else {
				$.SmartMessageBox({
    				title : "포트 번호 오류",
    				content : "포트 번호가 잘 못 되었습니다.",
    				buttons : '[닫기]'
    			});
			}
		};

		$scope.utmConnClose = function(){
			$scope.orgModal = false;
		};

		$scope.utmConnDelete = function(){
			$.SmartMessageBox({
				title : "UTM 연결정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.utmConnList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								utmSeq : $("#tdValue"+i).html(),
								utmLinkType : $("#tddValue"+i).html(),
								swSeq : $("#tdddValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	utmSeq : $scope.item[j].utmSeq,
					        	utmLinkType : $scope.item[j].utmLinkType,
					        	swSeq : $scope.item[j].swSeq,
					        	_xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/utmConn/deleteUtmConn',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertUtmConn').draggable();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').removeClass('active');
		$('#lp_inventory_utm_conn').attr('class','active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
