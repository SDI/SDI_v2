function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

	var module = angular.module('ngApp', []);
	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });
	module.controller("failService",function($scope,$http,$window,$timeout) {
		$scope.types = types;
		$scope.ip = '';

		$scope.page = function(page) {
			$.ajax({
		        type :'post',
		        data : {types : types, orgs : orgs, _xsrf : getCookie("_xsrf")},
		        dataType :'json',
		        url : '/monitor/fail',
		        success : function(data) {
		        	$scope.failList = JSON.parse(data.fails);
					$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		var data = '';
		if (types == "HW") {
			data = 'failover';
		} else if (types == "NFV") {
			data = 'failback';
		}

		$scope.failSubmit = function(){
			if ($scope.ip != null && $scope.ip != '') {
				$.ajax({
			        type :'post',
			        data : {data : data, office : office, ip : $scope.ip , msg : $scope.msg, _xsrf : getCookie("_xsrf")},
			        dataType :'json',
			        url : '/monitor/hacall',
			        success : function(data) {
			        	self.close();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("데이터 실패");
			        }
			    });
			}
		};

		$scope.failClose = function(){
			self.close();
		};
	});
$(document).ready(function() {
});
