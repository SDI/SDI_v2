function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
var testVlanList = [];
var module = angular.module('ngApp', ['ui.bootstrap']);
module.config(function($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  });

module.controller("CtrlService",function($scope, $http, $interval, $timeout) {
	$scope.provisionseq = pseq;
	$scope.orgName = orgName;
	$scope.orgSeq = orgSeq;
	$scope.vlanList = [];
	$scope.is_init = true;
	$scope.is_eth4 = false;
	$scope.is_eth5 = false;
	$scope.eth_add = true;
	$scope.eth_del = false;
	$scope.zone_cnt = 1;
	$scope.green_cnt = 1;
	$scope.orange_cnt = 0;
	$scope.blue_cnt = 0;
	$scope.green_cidr_cnt = 1;
	$scope.orange_cidr_cnt = 0;
	$scope.blue_cidr_cnt = 0;
	$scope.green_ip = [];
	$scope.orange_ip = [];
	$scope.blue_ip = [];
	$scope.green_vlans = ["", "", "", ""];
	$scope.orange_vlans = ["", "", ""];
	$scope.blue_vlans = ["", "", ""];
	$scope.green_cidr = [32, 32, 32, 32];
	$scope.orange_cidr = [32, 32, 32];
	$scope.blue_cidr = [32, 32, 32];
	$scope.eth1_cidrs_end = [32];
	$scope.hostname = '';
	$scope.domainname = '';
	$scope.dns1 = '';
	$scope.dns2 = '';
	$scope.blue_vlan_type = '고객사무실';
	$scope.utmNode = '0';
	$scope.port = 0;
	$scope.sport = '';
	$scope.eport = '';
	$scope.logicalID = '';
	$scope.swseq = '0';

	$scope.changeUtmNode = function() {
		$scope.zoneID = 0;
		$scope.port = 0;
		$scope.vlan_set();
		if ($scope.utmNode != '' && $scope.utmNode != null && $scope.utmNode != '0') {
			$.ajax({
		        type:'post',
		        data : {
		        	utmseq : $scope.utmNode,
		        	_xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/provisioning/getSwitch',
		        success: function(data) {
		        	$scope.swList = JSON.parse(data.switchs);
		        	$scope.switchsList = [];
		        	for (var i=0; i<data.switchsList.length; i++) {
		        		$scope.switchsList.push(data.switchsList[i][0]);
		        	}
			        $scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		} else {
			$scope.swList = [];
		}
	}

	$scope.changeSwitch = function() {
		$scope.zoneID = 0;
		$scope.port = 0;
		$scope.vlan_set();
		if ($scope.swseq != '' && $scope.swseq != null && $scope.swseq != '0') {
			$.ajax({
		        type:'post',
		        data : {
		        	swseq : $scope.swseq,
		        	_xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/provisioning/getport',
		        success: function(data) {
		        	$scope.port = data.port;
		        	$scope.sport = data.start*1;
		        	$scope.eport = data.end*1;
			        $scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		}
	}

	$scope.ch_etc = function() {
		$('#dns1_0').val($('#dns1').val());
		$('#dns2_0').val($('#dns2').val());
		$('#hostname_0').val($('#hostname').val());
		$('#domainname_0').val($('#domainname').val());
	}

	$scope.ch_check = function() {
		if ($("input:checkbox[id='br0_proxys0']").is(":checked")) {
			$("input:checkbox[id='br0_proxys1']").prop("checked", true);
		} else if (!$("input:checkbox[id='br0_proxys0']").is(":checked")) {
			$("input:checkbox[id='br0_proxys1']").prop("checked", false);
		}

		if ($("input:checkbox[id='br1_proxys0']").is(":checked")) {
			$("input:checkbox[id='br1_proxys1']").prop("checked", true);
		} else if (!$("input:checkbox[id='br1_proxys0']").is(":checked")) {
			$("input:checkbox[id='br1_proxys1']").prop("checked", false);
		}

		if ($("input:checkbox[id='br2_proxys0']").is(":checked")) {
			$("input:checkbox[id='br2_proxys1']").attr("checked", true);
		} else if (!$("input:checkbox[id='br2_proxys0']").is(":checked")) {
			$("input:checkbox[id='br2_proxys1']").attr("checked", false);
		}
	}

	$scope.vlan_set = function() {
		if ($scope.zoneID > 0) {
			$scope.logicalID = $scope.zoneID*1 + $scope.sport*1 - 1;
			$scope.eth1_vlan.value = ($scope.zoneID*1 + $scope.sport*1 - 1) * 10 + 2000;
			$scope.green_vlans[0] = ($scope.zoneID*1 + $scope.sport*1 - 1) * 10 + 1;

			if ($scope.orange_cnt > 0) {
				$scope.orange_vlans[0] = ($scope.zoneID*1 + $scope.sport*1 - 1) * 10;
			}

			if ($scope.blue_cnt > 0) {
				if ($('#blue_type').val() == '국사') {
					$scope.blue_vlans[0] = ($scope.zoneID*1 + $scope.sport*1 - 1) * 10 + 2;
				} else {
					$scope.blue_vlans[0] = ($scope.zoneID*1 + $scope.sport*1 - 1) * 10 + 3;
				}
			}
		} else {
			$scope.zoneID = '';
			$scope.logicalID = '';
			$scope.eth1_vlan.value = '';

			for (var i=0; i<$scope.green_cnt; i++) {
				$scope.green_vlans[i] = '';
			}

			for (var i=0; i<$scope.orange_cnt; i++) {
				$scope.orange_vlans[i] = '';
			}

			for (var i=0; i<$scope.blue_cnt; i++) {
				$scope.blue_vlans[i] = '';
			}
		}
		/*if ($scope.zone_cnt == 2) {
			if ($scope.green_cnt == 2) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
			} else if ($scope.orange_cnt == 1) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
			} else if ($scope.blue_cnt == 1) {
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			}
		} else if ($scope.zone_cnt == 3) {
			if ($scope.green_cnt == 3) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.green_vlans[2] = $scope.zoneID * 10 + 5;
			} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.orange_vlans[0] = $scope.zoneID * 10;
			} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 2) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 2 && $scope.blue_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 1 && $scope.blue_cnt == 2) {
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 3;
			} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt == 1) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 3;
			}
		} else if ($scope.zone_cnt == 4) {
			if ($scope.green_cnt == 4) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.green_vlans[2] = $scope.zoneID * 10 + 5;
				$scope.green_vlans[3] = $scope.zoneID * 10 + 7;
			} else if ($scope.green_cnt == 3 && $scope.orange_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.green_vlans[2] = $scope.zoneID * 10 + 5;
				$scope.orange_vlans[0] = $scope.zoneID * 10;
			} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 2) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 3 && $scope.blue_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.green_vlans[2] = $scope.zoneID * 10 + 5;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 2 && $scope.blue_cnt == 2) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 4;
			} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 1 && $scope.blue_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 3) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
				$scope.orange_vlans[2] = $scope.zoneID * 10 + 4;
			} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 2 && $scope.blue_cnt == 1) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 3;
			} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt == 2) {
				$scope.orange_vlans[0] = $scope.zoneID * 10;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 3;
			} else if ($scope.green_cnt == 1 && $scope.blue_cnt == 3) {
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 4;
			}
		}*/
	}

	$scope.addGreen = function() {
		$scope.zone_cnt++;
		$scope.green_cnt++;
		if ($scope.green_cidr_cnt == 0) {
			$scope.green_cidr_cnt++;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.delGreen = function() {
		$scope.zone_cnt--;
		$scope.green_cnt--;
		if ($scope.green_cnt == 0) {
			$scope.green_cidr_cnt--;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.addGreenIP = function() {
		$scope.green_cidr_cnt++;
		$scope.$apply();
	}

	$scope.delGreenIP = function() {
		$scope.green_cidr_cnt--;
		$scope.$apply();
	}

	$scope.addOrange = function() {
		$scope.zone_cnt++;
		$scope.orange_cnt++;
		if ($scope.orange_cidr_cnt == 0) {
			$scope.orange_cidr_cnt++;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.delOrange = function() {
		$scope.zone_cnt--;
		$scope.orange_cnt--;
		if ($scope.orange_cnt == 0) {
			$scope.orange_cidr_cnt--;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.addOrangeIP = function() {
		$scope.orange_cidr_cnt++;
		$scope.$apply();
	}

	$scope.delOrangeIP = function() {
		$scope.orange_cidr_cnt--;
		$scope.$apply();
	}

	$scope.addBlue = function() {
		$scope.zone_cnt++;
		$scope.blue_cnt++;
		if ($scope.blue_cidr_cnt == 0) {
			$scope.blue_cidr_cnt++;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.delBlue = function() {
		$scope.zone_cnt--;
		$scope.blue_cnt--;
		if ($scope.blue_cnt == 0) {
			$scope.blue_cidr_cnt--;
		}

		$scope.vlan_set();

		$scope.$apply();
	}

	$scope.addBlueIP = function() {
		$scope.blue_cidr_cnt++;
		$scope.$apply();
	}

	$scope.delBlueIP = function() {
		$scope.blue_cidr_cnt--;
		$scope.$apply();
	}

	$scope.changeZoneId = function() {
		var num_check=/^[0-9]*$/;

		if(num_check.test($scope.zoneID)) {
			$scope.vlan_set();
			/*$scope.orange_vlans[0] = $scope.zoneID * 10;
			if ($scope.green_cnt == 3) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.green_vlans[2] = $scope.zoneID * 10 + 4;
			} else if ($scope.orange_cnt == 3) {
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
				$scope.orange_vlans[2] = $scope.zoneID * 10 + 3;
			} else if ($scope.blue_cnt == 2) {
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[1] = $scope.zoneID * 10 + 3;
			} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 2) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
			} else if ($scope.green_cnt == 2 && $scope.blue_cnt == 1) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			} else if ($scope.orange_cnt == 2 && $scope.blue_cnt == 1) {
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 3;
			} else if ($scope.green_cnt == 2) {
				$scope.green_vlans[1] = $scope.zoneID * 10 + 3;
			} else if ($scope.orange_cnt == 2) {
				$scope.orange_vlans[1] = $scope.zoneID * 10 + 2;
			} else if ($scope.blue_cnt == 1) {
				$scope.blue_vlans[0] = $scope.zoneID * 10 + 2;
			}*/
		} else {
			$scope.zoneID = '';
		}
	}

	$scope.range = function(min, max){
		var input = [];

		for (var i = max; i >= min; i--) {
			input.push(i);
		}

		return input;
	};

	$scope.range2 = function(min, max){
		var input = [];

		for (var i = min; i <= max; i++) {
			input.push(i);
		}

		return input;
	};

	$scope.range3 = function(min, max){
		var input = [];

		for (var i = min; i <= max; i++) {
			if ($.inArray(i, $scope.switchsList) < 0) {
				input.push(i);
			}
		}

		return input;
	};

	$scope.init = function(){
		$scope.is_eth4 = false;
		$scope.is_eth5 = false;
		$scope.eth_add = true;
		$scope.eth_del = false;
		$scope.greenVlan = 0;
		$scope.orangeVlan = 0;
		$scope.vlanList = [];
		$scope.userid = userid;
		$scope.fixList = [];
		$scope.appAttribut = [];
		$scope.selectList = [];
		$scope.poolList = [];
		$scope.neclsseq = "";
		$scope.templatedesc = "";
		$scope.tempPool = "";
		$scope.consoleLog = [];
		$scope.appAttribut = [];
		$scope.count = "0";
		$scope.fixDetail = false;
		$scope.users = "0";
		$scope.ethernet = [];
		$scope.bridge = [];
		$scope.routers = [];
		$scope.br0_cidrs = [""];
		$scope.br1_cidrs = [""];
		$scope.br2_cidrs = [""];

		$scope.vlan = [];
		$scope.geteway = [];
		$scope.subnetcidr =[];
		$scope.values = [];
		$scope.eth1 = [];
		$scope.eth2 = [];
		$scope.eth3 = [];
		$scope.eth4 = [];
		$scope.eth5 = [];
		$scope.br0 = [];
		$scope.br1 = [];
		$scope.br2 = [];

		$scope.provisionDelBtn = false;
		$scope.provisionBtn = false;
		$scope.configuration = false;

		$scope.eth1_btn = true;
		$scope.eth2_btn = true;
		$scope.eth3_btn = true;
		$scope.eth4_btn = true;
		$scope.eth5_btn = true;

		$scope.br0_proxy_btn = true;
		$scope.br0_value_btn = true;
		$scope.br1_proxy_btn = true;
		$scope.br1_value_btn = true;
		$scope.br2_proxy_btn = true;
		$scope.br2_value_btn = true;

		$scope.router_btn = true;

		$scope.eth1_cidrs = [];
		$scope.eth2_cidrs = [];
		$scope.eth3_cidrs = [];
		$scope.eth4_cidrs = [];
		$scope.eth5_cidrs = [];

		$scope.eth1_cidrs_end = [32];
		$scope.eth2_cidrs_end = [];
		$scope.eth3_cidrs_end = [];
		$scope.eth4_cidrs_end = [];
		$scope.eth5_cidrs_end = [];

		$scope.br0_proxys = [];
		$scope.br0_values = [];
		$scope.br1_proxys = [];
		$scope.br1_values = [];
		$scope.br2_proxys = [];
		$scope.br2_values = [];

		$scope.routers = [];


		// 아이콘 칼라 이미지
		$scope.grayColor = "../static/lib/SmartAdmin/img/soweb_service/process_gray.png";
		$scope.yellowColor = "../static/lib/SmartAdmin/img/soweb_service/process_yellow.png";
		$scope.greenColor = "../static/lib/SmartAdmin/img/soweb_service/process_green.png";

		// 클래스 명
		$scope.createBar = "progress-bar progress-bar-success";
		$scope.delBar = "progress-bar progress-bar-danger";

		// 서버, 스위치
		$scope.fixList.push(
			{neclsseq : 13, subname : "L2_Switch"}
		);

		// 서비스 목록
		$scope.NFVprov();
	};

	$scope.NFVprov = function() {
		$.ajax({
	        type:'post',
	        data : {
				org : $scope.orgSeq, _xsrf : getCookie("_xsrf")
	        },
	        dataType:'json',
	        url: '/provisioning/templateData',
	        success: function(data) {
	        	if(data.template[0].nstemplateseq == '0'){
	        		$.SmartMessageBox({
	    				title : "Provisioning을 진행 할 수가 없습니다",
	    				content : "템플릿이 없습니다.",
	    				buttons : '[닫기]'
	    			});
	        	} else {
	        		$scope.templateData = JSON.parse(data.template);
	        		$scope.utmList = JSON.parse(data.utms);
	        		//console.log(JSON.stringify($scope.templateData));
		        	$scope.tempPool = JSON.parse(data.poolList);
		        	//console.log(JSON.stringify($scope.tempPool));
		        	$scope.userList = data.users;
		        	$scope.utmNode = '0';
		        	$scope.port = '0';
		        	$scope.swseq = '0';
		        	$scope.swList = [];
		        	$scope.vlanList = data.vlan;
		        	$scope.orgList = JSON.parse(data.orgs);
		        	if($scope.provisionseq == 0){
		    			$scope.templatedesc = $scope.templateData[0].templatedesc;
		    			$scope.templateseq = $scope.templateData[0].nstemplateseq;
		    			//$scope.orgSeq = $scope.orgList[0].orgs;
		    			if ($scope.is_init) {
		    				NFVPoolList();
		    			}
		    			$scope.NFVpool(1);
		    		} else {
		    			$scope.templateseq = templateseq;
		    			$scope.orgSeq = orgSeq;
		    			if ($scope.is_init) {
		    				NFVPoolList();
		    			}
		    			$scope.NFVpool(2);
		    		}
		        	$scope.is_init = false;
		        	$scope.$apply();
	        	}

	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert("데이터 실패");
	        }
	    });
	};

	function NFVPoolList(){
		$scope.poolList = [];
		$scope.count = "0";
		for(var i=0; i<$scope.tempPool.length; i++){
			//console.log("tempPool  :  "+JSON.stringify($scope.tempPool));
			if($scope.tempPool[i].templateseq == $scope.templateseq){
				$scope.poolList.push({
					subname : $scope.tempPool[i].subname,
					neclsseq : $scope.tempPool[i].neclsseq,
					imagesrc : $scope.tempPool[i].ico_small,
					necatseq : ""
				});
			}
		}
		//console.log(JSON.stringify($scope.poolList));
		//$("input:checkbox[id='template0']").attr("checked", true);
	};

	$scope.NFVpool = function(seq){
		$scope.appAttribut = [];
		// seq = 1 이면 초기모습, seq = 2 이면 프로비저닝 완료 화면
		var data = {
			      'templateseq' : $scope.templateseq,
			      'provisionseq' : $scope.provisionseq, _xsrf : getCookie("_xsrf"),
			       }
		$.ajax({
	        type:'post',
	        data : data,
	        dataType:'json',
	        url: '/provisioning/pool',
	        success: function(data) {
	        	$scope.templateSlide = false;
				$scope.provisionBtn = true;
				//$scope.provisionseq = 0;
				$scope.provisionDelBtn = false;
				$scope.configuration = true;
				$scope.necatseq = '61';
				necatTem();
	        	$scope.pool = JSON.parse(data.pool);
	        	//console.log(JSON.stringify($scope.pool));
	        	$scope.fixField = JSON.parse(data.fixField);
	        	var progressWidth = "";
				var servicename = "";
				var imagesrc = "";
				var item = "";
				var procstatecode = "";
				$scope.fixDetailList();
				//console.log(JSON.stringify($scope.poolList));
				if(seq == 1){
					for(var i=0; i < $scope.poolList.length; i++){
						progressWidth = "0";
						progressbar = true;
						urlbtn = false;
						consoleUrl = "";
						item = "";
						procstatecode = "";
						servicename = $scope.poolList[i].subname;
						imagesrc = $scope.poolList[i].imagesrc;
						iconsrc = $scope.grayColor;
						if(i==0){
							selected = true;
							$("input:checkbox[id='template0']").attr("checked", true);
						} else {
							selected = false;
						}
						$scope.appAttribut.push(
							{
								subname : $scope.poolList[i].subname,
								imagesrc : imagesrc,
								servicename : servicename,
								iconsrc : iconsrc,
								progressWidth : progressWidth,
								progressbar : progressbar,
								urlbtn : urlbtn,
								neclsseq : $scope.poolList[i].neclsseq,
								progressClass : $scope.createBar,
								necatseq : "",
								selected : selected,
								item : item,
								consoleUrl : consoleUrl,
								procstatecode : procstatecode
							}
						);
					}
				} else {
					$scope.provisionDelBtn = true;
					$scope.provisionBtn = false;
					$scope.users = provisionUserid;
					for(var x=0; x< $scope.poolList.length; x++){
						if($scope.pool[0].neclsseq == $scope.poolList[x].neclsseq){
							progressWidth = "100";
							progressbar = false;
							urlbtn = true;
							if($scope.pool[0].console_url != ""){
								consoleUrl = $scope.pool[0].console_url;
							} else {
								consoleUrl = "";
							}
							$scope.templatedesc = $scope.pool[0].templatedesc;
							servicename = $scope.pool[0].description;
							imagesrc = $scope.pool[0].icon_large;
							iconsrc = $scope.greenColor;
							procstatecode = $scope.pool[0].procstatecode;
							$scope.itemNameChange($scope.pool[0].neclsseq);
							item = $scope.items;
							selected = true;
							$("input:checkbox[id='template0']").attr("checked", true);
						} else {
							progressWidth = "0";
							progressbar = true;
							urlbtn = false;
							consoleUrl = "";
							item = "";
							procstatecode = "";
							iconsrc = $scope.grayColor;
							servicename = $scope.poolList[x].subname;
							imagesrc = $scope.poolList[x].ico_small;
							selected = false;
						}
						$scope.appAttribut.push(
							{
								subname : $scope.poolList[x].subname,
								imagesrc : imagesrc,
								servicename : servicename,
								iconsrc : iconsrc,
								progressWidth : progressWidth,
								progressbar : progressbar,
								urlbtn : urlbtn,
								neclsseq : $scope.poolList[x].neclsseq,
								progressClass : $scope.createBar,
								necatseq : "",
								selected : selected,
								item : item,
								consoleUrl : consoleUrl,
								procstatecode : procstatecode
							}
						);
					}
				}
				$scope.$apply();
				//console.log("$scope.appAttribut   "+JSON.stringify($scope.appAttribut));


				if(seq == 1){
					for(var j=0; j < $scope.poolList.length; j++){
						$("#template_"+$scope.appAttribut[j].subname).addClass('disable');
					}
				}
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert("data 실패");
	        }


		});
	};

	$scope.itemNameChange =  function(neclsseq){
		var default_item = $scope.pool[0].config_props.service[0].item;
		$scope.items = [];
		for(var i=0; i < default_item.length; i++){
			$scope.items.push({
				display_name : default_item[i].display_name,
				default_value : default_item[i].value,
				name : default_item[i].name
			});
		}
	};
	$scope.switchVlan = function(vlan, id){
		if(id == 'green'){
			$("#green_vlan").html(vlan);
			$('#VLAN').html(vlan);
			$('#de_VLAN').html(vlan);
			$scope.greenVlan = vlan;
		} else {
			$("#orange_vlan").html(vlan);
			$scope.orangeVlan = vlan;
		}


	}
	// url 호출
	$scope.urlConnect= function(neclsseq){
		$.ajax({
	        type:'post',
	        data : {
	        	templateseq : $scope.templateseq,
				provisionseq : $scope.provisionseq, _xsrf : getCookie("_xsrf")
	        },
	        dataType:'json',
	        url: '/provisioning/pool',
	        success: function(data) {
	        	window.open(JSON.parse(data.pool)[0].console_url);
				$scope.$apply();
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert("data 실패");
	        }
		});
    };

    $scope.urlLink = function(neclsseq){
    	for(var i=0; i<$scope.appAttribut.length; i++){
			if($scope.appAttribut[i].neclsseq == neclsseq){
				window.open($scope.appAttribut[i].consoleUrl);
			};
		}
    }
	// provisioning ( uuid ) 생성
	$scope.uuidGenerator =function(){

	    var d = new Date().getTime();
	    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	        var r = (d + Math.random()*16)%16 | 0;
	        d = Math.floor(d/16);
	        return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	    });
	    return uuid;

	};
    // 템플릿 변경시
    $scope.templateChange = function(){
    	$scope.provisionSeq = "0";
    	$scope.neclsseq = "0";
    	/*for(var i=0; i<$scope.templateData.length; i++){
    		console.log(JSON.stringify($scope.templateData));
    		console.log($scope.templatedesc);
    		if($scope.templateData[i].nstemplateseq == $scope.templateseq){
    			$scope.templateseq = $scope.templateData[i].nstemplateseq;
    		}
    	}*/
    	NFVPoolList();
    	$scope.appAttribut = [];
		$scope.selectList = [];
		$scope.neclsseq = "";
    	$scope.NFVpool(1);
    }

	// Switch, Server 상세페이지
    $scope.fixDetailList = function(){
    	$scope.fixFieldList = [];
    	for(var i=0; i<$scope.fixField.length; i++){
    		if(i == 0){
    			$scope.fixFieldList.push(
	    			{
	    				neclsseq : $scope.fixField[i].neclsseq,
	    				necatseq : $scope.fixField[i].necatseq,
	    				subname : $scope.fixList[0].subname,
	    				servicename : $scope.fixField[i].service_name,
	    				item : JSON.parse($scope.fixField[i].config_props.value).application.service[0].item
	    			}
	    		);
    		} else {
    			$scope.fixFieldList.push(
	    			{
	    				neclsseq : $scope.fixField[i].neclsseq,
	    				necatseq : $scope.fixField[i].necatseq,
	    				subname : $scope.fixList[1].subname,
	    				servicename : $scope.fixField[i].service_name,
	    				item : JSON.parse($scope.fixField[i].config_props.value).application.service[0].item
	    			}
	    		);
    		}
    	}
    };

  //pool 리스트에서 체크박스 값 변환
	$scope.changePool = function(){
		$scope.selectList =[];
		for(var i=0; i < $scope.poolList.length; i++){
			if($("input:checkbox[id='template"+i+"\']").is(":checked")){
				$scope.selectList.push(
					{check : true}
				);
				$scope.provisionDelBtn = false;
			} else {
				$scope.selectList.push(
					{check : false}
				);
				$scope.provisionDelBtn = false;
			}
		}
		creatApp();

	};

	// pool 리스트에서 체크박스 값 변경 될 때마다 호출
	function creatApp(){
		var temAttribut = [];
		var selected = "";
		for(var i=0; i<$scope.poolList.length; i++){
			selected = $scope.selectList[i].check;
			appSelected = $scope.appAttribut[i].selected;
			if(appSelected && $("input:checkbox[id='template"+i+"\']").is(":checked")){
				if($scope.appAttribut[i].neclsseq != $scope.neclsseq){
					temAttribut.push(
						{
							subname : $scope.appAttribut[i].subname,
							imagesrc : $scope.appAttribut[i].imagesrc,
							servicename : $scope.appAttribut[i].servicename,
							iconsrc : $scope.appAttribut[i].iconsrc,
							progressWidth : $scope.appAttribut[i].progressWidth,
							progressbar : $scope.appAttribut[i].progressbar,
							urlbtn : $scope.appAttribut[i].urlbtn,
							neclsseq : $scope.appAttribut[i].neclsseq,
							progressClass : $scope.appAttribut[i].progressClass,
							necatseq : $scope.appAttribut[i].necatseq,
							selected : selected,
							item : $scope.appAttribut[i].item,
							consoleUrl : $scope.appAttribut[i].consoleUrl,
							procstatecode : $scope.appAttribut[i].procstatecode
						}
					);
				} else {
					temAttribut.push(
						{
							subname : $scope.appAttribut[i].subname,
							imagesrc : $scope.necatTemplate[0].icon_large,
							servicename : $scope.necatTemplate[0].service_name,
							iconsrc : $scope.yellowColor,
							progressWidth : $scope.appAttribut[i].progressWidth,
							progressbar : $scope.appAttribut[i].progressbar,
							urlbtn : $scope.appAttribut[i].urlbtn,
							neclsseq : $scope.necatTemplate[0].neclsseq,
							progressClass : $scope.createBar,
							necatseq : $scope.necatTemplate[0].necatseq,
							selected : selected,
							item : $scope.necatItem,
							consoleUrl : "",
							procstatecode : "0"
						}
					);
				}
			} else {
				temAttribut.push(
					{
						subname : $scope.poolList[i].subname,
						imagesrc : $scope.poolList[i].imagesrc,
						servicename : $scope.poolList[i].subname,
						iconsrc : $scope.grayColor,
						progressWidth : "0",
						progressbar : true,
						urlbtn : false,
						neclsseq : $scope.poolList[i].neclsseq,
						progressClass : $scope.createBar,
						necatseq : $scope.poolList[i].necatseq,
						selected : selected,
						item : "",
						consoleUrl : "",
						procstatecode : "0"
					}
				);
			}
		}
		$timeout(function(){
			for(var j=0; j < $scope.poolList.length; j++){
				if(temAttribut[j].item == ""){
					$("#template_"+$scope.poolList[j].subname).addClass('disable');
				}
			}
		},100);
		$scope.appAttribut = temAttribut;
	};
	// 슬라이드 팝업
	$scope.popup = function(neclsseq, subname){
		$scope.templateSlide = false;
		$scope.fixDetail = false;
		var p = $("#"+subname);
		var offset = p.offset();
		$scope.cssLeft = parseInt(offset.left)-681;
		var data = {
				neclsseq : neclsseq, _xsrf : getCookie("_xsrf")
		};
		$.ajax({
	        type:'post',
	        data : data,
	        dataType:'json',
	        url: '/provisioning/appSlide',
	        success: function(data) {
	        	$scope.appTemplate = JSON.parse(data.appSlide);
	        	//console.log(JSON.stringify($scope.appTemplate));
				$scope.subname = subname;
				$scope.$apply();
				$timeout(function(){
					$("#utmCarousel").find("li").eq(0).addClass("active");
					$("#"+subname+"0").addClass("active");
					$scope.templateSlide = true;
					$scope.$apply();
				},100);
	        }
		});
	};
	// 취소 버튼
	$scope.closeBtn = function(){
		$scope.templateSlide = false;
		$scope.templateDetail = false;
		$scope.fixDetail = false;
	};

	// 템플릿 선택 버튼
	$scope.templateOkBtn = function(){
		$scope.templateSlide = false;
		$scope.provisionBtn = true;
		$scope.provisionseq = 0;
		$scope.provisionDelBtn = false;
		$scope.configuration = true;

		/*for(var k=0; k<$scope.appTemplate.length; k++){
			if($('#utmCarousel .carousel-inner').find('.item').eq(k).hasClass('active')) {
				$scope.necatseq = $('#necatseq'+k).val();
			}
		}*/
		$scope.necatseq = '61';
		necatTem();
	};

	// 템플릿 속성
	function necatTem(){
		var data = {
				necatseq : $scope.necatseq, _xsrf : getCookie("_xsrf")
		};
		$.ajax({
	        type:'post',
	        data : data,
	        dataType:'json',
	        url: '/provisioning/tbNecat',
	        success: function(data) {
	        	$scope.selectList = [];
				$scope.necatTemplate = JSON.parse(data.necat);
				//console.log(JSON.stringify($scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet));
//				console.log(JSON.stringify($scope.necatTemplate[0].config_props.provision.application[0].service[0].item[1].bridge[0].br0));

				$scope.eth1_vlan = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[3].eth1.vlan;
				$scope.eth2_vlan = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[2].eth2.vlan;
				$scope.eth3_vlan = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[0].eth3.vlan;
				$scope.eth4_vlan = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[1].eth4.vlan;
				$scope.eth5_vlan = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[4].eth5.vlan;

				$scope.eth1_gateway = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[3].eth1.defaultgateway;
				/*$scope.eth2_gateway = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[2].eth2.defaultgateway;
				$scope.eth3_gateway = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[0].eth3.defaultgateway;
				$scope.eth4_gateway = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[1].eth4.defaultgateway;*/

				$scope.eth1_cidr = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[3].eth1.subnetcidr;
				$scope.eth2_cidr = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[2].eth2.subnetcidr;
				$scope.eth3_cidr = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[0].eth3.subnetcidr;
				$scope.eth4_cidr = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[1].eth4.subnetcidr;
				$scope.eth5_cidr = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[0].ethernet[4].eth5.subnetcidr;

				$scope.eth1_cidr_start = [];
				$scope.eth1_cidr_end = [32];
				for(var i=0; i<$scope.eth1_cidr.value.length; i++){
					var value1 = $scope.eth1_cidr.value[i].split('/')[0];
					$scope.eth1_cidr_start.push(
						value1
					);

					var value2 = $scope.eth1_cidr.value[i].split('/')[1];
					$scope.eth1_cidr_end.push(
						value2
					);
				}

				$scope.eth2_cidr_start = [];
				$scope.eth2_cidr_end = [];
				for(var i=0; i<$scope.eth2_cidr.value.length; i++){
					var value1 = $scope.eth2_cidr.value[i].split('/')[0];
					$scope.eth2_cidr_start.push(
						value1
					);

					var value2 = $scope.eth2_cidr.value[i].split('/')[1];
					$scope.eth2_cidr_end.push(
						value2
					);
				}

				$scope.eth3_cidr_start = [];
				$scope.eth3_cidr_end = [];
				for(var i=0; i<$scope.eth3_cidr.value.length; i++){
					var value1 = $scope.eth3_cidr.value[i].split('/')[0];
					$scope.eth3_cidr_start.push(
						value1
					);

					var value2 = $scope.eth3_cidr.value[i].split('/')[1];
					$scope.eth3_cidr_end.push(
						value2
					);
				}

				$scope.eth4_cidr_start = [];
				$scope.eth4_cidr_end = [];
				for(var i=0; i<$scope.eth4_cidr.value.length; i++){
					var value1 = $scope.eth4_cidr.value[i].split('/')[0];
					$scope.eth4_cidr_start.push(
						value1
					);

					var value2 = $scope.eth4_cidr.value[i].split('/')[1];
					$scope.eth4_cidr_end.push(
						value2
					);
				}

				$scope.eth5_cidr_start = [];
				$scope.eth5_cidr_end = [];
				for(var i=0; i<$scope.eth5_cidr.value.length; i++){
					var value1 = $scope.eth5_cidr.value[i].split('/')[0];
					$scope.eth5_cidr_start.push(
						value1
					);

					var value2 = $scope.eth5_cidr.value[i].split('/')[1];
					$scope.eth5_cidr_end.push(
						value2
					);
				}

				$scope.br0 = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[1].bridge[0].br0;
				$scope.br1 = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[1].bridge[1].br1;
				$scope.br2 = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[1].bridge[2].br2;

				$scope.catalname = $scope.necatTemplate[0].config_props.provision.application[0].service[0].name;
				$scope.neclsseq = $scope.necatTemplate[0].config_props.provision.application[0].neclsseq;
				$scope.necatseq = $scope.necatTemplate[0].config_props.provision.application[0].necatseq;
				$scope.router = $scope.necatTemplate[0].config_props.provision.application[0].service[0].item[2].router;




				$scope.neclsseq = $scope.necatTemplate[0].neclsseq;
				for(var i=0; i < $scope.poolList.length; i++){
					if($("input:checkbox[id='template"+i+"\']").is(":checked")){
						$scope.selectList.push(
							{check : true}
						);
					} else {
						$scope.selectList.push(
							{check : false}
						);
					}
				}
				$scope.$apply();
				creatApp();
	        }, error: function(jqXHR, textStatus, errorThrown) {
	            alert("데이터 실패");
	        }
		});
	};
	function attribute(name){

	}
	function attribute_messageBox(){
		$.SmartMessageBox({
			title : "속성을 추가 하실 수 없습니다.",
			content : "내용을 입력해 주시기 바랍니다.",
			buttons : '[닫기]'
		});
	}
	$scope.addAttribute = function(name){
		var temp = '';
		if(name == 'eth1'){
			if($scope.eth1_btn){
				/*if ($scope.eth1_cidr_start[0].length != 0) {*/
					$scope.eth1_btn = false;
					var tmp =  $scope.eth1_cidrs[0];
					$scope.eth1_cidrs.push('');
				/*} else {
					attribute_messageBox();
				}*/
			} else {
				temp = $('#'+name+'_cidrs'+($scope.eth1_cidrs.length-1)).val();
				/*if(temp.length != 0){*/
					var tmp =  $scope.eth1_cidrs[0];
					$scope.eth1_cidrs.push('');
				/*} else {
					attribute_messageBox();
				}*/
			}
		}
	}

	$scope.delAttribute = function(name){
		var temp = '';
		if(name == 'eth1'){
			$scope.eth1_cidrs.pop();
			if($scope.eth1_cidrs.length == 0){
				$scope.eth1_btn = true;
			}
		}
	}

	// 상세 페이지
	$scope.detail = function(neclsseq, subname){
		$scope.templateDetail = true;
		$scope.fixDetail = false;

		$scope.detailSubname = subname;
		$scope.detailNeclsseq = neclsseq;
		for(var i=0; i<$scope.poolList.length; i++){
			if($scope.appAttribut[i].neclsseq == neclsseq){
				$scope.detailItem = $scope.appAttribut[i].item;
			}
		}
		var p = $("#template_"+subname);
		var offset = p.offset();
		$scope.cssLeft = parseInt(offset.left)-748;
	};

	// 상세피이지 ok 버튼
	$scope.detailOkBtn = function(neclsseq){
		var num_check=/^[0-9]*$/;
		var ip_check=/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;

		var checkers = true;

		$scope.vlans = [];
		for (var i=0; i<$scope.vlanList.length; i++) {
			$scope.vlans.push($scope.vlanList[i].vlan);
		}
		testVlanList = $scope.vlans;

		if ($.inArray($('#red_vlan_value').val()*1, $scope.vlans) < 0) {
			$.SmartMessageBox({
				title : "확인",
				content : "RED : 사용할 수 없는 VLAN(" + $('#red_vlan_value').val() + ")입니다.",
				buttons : '[닫기]'
			});
			checkers = false;
		}

		for (var i=0; i<$scope.green_cnt; i++) {
			if (checkers) {
				if ($.inArray($('#green_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					$.SmartMessageBox({
						title : "확인",
						content : "GREEN : 사용할 수 없는 VLAN(" + $('#green_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		for (var i=0; i<$scope.orange_cnt; i++) {
			if (checkers) {
				if ($.inArray($('#orange_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					$.SmartMessageBox({
						title : "확인",
						content : "ORANGE : 사용할 수 없는 VLAN(" + $('#orange_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		for (var i=0; i<$scope.blue_cnt; i++) {
			if (checkers) {
				if ($.inArray($('#blue_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					$.SmartMessageBox({
						title : "확인",
						content : "BLUE : 사용할 수 없는 VLAN(" + $('#blue_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		if ($('#dns1').val() != null && $('#dns1').val() != '') {
			if (checkers) {
				if	(!ip_check.test($('#dns1').val())) {
					$.SmartMessageBox({
						title : "확인",
						content : "DNS1 : 사용할 수 없는 IP(" + $('#dns1').val() + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		if ($('#dns1').val() != null && $('#dns2').val() != '') {
			if (checkers) {
				if	(!ip_check.test($('#dns2').val())) {
					$.SmartMessageBox({
						title : "확인",
						content : "DNS2 : 사용할 수 없는 IP(" + $('#dns2').val() + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		if ($scope.eth1_gateway.value == null || $scope.eth1_gateway.value == '') {
			$.SmartMessageBox({
				title : "확인",
				content : "Default Gateway : IP를 입력해 주세요.",
				buttons : '[닫기]'
			});
			checkers = false;
		}

		if ($scope.eth1_gateway.value != null && $scope.eth1_gateway.value != '') {
			if (checkers) {
				if	(!ip_check.test($scope.eth1_gateway.value)) {
					$.SmartMessageBox({
						title : "확인",
						content : "Default Gateway : 사용할 수 없는 IP(" + $scope.eth1_gateway.value + ")입니다.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		for (var i=0; i<$scope.eth1_cidr_start.length; i++) {
			if (checkers) {
				if ($scope.eth1_cidr_start[i] != null && $scope.eth1_cidr_start[i] != '') {
					if	(!ip_check.test($scope.eth1_cidr_start[i])) {
						$.SmartMessageBox({
							title : "확인",
							content : "RED IP #" + (i+1) + " : 사용할 수 없는 IP(" + $scope.eth1_cidr_start[i] + ")입니다.",
							buttons : '[닫기]'
						});
						checkers = false;
					}
				}

				if ($scope.eth1_cidr_start[i] == null || $scope.eth1_cidr_start[i] == '') {
					$.SmartMessageBox({
						title : "확인",
						content : "RED IP #" + (i+1) + " : IP를 입력해 주세요.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		for (var i=0; i<$scope.green_cidr_cnt; i++) {
			if (checkers) {
				if ($scope.green_ip[i] != null && $scope.green_ip[i] != '') {
					if	(!ip_check.test($('#green_ip'+i).val())) {
						$.SmartMessageBox({
							title : "확인",
							content : "GREEN IP #" + (i+1) + " : 사용할 수 없는 IP(" + $('#green_ip'+i).val() + ")입니다.",
							buttons : '[닫기]'
						});
						checkers = false;
					}
				}

				if ($scope.green_ip[i] == null || $scope.green_ip[i] == '') {
					$.SmartMessageBox({
						title : "확인",
						content : "GREEN IP #" + (i+1) + " : IP를 입력해 주세요.",
						buttons : '[닫기]'
					});
					checkers = false;
				}
			}
		}

		if ($scope.orange_cnt > 0) {
			for (var i=0; i<$scope.orange_cidr_cnt; i++) {
				if (checkers) {
					if ($scope.orange_ip[i] != null && $scope.orange_ip[i] != '') {
						if	(!ip_check.test($('#orange_ip'+i).val())) {
							$.SmartMessageBox({
								title : "확인",
								content : "ORANGE IP #" + (i+1) + " : 사용할 수 없는 IP(" + $('#orange_ip'+i).val() + ")입니다.",
								buttons : '[닫기]'
							});
							checkers = false;
						}
					}

					if ($scope.orange_ip[i] == null || $scope.orange_ip[i] == '') {
						$.SmartMessageBox({
							title : "확인",
							content : "ORANGE IP #" + (i+1) + " : IP를 입력해 주세요.",
							buttons : '[닫기]'
						});
						checkers = false;
					}
				}
			}
		}

		if ($scope.blue_cnt > 0) {
			for (var i=0; i<$scope.blue_cidr_cnt; i++) {
				if (checkers) {
					if ($scope.blue_ip[i] != null && $scope.blue_ip[i] != '') {
						if	(!ip_check.test($('#blue_ip'+i).val())) {
							$.SmartMessageBox({
								title : "확인",
								content : "BLUE IP #" + (i+1) + " : 사용할 수 없는 IP(" + $('#blue_ip'+i).val() + ")입니다.",
								buttons : '[닫기]'
							});
							checkers = false;
						}
					}

					if ($scope.blue_ip[i] == null || $scope.blue_ip[i] == '') {
						$.SmartMessageBox({
							title : "확인",
							content : "BLUE IP #" + (i+1) + " : IP를 입력해 주세요.",
							buttons : '[닫기]'
						});
						checkers = false;
					}
				}
			}
		}

		$scope.templateSlide = false;
		$scope.templateDetail = false;
		$scope.fixDetail = false;
	};

	// Switch, Server 상세페이지 정의
    $scope.fixDetailList = function(){
    	$scope.fixFieldList = [];
    	for(var i=0; i<$scope.fixField.length; i++){
    		if(i == 0){
    			$scope.fixFieldList.push(
	    			{
	    				neclsseq : $scope.fixField[i].neclsseq,
	    				necatseq : $scope.fixField[i].necatseq,
	    				subname : $scope.fixList[0].subname,
	    				servicename : $scope.fixField[i].service_name,
	    				item : $scope.fixField[i].config_props.application.service[0].item
	    			}
	    		);
    		} else {
    			$scope.fixFieldList.push(
	    			{
	    				neclsseq : $scope.fixField[i].neclsseq,
	    				necatseq : $scope.fixField[i].necatseq,
	    				subname : $scope.fixList[1].subname,
	    				servicename : $scope.fixField[i].service_name,
	    				item : $scope.fixField[i].config_props.application.service[0].item
	    			}
	    		);
    		}
    	}
    	for(var i=0; i<$scope.fixFieldList.length; i++){
    		if($scope.fixFieldList[i].neclsseq == '13'){
    			$scope.fixDetailItem = $scope.fixFieldList[i].item;
    			$scope.fixSubname = $scope.fixFieldList[i].subname;
    		}
    	}
    	$scope.$apply();
    };


    // Switch, Server 상세페이지 버튼
    $scope.fixDetailBtn = function(neclsseq){
    	$scope.fixDetail = true;
    	if(neclsseq == "13"){
    		$scope.css = "max-width: 540px; top: 72px; left: 15px; display: block; z-index:9999;";
    		$scope.modalBodyCss = "overflow-y: scroll; width: 100%; height: 240px; padding: 4px; border: 1px solid #eee;";
    	} else {
    		$scope.css = "max-width: 540px; top: 220px; left: 328px; display: block;";
    		$scope.modalBodyCss = "width: 100%; height: 150px; padding: 4px; border: 1px solid #eee;";
    	}
    	for(var i=0; i<$scope.fixFieldList.length; i++){
    		if($scope.fixFieldList[i].neclsseq == neclsseq){
    			$scope.fixDetailItem = $scope.fixFieldList[i].item;
    			$scope.fixSubname = $scope.fixFieldList[i].subname;
    		}
    	}
    };

 // provisioning 진행 버튼
	$scope.requestProvisioning = function() {
		$scope.apps = [];
		$scope.items = [];
		$scope.services = [];
		$scope.provisions = [];
		$scope.appProvisions = [];
		$scope.tempProvisionseq = $scope.provisionseq;
		var num = "0";
		var provisionSeq = 0;
		var orgSeq = 0;
		var templateCnt = 0;
		var serviceCnt = 0;

		var vlan_checker = true;
		$scope.vlans = [];
		for (var i=0; i<$scope.vlanList.length; i++) {
			$scope.vlans.push($scope.vlanList[i].vlan);
		}
		testVlanList = $scope.vlans;

		if (vlan_checker) {
			if ($.inArray($('#red_vlan_value').val()*1, $scope.vlans) < 0) {
				vlan_checker = false;
				$.SmartMessageBox({
					title : "Provisioning을 진행 할 수가 없습니다",
					content : "RED : 사용할 수 없는 VLAN(" + $('#red_vlan_value').val() + ")입니다.",
					buttons : '[닫기]'
				});
			}
		}

		for (var i=0; i<$scope.green_cnt; i++) {
			if (vlan_checker) {
				if ($.inArray($('#green_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					vlan_checker = false;
					$.SmartMessageBox({
						title : "Provisioning을 진행 할 수가 없습니다",
						content : "GREEN : 사용할 수 없는 VLAN(" + $('#green_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
				}
			}
		}

		for (var i=0; i<$scope.orange_cnt; i++) {
			if (vlan_checker) {
				if ($.inArray($('#orange_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					vlan_checker = false;
					$.SmartMessageBox({
						title : "Provisioning을 진행 할 수가 없습니다",
						content : "ORANGE : 사용할 수 없는 VLAN(" + $('#orange_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
				}
			}
		}

		for (var i=0; i<$scope.blue_cnt; i++) {
			if (vlan_checker) {
				if ($.inArray($('#blue_vlan_value'+i).val()*1, $scope.vlans) < 0) {
					vlan_checker = false;
					$.SmartMessageBox({
						title : "Provisioning을 진행 할 수가 없습니다",
						content : "BLUE : 사용할 수 없는 VLAN(" + $('#blue_vlan_value'+i).val() + ")입니다.",
						buttons : '[닫기]'
					});
				}
			}
		}

		if (!vlan_checker) {
		} else if($scope.orgName == ""){
			$.SmartMessageBox({
				title : "Provisioning을 진행 할 수가 없습니다",
				content : "국사를 선택하여 주시기 바랍니다.",
				buttons : '[닫기]'
			});
		} else if($scope.users == "0"){
			$.SmartMessageBox({
				title : "Provisioning을 진행 할 수가 없습니다",
				content : "사용자 ID를 선택해 주시기 바랍니다.",
				buttons : '[닫기]'
			});
		} else {
			var num_check=/^[0-9]*$/;
			var ip_check=/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;
			$scope.is_value = true;

			if ($('#dns1').val() != null && $('#dns1').val() != '') {
				if	(!ip_check.test($('#dns1').val())) {
					$scope.is_value = false;
				}
			}

			if ($('#dns2').val() != null && $('#dns2').val() != '') {
				if	(!ip_check.test($('#dns2').val())) {
					$scope.is_value = false;
				}
			}

			if ($scope.eth1_vlan.value == null || $scope.eth1_vlan.value == '') {
				$scope.is_value = false;
			}

			if	(!num_check.test($scope.eth1_vlan.value)) {
				$scope.is_value = false;
			}

			if ($scope.eth1_gateway.value == null || $scope.eth1_gateway.value == '') {
				$scope.is_value = false;
			}

			if	(!ip_check.test($scope.eth1_gateway.value)) {
				$scope.is_value = false;
			}

			for (var i=0; i<$scope.eth1_cidr_start.length; i++) {
				if ($scope.eth1_cidr_start[i] == null || $scope.eth1_cidr_start[i] == '') {
					$scope.is_value = false;
				}

				if	(!ip_check.test($scope.eth1_cidr_start[i])) {
					$scope.is_value = false;
				}

				if ($scope.eth1_cidr_end[i] == null || $scope.eth1_cidr_end[i] == '') {
					$scope.is_value = false;
				}

				if	(!num_check.test($scope.eth1_cidr_end[i])) {
					$scope.is_value = false;
				}
			}

			for (var i=0; i<$scope.green_cnt; i++) {
				if ($scope.green_vlans[i] == null || $scope.green_vlans[i] == '') {
					$scope.is_value = false;
				}

				if	(!num_check.test($scope.green_vlans[i])) {
					$scope.is_value = false;
				}
			}

			for (var i=0; i<$scope.green_cidr_cnt; i++) {
				if ($scope.green_ip[i] == null || $scope.green_ip[i] == '') {
					$scope.is_value = false;
				}

				if	(!ip_check.test($scope.green_ip[i])) {
					$scope.is_value = false;
				}

				if ($scope.green_cidr[i] == null || $scope.green_cidr[i] == '') {
					$scope.is_value = false;
				}

				if	(!num_check.test($scope.green_cidr[i])) {
					$scope.is_value = false;
				}
			}

			if ($scope.orange_cnt > 0) {
				for (var i=0; i<$scope.orange_cnt; i++) {
					if ($scope.orange_vlans[i] == null || $scope.orange_vlans[i] == '') {
						$scope.is_value = false;
					}

					if	(!num_check.test($scope.orange_vlans[i])) {
						$scope.is_value = false;
					}
				}

				for (var i=0; i<$scope.orange_cidr_cnt; i++) {
					if ($scope.orange_ip[i] == null || $scope.orange_ip[i] == '') {
						$scope.is_value = false;
					}

					if	(!ip_check.test($scope.orange_ip[i])) {
						$scope.is_value = false;
					}

					if ($scope.orange_cidr[i] == null || $scope.orange_cidr[i] == '') {
						$scope.is_value = false;
					}

					if	(!num_check.test($scope.orange_cidr[i])) {
						$scope.is_value = false;
					}
				}
			}

			if ($scope.blue_cnt > 0) {
				for (var i=0; i<$scope.blue_cnt; i++) {
					if ($scope.blue_vlans[i] == null || $scope.blue_vlans[i] == '') {
						$scope.is_value = false;
					}

					if	(!num_check.test($scope.blue_vlans[i])) {
						$scope.is_value = false;
					}
				}

				for (var i=0; i<$scope.blue_cidr_cnt; i++) {
					if ($scope.blue_ip[i] == null || $scope.blue_ip[i] == '') {
						$scope.is_value = false;
					}

					if	(!ip_check.test($scope.blue_ip[i])) {
						$scope.is_value = false;
					}

					if ($scope.blue_cidr[i] == null || $scope.blue_cidr[i] == '') {
						$scope.is_value = false;
					}

					if	(!num_check.test($scope.blue_cidr[i])) {
						$scope.is_value = false;
					}
				}
			}

			if ($scope.is_value) {
				if ($('#dns1').val() == null || $('#dns1').val() == '') {
					$('#dns1').val('168.126.63.1');
					$('#dns1_0').val('168.126.63.1');
				}

				if ($('#dns2').val() == null || $('#dns2').val() == '') {
					$('#dns2').val('168.126.63.2');
					$('#dns2_0').val('168.126.63.2');
				}
				$scope.provisionBtn = false;
				for(var i=0; i<$scope.eth1_cidr.value.length; i++) {

					var value =$scope.eth1_cidr_start[i] + '/' + $scope.eth1_cidr_end[i];
					$scope.values.push(
						value
					);
				}
				for(var i=0; i<$scope.eth1_cidrs.length; i++){
					if($("#eth1_cidrs"+i).val().length != 0){
						$scope.values.push(
							$("#eth1_cidrs"+i).val() + '/' + $("#eth1_cidrs_end"+i).val()
						);
					}
				}
				$scope.subnetcidr = {
					display_name : $scope.eth1_cidr.display_name,
					name : $scope.eth1_cidr.name,
					value : $scope.values
				}

				$scope.eth1_vlan.type = "인터넷";

				$scope.eth1_vlan.value = $scope.eth1_vlan.value;

				$scope.eth1 = {
					vlan : $scope.eth1_vlan,
					defaultgateway : $scope.eth1_gateway,
					subnetcidr : $scope.subnetcidr
				};
				$scope.vlan = [];
				$scope.subnetcidr = [];
				$scope.green_values = [];
				$scope.orange_values = [];
				$scope.blue_values = [];

				for(var i=0; i<$scope.green_cidr_cnt; i++){
					$scope.green_values.push(
						$("#green_ip"+i).val() + '/' + $("#green_cidr"+i).val()
					);
				}

				for(var i=0; i<$scope.orange_cidr_cnt; i++){
					$scope.orange_values.push(
						$("#orange_ip"+i).val() + '/' + $("#orange_cidr"+i).val()
					);
				}

				for(var i=0; i<$scope.blue_cidr_cnt; i++){
					$scope.blue_values.push(
						$("#blue_ip"+i).val() + '/' + $("#blue_cidr"+i).val()
					);
				}

				$scope.subnetcidr = {
					display_name : $scope.eth2_cidr.display_name,
					name : $scope.eth2_cidr.name,
					value : $scope.green_values
				}

				$scope.eth2_vlan.type = $('#green_type').val();

				$scope.eth2_vlan.value = $("#green_vlan_value0").val() + "";

				$scope.eth2 = {
					vlan : $scope.eth2_vlan,
					subnetcidr : $scope.subnetcidr
				};



				$scope.vlan = [];
				$scope.subnetcidr = [];

				if ($scope.green_cnt > 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth3_cidr.display_name,
						name : $scope.eth3_cidr.name,
						value : $scope.green_values
					}
					$scope.eth3_vlan.type = $('#green_type').val();
					$scope.eth3_vlan.value = $("#green_vlan_value1").val() + "";
				} else if ($scope.orange_cnt > 0) {
					$scope.subnetcidr = {
						display_name : $scope.eth3_cidr.display_name,
						name : $scope.eth3_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth3_vlan.type = $('#orange_type').val();
					$scope.eth3_vlan.value = $("#orange_vlan_value0").val() + "";
				} else if ($scope.blue_cnt > 0) {
					$scope.subnetcidr = {
						display_name : $scope.eth3_cidr.display_name,
						name : $scope.eth3_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth3_vlan.type = $('#blue_type').val();
					$scope.eth3_vlan.value = $("#blue_vlan_value0").val() + "";
				} else {
					$scope.eth3_vlan = '';
				}

				$scope.eth3 = {
					vlan : $scope.eth3_vlan,
					subnetcidr : $scope.subnetcidr
				};

				$scope.vlan = [];
				$scope.subnetcidr = [];

				if ($scope.green_cnt > 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.green_values
					}
					$scope.eth4_vlan.type = $('#green_type').val();
					$scope.eth4_vlan.value = $("#green_vlan_value2").val() + "";
				} else if ($scope.green_cnt == 2 && $scope.orange_cnt > 0) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth4_vlan.type = $('#orange_type').val();
					$scope.eth4_vlan.value = $("#orange_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 2 && $scope.blue_cnt > 0) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth4_vlan.type = $('#blue_type').val();
					$scope.eth4_vlan.value = $("#blue_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt > 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth4_vlan.type = $('#orange_type').val();
					$scope.eth4_vlan.value = $("#orange_vlan_value1").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt > 0) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth4_vlan.type = $('#blue_type').val();
					$scope.eth4_vlan.value = $("#blue_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt < 1 && $scope.blue_cnt > 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth4_cidr.display_name,
						name : $scope.eth4_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth4_vlan.type = $('#blue_type').val();
					$scope.eth4_vlan.value = $("#blue_vlan_value1").val() + "";
				} else {
					$scope.eth4_vlan = '';
				}

				$scope.eth4 = {
					vlan : $scope.eth4_vlan,
					subnetcidr : $scope.subnetcidr
				};

				$scope.vlan = [];
				$scope.subnetcidr = [];

				if ($scope.green_cnt == 4) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.green_values
					}
					$scope.eth5_vlan.type = $('#green_type').val();
					$scope.eth5_vlan.value = $("#green_vlan_value3").val() + "";
				} else if ($scope.green_cnt == 3 && $scope.orange_cnt == 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth5_vlan.type = $('#orange_type').val();
					$scope.eth5_vlan.value = $("#orange_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 3 && $scope.blue_cnt == 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth5_vlan.type = $('#orange_type').val();
					$scope.eth5_vlan.value = $("#orange_vlan_value1").val() + "";
				} else if ($scope.green_cnt == 2 && $scope.blue_cnt == 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value1").val() + "";
				} else if ($scope.green_cnt == 2 && $scope.orange_cnt == 1 && $scope.blue_cnt == 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt > 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.orange_values
					}
					$scope.eth5_vlan.type = $('#orange_type').val();
					$scope.eth5_vlan.value = $("#orange_vlan_value2").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 2 && $scope.blue_cnt == 1) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value0").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt == 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value1").val() + "";
				} else if ($scope.green_cnt == 1 && $scope.orange_cnt < 1 && $scope.blue_cnt > 2) {
					$scope.subnetcidr = {
						display_name : $scope.eth5_cidr.display_name,
						name : $scope.eth5_cidr.name,
						value : $scope.blue_values
					}
					$scope.eth5_vlan.type = $('#blue_type').val();
					$scope.eth5_vlan.value = $("#blue_vlan_value2").val() + "";
				} else {
					$scope.eth5_vlan = '';
				}

				$scope.eth5 = {
					vlan : $scope.eth5_vlan,
					subnetcidr : $scope.subnetcidr
				};

				$scope.ethernet.push({
					eth1 : $scope.eth1
				});

				if ($scope.eth2_vlan != null && $scope.eth2_vlan != '') {
					$scope.ethernet.push({
						eth2 : $scope.eth2
					});
				}

				if ($scope.eth3_vlan != null && $scope.eth3_vlan != '') {
					$scope.ethernet.push({
						eth3 : $scope.eth3
					});
				}

				if ($scope.eth4_vlan != null && $scope.eth4_vlan != '') {
					$scope.ethernet.push({
						eth4 : $scope.eth4
					});
				}

				if ($scope.eth5_vlan != null && $scope.eth5_vlan != '') {
					$scope.ethernet.push({
						eth5 : $scope.eth5
					});
				}

				$scope.items.push({
					ethernet : $scope.ethernet
				});

				$scope.values = [];
				$scope.proxys = [];

				$scope.values.push(
					'eth2'
				);

				if ($scope.green_cnt > 1) {
					$scope.values.push(
						'eth3'
					);
				}

				if ($scope.green_cnt > 2) {
					$scope.values.push(
						'eth4'
					);
				}

				if ($scope.green_cnt > 3) {
					$scope.values.push(
						'eth5'
					);
				}

				if ($("input:checkbox[id='br0_proxys0']").is(":checked")) {
					$scope.proxys.push(
						't'
					);
				}

				$scope.br0 = {
					display_name : $scope.br0.display_name,
					name : $scope.br0.name,
					value : $scope.values,
					proxy_arp : $scope.proxys
				}
				$scope.bridge.push({
					br0 : $scope.br0
				});

				$scope.values = [];
				$scope.proxys = [];

				if ($scope.green_cnt == 1 && $scope.orange_cnt > 0) {
					$scope.values.push(
						'eth3'
					);
				}

				if (($scope.green_cnt == 2 && $scope.orange_cnt > 0) || ($scope.green_cnt == 1 && $scope.orange_cnt > 1)) {
					$scope.values.push(
						'eth4'
					);
				}

				if (($scope.green_cnt == 3 && $scope.orange_cnt > 0) || ($scope.green_cnt == 2 && $scope.orange_cnt > 1) || ($scope.green_cnt == 1 && $scope.orange_cnt > 2)) {
					$scope.values.push(
						'eth5'
					);
				}

				if ($("input:checkbox[id='br1_proxys0']").is(":checked")) {
					$scope.proxys.push(
						't'
					);
				}

				$scope.br1 = {
					display_name : $scope.br1.display_name,
					name : $scope.br1.name,
					value : $scope.values,
					proxy_arp : $scope.proxys
				}
				$scope.bridge.push({
					br1 : $scope.br1
				});
				$scope.values = [];
				$scope.proxys = [];

				if ($scope.green_cnt == 1 && $scope.orange_cnt < 1 && $scope.blue_cnt > 0) {
					$scope.values.push(
						'eth3'
					);
				}

				if (($scope.green_cnt == 2 && $scope.orange_cnt < 1 && $scope.blue_cnt > 0) || ($scope.green_cnt == 1 && $scope.orange_cnt < 1 && $scope.blue_cnt > 1) || ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt > 0)) {
					$scope.values.push(
						'eth4'
					);
				}

				if (($scope.green_cnt == 3 && $scope.orange_cnt < 1 && $scope.blue_cnt > 0) || ($scope.green_cnt == 2 && $scope.orange_cnt == 1 && $scope.blue_cnt > 0) || ($scope.green_cnt == 2 && $scope.orange_cnt < 1 && $scope.blue_cnt > 1) || ($scope.green_cnt == 1 && $scope.orange_cnt == 2 && $scope.blue_cnt > 0) || ($scope.green_cnt == 1 && $scope.orange_cnt == 1 && $scope.blue_cnt > 1) || ($scope.green_cnt == 1 && $scope.orange_cnt < 1 && $scope.blue_cnt > 2)) {
					$scope.values.push(
						'eth5'
					);
				}

				if ($("input:checkbox[id='br2_proxys0']").is(":checked")) {
					$scope.proxys.push(
						't'
					);
				}

				$scope.br2 = {
					display_name : $scope.br2.display_name,
					name : $scope.br2.name,
					value : $scope.values,
					proxy_arp : $scope.proxys
				}
				$scope.bridge.push({
					br2 : $scope.br2
				});
				$scope.values = ["on,,10.0.0.0/24,192.168.10.1,test_routing,,,,,,,"];
				for(var i=0; i<$scope.router.value.length; i++){
					var value = $scope.router.value[i];
					if (value != null && value != '') {
						$scope.values.push(
							value
						);
					}
				}
				for(var i=0; i<$scope.routers.length; i++){
					if($("#routers"+i).val().length != 0){
						$scope.values.push(
							$("#routers"+i).val()
						);
					}
				}
				$scope.temp_router = {
					display_name : $scope.router.display_name,
					name : $scope.router.name,
					value : $scope.values
				}
				$scope.items.push({
					bridge : $scope.bridge
				});

				$scope.items.push({
					router : $scope.temp_router
				});
				$scope.services.push({
					name : $scope.catalname,
					item : $scope.items
				});

				if ($("input:checkbox[id='is_backup']").is(":checked")) {
					$scope.apps.push({
						backuptype : 'active',
						name : $scope.catalname,
						applicationid : $scope.uuidGenerator(),
						neclsseq : parseInt($scope.neclsseq),
						necatseq : parseInt($scope.necatseq),
						service : $scope.services
					});

					$scope.apps.push({
						backuptype : 'standby',
						name : $scope.catalname,
						applicationid : $scope.uuidGenerator(),
						neclsseq : parseInt($scope.neclsseq),
						necatseq : parseInt($scope.necatseq),
						service : $scope.services
					});
				} else {
					$scope.apps.push({
						name : $scope.catalname,
						applicationid : $scope.uuidGenerator(),
						neclsseq : parseInt($scope.neclsseq),
						necatseq : parseInt($scope.necatseq),
						service : $scope.services
					});
				}

//				console.log($scope.apps);

				//httpProvisioning();

				var is_backup = 'f';

				if ($("input:checkbox[id='is_backup']").is(":checked")) {
					is_backup = 't';
				}

				$.ajax({
			        type:'post',
			        data: {
			        	orgseq : $scope.orgSeq,
						userid : $scope.users,
						templateseq : $scope.templateseq,
						is_backup : is_backup,
			        	_xsrf : getCookie("_xsrf")
			        	},
			        dataType:'json',
			        url: '/provisioning/provisions/checkuser',
			        async : true,
			        success: function(data) {
			        	if (data.result == 'success') {
			        		$scope.readyApp();
			        	}
			        }, error: function(jqXHR, textStatus, errorThrown) {
			        	$.smallBox({
					        title : "생성 실패",
					        content : "Provisioning 생성 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 5000
					    });
			            $scope.provisionBtn = true;
			            $scope.$apply();
			        }
			    });
			} else {
				$.SmartMessageBox({
					title : "Provisioning을 진행 할 수가 없습니다",
					content : "설정값을 확인해 주시기 바랍니다.",
					buttons : '[닫기]'
				});
			}

		}
	};

	$scope.readyApp = function() {
		$scope.templateseq = 10;
		if($scope.provisionseq == "0"){
			var data = {
					orgseq : parseInt($scope.orgSeq),
					userid : $scope.users,
					templateseq : parseInt($scope.templateseq)
			}
			//console.log("seq : "+JSON.stringify(data));
			$.ajax({
		        type:'post',
		        data: {data : JSON.stringify(data), _xsrf : getCookie("_xsrf")},
		        dataType:'json',
		        url: '/provisioning/provisions/readyApp',
		        async : true,
		        success: function(data) {
		        	if(data.response.result=="success"){
						$scope.getWorkingData(data.response.provisionseq);
						$scope.provisionseq = data.response.provisionseq;
						var winLog = window.open("/monitor/e2etrace?comm=2&seq="+$scope.provisionseq,"provisionTrace",'width=1040, height=900, left=600, top=50,' +
												'scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
						//console.log("provisionseq 생성 성공");
						$timeout(function(){
							httpProvisioning();
						},1000);
					} else if(data.response.result=="fail"){
						$.smallBox({
					        title : "생성 실패",
					        content : "Provisioning 생성 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 5000
					    });
					}
		        	$scope.provisionBtn = true;
		        	$scope.$apply();
		        }, error: function(jqXHR, textStatus, errorThrown) {
		        	$.smallBox({
				        title : "생성 실패",
				        content : "Provisioning 생성 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 5000
				    });
		            $scope.provisionBtn = true;
		            $scope.$apply();
		        }
			});
		}
	}

	function httpProvisioning(){
		var vlans = { vlan_1 : $scope.eth1_vlan, vlan_2 : $scope.eth2_vlan, vlan_3 : $scope.eth3_vlan, vlan_4 : $scope.eth4_vlan, vlan_5 : $scope.eth4_vlan };
		var lang_check = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/;
		var hostnames = $('#hostname').val();
		var domainnames = $('#domainname').val();

		if (lang_check.test(hostnames)) {
			hostnames = '';
		}

		if (lang_check.test(domainnames)) {
			domainnames = '';
		}
		$scope.templateseq = 10;
		$scope.provisionBtn = false;
		$scope.provisions = {
				templateseq : parseInt($scope.templateseq),
				provisionseq : parseInt($scope.provisionseq),
				orgseq : parseInt($scope.orgSeq),
				userid : $scope.users,
				application : $scope.apps,
				hostname : hostnames,
				domainname : domainnames,
				dns1 : $('#dns1').val(),
				dns2 :$('#dns2').val()
		}
		$scope.appProvisions = {
				provision : $scope.provisions
		};
//		console.log(JSON.stringify($scope.appProvisions));
		$.ajax({
	        type:'post',
	        data: {
	        	appProvisions : JSON.stringify($scope.appProvisions), user : $scope.users, vlans : JSON.stringify(vlans), _xsrf : getCookie("_xsrf")
	        },
	        dataType:'json',
	        async : true,
	        url: '/provisioning/provisions/createApp',
	        success: function(data) {
	        	if(data.response.result == "success"){
					$scope.provisionseq = data.response.provisionseq;
					//console.log(JSON.stringify(data.response.provisionseq));
					$("#template_select").addClass('disable');

				} else if(data.response.result=="fail"){
					$.smallBox({
				        title : "생성 실패",
				        content : "Provisioning 생성 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 5000
				    });
					$scope.provisionBtn = true;
				}
	        	$scope.$apply();
	        }, error: function(jqXHR, textStatus, errorThrown) {
	            alert("provision 실패");
	            $scope.provisionBtn = true;
	        }
		});
	};

	$scope.getWorkingData = function(provisionseq){
		var num = 0;
		var tempLog = "";
		//console.log("pseq  :  "+provisionseq);
		timer = setInterval( function () {
		//----------------------------------------------------------------------------------
			$.ajax({
		        type:'post',
		        data: {provisionseq : provisionseq, _xsrf : getCookie("_xsrf")},
		        dataType:'json',
		        url: '/provisioning/provisions/step',
		        success: function(data) {
		        	$scope.neprocstatus = JSON.parse(data.step);
		        	//console.log("data : "+JSON.stringify(data.step));
					//$scope.progressWidth = [];
		        	if ($scope.neprocstatus.length > 0) {
						var step = $scope.neprocstatus[0].step;
						if (step == 999){
							$scope.consoleLog.push({
								message : $scope.neprocstatus[0].step_message,
								step : "fail"
								});
							provisionProgress(step);
							$scope.provisionBtn = true;
							$scope.provisionDelBtn = false;
							if($scope.tempProvisionseq != $scope.provisionseq){
								$scope.provisionseq = "0";
							}
							step++;
							clearInterval(timer);
							$.smallBox({
						        title : "생성 실패",
						        content : "Provisioning 생성 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
						        color : "#5384AF",
						        timeout: 5000
						    });
							$('#log').animate({
								scrollTop: 9999
							}, 800);
							$scope.$apply();

						} else if ($scope.neprocstatus[0].step_message != tempLog && step < 7){
							$scope.consoleLog.push({
								message : $scope.neprocstatus[0].step_message,
								step : $scope.count++
								});
							tempLog = $scope.neprocstatus[0].step_message;
							provisionProgress(step);
							$('#log').animate({
								scrollTop: 9999
							}, 800);
							$scope.$apply();
						} else if (step == 7){
							provisionProgress(step);
							$scope.consoleLog.push({
								message : $scope.neprocstatus[0].step_message,
								step : "succ"
							});
							$('#log').animate({
								scrollTop: 9999
							}, 800);
							$scope.provisionBtn = false;
							$scope.provisionDelBtn = true;
							clearInterval(timer);
							$scope.$apply();
						} else if (step == 55) {
							$("input:checkbox[id='template0']").attr("checked", false);
							provisionProgress(step);
							$scope.consoleLog.push({
								message : $scope.neprocstatus[0].step_message,
								step : "delSucc"
							});
							$('#log').animate({
								scrollTop: 9999
							}, 800);
							$.smallBox({
						        title : "삭제 완료",
						        content : "Provisioning 삭제 완료 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
						        color : "#5384AF",
						        timeout: 5000
						    });
							$scope.provisionBtn = false;
							$scope.provisionseq = 0;
							clearInterval(timer);
							$scope.$apply();
						} else if ($scope.neprocstatus[0].step_message != tempLog && step > 50){
							$scope.consoleLog.push({
								message : $scope.neprocstatus[0].step_message,
								step : $scope.count++
							});
							tempLog = $scope.neprocstatus[0].step_message;
							provisionProgress(step);
							$('#log').animate({
								scrollTop: 9999
							}, 800);
							$scope.$apply();
						}
		        	}
		        }, error: function(jqXHR, textStatus, errorThrown) {
		            alert("provision progressbar 실패");
		            clearInterval(timer);
		        }
			});
		}, 1500); // 1초에 한번씩 받아온다.
	};
	//console.log(JSON.stringify($scope.appAttribut));
	function provisionProgress(step){
		var temAttribut = [];
		var progressWidth = "";
		var progressbar = "";
		var urlbtn = "";
		var consoleUrl = "";
		var iconsrc = "";
		var selected = "";
		for(var i=0; i<$scope.poolList.length; i++){

			//step 1~7 프로비저닝 생성 , step 8~14 는 삭제
			if(step == 7){
				progressWidth = "100";
				progressbar = false;
				urlbtn = true;
				consoleUrl = $scope.consoleUrl;
				iconsrc = $scope.greenColor;
				progressClass = $scope.createBar;
				selected = $scope.appAttribut[i].selected;
				item = $scope.appAttribut[i].item;
			} else if(step == 999) {
				progressWidth = step * 0;
				progressbar = true;
				urlbtn = false;
				consoleUrl = $scope.appAttribut[i].consoleUrl;
				iconsrc = $scope.appAttribut[i].iconsrc;
				progressClass = $scope.createBar;
				selected = $scope.appAttribut[i].selected;
				item = $scope.appAttribut[i].item;
			} else if(step < 7){
				progressWidth = step * 15;
				progressbar = true;
				urlbtn = false;
				consoleUrl = $scope.appAttribut[i].consoleUrl;
				iconsrc = $scope.appAttribut[i].iconsrc;
				progressClass = $scope.createBar;
				selected = $scope.appAttribut[i].selected;
				item = $scope.appAttribut[i].item;
			} else if(step == 55) {
				progressWidth = 100;
				progressbar = true;
				urlbtn = false;
				consoleUrl = "";
				iconsrc = $scope.appAttribut[i].iconsrc;
				progressClass = $scope.delBar;
				selected = false;
				item = "";
			} else if(step >= 50){
				progressWidth = (step-50) * 20;
				progressbar = true;
				urlbtn = false;
				consoleUrl = $scope.appAttribut[i].consoleUrl;
				iconsrc = $scope.appAttribut[i].iconsrc;
				progressClass = $scope.delBar;
				selected = $scope.appAttribut[i].selected;
				item = "";
			}
			if($("input:checkbox[id='template"+i+"\']").is(":checked")){
				temAttribut.push(
					{
						subname : $scope.poolList[i].subname,
						imagesrc : $scope.appAttribut[i].imagesrc,
						servicename : $scope.appAttribut[i].servicename,
						iconsrc : iconsrc,
						progressWidth : progressWidth,
						progressbar : progressbar,
						urlbtn : urlbtn,
						neclsseq : $scope.appAttribut[i].neclsseq,
						progressClass : progressClass,
						necatseq : $scope.appAttribut[i].necatseq,
						selected : selected,
						item : item,
						consoleUrl : consoleUrl,
						procstatecode : $scope.appAttribut[i].procstatecode
					}
				);
			} else {
				temAttribut.push(
					{
						subname : $scope.poolList[i].subname,
						imagesrc : $scope.poolList[i].imagesrc,
						servicename : $scope.poolList[i].subname,
						iconsrc : $scope.grayColor,
						progressWidth : "0",
						progressbar : true,
						urlbtn : false,
						neclsseq : $scope.poolList[i].neclsseq,
						progressClass : $scope.createBar,
						necatseq : $scope.poolList[i].necatseq,
						selected : false,
						item : "",
						consoleUrl : "",
						procstatecode : "0"
					}
				);
			}
		}
		$timeout(function(){
			for(var j=0; j < $scope.poolList.length; j++){
				if(temAttribut[j].item == ""){
					$("#template_"+$scope.poolList[j].subname).addClass('disable');
				}
			}
		},100);
		$scope.appAttribut = temAttribut;
		if(step == 12){
			$("input:checkbox[id='template0']").attr("checked", false);
		}
	}

	$scope.provisionDelCheck = function(){
		$.SmartMessageBox({
			title : "Provisioning 해지 안내 문구 입니다.",
			content : "해지를 진행 하시겠습니까? 확인 부탁 드립니다.",
			buttons : '[취소][확인]'
		},function(ButtonPressed){
			if (ButtonPressed === "확인") {
				$scope.provisionDelBtn = false;
				provisionProgress(50);
				$scope.$apply();
				//$scope.getWorkingData($scope.provisionseq);
				$scope.provisionDel();
			}
		});
	};
	// 프로비저닝 삭제
	$scope.provisionDel = function(){
		$scope.templateDetail = false;
		$scope.provisionDelBtn = false;
		var appAtt = [];
		appAtt = $scope.appAttribut;
		$.ajax({
	        type:'post',
	        data: {provisionseq : $scope.provisionseq, user : $scope.users, _xsrf : getCookie("_xsrf")},
	        dataType:'json',
	        url: '/provisioning/provisions/deleteApp',
	        success: function(data) {
	        	$scope.getWorkingData($scope.provisionseq);
				$scope.$apply();
	        }, error: function(jqXHR, textStatus, errorThrown) {
	        	$.smallBox({
			        title : "삭제 실패",
			        content : "Provisioning 삭제 실패 하였습니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 5초후에 닫힙니다....</i>",
			        color : "#5384AF",
			        timeout: 5000
			    });
				$scope.appAttribut = appAtt;
				$scope.provisionDelBtn = true;
				//$scope.getWorkingData($scope.provisionseq);
				//provisionProgress(7);
	        }
		});
	};
	$scope.winPopup = function(){
		//var win = window.open("/provisioning/nfvProvisionLog?pseq="+$scope.provisionseq,"",'left=100, top=100, width=960, height=500, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
		var winLog = window.open("/monitor/e2etrace?comm=2&seq="+$scope.provisionseq,"provisionTrace",'width=1040, height=900, left=600, top=50,' +
		'scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	}
});

$(document).ready(function() {
	pageSetUp();
	functionMenu();
	$('#templateDetail').draggable();

	 // START AND FINISH DATE
	$("#fromdate").datepicker({
	    changeMonth: true,
	    numberOfMonths: 1,
	    dateFormat: 'yy-mm-dd',
	    prevText: '<i class="fa fa-chevron-left"></i>',
	    nextText: '<i class="fa fa-chevron-right"></i>',
	    onClose: function (selectedDate) {
	        $("#todate").datepicker("option", "minDate", selectedDate);
	    }

	});
	$("#todate").datepicker({
	    changeMonth: true,
	    numberOfMonths: 1,
	    dateFormat: 'yy-mm-dd',
	    prevText: '<i class="fa fa-chevron-left"></i>',
	    nextText: '<i class="fa fa-chevron-right"></i>',
	    onClose: function (selectedDate) {
	        $("#fromdate").datepicker("option", "maxDate", selectedDate);
	    }
	});

	function sleep(mil){
		var then,now;
		then=new Date().getTime();
		now=then;
		while((now-then)<mil){
			now=new Date().getTime();
		}
	};
});


function functionMenu(){
	$('#lp_ETS').removeClass('active');
	$('#lp_service_list').removeClass('active');
	$('#lp_service_custServer').removeClass('active');
	$('#lp_service_nfv').attr('class','active');

	$('#lp_service_ul').attr('style', 'display:block;');
	$('#wid-utm-provision-btn_connect').hide();
	$('#utmCarousel').carousel({
		pause : true,
		interval : false
	});
	$("#main").css("visibility","visible");
}
