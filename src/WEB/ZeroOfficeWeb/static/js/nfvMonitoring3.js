/**
 * 모니터링 페이지 메인 javascript
 */
var panel,
org_tree_data,
server_cust_grid_panel,
customer_grid_panel, main_panel,
hw_utm_grid_panel, infra_view_panel, section_pop_grid_panel, win, win_utm, win_net, process_grid_panel,
mgmt_vm_grid_panel, component_grid_panel, switchs_grid_panel, trace_grid_panel, rule_grid_panel, keystone_grid_panel, nova_grid_panel, neutron_grid_panel, glance_grid_panel, ect_infra_grid_panel, infras_grid_panel,
graphic_view_panel, graphic, grid, grid_view_panel, server_grid_panel, infra_grid_panel, body_panel, switchs, component, mgmt_vm, data_text='';
var selected_id, vmid, context = null, clickX=0, clickY=0, ws, ws_boolean = false, node, service, ip_addr;
var index_menu = "";
var pseq = '';
var org_array = [];
var hw_customer_grid_panel = "";
var dev_hw_customer_grid_panel = "";
var hw_utm_panel = ""
var server_farm_grid_panel = "";
var cloud_grid_panel = "";
var hw_customer_temp_panel = "";
var host_array = [];
var contextMenus = Ext.create('Ext.menu.Menu', {
	items: []
});

Ext.require([
	'Ext.form.*'
]);

// 컴포넌트 명칭을 한글화
var admin_node = {
		"nova-api"					: "노바외부연동서버",
		"nova-conductor"			: "노바DB인터페이스",
		"nova-scheduler"			: "VM할당위치관리자",
		"nova-cert"					: "인증관리",
		"nova-consoleauth"			: "콘솔인증관리",
		"nova-novncproxy"			: "외부VNC접속Proxy",
		"neutron-server"			: "뉴트론연동서버",
		"neutron-openvswitch-agent"	: "OVS관리에이전트",
		"neutron-dhcp-agent"		: "DHCP관리에이전트",
		"neutron-l3-agent"			: "L3관리에이전트",
		"neutron-metadata-agent"	: "메타데이타에이전트",
		"glance-api"				: "이미지연동서버",
		"glance-registry"			: "이미지등록서버",
		"keystone-all"				: "인증서버",
		"apache2"					: "웹서버",
		"memcached"					: "메모리캐쉬서버",
		/*"epmd"						: "메시지버스서버1",*/
		"beam.smp"					: "메시지버스서버",
		"mysqld"					: "데이터베이스",
		"dnsmasq"					: "dhcp서버",
		"ovsdb-server"				: "ovsdb서버",
		"ovs-vswitchd"				: "ovs데몬"
};

// 크로스 브라우징 방지
function getCookie(name) {
	var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
	return r ? r[1] : undefined;
}

// 장애 알림음 실행 함수
function beepSound() {
	$("#beep").trigger('play');
}

// 실행이력의 E2E Trace 팝업 생성 함수
function e2elink(seq, type) {
	if (type == '3') {
		var win = window.open("/monitor/e2etrace?seq="+seq+"&comm="+type,"e2e_ha",'width=950, height=180, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	} else if (type == '1') {
		var win = window.open("/monitor/e2etrace?seq="+seq+"&comm="+type+"&org="+s_Monitor.config.org_seq,"e2e_sync",'width=1020, height=600, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	} else {
		var win = window.open("/monitor/e2etrace?seq="+seq+"&comm="+type,"e2e",'width=950, height=600, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
	}
}

// 룰 동기화 이상 상세정보 팝업 함수
function ruleLink(seq, reg) {
	var win = window.open("/monitor/ruledetail?seq="+seq+"&rt="+reg,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
}

// UTM 세션 리스트 Grid panel 생성 함수
function section_pop_grid() {
	Ext.define('Grid_Section_Model',{
		extend: 'Ext.data.Model',
		fields: [ "section_list" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Section_Model',
		storeId : 'grid_section_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	section_pop_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'section_pop_grid',
		store: Ext.data.StoreManager.lookup('grid_section_store'),
		region: 'center',
		columns: [
			{text: "", flex: 1, dataIndex: 'section_list', sortable: true}
		],
		listeners: {
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
		},
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// UTM 세션 리스트 팝업 호출 함수
function section_pop() {
	if (!win) { // 팝업 생성 여부 판단(기존에 생성을 했는지 확인)
		win = Ext.create('widget.window', {
			title: 'UTM 세션 리스트',
			closable: true,
			closeAction: 'hide',
			constrain: true,
			width: 800,
			height: 400,
			layout: 'border',
			items: [section_pop_grid_panel]
		});
	}

	// 현재 보여지고 있는지 판단
	if (win.isVisible()) {
	} else {
		win.show(this, function() {});
	}
}

// UTM 세션 리스트 정보 조회
function section_pop_data(vm_section) {
	var section_stores = Ext.data.StoreManager.lookup('grid_section_store');
	section_stores.removeAll();

	Ext.Ajax.request({
		url : '/monitor/contrackdetail',
		type : 'POST',
		dataType : 'json',
		params : {vmid : vm_section, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			var section = JSON.parse(data.responseText).result[0][0];

			// 줄 바꿈 처리
			section = section.replace(/	  /gi, ' ');
			section = section.replace(/tcp/gi, ',tcp');
			section = section.replace(/udp/gi, ',udp');
			section = section.replace(',', '');
			section = section.split(',');

			for (var i=0; i<section.length; i++) {
				section_stores.insert(i, {
					"section_list" : '<pre>' + section[i] + '</pre>'
				});
			}
		}
	});

	section_pop();
}

// 네트워크 정보 Grid Panel 함수
function network_pop_grid() {
	Ext.define('Grid_Network_Model',{
		extend: 'Ext.data.Model',
		fields: [ "network_list" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Network_Model',
		storeId : 'grid_network_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	network_pop_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'network_pop_grid',
		store: Ext.data.StoreManager.lookup('grid_network_store'),
		region: 'center',
		columns: [
			{text: "", flex: 1, dataIndex: 'network_list', sortable: true}
		],
		listeners: {
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
		},
		viewConfig : {
			enableTextSelection: true
		}
	});
}

// 네트워크 정보 팝업 호출 함수
function network_pop() {
	if (!win_net) { // 팝업 존재 여부 확인
		win_net = Ext.create('widget.window', {
			title: '네트워크 정보',
			closable: true,
			closeAction: 'hide',
			constrain: true,
			width: 800,
			height: 400,
			layout: 'border',
			items: [network_pop_grid_panel]
		});
	}

	// 팝업이 보여지고 있는 지 확인
	if (win_net.isVisible()) {
	} else {
		win_net.show(this, function() {});
	}
}

// 네트워크 정보 조회
function network_pop_data() {
	Ext.data.StoreManager.lookup('grid_network_store').removeAll();

	Ext.Ajax.request({
		url : '/monitor/vm_network_info',
		type : 'POST',
		dataType : 'json',
		params : {data : s_Monitor.config.vm_name, seq : s_Monitor.config.org_seq, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			var vm_network_info = JSON.parse(JSON.parse(data.responseText).result);

			Ext.data.StoreManager.lookup('grid_network_store').insert(0, {
				"network_list" : '<pre style="white-space: pre;">' + JSON.stringify(vm_network_info, null, 4) + '</pre>'
			});
		}
	});

	network_pop();
}

// 현재 웹소켓 연결 상태 확인 함수 및 현재 접속 세션확인
function watch_websocket() {
	if (ws.readyState != 0 && ws.readyState != 1) { // 현재 웹소켓 상태 확인
		console.log(ws);
		console.log(new Date());
		ws.close();
		s_Monitor.websocket();
		call_org_state(s_Monitor.config.org_seq);
	}

	if (s_Monitor.config._xsrf != null && s_Monitor.config._xsrf != '') { // 현재 접속 세션확인(로그아웃 여부 및 다른 사용자 로그인 여부 확인)
		if (s_Monitor.config._xsrf != getCookie("_xsrf")) {
			location.href = location.href; // 세션이 변경되면 새로고침
		}
	}
}

// 그리드뷰 -> 고객 서비스 상태 -> UTM구동시간 계산 함수
function intervals() {
	Ext.data.StoreManager.lookup('grid_server_store').each(function(record, index) {
		// Set the unit values in milliseconds.
		var msecPerMinute = 1000 * 60;
		var msecPerHour = msecPerMinute * 60;
		var msecPerDay = msecPerHour * 24;
		var msecPerWeek = msecPerDay * 7;
		var msecPerMonth = msecPerWeek * 4;

		// Set a date and get the milliseconds
		var a_date = new Date();
		var b_date = new Date(record.data.createvm);

		// Get the difference in milliseconds.
		var interval = a_date - b_date;

		// Calculate how many days the interval contains. Subtract that
		// many days from the interval to determine the remainder.
		var months = Math.floor(interval / msecPerMonth);
		interval = interval - (months * msecPerMonth);

		var weeks = Math.floor(interval / msecPerWeek);
		interval = interval - (weeks * msecPerWeek);

		var days = Math.floor(interval / msecPerDay );
		interval = interval - (days * msecPerDay );

		// Calculate the hours, minutes, and seconds.
		var hours = Math.floor(interval / msecPerHour );
		interval = interval - (hours * msecPerHour );

		var minutes = Math.floor(interval / msecPerMinute );
		interval = interval - (minutes * msecPerMinute );

		var seconds = Math.floor(interval / 1000 );

		if (months > 0) {
			record.data.ativedt = months + " 개월 " + weeks + " 주";
		} else if (weeks > 0) {
			record.data.ativedt = weeks + " 주 " + days + " 일";
		} else if (days > 0) {
			record.data.ativedt = days + " 일 " + hours + " 시간";
		} else {
			record.data.ativedt = hours + " 시간 " + minutes + " 분";
		}
	});
}

// 페이지 준비 완료시 실행되는 함수
$(document).ready(function() {
	s_Monitor.websocket(); // 웹소켓 호출
	section_pop_grid(); // UTM 세션 리스트 팝업 초기화
	utm_rule_pop_grid(); // 원본/변환 룰 비교 팝업 초기화
	network_pop_grid(); // 네트워크 정보 팝업 초기화
	s_Monitor.config._xsrf = getCookie("_xsrf"); // 현재 세션 저장

	// 3초마다 현재 세션과 웹소켓 상태 확인
	task = Ext.TaskManager.start({
		run : watch_websocket,
		interval : 3000
	});

	// 국사 트리 조회
	$.ajax({
		url : "/monitor/orgNametree",
		type : "POST",
		dataType : "json",
		data : {data : userid, _xsrf : getCookie("_xsrf")},
		success : function(req) {
			var tree_data = [];
			for (var i=0; i<req.org.length; i++) {
				tree_data.push({
					text : '<b>' + req.org[i][1] + '</b>',
					orgname : req.org[i][1],
					expanded : false,
					org : req.org[i][0],
					children: [
							   { text : '고객 서비스 상태', leaf : true, org : req.org[i][0], index_text : "cust", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
							   { text : '인프라 동작 상태', expanded : false, org : req.org[i][0], index_text : "infra", orgname : req.org[i][1],
								   children: [
											  { text : '인증 서비스', leaf : true, org : req.org[i][0], index_text : "keystone", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
											  { text : '컴퓨팅 서비스', leaf : true, org : req.org[i][0], index_text : "nova", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
											  { text : '네트워킹 서비스', leaf : true, org : req.org[i][0], index_text : "neutron", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
											  { text : '이미지 서비스', leaf : true, org : req.org[i][0], index_text : "glance", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
											  { text : '서비스 지원 인프라', leaf : true, org : req.org[i][0], index_text : "etc", orgname : req.org[i][1], icon : '../static/img/stitle_icon.png' },
											  ]},
							  ]
				});
			}
			org_tree_data = { text : '전체', expanded : true, children: tree_data };
			init();
		}
	});

	// 1분마다 UTM구동시간 계산
	task2 = Ext.TaskManager.start({
		run : intervals,
		interval : 60000
	});
});

// 고객 서비스 상태 Grid Panel
function instances(){
	// contextmenu
	var getNetwork = Ext.create('Ext.Action', {
		text: '네트워크 정보',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event, a, b, c, d, e) {
			network_pop_data();
		}
	});

	// contextmenu
	var getE2ELog = Ext.create('Ext.Action', {
		text: '실행이력',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			openE2E();
		}
	});

	// contextmenu
	var getVNCConsole = Ext.create('Ext.Action', {
		text: 'VNC 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('vncconsole');
		}
	});

	// contextmenu
	var getHostConsole = Ext.create('Ext.Action', {
		text: 'Host 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('hostconsole');
		}
	});

	// contextmenu
	var getWebConsole = Ext.create('Ext.Action', {
		text: 'Web 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('endian');
		}
	});

	// contextmenu
	var getHorizon = Ext.create('Ext.Action', {
		text: 'Horizon 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			openHorizon();
		}
	});

	// contextmenu
	var setServerResume = Ext.create('Ext.Action', {
		text: '서버 재구동',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('resume');
		}
	});

	// contextmenu
	var setServerRestart = Ext.create('Ext.Action', {
		text: '서버 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('rebootserver');
		}
	});

	// contextmenu
	var setServerSuspendserver = Ext.create('Ext.Action', {
		text: '서버 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			serversendcommand('suspend');
		}
	});

	Ext.define('Grid_Server_Model',{
		extend: 'Ext.data.Model',
		fields: [ "provisionseq", "cust", "telnum", "user", "cat", "node", "state", "vm_name", "conntrackcnt", "vm_id", "id", "is_error", "createvm", "ativedt", "vlan" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Server_Model',
		storeId : 'grid_server_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	function error_check(val) {
		if (val != 'ACTIVE') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return '<span style="color:green;">' + val + '</span>';
	}

	function cust_name(val, meta, record) {
		if (val == '' || val == null || val == 'null') {
			return '<span style="color : red;">미정</span>'
		}

		return val
	}

	function links(val, meta, record) {
		return '<a href="#" onclick="section_pop_data(\'' + record.get('vm_id') + '\'); return false;">' + val + '</a>'
	}

	function createvms(val) {
		val = val.replace(/T/gi, " ");
		val = val.replace(/Z/gi, "");
		return val
	}

	server_grid_panel = Ext.create('Ext.grid.Panel', {
		title: '고객 서비스 상태',
		icon : '../static/img/title_icon.png',
		flex : 3,
		id: 'nfv_inventory_panel',
		store: Ext.data.StoreManager.lookup('grid_server_store'),
		columns: [
			{xtype: 'rownumberer',width: 30, sortable: false},
			{text: "고객사", width: 100, dataIndex: 'cust', sortable: true},
			{text: "사용자", width: 160, dataIndex: 'user', sortable: true},
			{text: "VLAN", width: 140, dataIndex: 'vlan', sortable: true},
			{text: "서비스유형", width: 100,  dataIndex: 'cat', sortable: true},
			{text: "노드", width: 70, dataIndex: 'node', sortable: true},
			{text: "상태", width: 90, renderer : error_check, dataIndex: 'state', sortable: true},
			{text: "UTM이름", width: 230, dataIndex: 'vm_name', sortable: true},
			{text: "UTM생성일", width: 150, renderer : createvms, dataIndex: 'createvm', sortable: true},
			{text: "UTM구동시간", width: 150, dataIndex: 'ativedt', sortable: true},
			{text: "UTM세션수", width: 150, renderer : links, dataIndex: 'conntrackcnt', sortable: true, flex: 1},
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				var getRow = server_grid_panel.getSelectionModel().getSelection()[0];
				pseq = getRow.data.provisionseq;
				vmid = getRow.data.vm_id;
				s_Monitor.config.vm_name = getRow.data.vm_name;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ getNetwork, getE2ELog, '-', getVNCConsole, getHostConsole, getWebConsole, getHorizon, '-', setServerResume, setServerRestart, setServerSuspendserver ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: [{xtype:'buttongroup',
				items: [{
					text: '룰 동기화',
					cls: 'tttt',
					handler: function() {
						if (s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
							Ext.Msg.show({
								title:'확인',
								message: '룰 동기화가 시작 됩니다.<br>비밀번호를 입력해주세요:<br><input type="password" id="rule_sync_pass" style="width: 100%"/>',
								buttons: Ext.Msg.YESNO,
								cls: 'x-message-box-alert-color',
								fn: function(btn) {
									if (btn === 'yes') {
										$.ajax({
											url : "/monitor/synmgr",
											type : "POST",
											dataType : "json",
											data : {
												orgseq: s_Monitor.config.org_seq,
												pwd : $('#rule_sync_pass').val(),
												_xsrf : getCookie("_xsrf")
											},
											success : function(req) {
												if (req.result == 'fail') {
													Ext.example.msg('실패', req.description);
												} else {
													Ext.example.msg('룰 동기화', '룰 동기화를 실행하였습니다.');
												}
											}
										});
									}
								}
							});
						} else {
							Ext.example.msg('룰 동기화', '국사를 선택해 주세요.');
							/*Ext.Msg.show({
							    title:'확인',
							    message: '국사를 선택해 주세요.',
							    buttons: Ext.Msg.CANCEL,
							    cls: 'x-message-box-alert-color',
							    fn: function(btn, text) {
							    	console.log(text);
							    }
							});*/
						}
					}
				}]
			},'-',{xtype:'buttongroup',
				items: [{
					text: 'UTM 생성 초기화',
					cls: 'tttt',
					handler: function() {
						if (s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
							Ext.Msg.show({
								title:'확인',
								message: 'UTM 생성 초기화가 시작 됩니다.<br>비밀번호를 입력해주세요:<br><input type="password" id="utm_reset_pass" style="width: 100%"/>',
								buttons: Ext.Msg.YESNO,
								cls: 'x-message-box-alert-color',
								fn: function(btn) {
									if (btn === 'yes') {
										$.ajax({
											url : "/monitor/synmgrReset",
											type : "POST",
											dataType : "json",
											data : {
												orgseq: s_Monitor.config.org_seq,
												pwd : $('#utm_reset_pass').val(),
												_xsrf : getCookie("_xsrf")
											},
											success : function(req) {
												if (req.result == 'fail') {
													Ext.example.msg('실패', req.description);
												} else {
													Ext.example.msg('UTM 생성 초기화', 'UTM 생성 초기화를 실행하셨습니다.');
												}
											}
										});
									}
								}
							});
						} else {
							Ext.example.msg('UTM 생성 초기화', '국사를 선택해 주세요.');
							/*Ext.Msg.show({
							    title:'확인',
							    message: '국사를 선택해 주세요.',
							    buttons: Ext.Msg.CANCEL,
							    cls: 'x-message-box-alert-color',
							    fn: function(btn, text) {
							    	console.log(text);
							    }
							});*/
						}
					}
				}]
			}, '->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		}),
		viewConfig: {
			getRowClass: function(record, index, rowParams, store) {
				if (record.data.is_error == '1') {
					return 'error';
				}
			}
		}
	});
}

function switchs() {
	Ext.define('Grid_Switch_Model',{
		extend: 'Ext.data.Model',
		fields: [ "switchname", "ip", "port", "state", "type", "speed", "name", "is_error", "id" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Switch_Model',
		storeId : 'grid_switch_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		},
	});

	function error_check(val) {
		if (val == 'down') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	switchs_grid_panel = Ext.create('Ext.grid.Panel', {
		flex : 1,
		id: 'nfv_switchs_panel',
		split: true,
		plain: true,
		store: Ext.data.StoreManager.lookup('grid_switch_store'),
		columns: [
			{text: "스위치명", width: 80, dataIndex: 'switchname', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "포트", width: 50, dataIndex: 'port', sortable: true},
			{text: "상태", width: 110, renderer : error_check, dataIndex: 'state', sortable: true},
			{text: "타입", width: 150,  dataIndex: 'type', sortable: true},
			{text: "속도", width: 120, dataIndex: 'speed', sortable: true},
			{text: "명칭", width: 120, dataIndex: 'name', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			}
		},
	});
}

function mgmt_vms() {
	Ext.define('Grid_Mgmt_Model',{
		extend: 'Ext.data.Model',
		fields: [ "node", "state", "vm_name", "vm_id", "id", "is_error" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Mgmt_Model',
		storeId : 'grid_mgmt_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		},
	});

	var getVNCConsole = Ext.create('Ext.Action', {
		text: 'VNC 접속',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			mgmtcommand('vncconsole');
		}
	});

	var setServerResume = Ext.create('Ext.Action', {
		text: '서버 재구동',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			mgmtcommand('resume');
		}
	});

	var setServerRestart = Ext.create('Ext.Action', {
		text: '서버 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			mgmtcommand('rebootserver');
		}
	});

	var setServerSuspendserver = Ext.create('Ext.Action', {
		text: '서버 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			mgmtcommand('suspend');
		}
	});

	function error_check(val) {
		if (val != 'ACTIVE') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return '<span style="color:green;">' + val + '</span>';
	}

	function tips(v, m, r) {
		if (r.get('vm_name').indexOf('mgmt') > -1) {
			return '고객 서비스 관리 VM'
		}
		return ''
	}

	mgmt_vm_grid_panel = Ext.create('Ext.grid.Panel', {
		flex : 1,
		id: 'nfv_mgmt_vm_panel',
		split: true,
		plain: true,
		store: Ext.data.StoreManager.lookup('grid_mgmt_store'),
		columns: [
			{text: "노드", width: 90, dataIndex: 'node', sortable: true},
			{text: "상태", width: 90, renderer : error_check, dataIndex: 'state', sortable: true},
			{text: "VM 이름", width: 150, dataIndex: 'vm_name', sortable: true},
			{text: "컴포넌트", width: 150, renderer : tips, dataIndex: 'vm_id', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				var getRow = mgmt_vm_grid_panel.getSelectionModel().getSelection()[0];
				vmid = getRow.data.vm_id;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ getVNCConsole, '-', setServerResume, setServerRestart, setServerSuspendserver ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
	});
}

function keystone() {
	Ext.define('Grid_Keystone_Model',{
		extend: 'Ext.data.Model',
		fields: [ "host", "service", "component", "ip", "pcount", "is_error", "id", "comp" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Keystone_Model',
		storeId : 'grid_keystone_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	var setHostStart = Ext.create('Ext.Action', {
		text: '컴포넌트 시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('start');
		}
	});

	var setHostStop = Ext.create('Ext.Action', {
		text: '컴포넌트 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('stop');
		}
	});

	var setHostRestart = Ext.create('Ext.Action', {
		text: '컴포넌트 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('restart');
		}
	});

	function error_check(val) {
		if (val == '0') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function component_state(v, m, r) {
		if (r.get('is_error')  == '1') {
			return '<span style="color:red;">off</span>';
		} else {
			return '<span style="color:green;">on</span>';
		}
	}

	keystone_grid_panel = Ext.create('Ext.grid.Panel', {
		title: '인증 서비스',
		flex : 2,
		id: 'nfv_keystone_panel',
		iconCls: 'auth',
		store: Ext.data.StoreManager.lookup('grid_keystone_store'),
		columns: [
			{text: "호스트", width: 90, dataIndex: 'host', sortable: true},
			{text: "서비스", width: 150, dataIndex: 'service', sortable: true},
			{text: "컴포넌트", width: 150, dataIndex: 'component', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "상태", width: 40, align: 'center', renderer : component_state, dataIndex: 'state', sortable: true},
			{text: "프로세스 수", width: 40, renderer : error_check, dataIndex: 'pcount', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, nd, index, e) {
				e.stopEvent();
				var getRow = keystone_grid_panel.getSelectionModel().getSelection()[0];
				node = getRow.data.host;
				service = getRow.data.comp;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ setHostStart, setHostStop, setHostRestart ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		})
	});
}

function nova() {
	Ext.define('Grid_Nova_Model',{
		extend: 'Ext.data.Model',
		fields: [ "host", "service", "component", "ip", "pcount", "is_error", "id", "comp" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Nova_Model',
		storeId : 'grid_nava_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	var setHostStart = Ext.create('Ext.Action', {
		text: '컴포넌트 시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('start');
		}
	});

	var setHostStop = Ext.create('Ext.Action', {
		text: '컴포넌트 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('stop');
		}
	});

	var setHostRestart = Ext.create('Ext.Action', {
		text: '컴포넌트 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('restart');
		}
	});

	function error_check(val) {
		if (val == '0') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function component_state(v, m, r) {
		if (r.get('is_error')  == '1') {
			return '<span style="color:red;">off</span>';
		} else {
			return '<span style="color:green;">on</span>';
		}
	}

	nova_grid_panel = Ext.create('Ext.grid.Panel', {
		title: '컴퓨팅 서비스',
		flex : 2,
		id: 'nfv_nova_panel',
		iconCls: 'server',
		store: Ext.data.StoreManager.lookup('grid_nava_store'),
		columns: [
			{text: "호스트", width: 90, dataIndex: 'host', sortable: true},
			{text: "서비스", width: 150, dataIndex: 'service', sortable: true},
			{text: "컴포넌트", width: 150, dataIndex: 'component', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "상태", width: 40, align: 'center', renderer : component_state, dataIndex: 'state', sortable: true},
			{text: "프로세스 수", width: 40, renderer : error_check, dataIndex: 'pcount', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, nd, index, e) {
				e.stopEvent();
				var getRow = nova_grid_panel.getSelectionModel().getSelection()[0];
				node = getRow.data.host;
				service = getRow.data.comp;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ setHostStart, setHostStop, setHostRestart ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		})
	});
}

function neutron() {
	Ext.define('Grid_Neutron_Model',{
		extend: 'Ext.data.Model',
		fields: [ "host", "service", "component", "ip", "pcount", "is_error", "id", "comp" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Neutron_Model',
		storeId : 'grid_neutron_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	var setHostStart = Ext.create('Ext.Action', {
		text: '컴포넌트 시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('start');
		}
	});

	var setHostStop = Ext.create('Ext.Action', {
		text: '컴포넌트 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('stop');
		}
	});

	var setHostRestart = Ext.create('Ext.Action', {
		text: '컴포넌트 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('restart');
		}
	});

	function error_check(val) {
		if (val == '0') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function component_state(v, m, r) {
		if (r.get('is_error')  == '1') {
			return '<span style="color:red;">off</span>';
		} else {
			return '<span style="color:green;">on</span>';
		}
	}

	neutron_grid_panel = Ext.create('Ext.grid.Panel', {
		title: '네트워킹 서비스',
		flex : 2,
		id: 'nfv_neutron_panel',
		iconCls: 'network',
		store: Ext.data.StoreManager.lookup('grid_neutron_store'),
		columns: [
			{text: "호스트", width: 90, dataIndex: 'host', sortable: true},
			{text: "서비스", width: 150, dataIndex: 'service', sortable: true},
			{text: "컴포넌트", width: 150, dataIndex: 'component', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "상태", width: 40, align: 'center', renderer : component_state, dataIndex: 'state', sortable: true},
			{text: "프로세스 수", width: 40, renderer : error_check, dataIndex: 'pcount', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, nd, index, e) {
				e.stopEvent();
				var getRow = neutron_grid_panel.getSelectionModel().getSelection()[0];
				node = getRow.data.host;
				service = getRow.data.comp;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ setHostStart, setHostStop, setHostRestart ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		})
	});
}

function glance() {
	Ext.define('Grid_Glance_Model',{
		extend: 'Ext.data.Model',
		fields: [ "host", "service", "component", "ip", "pcount", "is_error", "id", "comp" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Glance_Model',
		storeId : 'grid_glance_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	var setHostStart = Ext.create('Ext.Action', {
		text: '컴포넌트 시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('start');
		}
	});

	var setHostStop = Ext.create('Ext.Action', {
		text: '컴포넌트 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('stop');
		}
	});

	var setHostRestart = Ext.create('Ext.Action', {
		text: '컴포넌트 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('restart');
		}
	});

	function error_check(val) {
		if (val == '0') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function component_state(v, m, r) {
		if (r.get('is_error')  == '1') {
			return '<span style="color:red;">off</span>';
		} else {
			return '<span style="color:green;">on</span>';
		}
	}

	glance_grid_panel = Ext.create('Ext.grid.Panel', {
		title: '이미지 서비스',
		flex : 2,
		id: 'nfv_glance_panel',
		iconCls: 'images',
		store: Ext.data.StoreManager.lookup('grid_glance_store'),
		columns: [
			{text: "호스트", width: 90, dataIndex: 'host', sortable: true},
			{text: "서비스", width: 150, dataIndex: 'service', sortable: true},
			{text: "컴포넌트", width: 150, dataIndex: 'component', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "상태", width: 40, align: 'center', renderer : component_state, dataIndex: 'state', sortable: true},
			{text: "프로세스 수", width: 40, renderer : error_check, dataIndex: 'pcount', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, nd, index, e) {
				e.stopEvent();
				var getRow = glance_grid_panel.getSelectionModel().getSelection()[0];
				node = getRow.data.host;
				service = getRow.data.comp;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ setHostStart, setHostStop, setHostRestart ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		})
	});
}

function etc_infra() {
	Ext.define('Grid_Etc_Infra_Model',{
		extend: 'Ext.data.Model',
		fields: [ "host", "service", "component", "ip", "pcount", "is_error", "id", "comp" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Etc_Infra_Model',
		storeId : 'grid_etc_infra_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	var setHostStart = Ext.create('Ext.Action', {
		text: '컴포넌트 시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('start');
		}
	});

	var setHostStop = Ext.create('Ext.Action', {
		text: '컴포넌트 정지',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('stop');
		}
	});

	var setHostRestart = Ext.create('Ext.Action', {
		text: '컴포넌트 재시작',
		icon: '../static/img/stitle_icon.png',
		handler: function(widget, event) {
			hostsendcommand('restart');
		}
	});

	function error_check(val) {
		if (val == '0') {
			return '<span style="color:red;">' + val + '</span>';
		}
		return val;
	}

	function component_state(v, m, r) {
		if (r.get('is_error')  == '1') {
			return '<span style="color:red;">off</span>';
		} else {
			return '<span style="color:green;">on</span>';
		}
	}

	ect_infra_grid_panel = Ext.create('Ext.grid.Panel', {
		flex : 1,
		id: 'nfv_ect_infra_panel',
		store: Ext.data.StoreManager.lookup('grid_etc_infra_store'),
		columns: [
			{text: "호스트", width: 90, dataIndex: 'host', sortable: true},
			{text: "서비스", width: 150, dataIndex: 'service', sortable: true},
			{text: "컴포넌트", width: 150, dataIndex: 'component', sortable: true},
			{text: "아이피", width: 150, dataIndex: 'ip', sortable: true},
			{text: "상태", width: 40, align: 'center', renderer : component_state, dataIndex: 'state', sortable: true},
			{text: "프로세스 수", width: 40, renderer : error_check, dataIndex: 'pcount', sortable: true, flex: 1}
		],
		listeners: {
			itemcontextmenu: function(view, rec, nd, index, e) {
				e.stopEvent();
				var getRow = ect_infra_grid_panel.getSelectionModel().getSelection()[0];
				node = getRow.data.host;
				service = getRow.data.comp;
				contextMenus.destroy();
				contextMenus = Ext.create('Ext.menu.Menu', {
					items: [ setHostStart, setHostStop, setHostRestart ]
				});
				contextMenus.showAt(e.getXY());
				return false;
			}
		},
		tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: ['->', {
				icon: '../static/img/Very-Basic-Refresh-icon.png',
				cls: 'x-btn-icon',
				handler: function() {
					call_org_state(s_Monitor.config.org_seq);
				}
			}]
		})
	});
}

function traces() {
	Ext.define('Grid_Trace_Model',{
		extend: 'Ext.data.Model',
		fields: [ "mgr", "msg", "st", "et", "exc", "hs", "ts", "trace_type", "userid", "username", "cust", "status", "caller", "callee", "warn" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Trace_Model',
		storeId : 'grid_trace_store',
		autoLoad: false,
		pageSize: 50,
		proxy : {
			type: 'ajax',
			api: {
				read: '/monitor/action_trace'
			},
			reader: {
				type: 'json',
				root: 'items',
				totalProperty: 'totalCount'
			}
		},
	});

	function cust_name(val, meta, record) {
		if (val == '' || val == null || val == 'null') {
			if (record.data.username == '' || record.data.username == null || record.data.username == 'null') {
				return ''
			}
			return '<span style="color : gray;">' + record.data.username + '</span>'
		}

		return val
	}

	function links(val, meta, record) {
		if(val == 'Sync Manager') {
			return '<a href="#" onclick="e2elink(\'' + record.data.ts + '\', \'' + record.data.trace_type + '\'); return false;">' + val + ' : ' + record.data.ts + '</a>';
		}
		return '<a href="#" onclick="e2elink(\'' + record.data.ts + '\', \'' + record.data.trace_type + '\'); return false;">' + val + '</a>';
	}

	function history_result(v, m, r) {
		if (v  == '실패') {
			return '<span style="color:red;">' + v + '</span>';
		} else {
			return '<span style="color:green;">' + v + '</span>';
		}
	}

	function caller(v, m, r) {
		if (v != null && v != '') {
			if (r.data.trace_type == '2') {
				return '<a href="#" onclick="e2elink(\'' + v + '\', \'1\'); return false;" title="Sync Manager 정보">Sync Manager : ' + v + '</a>';
			} else if (r.data.trace_type == '1') {
				return '<a href="#" onclick="ruleCompareError(\'' + r.data.ts + '\'); return false;" title="룰 동기화 이상 정보">' + v + '</a>';
			} else {
				return v;
			}
		} else {
			return ''
		}
	}

	var select1 = Ext.create('Ext.data.Store', { // 데이터 저상소
		fields : ['v', 'd'],
		data : [
		        {'v' : '0', "d" : '전체'},
		        {'v' : '1', "d" : 'Sync Manager'},
		        {'v' : '2', "d" : 'Provision Manager'},
		        {'v' : '3', "d" : 'HA Manager'},
		]
	});

	var select2 = Ext.create('Ext.data.Store', { // 데이터 저상소
		fields : ['v', 'd'],
		data : [
		        {'v' : '0', "d" : '전체'},
		        {'v' : '1', "d" : '성공'},
		        {'v' : '2', "d" : '실패'}
		]
	})

	trace_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'nfv_trace_panel',
		store: Ext.data.StoreManager.lookup('grid_trace_store'),
		flex: 1,
		columns: [
			{text: "매니저", width: 130, renderer : links, dataIndex: 'msg', sortable: true},
			{text: "호출자", width: 130, renderer : caller, dataIndex: 'caller', sortable: true},
			{text: "고객사", width: 120, renderer : cust_name, dataIndex: 'cust', sortable: true},
			{text: "결과", width: 50, renderer : history_result, dataIndex: 'status', sortable: true},
			{text: "시작시간", width: 110, dataIndex: 'st', sortable: true},
			{text: "종료시간", width: 110, dataIndex: 'et', sortable: true},
			{text: "소요시간", width: 70, renderer : function(val, meta, record) { if (val == 'null' || val == null) { return '' } return '<span style="color:' + record.data.warn + ';">' + val + '</span>'; }, dataIndex: 'exc', sortable: true, flex: 1},
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				if (rec.data.trace_type == '3') {
					var win = window.open("/monitor/e2etrace?seq="+rec.data.ts+"&comm="+rec.data.trace_type+"&org="+s_Monitor.config.org_seq,"e2e_ha",'width=950, height=180, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
				} else if (rec.data.trace_type == '1') {
					var win = window.open("/monitor/e2etrace?seq="+rec.data.ts+"&comm="+rec.data.trace_type+"&org="+s_Monitor.config.org_seq,"e2e_sync",'width=1020, height=600, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
				} else {
					var win = window.open("/monitor/e2etrace?seq="+rec.data.ts+"&comm="+rec.data.trace_type+"&org="+s_Monitor.config.org_seq,"e2e",'width=950, height=600, scrollbars=yes, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
				}
				return false;
			},
		},
		dockedItems: [{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			store: Ext.data.StoreManager.lookup('grid_trace_store'),
			displayInfo : true,
			displayMsg: '현재데이터 : {0} - {1} / 총 데이터수 : {2}',
			emptyMsg: '데이터가 존재하지 않습니다.'
		}],
	    tbar: Ext.create('Ext.toolbar.Toolbar',{
			items: [{
                xtype     : 'combo',
                fieldLabel: '매니저',
                labelWidth: '45px',
//                width     : 150,
                id : 'mgr',
                displayField : 'd',
                valueField : 'v',
                editable: false,
                store     : select1,
                listeners :{
                	'select' : function(a, b) {
                		s_Monitor.config.mgr = b.data.v;
                		Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);
        				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
                	}
                }
            },'-',{
                xtype     : 'combo',
                fieldLabel: '상태',
                labelWidth: '40px',
//                width     : 150,
                id : 'state',
                displayField : 'd',
                valueField : 'v',
                editable: false,
                store     : select2,
                listeners :{
                	'select' : function(a, b) {
                		s_Monitor.config.re = b.data.v;
                		Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);
        				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
                	}
                }
            }, '-',{
            	xtype : 'datefield',
                fieldLabel: '조회일자',
                labelWidth: '60px',
                name: 'startdt',
                id: 'startdt',
                format: 'Y-m-d',
                listeners : {
                	change: function(a, b){
                        var c = new Date(b);
                        var d = c.getFullYear() + '-' + (c.getMonth() + 1) + '-' + c.getDate() + ' 00:00:00';

                        if (c.getFullYear() != '1970') {
	                        s_Monitor.config.st = d;

	                        /*console.log(s_Monitor.config.st);
	                        console.log(s_Monitor.config.et);*/

	                        Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);
	        				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
                        }
                    }
                }
            },
            {
            	xtype : 'datefield',
                name: 'enddt',
                id: 'enddt',
                format: 'Y-m-d',
                listeners : {
                	change: function(a, b){
                        var c = new Date(b);
                        var d = c.getFullYear() + '-' + (c.getMonth() + 1) + '-' + c.getDate() + ' 23:59:59';

                        if (c.getFullYear() != '1970') {
                        	s_Monitor.config.et = d;
                        	/*console.log(s_Monitor.config.st);
                            console.log(s_Monitor.config.et);*/

	                        Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);
	        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);
	        				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
                        }
                    }
                }
            },'-',{xtype:'buttongroup',
				items: [{
					text: '초기화',
					cls: 'tttt',
					handler: function() {
						s_Monitor.config.re = '0'
						s_Monitor.config.mgr = '0';
						s_Monitor.config.st = '';
						s_Monitor.config.et = '';

						Ext.getCmp('mgr').setValue('0');
						Ext.getCmp('state').setValue('0');
						Ext.getCmp('startdt').setValue('');
						Ext.getCmp('enddt').setValue('');

						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', '0');
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', '0');
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', '');
        				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', '');
        				Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
					}
				}]
			},/*{xtype:'buttongroup',
				items: [{
					text: '전체',
					cls: 'tttt',
					handler: function() {
						s_Monitor.config.re = '0';
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
						Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
					}
				}]
			},'-',{xtype:'buttongroup',
				items: [{
					text: '성공',
					cls: 'tttt',
					handler: function() {
						s_Monitor.config.re = '1';
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
						Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
					}
				}]
			},'-',{xtype:'buttongroup',
				items: [{
					text: '실패',
					cls: 'tttt',
					handler: function() {
						s_Monitor.config.re = '2';
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
						Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
						Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
					}
				}]
			}*/]
	    })
	});
	Ext.getCmp('mgr').setValue('0');
	Ext.getCmp('state').setValue('0');
}

function syncs_rule() {
	Ext.define('Grid_Rule_Model',{
		extend: 'Ext.data.Model',
		fields: [ "seq", "reg", "host", "ip", "name", "case", "mod", "prc", "msg", "dt", "username" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Rule_Model',
		storeId : 'grid_rule_store',
		autoLoad: false,
		pageSize: 50,
		proxy : {
			type: 'ajax',
			api: {
				read: '/monitor/sync_trace'
			},
			reader: {
				type: 'json',
				root: 'items',
				totalProperty: 'totalCount'
			}
		},
	});

	function cust_name(val, meta, record) {
		if (val == '' || val == null || val == 'null') {
			if (record.data.username == '' || record.data.username == null || record.data.username == 'null') {
				return ''
			}
			return '<span style="color : gray;">' + record.data.username + '</span>'
		}

		return val
	}

	function links(val, meta, record) {
		return '<a href="#" onclick="ruleLink(\'' + record.data.seq + '\', \'' + record.data.reg + '\'); return false;">' + val + '</a>';
	}

	rule_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'nfv_rule_panel',
		store: Ext.data.StoreManager.lookup('grid_rule_store'),
		flex: 1,
		columns: [
		    {text: "번호", width: 70, dataIndex: 'seq', sortable: true},
			{text: "장비명", width: 120, dataIndex: 'host', sortable: true},
			{text: "장치IP", width: 120, dataIndex: 'ip', sortable: true},
			{text: "CASE TYPE", width: 100,
				renderer : function(val, meta, record) {
					if (val == 'null' || val == null) {
						return ''
					} else if (val == '명명규칙 위반') {
						return '<span style="color: #E86F0B;">' + val + '</span>';
					} else if (val == '미지원 기능') {
						return '<span style="color: #FFC500;">' + val + '</span>';
					} else {
						return val;
					}
				}, dataIndex: 'case', sortable: true},
			{text: "분류", width: 100, dataIndex: 'mod', sortable: true},
			{text: "내용", width: 400, renderer : links, dataIndex: 'msg', sortable: true},
			{text: "고객사", width: 150, renderer : cust_name, dataIndex: 'name', sortable: true},
			{text: "처리", width: 100, dataIndex: 'prc', sortable: true},
			{text: "일시", width: 100, dataIndex: 'dt', sortable: true, flex: 1},
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
		},
		dockedItems: [{
			xtype: 'pagingtoolbar',
			dock: 'bottom',
			store: Ext.data.StoreManager.lookup('grid_rule_store'),
			displayInfo : true,
			displayMsg: '현재데이터수 : {0} - {1}/총 데이터수 : {2}',
			emptyMsg: '데이터가 존재하지 않습니다.'
		}]
	});
}

function process() {
	Ext.define('Grid_Process_Model',{
		extend: 'Ext.data.Model',
		fields: [ "orgseq", "process", "status", "process_name", "is_error" ]
	});

	Ext.create('Ext.data.Store', { // 데이터 저상소
		model: 'Grid_Process_Model',
		storeId : 'grid_process_store',
		proxy : {
			type: 'memory',
			reader: {
				type: 'json',
				root: 'items'
			}
		}
	});

	process_grid_panel = Ext.create('Ext.grid.Panel', {
		id: 'nfv_process_panel',
		title: '프로세스',
		icon: '../static/img/process.png',
		store: Ext.data.StoreManager.lookup('grid_process_store'),
		flex: 1,
		columns: [
			{text: "프로세스명", width: 250, dataIndex: 'process', sortable: true},
			{text: "상태", width: 80,
				renderer : function(val, meta, record) {
					if (val  != 'ACTIVE') {
						return '<span style="color:red;">off</span>';
					} else {
						return '<span style="color:green;">on</span>';
					}
				}, dataIndex: 'status', sortable: true, flex: 1},
		],
		listeners: {
			itemcontextmenu: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
		},
			itemdblclick: function(view, rec, node, index, e) {
				e.stopEvent();
				return false;
			},
		}
	});
}

function grid_show_hide(text) {
	if (text != null && text != '') {
		grid_view_panel.setHidden(false);
	}

	if (text == 'cust') {
		infras_grid_panel.setHidden(true);
		server_grid_panel.setHidden(false);
	} else if (text == 'infra') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
	} else if (text == 'keystone') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
		keystone_grid_panel.setHidden(false);
	} else if (text == 'nova') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
		nova_grid_panel.setHidden(false);
	} else if (text == 'neutron') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
		neutron_grid_panel.setHidden(false);
	} else if (text == 'glance') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
		glance_grid_panel.setHidden(false);
	} else if (text == 'etc') {
		server_grid_panel.setHidden(true);
		infras_grid_panel.setHidden(false);
		infra_view_panel.setHidden(false);
	} else {
		infras_grid_panel.setHidden(false);
		server_grid_panel.setHidden(false);
	}
}

function view_refresh() {
	trace_grid_panel.getView().refresh();
	keystone_grid_panel.getView().refresh();
	nova_grid_panel.getView().refresh();
	neutron_grid_panel.getView().refresh();
	glance_grid_panel.getView().refresh();
	ect_infra_grid_panel.getView().refresh();
	mgmt_vm_grid_panel.getView().refresh();
	process_grid_panel.getView().refresh();
	hw_customer_grid_panel.getView().refresh();
	dev_hw_customer_grid_panel.getView().refresh();
	cloud_grid_panel.getView().refresh();
	server_farm_grid_panel.getView().refresh();
	server_grid_panel.getView().refresh();
	rule_grid_panel.getView().refresh();
}

function init() {
	$("#beep").prop('muted', true);

	if (!('WebSocket' in window)) {
		alert('WebSocket is not support');
	}

	var org_view_tree_store = Ext.create('Ext.data.TreeStore', {
		root : org_tree_data,
		proxy : {
			type : 'memory'
		}
	});

	var org_view_tree_panel = Ext.create('Ext.tree.Panel', {
		title: '',
		region: 'west',
		collapsible: true,
		split: false,
		width: 180,

		id:'org',
		animate:true,
		autoScroll:true,
		rootVisible: false,
		enableDD:true,

		containerScroll: true,
		border: false,

		store: org_view_tree_store,
		layout: 'fit',
		dockedItems: [{
			xtype: 'toolbar',
			items: [{
				xtype: 'tbtext',
				text: userid
			},'-',{xtype:'buttongroup',
				items: [{
					text: '로그아웃',
					cls: 'tttt',
					handler: function() {
						Ext.Msg.show({
							title:'확인',
							message: '로그아웃 하시겠습니까?',
							buttons: Ext.Msg.YESNO,
							fn: function(btn) {
								if (btn === 'yes') {
									location.href = '/logout';
								}
							}
						});
					}
				}]
			}]
		}],
		listeners: {
			itemclick: function(s,r) {
				contextMenus.destroy();
				$('#org_header-targetEl > div.x-title > div.x-title-item').html(r.data.orgname);
				$('#org-placeholder-targetEl > div.x-title > div.x-title-item').html(r.data.orgname);
				s_Monitor.config.orgname = r.data.orgname;
				$('#orgname').html(s_Monitor.config.orgname);
				if (s_Monitor.config.org_seq != r.data.org) {
					s_Monitor.config.active_standby = '';
					$('#utm_ative').attr('class', 'fa fa-gear');
					$('#cloud_ative').attr('class', 'fa fa-gear');
					$('#activeCloud').attr('class','hidden');
					$('#activeHwutm').attr('class','hidden');
					$('#nonsel').attr('class','');
					if(r.data.index_text){
						data_text = r.data.index_text;
					} else {
						data_text = '';
					}
					s_Monitor.config.org_seq = r.data.org;
					s_Monitor.config.re = '0';
					s_Monitor.config.mgr = '0';
					Ext.getCmp('mgr').setValue('0');
					Ext.getCmp('state').setValue('0');
					index_menu = data_text;
					call_org_state(s_Monitor.config.org_seq);
				}

				grid_show_hide(r.data.index_text);
			},
			itemcontextmenu : function( object, record, item, index, e){
				   e.stopEvent();
			},
			collapse : function(s,r){
				$('#org-placeholder-targetEl > div.x-title > div.x-title-item').html($('#org_header-targetEl > div.x-title > div.x-title-item').html());
			}
		}
	});

	instances();
	switchs();
	mgmt_vms();
	traces();
	syncs_rule();
	keystone();
	nova();
	neutron();
	glance();
	etc_infra();
	dev_hw_customer_tap();
	hw_customer_tap();
	cloud_tap();
	server_Cust_Tap();
	process();

	rule_view_panel = Ext.widget('panel', {
		title: '룰 동기화 이상',
		layout: 'border',
		iconCls: 'rule',
		xtype: 'panel',
		region: 'center',
		autoScroll: true,
		id: 'rule',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [rule_grid_panel]
	});

	hw_utm_panel = Ext.widget('panel', {
		title: 'H/W-UTM',
		layout: 'border',
		icon : '../static/img/person_company.png',
		xtype: 'panel',
		region: 'center',
		autoScroll: true,
		id: 'hw_customer_grid_panel',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [dev_hw_customer_grid_panel, hw_customer_grid_panel]
	});

	detail = Ext.widget('tabpanel', {
		region: 'center',
		layout: 'fit',
		flex : 1,
		split: true,
		plain: true,
		margins: '0 5 5 5',
		activeTab: 0,
		listeners: {
			'tabchange': function(tabPanel, tabId) {
				view_refresh();
				contextMenus.destroy();
				$('#orgname').html(s_Monitor.config.orgname);
				if(tabId.id == 'hw_customer_grid_panel'){
					$('#utm_border').attr('class','');
					$('#cloud_border').attr('class','hidden');
					$('#serverfarm_border').attr('class','hidden');
					detail.setActiveTab(0);
				} else if(tabId.id == 'cloud_grid_panel'){
					$('#utm_border').attr('class','hidden');
					$('#cloud_border').attr('class','');
					$('#serverfarm_border').attr('class','hidden');
					detail.setActiveTab(1);
				} else if(tabId.id == 'server_farm_grid_panel'){
					$('#utm_border').attr('class','hidden');
					$('#cloud_border').attr('class','hidden');
					$('#serverfarm_border').attr('class','');
					detail.setActiveTab(3);
				} else if (tabId.id == 'rule' && s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
					$('#utm_border').attr('class','hidden');
					$('#cloud_border').attr('class','hidden');
					$('#serverfarm_border').attr('class','hidden');
					Ext.data.StoreManager.lookup('grid_rule_store').getProxy().setExtraParam('org', s_Monitor.config.org_seq);
					Ext.data.StoreManager.lookup('grid_rule_store').loadPage(1);
				}
			}
		},
		items: [hw_utm_panel, cloud_grid_panel, server_farm_grid_panel]
	});

	master = master_view();

	graphic_view_panel = Ext.widget({
		title: '그래픽뷰',
		layout: 'border',
		iconCls: 'graphic',
		xtype: 'panel',
		height: '100%',
		width: '100%',
		region: 'center',
		id: 'graphic',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [master, detail, server_cust_grid_panel, customer_grid_panel, hw_utm_grid_panel]
	});

	switchs_grid_panel.setHidden(true);

	infra_view_panel = Ext.widget('panel', {
		title: '서비스 지원 인프라',
		layout: 'border',
		iconCls: 'infras',
		xtype: 'panel',
		region: 'center',
		autoScroll: true,
		id: 'infras',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [ect_infra_grid_panel, mgmt_vm_grid_panel, switchs_grid_panel]
	});

	if (userauth != '0' && userauth != '99') {
		process_grid_panel.setHidden(true);
	}

	infras_grid_panel = Ext.widget('tabpanel', {
		title: '인프라 동작 상태',
		icon : '../static/img/title_icon.png',
		region: 'center',
		layout: 'fit',
		flex : 2,
		split: true,
		plain: true,
		margins: '0 5 5 5',
		activeTab: 0,
		items: [keystone_grid_panel, nova_grid_panel, neutron_grid_panel, glance_grid_panel, infra_view_panel, process_grid_panel],
		listeners: {
			'tabchange': function(tabPanel, tabId) {
				view_refresh();
				$('#orgname').html(s_Monitor.config.orgname);
				contextMenus.destroy();
			}
		}
	});

	grid_view_panel = Ext.widget('panel', {
		title: '그리드뷰',
		layout: 'border',
		iconCls: 'grid',
		xtype: 'panel',
		height: '100%',
		width: '100%',
		region: 'center',
		id: 'grid',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [server_grid_panel, infras_grid_panel]
	});

	trace_view_panel = Ext.widget('panel', {
		title: '실행이력',
		layout: 'border',
		iconCls: 'trace',
		xtype: 'panel',
		region: 'center',
		autoScroll: true,
		id: 'trace',
		layout: {
			type: 'vbox',
			align: 'stretch'
		},
		items: [trace_grid_panel]
	});

	Ext.tip.QuickTipManager.init();

	main_panel = Ext.widget('tabpanel', {
		region: 'center',
		layout: 'fit',
		split: true,
		plain: true,
		margins: '0 5 5 5',
		activeTab: 0,
		items: [graphic_view_panel, grid_view_panel, trace_view_panel, rule_view_panel],
		listeners: {
			'tabchange': function(tabPanel, tabId) {
				if (tabId.id == 'trace' && s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
					Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('seq', s_Monitor.config.org_seq);
					Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('re', s_Monitor.config.re);
					Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('mgr', s_Monitor.config.mgr);
					Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('st', s_Monitor.config.st);
    				Ext.data.StoreManager.lookup('grid_trace_store').getProxy().setExtraParam('et', s_Monitor.config.et);
					Ext.data.StoreManager.lookup('grid_trace_store').loadPage(1);
				} else if (tabId.id == 'rule' && s_Monitor.config.org_seq != '' && s_Monitor.config.org_seq != null) {
					Ext.data.StoreManager.lookup('grid_rule_store').getProxy().setExtraParam('org', s_Monitor.config.org_seq);
					Ext.data.StoreManager.lookup('grid_rule_store').loadPage(1);
				}
				view_refresh();
				$('#orgname').html(s_Monitor.config.orgname);
				contextMenus.destroy();

				if(s_Monitor.config.serverfarm_error != 0){
					setTimeout(function() {
						$('#serverfarm_a').attr('class','');
						$('#serverfarm').attr('class','hidden');
					}, 100);
				} else {
					setTimeout(function() {
						$('#serverfarm_a').attr('class','hidden');
						$('#serverfarm').attr('class','');
					}, 100);
				}

				$('#farmName').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
				$('#farmName_a').html("고객 서버팜(총: "+s_Monitor.config.serverfarm_total+"대, 장애: "+s_Monitor.config.serverfarm_error+"대)");
				if (s_Monitor.config.active_standby == '') {
					$('#utm_ative').attr('class', 'fa fa-gear');
					$('#cloud_ative').attr('class', 'fa fa-gear');
					$('#activeCloud').attr('class','hidden');
					$('#activeHwutm').attr('class','hidden');
					$('#nonsel').attr('class','');
				} else if (s_Monitor.config.active_standby == '1') {
					$('#utm_a').attr('class','hidden');
					$('#utm').attr('class','');
					$('#activeCloud').attr('class','hidden');
					$('#activeHwutm').attr('class','');
					$('#nonsel').attr('class','hidden');
					$('#utm_ative').attr('class', 'fa fa-gear fa-spin');
					$('#cloud_ative').attr('class', 'fa fa-gear');
				} else {
					$('#utm_a').attr('class','');
					$('#utm').attr('class','hidden');
					$('#activeCloud').attr('class','');
					$('#activeHwutm').attr('class','hidden');
					$('#nonsel').attr('class','hidden');
					$('#utm_ative').attr('class', 'fa fa-gear');
					$('#cloud_ative').attr('class', 'fa fa-gear fa-spin');
				}
			}
		}
	});

	body_panel = Ext.widget('panel', {
		layout: 'border',
		xtype: 'panel',
		height: '100%',
		width: '100%',
		region: 'center',
		items: [org_view_tree_panel, main_panel],
		bbar: Ext.create('Ext.ux.StatusBar',{
			id: 'basic-statusbar',
			defaultText: 'Ready',
			defaultIconCls: 'x-status-valid',
			items: ['->', {
				boxLabel: '음소거',
				xtype: 'checkbox',
				checked: false,
				listeners: {
					change: function(cb, newValue, oldValue) {
						if (newValue == true) {
							$("#beep").prop('muted', true);
							console.log('mute on');
						} else {
							$("#beep").prop('muted', false);
							console.log('mute off');
						}
					}
				}
			}]
		}),
	});

	Ext.create('Ext.Viewport', {
		layout: {
			type: 'border',
			padding: 5
		},
		items: [body_panel],
		renderTo: Ext.getBody()
	});
}

	function server_stores(grid_cnode1) {
		var servers_stores = Ext.data.StoreManager.lookup('grid_server_store');
		servers_stores.removeAll();

		var ser_cnt = 0;
		var ser_id = '';
		var errors = 0;
		for (var i=0; i<grid_cnode1.length; i++) {
			var datas = grid_cnode1[i];
			ser_id = datas[2] + '_' + datas[4];
			var is_error = 0;
			if (datas[5] != 'ACTIVE') {
				is_error = 1;
				errors++;
			}

			if (datas[1] != null && datas[13] == 0) {
				is_error = 1;
				errors++;
			}

			if (datas[1] != null){
				servers_stores.insert(ser_cnt, { 'provisionseq' : datas[1], 'cust' : datas[7], 'telnum' : datas[11], 'user' : datas[6],
													'cat' : datas[12], 'node' : datas[2], 'state' : datas[5], 'vm_name' : datas[4], 'vm_id' : datas[3],
													'id' : ser_id, 'is_error' : is_error, 'conntrackcnt' : datas[13], 'createvm' : datas[14], 'vlan' : datas[15] });
				ser_cnt++;
			}
		}

		if(errors != 0){
			setTimeout(function() {
				$('#cloud_css_a').attr('class','');
				$('#cloud_css').attr('class','hidden');
			}, 100);
		}

		intervals();
	}

	function component_stores(grid_mnode) {
		var keystone_stores = Ext.data.StoreManager.lookup('grid_keystone_store');
		var nova_stores = Ext.data.StoreManager.lookup('grid_nava_store');
		var neutron_stores = Ext.data.StoreManager.lookup('grid_neutron_store');
		var glance_stores = Ext.data.StoreManager.lookup('grid_glance_store');
		var etc_infra_stores = Ext.data.StoreManager.lookup('grid_etc_infra_store');

		keystone_stores.removeAll();
		nova_stores.removeAll();
		neutron_stores.removeAll();
		glance_stores.removeAll();
		etc_infra_stores.removeAll();

		var keystone_cnt = 0;
		var nova_cnt = 0;
		var neutron_cnt = 0;
		var glance_cnt = 0;
		var etc_infra_cnt = 0;
		var comp_id = '';

		for (var i=0; i<grid_mnode.length; i++) {
			var datas = grid_mnode[i], comp_name = admin_node[datas[3]];
			var is_error = 0;
			comp_id = datas[1] + '_' + datas[3];
			if (datas[4] == '0'){
				is_error = 1;
			}
			if (datas[2].indexOf('keystone') > -1) {
				if (comp_name != null && comp_name != '') {
					keystone_stores.insert(keystone_cnt, { 'host' : datas[1], 'service' : datas[2], 'component' : comp_name, 'ip' : datas[5], 'pcount' : datas[4], 'is_error' : is_error, 'id' : comp_id, 'comp' : datas[3] });
					keystone_cnt++;
				}
			} else if (datas[2].indexOf('nova') > -1) {
				if (comp_name != null && comp_name != '') {
					nova_stores.insert(nova_cnt, { 'host' : datas[1], 'service' : datas[2], 'component' : comp_name, 'ip' : datas[5], 'pcount' : datas[4], 'is_error' : is_error, 'id' : comp_id, 'comp' : datas[3] });
					nova_cnt++;
				}
			} else if (datas[2].indexOf('neutron') > -1) {
				if (comp_name != null && comp_name != '') {
					neutron_stores.insert(neutron_cnt, { 'host' : datas[1], 'service' : datas[2], 'component' : comp_name, 'ip' : datas[5], 'pcount' : datas[4], 'is_error' : is_error, 'id' : comp_id, 'comp' : datas[3] });
					neutron_cnt++;
				}
			} else if (datas[2].indexOf('glance') > -1) {
				if (comp_name != null && comp_name != '') {
					glance_stores.insert(glance_cnt, { 'host' : datas[1], 'service' : datas[2], 'component' : comp_name, 'ip' : datas[5], 'pcount' : datas[4], 'is_error' : is_error, 'id' : comp_id, 'comp' : datas[3] });
					glance_cnt++;
				}
			} else {
				if (comp_name != null && comp_name != '') {
					etc_infra_stores.insert(etc_infra_cnt, { 'host' : datas[1], 'service' : datas[2], 'component' : comp_name, 'ip' : datas[5], 'pcount' : datas[4], 'is_error' : is_error, 'id' : comp_id, 'comp' : datas[3] });
					etc_infra_cnt++;
				}
			}
		}
	}

	function mgmt_stores(grid_cnode1) {
		var mgmts_stores = Ext.data.StoreManager.lookup('grid_mgmt_store');

		mgmts_stores.removeAll();

		var mgmt_cnt = 0;
		var mgmt_id = '';

		for (var i=0; i<grid_cnode1.length; i++) {
			var datas = grid_cnode1[i];
			mgmt_id = datas[2] + '_' + datas[4];
			var is_error = 0;
			if (datas[5] != 'ACTIVE') {
				is_error = 1;
			}
			if (datas[1] == null){
				mgmts_stores.insert(mgmt_cnt, { 'node' : datas[2], 'state' : datas[5], 'vm_name' : datas[4], 'vm_id' : datas[3], 'id' : mgmt_id, 'is_error' : is_error });
				mgmt_cnt++;
			}
		}
	}

	function hw_utm_svg_stores(hw_utm_data) {
		var hw_utm_stores = Ext.data.StoreManager.lookup('grid_hw_utm_store');

		hw_utm_stores.removeAll();

		for (var i=0; i<hw_utm_data.length; i++) {
			var datas = hw_utm_data[i];
			if(datas[3] == 'ACTIVE'){
				hw_utm_stores.insert(i, { 'appliance_name' : datas[0], 'ip_addr' : datas[1], 'companyname' : datas[2], 'status' : datas[3], 'is_error' : 0 });
			} else {
				hw_utm_stores.insert(i, { 'appliance_name' : datas[0], 'ip_addr' : datas[1], 'companyname' : datas[2], 'status' : datas[3], 'is_error' : 1 });
			}
		}
	}

	function process_stores(process_data) {
		var process_stores = Ext.data.StoreManager.lookup('grid_process_store');

		process_stores.removeAll();

		for (var i=0; i<process_data.length; i++) {
			var datas = process_data[i];
			if(datas[3] == 'ACTIVE'){
				process_stores.insert(i, { 'orgseq' : datas[0], 'process' : datas[1], 'status' : datas[2], 'process_name' : datas[3], 'is_error' : 0 });
			} else {
				process_stores.insert(i, { 'orgseq' : datas[0], 'process' : datas[1], 'status' : datas[2], 'process_name' : datas[3], 'is_error' : 1 });
			}
		}
	}

	function grid_sorters(stores) {
		var servers_stores = Ext.data.StoreManager.lookup('grid_server_store');
		var switchs_stores = Ext.data.StoreManager.lookup('grid_switch_store');
		var mgmts_stores = Ext.data.StoreManager.lookup('grid_mgmt_store');
		var servercusts_stores = Ext.data.StoreManager.lookup('grid_serverCust_store');
		var traces_stores = Ext.data.StoreManager.lookup('grid_trace_store');
		var keystone_stores = Ext.data.StoreManager.lookup('grid_keystone_store');
		var nova_stores = Ext.data.StoreManager.lookup('grid_nava_store');
		var neutron_stores = Ext.data.StoreManager.lookup('grid_neutron_store');
		var glance_stores = Ext.data.StoreManager.lookup('grid_glance_store');
		var etc_infra_stores = Ext.data.StoreManager.lookup('grid_etc_infra_store');
		var hw_cust_stores = Ext.data.StoreManager.lookup('grid_hw_utm_store');
		var dev_hw_cust_stores = Ext.data.StoreManager.lookup('grid_dev_hw_utm_store');

		Ext.data.StoreManager.lookup('grid_process_store').sort([
			{ property: 'status', direction: 'DESC' }
		]);

		if (stores == ''){
			servers_stores.sort([
									{ property: 'state', direction: 'DESC' },
									{ property: 'provisionseq', direction: 'DESC' }
			]);
			switchs_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'switchname', direction: 'ASC' },
									{ property: 'port', direction: 'ASC' }
			]);
			mgmts_stores.sort([
								{ property: 'state', direction: 'DESC' },
								{ property: 'node', direction: 'ASC' }
			]);
			keystone_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			nova_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			neutron_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			glance_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			etc_infra_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			servercusts_stores.sort([
									{ property: 'is_error', direction: 'DESC' }
			]);
			traces_stores.sort([
								{property: 'hs', direction: 'DESC'}
			]);
			hw_cust_stores.sort([
									{ property: 'INCOMING', direction: 'DESC' }
			]);
			dev_hw_cust_stores.sort([
										{ property: 'is_error', direction: 'DESC' }
			]);
		} else if (stores == 'server') {
			servers_stores.sort([
									{ property: 'state', direction: 'DESC' },
									{ property: 'provisionseq', direction: 'DESC' }
			]);
		} else if (stores == 'switch') {
			switchs_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'switchname', direction: 'ASC' },
									{ property: 'port', direction: 'ASC' }
			]);
		} else if (stores == 'mgmt') {
			mgmts_stores.sort([
								{ property: 'state', direction: 'DESC' },
								{ property: 'node', direction: 'ASC' }
			]);
		} else if (stores == 'component') {
			keystone_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			nova_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			neutron_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			glance_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
			etc_infra_stores.sort([
									{ property: 'is_error', direction: 'DESC' },
									{ property: 'host', direction: 'ASC' },
									{ property: 'service', direction: 'ASC' },
									{ property: 'component', direction: 'ASC' }
			]);
		} else if (stores == 'servercust') {
			servercusts_stores.sort([
										{ property: 'is_error', direction: 'DESC' }
			]);
		} else if (stores == 'trace') {
			traces_stores.sort([
								{property: 'hs', direction: 'DESC'}
			]);
		} else if (stores == 'hw_utm') {
			hw_cust_stores.sort([
								{ property: 'INCOMING', direction: 'DESC' },
			]);
			dev_hw_cust_stores.sort([
										{ property: 'is_error', direction: 'DESC' }
			]);
		}
	}

	function call_org_state(orgseq) {
		var sb = Ext.getCmp('basic-statusbar');
		sb.showBusy();

		s_Monitor.get_state();

		grid_sorters('');
	}

	function functionMenu(){
		$('#monitoring').attr('class','active');
		$('#monitoring_ul').attr('style', 'display:block;');
		$("#main").css("visibility","visible");
	}
	function cssSetting(){
		if (detail.getActiveTab().id == "hw_customer_grid_panel") {
			$('#utm_border').attr('class','');
			$('#cloud_css_border').attr('class','hidden');
			$('#serverfarm_border').attr('class','hidden');
			detail.setActiveTab(0);
		} else if (detail.getActiveTab().id == "cloud_grid_panel") {
			$('#utm_border').attr('class','hidden');
			$('#cloud_css_border').attr('class','');
			$('#serverfarm_border').attr('class','hidden');
			detail.setActiveTab(1);
		} else if (detail.getActiveTab().id == "server_farm_grid_panel") {
			$('#utm_border').attr('class','hidden');
			$('#cloud_css_border').attr('class','hidden');
			$('#serverfarm_border').attr('class','');
			detail.setActiveTab(2);
		}
		$('#cloud_css').attr('class','');
		$('#cloud_css_a').attr('class','hidden');
		$('#serverfarm').attr('class','');
		$('#serverfarm_a').attr('class','hidden');
		$('#utm').attr('class','');
		$('#utm_a').attr('class','hidden');
	}