function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("userService",function($scope,$timeout) {
		$scope.init = function(){
			$scope.duplicationCheck = false;
			$scope.userDel = false;
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUserBtn = true;
			$scope.checked = false;
			$scope.searchUserId = "";
			$scope.searchUserName = "";
			$scope.orgseq = "0";
			$scope.modal = "modal";
			$scope.paging(1);
			if(loginSeq==1){
				$.ajax({
			        type:'post',
			        data : {
			        	username : "%"+username+"%", _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/user/modifyUser',
			        success: function(data) {
			        	console.log(JSON.stringify(data));
			        	$scope.modalTitle = "수 정"

						$scope.insertId = data.user[0].userid;
						$scope.insertPass = data.user[0].password;
						$scope.insertName = data.user[0].username;
						$scope.insertAuthority = data.user[0].userauth;
						if ($scope.insertAuthority == '0') {
							$scope.insertOrgseq = 0;
						} else {
							$scope.insertOrgseq = data.user[0].orgseq;
						}
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	$.SmartMessageBox({
		    				title : "조회 실패",
		    				buttons : '[닫기]'
		    			});
			        }
			    });
				$("#modifyUser").modal('show');
			}
		};

		$scope.id_change = function() {
			$scope.duplicationCheck = false;
		}

		$scope.is_disable = false;

		$scope.auth_ch = function() {
			if ($scope.insertAuthority == '0') {
				$scope.insertOrgseq = '0';
				$scope.is_disable = true;
			} else {
				$scope.insertOrgseq = '1';
				$scope.is_disable = false;
			}
		}

		$scope.userDuplicationCheck = function() {
			$.ajax({
		        type:'post',
		        data : {
		        	userId : $scope.insertId, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/user/userCheck',
		        success: function(data) {
		        	if (data.result == 't') {
		        		$scope.duplicationCheck = true;
		        		$.SmartMessageBox({
		    				title : "사용 가능한 아이디 입니다.",
		    				buttons : '[닫기]'
		    			});
		        	} else {
		        		$scope.duplicationCheck = false;
		        		$.SmartMessageBox({
		    				title : "중복된 아이디가 존재 합니다.",
		    				content : "다른 아이디를 사용해 주세요.",
		    				buttons : '[닫기]'
		    			});
		        		$scope.insertId = '';
		        	}
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	$.SmartMessageBox({
	    				title : "조회 실패",
	    				buttons : '[닫기]'
	    			});
		        }
		    });
		}

		$scope.paging = function(page) {
			if($scope.searchUserId == null || $scope.searchUserId == ""){
				searchUserId = "";
			}else{
				searchUserId = "%"+$scope.searchUserId+"%";
			}

			if($scope.searchUserName == null || $scope.searchUserName == ""){
				searchUserName = "";
			}else{
				searchUserName ="%"+$scope.searchUserName+"%";
			}
			$.ajax({
		        type:'post',
		        data : {
		        	userId : searchUserId,
					userName : searchUserName,
					orgseq : $scope.orgseq,
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/user/userList',
		        success: function(data) {
		        	$scope.userList = JSON.parse(data.user);
		        	$scope.orgList = JSON.parse(data.orgs);
		        	$scope.orgseq = data.orgseq;
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	$.SmartMessageBox({
	    				title : "조회 실패",
	    				buttons : '[닫기]'
	    			});
		        }
		    });
		};
		/*// 국사 검색 모달창 초기화
		$scope.addressSearch = function(){
			$scope.dongName = "";
			$scope.orgTable = false;
			$scope.orgModal = true;
	    };*/
	   /* // 국사 검색
		$scope.searchDong = function(seq){
	    	var addr3 = "";
	    	if(seq == 1){
	    		addr3 = "%"+$scope.dongName+"%";
	    	} else {
	    		addr3 = "%"+$scope.orgNameModal+"%";
	    	}
	    	$.ajax({
		        type:'post',
		        data : {
		        	addr3 : addr3
		        },
		        dataType:'json',
		        url: '/provisioning/serviceOrgSearch',
		        success: function(data) {
		        	$scope.org = JSON.parse(data.orgs);
					$scope.orgTable = true;
					$scope.seq = seq;
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
	    };
	    // 국사 선택
	    $scope.selectedOrg = function(orgseq, orgname){
	    	if($scope.seq == 1){
	    		$scope.searchOrgName = orgname;
	    	} else {
	    		$scope.orgNameModal = orgname;
	    	}
	    	$scope.orgSeq = orgseq;
	    	$scope.insertOrgseq = orgseq;
	    	$('#searchAddr').modal('hide');
	    	$scope.orgTable = false;
	    };*/

		$scope.addUser = function(){
			$scope.insertId = "";
			$scope.insertPass = "";
			$scope.checkPass = "";
			$scope.insertName = "";
			$scope.insertAuthority = "1";
			$scope.insertOrgseq = "1";
			$scope.orgNameModal = "";
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
			$scope.orgNameModal = "";
			$scope.passInput = true;
			$scope.passDiv = false;
		};

		$scope.userSubmit = function(){
			if ($scope.duplicationCheck) {
				if ($scope.insertPass == $scope.checkPass) {
					$.ajax({
				        type:'post',
				        data : {
				        	userId : $scope.insertId,
							password : $scope.insertPass,
							userName : $scope.insertName,
							userAuth : $scope.insertAuthority,
							orgSeq : $scope.insertOrgseq, _xsrf : getCookie("_xsrf")
				        },
				        dataType:'json',
				        url: '/user/insertUser',
				        success: function(data) {
				        	$.smallBox({
						        title : "등록 완료",
						        content : $scope.insertId+"로 유저등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
						        color : "#5384AF",
						        timeout: 2000,
						        icon : "fa fa-bell"
						    });
							$('#insertUser').modal('hide');
							$scope.init();
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				        	$.SmartMessageBox({
			    				title : "등록 실패",
			    				buttons : '[닫기]'
			    			});
				        }
				    });
				} else {
					$.SmartMessageBox({
	    				title : "비밀번호와 비밀번호 확인이 일치하지 않습니다.",
	    				buttons : '[닫기]'
	    			});
				}
			} else {
				$.SmartMessageBox({
    				title : "아이디 중복 체크를 해주세요.",
    				buttons : '[닫기]'
    			});
			}
		};

		$scope.userDetail = function(userid, orgseq){
			$scope.modalTitle = "수정";
			$scope.checkPass = "";
			$scope.passInput = false;
			$scope.passDiv = true;
			$.ajax({
		        type:'post',
		        data : {
		        	userid : userid, orgseq : orgseq, _xsrf : getCookie("_xsrf"),
		        },
		        dataType:'json',
		        url: '/user/userDetail',
		        success: function(data) {
		        	$scope.insertId = data.user[0].userid;
					$scope.insertPass = data.user[0].password;
					$scope.insertName = data.user[0].username;
					if(data.user[0].userauth == '관리자'){
						$scope.insertAuthority = 0;
					} else if(data.user[0].userauth == '운영자'){
						$scope.insertAuthority = 1;
					} else if(data.user[0].userauth == '고객'){
						$scope.insertAuthority = 2;
					}
					console.log(JSON.stringify($scope.orgList));
					console.log(data.user[0].orgseq);
					if ($scope.insertAuthority == '0') {
						$scope.insertOrgseq = 0;
					} else {
						$scope.insertOrgseq = data.user[0].orgseq;
					}
					$scope.originOrgseq = data.user[0].orgseq;
					$scope.orgNameModal = data.user[0].orgname;
					$scope.modify = true;
					$scope.submit = false;
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	$.SmartMessageBox({
	    				title : "조회 실패",
	    				buttons : '[닫기]'
	    			});
		        }
		    });
		};

		$scope.userModify = function(){
			if ($scope.insertPass == $scope.checkPass) {
				$.ajax({
			        type:'post',
			        data : {
			        	userId : $scope.insertId,
						password : $scope.insertPass,
						userName : $scope.insertName,
						userAuth : $scope.insertAuthority,
						originOrgSeq : $scope.originOrgseq,
						orgSeq : $scope.insertOrgseq, _xsrf : getCookie("_xsrf")
			        },
			        dataType:'json',
			        url: '/user/updateUser',
			        success: function(data) {
			        	$('#insertUser').modal('hide');
						$.smallBox({
					        title : "수정 완료",
					        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
						loginSeq = false;
						$scope.init();
			    		$scope.$apply();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			        	$.SmartMessageBox({
		    				title : "수정 실패",
		    				buttons : '[닫기]'
		    			});
			        }
			    });
			} else {
				$.SmartMessageBox({
    				title : "비밀번호와 비밀번호 확인이 일치하지 않습니다.",
    				buttons : '[닫기]'
    			});
			}
		};

		$scope.userClose = function(){
			$scope.orgModal = false;
		};

		$scope.userDelete = function(){
			$.SmartMessageBox({
				title : "사용자 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.userList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								userid : $("#tdValue"+i).html(),
								orgseq : $("#tddValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	userId : $scope.item[j].userid, orgseq : $scope.item[j].orgseq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/user/deleteUser',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	$.SmartMessageBox({
				    				title : "삭제 실패",
				    				buttons : '[닫기]'
				    			});
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertUser').draggable();
		$('#modifyUser').draggable();
	});

	function init() {
		$('#lp_user_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_user_user').attr('class','active');
		$("#main").css("visibility","visible");
	}
