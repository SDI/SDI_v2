function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	var module = angular.module('ngApp', []);

	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	module.controller("utmService",function($scope,$timeout) {
		$scope.init = function(){
			$scope.modify = false;
			$scope.submit = false;
			$scope.addUtmBtn = true;
			$scope.checked = false;
			$scope.mgmtIp1 = 0;
			$scope.mgmtIp2 = 0;
			$scope.mgmtIp3 = 0;
			$scope.mgmtIp4 = 0;
			$scope.modal = "modal";
			$scope.insertUtmType = "HW";
			$scope.utm_seq = "0";
			$scope.paging(1);
		};

		$scope.paging = function(page) {
			$.ajax({
		        type:'post',
		        data : {
					page : page, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/utm/utmList',
		        success: function(data) {
		        	$scope.utmList = JSON.parse(data.utms);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.addUtm = function(){
			$scope.insertUtmName = "";
			$scope.mgmtIp1 = 0;
			$scope.mgmtIp2 = 0;
			$scope.mgmtIp3 = 0;
			$scope.mgmtIp4 = 0;
			$scope.insertUtmType = "HW";
			$scope.modify = false;
			$scope.submit = true;
			$scope.modalTitle = "등록";
		};

		$scope.utmSubmit = function(){
			if ($scope.mgmtIp1 < 0 || $scope.mgmtIp1 > 255 || $scope.mgmtIp2 < 0 || $scope.mgmtIp2 > 255 || $scope.mgmtIp3 < 0 || $scope.mgmtIp3 > 255 || $scope.mgmtIp4 < 0 || $scope.mgmtIp4 > 255) {
				$.smallBox({
			        title : "IP 형식 오류",
			        content : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4 + "로 IP를 입력 하셨습니다.",
			        color : "#e8a0dc",
			        timeout: 2000,
			        icon : "fa fa-bell"
			    });
				return false;
			}

			$.ajax({
		        type:'post',
		        data : {
		        	utmName : $scope.insertUtmName,
					mgmtIp : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4,
					utmType : $scope.insertUtmType, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/utm/insertUtm',
		        success: function(data) {
		        	$.smallBox({
				        title : "등록 완료",
				        content : $scope.insertUtmName+"로 UTM 등록을 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000,
				        icon : "fa fa-bell"
				    });
					$('#insertUtm').modal('hide');
					$scope.init();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.utmDetail = function(utm_seq){
			$scope.utm_seq = utm_seq;
			$scope.modalTitle = "수정";
			$.ajax({
		        type:'post',
		        data : {
		        	utm_seq : utm_seq, _xsrf : getCookie("_xsrf"),
		        },
		        dataType:'json',
		        url: '/utm/utmDetail',
		        success: function(data) {
		        	$scope.insertUtmName = data.utms[0].utm_name;
		        	var ipArr = data.utms[0].utm_mgmt_ip.split('.');
		        	$scope.mgmtIp1 = ipArr[0] * 1;
					$scope.mgmtIp2 = ipArr[1] * 1;
					$scope.mgmtIp3 = ipArr[2] * 1;
					$scope.mgmtIp4 = ipArr[3] * 1;
					$scope.insertUtmType = data.utms[0].utm_type;
					$scope.modify = true;
					$scope.submit = false;
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		$scope.utmModify = function(){
			$.ajax({
		        type:'post',
		        data : {
		        	utm_seq : $scope.utm_seq,
		        	utmName : $scope.insertUtmName,
					mgmtIp : $scope.mgmtIp1 + "." + $scope.mgmtIp2 + "." + $scope.mgmtIp3 + "." + $scope.mgmtIp4,
					utmType : $scope.insertUtmType, _xsrf : getCookie("_xsrf")
		        },
		        dataType:'json',
		        url: '/utm/updateUtm',
		        success: function(data) {
		        	$('#insertUtm').modal('hide');
					$.smallBox({
				        title : "수정 완료",
				        content : "수정하신 내용으로 변경하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
				        color : "#5384AF",
				        timeout: 2000,
				        icon : "fa fa-bell"
				    });
					$scope.init();
		    		$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		        	alert("수정 실패");
		        }
		    });
		};

		$scope.utmClose = function(){
			$scope.orgModal = false;
		};

		$scope.utmDelete = function(){
			$.SmartMessageBox({
				title : "UTM 정보 삭제 안내 문구 입니다.",
				content : "삭제를 진행 하시겠습니까? 확인 부탁 드립니다.",
				buttons : '[취소][확인]'
			},function(ButtonPressed){
				if (ButtonPressed === "확인") {
					$scope.item=[];
					var num = 0;
					for(var i=0; i<$scope.utmList.length; i++){
						if($("input:checkbox[id='checked"+i+"\']").is(":checked")){
							$scope.item.push({
								utm_seq : $("#tdValue"+i).html()
							});
						}
					}
					for(var j=0; j<$scope.item.length; j++){
						$.ajax({
					        type:'post',
					        data : {
					        	utm_seq : $scope.item[j].utm_seq, _xsrf : getCookie("_xsrf")
					        },
					        dataType:'json',
					        url: '/utm/deleteUtm',
					        success: function(data) {
					        	num = 1
								$scope.init();
					    		$scope.$apply();
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					        	alert("삭제 실패");
					        }
					    });
					}
					if(num == 1){
						$.smallBox({
					        title : "삭제 완료",
					        content : "삭제를 완료 하였습니다. 감사합니다.<br> <i class='fa fa-clock-o'></i> <i>이 메시지는 2초후에 닫힙니다....</i>",
					        color : "#5384AF",
					        timeout: 2000,
					        icon : "fa fa-bell"
					    });
					}
				}
			});
		};
	});
	$(document).ready(function() {
		functionMenu();
		init();
		$('#insertUtm').draggable();
	});

	function init() {
		$('#lp_service_template').attr('style', 'display:block;');
		$('#wid-utm-provision-btn_connect').hide();
	};

	function functionMenu(){
		$('#lp_inventory_template').removeClass('active');
		$('#lp_inventory_appliance').removeClass('active');
		$('#lp_inventory_user').removeClass('active');
		$('#lp_inventory_org').removeClass('active');
		$('#lp_inventory_vlan').removeClass('active');
		$('#lp_inventory_ip').removeClass('active');
		$('#lp_inventory_switch').removeClass('active');
		$('#lp_inventory_utm').attr('class','active');
		$('#lp_inventory_utm_conn').removeClass('active');
		$('#lp_inventory_ha').removeClass('active');
		$("#main").css("visibility","visible");
	}
