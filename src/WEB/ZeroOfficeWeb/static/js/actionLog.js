// 크로스 브라우징 방지
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}
	// AngularJS 사용을 위한 선언
	var module = angular.module('ngApp', []);

	// AngularJS의 변수 표현형식 선언 = > {[{변수명}]}
	module.config(function($interpolateProvider) {
	    $interpolateProvider.startSymbol('{[{');
	    $interpolateProvider.endSymbol('}]}');
	  });

	// AngularJS 컨트롤러
	module.controller("actionService",function($scope,$http,$window,$timeout) {
		$scope.orgseq = 0;

		var start,end;

		// 처음 페이지 로드 시 호출 초기 작업이력 리스트 조회
		$scope.page = function(page) {
			$scope.pagination1=true;
			$scope.pagination2=false;

			$.ajax({
		        type :'post',
		        data : {page : page, orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")},
		        dataType :'json',
		        url : '/action/actionList',
		        success : function(data) {
		        	$scope.data = JSON.parse(data.actions);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.orgList = JSON.parse(data.orgs);
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$("#order").css("visibility","visible");
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
					$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		// 검색 버튼 클릭시 호출
		$scope.searchSubmit = function(page){
			$scope.pagination1=false;
			$scope.pagination2=true;

			var searchUserNm;
			var searchServiceNm;

			if($scope.searchOrgNm==null || $scope.searchOrgNm==""){
				$scope.searchOrgNm = "";
			}
			if($scope.searchUserNm == null || $scope.searchUserNm == ""){
				searchUserNm = "";
			}else{
				searchUserNm ="%"+$scope.searchUserNm+"%";
			}
			if($scope.searchServiceNm == null || $scope.searchServiceNm == ""){
				searchServiceNm = "";
			}else{
				searchServiceNm = "%"+$scope.searchServiceNm+"%";
			}
			if($scope.searchStatus == null || $scope.searchStatus == ""){
				$scope.searchStatus = "";
			}
			if($scope.fromdate == null || $scope.fromdate == ""){
				$scope.fromdate = "";
			}
			if($scope.todate == null || $scope.todate == ""){
				$scope.todate = "";
			}

			// 검색 조건에 맞는 작업이력 조회
			$.ajax({
		        type :'post',
		        data :{
					orgNm : $scope.searchOrgNm,
					userNm : searchUserNm,
					serviceNm : searchServiceNm,
					startdt : $scope.fromdate,
					enddt : $scope.todate,
					page: page, orgseq : $scope.orgseq, _xsrf : getCookie("_xsrf")
				},
		        dataType :'json',
		        url : '/action/actionSearch',
		        success : function(data) {
		        	$scope.data = JSON.parse(data.actions);
					$scope.pageNum = data.pageNum;
					$scope.start = data.startPage;
					$scope.end = data.endPage;
					$scope.pages = [];
					for(var i=$scope.start;i<=$scope.end;i++){
						$scope.pages.push({page:i});
					}
					$timeout(function(){
						$("#current"+page).addClass("active");
					},100);
					$scope.$apply();
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("데이터 실패");
		        }
		    });
		};

		// 해당 row 클릭시 상세정보 팝업
		$scope.trClick = function(seq){
			var win = window.open("/action/actionDetail?seq="+seq,"",'width=800, height=700, scrollbars=no, toolbar=no, location=no, directories=no, status=no, menubar=no, copyhistory=yes');
		};
	});
$(document).ready(function() {
	functionMenu();
	init();
	// START AND FINISH DATE
	$("#fromdate").datepicker({
						changeMonth : true,
						numberOfMonths : 1,
						dateFormat : 'yy-mm-dd',
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
										$("#todate").datepicker("option","minDate",selectedDate);
						}
	});
	$("#todate").datepicker({
						changeMonth : true,
						numberOfMonths : 1,
						dateFormat : 'yy-mm-dd',
						prevText : '<i class="fa fa-chevron-left"></i>',
						nextText : '<i class="fa fa-chevron-right"></i>',
						onClose : function(selectedDate) {
										$("#fromdate").datepicker("option","maxDate",selectedDate);
						}
	});

});

function init() { // 좌측 메뉴에서 서비스 Monitoring 활성화
	$('#monitoring_ul').attr('style', 'display:block;');
	$('#wid-utm-provision-btn_connect').hide();
};

function functionMenu(){ // 좌측 메뉴에서 작업이력 관리 강조
	$('#lp_inventory_action').attr('class','active');
	$("#main").css("visibility","visible");
}
