// 크로스 사이트 요청 위조 방어 설정
function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}

Ext.onReady(function() {
	// 원본 룰과 변환 룰 조회
	Ext.Ajax.request({
		url : '/monitor/ruleCompare',
		type : 'POST',
		dataType : 'json',
		params : {seq : seq, reg : reg, _xsrf : getCookie("_xsrf")},
		success : function(data) {
			result = JSON.parse(data.responseText)['rules'][0];
			console.log(result);
			rules = JSON.parse(result);
			console.log(rules);

			$('#original').html('<pre>' + rules.original_data + '</pre>'); // 원본 룰 입력
			$('#change').html('<pre>' + rules.parsing_data + '</pre>'); // 변환 룰 입력

			$('#viewLoading').hide();
		}
	});
});