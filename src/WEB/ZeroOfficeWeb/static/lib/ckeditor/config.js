/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	 
	config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
    [
        ['Bold','Italic','Strike'],
        ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
        ['Maximize','-','About'],
        '/',
        [ 'Styles', 'Format', 'Font', 'FontSize' ],
        [ 'TextColor', 'BGColor','List','Liststyle' ],
        ["JustifyLeft","JustifyCenter","JustifyRight","JustifyBlock"]
    ];
	config.width = '655px';
	config.height = '330px';
	config.allowedContent=true;
};
