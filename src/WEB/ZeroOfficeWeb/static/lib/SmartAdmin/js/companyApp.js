var companyApp = angular.module('companyApp', []);
companyApp.controller('MainCtrl', function($scope){

		  $scope.companies = {};

		  $http({
		    method: 'GET',
		    url: 'http://localhost:8080/soweb2/test/rest/company',
		    headers: {'Accept':'application/json'}
		  })
		  .success(function (data, status, headers, config) {
		    // 성공! 데이터를 가져왔어
			  $scope.companies = data.companies;
		  })
		  .error(function (data, status, headers, config) {
		    // 이런. 뭔가 잘못되었음! :(
			  alert('error');
		  });

});

