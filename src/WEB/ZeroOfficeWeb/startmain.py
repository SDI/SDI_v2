#-*- coding: utf-8 -*-

import os.path
import sys

import paramiko
import psycopg2
import tornado.httpserver
import tornado.ioloop
from tornado.options import define, options
import tornado.options
import tornado.web

import config.sdi_lib_path
from handler.appliancehandler import *
from handler.boardhandler import *
from handler.custServerhandler import *
from handler.hosthandler import HostSendCommandHandler
from handler.iphandler import *
from handler.monitorhandler import *
from handler.monitorhandler import *
from handler.monitorhandler import WebSocketHandler
from handler.ordermanagehandler import *
from handler.orghandler import *
from handler.provisioninghandler import *
from handler.templatehandler import *
from handler.userhandler import *
from handler.utmhandler import *
from handler.vlanhandler import *
from handler.switchhandler import *
from handler.utmConnhandler import *
from handler.hahandler import *
from handler.custhandler import *
from handler.actionhandler import *
from handler.smshandler import *
from helper.paramikoHelper import myParamiko
from helper.psycopg_helper import PsycopgHelper
from sql.usermanagesql import UserSql
from util.restclient import SoapClient
from helper.logHelper import myLogger
import tornado.autoreload
	
#서버의 포트 설정
define("port", default=9999, help="run on the given port", type=int)
define("config-file", default="config/web.conf", help="run on the given port")

#로그파일 설정(tag:파일명, logdir:파일위치, loglevel:기록한 로그 수준)
log = myLogger(tag='startmain', logdir='./log', loglevel='debug', logConsole=True).get_instance()

#로그인 및 index 페이지 제어 클래스
class LoginHandler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        #교차 프레임 스크립팅 방어(보안성 검토에 따른 보완)
        self.set_header('X-FRAME-OPTIONS', 'Deny')
        #userid가 저장되 쿠키값 불러오기
        username = self.get_secure_cookie("user")
        #로그인시 모니터링으로 바로가기 체크 여부가 저장된 쿠키값 불러오기
        monitor_ck = self.get_secure_cookie("monitor_ck")
        try:
            usersql = UserSql()
            #사용자 존재 여부 조회
            data = self.execute_protect(usersql.getUserIndexLogin(), (username,))

            #사용자 존재
            if len(data['rows']) > 0 :
                #모니터링 페이지로 가도록 설정되 었는지 확인
                if monitor_ck == 'T':
                    self.redirect("/monitor/nfvmonitoringThird")
                else:
                    self.redirect("/provisioning/nfvProvisioning")
            #사용자 없음
            else :
                self.render('login.html')

        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

    def post(self):
        #로그인 시 모니터링 페이지 바로가기 설정값
        monitor_ck = self.get_argument("remember", '')
        #로그인 시 입력한 userid
        j_username = self.get_argument("j_username")
        #로그인 시 입력한 비밀번호
        j_password = self.get_argument("j_password")

        #sql인젝션 방어
        if j_username.find(" ") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("-") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find(";") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find(">") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("<") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("=") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(" ") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("-") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(";") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(">") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("<") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("=") >= 0:
            self.set_status(404)
            self.render('error.html')

        try:
            usersql = UserSql()

            #사용자 존재 여부 확인
            data = self.execute_protect(usersql.getUserLogin(), (j_username, j_password,))

            #조회된 정보 저장
            for row in data['rows']:
                print "ID = ", row[0]
                print "NAME = ", row[1]
                print "USERAUTH = ", row[2]
                print "orgseq = ", row[3], "\n"
                userauth = row[2]
                orgname = row[4]

            #사용자 존재
            if len(data['rows']) > 0 :
                #userid, 권한, 소송국사명을 쿠키에 저장
                self.set_secure_cookie("user", j_username, expires_days=None)
                self.set_secure_cookie("userauth", str(userauth), expires_days=None)
                self.set_secure_cookie("orgname", str(orgname), expires_days=None)

                #로그인 기록을 DB 및 파일로 저장(log)
                self.execute_commit(usersql.setCommandLog(j_username, '/', 'login', self.request.remote_ip))
                log.info("login userid : %s, userauth : %s, orgname : %s, ip : %s", j_username, userauth, orgname, self.request.remote_ip)

                if monitor_ck :
                    self.set_secure_cookie("monitor_ck", 'T', expires_days=None)
                    self.redirect("/monitor/nfvmonitoringThird")
                else :
                    self.redirect("/provisioning/nfvProvisioning")
            #사용자 없음
            else :
                self.redirect("/login")

        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

#mss와 sso(single sign on)을 위한 클래스(로그인 처리 클래스와 유사)
class mssLoginHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        j_username = self.get_argument("userid")
        j_password = self.get_argument("pwd")

        if j_username.find(" ") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("-") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find(";") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find(">") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("<") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_username.find("=") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(" ") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("-") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(";") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("'") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find(">") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("<") >= 0:
            self.set_status(404)
            self.render('error.html')

        if j_password.find("=") >= 0:
            self.set_status(404)
            self.render('error.html')

        try:
            usersql = UserSql()

            data = self.execute_protect(usersql.getUserLogin(), (j_username, j_password,))

            for row in data['rows']:
                print "ID = ", row[0]
                print "NAME = ", row[1]
                print "USERAUTH = ", row[2]
                print "orgseq = ", row[3], "\n"
                userauth = row[2]
                orgname = row[4]

            if len(data['rows']) > 0 :
                self.set_secure_cookie("user", j_username, expires_days=None)
                self.set_secure_cookie("userauth", str(userauth), expires_days=None)
                self.set_secure_cookie("orgname", str(orgname), expires_days=None)

                self.execute_commit(usersql.setCommandLog(j_username, '/mss_login', 'login', self.request.remote_ip))
                log.info("mss_login userid : %s, userauth : %s, orgname : %s, ip : %s", j_username, userauth, orgname, self.request.remote_ip)

                self.redirect("/monitor/nfvmonitoringThird")
            else :
                self.redirect("/login")

        except psycopg2.Error as e:
            log.error('[%s]Exception Error %s' %e)

#로그아웃 처리 클래스(저장되어 있던 쿠키값들 제거)
class LogoutHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('X-FRAME-OPTIONS', 'Deny')
        self.clear_cookie("user")
        self.clear_cookie("userauth")
        self.clear_cookie("orgname")
        self.clear_cookie("_xsrf")
        self.redirect("/")

#퀀한 에러 페이지에 진입 처리 클래스
class BaseErrorHandler(tornado.web.RequestHandler):
    def write_error(self, status_code, **kwargs):
        log.info("auth_error userid : %s, userauth : %s, orgname : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"))
        self.set_status(404)
        self.render('error.html')

#에러 페이지 진입 처리 클래스
class ErrorHandler(tornado.web.RequestHandler):
    def get(self):
        log.info("error userid : %s, userauth : %s, orgname : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"))
        self.set_status(404)
        self.render('error.html')
    def post(self):
        self.write({"data" : 't'})

if __name__ == '__main__':
    tornado.options.parse_command_line()

    #SSL 인증서 위치
    cert_path=os.path.join(os.path.dirname(__file__), "cert")
    settings = dict(
        ssl_options = {
            "certfile": os.path.join(cert_path, "server.crt"),
            "keyfile": os.path.join(cert_path, "server.key")
        }
    )
    
    app = tornado.web.Application(
        #사용할 URL 등록
        handlers = [
                    (r'/', LoginHandler),
                    (r'/index', LoginHandler),
                    (r'/login', LoginHandler),
                    (r'/logout', LogoutHandler),
                    (r'/ws', WebSocketHandler),
                    (r'/autherror', BaseErrorHandler),
                    (r'/hostsendcommand', HostSendCommandHandler),
                    (r'/error', ErrorHandler),
                    (r'/mss_login', mssLoginHandler),
                   ]
                   + BoardURL().url()
                   + TemplateURL().url()
                   + ApplianceURL().url()
                   + UserURL().url()
                   + OrgURL().url()
                   + VlanURL().url()
                   + IpURL().url()
                   + OrderManageURL().url()
                   + ProvisionURL().url()
                   + MonitorURL().url()
                   + CustServerURL().url()
                   + SwitchURL().url()
                   + UtmURL().url()
                   + UtmConnURL().url()
                   + HaURL().url()
                   + CustURL().url()
                   + ActionManageURL().url()
                   + SmsURL().url(),
        #HTML파일의 폴더 위치 설정
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        #lib, js, img, css 등의 리소스 파일들 위치 설정
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        #SSL을 위한 인증서 파일 폴더 설정
        data_dir=os.path.join(os.path.dirname(__file__), "cert"),
        #보안쿠키를 위한 설정
        cookie_secret="fggakfdjdlakjf;klja;lfj;lkdfgf",
        #크로스 사이트 요청 위조 방어 설정
        xsrf_cookies=True,
        debug=False
    )
    #tornado서버 가동을 위한 명령들
    http_server = tornado.httpserver.HTTPServer(app, **settings)
    http_server.listen(options.port)
#     http_server.listen(11443)
#     tornado.ioloop.IOLoop.instance().start()
    ioloop = tornado.ioloop.IOLoop.instance()
    tornado.autoreload.start(ioloop)
    ioloop.start()
    
