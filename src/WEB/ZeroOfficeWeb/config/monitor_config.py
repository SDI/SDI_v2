# -*- coding: utf-8 -*-

import os
import sys

from sdi_config import SDI_Web_Config
cfg = SDI_Web_Config("../config/web.conf")

# NGKIM: test print>> check by using --help option
#print "db= %s" % cfg.get("database_web", "db"),

###Staging
#postgresql DB 정보들(암호화 상태)
postgresql_db_info = {
    'db' 		: cfg.get("database_web", "db"),
    'db_host' : cfg.get("database_web", "db_host"),
    'port' 	: cfg.get("database_web", "port"),
    'id'		: cfg.get("database_web", "id"),
    'pw'		: cfg.get("database_web", "pw")
}

#Provision Manager URL
ap_url_info = {
    'url' 		: cfg.get("provision_manager", "ap_url")
}

#HA Manager URL
ha_url_info = {
    'url' 		: cfg.get("ha_manager", "ha_url")
}

#Websocket URL(WEB의 IP)
websocket_url_info = {
    'url' 		: cfg.get("websocket_web", "websocket_url")
}

ssh_url_info = {
    'url' 		: cfg.get("ssh", "ssh_url")
}

#암복호화용 키값
key = {
    'key' 		: cfg.get("key", "key")
}
