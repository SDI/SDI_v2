#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 5.
@summary: openstack의 host 제어 모듈
@author: 진수
'''
import tornado.web
import json
import time

from helper.paramikoHelper import myParamiko
from helper.psycopg_helper import PsycopgHelper
from sql.monitorcollectorsql import MonitoringSql
from config.monitor_config import key
from util.aescipher import AESCipher
from helper.logHelper import myLogger

log = myLogger(tag='monitor', logdir='./log', loglevel='debug', logConsole=True).get_instance()

# 모니터링 페이지에서 컴포넌트 제어를 처리하는 클래스
class HostSendCommandHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        try:
            paramiko = myParamiko()
            aes = AESCipher(key['key'])

            service = self.get_argument("service")
            cmdtype = self.get_argument("cmdtype")
            orgseq = self.get_argument("orgseq")
            pwds = self.get_argument("pwd")
            userauth = self.get_secure_cookie("userauth")
            userid = self.get_secure_cookie("user")

            # 슈퍼유저인지 권한 확인
            if userauth != '99':
                result = "fail"
                sendresult = "권한이 없습니다."
            else:
                monitorsql = MonitoringSql()

                # 슈퍼유저의 아이디와 입력받은 비밀번호가 매칭되는지 확인
                auths = self.execute(monitorsql.getSuper(userid, pwds))
                if not auths['rows']:
                    result = "fail"
                    sendresult = "비밀번호가 잘못되었습니다."
                else:
                    # openstack의 정보 조회
                    result = self.execute(monitorsql.get_horizon(orgseq))

                    addr = aes.decrypt(result['rows'][0][0]['openstack_connect']['host'])
                    username = aes.decrypt(result['rows'][0][0]['openstack_db_connect']['userid'])
                    password = aes.decrypt(result['rows'][0][0]['openstack_connect']['password'])

                    # 각 서비스 및 명령마다 분기 처리
                    if service == 'mysqld':
                        if cmdtype == "restart":
                            cmd = "/etc/init.d/mysql %s" %("stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "/etc/init.d/mysql %s" %("start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "/etc/init.d/mysql %s" %(cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                    elif service == 'epmd':
                        if cmdtype == "restart":
                            cmd = "service rabbitmq-server %s" %("stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "service rabbitmq-server %s" %("start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "service rabbitmq-server %s" %(cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                    elif service == 'beam.smp':
                        if cmdtype == "restart":
                            cmd = "service rabbitmq-server %s" %("stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "service rabbitmq-server %s" %("start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "service rabbitmq-server %s" %(cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                    elif service == 'ovs-vswitchd':
                        if cmdtype == "restart":
                            cmd = "service openvswitch-switch %s" %("stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "service openvswitch-switch %s" %("start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "service openvswitch-switch %s" %(cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                    elif service == 'keystone-all':
                        if cmdtype == "restart":
                            cmd = "service keystone %s" %("stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "service keystone %s" %("start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "service keystone %s" %(cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                    else:
                        if cmdtype == "restart":
                            cmd = "service %s %s" %(service, "stop")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                            time.sleep(12)
                            cmd = "service %s %s" %(service, "start")
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)
                        else:
                            cmd = "service %s %s" %(service, cmdtype)
                            sendresult = paramiko.sendCommand(addr, cmd, username, password)

                    result = "success"

        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
            sendresult =  str(e)
            result = "fail"

        # 처리한 명령에 대한 기록(log)
        param = '{ "userid" : "%s", "command" : "%s", "service" : "%s" }' %(userid, cmdtype, service)
        self.execute_commit(monitorsql.setCommandLog(userid, '/hostsendcommand', cmdtype, param))

        self.write({"result": result, "description": sendresult})

if __name__ == '__main__':
    pass