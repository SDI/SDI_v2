#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.hamanagesql import HaSql
from helper.psycopg_helper import PsycopgHelper

# HA그룹 관리 페이지 처리 파일

# HA그룹 관리 관련 URL
class HaURL(object):
    def url(self):
        url = [(r'/ha/haManage', HaManageHandler),
               (r'/ha/haList', HaListHandler),
               (r'/ha/insertHa', HaInsertHandler),
               (r'/ha/updateHa', HaModifyHandler),
               (r'/ha/deleteHa', HaDeleteHandler),
               (r'/ha/deleteHaG', HaGDeleteHandler),
               (r'/ha/haDetail', HaDetailHandler),
               (r'/ha/haMax', HaGetMaxSeqHandler)
              ]
        return url

# HA그룹 관리 페이지 진입 처리
class HaManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1'  and userauth != '99':
                self.redirect('/error')
        self.render('inventory/haList.html', username = username, userauth = userauth)

# HA그룹 리스트 조회 처리
class HaListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        username = self.get_secure_cookie("user")
        orgseq = int(self.get_argument('orgseq'))
        try:
            hasql = HaSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(hasql.getHaRow(username, orgseq))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # HA그룹 리스트 조회
            has = self.execute(hasql.getHa(username, orgseq, start))
            if not has['rows'] :
                has = json.dumps([])
            else :
                result = []

                for row in has['rows']:
                    row = dict(zip(has['columns'], row))
                    result.append(row)
                has = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(hasql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)

            # UTM 연결정보 리스트 조회
            utms = self.execute(hasql.getUtmConns())
            result = []

            for row in utms['rows']:
                row = dict(zip(utms['columns'], row))
                result.append(row)

            utms = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "has" : has, "orgs" : orgs, "orgseq" : orgseq, "utms" : utms})

# HA그룹 등록 처리
class HaInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmSeq = self.get_argument('utmSeq')
        haGName = self.get_argument('haGName')
        isPrimary = self.get_argument('isPrimary')
        haState = self.get_argument('status')
        haSeq = self.get_argument('haSeq')

        hasql = HaSql()

        try:
            self.execute_commit(hasql.setHaInsert(utmSeq, haSeq, haGName, isPrimary, haState))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# HA그룹 상세 정보 조회 처리
class HaDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        haSeq = self.get_argument('haSeq')
        try:
            hasql = HaSql()
            has = self.execute(hasql.getHaDetail(haSeq))
            result = []

            for row in has['rows']:
                row = dict(zip(has['columns'], row))
                result.append(row)

            has = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"has" : has})

# HA그룹 정보 수정 처리
class HaModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        isPrimary = self.get_argument('isPrimary')
        haGName = self.get_argument('haGName')
        utmSeq = self.get_argument('utmSeq')
        haState = self.get_argument('haState')
        data = 0

        try:
            hasql = HaSql()
            self.execute_commit(hasql.setHaModify(utmSeq, haGName, isPrimary, haState))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# HA그룹 삭제 처리(그룹 내 개별 삭제)
class HaDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmSeq = self.get_argument('utmSeq')
        data = 0
        try:
            hasql = HaSql()
            self.execute_commit(hasql.setHaDelete(utmSeq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# HA그룹 삭제 처리(그룹 전체 삭제[김복순 박사님 요청에 의해 기능 숨김])
class HaGDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        haSeq = self.get_argument('haSeq')
        data = 0
        try:
            hasql = HaSql()
            self.execute_commit(hasql.setHaGroupDelete(haSeq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# HA그룹을 등록할 seq값 조회 처리
class HaGetMaxSeqHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        hasql = HaSql()
        data = self.execute(hasql.getHaSeq())['rows']

        self.write({"data" : data[0][0]})
