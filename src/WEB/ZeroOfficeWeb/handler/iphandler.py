#-*- coding: utf-8 -*-
'''
Created on 2015. 2. 25.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.ipmanagesql import IpSql
from helper.psycopg_helper import PsycopgHelper

###############################################
# 사용 안함
###############################################

class IpURL(object):
    def url(self):
        url = [(r'/ip/ipManage', IpManageHandler),
               (r'/ip/ipList', IpListHandler),
               (r'/ip/ipCheck', IpCheckHandler),
               (r'/ip/ipInsert', IpInsertHandler),
               (r'/ip/ipModify', IpModifyHandler),
               (r'/ip/ipDelete', IpDeleteHandler)
              ]
        return url

class IpManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
                self.redirect('/error')
        self.render('inventory/ipList.html', username = username, userauth = userauth)


class IpListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        username = self.get_secure_cookie("user")
        custname = self.get_argument('userid')
        ip = self.get_argument('ip')
        orgseq = int(self.get_argument('orgseq'))
        allocate_yn = self.get_argument('allocateYn')
        try:
            ipsql = IpSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(ipsql.getIpRow(username, custname, ip, orgseq, allocate_yn))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            ips = self.execute(ipsql.getIp(username, custname, ip, orgseq, start, allocate_yn))
            if not ips['rows'] :
                ips = json.dumps([])
            else :
                result = []

                for row in ips['rows']:
                    row = dict(zip(ips['columns'], row))
                    result.append(row)
                ips = json.dumps(result)

            orgs = self.execute(ipsql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "ip" : ips, "orgs" : orgs, "orgseq" : orgseq})

class IpCheckHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('ip')
        response = "succ"
        try:
            ipsql = IpSql()
            for val in json.loads(data) :
                if response == "succ" :
                    result = self.execute(ipsql.getIpCheck(val['ip']))
                    if result['rows'] == []:
                        response = "succ"
                    else :
                        response = "fail"
        except psycopg2.Error as e:
            print e
        self.write({"response" : response})

class IpInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument('ip')
        orgseq = self.get_argument('orgseq')
        try:
            ipsql = IpSql()
            for val in json.loads(data) :
                self.execute_commit(ipsql.setIpInsert(val['ip'], orgseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class IpModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        ip = self.get_argument('ip')
        userid = self.get_argument('userid')
        orgseq = self.get_argument('orgseq')
        allocate = self.get_argument('allocate')
        try:
            ipsql = IpSql()
            self.execute_commit(ipsql.setIpModify(ip, userid, orgseq, allocate))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

class IpDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        ip = self.get_argument('ip')
        try:
            ipsql = IpSql()
            for val in json.loads(ip) :
                self.execute_commit(ipsql.setUserDelete(val['ip']))
                data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})