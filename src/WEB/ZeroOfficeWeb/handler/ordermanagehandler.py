#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 13.

@author: 만구

'''


import tornado.web
import psycopg2
import json
from sql.ordermanagesql import OrderSql
from helper.psycopg_helper import PsycopgHelper

# 개통 목록 관리 페이지 처리 파일

# 개통 목록 관리 관련 URL
class OrderManageURL(object):
    def url(self):
        url = [(r'/order/orderManage', OrderManagementHandler),
               (r'/order/orderList', OrderListHandler),
               (r'/order/orderSearch', OrderSearchHandler)
              ]
        return url

# 개통 목록 관리 페이지 진입 처리
class OrderManagementHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '2' and userauth != '3' and userauth != '99' :
                    self.redirect('/error')
        self.render('service/ordermanagement.html',username=username, userauth=userauth)

# 개통 목록 리스트 조회 처리
class OrderListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        orgseq = int(self.get_argument('orgseq', '0'))
        try:
            ordersql = OrderSql()

            # 페이징
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            result = []
            rownum = self.execute(ordersql.getTemplateRow(orgseq))
            columns = rownum['columns']
            for row in rownum['rows']:
                row = dict(zip(columns, row))
                result.append(row)

            rownum = result
            rownum = rownum[0]['count']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 개통목록 리스트 조회
            result = []
            template = self.execute(ordersql.getTemplate(orgseq, start))
            columns = template['columns']
            for row in template['rows']:
                row = dict(zip(columns, row))
                result.append(row)

            template = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(ordersql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e

        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "template" : template, "orgs" : orgs, "orgseq" : orgseq})

# 검색 조건에 따른 개통목록 리스트 처리
class OrderSearchHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        userNm = self.get_argument('userNm')
        serviceNm = self.get_argument('serviceNm')
        startdt = self.get_argument('startdt')
        enddt = self.get_argument('enddt')
        orgseq = int(self.get_argument('orgseq', '0'))
        try:
            ordersql = OrderSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(ordersql.getTemplateSearchRow(userNm, serviceNm, startdt, enddt, orgseq))
            if not rownum['rows']:
                rownum = 0
            else :
                columns = rownum['columns']
                result = []

                for row in rownum['rows']:
                    row = dict(zip(columns, row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['count']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 검색 조건에 따른 개통목록 리스트 조회
            template = self.execute(ordersql.getTemplateSearch(userNm, serviceNm, startdt, enddt, start, orgseq))
            if not template['rows'] :
                template = json.dumps([])
            else :
                columns = template['columns']
                result = []

                for row in template['rows']:
                    row = dict(zip(columns, row))
                    result.append(row)

                template = json.dumps(result)

        except psycopg2.Error as e:
            print e

        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "template" : template})