#-*- coding: utf-8 -*-
'''
Created on 2015. 5. 11.

@author: 세민
'''
import psycopg2
import tornado.web
import json
from sql.custmanagesql import CustSql
from helper.psycopg_helper import PsycopgHelper

# 고객등록 페이지 처리 파일

# 고객등록 관련 URL
class CustURL(object):
    def url(self):
        url = [(r'/cust/custManage', CustManageHandler),
               (r'/cust/custList', CustListHandler),
               (r'/cust/insertCust', CustInsertHandler),
               (r'/cust/custDetail', CustDetailHandler),
               (r'/cust/updateCust', CustUpdateHandler),
               (r'/cust/deleteCust', CustDeleteHandler),
               (r'/cust/custCheck', CustCheckHandler)
              ]
        return url

# 고객등록 페이지 진입 처리
class CustManageHandler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        custname = self.get_secure_cookie("user")
        seq = int(self.get_argument('seq', '0'))
        custauth = self.get_secure_cookie("userauth")
        # 권한 체크
        if custauth != '0' and custauth != '1' and custauth != '99':
            self.redirect('/error')

        custsql = CustSql()
        result = []
        orgSeq = self.execute(custsql.getOrgName(custname))

        for row in orgSeq['rows']:
            row = dict(zip(orgSeq['columns'], row))
            result.append(row)

        orgSeq = result[0]['orgseq']
        self.render('service/custList.html', username = custname, seq = seq, userauth = custauth, orgs = orgSeq)

# 고객 리스트 조회 처리
class CustListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        userid = self.get_secure_cookie("user")
        custid = self.get_argument('userId')
        custname = self.get_argument('userName')
        orgseq = self.get_argument('orgseq')
        cust = self.get_secure_cookie("user")
        try:
            custsql = CustSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(custsql.getCustRow(cust, custid, custname, orgseq))
            if not rownum['rows']:
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip( rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 고객 리스트 조회
            cust = self.execute(custsql.getCust(cust, custid, custname, orgseq, start))
            if not cust['rows'] :
                cust = json.dumps([])
            else :
                result = []

                for row in cust['rows']:
                    row = dict(zip(cust['columns'], row))
                    result.append(row)

                cust = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(custsql.getOrgs(userid))
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "user" : cust, "orgs" : orgs, "orgseq" : orgseq })

# 고객 등록 처리
class CustInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        custid = self.get_argument('userId')
        password = self.get_argument('password')
        custname = self.get_argument('userName')
        custTenant = self.get_argument('custTenant')
        telnum = self.get_argument('telnum')
        email = self.get_argument('email')
        custauth = '2'
        orgseq = self.get_argument('orgSeq')
        try:
            custsql = CustSql()
            self.execute_commit(custsql.setUserInsert(custid, password, custname, custauth, orgseq))
            self.execute_commit(custsql.setCustInsert(custid, custname, custTenant, telnum, email))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 고객 상세정보 조회
class CustDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        custid = self.get_argument('userid')
        orgseq = self.get_argument('orgseq')
        try:
            custsql = CustSql()
            result = []
            cust = self.execute(custsql.getCustDetail(custid, orgseq))
            for row in cust['rows']:
                row = dict(zip(cust['columns'], row))
                result.append(row)

            cust = result
        except psycopg2.Error as e:
            print e
        self.write({"user" : cust})

# 고객 정보 수정 처리
class CustUpdateHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        custid = self.get_argument('userId')
        password = self.get_argument('password')
        custname = self.get_argument('userName')
        custTenant = self.get_argument('custTenant')
        telnum = self.get_argument('telnum')
        email = self.get_argument('email')
        orgseq = self.get_argument('orgSeq')
        originOrgSeq = self.get_argument('originOrgSeq')
        try:
            custsql = CustSql()
            self.execute_commit(custsql.setUserUpdate(custid, password, custname, orgseq, originOrgSeq))
            cust = self.execute(custsql.getIsCustomer(custid))
            if not cust['rows'] :
                self.execute_commit(custsql.setCustInsert(custid, custname, custTenant, telnum, email))
            else:
                self.execute_commit(custsql.setCustUpdate(custid, custname, custTenant, telnum, email))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 고객 삭제
class CustDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        custid = self.get_argument('userId')
        orgseq = self.get_argument('orgseq')
        try:
            custsql = CustSql()
            self.execute_commit(custsql.setUserDelete(custid, orgseq))
            self.execute_commit(custsql.setCustDelete(custid))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 고객 아이디 중복 체크 처리
class CustCheckHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        custId = self.get_argument('userId')
        result = "f"
        try:
            custsql = CustSql()
            result = self.execute(custsql.getCustCheck(custId))

            if result['rows'] == []:
                result = "t"
            else :
                result = "f"
        except psycopg2.Error as e:
            print e
        self.write({"result" : result})