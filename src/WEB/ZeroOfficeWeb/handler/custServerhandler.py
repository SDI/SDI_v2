#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 5.

@author: 만구
'''
import psycopg2
import tornado.web
import json
from sql.custServermanagesql import CustServerSql
from helper.psycopg_helper import PsycopgHelper

###############################################
# 사용 안함
###############################################

class CustServerURL(object):
    def url(self):
        url = [(r'/cust/custServer', custServerManageHandler),
               (r'/cust/serverList', custServerListHandler),
                (r'/cust/serverInsert', custServerInsertHandler),
                (r'/cust/serverModify', custServerModifyHandler),
                (r'/cust/serverDelete', custServerDeleteHandler)
              ]
        return url

class custServerManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '2' and userauth != '3' and userauth != '99':
            self.redirect('/error')
        self.render('service/custServerList.html', username = username, userauth = userauth)


class custServerListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        servername = self.get_argument('servername')
        servertype = self.get_argument('servertype')
        orgseq = int(self.get_argument('orgseq'))
        username = self.get_secure_cookie("user")
        customername = self.get_argument('customername')
        try:
            custsql = CustServerSql()
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(custsql.getCustServerRow(username, servername, servertype, orgseq, customername))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['count']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            custServers = self.execute(custsql.getCustServer(username, servername, servertype, start, orgseq, customername))
            if not custServers['rows'] :
                custServers = json.dumps([])
            else :
                result = []

                for row in custServers['rows']:
                    row = dict(zip(custServers['columns'], row))
                    result.append(row)
                custServers = json.dumps(result)

            custs = self.execute(custsql.getCust())
            result = []

            for row in custs['rows']:
                row = dict(zip(custs['columns'], row))
                result.append(row)

            custs = json.dumps(result)

            orgs = self.execute(custsql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "custserver" : custServers, "custs" : custs, "orgs" : orgs, "orgseq" : orgseq})

class custServerInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        serverName = self.get_argument('serverName')
        insertCust = self.get_argument('insertCust')
        serverIp = self.get_argument('serverIp')
        publicIpYn = self.get_argument('publicIpYn')
        serverType = self.get_argument('serverType')
        state = self.get_argument('state')
        storeDttm = ''
        releaseDttm = ''
        if state == 'i' :
            storeDttm = self.get_argument('storeDttm')
        if state == 'o' :
            releaseDttm = self.get_argument('releaseDttm')
        locat = self.get_argument('locat')
        spec = self.get_argument('spec')
        note = ''
        noteYn = self.get_argument('noteYn')
        if noteYn == 'y' :
            note = self.get_argument('note')
        try:
            custsql = CustServerSql()
            self.execute_commit(custsql.setCustServerInsert(serverName, insertCust, serverIp, publicIpYn, serverType, state, storeDttm, releaseDttm, locat, spec, note))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

class custServerModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        serverseq = self.get_argument('serverseq')
        serverName = self.get_argument('serverName')
        insertCust = self.get_argument('insertCust')
        serverIp = self.get_argument('serverIp')
        publicIpYn = self.get_argument('publicIpYn')
        serverType = self.get_argument('serverType')
        state = self.get_argument('state')
        storeDttm = ''
        releaseDttm = ''
        if state == 'i' :
            storeDttm = self.get_argument('storeDttm')
        if state == 'o' :
            releaseDttm = self.get_argument('releaseDttm')
        locat = self.get_argument('locat')
        spec = self.get_argument('spec')
        note = ''
        noteYn = self.get_argument('noteYn')
        if noteYn == 'y' :
            note = self.get_argument('note')
        try:
            custsql = CustServerSql()
            self.execute_commit(custsql.setCustServerModify(serverseq, serverName, insertCust, serverIp, publicIpYn, serverType, state, storeDttm, releaseDttm, locat, spec, note))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

class custServerDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        serverseq = self.get_argument('serverseq')
        try:
            custsql = CustServerSql()
            for val in json.loads(serverseq) :
                self.execute_commit(custsql.setServerDelete(val['serverseq']))
                data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})