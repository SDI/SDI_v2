#-*- coding: utf-8 -*-

'''
Created on 2015. 1. 5.

@author: 만구
'''

import psycopg2
import tornado.web
import json
from sql.boardsql import BoardSql
from helper.psycopg_helper import PsycopgHelper
from config.monitor_config import key
from util.aescipher import AESCipher
import os
import random

# Q&A 페이지 처리 파일

# Q&A 관련 URL
class BoardURL(object):
    def url(self):
        url = [(r'/board/boardPage', BoardHandler),
               (r'/board/boardList', BoardListHandler),
               (r'/board/boardSubmit', BoardInsertHandler),
               (r'/board/boardDetail', BoardDetailHandler),
               (r'/board/boardModify', BoardModifyHandler),
               (r'/board/boardDelete', BoardDeleteHandler),
               (r'/board/boardFileDelete', BoardFileDeleteHandler),
               (r'/board/boardFileUpload', BoardFileUploadHandler)
              ]
        return url

# Q&A 페이지 진입 처리
class BoardHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        if username == None :
            username = ''
        userauth = self.get_secure_cookie("userauth")
        if userauth == None :
            userauth = ''
        self.render('board/boardList.html', username = username, userauth = userauth)

# Q&A 글 리스트 조회 처리
class BoardListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument('userid')
        title = self.get_argument('title')
        content = self.get_argument('content')
        page = int(self.get_argument('page'))
        try:
            boardsql = BoardSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(boardsql.getBoardRow(userid, title, content))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # Q&A 글 리스트 조회
            boards = self.execute(boardsql.getBoard(userid, title, content, start))
            if not boards['rows']:
                boards = json.dumps([])
            else :
                result = []

                for row in boards['rows']:
                    row = dict(zip(boards['columns'], row))
                    result.append(row)

                boards = json.dumps(result)

        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "boards" : boards})

# Q&A 글 등록 처리
class BoardInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        title = self.get_argument('title')
        userid = self.get_argument('userid')
        content = self.get_argument('content')
        boardpass = self.get_argument('boardpass')
        filename = self.get_argument('filename')
        path = self.get_argument('path', '')
        data = 0

        try:
            boardsql = BoardSql()
            if path != '':
                aes = AESCipher(key['key'])
                path = aes.decrypt(path)
            self.execute_commit(boardsql.setBoardInsert(title, content, userid, boardpass, filename, path))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# Q&A 글 조회 처리
class BoardDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        boardseq = self.get_argument('boardseq')
        try:
            boardsql = BoardSql()
            result = []
            boards = self.execute(boardsql.getBoardDetail(boardseq))
            for row in boards['rows']:
                row = dict(zip(boards['columns'], row))
                result.append(row)

            boards = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"boards" : boards})

# Q&A 글 수정 처리
class BoardModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        title = self.get_argument('title')
        userid = self.get_argument('userid')
        content = self.get_argument('content')
        boardpass = self.get_argument('boardpass')
        path = self.get_argument('path')
        filename = self.get_argument('filename')
        boardseq = self.get_argument('boardseq')
        data = 0
        try:
            boardsql = BoardSql()
            self.execute_commit(boardsql.setBoardUpdate(title, content, userid, boardpass, path, filename, boardseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# Q&A 글 삭제 처리
class BoardDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        boardseq = self.get_argument('boardseq')
        data = 0
        try:
            boardsql = BoardSql()
            self.execute_commit(boardsql.setBoardDelete(boardseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# Q&A 첨부파일 삭제 처리
class BoardFileDeleteHandler(tornado.web.RequestHandler):
    def post(self):
        path = self.get_argument('path')
        if path != '/':
            os.remove(path)

# Q&A 첨부파일 업로드 처리
class BoardFileUploadHandler(tornado.web.RequestHandler):
    def post(self):
        self.set_header('X-FRAME-OPTIONS', 'Deny')
        file1 = self.request.files['fnm'][0]
        original_fname = file1['filename']
        extension = original_fname.split('.')
        extension = extension[len(extension)-1]
        # 그림 파일만 첨부 가능하기 때문에 확장자로 필터링
        if extension != 'gif' and extension != 'jpeg' and extension != 'bmp' and extension != 'png' and extension != 'jpg':
            self.write({"file" : '', "path" : '', "result" : 'f'})
        else:
            number = str(random.randrange(10,100))
            output_file = open("static/fileUpload/" + number + original_fname, 'wb')
            output_file.write(file1['body'])
            path = os.getcwd() + "/static/fileUpload"

            aes = AESCipher(key['key'])
            path = aes.encrypt(path)

            self.write({"file" : number + original_fname, "path" : path, "result" : 't'})

settings = {
'template_path': 'templates',
'static_path': 'static',
"xsrf_cookies": False
}