#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 5.

@author: 만구
'''
import psycopg2
import tornado.web
import json
from sql.usermanagesql import UserSql
from helper.psycopg_helper import PsycopgHelper

# 사용자 관리 페이지 처리 파일

# 사용자 관리 관련 URL
class UserURL(object):
    def url(self):
        url = [(r'/user/userManage', UserManageHandler),
               (r'/user/modifyUser', UserModifyHandler),
               (r'/user/userList', UserListHandler),
               (r'/user/insertUser', UserInsertHandler),
               (r'/user/userDetail', UserDetailHandler),
               (r'/user/updateUser', UserUpdateHandler),
               (r'/user/deleteUser', UserDeleteHandler),
               (r'/user/userCheck', UserCheckHandler)
              ]
        return url

# 사용자 관리 페이지 진입 처리 클래스
class UserManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        seq = int(self.get_argument('seq'))
        userauth = self.get_secure_cookie("userauth")
        # 권한 확인
        if userauth != '0' and userauth != '1' and userauth != '99' :
                self.redirect('/error')
        self.render('inventory/userList.html', username = username, seq = seq, userauth = userauth)

# 회원정보수정을 통해 사용자 관리 페이지 진입시 사용자 정보 수정을 처리하는 클래스
class UserModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        username = self.get_argument('username')
        try:
            usersql = UserSql()
            result = []
            user = self.execute(usersql.getLoginModify(username))
            for row in user['rows']:
                row = dict(zip(user['columns'], row))
                result.append(row)

            user = result
        except psycopg2.Error as e:
            print e
        self.write({"user" : user})

# 사용자 리스트 조회 처리 클래스
class UserListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        userid = self.get_argument('userId')
        username = self.get_argument('userName')
        orgseq = self.get_argument('orgseq')
        user = self.get_secure_cookie("user")
        try:
            usersql = UserSql()

            # 페이징 처리
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            rownum = self.execute(usersql.getUserRow(user, userid, username, orgseq))
            if not rownum['rows']:
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip( rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 사용자 리스트 데이터 조회
            user = self.execute(usersql.getUser(user, userid, username, orgseq, start))
            if not user['rows'] :
                user = json.dumps([])
            else :
                result = []

                for row in user['rows']:
                    row = dict(zip(user['columns'], row))
                    result.append(row)

                user = json.dumps(result)

            # 국사 리스트 조회
            orgs = self.execute(usersql.getOrgs())
            result = []

            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)

            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "user" : user, "orgs" : orgs, "orgseq" : orgseq })

# 사용자 등록 처리 클래스
class UserInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument('userId')
        password = self.get_argument('password')
        username = self.get_argument('userName')
        userauth = self.get_argument('userAuth')
        orgseq = self.get_argument('orgSeq')
        try:
            usersql = UserSql()
            self.execute_commit(usersql.setUserInsert(userid, password, username, userauth, orgseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 사용자 상세정보 조회 처리 클래스
class UserDetailHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument('userid')
        orgseq = self.get_argument('orgseq')
        try:
            usersql = UserSql()
            result = []
            user = self.execute(usersql.getUserDetail(userid, orgseq))
            for row in user['rows']:
                row = dict(zip(user['columns'], row))
                result.append(row)

            user = result
        except psycopg2.Error as e:
            print e
        self.write({"user" : user})

# 사용자 정보 수정 처리 클래스
class UserUpdateHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument('userId')
        password = self.get_argument('password')
        username = self.get_argument('userName')
        userauth = self.get_argument('userAuth')
        orgseq = self.get_argument('orgSeq')
        originOrgSeq = self.get_argument('originOrgSeq')
        try:
            usersql = UserSql()
            self.execute_commit(usersql.setUserUpdate(userid, password, username, userauth, orgseq, originOrgSeq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 사용자 삭제 처리 클래스
class UserDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument('userId')
        orgseq = self.get_argument('orgseq')
        try:
            usersql = UserSql()
            self.execute_commit(usersql.setUserDelete(userid, orgseq))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# 사용자 아이디 중복 체크 처리 클래스
class UserCheckHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userId = self.get_argument('userId')
        result = "f"
        try:
            usersql = UserSql()
            result = self.execute(usersql.getUserCheck(userId))

            if result['rows'] == []:
                result = "t"
            else :
                result = "f"
        except psycopg2.Error as e:
            print e
        self.write({"result" : result})