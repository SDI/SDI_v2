#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 23.

@author: SM
'''

import psycopg2
import tornado.web
import json
from sql.smsmanagesql import SmsSql
from helper.psycopg_helper import PsycopgHelper

# SMS수신자 관리 페이지 처리 파일

# SMS수신자 관리 관련 URL
class SmsURL(object):
    def url(self):
        url = [(r'/sms/smsManage', SmsManageHandler),
               (r'/sms/smsList', SmsListHandler),
               (r'/sms/insertSms', SmsInsertHandler),
               (r'/sms/updateSms', SmsModifyHandler),
               (r'/sms/deleteSms', SmsDeleteHandler)
              ]
        return url

# SMS수신자 관리 페이지 진입 처리 클래스
class SmsManageHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        # 사용자 권한 체크
        if userauth != '0' and userauth != '99' :
            self.redirect('/error')
        self.render('inventory/smsManage.html', username = username, userauth = userauth)

# SMS수신자 목록 조회 클래스
class SmsListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        page = int(self.get_argument('page'))
        user = self.get_argument('user', '')
        if user != '':
            user = '%' + user + '%'
        try:
            smssql = SmsSql()

            # 페이징 처리 관련
            startPage = (page - 1) / 5 * 5 + 1
            endPage = startPage + 5 - 1
            if page == 1 :
                start = 0
            else :
                temp = (page-1)*15;
                start= temp;

            # 조회된 데이터 수
            rownum = self.execute(smssql.getSmsRow(user))
            if not rownum['rows'] :
                rownum = 0
            else :
                result = []

                for row in rownum['rows']:
                    row = dict(zip(rownum['columns'], row))
                    result.append(row)

                rownum = result
                rownum = rownum[0]['r']

            # 페이징 처리 관련
            pageNum = int(rownum)/15+1

            if rownum%15 == 0 :
                pageNum = pageNum-1

            if endPage > pageNum :
                endPage = pageNum

            # 검색된 데이터 처리
            sms = self.execute(smssql.getSms(start, user))
            if not sms['rows'] :
                sms = json.dumps([])
            else :
                result = []

                for row in sms['rows']:
                    row = dict(zip(sms['columns'], row))
                    result.append(row)
                sms = json.dumps(result)

            # 국사 리스트 조회
            result = []
            orgs = self.execute(smssql.getOrg())
            for row in orgs['rows']:
                row = dict(zip(orgs['columns'], row))
                result.append(row)
            orgs = json.dumps(result)
        except psycopg2.Error as e:
            print e
        self.write({"startPage" : startPage, "endPage" : endPage, "pageNum" : pageNum, "sms" : sms, "orgs" : orgs})

# SMS수신자 등록 처리 클래스
class SmsInsertHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        user = self.get_argument('user')
        hp = self.get_argument('hp')
        dep = self.get_argument('dep')
        orgs = self.get_argument('orgs')
        types = self.get_argument('types')

        smssql = SmsSql()

        try:
            self.execute_commit(smssql.setSmsInsert(user, hp, dep, orgs, types))
            data = 1
        except psycopg2.Error as e:
            print e

        self.write({"data" : data})

# SMS수신자 정보 수정 처리 클래스
class SmsModifyHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        smsseq = self.get_argument('smsseq')
        user = self.get_argument('user')
        hp = self.get_argument('hp')
        dep = self.get_argument('dep')
        orgs = self.get_argument('orgs')
        types = self.get_argument('types')
        data = 0

        try:
            smssql = SmsSql()
            self.execute_commit(smssql.setSmsModify(smsseq, user, hp, dep, orgs, types))
            data = 1
        except psycopg2.Error as e:
            print e
        self.write({"data" : data})

# SMS수신자 삭제 처리 클래스
class SmsDeleteHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        smsseq = self.get_argument('smsseq')
        try:
            smssql = SmsSql()
            self.execute_commit(smssql.setSmsDelete(smsseq))
            data = 1

        except psycopg2.Error as e:
            print e

        self.write({"data" : data})