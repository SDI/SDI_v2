#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 7.

@author: 만구
'''
import json

import tornado.websocket
import MySQLdb
import subprocess

from helper.psycopg_helper import PsycopgHelper
from sql.monitorcollectorsql import MonitoringSql
from util.restclient import SoapClient
from config.monitor_config import websocket_url_info, ssh_url_info, key
from util.aescipher import AESCipher
from helper.logHelper import myLogger

log = myLogger(tag='monitor', logdir='./log', loglevel='debug', logConsole=True).get_instance()
CONNECTED_CLIENTS = []

# 서비스 상태조회(모니터링) 페이지 처리 파일

# 서비스 상태조회(모니터링) 관련 URL
class MonitorURL(object):
    def url(self):
        url = [(r'/monitor/nfvmonitoringThird', MonitoringThirdHandler),
               (r'/monitor/orgNametree', MonitorOrgTreeHandler),
               (r'/monitor/openStackState', MonitorStateHandler),
               (r'/monitor/vmHandle', vmHandle),
               (r'/monitor/mgmtHandle', mgmtHandle),
               (r'/monitor/e2etrace', trace_e2e_handler),
               (r'/monitor/tracedetail', trace_detail_handler),
               (r'/monitor/contrackdetail', contrack_handler),
               (r'/monitor/utmRuleDetail', hw_utm_rule_handler),
               (r'/monitor/hostError', host_error_handler),
               (r'/monitor/getHorizon', get_horizon_handler),
               (r'/monitor/action_trace', get_action_trace_handler),
               (r'/monitor/vm_network_info', get_vm_network_info),
               (r'/monitor/ruleCompare', get_rule_file),
               (r'/monitor/failover_back', failover_back_handler),
               (r'/monitor/synmgr', callSynmgrHandler),
               (r'/monitor/synmgrReset', callSynmgrResetHandler),
               (r'/monitor/error', errorHandler),
               (r'/monitor/sync_trace', get_sync_trace_handler),
               (r'/monitor/ruledetail', getRuleDetailhandler),
               (r'/monitor/hacall', haCallHandler),
               (r'/monitor/fail', failHandler),
               (r'/monitor/ruleCompareError', ruleCompareErrorHandler),
               (r'/monitor/ruleCompareErrorList', ruleCompareErrorListHandler)
              ]
        return url

# 서비스 상태조회 페이지 진입 처리
class MonitoringThirdHandler(tornado.web.RequestHandler):
    def get(self):
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        aes = AESCipher(key['key'])
        # 권한 확인
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        self.render('monitoring/nfvmonitoring3.html', username = username, userauth = userauth, socket_url = aes.decrypt(websocket_url_info['url']), ssh_url = aes.decrypt(ssh_url_info['url']))

# 호스트 장애 발생 알림 팝업 페이지 처리
class host_error_handler(tornado.web.RequestHandler):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        host = self.get_argument("host")
        create_time = self.get_argument("create_time")
        self.render('monitoring/hostError.html', host = host, create_time = create_time)

# 서비스 상태조회 페이지 좌측 국사tree 조회 처리
class MonitorOrgTreeHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        userid = self.get_argument("data")

        try:
            monitorsql = MonitoringSql()
            data = self.execute(monitorsql.selectOrgTree(userid))
        except Exception as e:
            log.error('[%s]Exception Error %s' %e)

        self.write({"org" : data['rows']})

# 선택한 국사의 서비스 상태조회 처리
class MonitorStateHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        orgseq = self.get_argument("data")

        monitorsql = MonitoringSql()

        # 컴포넌트 상태 조회
        hostnode = self.execute(monitorsql.selectOpenstackHostStateInfoMnnode(orgseq))

        # VM상태 조회
        node = self.execute(monitorsql.selectOpenstackInstanceStateInfoCnode(orgseq))

        # 고객서버팜상태 조회(사용 안함)
        serverCust = self.execute(monitorsql.selectServerFarm(orgseq))

        # 고객정보 조회(사용 안함)
        customer = self.execute(monitorsql.select_monitoring_customer(orgseq))

        # H/W-UTM 고객정보 조회
        hwutm = self.execute(monitorsql.select_monitoring_hwutm(orgseq))

        # H/W-UTM 장비 정보 및 상태 조회
        devhwutm = self.execute(monitorsql.select_monitor_sdn(orgseq))

        # H/W-UTM과 NFV-UTM의 ACTVE/STANDBY 상태 조회
        active_standby = self.execute(monitorsql.active_standby(orgseq))

        # 프로세스 상태 조회
        process_state = self.execute(monitorsql.get_process_state(orgseq))

        # 해당 국사의 스위치 타입 조회(SDN/Non-SDN)
        swtichInfo = self.execute(monitorsql.getSwitchType(orgseq))

        self.write({"host" : hostnode['rows'], "customer" : customer['rows'], "node" : node['rows'], "serverCust" : serverCust['rows'],
                    "hwutm" : hwutm['rows'], "active_standby" : active_standby['rows'], "process_state" : process_state['rows'], "swtichInfo" : swtichInfo['rows'], "devhwutm" : devhwutm['rows']})

# 웹소켓 처리
class WebSocketHandler(tornado.websocket.WebSocketHandler):
    # 웹소켓을 시작하는 함수
    def open(self, *args):
        CONNECTED_CLIENTS.append(self)

    # 웹소켓으로 들어오는 메시지를 처리하는 함수
    def on_message(self, message):
        """
                서버로 보내는 모든 data는 json 포맷. 모든 response도 json 포맷
        """
        try:
            receivedata = json.loads(message)
        except:
            resultdata = self.create_message_error('format error')
            return resultdata

        #self.ismock = receivedata['ismock']
        #self.userid = receivedata['userid']
        if receivedata['command'] == 'notifyAll':
            self.broadcast(receivedata)

    # 웹소켓을 종료하는 함수
    def on_close(self):
        CONNECTED_CLIENTS.remove(self)

    # 웹소켓에 연결된 클라이언트 모두에게 메시지 전송하는 함수
    def broadcast(self, pkg, all_but=None):
        for c in CONNECTED_CLIENTS:
            #if c.join_completed and c != all_but:
            c.write_message(pkg)

    # 웹소켓 연결 허가 관련 함수
    def check_origin(self, origin):
        return True

# 고객 서비스 UTM제어 처리 클래스
class vmHandle(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        vmid = self.get_argument("data")
        comm = self.get_argument("comm")
        orgseq = self.get_argument("orgseq")
        username = self.get_secure_cookie("user")

        monitorsql = MonitoringSql()

        data = self.execute(monitorsql.selectUserPass(vmid))
        customer = data['rows'][0][3]

        result = {"userid" : data['rows'][0][0], "password" : data['rows'][0][1], "vmid" : data['rows'][0][2], "orgseq" : orgseq, "action" : comm}
        result = json.dumps(result)
        soap = SoapClient()

        # VNC 콘솔 접속
        if (comm == 'vncconsole') :
            result = soap.sendData("/openstack/vncconsole", "post", result)
        # 재시작
        if (comm == 'rebootserver') :
            result = soap.sendData("/openstack/rebootserver", "post", result)
        # 재구동
        if (comm == 'resume') :
            result = soap.sendData("/openstack/excuteServer", "post", result)
        # 정지
        if (comm == 'suspend') :
            result = soap.sendData("/openstack/excuteServer", "post", result)
        # ssh 접속
        if (comm == 'hostconsole') :
            result = soap.sendData("/openstack/getaddress", "post", result)
            result = json.dumps({"result":"success", "description": result, "customer" : customer})
        # Web 접속
        if (comm == 'endian') :
            data = self.execute(monitorsql.get_console_url(vmid))
            result = json.dumps({"result":"success", "description": data['rows'][0]})

        param = "userid : %s, orgseq : %s, vmid : %s" % (username, orgseq, vmid)

        self.execute_commit(monitorsql.setCommandLog(username, '/monitor/vmHandle', comm, param))

        log.info("vmHandler userid : %s, userauth : %s, orgname : %s, comm : %s, orgseq : %s, vmid : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), comm, orgseq, vmid)

        self.write(result)

# mgmtVM 제어 처리 클래스
class mgmtHandle(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        vmid = self.get_argument("data")
        comm = self.get_argument("comm")
        orgseq = self.get_argument("orgseq")
        pwds = self.get_argument("pwds")
        username = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")

        # 권한 체크
        if userauth != '99':
                is_fail = "f"
                result = "권한이 없습니다."
        else:
            aes = AESCipher(key['key'])

            monitorsql = MonitoringSql()

            # 비밀번호 체크
            auths = self.execute(monitorsql.getSuper(username, pwds))
            if not auths['rows']:
                is_fail = "f"
                result = "비밀번호가 잘못되었습니다."
            else:
                data = self.execute(monitorsql.selectMgmt(orgseq))['rows'][0][0]

                result = {"userid" : aes.decrypt(data['openstack_connect']['userid']), "password" : aes.decrypt(data['openstack_connect']['password']), "vmid" : vmid, "orgseq" : orgseq, "action" : comm, "vmtype" : "mgmtvm"}
                result = json.dumps(result)
                soap = SoapClient()

                # VNC 접속
                if (comm == 'vncconsole') :
                    result = soap.sendData("/openstack/vncconsole", "post", result)
                # 재시작
                if (comm == 'rebootserver') :
                    result = soap.sendData("/openstack/rebootserver", "post", result)
                # 재구동
                if (comm == 'resume') :
                    result = soap.sendData("/openstack/excuteServer", "post", result)
                # 정지
                if (comm == 'suspend') :
                    result = soap.sendData("/openstack/excuteServer", "post", result)

                param = "userid : %s, orgseq : %s, vmid : %s" % (username, orgseq, vmid)

                self.execute_commit(monitorsql.setCommandLog(username, '/monitor/vmHandle', comm, param))

                log.info("mgmtHandler userid : %s, userauth : %s, orgname : %s, comm : %s, orgseq : %s, vmid : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), comm, orgseq, vmid)

                is_fail = 's'

        self.write({'is_fail' : is_fail, 'descript' : result})

# E2E Trace 페이지 진입 및 정보 조회 처리
class trace_e2e_handler(tornado.web.RequestHandler, PsycopgHelper):
    # 페이지 진입 처리 함수
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        # 권한체크
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        seq = self.get_argument("seq")
        comm = self.get_argument("comm")
        org = self.get_argument("org", '0')

        monitorsql = MonitoringSql()

        result = 0
        titles = 4

        # Sync Manager
        if comm == '1' :
            result = self.execute(monitorsql.get_syncmgr_trace_end(seq))['rows'][0][0]
            titles = 1
        # Provisioning Manager
        if comm == '2' :
            result = self.execute(monitorsql.get_provisionmgr_trace_end(seq))['rows'][0][0]
            re = self.execute(monitorsql.getIsBackup(seq))['rows']
            if re:
                titles = re[0][0]
        # HA Manager
        if comm == '3' :
            result = self.execute(monitorsql.get_hamgr_trace_end(seq))['rows'][0][0]
            titles = 0

        self.render('monitoring/nfvTrace.html', seq = seq, comm = comm, end = result, titles = titles, org = org)

    # E2E Trace 정보 조회 처리 함수
    def post(self):
        seq = self.get_argument("seq")
        comm = self.get_argument("comm")
        org = self.get_argument("org", '')

        monitorsql = MonitoringSql()

        result = ''

        # Sync Manager
        if comm == '1' :
            result = self.execute(monitorsql.get_syncmgr_trace(seq, org))['rows']
        # Provisioning Manager
        if comm == '2' :
            result = self.execute(monitorsql.get_provisionmgr_trace(seq))['rows']
        # HA Manager
        if comm == '3' :
            result = self.execute(monitorsql.get_hamgr_trace(seq))['rows']

        result = {'result' : result}

        self.write(result)

# E2E Trace 상세정보 조회 처리
class trace_detail_handler(tornado.web.RequestHandler, PsycopgHelper):
    # E2E Trace 상세정보 페이지 진입 처리 함수
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        # 권한체크
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        seq = self.get_argument("seq")
        comm = self.get_argument("comm")
        reg = self.get_argument("rt")
        titles = self.get_argument("titles")

        self.render('monitoring/nfvTraceDetail.html', seq = seq, comm = comm, reg = reg, titles = titles)

    # E2E Trace 상세정보 조회 처리 함수
    def post(self):
        seq = self.get_argument("seq")
        comm = self.get_argument("comm")
        reg = self.get_argument("reg")

        monitorsql = MonitoringSql()

        result = ''

        # Sync Manager
        if comm == '1' :
            result = self.execute(monitorsql.get_syncmgr_trace_detail(seq, reg))['rows']
            if result[0][17] != None:
                result[0] = result[0] + ('rules',)
        # Provisioning Manager
        if comm == '2' :
            result = self.execute(monitorsql.get_provisionmgr_trace_detail(seq, reg))['rows']
        # HA Manager
        if comm == '3' :
            result = self.execute(monitorsql.get_hamgr_trace_detail(seq, reg))['rows']

        result = {'result' : result}

        self.write(result)


# UTM 세션 리스트 처리 함수
class contrack_handler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        vmid = self.get_argument("vmid")

        monitorsql = MonitoringSql()

        rows = self.execute(monitorsql.get_conrack(vmid))

        result = {'result' : rows['rows']}

        self.write(result)

# 룰 상세정보 조회 처리
class hw_utm_rule_handler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        utmid = self.get_argument("utmid")
        utmtype = self.get_argument("utmtype")
        orgseq = self.get_argument("orgseq")

        monitorsql = MonitoringSql()

        result = self.execute(monitorsql.get_utm_rule_detail(utmid, utmtype, orgseq))['rows']

        result = {'result' : result[0][5]}

        self.write(result)

# Horizon 접속 URL 조회 처리
class get_horizon_handler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        seq = self.get_argument("seq", '')
        username = self.get_secure_cookie("user")

        monitorsql = MonitoringSql()

        aes = AESCipher(key['key'])
        result = self.execute(monitorsql.get_horizon(seq))

        result = { 'ip' : aes.decrypt(result['rows'][0][0]['openstack_connect']['horizon']) }

        self.execute_commit(monitorsql.setCommandLog(username, '/monitor/getHorizon', 'getHorizon', ''))

        log.info("horizon userid : %s, userauth : %s, orgname : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"))

        self.write(result)

# 네트워크 정보 조회 처리
class get_vm_network_info(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        data = self.get_argument("data", '')
        seq = self.get_argument("seq", '')

        monitorsql = MonitoringSql()

        aes = AESCipher(key['key'])
        db_info = self.execute(monitorsql.get_horizon(seq))
        mysql = MySQLdb.connect(aes.decrypt(db_info['rows'][0][0]['openstack_connect']['host']),
                                aes.decrypt(db_info['rows'][0][0]['openstack_db_connect']['userid']),
                                aes.decrypt(db_info['rows'][0][0]['openstack_db_connect']['password']),
                                aes.decrypt(db_info['rows'][0][0]['openstack_db_connect']['database']))

        cur = mysql.cursor()
        cur.execute("select vm_network_info from nova.vw_vm_inventory where vm_desc = '%s'" % data)
        result = cur.fetchone()

        self.write({'result' : result[0]})

# 실행이력 정보 조회 처리
class get_action_trace_handler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        seq = self.get_argument("seq", "")
        start = self.get_argument("start", "")
        limit = self.get_argument("limit", "")
        re = self.get_argument("re", "0")
        mgr = self.get_argument("mgr", "0")
        st = self.get_argument("st", "")
        et = self.get_argument("et", "")

        monitorsql = MonitoringSql()

        trace = self.execute(monitorsql.select_trace_paging_history(seq, start, limit, re, mgr, st, et))
        trace_cnt = self.execute(monitorsql.select_trace_cnt_history(seq, re, mgr, st, et))

        self.write({'items' : trace['rows'], 'totalCount' : trace_cnt['rows']})

# 원본 룰 비교 처리
class get_rule_file(tornado.web.RequestHandler, PsycopgHelper):
    # 원본 룰 비교 페이지 진입 처리 함수
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        seq = self.get_argument("seq", "")
        reg = self.get_argument("rt", "")
        self.render('monitoring/ruleCompare.html', seq = seq, reg = reg)

    # 원본 룰 비교 정보 조회 처리 함수
    def post(self):
        seq = self.get_argument("seq", "")
        reg = self.get_argument("reg", "")

        monitorsql = MonitoringSql()

        rows = self.execute(monitorsql.select_rule_file(seq, reg))

        self.write({'rules' : rows['rows']})

# 절체/복구 알림 팝업 페이지 처리
class failover_back_handler(tornado.web.RequestHandler):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        active = self.get_argument("active")
        reg = self.get_argument("rt")

        fm = ""
        to = ""
        msg = ""
        msg2 = ""

        # 절체 요청
        if active == 'cloud':
            fm = "H/W-UTM"
            to = "NFV-UTM"
            msg = "절체"
            msg2 = "절체 시간"
        # 복구 요청
        else:
            to = "H/W-UTM"
            fm = "NFV-UTM"
            msg = "복구"
            msg2 = "복구 시간"

        self.render('monitoring/failover_back.html', fm = fm, to = to, msg = msg, msg2 = msg2, reg = reg)

# 룰 동기화 처리(Sync Manager 호출)
class callSynmgrHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        org = self.get_argument("orgseq")
        pwd = self.get_argument("pwd")
        username = self.get_secure_cookie("user")

        monitorsql = MonitoringSql()

        result = "fail"
        sendresult = ""

        auths = self.execute(monitorsql.getUserAuth(username, pwd))
        if not auths['rows']:
            result = "fail"
            sendresult = "비밀번호가 잘못되었습니다."
        else:
#         monitorsql = MonitoringSql()
            param = '{"userid" : "%s", "userauth" : "%s", "orgseq" : "%s"}' % (self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), org)
            param = json.dumps(param)

            self.execute_commit(monitorsql.setCommandLog(self.get_secure_cookie("user"), '/monitor/synmgr', '룰 동기화', param))

            log.info("callSyncMgr userid : %s, userauth : %s, orgname : %s, orgseq : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), str(org))
#         subprocess.Popen(['python', '/usr/local/SyncManager/src/SyncManager.py'])
#         subprocess.Popen(['python', '/usr/local/Sync/src/SyncManager.py', username, str(org)])
            subprocess.Popen(['/usr/local/Sync/SyncManager.sh', username, str(org)])

            result = "succ"
            sendresult = ""

        self.write({"result": result, "description": sendresult})

# UTM 생성 초기화 처리
class callSynmgrResetHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        org = self.get_argument("orgseq")
        pwd = self.get_argument("pwd")
        username = self.get_secure_cookie("user")

        monitorsql = MonitoringSql()

        result = "fail"
        sendresult = ""

        auths = self.execute(monitorsql.getUserAuth(username, pwd))
        if not auths['rows']:
            result = "fail"
            sendresult = "비밀번호가 잘못되었습니다."
        else:

#         monitorsql = MonitoringSql()
            param = "{'userid' : '%s', 'userauth' : '%s', 'orgseq' : '%s'}" % (self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), org)
            param = json.dumps(param)

            self.execute_commit(monitorsql.setCommandLog(self.get_secure_cookie("user"), '/monitor/synmgr', '룰 초기화', param))

            log.info("callSyncMgrReset userid : %s, userauth : %s, orgname : %s, orgseq : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), str(org))
#         subprocess.Popen(['python', '/usr/local/SyncManager/src/SyncManager.py'])
#         subprocess.Popen(['python', '/usr/local/Sync/src/DeleteUTM.py', username, str(org)])
            subprocess.Popen(['/usr/local/Sync/DeleteUTM.sh', username, str(org)])

            result = "succ"
            sendresult = ""

        self.write({"result": result, "description": sendresult})

# 장애 알림 팝업 처리
class errorHandler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        org = self.get_argument("orgseq")
        reg = self.get_argument("rt")
        fm = self.get_argument("fm")
        infotype = self.get_argument("infotype")
        user_type = self.get_argument("user_type", '')
        msg = ''
        ins = ''

        monitorsql = MonitoringSql()

        org = self.execute(monitorsql.getOrgName(org))['rows'][0][0]

        if infotype == '1':
            ins = '인프라 동작 상태'
            msg = 'off'
        elif infotype == '2':
            ins = '스위치'
            msg = 'down'
        elif infotype == '3':
            if user_type == '1':
                ins = '고객 서비스 상태'
                msg = 'DEACTIVE'
            else:
                ins = '관리 VM'
                msg = 'DEACTIVE'
        elif infotype == '4':
            ins = '고객 서버팜'
            msg = 'off'
        elif infotype == '5':
            ins = 'H/W-UTM'
            msg = 'off'
        elif infotype == '6':
            ins = 'NFV-UTM'
            msg = 'off'
        elif infotype == '7':
            ins = '스위치'
            msg = 'off'
        elif infotype == '9':
            ins = '프로세스'
            msg = 'off'

        self.render('monitoring/error_pop.html', org = org, fm = fm, msg = msg, reg = reg, ins = ins)

# 룰 동기화 이상 탭 처리
class get_sync_trace_handler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        org = self.get_argument("org", "")
        start = self.get_argument("start", "")
        limit = self.get_argument("limit", "")

        monitorsql = MonitoringSql()

        trace = self.execute(monitorsql.select_sync_trace_paging_history(org, start, limit))
        trace_cnt = self.execute(monitorsql.select_sync_trace_cnt_history(org))

        self.write({'items' : trace['rows'], 'totalCount' : trace_cnt['rows']})

# 룰 동기화 이상 상세정보 팝업 처리
class getRuleDetailhandler(tornado.web.RequestHandler, PsycopgHelper):
    # 룰 동기화 이상 상세정보 팝업 페이지 진입 처리 함수
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        seq = self.get_argument("seq")
        reg = self.get_argument("rt")

        self.render('monitoring/nfvRuleDetail.html', seq = seq, reg = reg)

    # 룰 동기화 이상 상세정보 조회 함수
    def post(self):
        seq = self.get_argument("seq")
        reg = self.get_argument("reg")

        monitorsql = MonitoringSql()

        result = self.execute(monitorsql.getRuleDetail(seq, reg))['rows']

        result = {'result' : result}

        self.write(result)

# 절체/복구 요청 기능 처리
class haCallHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):
        user = self.get_secure_cookie("user")
        userauth = self.get_secure_cookie("userauth")
        data = self.get_argument('data', '')
        office = self.get_argument('office')
        ip = self.get_argument('ip')
        msg = self.get_argument('msg', '')

        if str(userauth) == '0' or str(userauth) == '99':
            monitorsql = MonitoringSql()

            # 절체 요청
            if data == 'failover':
                url = '/service/failover'
                body = json.dumps({"failover" : { "officecode" : office, "axgateip" : ip, "failmsg" : msg, "userid" : user, "request" : "NFV"}})
                self.execute_commit(monitorsql.setCommandLog(self.get_secure_cookie("user"), '/monitor/hacall', '절체', body))
            # 복구 요청
            else:
                url = '/service/failback'
                body = json.dumps({"failback" : { "officecode" : office, "axgateip" : ip, "failmsg" : msg, "userid" : user, "request" : "NFV"}})
                self.execute_commit(monitorsql.setCommandLog(self.get_secure_cookie("user"), '/monitor/hacall', '복구', body))

            log.info("Ha-mgr userid : %s, userauth : %s, orgname : %s, comm : %s, body : %s", self.get_secure_cookie("user"), self.get_secure_cookie("userauth"), self.get_secure_cookie("orgname"), data, body)

            soap = SoapClient()
            result = soap.sendHaData(url, "post", body)
        else:
            body = ''
            result = ''
        self.write({"result" : ((body, result),)})

# 절체/복구 요청 팝업 처리
class failHandler(tornado.web.RequestHandler, PsycopgHelper):
    # 절체/복구 요청 팝업 페이지 진입 처리 함수
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        orgs = self.get_argument('orgs')
        types = self.get_argument('types')
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')

        monitorsql = MonitoringSql()
        office = self.execute(monitorsql.getOffice(orgs))['rows'][0][0]
        self.render('monitoring/fail.html', orgs = orgs, types = types , office = office)

    # 절체/복구를 위한 UTM 조회 함수
    def post(self):
        orgs = self.get_argument('orgs')
        types = self.get_argument('types')

        monitorsql = MonitoringSql()

        result = []
        fails = self.execute(monitorsql.getFail(orgs, types))
        for row in fails['rows']:
            row = dict(zip(fails['columns'], row))
            result.append(row)
        fails = json.dumps(result)

        fails = {'fails' : fails}

        self.write(fails)

# 룰 동기화 이상 팝업 페이지 진입 처리
class ruleCompareErrorHandler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        orgs = self.get_argument('orgs')
        ts = self.get_argument('ts')

        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')

        self.render('monitoring/ruleCompareError.html', orgs = orgs, ts = ts)

# 룰 동기화 이상 팝업 데이터 조회 처리
class ruleCompareErrorListHandler(tornado.web.RequestHandler, PsycopgHelper):
    def get(self):
        userauth = self.get_secure_cookie("userauth")
        if userauth != '0' and userauth != '1' and userauth != '99' :
            self.redirect('/error')
        orgs = self.get_argument("orgs", "")
        ts = self.get_argument("ts", "")
        start = self.get_argument("start", "")
        limit = self.get_argument("limit", "")

        monitorsql = MonitoringSql()

        trace = self.execute(monitorsql.select_rule_error(orgs, ts, start, limit))
        trace_cnt = self.execute(monitorsql.select_rule_error_cnt(orgs, ts))

        self.write({'items' : trace['rows'], 'totalCount' : trace_cnt['rows']})