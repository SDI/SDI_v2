#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class ApplianceSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getDtlcode(self):
        result = "select distinct dtlcode,dtlname from tb_neclscode where 1=1"
        
        return result
    
    def getDtl(self):
        result = "select * from tb_neclscode where 1=1"
        
        return result
    
    def getAppRow(self, dtlcode, necatNm, appNm, serviceNm, serverSpec, startdt, enddt):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.necatseq) AS R
                    from tb_necat a
                    inner join tb_neclscode b on a.neclsseq = b.neclsseq
                    where a.del_dttm is null"""
        if not dtlcode == "0" :
            result = result + " and b.dtlcode = '%s'" % dtlcode
        if not necatNm == "" :
            result = result + " and b.neclsseq = '%s'" % necatNm
        if not appNm == "" :
            result = result + " and a.application_name like '%s'" % appNm
        if not serviceNm == "" :
            result = result + " and a.service_name like '%s'" % serviceNm
        if not serverSpec == "" :
            result = result + " and a.spec_vm = '%s'" % serverSpec
        if not startdt == "" :
            result = result + " and a.reg_dttm >= '%s'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and a.reg_dttm <= '%s'::timestamp" % enddt
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getApp(self, dtlcode, necatNm, appNm, serviceNm, serverSpec, startdt, enddt, start):
        result = """select a.necatseq, a.service_name, a.application_name, a.spec_vm, a.neclsseq, a.reg_dttm::text, b.dtlcode, b.description
                    from tb_necat a
                    inner join tb_neclscode b on a.neclsseq = b.neclsseq
                    where a.del_dttm is null"""
        if not dtlcode == "0" :
            result = result + " and b.dtlcode = '%s'" % dtlcode
        if not necatNm == "" :
            result = result + " and b.neclsseq = '%s'" % necatNm
        if not appNm == "" :
            result = result + " and a.application_name like '%s'" % appNm
        if not serviceNm == "" :
            result = result + " and a.service_name like '%s'" % serviceNm
        if not serverSpec == "" :
            result = result + " and a.spec_vm = '%s'" % serverSpec
        if not startdt == "" :
            result = result + " and a.reg_dttm >= '%s'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and a.reg_dttm <= '%s'::timestamp" % enddt
        result = result + " order by necatseq desc LIMIT 15 OFFSET '%s'" % start
        return result
    
    def setServiceInsert(self, name, neclsseq, icon_small, icon_large, servicename, imagename, service):
        result = """insert into tb_necat (modelname, neclsseq, price_monthly, spec_thruput, spec_sessions, service_name, icon_small, icon_large,
                    running_image, config_props, reg_dttm, application_name, monitor_yn) 
                    values ('%s', %s, '2000', '2G', '5000', '%s', '%s', '%s', '%s', '%s'::json, CURRENT_TIMESTAMP, '%s', 'N')
                    """ % (name, neclsseq, servicename, icon_small, icon_large, imagename, service, name)
                    
        return result
    
    def setTemplateUpdate(self, necatseq):
        result = "UPDATE tb_necat SET del_dttm = CURRENT_TIMESTAMP WHERE necatseq = '%s'" % necatseq
        return result
    
    def setOrgUpdateAddr(self, orgseq, addrseq):
        result = """update tb_org set addrseq = '%s' where orgseq = '%s'""" % (addrseq, orgseq)
        return result
    
    def getOrgDetail(self, orgseq):
        result = """SELECT a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a join tb_addr b on a.orgseq = b.orgseq where 1=1
                    and a.orgseq = '%s'""" % orgseq
        return result
    
    def setOrgUpdate(self, orgseq, orgname):
        result = "update tb_org set orgname = '%s' where orgseq = '%s'" % (orgname, orgseq)
        return result
    
    def setAddrDelete(self, orgseq):
        result = "update tb_addr set orgseq = 0 where orgseq = '%s'" % orgseq
        return result
    
    def getNecat(self, necatSeq):
        result = "select config_props from tb_necat where necatseq = '%s'" % necatSeq
        return result
    