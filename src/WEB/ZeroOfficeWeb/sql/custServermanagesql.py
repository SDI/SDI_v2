#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class CustServerSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getCustServerRow(self, userid, servername, server_type, orgseq, custname):
        result = """select distinct count(csi.serverseq)
                    from tb_customer_server_info as csi
                    inner join tb_customer as cust
                    on csi.customerseq = cust.customerseq
                    inner join tb_user as use
                    on use.customerseq = csi.customerseq
                    inner join tb_org as org
                    on use.orgseq = org.orgseq
                    inner join tb_user as auth
                    on org.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1 """
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not servername == "" :
            result = result + " and csi.servername like '%s'" % servername
        if not server_type == "" :
            result = result + " and csi.server_type like '%s'" % server_type
        if not orgseq == 0 :
            result = result + " and org.orgseq = '%s'" % orgseq
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        return result

    def getCustServer(self, userid, servername, server_type, start, orgseq, custname):
        result = """select distinct csi.serverseq, csi.customerseq, cust.customername, csi.servername, csi.server_ip, csi.public_ip_yn,
                    csi.server_type, csi.locat, csi.spec, csi.note, csi.state,
                    to_char(csi.store_dttm, 'yyyy-mm-dd') as store_dttm, to_char(csi.release_dttm, 'yyyy-mm-dd') as release_dttm, org.orgname
                    from tb_customer_server_info as csi
                    inner join tb_customer as cust
                    on csi.customerseq = cust.customerseq
                    inner join tb_user as use
                    on use.customerseq = csi.customerseq
                    inner join tb_org as org
                    on use.orgseq = org.orgseq
                    inner join tb_user as auth
                    on org.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1 """
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not servername == "" :
            result = result + " and csi.servername like '%s'" % servername
        if not server_type == "" :
            result = result + " and csi.server_type like '%s'" % server_type
        if not orgseq == 0 :
            result = result + " and org.orgseq = '%s'" % orgseq
        if not custname == "" :
            result = result + " and cust.customername like '%s'" % custname
        result = result + " ORDER BY csi.serverseq DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getCust(self):
        result = "select distinct customerseq, customername from tb_customer order by customerseq desc"
        return result

    def setCustServerInsert(self, serverName, insertCust, serverIp, publicIpYn, serverType, state, storeDttm, releaseDttm, locat, spec, note):
        result = """insert into tb_customer_server_info
                    (customerseq, servername, server_ip, public_ip_yn,
                    server_type, locat, spec, note, reg_dttm, update_dttm, state, """
        if state == "i":
            result = result + "store_dttm)"
        if state == "o":
            result = result + "release_dttm)"
        result = result + " values('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', now(), now(), '%s', " % (insertCust, serverName, serverIp, publicIpYn, serverType, locat, spec, note, state)
        if state == "i":
            result = result + " '%s')" % storeDttm
        if state == "o":
            result = result + " '%s')" % releaseDttm
        return result

    def setCustServerModify(self, serverseq, serverName, insertCust, serverIp, publicIpYn, serverType, state, storeDttm, releaseDttm, locat, spec, note):
        result = """update tb_customer_server_info set customerseq = '%s',  servername = '%s', server_ip = '%s', public_ip_yn = '%s',
                    server_type = '%s', locat = '%s', spec = '%s', note = '%s', update_dttm = now(),
                    state = '%s', """% (insertCust, serverName, serverIp, publicIpYn, serverType, locat, spec, note, state)
        if state == "i":
            result = result + "store_dttm = '%s'" % storeDttm
        if state == "o":
            result = result + "release_dttm = '%s'" % releaseDttm
        result = result + " where serverseq = '%s'" % serverseq

        return result

    def setServerDelete(self, serverseq):
        result = "delete from tb_customer_server_info where serverseq = '%s'" % serverseq
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result