# -*- coding: utf-8 -*-
'''
Created on 2015. 1. 14.

@author: 진수
'''
from time import strptime
import time


class MonitoringSql(object):
    '''
    DB 쿼리를 집합.
    '''


    def __init__(self):
        '''
        Constructor
        '''
    def selectOrgTree(self, userid):
        result = """select
                        org.orgseq, org.orgname
                    from tb_org as org
                        inner join tb_user as use
                            on org.orgseq = use.orgseq
                            or use.userauth = 0
                            or use.userauth = 99
                    where pop_yn = 'Y'
                        and use.userid = '%s'
                    group by org.orgseq, org.orgname
                    order by org.orgseq""" % userid
        return result

    def insertOpenstackHostStateInfo(self, createTime, orgseq, host, hostip, service, component, processcount):
        result = """INSERT
                    INTO tb_monitor_openstack_host_state_info
                        (create_time, orgseq, host, hostip, service, component, pcount)
                    VALUES
                        (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, '%s', '%s', '%s', '%s', %d)""" % (createTime, orgseq, host, hostip, service, component, processcount )
        return result

    def insertOpenstackSwitchStateInfo(self, createTime, orgseq, switchName, ipaddr, port, ifOperStatus, ifType, ifSpeed, ifName):
        result = """INSERT
                    INTO tb_monitor_openstack_switch_state_info
                        (create_time, orgseq, switchname, ipaddr, port, ifoperstatus,iftype, ifspeed, ifname)
                    VALUES
                        (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, '%s', '%s', %d, %d, %d, %f, '%s')""" %(createTime, orgseq, switchName, ipaddr, port, ifOperStatus, ifType, ifSpeed, ifName )

        return result

    def insertOpenstackInstanceStateInfo(self, createtime, orgseq, vmid, status, vmname, hostname):
        result = """INSERT
                    INTO tb_monitor_openstack_instance_state_info
                        (create_time, orgseq, vmid, status, vmname, host)
                    VALUES
                        (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), '%s', '%s', '%s', '%s', '%s')""" %(createtime, orgseq, vmid, status, vmname, hostname)
        return result

    def selectOpenstackHostStateInfoMnnode(self, orgseq):
        result = """select
                        coh.cnt, oh.host, oh.service, oh.component, oh.pcount, oh.hostip
                    from tb_monitor_openstack_host_state_info as oh
                        inner join
                            (select
                                ct.host, ct.service, ct.orgseq, count(*) as cnt
                            from tb_monitor_openstack_host_state_info as ct
                            where ct.create_time =
                                    (select max(create_time) from tb_monitor_openstack_host_state_info where orgseq = %s)
                                and ct.orgseq = %s
                            group by ct.host, ct.service, ct.orgseq) as coh
                            on oh.host = coh.host
                                and oh.service = coh.service
                    where oh.create_time =
                        (select max(create_time) from tb_monitor_openstack_host_state_info where orgseq = %s)
                        and oh.orgseq = %s
                    group by coh.cnt, oh.host, oh.service, oh.component, oh.pcount, oh.orgseq, oh.hostip
                    order by oh.service""" % (orgseq, orgseq, orgseq, orgseq)
        return result

    def selectOpenstackInstanceStateInfoCnode(self, orgseq):
        result = """select
                        CASE
                            WHEN ne.provisionseq is null
                                THEN 1
                            ELSE 2
                        END as is_seq,
                        ne.provisionseq, oisi.host, oisi.vmid,
                        oisi.vmname, oisi.status, use.userid,
                        case
                            when mg.utm_userid is null
                                then cust.customername
                            else mo.companyname
                        end as customername,
                        org.orgname, cust.detailaddr, cust.email,
                        cust.telnum, oisi.productname, oisi.conntrackcnt, oisi.createvm,
                        array_to_string(array_agg(v.vlan order by v.vlan),'/')
                    from tb_monitor_openstack_instance_state_info as oisi
                        left join tb_ne as ne
                            on oisi.vmid = ne.vm_id
                        left join tb_nsprovisioning as ns
                            on ne.provisionseq = ns.nsprovisioningseq
                        left join tb_user as use
                            on ns.userid = use.userid and use.orgseq = oisi.orgseq
                        left join tb_customer as cust
                            on use.userid = cust.userid
                        left join tb_org as org
                            on org.orgseq = use.orgseq
                        left join tb_mg_utm as mg
                            on use.userid = mg.utm_userid
                        left join tb_mss_order as mo
                            on mg.utm_mappid = mo.utm_mappid
                        left join tb_vlan as v
                            on v.allocate_yn = 'Y' and v.userid = use.userid and v.orgseq = oisi.orgseq
                    where oisi.create_time = (select max(create_time) from tb_monitor_openstack_instance_state_info where orgseq = %s)
                        and oisi.orgseq = %s
                    group by ne.provisionseq, oisi.productname, oisi.host, oisi.vmid, oisi.vmname,
                        oisi.status, use.userid, cust.customername, org.orgname, cust.detailaddr,
                        cust.email, cust.telnum, oisi.conntrackcnt, oisi.createvm, mg.utm_userid,
                        mg.utm_mappid, mo.companyname""" % (orgseq, orgseq)
        return result

    def getHostMnnodeServiceRow(self, orgseq):
        result = """select
                        service
                    from (select * from tb_monitor_openstack_host_state_info order by service) a
                    WHERE 1=1
                        and a.orgseq = %s
                        and a.host = 'mnnode'
                    group BY service""" % (orgseq)
        return result

    def selectOpenstackSwitchStateInfoMnnode(self, orgseq):
        result = """select
                        ipaddr, switchname, port, ifoperstatus, iftype,
                        ifspeed, ifname
                    from tb_monitor_openstack_switch_state_info
                    where create_time = (select max(create_time) from tb_monitor_openstack_switch_state_info where orgseq = %s)
                        and orgseq = '%s'
                        and port < 49
                    order by switchname, port""" % (orgseq, orgseq)
        return result

    def selectOpenstackInstanceStateInfo(self, orgseq):
        result = """select
                        vmid, status, vmname, host
                    from tb_monitor_openstack_instance_state_info as oi
                    where oi.create_time = (select max(create_time) from tb_monitor_openstack_instance_state_info where orgseq = %s)
                        and not vmid in (select
                                            vm_id
                                        from tb_ne as ne
                                            inner join tb_nsprovisioning as ns
                                                on ne.provisionseq = ns.nsprovisioningseq
                                                    and ns.del_dttm is null
                                                    and ns.procstatecode != '0')
                    and orgseq = %s""" % (orgseq, orgseq)
        return result

    def selectProvisionSeq(self, orgseq):
        result = """select
                        ne.provisionseq, oisi.host, oisi.vmid, oisi.vmname
                    from tb_ne as ne
                        inner join tb_monitor_openstack_instance_state_info as oisi
                            on oisi.vmid = ne.vm_id
                                and oisi.create_time = (select max(create_time) from tb_monitor_openstack_instance_state_info where orgseq = %s)
                        inner join tb_nsprovisioning as ns
                            on ne.provisionseq = ns.nsprovisioningseq
                                and ns.del_dttm is null
                                and ns.procstatecode != '0'
                                and ns.orgseq = %s""" % (orgseq, orgseq)
        return result

    def selectResouceID(self, pseq):
        result = """select
                        pt.message, pt.response, config_props, use.userid, use.username,
                        cust.customername, org.orgname, cust.email, cust.telnum, cust.detailaddr
                    from tb_provisionmgr_trace as pt
                        inner join tb_ne as ne
                            on ne.provisionseq = pt.provisionseq
                        inner join tb_nsprovisioning as ns
                            on pt.provisionseq = ns.nsprovisioningseq
                        inner join tb_user as use
                            on use.userid = ns.userid and ns.orgseq = use.orgseq
                        inner join tb_customer as cust
                            on use.userid = cust.userid
                        inner join tb_org as org
                            on use.orgseq = org.orgseq
                    where pt.provisionseq = %s
                    order by pt.reg_dttm""" % pseq
        return result

    def selectUserPass(self, vmid):
        result = """select
                        CASE
                            WHEN ns.userid is null
                                THEN 'admin' ELSE ns.userid END,
                    (select convert_from(decrypt(decode((select tu.password from tb_user as tu where tu.userid = ns.userid and oisi.orgseq = tu.orgseq), 'hex'), 'gigaoffice!234', '3des'),
                    'SQL_ASCII')), oisi.vmid, cust.customername
                    from tb_monitor_openstack_instance_state_info as oisi
                    left join tb_ne as ne
                    on oisi.vmid = ne.vm_id
                    left join tb_nsprovisioning as ns
                    on ne.provisionseq = ns.nsprovisioningseq
                    left join tb_user as us
                    on us.userid = ns.userid and oisi.orgseq = us.orgseq
                    left join tb_customer as cust
                    on us.userid = cust.userid
                    where vmid = '%s'
                    group by ns.userid, password, oisi.vmid, cust.customername, oisi.orgseq""" % vmid
        return result

    def selectMgmt(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s""" % orgseq
        return result

    def selectServerFarm(self, orgseq):
        result = """select b.orgname, cust.customername, a.servername, a.server_ip, a.server_type,
                            to_char(a.store_dttm, 'YYYY-MM-DD HH24:MM:SS') AS store_dttm ,
                            to_char(a.release_dttm, 'YYYY-MM-DD HH24:MM:SS') AS release_dttm, c.status
                    from tb_customer_server_info a
                    inner join tb_customer cust on a.customerseq = cust.customerseq
                    inner join tb_user us on cust.userid = us.userid and us.orgseq = %s
                    inner join tb_org b on us.orgseq = b.orgseq
                    left join tb_monitor_customer_server_state_info c on a.server_ip = c.serverip
                    and create_time = (select max(create_time) from tb_monitor_customer_server_state_info where orgseq = %s)
                    where b.orgseq = %s
                    order by create_time desc""" % (orgseq, orgseq, orgseq)
        return result

    def select_monitoring_customer(self, orgseq):
        result = """select a.companyname, a.zone_id, a.ax_gate_ip,
                    to_char(a.event_date, 'YYYY-MM-DD HH24:MM:SS') AS event_date
                    from tb_mss_order a
                    join tb_org b on a.officecode = b.officecode
                    where orgseq = %s""" % orgseq
        return result

    def select_monitoring_hwutm(self, orgseq):
        result = """select
                        sa.appliance_name, sa.ip_addr, mo.companyname, sa.status,
                        to_char(dhcp.parsing_cnt, '999,999'), to_char(dnat.parsing_cnt, '999,999'),to_char(snat.parsing_cnt, '999,999'), mg.utm_userid,
                        case
                            when dhcp.rule_file_name is not null
                                then dhcp.rule_file_name
                            when dnat.rule_file_name is not null
                                then dnat.rule_file_name
                            when snat.rule_file_name is not null
                                then snat.rule_file_name
                            when firewall.rule_file_name is not null
                                then firewall.rule_file_name
                            when router.rule_file_name is not null
                                then router.rule_file_name
                            when incoming.rule_file_name is not null
                                then incoming.rule_file_name
                            when outgoing.rule_file_name is not null
                                then outgoing.rule_file_name
                            when inter.rule_file_name is not null
                                then inter.rule_file_name
                        else ''
                        end,
                        mo.zone_id, firewall.parsing_cnt, to_char(router.parsing_cnt, '999,999'),
                        mo.ax_gate_vd_label, mo.ax_gate_vd,
                        to_char(incoming.parsing_cnt, '999,999'), to_char(outgoing.parsing_cnt, '999,999'), to_char(inter.parsing_cnt, '999,999')
                    from tb_monitor_sdn_appliance_state_info sa
                        inner join tb_mss_order mo
                            on sa.ip_addr = mo.ax_gate_ip
                        left join tb_mg_utm as mg
                            on mo.utm_mappid = mg.utm_mappid
                        left join tb_syncmgr_trace dhcp
                            on mg.utm_userid = dhcp.utm_userid
                                and dhcp.parsing_type = '1'
                                and dhcp.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace dnat
                            on mg.utm_userid = dnat.utm_userid
                                and dnat.parsing_type = '2'
                                and dnat.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace snat
                            on mg.utm_userid = snat.utm_userid
                                and snat.parsing_type = '3'
                                and snat.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace firewall
                            on mg.utm_userid = firewall.utm_userid
                                and firewall.parsing_type = '4'
                                and firewall.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace router
                            on mg.utm_userid = router.utm_userid
                                and router.parsing_type = '5'
                                and router.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace incoming
                            on mg.utm_userid = incoming.utm_userid
                                and incoming.parsing_type = '6'
                                and incoming.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace outgoing
                            on mg.utm_userid = outgoing.utm_userid
                                and outgoing.parsing_type = '7'
                                and outgoing.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                        left join tb_syncmgr_trace inter
                            on mg.utm_userid = inter.utm_userid
                                and inter.parsing_type = '8'
                                and inter.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = sa.orgseq)
                    where create_time = (select max(create_time) from tb_monitor_sdn_appliance_state_info where orgseq = %s)
                        and sa.orgseq = %s
                    group by sa.appliance_name, sa.ip_addr, mo.companyname, sa.status,
                        dhcp.rule_file_name, dhcp.parsing_cnt,
                        dnat.rule_file_name, dnat.parsing_cnt,
                        snat.rule_file_name, snat.parsing_cnt,
                        mg.utm_userid, mo.zone_id,
                        firewall.rule_file_name, firewall.parsing_cnt,
                        router.rule_file_name, router.parsing_cnt,
                        mo.ax_gate_vd_label, mo.ax_gate_vd,
                        incoming.parsing_cnt, incoming.rule_file_name,
                        outgoing.parsing_cnt, outgoing.rule_file_name,
                        inter.parsing_cnt, inter.rule_file_name""" % (orgseq, orgseq)
        return result

    def select_monitor_sdn(self, orgseq):
        result = """select appliance_name, ip_addr, status
                    from tb_monitor_sdn_appliance_state_info
                    where create_time = (select max(create_time) from tb_monitor_sdn_appliance_state_info where orgseq = %s)""" % orgseq
        return result

    def select_failover(self, ipaddr):
        result = """select axgateip, failmsg, resultmsg, mssuserid,
                    case failovertype
                    when '1' then '절체'
                    when '2' then '복귀'
                    END as failovertype,
                    to_char(regdt, 'YYYY-MM-DD HH24:MM:SS') AS regdt
                    from tb_failover_info
                    where axgateip = \'%s\'""" % ipaddr
        return result

    def select_trace_history(self, orgseq):
        result = """select
                    case
                    when th.trace_type = 1 then 'Sync-mgr'
                    when th.trace_type = 2 then 'Prov-mgr'
                    when th.trace_type = 3 then 'HA-mgr'
                    end,
                    th.step_msg, to_char(th.start_dttm, 'MM월DD일 HH24:MI:SS'), to_char(th.end_dttm, 'MM월DD일 HH24:MI:SS'),
                    to_char(th.end_dttm - th.start_dttm, 'MI분 SS초'),
                    th.histseq, th.traceseq, th.trace_type, use.userid, use.username, cust.customername,
                    case
                    when upper(th.response_status) = 'S' then '성공'
                    when upper(th.response_status) = 'F' then '실패'
                    else '진행중'
                    end,
                    th.caller, th.callee,
                    case
                    when (th.end_dttm - th.start_dttm) >= '00:00:00' and (th.end_dttm - th.start_dttm) < '00:01:00' then 'green'
                    when (th.end_dttm - th.start_dttm) >= '00:01:00' and (th.end_dttm - th.start_dttm) < '00:02:00' then '#FFC500'
                    when (th.end_dttm - th.start_dttm) >= '00:02:00' and (th.end_dttm - th.start_dttm) < '00:03:00' then '#E86F0B'
                    when (th.end_dttm - th.start_dttm) >= '00:03:00' then '#E86F0B'
                    else 'black'
                    end
                    from tb_trace_history as th
                    left join tb_nsprovisioning as ns
                    on th.trace_type = 2 and th.traceseq = ns.nsprovisioningseq
                    left join tb_user as use
                    on ns.userid = use.userid
                    left join tb_customer as cust
                    on cust.userid = use.userid
                    where th.orgseq = %s""" % orgseq
        return result

    def get_syncmgr_trace(self, seq, org):
        result = """select
                    syncseq, mainclass, subclass, message,
                    request_cmd, request_parameter, response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'),
                    axgate_job, thread_job, provisionseq, indent,
                    case
                    when (end_dttm - start_dttm) >= '00:00:00' and (end_dttm - start_dttm) < '00:01:00' then 'green'
                    when (end_dttm - start_dttm) >= '00:01:00' and (end_dttm - start_dttm) < '00:02:00' then '#FFC500'
                    when (end_dttm - start_dttm) >= '00:02:00' and (end_dttm - start_dttm) < '00:03:00' then '#E86F0B'
                    when (end_dttm - start_dttm) >= '00:03:00' then '#E86F0B'
                    else 'black'
                    end
                    from tb_syncmgr_trace
                    where syncseq = '%s' and (orgseq = %s or orgseq is null)
                    order by axgate_job, thread_job, provisionseq, reg_dttm""" % (seq, org)
        return result

    def get_syncmgr_trace_detail(self, seq, reg):
        result = """select
                    syncseq, mainclass, subclass, message,
                    request_cmd, request_parameter, response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'),
                    axgate_job, thread_job, provisionseq,
                    case
                    when parsing_type = '1' then 'DHCP'
                    when parsing_type = '2' then 'DNAT'
                    when parsing_type = '3' then 'SNAT'
                    when parsing_type = '4' then 'FIREWALL'
                    when parsing_type = '5' then 'ROUTER'
                    when parsing_type = '6' then 'FW[INCOMING]'
                    when parsing_type = '7' then 'FW[OUTGOING]'
                    when parsing_type = '8' then 'FW[INTERZONE]'
                    else parsing_type end,
                    rule_file_name
                    from tb_syncmgr_trace
                    where syncseq = '%s' and reg_dttm = '%s'
                    order by axgate_job, thread_job, provisionseq, reg_dttm""" % (seq, reg)
        return result

    def get_hamgr_trace(self, seq):
        result = """select
                    haseq, mainclass, subclass, message,
                    request_cmd, request_parameter, response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'), '0',
                    case
                    when (end_dttm - start_dttm) >= '00:00:00' and (end_dttm - start_dttm) < '00:01:00' then 'green'
                    when (end_dttm - start_dttm) >= '00:01:00' and (end_dttm - start_dttm) < '00:02:00' then '#FFC500'
                    when (end_dttm - start_dttm) >= '00:02:00' and (end_dttm - start_dttm) < '00:03:00' then '#E86F0B'
                    when (end_dttm - start_dttm) >= '00:03:00' then '#E86F0B'
                    else 'black'
                    end
                    from tb_hamgr_trace
                    where haseq = '%s'
                    order by reg_dttm""" % seq
        return result

    def get_hamgr_trace_detail(self, seq, reg):
        result = """select
                    haseq, mainclass, subclass, message,
                    request_cmd, request_parameter, response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US')
                    from tb_hamgr_trace
                    where haseq = '%s' and reg_dttm = '%s'
                    order by reg_dttm""" % (seq, reg)
        return result

    def get_provisionmgr_trace(self, seq):
        result = """select
                    provisionseq, mainclass, subclass, message,
                    '' as request_cmd, '' as request_parameter, '' as response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, '' as message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'), '0',
                    case
                    when (end_dttm - start_dttm) >= '00:00:00' and (end_dttm - start_dttm) < '00:01:00' then 'green'
                    when (end_dttm - start_dttm) >= '00:01:00' and (end_dttm - start_dttm) < '00:02:00' then '#FFC500'
                    when (end_dttm - start_dttm) >= '00:02:00' and (end_dttm - start_dttm) < '00:03:00' then '#E86F0B'
                    when (end_dttm - start_dttm) >= '00:03:00' then '#E86F0B'
                    else 'black'
                    end
                    from tb_provisionmgr_trace
                    where provisionseq = '%s'
                    order by reg_dttm""" % seq
        return result

    def get_provisionmgr_trace_detail(self, seq, reg):
        result = """select
                    provisionseq, mainclass, subclass, message,
                    request_cmd, request_parameter, response,
                    to_char(start_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm, 'mm-dd HH24:MI:SS'), to_char(end_dttm - start_dttm, 'MI분 SS초'),
                    case when response_status = 't' then '성공' else '실패' end, message_detail, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US')
                    from tb_provisionmgr_trace
                    where provisionseq = '%s' and reg_dttm = '%s'
                    order by reg_dttm""" % (seq, reg)
        return result

    def get_syncmgr_trace_end(self, seq):
        result = """select count(*) from tb_syncmgr_trace where syncseq = '%s' and message = 'end'""" % seq
        return result

    def get_hamgr_trace_end(self, seq):
        result = """select count(*) from tb_hamgr_trace where haseq = %s and message = 'end'""" % seq
        return result

    def get_provisionmgr_trace_end(self, seq):
        result = """select count(*) from tb_provisionmgr_trace where provisionseq = %s and message = 'end'""" % seq
        return result

    def getIsBackup(self, seq):
        result = """select u.userauth
                    from tb_nsprovisioning as ns
                    inner join tb_user as u
                    on ns.userid = u.userid
                    where nsprovisioningseq = '%s'""" % seq
        return result

    def get_console_url(self, vmid):
        result = """select console_url from tb_ne where vm_id = '%s'""" % vmid
        return result

    def get_horizon(self, seq):
        result = """select infraprops
                    from tb_openstack_infra
                    where orgseq = %s""" % seq
        return result

    def get_conrack(self, vmid):
        result = """select conntracklist
                    from tb_monitor_openstack_instance_state_info
                    where vmid = '%s'
                    order by create_time desc limit 1""" % vmid
        return result

    def get_utm_rule_detail(self, utmid, utmtype, orgseq):
        result = """select sa.appliance_name, sa.ip_addr, mo.companyname, sa.status,
                    st.message_detail, st.rule_file_name, st.parsing_type, st.utm_userid,
                    to_char(st.reg_dttm, 'YYYY-MM-DD HH24:MM:SS') AS regdttm
                    from tb_monitor_sdn_appliance_state_info sa
                    join tb_mss_order mo on sa.ip_addr = mo.ax_gate_ip
                    join tb_mg_utm as mg on mg.utm_mappid = mo.utm_mappid
                    join tb_syncmgr_trace st on mg.utm_userid = st.utm_userid
                    where create_time = (select max(create_time) from tb_monitor_sdn_appliance_state_info where orgseq = %s)
                    and st.utm_userid = '%s'
                    and st.parsing_type = '%s'
                    and sa.orgseq = '%s'
                    order by reg_dttm desc limit 1""" % (orgseq, utmid, utmtype, orgseq)
        return result

    def select_trace_paging_history(self, orgseq, start, limit, re, mgr, st, et):
        result = """select
                    case
                    when th.trace_type = 1 then 'Sync-mgr'
                    when th.trace_type = 2 then 'Prov-mgr'
                    when th.trace_type = 3 then 'HA-mgr'
                    end,
                    th.step_msg, to_char(th.start_dttm, 'MM-DD HH24:MI:SS'), to_char(th.end_dttm, 'MM-DD HH24:MI:SS'),
                    to_char(th.end_dttm - th.start_dttm, 'MI분 SS초'),
                    th.histseq, th.traceseq, th.trace_type, use.userid, use.username,
                    case
                    when cust.customername is not null then cust.customername
                    when mss.companyname is not null then mss.companyname
                    else ''
                    end as customername,
                    case
                    when upper(th.response_status) = 'S' then '성공'
                    when upper(th.response_status) = 'F' then '실패'
                    else '진행중'
                    end,
                    case
                    when uu.username is null then th.caller
                    else uu.username
                    end as caller, th.callee,
                    case
                    when (th.end_dttm - th.start_dttm) >= '00:00:00' and (th.end_dttm - th.start_dttm) < '00:01:00' then 'green'
                    when (th.end_dttm - th.start_dttm) >= '00:01:00' and (th.end_dttm - th.start_dttm) < '00:02:00' then '#FFC500'
                    when (th.end_dttm - th.start_dttm) >= '00:02:00' and (th.end_dttm - th.start_dttm) < '00:03:00' then '#E86F0B'
                    when (th.end_dttm - th.start_dttm) >= '00:03:00' then '#E86F0B'
                    else 'black'
                    end
                    from tb_trace_history as th
                    left join tb_nsprovisioning as ns
                    on th.trace_type = 2 and th.traceseq = ns.nsprovisioningseq
                    left join tb_user as use
                    on ns.userid = use.userid and use.orgseq = th.orgseq
                    left join tb_customer as cust
                    on cust.userid = use.userid
                    left join tb_mg_utm as mg
                    on use.userid = mg.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    left join tb_user as uu
                    on uu.userid = th.caller
                    where 1=1"""
        if re == '1':
            result = result + " and upper(th.response_status) = 'S'"
        elif re == '2':
            result = result + " and upper(th.response_status) = 'F'"
        if mgr == '1':
            result = result + " and th.trace_type = 1"
        elif mgr == '2':
            result = result + " and th.trace_type = 2"
        elif mgr == '3':
            result = result + " and th.trace_type = 3"
        if st != '':
            result = result + " and th.reg_dttm >= '%s'" % st
        if st == '':
            if et == '':
                result = result + " and th.reg_dttm >= (now() - interval '7 day')"
        if et != '':
            result = result + " and th.reg_dttm <= '%s'" % et
        result = result + """ and th.orgseq = %s group by th.trace_type, th.step_msg, th.start_dttm, th.end_dttm, th.histseq, th.traceseq, th.trace_type, use.userid, use.username, cust.customername, th.response_status, th.caller, th.callee, th.reg_dttm, mss.companyname, uu.username
                    order by th.reg_dttm desc
                    limit %s offset %s""" % (orgseq, limit, start)
        return result

    def select_trace_cnt_history(self, orgseq, re, mgr, st, et):
        result = """select
                    count(*)
                    from tb_trace_history as th
                    left join tb_nsprovisioning as ns
                    on th.trace_type = 2 and th.traceseq = ns.nsprovisioningseq
                    left join tb_user as use
                    on ns.userid = use.userid and use.orgseq = th.orgseq
                    left join tb_customer as cust
                    on cust.userid = use.userid
                    where th.orgseq = %s""" % (orgseq)
        if re == '1':
            result = result + " and upper(th.response_status) = 'S'"
        elif re == '2':
            result = result + " and upper(th.response_status) = 'F'"
        if mgr == '1':
            result = result + " and th.trace_type = 1"
        elif mgr == '2':
            result = result + " and th.trace_type = 2"
        elif mgr == '3':
            result = result + " and th.trace_type = 3"
        if st != '':
            result = result + " and th.reg_dttm >= '%s'" % st
        if st == '':
            if et == '':
                result = result + " and th.reg_dttm >= (now() - interval '7 day')"
        if et != '':
            result = result + " and th.reg_dttm <= '%s'" % et
        return result

    def select_rule_file(self, seq, reg):
        result = """select rule_file_name
                    from tb_syncmgr_trace
                    where syncseq = %s
                    and reg_dttm = '%s'""" % (seq, reg)
        return result

    def active_standby(self, seq):
        result = """select source_ip_active, dest_ip_active
                    from tb_sdn_ip_mapping
                    where officecode =
                    (select officecode
                    from tb_org
                    where orgseq = '%s')""" % seq
        return result

    def get_process_state(self, seq):
#         result = """select orgseq,
#                     case
#                     when process_name = 'utmstatusmonitoring' then 'NFV-UTM상태감시'
#                     when process_name = 'utmswitchovermonitoring' then 'UTM절체감시'
#                     end, upper(process_status), process_name
#                     from tb_monitor_openstack_process_monitoring_state_info
#                     where create_time = (select max(create_time) from tb_monitor_openstack_process_monitoring_state_info where orgseq = %s)
#                     and orgseq = %s""" % (seq, seq)
        result = """select
                    pro.orgseq,
                    col.remark, upper(process_status), pro.process_name
                    from tb_monitor_mgmt_vm_state_info as pro
                    inner join tb_collector_monitor_process_list as col
                    on pro.process_name = col.process_name
                    where pro.create_time = (select max(create_time) from tb_monitor_mgmt_vm_state_info where orgseq = %s)
                    and pro.orgseq = %s""" % (seq, seq)
        return result

    def getOrgName(self, seq):
        result = """select orgname from tb_org where orgseq = %s""" % seq
        return result

    def setCommandLog(self, userid, url, command, param):
        result = """insert into tb_command_log(userid, reg_dttm, url, command, param) values('%s', now(), '%s', '%s', '%s')""" % (userid, url, command, param)
        return result

    def getSwitchType(self, orgseq):
        result = """select sw_type
                    from tb_switch_info
                    where orgseq = %s
                    group by sw_type
                    limit 1""" % orgseq
        return result

    def select_sync_trace_paging_history(self, org, start, limit):
        result = """select
                    e.syncseq, to_char(e.reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'), e.hostname, e.ax_gate_ip,
                    mss.companyname as companyname,
                    e.case_type, e.module_type, e.process_type, e.message, to_char(e.reg_dttm, 'mm-dd HH24:MI:SS'), use.username
                    from tb_syncmgr_event as e
                    left join tb_mg_utm as mg
                    on mg.utm_userid = e.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    left join tb_user as use
                    on use.userid = e.utm_userid
                    where e.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = %s) and e.orgseq = %s
                    order by e.reg_dttm
                    limit %s offset %s""" % (str(org), str(org), str(limit), str(start))
        return result

    def select_sync_trace_cnt_history(self, orgseq):
        result = """select
                    count(*)
                    from tb_syncmgr_event as e
                    left join tb_mg_utm as mg
                    on mg.utm_userid = e.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    where e.syncseq = (select max(traceseq) from tb_trace_history where trace_type = 1 and start_dttm is not null and end_dttm is not null and orgseq = %s) and e.orgseq = %s""" % (str(orgseq), str(orgseq))
        return result

    def getRuleDetail(self, seq, reg):
        result = """select
                    e.hostname, e.ax_gate_ip,
                    case
                    when mss.companyname is null then e.utm_userid
                    when mss.companyname = '' then e.utm_userid
                    else mss.companyname
                    end as companyname,
                    e.message, e.message_detail, e.case_type, e.module_type, e.process_type
                    from tb_syncmgr_event as e
                    left join tb_mg_utm as mg
                    on mg.utm_userid = e.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    where e.syncseq = %s and e.reg_dttm = '%s'
                    order by e.reg_dttm desc""" % (seq, reg)
        return result

    def getSuper(self, userid, pwds):
        result = """select userid
                    from tb_user
                    where userid = '%s'
                    and userauth = 99
                    and password = (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex'))""" % (userid, pwds)
        return result

    def getUserAuth(self, userid, pwds):
        result = """select userid
                    from tb_user
                    where userid = '%s'
                    and (userauth = 0 or userauth = 1 or userauth = 99)
                    and password = (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex'))""" % (userid, pwds)
        return result

    def getHaInfo(self, org):
        result = """select utm.utm_seq as applianceid, utm.utm_type as appliancetype,
                    utm.utm_name as appliancename, utm.utm_mgmt_ip as ipaddr,
                    con.sw_port as portnum, sw.sw_Seq as switchdpid,
                    org.officecode
                    from tb_utm_info utm
                    inner join tb_utm_conn_info con on con.utm_seq = utm.utm_seq
                    inner join tb_switch_info sw on sw.orgseq = sw.orgseq
                    inner join tb_org org on org.orgseq = sw.orgseq
                    where sw.orgseq = %s and utm.utm_type = 'HW'""" % org
        return result

    def getFail(self, orgs, types):
        result = """select
                    utm.utm_mgmt_ip as utm_mgmt_ip, utm.utm_type as utm_type, utm.utm_name
                    from tb_utm_conn_info as uci
                    inner join tb_switch_info as sw
                    on uci.sw_seq = sw.sw_seq
                    inner join tb_utm_info as utm
                    on utm.utm_seq = uci.utm_seq
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    where 1=1 and org.orgseq = %s and utm.utm_type = 'HW'""" % (orgs)
        return result

    def getOffice(self, orgs):
        result = "select officecode from tb_org where orgseq = %s" % orgs
        return result

    def select_rule_error(self, org, ts, start, limit):
        result = """select
                    e.syncseq, to_char(e.reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US'), e.hostname, e.ax_gate_ip,
                    mss.companyname as companyname,
                    e.case_type, e.module_type, e.process_type, e.message, to_char(e.reg_dttm, 'mm-dd HH24:MI:SS'), use.username
                    from tb_syncmgr_event as e
                    left join tb_mg_utm as mg
                    on mg.utm_userid = e.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    left join tb_user as use
                    on use.userid = e.utm_userid
                    where e.syncseq = %s and e.orgseq = %s
                    order by e.reg_dttm
                    limit %s offset %s""" % (str(ts), str(org), str(limit), str(start))
        return result

    def select_rule_error_cnt(self, orgseq, ts):
        result = """select
                    count(*)
                    from tb_syncmgr_event as e
                    left join tb_mg_utm as mg
                    on mg.utm_userid = e.utm_userid
                    left join tb_mss_order as mss
                    on mg.utm_mappid = mss.utm_mappid
                    where e.syncseq = %s and e.orgseq = %s""" % (str(ts), str(orgseq))
        return result

if __name__ == '__main__':
    pass