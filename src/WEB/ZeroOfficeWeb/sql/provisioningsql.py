#-*- coding: utf-8

'''
Created on 2015. 1. 12.

@author: 진수
'''

class ProvisionSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getTemplate(self):
        result = "select nstemplateseq, templatedesc from tb_nstemplate where del_dttm is null order by nstemplateseq asc;"
        return result

    def getProvisionUserid(self, pseq):
        result = "select userid from tb_nsprovisioning where nsprovisioningseq = %s" % pseq
        return result

    def getOrgName(self, userid):
        result = """SELECT org.orgseq, org.orgname from tb_org as org
                    inner join tb_user as us
                    on us.orgseq = org.orgseq WHERE us.userid = '%s'""" % userid
        return result

    def getTemplateList(self):
        result = "select * from tb_neclscode where maincode = '90' and dtlcode='S' order by neclsseq;"
        return result

    def getFixListName(self):
        result = """SELECT necatseq, neclsseq, service_name, config_props FROM tb_necat WHERE del_dttm is null
                    AND neclsseq = 13 ORDER BY necatseq;"""
        return result

    def getOrgs(self, userid):
        result = """select org.orgseq, org.orgname, us.orgseq as orgs
                    from tb_org as org
                    left join tb_user as us
                    on us.orgseq = org.orgseq or us.userauth = 0 or us.userauth = 99
                    where org.pop_yn = 'Y' and us.userid = '%s'
                    order by org.orgseq""" % userid
        return result

    def getNsTemplate(self):
        result = "select nstemplateseq, templatedesc from tb_nstemplate where del_dttm is null order by nstemplateseq asc;"
        return result

    def getPoolList(self):
        result = """SELECT n.templateseq, n.neseq, n.neclsseq, c.subname, c.ico_small FROM tb_ne n
                    INNER JOIN tb_neclscode c ON n.neclsseq=c.neclsseq
                    WHERE n.del_dttm is null and n.usgstatuscode=9 and n.neusgcode='9001' ORDER BY n.neclsseq ASC;"""
        return result

    def getPool(self, provisionseq, templateseq):
        result = """SELECT a.icon_large, n.procstatecode, n.console_url, n.templateseq, t.templatedesc,
                    n.provisionseq, n.neseq, n.neclsseq, n.config_props, c.subname, c.maincode, c.subcode,
                    c.dtlcode, c.description, c.mainname, c.dtlname, c.ico_small
                    FROM tb_ne n
                    INNER JOIN tb_neclscode c ON n.neclsseq = c.neclsseq
                    INNER JOIN tb_nstemplate t ON n.templateseq = t.nstemplateseq
                    LEFT OUTER JOIN tb_necat a ON n.necatseq = a.necatseq
                    WHERE n.del_dttm is null and (n.backuptype is null or n.backuptype = 'active')"""
        if provisionseq == 0 :
            result = result + " AND n.usgstatuscode=9 and n.neusgcode='9001' and n.templateseq = '%s'" % templateseq
        else :
            result = result + " AND n.provisionseq = '%s' AND n.templateseq = '%s'" % (provisionseq , templateseq)
        return result

    def getAppSlide(self, neclsseq):
        result = """SELECT necatseq, neclsseq, icon_small, icon_large, service_name, spec_thruput, price_monthly
                    FROM tb_necat WHERE del_dttm is null and neclsseq = '%s' ORDER BY necatseq;""" % neclsseq
        return result

    def getNecat(self, necatseq):
        result = """SELECT service_name, icon_large, neclsseq, necatseq, config_props
                    FROM tb_necat
                    WHERE del_dttm is null
                    and necatseq = '%s'""" % necatseq
        return result

    def getProvisionReady(self,data):
        result = ""
        return result

    def getTrace(self, pseq):
        result = """SELECT pt.svc_type, TO_CHAR(pt.reg_dttm, 'yyyy-mm-dd HH24:MI:SS.MS') as reg_dttm, pt.step_msg,
                    CASE WHEN pt.indent is null THEN 0 ELSE pt.indent END,
                    CASE WHEN pt.msg_level is null THEN 0 ELSE pt.msg_level END,
                    ps.step
                    from tb_provision_trace as pt
                    inner join tb_provision_step as ps on pt.provisionseq = ps.provisionseq
                    WHERE pt.provisionseq = '%s'""" % pseq
        return result

    def getSvgTrace(self, pseq):
        result = """SELECT pt.message, use.userid, use.username, org.orgname, cust.customername, cust.email, cust.telnum
                    from tb_provisionmgr_trace as pt
                    inner join tb_nsprovisioning as ns
                    on pt.provisionseq = ns.nsprovisioningseq
                    inner join tb_user as use
                    on use.userid = ns.userid
                    inner join tb_org as org
                    on org.orgseq = use.orgseq
                    left join tb_customer as cust
                    on cust.userid = use.userid
                    WHERE pt.provisionseq = '%s'""" % pseq
        return result

    def getMsg(self, pseq):
        result = """SELECT svc_type, TO_CHAR(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.MS') as reg_dttm, step_msg,
                    CASE WHEN indent is null THEN 0 ELSE indent END,
                    CASE WHEN msg_level is null THEN 0 ELSE msg_level END
                    from tb_provision_trace WHERE provisionseq = '%s'""" % pseq
        return result

    def getUser(self, userid):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = '%s'" % userid
        return result

    def getProvisionStep(self, pseq):
        result = "select provisionseq, step_message, step from tb_provision_step where 1=1 and provisionseq = '%s'"% pseq
        return result

    def getTraceGrid(self, pseq):
        result = """select pt.provisionseq as provisionseq,
                    case when pt.manage_type = 0 then 'VIM'
                        when pt.manage_type = 1 then 'VNF'
                        when pt.manage_type = 2 then 'TEST'
                        end as type,
                    case when pt.cmd_type = 'C' then '성공'
                        when pt.cmd_type = 'D' then '삭제'
                        end as cmd,
                    pt.step_msg as group,
                    TO_CHAR(max_time, 'yyyy-mm-dd HH24:MI:SS') as max_time,
                    TO_CHAR(min_time, 'yyyy-mm-dd HH24:MI:SS') as min_time, TO_CHAR((max_time - min_time),
                    'MI:SS') as time, pt.manage_group as manage
                    from tb_provision_trace as pt
                    inner join
                    (
                    select provisionseq, manage_group, manage_type, max(reg_dttm) as max_time, min(reg_dttm) as min_time, min(step) as min_step
                    from tb_provision_trace
                    group by provisionseq, manage_group, manage_type
                    ) as ti
                    on pt.provisionseq = ti.provisionseq and pt.manage_group = ti.manage_group and pt.step = ti.min_step and pt.manage_type = ti.manage_type
                    where pt.provisionseq = '%s' order by pt.reg_dttm""" % pseq
        return result

    def getTraceGridDetail(self, pseq):
        result = """SELECT step_msg, step_msg_details, step_msg_cmd,
                    manage_group,
                    case when manage_type = 0 then 'VIM'
                    when manage_type = 1 then 'VNF'
                    when manage_type = 2 then 'TEST'
                    end as type
                    from tb_provision_trace
                    WHERE provisionseq = '%s' ORDER BY reg_dttm""" % pseq
        return result
    def getVlan(self, pseq):
        result = """select config_props, use.userid, use.username,
                    cust.customername, cust.telnum, cust.email, org.orgname
                    from tb_ne as ne
                    inner join tb_provision_trace as pt
                    on ne.provisionseq = pt.provisionseq
                    inner join tb_user as use
                    on pt.userid = use.userid
                    inner join tb_customer as cust
                    on use.userid = cust.userid
                    inner join tb_org as org
                    on use.orgseq = org.orgseq
                    where pt.provisionseq = %s
                    limit 1""" % pseq
        return result

    def getCustomer(self, userid):
        result = """select use.userid, use.username, cust.customername, cust.telnum, cust.email, org.orgname
                    from tb_user as use
                    left join tb_customer as cust
                    on cust.userid = use.userid
                    inner join tb_org as org
                    on org.orgseq = use.orgseq
                    where use.userid = '%s'""" % userid
        return result

    def getVlanList(self, orgseq):
        result = """select vlan
                    from tb_vlan where allocate_yn = 'N' and orgseq = %s order by vlan""" % orgseq
        return result

    def getUserList(self, orgseq):
        result = """select u.userid, u.username
                    from tb_user as u
                    left join tb_nsprovisioning as ns
                    on u.userid = ns.userid and ns.procstatecode = '1'
                    where u.userauth = 2 and u.orgseq = '%s' and ns.nsprovisioningseq is null""" % orgseq
        return result

    def getTemplateDel(self, necatseq):
        result = """UPDATE tb_necat
                    SET del_dttm = CURRENT_TIMESTAMP
                    WHERE necatseq = %s""" % necatseq
        return result

    def getProvUser(self, userid, orgseq):
        result = """select userid, username,
                    convert_from(decrypt(decode((select password from tb_user where userid = '%s' and orgseq = %s), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password
                    from tb_user
                    where userid = '%s' and orgseq = %s
                    """ % (userid, orgseq, userid, orgseq)
        return result

    def setCommandLog(self, userid, url, command, param):
        result = """insert into tb_command_log(userid, reg_dttm, url, command, param) values('%s', now(), '%s', '%s', '%s')""" % (userid, url, command, param)
        return result

    def getUtms(self, orgseq):
        result = """select utm.utm_seq, utm.utm_name
                    from tb_utm_conn_info as conn
                    inner join tb_switch_info as sw
                    on sw.sw_seq = conn.sw_seq
                    inner join tb_utm_info as utm
                    on conn.utm_seq = utm.utm_seq and utm_type = 'NFV'
                    where sw.orgseq = %s
                    group by utm.utm_seq, utm.utm_name""" % orgseq
        return result

    def getSwitch(self, utmseq):
        result = """select sw.sw_seq, sw.sw_name
                    from tb_utm_conn_info as conn
                    inner join tb_switch_info as sw
                    on sw.sw_seq = conn.sw_seq
                    inner join tb_utm_info as utm
                    on conn.utm_seq = utm.utm_seq and utm_type = 'NFV'
                    where utm.utm_seq = %s
                    group by sw.sw_seq, sw.sw_name""" % utmseq
        return result

    def getSwitchPortList(self, utmseq):
        result = """select sw_port from tb_utm_conn_info as conn where utm_seq = %s group by sw_port""" % utmseq
        return result

    def getSwitchPort(self, swseq):
        result = """select sw_port, sw_start, sw_end from tb_switch_info where sw_seq =  %s""" % swseq
        return result