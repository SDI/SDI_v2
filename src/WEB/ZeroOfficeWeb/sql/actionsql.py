#-*- coding: utf-8 -*-
'''
Created on 2015. 5. 14.

@author: 세민
'''

class ActionSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getActionRow(self, orgseq):
        result = """SELECT count(*)
                    from tb_command_log as comm
                    left join tb_user as u
                    on comm.userid = u.userid
                    where 1=1"""
        if not orgseq == 0:
            result = result + " and u.orgseq = %s" % orgseq
        return result

    def getAction(self, orgseq, start):
        result = """select comm.command_seq as command_seq, comm.userid as userid, to_char(comm.reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US') as reg_dttm, comm.url as url, comm.command as command
                    from tb_command_log as comm
                    left join tb_user as u
                    on comm.userid = u.userid
                    where 1=1"""
        if not orgseq == 0:
            result = result + " and u.orgseq = %s" % orgseq
        result = result + " order by comm.reg_dttm desc LIMIT 15 OFFSET '%s';" % start
        return result

    def getActionSearchRow(self, userNm, startdt, enddt, orgseq):
        result = """select count(*)
                    from tb_command_log as comm
                    left join tb_user as u
                    on comm.userid = u.userid
                    where 1=1"""
        if not userNm == "" :
            result = result + " and comm.userid like '%s'" % userNm
        if not startdt == "" :
            result = result + " and comm.reg_dttm >= '%s 00:00:00'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and comm.reg_dttm <= '%s 23:59:59'::timestamp" % enddt
        if not orgseq == 0:
            result = result + " and u.orgseq = %s" % orgseq

        return result

    def getActionSearch(self, userNm, startdt, enddt, start, orgseq):
        result = """select comm.command_seq as command_seq, comm.userid as userid, to_char(comm.reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US') as reg_dttm, comm.url as url, comm.command as command
                    from tb_command_log as comm
                    left join tb_user as u
                    on comm.userid = u.userid
                    where 1=1"""
        if not userNm == "" :
            result = result + " and comm.userid like '%s'" % userNm
        if not startdt == "" :
            result = result + " and comm.reg_dttm >= '%s 00:00:00'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and comm.reg_dttm <= '%s 23:59:59'::timestamp" % enddt
        if not orgseq == 0:
            result = result + " and u.orgseq = %s" % orgseq
        result = result + " order by comm.reg_dttm desc LIMIT 15 OFFSET '%s'" % start
        return result

    def getActionDetail(self, seq):
        result = """select command_seq, userid, to_char(reg_dttm, 'yyyy-mm-dd HH24:MI:SS.US') as reg_dttm, url, command, param
                    from tb_command_log where command_seq = '%s'""" % seq
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result