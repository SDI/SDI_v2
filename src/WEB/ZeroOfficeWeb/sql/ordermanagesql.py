#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class OrderSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getTemplateRow(self, orgseq):
        result = """SELECT count(*)
                    from tb_nsprovisioning a inner join tb_org b on a.orgseq = b.orgseq
                    left outer join tb_user c on a.userid = c.userid
                    left outer join tb_customer d on c.userid = d.userid
                    left join tb_addr e on b.addrseq = e.addrseq
                    inner join tb_nstemplate f on a.templateseq = f.nstemplateseq
                    where a.procstatecode = '1' and c.userauth = 2"""
        if not orgseq == 0:
            result = result + " and a.orgseq = %s" % orgseq
        return result

    def getTemplate(self, orgseq, start):
        result = """select a.templateseq, a.nsprovisioningseq pseq , b.orgname , b.orgseq, case when d.customername != '' then d.customername else a.userid end cname ,
                    concat(e.addr1, ' ', e.addr2, ' ', e.addr3, ' ', d.detailaddr) address,
                    f.templatedesc as serviceNm, a.procstatecode as pstate, a.startdate::text as startdt, a.enddate::text as enddt,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '인터넷') as t1,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '국사') as t2,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '고객사무실') as t3,
                    case when n.backuptype is null then '0' when n.backuptype = '' then '0' when n.backuptype = 'active' then '1' end as backuptype
                    from tb_nsprovisioning a inner join tb_org b on a.orgseq = b.orgseq
                    left outer join tb_user c on a.userid = c.userid
                    left outer join tb_customer d on c.userid = d.userid
                    left join tb_addr e on b.addrseq = e.addrseq
                    inner join tb_nstemplate f on a.templateseq = f.nstemplateseq
                    inner join tb_ne as n on a.nsprovisioningseq = n.provisionseq and (n.backuptype = 'active' or n.backuptype is null or n.backuptype = '')
                    where a.procstatecode = '1' and c.userauth = 2"""
        if not orgseq == 0:
            result = result + " and a.orgseq = %s" % orgseq
        result = result + " ORDER BY a.nsprovisioningseq DESC LIMIT 15 OFFSET '%s';" % start
        return result

    def getPoolList(self):
        result = """SELECT n.templateseq, n.neseq, n.neclsseq, c.subname, c.ico_small FROM tb_ne n
                    INNER JOIN tb_neclscode c ON n.neclsseq=c.neclsseq
                    WHERE n.del_dttm is null and n.usgstatuscode=9 and n.neusgcode='9001' ORDER BY n.neclsseq ASC;"""
        return result

    def getTemplateSearchRow(self, userNm, serviceNm, startdt, enddt, orgseq):
        result = """select count(*)
                    from tb_nsprovisioning a inner join tb_org b on a.orgseq = b.orgseq
                    left outer join tb_user c on a.userid = c.userid
                    left outer join tb_customer d on c.userid = d.userid
                    left join tb_addr e on b.addrseq = e.addrseq
                    inner join tb_nstemplate f on a.templateseq = f.nstemplateseq
                    where 1=1 and a.procstatecode = '1' and c.userauth = 2"""
        if not userNm == "" :
            result = result + " and d.customername like '%s'" % userNm
        if not serviceNm == "" :
            result = result + " and f.templatedesc like '%s'" % serviceNm
        if not startdt == "" :
            result = result + " and a.startdate >= '%s'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and a.enddate <= '%s'::timestamp" % enddt
        if not orgseq == 0:
            result = result + " and a.orgseq = %s" % orgseq

        return result

    def getTemplateSearch(self, userNm, serviceNm, startdt, enddt, start, orgseq):
        result = """select a.templateseq, a.nsprovisioningseq pseq , b.orgname , b.orgseq, case when d.customername != '' then d.customername else a.userid end cname ,
                    concat(e.addr1, ' ', e.addr2, ' ', e.addr3, ' ', d.detailaddr) address,
                    f.templatedesc as serviceNm, a.procstatecode as pstate, a.startdate::text as startdt, a.enddate::text as enddt,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '인터넷') as t1,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '국사') as t2,
                    (SELECT array_to_string(array_agg(vlan),',') from tb_vlan where allocate_yn = 'Y' and userid = a.userid and type = '고객사무실') as t3
                    from tb_nsprovisioning a inner join tb_org b on a.orgseq = b.orgseq
                    left outer join tb_user c on a.userid = c.userid
                    left outer join tb_customer d on c.userid = d.userid
                    left join tb_addr e on b.addrseq = e.addrseq
                    inner join tb_nstemplate f on a.templateseq = f.nstemplateseq
                    where a.procstatecode = '1' and c.userauth = 2"""
        if not userNm == "" :
            result = result + " and d.customername like '%s'" % userNm
        if not serviceNm == "" :
            result = result + " and f.templatedesc like '%s'" % serviceNm
        if not startdt == "" :
            result = result + " and a.startdate >= '%s'::timestamp" % startdt
        if not enddt == "" :
            result = result + " and a.enddate <= '%s'::timestamp" % enddt
        if not orgseq == 0:
            result = result + " and a.orgseq = %s" % orgseq
        result = result + " order by a.nsprovisioningseq desc LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result