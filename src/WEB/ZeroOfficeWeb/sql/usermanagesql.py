#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class UserSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getAllUser(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user"
        return result

    def getIsUser(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = %s"
        return result

    def getUserLogin(self):
        result = "SELECT u.userid, u.username, u.userauth, u.orgseq, org.orgname from tb_user as u left join tb_org as org on u.orgseq = org.orgseq WHERE u.userid = %s and password = (SELECT encode(encrypt(%s, 'gigaoffice!234', '3des'), 'hex'))"
        return result

    def getUserIndexLogin(self):
        result = "SELECT userid, username, userauth, orgseq  from tb_user WHERE userid = %s"
        return result

    def getLoginModify(self, username):
        result = """select a.userid,
                    convert_from(decrypt(decode((select password from tb_user where userid = '%s'), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.username, a.userauth as userauth, a.orgseq, b.orgname
                    from tb_user a join tb_org b on a.orgseq = b.orgseq where 1=1
                    and userid like '%s'""" % (str(username), str(username))
        return result

    def getUserRow(self, user, userid, username, orgseq):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.userid) AS R
                    FROM tb_user a join tb_org b on a.orgseq = b.orgseq
                    inner join tb_user as auth
                    on b.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1 and a.userauth != 2 and a.userauth != 3"""
        if not userid == "" :
            result = result + " and a.userid like '%s'" % userid
        if not username == "" :
            result = result + " and a.username like '%s'" % username
        if not orgseq == "0" :
            result = result + " and b.orgseq = '%s'" % orgseq
        if not user == "" :
            result = result + " and auth.userid = '%s'" % user
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getUser(self, user, userid, username, orgseq, start):
        result = """select ROW_NUMBER() OVER(ORDER BY a.userid DESC) as rownum, a.userid, a.password, a.username,
                    case a.userauth
                    when '0' then '관리자'
                    when '1' then '운영자'
                    when '2' then '고객'
                    END as userauth,
                    a.orgseq, b.orgname
                    from tb_user a join tb_org b on a.orgseq = b.orgseq
                    inner join tb_user as auth
                    on b.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1 and a.userauth != 2 and a.userauth != 3"""
        if not userid == "" :
            result = result + " and a.userid like '%s'" % str(userid)
        if not username == "" :
            result = result + " and a.username like '%s'" % str(username)
        if not orgseq == "0" :
            result = result + " and b.orgseq = '%s'" % str(orgseq)
        if not user == "" :
            result = result + " and auth.userid = '%s'" % user
        result = result + " ORDER BY a.reg_dttm desc, a.userid DESC LIMIT 15 OFFSET '%s';" % start
        return result

    def setUserInsert(self, userid, password, username, userauth, orgseq):
        result = """insert into tb_user(userid, password, username, userauth, orgseq, reg_dttm)
                    values('%s', (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), '%s', '%s', '%s', now())""" % (userid, password, username, userauth, orgseq)
        return result

    def getUserDetail(self, userid, orgseq):
        result = """select a.userid,
                    convert_from(decrypt(decode((select password from tb_user where userid = '%s' and orgseq = %s), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.username,
                    case a.userauth
                    when '0' then '관리자'
                    when '1' then '운영자'
                    when '2' then '고객'
                    END as userauth,
                    a.orgseq, b.orgname
                    from tb_user a join tb_org b on a.orgseq = b.orgseq where 1=1 and a.userauth != 2 and a.userauth != 3
                    and userid = '%s' and a.orgseq = %s""" % (str(userid), orgseq, str(userid), orgseq)
        return result

    def setUserUpdate(self, userid, password, username, userauth, orgseq, originOrgSeq):
        result = """update tb_user set password = (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')),  username = '%s', userauth = '%s', orgseq = '%s'
                    where userid = '%s' and orgseq = %s"""% (password, username, userauth, orgseq, userid, originOrgSeq)
        return result

    def setUserDelete(self, userid, orgseq):
        result = "delete from tb_user where userid = '%s' and orgseq = %s" % (userid, orgseq)
        return result

    def setCustomer(self, customername, empcnt, telnum, hpnum, email):
        result = """insert into tb_customer(customername, emp_cnt, telnum, hp_num, email)
                    values('%s', '%s', '%s', '%s', '%s')""" % (customername, empcnt, telnum, hpnum, email)
        return result

    def setUserCustomer(self, userid, password, username, userauth, customerseq):
        result = """insert into tb_user(userid, password, username, userauth, customerseq)
                    values('%s', (SELECT encode(encrypt('%s', 'gigaoffice!234', '3des'), 'hex')), '%s', '%s', '%s')""" % (userid, password, username, userauth, customerseq)
        return result

    def getCustomerSeq(self, customername, empcnt, telnum, hpnum, email):
        result = """SELECT customerseq FROM tb_customer WHERE customername = '%s'
                    AND  emp_cnt = '%s' AND telnum = '%s' AND hp_num = '%s'
                    AND email = '%s'""" % (customername, empcnt, telnum, hpnum, email)
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result

    def getUserCheck(self, userId):
        result = "SELECT * FROM tb_user WHERE userid = '%s'" % userId
        return result

    def setCommandLog(self, userid, url, command, param):
        result = """insert into tb_command_log(userid, reg_dttm, url, command, param) values('%s', now(), '%s', '%s', '%s')""" % (userid, url, command, param)
        return result