#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 22.

@author: SM
'''

class UtmConnSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getUtmConnRow(self, userid, orgseq):
        result = """SELECT count(*) AS R
                    from tb_utm_conn_info as uci
                    inner join tb_switch_info as sw
                    on uci.sw_seq = sw.sw_seq
                    inner join tb_utm_info as utm
                    on utm.utm_seq = uci.utm_seq
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    WHERE 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if not orgseq == 0 :
            result = result + " and sw.orgseq = '%s'" % orgseq
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getUtmConn(self, userid, orgseq, start):
        result = """select ROW_NUMBER() OVER(ORDER BY uci.utm_seq DESC) as rownum, uci.utm_seq as utm_seq, uci.utm_link_type as utm_link_type,
                    utm.utm_mgmt_ip as utm_mgmt_ip, utm.utm_type as utm_type,
                    sw.sw_name as sw_name, utm.utm_name as utm_name, uci.status as status, uci.sw_port as sw_port, uci.sw_seq as sw_seq, org.orgname as orgname, sw.sw_port as portlimit
                    from tb_utm_conn_info as uci
                    inner join tb_switch_info as sw
                    on uci.sw_seq = sw.sw_seq
                    inner join tb_utm_info as utm
                    on utm.utm_seq = uci.utm_seq
                    inner join tb_org as org
                    on sw.orgseq = org.orgseq
                    inner join tb_user as auth
                    on sw.orgseq = auth.orgseq or auth.userauth = 0 or auth.userauth = 99
                    where 1=1"""
        if not userid == "" :
            result = result + " and auth.userid = '%s'" % userid
        if orgseq != 0 :
            result = result + " and org.orgseq = '%s'" % orgseq
        result = result + " ORDER BY sw.reg_dttm DESC LIMIT 15 OFFSET '%s'" % start
        return result

    def getOrgs(self):
        result = "select orgname, orgseq from tb_org where pop_yn = 'Y' order by orgseq"
        return result

    def getUtms(self):
        result = "select utm_seq, utm_name, utm_mgmt_ip, utm_type from tb_utm_info order by utm_seq desc"
        return result

    def getSwitchs(self):
        result = "select sw_seq, sw_name, orgseq, sw_mgmt_ip, sw_port from tb_switch_info order by sw_seq desc"
        return result

    def setUtmConnInsert(self, utmSeq, utmLinkType, swSeq, sw_port):
        result = """insert into tb_utm_conn_info(utm_seq, utm_link_type, sw_seq, sw_port, reg_dttm)
                    values('%s', '%s', '%s', '%s', CURRENT_TIMESTAMP)""" % (utmSeq, utmLinkType, swSeq, sw_port)
        return result

    def getUtmConnDetail(self, sw_seq):
        result = """select sw_name, orgseq, sw_mgmt_ip
                    from tb_switch_info where sw_seq = '%s'""" % sw_seq
        return result

    def setUtmConnModify(self, swSeq, sw_port, utmSeq, utmLinkType):
        result = """update tb_utm_conn_info set sw_port = '%s'
                    where utm_seq = '%s' and utm_link_type = '%s' and sw_seq = '%s'"""% (sw_port, utmSeq, utmLinkType, swSeq)
        return result

    def setUtmConnDelete(self, utmSeq, utmLinkType, swSeq):
        result = "delete from tb_utm_conn_info where utm_seq = '%s' and utm_link_type = '%s' and sw_seq = '%s'" % (utmSeq, utmLinkType, swSeq)
        return result

    def getPort(self, swSeq):
        result = "select sw_port from tb_switch_info where sw_seq = '%s'" % (swSeq)
        return result