#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: 만구
'''

class OrgSql(object):
    '''
    classdocs
    '''
    def __init__(self):
        '''
        Constructor
        '''

    def getOrgRow(self):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.orgname) AS R
                    FROM tb_org a left join tb_addr b on a.orgseq = b.orgseq
                    WHERE 1=1 ORDER BY R DESC LIMIT 1 OFFSET 0;"""
        return result

    def getOrg(self,start):
        result = """select ROW_NUMBER() OVER(ORDER BY a.orgseq DESC) as rownum, a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a left join tb_addr b on a.orgseq = b.orgseq where 1=1
                    ORDER BY a.orgseq DESC LIMIT 15 OFFSET '%s'""" % start
        return result

    def getOrgs(self, addr3):
        result = """select a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3 FROM tb_org a
                    left join tb_addr b on a.orgseq = b.orgseq where 1=1 and b.addr3 like '%s'
                    ORDER BY a.orgseq DESC;""" % ("%"+addr3+"%")
        return result

    def getOrgSearchRow(self, searchAddr1, searchAddr2, searchAddr3):
        result = """SELECT ROW_NUMBER() OVER(ORDER BY a.orgname) AS R
                    FROM tb_org a left join tb_addr b on a.orgseq = b.orgseq
                    WHERE 1=1"""
        if not searchAddr1 == "" :
            result = result + " and b.addr1 like '%s'" % searchAddr1
        if not searchAddr2 == "" :
            result = result + " and b.addr2 like '%s'" % searchAddr2
        if not searchAddr3 == "" :
            result = result + " and b.addr3 like '%s'" % searchAddr3
        result = result + " ORDER BY R DESC LIMIT 1 OFFSET 0;"
        return result

    def getOrgSearch(self, searchAddr1, searchAddr2, searchAddr3, start):
        result = """select ROW_NUMBER() OVER(ORDER BY a.orgseq DESC) as rownum, a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a left join tb_addr b on a.orgseq = b.orgseq where 1=1"""
        if not searchAddr1 == "" :
            result = result + " and b.addr1 like '%s'" % searchAddr1
        if not searchAddr2 == "" :
            result = result + " and b.addr2 like '%s'" % searchAddr2
        if not searchAddr3 == "" :
            result = result + " and b.addr3 like '%s'" % searchAddr3
        result = result + " ORDER BY a.orgseq DESC LIMIT 15 OFFSET '%s';" % start
        return result

    def getAddressSearch(self, dongName):
        result = """SELECT DISTINCT addr1, addr2, addr3, addrseq FROM tb_addr
                    WHERE 1=1 and addr3 like '%s'""" % dongName
        return result

    def setOrgInsert(self, orgname, addrs):
        result = """insert into tb_org(orgseq, orgname, addrseq, pop_yn) values((select max(orgseq)+1 from tb_org), '%s', '%s', 'Y')""" % (orgname, addrs)
        return result

    def getSelectOrg(self, orgname):
        result = """select a.orgseq
                    FROM tb_org a where 1=1 and orgname = '%s'""" % orgname
        return result

    def getSelectAddrRow(self, addr1, addr2, addr3):
        result = """SELECT a.addrseq FROM (
                        SELECT ROW_NUMBER() OVER(ORDER BY addr1) AS r, addrseq
                        FROM tb_addr
                        where addr1 = '%s' and addr2 = '%s' and addr3 = '%s') a
                    WHERE r = 1""" % (addr1, addr2, addr3)
        return result

    def setAddrUpdate(self, orgseq, addrseq):
        result = """update tb_addr set orgseq = '%s' where addrseq = '%s'""" % (orgseq, addrseq)
        return result

    def setOrgUpdateAddr(self, orgseq, addrseq):
        result = """update tb_org set addrseq = '%s' where orgseq = '%s'""" % (addrseq, orgseq)
        return result

    def getOrgDetail(self, orgseq):
        result = """SELECT a.orgname, a.orgseq, b.addr1, b.addr2, b.addr3
                    FROM tb_org a left join tb_addr b on a.orgseq = b.orgseq where 1=1
                    and a.orgseq = '%s'""" % orgseq
        return result

    def setOrgUpdate(self, orgseq, orgname):
        result = "update tb_org set orgname = '%s' where orgseq = '%s'" % (orgname, orgseq)
        return result

    def setOrgDelete(self, orgseq):
        result = "delete from tb_org where orgseq = '%s'" % orgseq
        return result

    def setAddrDelete(self, orgseq):
        result = "update tb_addr set orgseq = 0 where orgseq = '%s'" % orgseq
        return result
