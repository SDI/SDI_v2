#!/bin/bash

echo "======================================================"
echo "Delete UTM Start ...!!!"

#############################################################
# PARAMETERS CHECKING
#############################################################
USERID=$1
ORGSEQ=$2
if [ "$1" == "" ] ; then
    USERID="System"
fi
echo "------------------------------------------------------"
echo " 0. Initialized Checking ..."
echo "   - Prameters : userid[$1], orgseq[$2]"
echo "------------------------------------------------------"

#############################################################
# LOG FILE CHECKING
#############################################################
HOME_PATH="/usr/local/Sync"
LOG_PATH="$HOME_PATH/log"
TODAY=`/bin/date +%Y%m%d`
LOG_FILE="$LOG_PATH/DeleteUTM_$TODAY.log"

echo " 1. Log File Checking ..."
if [ -d $LOG_PATH ] ; then
    echo "   - Exists Log Path : [$LOG_PATH]"
    echo "   - 30day ago Log File Delete..."
    
    find $LOG_PATH -mtime +31 -name DeleteUTM_\* -exec rm {} \;
    
else
    echo "   - Make Log Path : [$LOG_PATH]"
    mkdir -p $LOG_PATH
    
    cp "/dev/null" $LOG_FILE
    chmod 666 $LOGFILE
fi
echo "   - Log File : [$LOG_PATH/$LOG_FILE]"
echo "------------------------------------------------------"

#############################################################
# RUN CHECKING
#############################################################
RUN_CMD="python"
RUN_FILE="$HOME_PATH/src/DeleteUTM.py"
echo " 2. Delete UTM Running ..."
echo "   - cmd : $RUN_CMD $RUN_FILE $USERID $ORGSEQ >> $LOG_FILE &"
echo "------------------------------------------------------"
echo "Delete UTM End ...!!!"
echo "======================================================"

$RUN_CMD $RUN_FILE $USERID $ORGSEQ >> $LOG_FILE &
