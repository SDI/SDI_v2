# -*- coding: utf8 -*-
import tornado.ioloop  # @UnresolvedImport
import pyrestful.rest  # @UnresolvedImport
import psycopg2

from pyrestful import mediatypes  # @UnresolvedImport
from pyrestful.rest import post
from pyrestful.rest import get
import json


class RestService(pyrestful.rest.RestHandler):
    #@post(_path="/MSS", _produces=mediatypes.APPLICATION_JSON)
    @get(_path="/api/softutm/get_customer_list", _types=[str], _produces=mediatypes.APPLICATION_JSON)
    def changeConfig(self, office_code):
        
        try:
            print "MSS Start"
            #reqdata = json.loads(self.request.body, object_hook=JsonObject)
            #print reqdata.office_code
            
            dbAccess = DBAccess()

            axGateList = dbAccess.select("""SELECT * FROM tb_mss_likage_test WHERE officecode = '%s'""" % (office_code))
            
            data = {office_code:[]}
            count = 0
            
            for rowNum in axGateList:
                data[office_code].append([])
                data[office_code][count].append({"ax_gate_ip":axGateList[rowNum]["ax_gate_ip"]})
                data[office_code][count].append({"zone_id":axGateList[rowNum]["zone_id"]})
                data[office_code][count].append({"company_name":axGateList[rowNum]["companyname"]})
                
                count = count + 1

            print json.dumps(data)
            #data = {"result":"성공","data":{rowNum:{}}}
            
            #print reqdata.    
            #self.uploadFile("211.224.204.158", self.createFileFromTemplate(reqdata.provision.application[0].service[0].item, "red_settings.template", "red_settings_gen.txt"), "/var/tomcat/redtest")
            
        except Exception as ex:
            print type(ex)
            print ex
        
        return data
    
class JsonObject:
    def __init__(self, d):
        self.__dict__ = d

class DBAccess():
    #DBConnect 정보.
    cur = None
    
    #myLock = Lock()
    
    def __init__(self):
        #DB커넥션 연결.
        self.dbConnect()

        
    def __del__(self):
        #커넥션 종료.
        #print "status -- : " + str(self.cur.status)
        self.cur.close()
        #print "status -- : " + str(self.cur.status)
        
    def close(self):
        #커넥션 종료.
        self.cur.close()
        
    def dbConnect(self):
        try:
            #DB Access 정보 생성.
            conn = psycopg2.connect(database = "sdi", 
                                    user = "orchestrator", 
                                    password = "orch!234", 
                                    host = "211.224.204.160", 
                                    port = "5432")

            conn.autocommit = True
            self.cur = conn.cursor()
        except Exception as ex:
            self.logger("\t%s" % ("dbConnect - ex : "), "E")
            self.logger("\t%s" % (str(type(ex))), "E")
            self.logger("\t%s" % (str(ex.args)), "E")
            self.logger("\t%s" % (str(ex)), "E")
            return False
        
        return True
    
    def logger(self, strLogInfo, strLogtype):
        
        if strLogtype == "D":
            print strLogInfo
        elif strLogtype == "I":
            print strLogInfo
        elif strLogtype == "E":
            print strLogInfo
        else:
            print strLogInfo
            
    def select(self, strSql):
        try:
            self.cur.execute(strSql)
            columns = [desc[0] for desc in self.cur.description]
            
            dictionary = {}
            rowCount = 0
            
            for row in self.cur.fetchall():
                checkCount = 0
                subDictionary = {}
    
                for column in columns:
                    subDictionary[column] = row[checkCount]
                    checkCount = checkCount + 1
                   
                dictionary[rowCount] = subDictionary
                rowCount = rowCount + 1
            
        except Exception as ex:
            self.logger("\t%s" % ("select - ex : "), "E")
            self.logger("\t%s" % (str(type(ex))), "E")
            self.logger("\t%s" % (str(ex.args)), "E")
            self.logger("\t%s" % (str(ex)), "E")
            dictionary = None
        
        return dictionary
    
    def insert(self, strSql):
        return self.dbExecute(strSql)
    
    def delete(self, strSql):
        return self.dbExecute(strSql)
    
    def update(self, strSql):
        return self.dbExecute(strSql)
    
    def dbExecute(self, strSql):
        self.cur.execute(strSql)
        return self.cur.rowcount
    
if __name__ == '__main__':
    try:
        print "Start"
        #logging.info("Start the UTM network configuration service %s"%logfilename)
        app = pyrestful.rest.RestService([RestService])
        app.listen(8089)
#         app.listen(8080)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:

        print "Ex"

