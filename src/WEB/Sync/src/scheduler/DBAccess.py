#-*- coding: utf-8 -*-
'''
Created on 2015-02-08
@author: 정현성
@summary: 메인 스케줄러
    testScheduled - 테스트용 스케줄
'''
import config.SyncConfig
import psycopg2 #export PATH=${PATH}:/Library/PostgreSQL/9.4/bin
import threading

from util.Logger import Logger

log = Logger()

class DBAccess(object):
    #DBConnect 저장을 위한 변순 선언
    cur = None
    
    #lock 설정을 위한 변수 선언
    lock = None
    
    def __init__(self):
        #DB커넥션 연결.
        self.dbConnect()
        
        #lock 객체 선언
        self.lock = threading.Lock()
        
    def __del__(self):
        #커넥션 종료.
        self.cur.close()
        
    def close(self):
        #커넥션 종료.
        self.cur.close()
        
    def dbConnect(self):
        try:
            #DB Access 정보 생성.
            conn = psycopg2.connect(database = config.SyncConfig.postgresql_db_info["db"], 
                                    user = config.SyncConfig.postgresql_db_info["id"], 
                                    password = config.SyncConfig.postgresql_db_info["pw"], 
                                    host = config.SyncConfig.postgresql_db_info["db_host"], 
                                    port = config.SyncConfig.postgresql_db_info["port"])

            conn.autocommit = True
            self.cur = conn.cursor()
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("dbConnect", str(type(ex)), str(ex.args), str(ex)))
            return False
        
        return True
            
    def select(self, strSql):
        try:
            #Lock획득
            self.lock.acquire()
            
            #쿼리 실행
            self.cur.execute(strSql)
            
            #쿼리 실행 결과 딕셔너리 저장.
            columns = [desc[0] for desc in self.cur.description]
            
            dictionary = {}
            rowCount = 0
            
            for row in self.cur.fetchall():
                checkCount = 0
                subDictionary = {}
    
                for column in columns:
                    subDictionary[column] = row[checkCount]
                    checkCount = checkCount + 1
                   
                dictionary[rowCount] = subDictionary
                rowCount = rowCount + 1
                
            return dictionary
            
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("select", str(type(ex)), str(ex.args), str(ex)))
            return None
        finally:
            #Lock해제 
            self.lock.release()
        
    def insert(self, strSql):
        return self.dbExecute(strSql)
    
    def delete(self, strSql):
        return self.dbExecute(strSql)
    
    def update(self, strSql):
        return self.dbExecute(strSql)
    
    def dbExecute(self, strSql):
        try:
            #Lock획득
            self.lock.acquire()
            
            #쿼리 실행
            self.cur.execute(strSql)
            
            #쿼리 실행 결과
            row_count = self.cur.rowcount
            
            return row_count
        
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s\n쿼리 : %s" % ("dbExecute", str(type(ex)), str(ex.args), str(ex), strSql))
            return None
        finally:
            #Lock해제 
            self.lock.release()
            
if __name__ == '__main__':
    pass