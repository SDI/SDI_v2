#-*- coding: utf-8 -*-
###################################################################
# author : 정현성
# Date : 2015. 4.17
# Description : 룰 변환 데이터 저장소
###################################################################

from util.Logger import Logger
import config


log = Logger()

class AXGateData(object):
    def __init__(self):
        pass
    def __end__(self):
        pass
    
    zone = {}
    network = {}
    rule_file_info = ""
    rule_comparison = {}
    delegate_zone = {}
    
    virtual_domain = {}
    temporary_bridge = {}
    
    temporary_router_virtual = {}
    temporary_vlan = {}
    """
    temporary_vlan = 
    {
      "zone21_trust": {
        "ip": [
          "xxx.xxx.xxx.xxx/x"
        ],
        "vlan": "eth3.21",
        "proxy": [
          "ok"
        ],
        "bridge": []
      },
      "zone121_trust": {
        "ip": [
          "xxx.xxx.xxx.xxx/x"
        ],
        "vlan": "eth3.121",
        "proxy": [
          "ok"
        ],
        "bridge": []
      }
    }
    """
    event_msg = {"sync_seq":"",
                "orgseq":"",   
                "reg_dttm":"",
                "host_name":"",
                "ax_gate_ip":"",
                "utm_userid":"",
                "case_type":"",        # Convention, Parameter
                "module_type":"",      # DHCP, DNAT, SNAT, FIREWALL, ROUTER
                "process_type":"",     # Stop, Continue
                "message":"",
                "message_detail":""             
    }           # 룰 변환시 이상 Case 데이터 : EventData()
    
    temporary_snat = {}
    temporary_dnat = {}
    temporary_service_group = {}
    temporary_ip_group = {}
    temporary_dhcp_pool = {}
    
    parsing_data = {"dnat":{}, 
                    "snat":{}, 
                    "dhcp":{}, 
                    "router":{}}
    
    ax_gate_info = {"ax_gate_ip":"",
                    "ax_gate_name":"", 
                    "ax_gate_id":"", 
                    "ax_gate_password":"", 
                    "office_code":"", 
                    "org_seq":"", 
                    "trace_seq":"",
                    "ax_gate_job":""}
    
    file_info = {"directory_path":"", 
                 "rule"         :{"name":"", "path":""}, 
                 "dhcp"         :{"name":"", "path":""}, 
                 "dnat"         :{"name":"", "path":""}, 
                 "snat"         :{"name":"", "path":""},
                 "firewall"     :{"name":"", "path":""},
                 "fw_incoming"  :{"name":"", "path":""},
                 "fw_outgoing"  :{"name":"", "path":""},
                 "fw_interzone" :{"name":"", "path":""},
                 "router"       :{"name":"", "path":""}
                 }
    
    rule = {"dhcp":[], 
            "dhcp_pool":[],
            "interface":[], 
            "bridge":[], 
            "router_virtual":[], 
            "ip_group":[], 
            "ip_group_set":[], 
            "service_group":[], 
            "snat_profile":[], 
            "dnat_profile":[], 
            "security_policy":{"snat":[], 
                               "dnat":[], 
                               "firewall":[]}, 
            "virtual_domain":[]}
    
    def set_event_msg_update(self, module_type, utm_userid, case_type, process_type, message, message_detail):
        self.event_msg["module_type"]       = config.SyncConfig.case_type_info["module"].get(module_type.upper())
        self.event_msg["utm_userid"]        = utm_userid
        self.event_msg["case_type"]         = config.SyncConfig.case_type_info["case"].get(case_type.upper())
        self.event_msg["process_type"]      = config.SyncConfig.case_type_info["process"].get(process_type.upper())
        self.event_msg["message"]           = message
        self.event_msg["message_detail"]    = message_detail
    
    def print_data(self):
        log.trace( "zone : " + str(self.zone) )
        log.trace( "ax_gate_info : " + str(self.ax_gate_info) )
        log.trace( "file_info : " + str(self.file_info) )
        log.trace( "rule : " + str(self.rule) )
        #log.trace( "router_virtual : " + str(self.router_virtual) )
        log.trace( "delegate_zone : " + str(self.delegate_zone) )
        log.trace( "temporary_bridge : " + str(self.temporary_bridge) )
        log.trace( "temporary_vlan : " + str(self.temporary_vlan) )
        log.trace( "temporary_snat : " + str(self.temporary_snat) )
        log.trace( "temporary_dnat : " + str(self.temporary_dnat) )
        log.trace( "temporary_service_group : " + str(self.temporary_service_group) )
        log.trace( "temporary_ip_group : " + str(self.temporary_ip_group) )
        log.trace( "temporary_dhcp_pool : " + str(self.temporary_dhcp_pool) )
        
        
        