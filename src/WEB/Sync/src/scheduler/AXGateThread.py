#-*- coding: utf-8 -*-
'''
Created on 2015. 4. 20.
@author: JHS
'''
import datetime
import json
import os
import subprocess
import threading
import time

from IPy import IP
import paramiko

import config.SyncConfig
from pgSql.SyncManagerSql import SyncManagerSql
from scheduler.AXGateData import AXGateData
from scheduler.DBAccess import DBAccess
from scheduler.HttpClient import HttpClient
from util.Logger import Logger
from util.MsgData import MsgData
from util.aescipher import AESCipher

#루트 폴더 설정.(현재 테스트를 위해 프로젝트내 임시 폴더를 사용하기 위한 설정으로 추후 배포시 변경되야함.)
#os.chdir(config.SyncConfig.directory_path["path"])
#로그 설정
log = Logger()

class AXGateThread(threading.Thread):
    connect = None
    sql = None
    main_data = AXGateData()
    
    def __init__(self, main_data):
        threading.Thread.__init__(self)
        
        self.main_data = main_data
        self.connect = DBAccess()
        self.sql = SyncManagerSql()
        
        # AXgate Info Setting (Rule 변환 Event)
        self.main_data.event_msg["sync_seq"]    = self.main_data.ax_gate_info["trace_seq"]
        self.main_data.event_msg["orgseq"]      = self.main_data.ax_gate_info["org_seq"]
        self.main_data.event_msg["ax_gate_ip"]  = self.main_data.ax_gate_info["ax_gate_ip"]
        self.main_data.event_msg["host_name"]   = self.main_data.ax_gate_info["ax_gate_name"]

    def __end__(self):
        pass
        
    def run(self):
        #에러 확인을 위한 변수 선언
        fail_msg = ""
        
        try:
            #ax_gate_job ID설정값 설정
            self.main_data.ax_gate_info["ax_gate_job"] = str(datetime.datetime.now().strftime("%Y%m%d%H%M%S%f"))
            
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("AXGate 룰 정보 파싱 시작(%s)" % (self.main_data.ax_gate_info["ax_gate_ip"]))
            e2e_msg.set_indent(0)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #AXGate에 ssh 접속하여 룰 정보를 가져와 이력 파일을 생성한다.
            fail_msg = self.ax_gate_ssh_access()
            if fail_msg != "":
                return
            
            #룰 정보 구분
            fail_msg = self.scan_rule_file()
            if fail_msg != "":
                return

            #virtual_router 정보 파싱
            fail_msg = self.virtual_router_parsing()
            if fail_msg != "":
                return
            
            #대표존ID를 생성 한다.
            fail_msg = self.scan_delegate_zone_parsing()
            if fail_msg != "":
                return
            
            #bridge 정보 파싱
            fail_msg = self.temporary_bridge_parsing()
            if fail_msg != "":
                return
            
            #vlan 정보를 파싱한다.
            fail_msg = self.vlan_parsing()
            if fail_msg != "":
                return

            #NAT 프로파일 정보 파싱
            fail_msg = self.scan_nat_profile()
            if fail_msg != "" :
                return
            
            #Service Group 정보 파싱
            fail_msg = self.scan_service_group()
            if fail_msg != "":
                return
            
            #IP Group 정보 파싱
            fail_msg = self.scan_ip_group()
            if fail_msg != "":
                return
            
            #DHCP Pool 정보 파싱
            fail_msg = self.scan_dhcp_pool()
            if fail_msg != "":
                return
            
            #DHCP 정보 생성
            fail_msg = self.dhcp_parsing()
            if fail_msg != "":
                return
            
            #DNAT 파싱
            fail_msg = self.dnat_parsing()
            if fail_msg  != "":
                return
            
            #SNAT 파싱
            fail_msg = self.snat_parsing()
            if fail_msg != "":
                return
            
            #Firewall 파싱
            fail_msg = self.firewall_parsing()
            if fail_msg != "":
                return

            #public ip 파싱 --> 5월 23일 삭제(정현호 : EDIAN Default SNAT 삭제로 해결)
#             fail_msg = self.public_ip_parsing()
#             if fail_msg != "":
#                 return
            
            #이전에 처리한 각 SW UTM 네트워크 정보를 가져온다(추후 네트워크 정보가 변경되었을 경우 SW UTM을 삭제후 다시 생성하기 위해)
            rule_his = self.connect.select(self.sql.select_parsing_date(self.main_data.ax_gate_info["ax_gate_ip"]))
             
            #현재 파싱된 SW UTM의 네트워크 정보 및 파싱 정보를 저장한다.
            self.connect.insert(self.sql.insert_parsing_date(self.main_data.ax_gate_info["ax_gate_ip"], json.dumps(self.main_data.zone)))
             
            #갱신된 각 SW UTM 네트워크 정보를 가져온다.
            rule_current = self.connect.select(self.sql.select_parsing_date(self.main_data.ax_gate_info["ax_gate_ip"]))
            
            if config.SyncConfig.isReal == True :
                """
                #/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
                #/\-각 zone(고객사)별 스레드 할당
                #/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
                """
                #각 zone(고객사)별 스레드 관리 변수
                threads = []
                       
                #각 스레드별 ID 초기값 설정
                thread_id = 1
                
                #파싱된 dhcp/dnat/snat 파일이 생성된 zone별로 작업 스레드를 할당한다
                for zone_row in self.main_data.zone:
        
                    #스레드 작업 할당
                    th = threading.Thread(target=self.utm_setting, args=(zone_row, thread_id, rule_his, rule_current))
                                  
                    #스레드 시작
                    th.start()
                                  
                    #스레드 ID변경
                    thread_id = thread_id + 1
                                  
                    #스레드 관리를 위한 객체 저장
                    threads.append(th)
                              
                #스레드 관리 - 할당된 zone별 작업이 끝날때까지 대기 한다
                for thread in threads:
                    thread.join()
 
            #AXGate별 작업 히스토리 확인을 위한 DB이력 저장(UI출력을 위한 Insert)
            #self.connect.update(self.sql.update_trace_history(self.main_data.ax_gate_info["org_seq"], self.main_data.ax_gate_info["trace_seq"]))
        
        except Exception as ex :
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("run", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = str(ex)
        finally:
            #AXGate별 작업 히스토리 확인을 위한 DB이력 저장(UI출력을 위한 Insert)
            self.connect.update(self.sql.update_trace_history(self.main_data.ax_gate_info["org_seq"], self.main_data.ax_gate_info["trace_seq"]))
            
            #메인 E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_thread_job(9999)
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("AXGate 룰 정보 파싱 종료(%s)" % (self.main_data.ax_gate_info["ax_gate_ip"]))
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #데이터 확인
            if config.SyncConfig.isReal != True :
                self.main_data.print_data()
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - FTP 전송. 
    """
    def sendFtp(self, filepath, localpath, hostIP, hostPort, username, password):
        count = 1
        max_count = 3
        
        send_check = False
        
        #FTP 전송 시도를 최대 3회 까지 실행한다.
        while count <= max_count:
            try:
                #접속 정보 설정.
                connectInfo = paramiko.Transport(hostIP, hostPort)
                connectInfo.connect(username = username, password = password)
                
                #sftp 연결.
                sftp = paramiko.SFTPClient.from_transport(connectInfo)
                            
                # 파일 전송.
                sftp.put(localpath, filepath)
                connectInfo.close()
                send_check = True
                
            except Exception as ex :
                log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("sendFtp", str(type(ex)), str(ex.args), str(ex)))
                send_check = False
            
            if send_check == True:
                break
            
            if count == max_count:
                return "FTP 전송 실패"
            
            count = count + 1
        
        return ""
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - UTM 생성. 
    """    
    def create_utm(self, utm_userid, zone_name):

        try:
            client = HttpClient()
            
            #org_seq 정보 확인
            data = {}
            redata = {}
            orgseq = self.main_data.ax_gate_info["org_seq"]
            templateseq = self.connect.select(self.sql.select_template_seq("GiGA_Office(NFV-UTM)"))
            
            if templateseq == None or len(templateseq) == 0:
                log.info("templateseq 확인 실패")
                return "templateseq 확인 실패", None, ""
            
            #사용자 생성.
            data = {"orgseq":orgseq,
                    "userid":utm_userid, 
                    "username":zone_name, 
                    "password": "%s%s" % ( zone_name, config.SyncConfig.utm_password ) , # 비밀번호 수정 ( zone_id + 기존 비번 )
                    "backuputmseq":"1"}
             
            log.info("계정 생성 요청 : %s" % (str(data)))
             
            redata = client.send(data, "%s" % (config.SyncConfig.orchestrator_server_info["info"]["url"]["create_userid"]), "post", "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
              
            if redata["result"] == "fail":
                self.log.info(redata["result"], "I")
                self.log.info(redata["description"], "I")
                return "계정 생성 요청 실패", None, json.dumps(redata)
              
            #provisionseq 획득 요청.
            data = {"templateseq":templateseq[0]["nstemplateseq"],"orgseq":int(orgseq),"userid":utm_userid}
             
            log.info("provisionseq 획득 요청 : %s" % (str(data)))
             
            redata = client.send(data, "%s" % (config.SyncConfig.orchestrator_server_info["info"]["url"]["get_provisions_seq"]), "post", "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
              
            #success
            if redata["response"]["result"] == "fail":
                log.info(redata["response"])
                log.info("provisionseq 획득 실패 : %s" % (str(data)))
                return "provisionseq 획득 실패", None, json.dumps(redata)
             
            #획득한 provisionseq 저장
            provisionseq = redata["response"]["provisionseq"]
            
            #이더넷 정보를 저장할 변수 선언
            ethernet_info = []
            
            for vlan_row in self.main_data.zone[zone_name]["network"]["interface"]:
                
                if vlan_row["check"] == True:
                    if vlan_row["vlan_id"] == "eth1":
                        data = self.main_data.zone[zone_name]["network"]["default_router"]
                        if len(self.main_data.zone[zone_name]["network"]["default_router"]) == 0:
                            return "default_router 정보 없음", provisionseq, ""
                            
                        ethernet_info.append({
                                            "eth1": {
                                              "subnetcidr": {
                                                "display_name": "Eth1 Subnet Cidr",
                                                "name": "eth1_subnet_cidr",
                                                "value": vlan_row["ip"]
                                              },
                                              "vlan": {
                                                "display_name": "eth1 zone vlan",
                                                "name": "eth1_vlan",
                                                "value": vlan_row["vlan"].split(".")[1],
                                                "status": self.convert_shutdown2status(vlan_row["ip"], vlan_row["shutdown"])
                                              },
                                              "defaultgateway": {
                                                "display_name": "Default Gateway",
                                                "name": "default_gateway",
                                                "value": self.main_data.zone[zone_name]["network"]["default_router"]
                                              }
                                            }
                                          })
                        
                    elif vlan_row["vlan_id"] == "eth2":
                        ethernet_info.append({
                                            "eth2": {
                                              "subnetcidr": {
                                                "display_name": "eth2 Subnet Cidr",
                                                "name": "eth2_subnet_cidr",
                                                "value": vlan_row["ip"]
                                              },
                                              "vlan": {
                                                "display_name": "eth2 zone vlan",
                                                "name": "eth2_vlan",
                                                "value": vlan_row["vlan"].split(".")[1],
                                                "status": self.convert_shutdown2status(vlan_row["ip"], vlan_row["shutdown"])
                                              }
                                            }
                                          })
                            
                    
                    elif vlan_row["vlan_id"] == "eth3":
                        ethernet_info.append({
                                            "eth3": {
                                              "subnetcidr": {
                                                "display_name": "eth3 Subnet Cidr",
                                                "name": "eth3_subnet_cidr",
                                                "value": vlan_row["ip"]
                                              },
                                              "vlan": {
                                                "display_name": "eth3 zone vlan",
                                                "name": "eth3_vlan",
                                                "value": vlan_row["vlan"].split(".")[1],
                                                "status": self.convert_shutdown2status(vlan_row["ip"], vlan_row["shutdown"])
                                              }
                                            }
                                          })
                            
                    elif vlan_row["vlan_id"] == "eth4":
                        ethernet_info.append({
                                            "eth4": {
                                              "subnetcidr": {
                                                "display_name": "eth4 Subnet Cidr",
                                                "name": "eth4_subnet_cidr",
                                                "value": vlan_row["ip"]
                                              },
                                              "vlan": {
                                                "display_name": "eth4 zone vlan",
                                                "name": "eth4_vlan",
                                                "value": vlan_row["vlan"].split(".")[1],
                                                "status": self.convert_shutdown2status(vlan_row["ip"], vlan_row["shutdown"])
                                              }
                                            }
                                          })
                    
                    elif vlan_row["vlan_id"] == "eth5":
                        ethernet_info.append({
                                            "eth5": {
                                              "subnetcidr": {
                                                "display_name": "eth5 Subnet Cidr",
                                                "name": "eth5_subnet_cidr",
                                                "value": vlan_row["ip"]
                                              },
                                              "vlan": {
                                                "display_name": "eth5 zone vlan",
                                                "name": "eth5_vlan",
                                                "value": vlan_row["vlan"].split(".")[1],
                                                "status": self.convert_shutdown2status(vlan_row["ip"], vlan_row["shutdown"])
                                              }
                                            }
                                          })
            
            #프로비저닝 요청 파라미터 생성
            data = {
                      "provision": {
                        "syncseq":self.main_data.ax_gate_info["trace_seq"],
                        "hostname":zone_name,
                        "templateseq": templateseq[0]["nstemplateseq"],
                        "provisionseq": int(provisionseq),
                        "orgseq": int(orgseq),
                        "userid": utm_userid,
                        "application": [
                          {
                            "necatseq": 61,
                            "service": [
                              {
                                "item": [
                                  {
                                    "ethernet": ethernet_info
                                  },
                                  {
                                    "bridge": [
                                      {
                                        "br0": {
                                          "display_name": "bridge0",
                                          "name": "br0",
                                          "value": self.main_data.zone[zone_name]["network"]["bridge"][0],
                                          "proxy_arp": self.main_data.zone[zone_name]["network"]["proxy-arp"][0],
                                          "status": self.convert_shutdown2status(self.main_data.zone[zone_name]["network"]["bridge"][0], self.main_data.zone[zone_name]["network"]["shutdown"][0]) 
                                        }
                                      },
                                      {
                                        "br1": {
                                          "display_name": "bridge1",
                                          "name": "br1",
                                          "value": self.main_data.zone[zone_name]["network"]["bridge"][1],
                                          "proxy_arp": self.main_data.zone[zone_name]["network"]["proxy-arp"][1],
                                          "status": self.convert_shutdown2status(self.main_data.zone[zone_name]["network"]["bridge"][1], self.main_data.zone[zone_name]["network"]["shutdown"][1])
                                        }
                                      },
                                      {
                                        "br2": {
                                          "display_name": "bridge2",
                                          "name": "br2",
                                          "value": self.main_data.zone[zone_name]["network"]["bridge"][2],
                                          "proxy_arp": self.main_data.zone[zone_name]["network"]["proxy-arp"][2],
                                          "status": self.convert_shutdown2status(self.main_data.zone[zone_name]["network"]["bridge"][2], self.main_data.zone[zone_name]["network"]["shutdown"][2])
                                        }
                                      }
                                    ]
                                  },
                                  {
                                    "router": {
                                    "display_name": "router",
                                    "name": "router",
                                    "value": config.SyncConfig.testing_secondary_router
                                    }
                                  }
                                ],
                                "name": "Olleh-vUTM"
                              }
                            ],
                            "name": "Olleh-vUTM",
                            "neclsseq": 32
                          }
                        ]
                      }
                    }
            log.debug(data)
            
            redata = client.send(data, "%s" % (config.SyncConfig.orchestrator_server_info["info"]["url"]["create_provisions"]), "post", "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
             
            if redata["response"]["result"] == "fail":
                log.info(redata["response"])
                return "UTM 생성 실패 : %s" % (zone_name), provisionseq, json.dumps(redata)
              
            #생성된 UTM ID 확인.
            getUtmid = self.connect.select(self.sql.selectUtmIdInfo(provisionseq))
              
            insert_result = self.connect.insert(self.sql.insert_provision_info(utm_userid, "%s%s" % (zone_name, config.SyncConfig.utm_password), getUtmid[0]["res_id"], provisionseq, self.main_data.zone[zone_name]["utm_mappid"]))
 
            if insert_result != 1:
                return "UTM 생성 실패 : %s" % (zone_name), provisionseq, ""
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("create_utm", str(type(ex)), str(ex.args), str(ex)))
            return "UTM 생성 실패 : %s" % (zone_name), provisionseq, str(ex)
        
        return "", provisionseq, json.dumps(redata)
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - UTM 생성 및 룰 정보/네트워크 정보 적용. 
    """
    def rule_setup(self, remote_file_path, local_file_path, vm_remote_file_path, mg_vm_ip, mg_vm_id, mg_vm_password, utm_userid, utm_vm_password, utm_provision_id, zone_id, jobcontrol):
        redata   = {}
        
        try:
            if os.path.isfile(local_file_path):
                log.info("FTP 전송 시작(%s)" % (zone_id))
                
                redata = self.sendFtp(remote_file_path, local_file_path, mg_vm_ip, 22, mg_vm_id, mg_vm_password)
                if redata != "":
                    return "FTP 전송 실패(%s)" % (zone_id), json.dumps(redata)

                log.info("FTP 전송 완료(%s)" % (zone_id))
                     
                data = {"orgseq":int(self.main_data.ax_gate_info["org_seq"]),
                        "userid":utm_userid, 
                        "password":utm_vm_password, 
                        "vmid":utm_provision_id,
                        "localfile":remote_file_path,
                        "remotefile":vm_remote_file_path,
                        "jobcontrol":jobcontrol}
                
                client = HttpClient()
                redata = client.send(data, "/fileupload", "post", "http://%s:%s"  % (mg_vm_ip, "80"))
                     
                if redata.get("result") == "fail":
                    log.info("Rule 적용 실패(%s) : %s" % (zone_id, redata.get("result")))
                    return "Rule 적용 실패(%s)" % (zone_id), json.dumps(redata)
                    
            else:
                #log.info("FTP 전송 Rule 없음(%s)" % (zone_id))
                return "FTP 전송 Rule 없음(%s)" % (zone_id), ""
            
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("rule_setup", str(type(ex)), str(ex.args), str(ex)))
            return "FTP 전송/Rule 적용 실패(%s)" % (zone_id), str(ex)
        
        return "", json.dumps(redata)
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - UTM 생성 및 룰 정보/네트워크 정보 적용. 
    """
    def utm_setting(self, zone_id, thread_id, rule_his, rule_current):
        try:
            fail_msg = ""
            utm_userid = "%s_%s" % (zone_id, self.main_data.ax_gate_info["ax_gate_ip"].replace(".", "_"))
            client = HttpClient()
            ingress_security_port = {}
            egress_security_port = {}
            
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("룰 적용 시작(%s)" % (zone_id))
            e2e_msg.set_indent(1)
            e2e_msg.set_thread_job(thread_id)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #현재 프로비저닝이 안되는 대상은 처리하지 않는다.향후 삭제 요망
#             if utm_userid == "zone200_211_224_204_160" or utm_userid == "zone1000_211_224_204_160" or utm_userid == "vd10_211_224_204_160" or utm_userid == "zone200_10_0_0_100" or utm_userid == "zone1000_10_0_0_100" or utm_userid == "vd10_10_0_0_100":
#                 return False
            
            if len(rule_his) > 0:
                rule_his = json.loads(json.dumps(rule_his))
                rule_current = json.loads(json.dumps(rule_current))
                
                if rule_his["0"]["parsing_data"].get(zone_id) != None:
                    if rule_his["0"]["parsing_data"][zone_id] != rule_current["0"]["parsing_data"][zone_id]:
                        #네트워크 정보가 변경된 UTM을 삭제 한다.
                        log.debug("%s\n%s" % (rule_his["0"]["parsing_data"][zone_id], rule_current["0"]["parsing_data"][zone_id]))
                        
                        #UTM정보 확인.
                        utm_info = self.connect.select(self.sql.selectUtmInfo(utm_userid))
                        
                        #UTM정보가 있을 경우 UTM삭제를 진행한다.
                        if len(utm_info) > 0:
                            #파라미터로 provisionseq 저장
                            data = int(utm_info[0]["provisionseq"])
                            
                            #UTM삭제 요청
                            redata = client.send(data, "%s" % (config.SyncConfig.orchestrator_server_info["info"]["url"]["delete_utm"]), "delete", "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
                            
                            #UTM삭제 결과 확인
                            case_detail_msg = None 
                            if redata.get("result") != "fail":
                                utmInfoDeleteResult = self.connect.delete(self.sql.deleteUtmInfo(utm_userid))
                                if utmInfoDeleteResult != 0:
                                    log.info("UTM 정보 삭제 완료 : %s" % (utm_userid))
                                    
                                else:
                                    fail_msg = "UTM 정보 삭제 실패 : %s" % (utm_userid)
                                    case_detail_msg = {"result" : "%s" % (utmInfoDeleteResult)}
                                    #log.info("UTM 정보 삭제 실패 : %s" % (utm_userid))
                                    #return False
                                
                            else:
                                fail_msg = "UTM 삭제 연동 실패 : %s" % (utm_userid)
                                case_detail_msg = redata
                                #log.info("UTM 삭제 연동 실패 : %s" % (utm_userid))
                                #return False
                            
                            # Sync Event 저장
                            if fail_msg != "" :
                                log.info(fail_msg)
                                self.main_data.set_event_msg_update("PO", utm_userid, "U", "S", fail_msg, self.print_list(case_detail_msg) )
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                return False

            #UTM정보 확인.
            utm_info = self.connect.select(self.sql.selectUtmInfo(utm_userid))
            
            if len(utm_info) > 1:
                #돌발 상황으로 인해 UTM정보는 있지만 UTM이 삭제된 경우가 있어 현재 UTM정보 상태를 확인한다.
                utm_status_check = self.connect.select(self.sql.select_utm_delete_check(utm_info[0]["provisionseq"])) 
            
                #UTM정보가 삭제된 경우 이력이 남기 때문에 값이 존재하기 때문에 DB에서 삭제한다.
                if len(utm_status_check) > 0:
                    log.info("잘못된 UTM 정보 존재 DB 삭제 : %s" % (utm_userid))
                    
                    #UTM DB 정보 삭제
                    utmInfoDeleteResult = self.connect.delete(self.sql.deleteUtmInfo(utm_userid))
                    if utmInfoDeleteResult != 0:
                        log.info("잘못된 UTM 정보 삭제 완료: %s" % (utm_userid))
                        
                    else:
                        #log.info("잘못된 UTM 정보 삭제 실패 : %s" % (utm_userid))
                        fail_msg = "잘못된 UTM 정보 삭제 실패 : %s" % (utm_userid)
                        #return False
                    
                    # Sync Event 저장
                    if fail_msg != "" :
                        log.info(fail_msg)
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "S", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        return False
            
            #UTM정보 확인.(위 돌발상황으로 인해 UTM정보가 삭제되지 않아 강제로 UTM정보를 삭제한 경우때문에 값을 다시한번 확인한다.)
            utm_info = self.connect.select(self.sql.selectUtmInfo(utm_userid))
            
            #utm_provision_id 가 없을 경우 SW UTM이 생성되지 않은 경우다.
            if len(utm_info) < 1:
                log.info("신규 UTM 생성(%s)" % (utm_userid))
 
                #E2E Trace 생성
                e2e_msg_sub = MsgData()
                e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
                e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
                e2e_msg_sub.set_main_class("Sync Manager")
                e2e_msg_sub.set_sub_class("Rule Sync")
                 
                e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
                e2e_msg_sub.set_start_dttm()
                e2e_msg_sub.set_message("신규 UTM 생성(%s)" % (zone_id))
                e2e_msg_sub.set_indent(2)
                e2e_msg_sub.set_thread_job(thread_id)
                 
                #UTM 생성
                fail_msg, provisionseq, redata = self.create_utm(utm_userid, zone_id)
                 
                e2e_msg_sub.set_provision_seq(provisionseq)
                e2e_msg_sub.set_response_status(fail_msg)
                e2e_msg_sub.set_response(redata)
                e2e_msg_sub.set_message_detail(fail_msg)
                e2e_msg_sub.set_end_dttm()
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                if fail_msg != "":
                    #log.info("UTM 생성 실패(%s)" % (zone_id))
                    fail_msg = "UTM 생성 실패(%s)" % (zone_id)
                    
                    # Sync Event 저장
                    log.info(fail_msg)
                    self.main_data.set_event_msg_update("PO", utm_userid, "U", "S", fail_msg, redata)
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    return False
                 
                log.info("UTM 생성 성공(%s)" % (zone_id))
            
            
            #생성된 UTM 정보를 가져온다.
            utm_server_info = self.connect.select(self.sql.selectAXGateServerInfoTest(utm_userid))

            if len(utm_server_info) < 1:
                fail_msg = "UTM 정보 없음(%s)" % (utm_userid)
                log.info(fail_msg)
                #log.info("UTM 정보 없음(%s)" % (utm_userid))
                
                # Sync Event 저장
                self.main_data.set_event_msg_update("PO", utm_userid, "U", "S", fail_msg, fail_msg)
                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                return False
            
            mgVmInfo = self.connect.select(self.sql.selectMvInfo(self.main_data.ax_gate_info["org_seq"]))
             
            if len(mgVmInfo) < 1 :
                case_detail_msg = "MG UTM 정보 없음(%s, orgseq : %s)" % (zone_id, self.main_data.ax_gate_info["org_seq"])
                fail_msg = "MG UTM 정보 없음(orgseq : %s)" % (self.main_data.ax_gate_info["org_seq"])
                log.info(fail_msg)
                
                # Sync Event 저장
                self.main_data.set_event_msg_update("PO", utm_userid, "U", "S", fail_msg, case_detail_msg)
                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                #return "MG UTM 정보 없음(orgseq : %s)" % (self.main_data.ax_gate_info["org_seq"]), None
                return fail_msg
            
            #개발 테스트가 아닌경우에만 수행
            if config.SyncConfig.isReal == True:
                #암호화된 데이터 복호화
                aes = AESCipher(config.SyncConfig.security_key)
                mg_vm_ip = aes.decrypt(mgVmInfo[0]["infraprops"]["mgmtvms"][0]["host"])
                mg_vm_id = aes.decrypt(mgVmInfo[0]["infraprops"]["mgmtvms"][0]["userid"])
                mg_vm_password = aes.decrypt(mgVmInfo[0]["infraprops"]["mgmtvms"][0]["password"])
                 
                #E2E Trace 생성
                e2e_msg_sub_2 = MsgData()
                e2e_msg_sub_2.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
                e2e_msg_sub_2.set_orgseq (self.main_data.ax_gate_info["org_seq"])
                e2e_msg_sub_2.set_main_class("Sync Manager")
                e2e_msg_sub_2.set_sub_class("Rule Sync")
                  
                e2e_msg_sub_2.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
                e2e_msg_sub_2.set_indent(2)
                e2e_msg_sub_2.set_thread_job(thread_id)
     
                ###################################################################################################################
                #DHCP 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("DHCP 적용(%s)" % (zone_id))
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["dhcp"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["dhcp"]["name"]), 
                                                    "/var/efw/dhcp/settings", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol request dhcp.updatewizard")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["dhcp"]) > 0:
                        if ingress_security_port.get("udp") == None:
                            ingress_security_port["udp"] = []
                        if egress_security_port.get("udp") == None:
                            egress_security_port["udp"] = []
                             
                        ingress_security_port["udp"].append(67)
                        egress_security_port["udp"].append(67)
                        ingress_security_port["udp"].append(68)
                        egress_security_port["udp"].append(68)
                
                if redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("DHCP %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    if fail_msg != "":# 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################

                ###################################################################################################################
                #DNAT 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("DNAT 적용(%s)" % (zone_id))
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["dnat"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["dnat"]["name"]), 
                                                    "/var/efw/dnat/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setdnat")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["dnat"]) > 0:
                        self.get_security_port("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["dnat"]["name"]), ingress_security_port, egress_security_port, "dnat")
                
                if redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("DNAT %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    if fail_msg != "": # 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                 
                ###################################################################################################################
                #SNAT 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("SNAT 적용(%s)" % (zone_id))
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["snat"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["snat"]["name"]), 
                                                    "/var/efw/snat/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setsnat")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["snat"]) > 0:
                        self.get_security_port("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["snat"]["name"]), ingress_security_port, egress_security_port, "snat")
                
                if redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("SNAT %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    if fail_msg != "" :# 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
                ###################################################################################################################
                #방화벽[incoming] 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("방화벽[INCOMING] 적용(%s)" % (zone_id)) 
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["fw_incoming"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_incoming"]["name"]), 
                                                    "/var/efw/incoming/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setincoming")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["fw_incoming"]) > 0:
                        self.get_security_port("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_incoming"]["name"]), ingress_security_port, egress_security_port, "fw_incoming")
                
                if  redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("FW[INCOMING] %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    
                    if fail_msg != "" :  # 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
                ###################################################################################################################
                #방화벽[outgoing] 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("방화벽[OUTGOING] 적용(%s)" % (zone_id)) 
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["fw_outgoing"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_outgoing"]["name"]), 
                                                    "/var/efw/outgoing/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setoutgoing")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["fw_outgoing"]) > 0:
                        self.get_security_port("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_outgoing"]["name"]), ingress_security_port, egress_security_port, "fw_outgoing")
                
                if  redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("FW[OUTGOING] %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    
                    if fail_msg != "" :  # 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
                ###################################################################################################################
                #방화벽[interzone] 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("방화벽[INTERZONE] 적용(%s)" % (zone_id)) 
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["fw_interzone"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_interzone"]["name"]), 
                                                    "/var/efw/zonefw/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setzonefw")
                
                if fail_msg == "" : # 성공의 경우만
                    if len(self.main_data.zone[zone_id]["fw_interzone"]) > 0:
                        self.get_security_port("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["fw_interzone"]["name"]), ingress_security_port, egress_security_port, "fw_interzone")
                
                if  redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("FW[INTERZONE] %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    
                    if fail_msg != "" :  # 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
                ###################################################################################################################
                #Router 정보 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("Router 적용(%s)" % (zone_id)) 
                fail_msg, redata = self.rule_setup("/home/orchestrator/%s_%s" % (zone_id, self.main_data.file_info["router"]["name"]), 
                                                    "%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_id, self.main_data.file_info["router"]["name"]), 
                                                    "/var/efw/routing/config", 
                                                    mg_vm_ip, 
                                                    mg_vm_id, 
                                                    mg_vm_password, 
                                                    utm_server_info[0]["utm_userid"], 
                                                    utm_server_info[0]["utm_vm_password"], 
                                                    utm_server_info[0]["utm_provision_id"],
                                                    zone_id,
                                                    "jobcontrol restart setrouting")
                
                if redata == "" :    # Skip의 경우 파일없으므로.
                    log.info("ROUTER %s" % fail_msg)
                    
                else :                                  # 성공 또는 실패의 경우
                    if fail_msg != "" : # 실패의 경우만 - Sync Event 저장
                        self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, redata)
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    # e2e Trace
                    e2e_msg_sub_2.set_request_parameter("")
                    e2e_msg_sub_2.set_response(redata)
                    e2e_msg_sub_2.set_response_status(fail_msg)
                    e2e_msg_sub_2.set_message_detail(fail_msg)
                    e2e_msg_sub_2.set_end_dttm()
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
                #중복 제거.
                for row in ingress_security_port:
                    ingress_security_port[row] = list(set(ingress_security_port[row]))
                  
                for row in egress_security_port:
                    egress_security_port[row] = list(set(egress_security_port[row]))
                
                ###################################################################################################################    
                #security_port 적용
                e2e_msg_sub_2.set_start_dttm()
                e2e_msg_sub_2.set_message("security_port 적용(%s)" % (zone_id))
                data = {
                          "orgseq": int(self.main_data.ax_gate_info["org_seq"]),
                          "userid": utm_server_info[0]["utm_userid"],
                          "password": utm_server_info[0]["utm_vm_password"],
                          "vmid": utm_server_info[0]["utm_provision_id"],
                          "ingress_security_port": ingress_security_port,
                          "egress_security_port": egress_security_port
                        }
    
                log.info("security_port 요청(%s)" % (data))
                redata = client.send(data, "%s" % (config.SyncConfig.orchestrator_server_info["info"]["url"]["security_group"]), "post", "http://%s" % (config.SyncConfig.orchestrator_server_info["info"]["ip"]))
                
                fail_msg = ""
                detail_msg = ""
                if redata["result"] == "fail":
                    fail_msg = "security_port 적용 실패(%s)" % (zone_id)
                    detail_msg = fail_msg
                    log.info(fail_msg)
                    
                    self.main_data.set_event_msg_update("PO", utm_userid, "U", "C", fail_msg, json.dumps(redata))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                else :
                    detail_msg = "security_port 적용 성공"
                    log.info("security_port 적용 성공(%s)" % (redata))
                
                e2e_msg_sub_2.set_request_parameter("")
                e2e_msg_sub_2.set_response(json.dumps(redata))   
                e2e_msg_sub_2.set_response_status(fail_msg)
                e2e_msg_sub_2.set_message_detail(detail_msg)
                e2e_msg_sub_2.set_end_dttm()
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub_2))
                ###################################################################################################################
                
            fail_msg = ""    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("utm_setting", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "룰 적용 실패(%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("룰 적용 종료(%s)" % (zone_id))
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))

    """
    @Created - 2015. 02. 10.
    @author - 정현성
    @description - security port 얻기
    """    
    def get_security_port(self, localFileRoot, ingress_security_port, egress_security_port, data_type):
        
        try:
            read_file_info = self.fileRead(localFileRoot)
            
            if read_file_info == None:
                self.log.info("get_security_port 파일 읽기 실패(%s)" % (localFileRoot), "I")
                return
            
            if data_type == "dnat":
                for line_info in read_file_info.split('\n'):
                    ##############################################################################################
                    # - on,tcp,,211.196.251.154/30,,211.193.42.24,43315,10.10.9.22,43315,DNAT,,,ACCEPT
                    ##############################################################################################
                    line_info = line_info.split(',')
                    
                    if line_info[1] == "":
                        line_info[1] = "any"
                    
                    if line_info[6] == "":
                        line_info[6] = "any"
                    
                    if line_info[8] == "":
                        line_info[8] = "any"
                    
                    if ingress_security_port.get(line_info[1]) == None:
                        ingress_security_port[line_info[1]] = []
                    
                    if egress_security_port.get(line_info[1]) == None:
                        egress_security_port[line_info[1]] = []
                    
                    ingress_security_port[line_info[1]].append(line_info[6])
                    egress_security_port[line_info[1]].append(line_info[8])
                    
            elif data_type == "snat":
                for line_info in read_file_info.split('\n'):
                    ##############################################################################################
                    # - on,,10.10.4.0/24,211.196.251.154/30,,,SNAT,,,211.193.42.51
                    ##############################################################################################
                    line_info = line_info.split(',')
                    
                    if line_info[1] == "":
                        line_info[1] = "any"
                        
                    if line_info[2] == "":
                        line_info[2] = "any"
                    
                    if line_info[4] == "":
                        line_info[4] = "any"
                    
                    if egress_security_port.get(line_info[1]) == None:
                        egress_security_port[line_info[1]] = []
                        
                    egress_security_port[line_info[1]].append(line_info[4])
                    
            elif data_type == "firewall":
                for line_info in read_file_info.split('\n'):
                    ##############################################################################################
                    # - off,tcp,100.100.100.128/25,200.200.200.0/25,8000:8100,ACCEPT,,,,,
                    ##############################################################################################
                    line_info = line_info.split(',')
                    
                    if line_info[1] == "":
                        line_info[1] = "any"
                    
                    if line_info[4] == "":
                        line_info[4] = "any"
                    
                    if egress_security_port.get(line_info[1]) == None:
                        egress_security_port[line_info[1]] = []

                    egress_security_port[line_info[1]].append(line_info[4])
                    
            elif data_type == "fw_outgoing" or data_type == "fw_interzone" :
                for line_info in read_file_info.split('\n'):
                    ##############################################################################################
                    # - off,tcp,100.100.100.128/25,200.200.200.0/25,8000:8100,ACCEPT,,,,,
                    ##############################################################################################
                    line_info = line_info.split(',')
                    
                    if line_info[1] == "":
                        line_info[1] = "any"
                    
                    if line_info[4] == "":
                        line_info[4] = "any"
                    
                    if egress_security_port.get(line_info[1]) == None:
                        egress_security_port[line_info[1]] = []

                    egress_security_port[line_info[1]].append(line_info[4])
                    
            elif data_type == "fw_incoming":
                for line_info in read_file_info.split('\n'):
                    ##############################################################################################
                    # - off,tcp,100.100.100.128/25,200.200.200.0/25,8000:8100,ACCEPT,,,,,
                    ##############################################################################################
                    line_info = line_info.split(',')
                    
                    if line_info[1] == "":
                        line_info[1] = "any"
                    
                    if line_info[6] == "":
                        line_info[6] = "any"
                    
                    if egress_security_port.get(line_info[1]) == None:
                        egress_security_port[line_info[1]] = []

                    egress_security_port[line_info[1]].append(line_info[6])
                
        except Exception as ex:
            self.log.error("\t%s" % ("get_security_port - ex : "), "E")
            self.log.error("\t%s" % (str(type(ex))))
            self.log.error("\t%s" % (str(ex.args)))
            self.log.error("\t%s" % (str(ex)))

    """
    @Created - 2015. 02. 10.
    @author - 정현성
    @description - AXGate의 DHCP/Rule/sNat/dNat 정보를 읽어와 저장한다.
    """ 
    def ax_gate_ssh_access(self):
        fail_msg = ""
        access_info = ""
        detail_msg = ""
        case_msg = ""
        
        try:
            #메인 E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("AXGate 룰 정보 수신")
            e2e_msg.set_indent(1)
            
            #SSH 접속 정보 획득
            access_info = "java -jar %s %s %s %s %s" % (config.SyncConfig.ax_gate_info["ax_gate_server_info"]["ssh_access_path"],   #SSH접속을 위한 jar파일 위치 
                                                        config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"],                #AXGate접속 계정
                                                        config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"],          #AXGate접속 암호
                                                        self.main_data.ax_gate_info["ax_gate_ip"],                                  #AXGate접속 IP
                                                        config.SyncConfig.ax_gate_info["ax_gate_server_info"]["port"])              #AXGate접속 Port
            
            srv = subprocess.Popen(access_info, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell = True)
            
            #파일 실행후 읽어오는 시간 여유가 필요해 2초정도 여유시간을 준다.(4초 이상 여유시간을 줄 경우 데이터가 출력되지 않을수 있음 - SSHAccess.jar 설정 시간이 5초이기 때문임)
            time.sleep(3) 
            
            #print srv.stderr.read()
            self.main_data.rule_file_info = srv.stdout.read()
            
            #rule_file_info의 값이 존재하지 않거나 rule_file_info의 길이가 1000보다 낮은경우 AXGate의 정보 가져오기 실패로 본다.
            #rule_file_info의 길이가 1000보다 낮은경우 실패로 보는 이유는 특정 이유로 인해 jar실행 단계에서 실패가 난경우 Exception을 찍기 때문에이다.
            #AXGate의 룰정보를 가져올경우 최소 1000보다 길이가 길때 때문에 이런 확인작업이 추가 되었다.
            if self.main_data.rule_file_info == None or self.main_data.rule_file_info == "" or len(self.main_data.rule_file_info) < 1000 :
                case_msg    = "AXGate 데이터 가져오기 실패(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                detail_msg  = self.main_data.rule_file_info
                fail_msg    = "%s\n%s" % (case_msg, detail_msg)
                return fail_msg
            
            #기본 폴더 위치 정보 저장.
            self.main_data.file_info["directory_path"] = "%s/%s/%s/%s/%s" % (config.SyncConfig.directory_path["path"], 
                                                config.SyncConfig.directory_info["logDirectory"], 
                                                config.SyncConfig.directory_info["rootDirectory"], 
                                                self.main_data.ax_gate_info["office_code"],
                                                self.main_data.ax_gate_info["ax_gate_ip"])
            
            #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
            if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"]), config.SyncConfig.directory_path["os_type"]) == False:
                case_msg    = "룰 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                detail_msg  = "%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"])
                fail_msg    = "%s\n%s" % (case_msg, detail_msg)
                return fail_msg
            
            #룰 정보 저장
            if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"]), self.main_data.rule_file_info, "forced") == False:
                case_msg    = "룰 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                detail_msg  = "%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"]) 
                fail_msg    = "%s\n%s" % (case_msg, detail_msg)
                return fail_msg
            
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("ax_gate_ssh_access", str(type(ex)), str(ex.args), str(ex)))
            case_msg    = "AxGate 연결 실패(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
            detail_msg  = str(ex)
            fail_msg    = "%s\n%s" % (case_msg, detail_msg)
            
        finally:
            #event 설정
            if case_msg != "":
                self.main_data.set_event_msg_update("AA", "", "S", "S", case_msg, detail_msg)
                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
            
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            #e2e_msg.set_request_parameter(access_info)
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message_detail(self.main_data.rule_file_info if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    """
    @Created - 2015. 02. 25.
    @author - 정현성
    @description - 폴더를 생성한다.
    """
    def create_directory(self, directory_info, os_type):
        
        try:
            #path /를 기준으로 분리
            path_split = directory_info.split('/')
            path_log = ""
            
            for path in path_split:
                if path == "" or path == None:
                    continue
                
                if os_type == "windows":
                    if path_log == "":
                        path_log = "%s" % (path)
                        continue
                    else:
                        path_log = "%s/%s" % (path_log, path)
                        
                elif os_type == "linux":
                    path_log = "%s/%s" % (path_log, path)
                    
                if not os.path.isdir(path_log):
                    os.mkdir(path_log)
            
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("create_directory", str(type(ex)), str(ex.args), str(ex)))
            
            #디렉토리 생성 실패.
            return False
        
        #디렉토리 생성 성공.
        return True  
    
    """
    @Created - 2015. 02. 10.
    @author - 정현성
    @description - 파일에 정보를 기록한다.
    """
    def fileWrite(self, directory_info, strWriteText, strWriteType):
        
        try:
            #파일이 존재하는지 확인한다.
            if os.path.isfile(directory_info):
                if strWriteType == "forced":
                    #파일이 존재해도 강제 쓰기 모드일 경우 처리.
                    openFile = open(directory_info, 'w')
                else:
                    #강제 쓰기 모드가 아닐 경우 처리.
                    openFile = open(directory_info, 'a')
            else:
                #파일이 존재하지 않는 경우 쓰기 모드로 파일 오픈.
                openFile = open(directory_info, 'w')
            
            #파일에 데이터 기록 하기.
            openFile.write(strWriteText)
            
            #세션 종료.
            openFile.close()
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("fileWrite", str(type(ex)), str(ex.args), str(ex)))
            return False
        
        return True
    
    """
    @Created - 2015. 02. 10.
    @author - 정현성
    @fileRead - 파일 정보를 읽어 온다.
    """
    def fileRead(self, directory_info):
        fileInfo = None
        openFile = None
        
        try:
            #파일이 존재하는지 확인한다.
            if os.path.isfile(directory_info):
                #파일이 존재할 경우 파일 오픈.
                openFile = open(directory_info, 'r')
            else:
                #파일이 존재하지 않는 경우 
                log.info("파일이 존재하지 않습니다. (%s)" % (directory_info))
                return None
                
            #읽어온 파일 정보 저장.
            fileInfo = openFile.read()
            
            #세션 종료.
            openFile.close()
        except Exception as ex:
            self.log.error("\t%s" % ("fileRead - ex : "), "E")
            self.log.error("\t%s" % (str(type(ex))))
            self.log.error("\t%s" % (str(ex.args)))
            self.log.error("\t%s" % (str(ex)))
            fileInfo = None
        
        return fileInfo
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - List에 있는 값중 지정된 값의 위치를 찾아 check_type 에 맞게 처리하는 공통 로직
    """
    def scan_description_check(self, description, line_info_split, check_type):
        #description에 대한 설정
        description_check = 0
        data = ""
        
        #description 위치 확인
        while description_check < len(line_info_split):
            if line_info_split[description_check] == description:
                if line_info_split[description_check+1] == "range":
                    #Endian은 port Range인 경우 : 를 사용하고 IP Range인 경우 - 을 사용한다.
                    if check_type == "port":
                        data = "%s:%s" % (line_info_split[description_check+2], line_info_split[description_check+3])
                        
                    elif check_type == "ip":
                        data = "%s-%s" % (line_info_split[description_check+2], line_info_split[description_check+3])
                    
                    elif check_type == "dynamic":
                        #특정 옵션의 경우 range옵션이 적용되지 않아 처음 IP만 사용하도록 설정)
                        data = "%s" % (line_info_split[description_check+2])
                        
                    elif check_type == "protocol":
                        #특정 상황에서 프로토콜 확인을 해야하는 경우가 발생 하여 그에 대한 예외 처리를 한다.
                        #프로토콜은 tcp/udp만 파싱 대상이 되고 이외 대상은 any 처리 한다.
                        if line_info_split[description_check+4] == "tcp" or line_info_split[description_check+4] == "udp":
                            data = "%s" % (line_info_split[description_check+4])
                        else:
                            log.info("정의되지 않은 프로토콜 정보가 포함되어 있습니다.\n%s" % (line_info_split))
                            data = "any"
                        
                    else:
                        #위 조건에 맞지 않을 경우 예외 표시를 위해 설정
                        data = "range_type_error" 

                    break
                            
                elif line_info_split[description_check+1] == "auto":
                    data = "" 
                    break
                
                elif line_info_split[description_check+1] == "static":
                    data = "%s" % (line_info_split[description_check+2])
                    break
                
                elif line_info_split[description_check+1] == "eq":
                    data = "%s" % (line_info_split[description_check+2])
                    break
                
                elif line_info_split[description_check+1] == "group" or line_info_split[description_check+1] == "group-set":
                    if self.main_data.temporary_ip_group.get(line_info_split[description_check+2]) != None:
                        data = "%s" % (self.main_data.temporary_ip_group.get(line_info_split[description_check+2])["data"])
                    
                    break
                
                elif line_info_split[description_check+1] == "multi":
                    multi_count = line_info_split[description_check+2]  #최초 시작 위치 지정
                    
                    #line_info를 스페이스로 split했을때 나온 사이즈 만큼 돌린다.
                    while multi_count < len(line_info_split):
                        try:
                            #Except가 발생할 경우 숫자가 아니라고 판단한다
                            isinstance(line_info_split[multi_count], int)
                        except SyntaxError:
                            break
                        
                        #port정보 저장
                        if data == "":
                            data = "%s" % (line_info_split[multi_count])
                            
                        else:
                            data = "%s&%s" % (data, line_info_split[multi_count])
                    
                    break
                
                else:
                    if description =="destination" and check_type == "protocol":
                        #프로토콜은 그대로 돌려주고 any/tcp/udp가 아닌경우만 로그로 남긴다.
                        data = "%s" % (line_info_split[description_check+2])
                        if line_info_split[description_check+2] != "any" and line_info_split[description_check+2] != "tcp" and line_info_split[description_check+2] != "udp":
                            log.info("정의되지 않은 프로토콜 정보가 포함되어 있습니다.\n%s" % (line_info_split))
                            
                        #프로토콜은 tcp/udp만 파싱 대상이 되고 이외 대상은 any 처리 한다.   
#                         if line_info_split[description_check+2] == "tcp" or line_info_split[description_check+2] == "udp":
#                             data = "%s" % (line_info_split[description_check+2])
#                         else:
#                             log.info("정의되지 않은 프로토콜 정보가 포함되어 있습니다.\n%s" % (line_info_split))
#                             data = "any"
                    else:
                        data = "%s" % (line_info_split[description_check+1])
                        
                    break
                
            description_check = description_check + 1
            
        return data
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - NAT Profile 정보를 파싱한다
    """
    def scan_nat_profile(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("NAT 프로파일 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #SNAT 프로파일 정보 파싱
            fail_msg = self.snat_profile_parsing()

            if fail_msg == "" :
                #DNAT 프로파일 정보 파싱
                fail_msg = self.dnat_profile_parsing()

        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_nat_profile", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "nat profile 생성 실패 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("NAT 프로파일 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - NAT Profile 정보를 파싱한다
    """
    def print_list(self, value):
        data = ""
        
        try:
            for value_row in value:
                
                if data == "":
                    data = "%s" % (value_row)
                else:
                    data = "%s\n%s" % (data, value_row)
                    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("print_list", str(type(ex)), str(ex.args), str(ex)))
            data = "생성 실패 (%s)" % (str(ex))

        return data
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - NAT Profile 정보를 파싱한다
    """
    def scan_service_group(self):
        fail_msg = ""
        e2e_service_group = {}
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("Server Group 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #E2E Sub Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_message("Server Group 정보 파싱")
            e2e_msg_sub.set_indent(2)
            
            #service_group 정보 추출.
            for service_group_info_row in self.main_data.rule["service_group"]:
                service_group_id = ""   #service group ID 관리 변수
                temp_service_group_list = []    #service group 저장을 위한 임시 변수
                
                for line_info in service_group_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - service group window_terminal
                    # - proto tcp sport any dport eq 3389
                    # - proto tcp sport any dport eq 43215
                    # - proto tcp sport any dport eq 43315
                    # - proto icmp any any
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "service":
                        #service group ID 정보 저장
                        service_group_id = line_info_split[2]
                        
                    elif line_info_split[0] == "proto":
                        if line_info_split[1] == "tcp" or line_info_split[1] == "udp" or line_info_split[1] == "any":
                            porotocol = self.scan_description_check("proto", line_info_split, "default")
                            source_port = self.scan_description_check("sport", line_info_split, "default")
                            destination_port = self.scan_description_check("dport", line_info_split, '"default')
                            
                            #프로파일 하나에 대한  Service Group 정보 저장
                            temp_service_group_list.append({"protocol":porotocol, "source_port":source_port, "destination_port":destination_port})
                        
                        else:
                            case_msg = "정의되지 않은 프로토콜 정보(%s)" % (line_info_split[1])
                            log.info("%s\n%s" % (case_msg, self.print_list(service_group_info_row)))
                            # log.info("정의되지 않은 프로토콜 정보가 포함되어 있습니다.\n%s" % (self.print_list(service_group_info_row)))
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("SG", "", "P", "C", case_msg, self.print_list(service_group_info_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                
                #Service Group 정보 저장
                self.main_data.temporary_service_group[service_group_id] = {}
                self.main_data.temporary_service_group[service_group_id]["data"] = temp_service_group_list
                self.main_data.temporary_service_group[service_group_id]["original_data"] = self.print_list(service_group_info_row)
                
                e2e_service_group[service_group_id] = {}
                e2e_service_group[service_group_id]["data"] = temp_service_group_list
                e2e_service_group[service_group_id]["original_data"] = [self.print_list(service_group_info_row)]
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_service_group", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "Server Group 생성 실패 (%s)" % (str(ex))
        finally:
            #E2E Sub Trace 결과
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_response_status(fail_msg)
            detail_msg = str(e2e_service_group).replace("}],", "}],\n%s\n" % (config.SyncConfig.sub_split)).replace("]},", "]},\n%s\n\n" % (config.SyncConfig.line_split))
            e2e_msg_sub.set_message_detail(detail_msg if fail_msg == "" else fail_msg)
            #e2e_msg_sub.set_message_detail(self.main_data.temporary_service_group if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
            
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("Server Group 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_service_group if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - IP Group / IP Group-Set 정보를 파싱한다.(
    """
    def scan_ip_group(self):
        fail_msg = ""
        e2e_ip_group = {}
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("IP Group / IP Group-Set 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))           
            
            #E2E Sub Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_message("IP Group / IP Group-Set 정보 파싱")
            e2e_msg_sub.set_indent(2)
            
            #ip_group 정보 추출.
            for ip_group_row in self.main_data.rule["ip_group"]:
                ip_group_id = ""   #ip group ID 관리 변수
                address = ""
                
                for line_info in ip_group_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - security zone zone123_trust ip group techandlaw_public
                    # - address xxx.xxx.xxx.xxx/x
                    # - address xxx.xxx.xxx.xxx
                    # - address range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "security":
                        #IP Group ID 정보 저장
                        ip_group_id = line_info_split[5]
                        
                    elif line_info_split[0] == "address":
                        if address == "":
                            address = "%s" % (self.scan_description_check("address", line_info_split, "ip"))
                        else:
                            address = "%s&%s" % (address, self.scan_description_check("address", line_info_split, "ip"))
                       
                #IP Group 정보 저장
                self.main_data.temporary_ip_group[ip_group_id] = {}
                self.main_data.temporary_ip_group[ip_group_id]["data"] = address
                self.main_data.temporary_ip_group[ip_group_id]["original_data"] = self.print_list(ip_group_row)
                
                e2e_ip_group[ip_group_id] = {}
                e2e_ip_group[ip_group_id]["data"] = address
                e2e_ip_group[ip_group_id]["original_data"] = self.print_list(ip_group_row)
                
            #IP Group Set 정보 추출.
            for ip_group_set_row in self.main_data.rule["ip_group_set"]:
                ip_group_set_id = ""   #ip group set ID 관리 변수
                address_set = ""
                
                for line_info in ip_group_set_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                    # - group test_ip_group_1
                    # - group test_ip_group_2
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "security":
                        #IP Group setID 정보 저장
                        ip_group_set_id = line_info_split[5]
                        
                    elif line_info_split[0] == "group":
                        #프로파일 하나에 대한  IP Group Set 정보 저장
                        if address_set == "":
                            address_set = "%s" % (self.main_data.temporary_ip_group.get(line_info_split[1])["data"])
                        else:
                            address_set = "%s&%s" % (address_set, self.main_data.temporary_ip_group.get(line_info_split[1])["data"])
                        
                #IP Group Set정보 저장
                self.main_data.temporary_ip_group[ip_group_set_id] = {}
                self.main_data.temporary_ip_group[ip_group_set_id]["data"] = address_set
                self.main_data.temporary_ip_group[ip_group_set_id]["original_data"] = [self.print_list(ip_group_set_row)]
                
                e2e_ip_group[ip_group_set_id] = {}
                e2e_ip_group[ip_group_set_id]["data"] = address_set
                e2e_ip_group[ip_group_set_id]["original_data"] = self.print_list(ip_group_set_row)
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_ip_group", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "IP Group / IP Group-Set 정보 파싱 (%s)" % (str(ex))
        finally:
            #E2E Sub Trace 생성    
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_response_status(fail_msg)
            detail_msg = str(e2e_ip_group).replace("'original_data':", "%s\n'original_data':" % (config.SyncConfig.sub_split)).replace("},", "},\n%s\n\n" % (config.SyncConfig.line_split))
            e2e_msg_sub.set_message_detail(detail_msg if fail_msg == "" else fail_msg)
            #e2e_msg_sub.set_message_detail(self.main_data.temporary_ip_group if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
            
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("IP Group / IP Group-Set 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_ip_group if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - DHCP 정보를 파싱한다.
    """
    def dhcp_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("DHCP 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #dhcp 정보 추출.
            for dhcp_pool_row in self.main_data.rule["dhcp"]:
                dhcp_id = ""
                eth_id = ""
                
                for line_info in dhcp_pool_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip dhcp multi-server 1 lease-check icmp eth3.111 pool dhcp111
                    # - ip dhcp multi-server 2 lease-check icmp eth3.31 pool dhcp31
                    # - ip dhcp multi-server 3 lease-check icmp eth3.41 pool dhcp41
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        #DHCP ID 정보 저장
                        dhcp_id = line_info_split[8]
                        
                        #VLAN 정보 저장
                        eth_id = line_info_split[6]
                        
                        dhcp_pool_info  = self.main_data.temporary_dhcp_pool.get(dhcp_id)
                        
                        if dhcp_pool_info == None:
                            case_msg = "DHCP Pool 정보 없음(%s)" % (dhcp_id)
                            log.info("%s\n%s" % (case_msg, self.print_list(dhcp_pool_row)))
                            #log.info("DHCP Pool 정보 없음 \n%s" % (line_info))
                            
                            self.main_data.set_event_msg_update("DH", "", "C", "S", case_msg, self.print_list(dhcp_pool_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                        
                        break_point = False
                        
                        for zone_row in self.main_data.zone:
                            vlan_point = 1
                            
                            while vlan_point < len(self.main_data.zone[zone_row]["network"]["interface"]):
                                if self.main_data.zone[zone_row]["network"]["interface"][vlan_point]["vlan"] == eth_id:
                                    #br0 : GREEN, br1 : ORANGE, br2 : BLUE
                                    if eth_id.endswith("0") == True:
                                        dhcp_pool_info["dhcp"] = dhcp_pool_info["dhcp"].replace("UNKNOWN", "ORANGE")
                                    elif eth_id.endswith("1") == True:
                                        dhcp_pool_info["dhcp"] = dhcp_pool_info["dhcp"].replace("UNKNOWN", "GREEN")
                                    else:
                                        dhcp_pool_info["dhcp"] = dhcp_pool_info["dhcp"].replace("UNKNOWN", "BLUE")
                                    
                                    self.main_data.zone[zone_row]["dhcp"].append(dhcp_pool_info["dhcp"])
                                    
                                    if len(self.main_data.zone[zone_row]["dhcp_original_data"]) == 0:
                                        self.main_data.zone[zone_row]["dhcp_original_data"] = dhcp_pool_info["original_data"]
                                        
                                    for router_row in dhcp_pool_info["router"]:
                                        self.main_data.zone[zone_row]["router"]["secondary_router"].append(router_row)
                                    
                                    break_point = True
                                
                                if break_point == True:
                                    break
                                
                                vlan_point = vlan_point + 1
                                
                            if break_point == True:
                                break
                            
            #E2E Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            e2e_msg_sub.set_parsing_type("1")   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER

            for zone_row in self.main_data.zone:
                dhcp_cnt    = len(self.main_data.zone[zone_row]["dhcp"])
                dhcp_data   = self.print_list(self.main_data.zone[zone_row]["dhcp"])
                
                #룰 정보 없어도 만든다. (정현호 :2015.05.27)
                #if dhcp_data == "":
                #    continue
                
                e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                e2e_msg_sub.set_message("DHCP 정보 파싱(%s)[%s]" % (zone_row, dhcp_cnt))
                e2e_msg_sub.set_parsing_cnt(dhcp_cnt)   # 파싱 개수
                e2e_msg_sub.set_message_detail(dhcp_data)
                #e2e_msg_sub.set_rule_file_name(json.dumps({"original_data":self.main_data.zone[zone_row]["dhcp_original_data"],"parsing_data":dhcp_data, "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["dhcp"]["name"])}))
                e2e_msg_sub.set_rule_file_name(json.dumps({"original_data":self.main_data.zone[zone_row]["dhcp_original_data"],
                                                           "parsing_data":dhcp_data, 
                                                           "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                    fail_msg = "DHCP 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
                #룰 정보 저장
                if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["dhcp"]["name"]), dhcp_data, "forced") == False:
                    fail_msg = "DHCP 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("dhcp_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "DHCP 정보 파싱 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("DHCP 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - DHCP Pool 정보를 파싱한다.(추후 DHCP 적용에 필요한 정보)
    """
    def scan_dhcp_pool(self):
        fail_msg = ""
        e2e_dhcp_pool = {}
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("DHCP Pool 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))           
            
            #E2E Sub Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_message("DHCP Pool 정보 파싱")
            e2e_msg_sub.set_indent(2)
            
            #dhcp pool 정보 추출.
            for dhcp_pool_row in self.main_data.rule["dhcp_pool"]:
                dhcp_id = ""   #service group ID 관리 변수
                
                #DHCP 기본 정보값 초기화.
                enable = "ENABLE_UNKNOWN=on"
                gateway = ""
                rowcolor = "rowcolor=odd|even|odd"
                max_lease_timr = "MAX_LEASE_TIME_UNKNOWN=720"
                start_addr = ""
                end_addr = ""
                dns = ""
                router = []
                default_router_check = False

                for line_info in dhcp_pool_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip dhcp multi-server pool dhcp_pool_jhh
                    # - network xxx.xxx.xxx.x xxx.xxx.xxx.xxx    -> 네트워크 주소
                    # - range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx    -> DHCP IP 범위 (Multi-range 가능)
                    # - range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx -> DHCP IP 범위 (Multi-range 가능)
                    # - dns-server xxx.xxx.xxx.xxx 1    -> DNS Server (3개까지 설정 가능 Endian은 2개까지 설정 가능)
                    # - dns-server xxx.xxx.xxx.xxx 2    -> DNS Server (3개까지 설정 가능 Endian은 2개까지 설정 가능)
                    # - dns-server xxx.xxx.xxx.xxx 3    -> DNS Server (3개까지 설정 가능 Endian은 2개까지 설정 가능)
                    # - static-routes xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx    -> [Destination IP] [Router IP]
                    # - static-routes xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx    -> [Destination IP] [Router IP]
                    # - classless-routes xxx.xxx.xxx.xxx/x xxx.xxx.xxx.xxx    -> [Network Destination] [Router IP] Classless Routes 설정에서 Router IP는 network 주소 대역 내에 있어야 함
                    # - classless-routes xxx.xxx.xxx.xxx/x xxx.xxx.xxx.xxx    -> [Network Destination] [Router IP] Classless Routes 설정에서 Router IP는 network 주소 대역 내에 있어야 함
                    # - default-router xxx.xxx.xxx.xxx    -> 기본 게이트웨이
                    # - lease 1 0 0 (lease infinite)    -> Lease Time (duration(일-시간-분)/infinite로 설정 가능
                    # - domain-name domain.com
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        #DHCP POOL ID 정보 저장
                        dhcp_id = line_info_split[4]
                        
                    elif line_info_split[0] == "range":
                        if start_addr == "":
                            start_addr = "START_ADDR_UNKNOWN=%s" % (line_info_split[1])
                            end_addr = "END_ADDR_UNKNOWN=%s" % (line_info_split[2])
                        else:
                            case_msg = "한개 이상의 DHCP Range 정보 확인(%s)" % (line_info_split[0])
                            log.info("%s\n%s" % (case_msg, self.print_list(dhcp_pool_row)))
                            #log.info("한개 이상의 DHCP Range 정보가 있습니다.\n%s" % dhcp_pool_row)
                            
                            # 미지원 기능 잠금 (인수시험을 위한 2015.06.02 정현호)
                            self.main_data.set_event_msg_update("DH", "", "P", "C", case_msg, self.print_list(dhcp_pool_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    elif line_info_split[0] == "dns-server":
                        if dns == "":
                            dns = "DNS%s_UNKNOWN=%s" % (line_info_split[2], line_info_split[1])
                        else:
                            dns = "%s\nDNS%s_UNKNOWN=%s" % (dns, line_info_split[2], line_info_split[1])
                    
                    elif line_info_split[0] == "default-router":
                        gateway = "GATEWAY_UNKNOWN=%s" % (line_info_split[1])
                        default_router_check = True
                        
                    elif line_info_split[0] == "static-routes" or line_info_split[0] == "classless-routes" :
                        router.append("on,,%s,%s,%s,,,,,,,"  % (line_info_split[1], line_info_split[2], line_info_split[0]))
                
                #default gateway 가 없는 경우는 이벤트 로그로 빼고 파싱하지 않는다.(정현호 : 2015.05.12)
                if default_router_check == False :
                    case_msg = "default route 정보 없음"
                    log.info("%s\n%s" % (case_msg, self.print_list(dhcp_pool_row)))
                    #log.info("default route 정보가 존재하지 않습니다.\n%s" % dhcp_pool_row)
                    
                    self.main_data.set_event_msg_update("DH", "", "C", "S", case_msg, self.print_list(dhcp_pool_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #DHCP 정보 취합.
                dhcp = "%s\n%s\n%s\n%s\n%s\n%s\n%s" % (enable, gateway, rowcolor, max_lease_timr, start_addr, end_addr, dns)
    
                #DHCP Pool 정보 저장
                self.main_data.temporary_dhcp_pool[dhcp_id] = {"dhcp":dhcp, "router":router, "original_data":self.print_list(dhcp_pool_row)}
                e2e_dhcp_pool[dhcp_id] = {"dhcp":dhcp, "router":router, "original_data":self.print_list(dhcp_pool_row)}
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_dhcp_pool", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "DHCP Pool 정보 파싱 (%s)" % (str(ex))
            
        finally:
            #E2E Sub Trace 생성    
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_response_status(fail_msg)
            detail_msg = str(e2e_dhcp_pool).replace("'original_data':", "%s\n'original_data':" % (config.SyncConfig.sub_split)).replace("]},", "]},\n%s\n\n" % (config.SyncConfig.line_split))
            e2e_msg_sub.set_message_detail(detail_msg if fail_msg == "" else fail_msg)
            #e2e_msg_sub.set_message_detail(self.main_data.temporary_dhcp_pool if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
            
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("DHCP Pool 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_dhcp_pool if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - DNAT 정보를 파싱한다.(
    """
    def dnat_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("DNAT 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #기본 DNAT정보 정의.
            dnat_default_data = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                "Acction",
                                "Protocol",
                                "UPLINK:ANY(AccessFrom)",
                                "AccessFrom(Network/IP/Range)",
                                "UPLINK:ANY(incomingIP)",
                                "incomingIP(Network/IP/Range)",
                                "incomingIP(Port/Range)",
                                "InsertIP(IP)",
                                "InsertPort(Port/Range)",
                                "DNAT",
                                "Remark",
                                "",
                                "FilterPolicy")
            
            #dnat 정보 추출.
            for dnat_row in self.main_data.rule["security_policy"].get("dnat"):
                dnat_profile_id = ""
                security_zone_source = ""
                security_zone_destination = ""
                source_port = ""
                destination_port = ""
                protocol = ""
                filter_policy = ""
                enable_check = "off"
                destination_list = ""
                source_list = ""
                dnat_id = ""
                zone_check = True
                
                temp_original_data = {"source_group_set"        : [],
                                      "source_group"            : [],
                                      "service_group"           : [],
                                      "dnat_profile"            : []}
                
                
                for line_info in dnat_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip security policy from jhhtest_untrust to jhhtest 10 id 189
                    # - label test by jhh
                    # - source xxx.xxx.xxx.xxx
                    # - source xxx.xxx.xxx.xxx/x
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - source group test_ip_group_1
                    # - source group test_ip_group_2
                    # - source group-set TEST_GROUP-SET
                    # - destination dnat-profile 999
                    # - service proto igmp
                    # - service proto tcp sport multi 21 25 dport range 555 559
                    # - service proto 66
                    # - service-group window_terminal
                    # - time-group time_group_name_test
                    # - snat-profile 999
                    # - action pass log
                    # - enable
                    #
                    # - source / destination 이 any 인경우에는 source / destination 각각 1개 이상 선언될 수 없다.(dnat-profile 이 선언된 경우 destination 은 1개이상 선언될 수 없음.) 
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
            
                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        security_zone_source = line_info_split[4]
                        security_zone_destination = line_info_split[6]
                        
                        # id를 룰 변환의 Remark값에 넣어주자(정현호:2015.05.28)
                        if line_info_split[8] == "id" :
                            dnat_id = line_info_split[9]
                        
                        # source zone 체크
                        if self.main_data.delegate_zone.get(security_zone_source) == None:
                            case_msg = "security_zone_source(domain) 정보 없음(ip, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                            #log.info("security_zone_source(domain) 정보가 없습니다. - DNAT(%s)\n%s" % (security_zone_source, dnat_row))
                            
                            self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_source)) == None:
                                case_msg = "security_zone_source(대표존) 정보 없음(ip, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                #log.info("security_zone_source(대표존) 정보가 없습니다. - DNAT(%s)\n%s" %  (security_zone_source, dnat_row))
                                
                                self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_source) == None:
                                    case_msg = "security_zone_source(interface) 정보 없음(ip, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                    #log.info("security_zone_source(interface) 정보가 없습니다. - DNAT(%s)\n%s" %  (security_zone_source, dnat_row))
                                    
                                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                        
                        # destination zone 체크        
                        if self.main_data.delegate_zone.get(security_zone_destination) == None:
                            case_msg = "security_zone_destination(domain) 정보 없음(ip, %s)" % (security_zone_destination)
                            log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                            #log.info("security_zone_destination(domain) 정보가 없습니다. - DNAT(%s)\n%s" %  (security_zone_destination, dnat_row))
                            
                            self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_destination)) == None:
                                case_msg = "security_zone_destination(대표존) 정보 없음(ip, %s)" % (security_zone_destination)
                                log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                #log.info("security_zone_destination(대표존) 정보가 없습니다. - DNAT(%s)\n%s" %  (security_zone_destination, dnat_row))
                                
                                self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_destination) == None:
                                    case_msg = "security_zone_destination(interface) 정보 없음(ip, %s)" % (security_zone_destination)
                                    log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                    #log.info("security_zone_destination(interface) 정보가 없습니다. - DNAT(%s)\n%s" %  (security_zone_destination, dnat_row))
                                    
                                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                                
                    elif line_info_split[0] == "source":
                        
                        # source 원본을 보여 주기 위함
                        if line_info_split[1] == "group" :
                            temp_original_data["source_group"].append(line_info_split[2])
                        elif line_info_split[1] == "group-set" :
                            temp_original_data["source_group_set"].append(line_info_split[2])
                        
                        
                        if source_list == "":
                            source_list = "%s" % (self.scan_description_check("source", line_info_split, "ip"))
                        else:
                            source_list = "%s&%s" % (source_list, self.scan_description_check("source", line_info_split, "ip"))
                            
                        #source 이 any인 경우 더이상의 source 가 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if source_list == "any":
                            temp_source_list = ""
                            
                            for source_row in self.main_data.temporary_vlan[security_zone_source]["ip"]:
                                if temp_source_list == "":
                                    temp_source_list = "%s" % (source_row)
                                else:
                                    temp_source_list = "%s&%s" % (temp_source_list, source_row)
                                    
                            source_list = temp_source_list
                            
                    elif line_info_split[0] == "destination":
                        #destination에 dnat를 적용시키면 destination은 1개밖에 설정할 수 없다.
                        #현재 로직은 DNAT 에 대한 정보밖에 없기 때문에 destination 에대한 다른 값은 확인 하지 않고 dnat-profile에 대한 ID값만 획득 한다.
                        dnat_profile_id = line_info_split[2]
                        temp_original_data["dnat_profile"].append(dnat_profile_id)
                        
                        #destination_list 기본 값은 security policy 선언되어 있던 IP 정보를 사용한다.(추후 프로파일에서 정의된 정보가 있을 경우 프로파일 정보로 대체됨)
                        temp_destination_list = ""
                        
                        for destination_row in self.main_data.temporary_vlan[security_zone_destination]["ip"]:
                            if temp_destination_list == "":
                                temp_destination_list = "%s" % (destination_row)
                                
                            else:
                                temp_destination_list = "%s&%s" % (temp_destination_list,destination_row)
                                    
                        destination_list = temp_destination_list

                    elif line_info_split[0].lower() == "action":
                        #Action에 따른 설정 값을 적용한다.
                        if line_info_split[1].lower() == "pass":
                            filter_policy = "ACCEPT"
                        elif line_info_split[1].lower() == "drop":
                            filter_policy = "DROP"
                        elif line_info_split[1].lower() == "reject":
                            filter_policy = "REJECT"
                    
                    elif line_info_split[0] == "enable":
                        enable_check = "on"
                
                #source_list 가 RED Zone의 IP정보와 같다면 UPLINK:ANY 으로 변경해야 한다.
                if self.main_data.delegate_zone.get(security_zone_source) != None and zone_check == True:
                    if len(self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0]["ip"]) == 0:
                        case_msg = "RED VLAN(source) 정보 없음(%s)" % (security_zone_source)
                        log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                        #log.info("RED VLAN 정보 없음\n%s" % (self.print_list(dnat_row)))
                        
                        self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
                    else:
                        for ip_info in self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0]["ip"]:
                            if source_list == ip_info:
                                source_list = "UPLINK:ANY"
                                
                else:
                    case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                    log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                    #log.info("security zone source 정보 없음\n%s" % (self.print_list(dnat_row))) 
                    
                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #destination_list 가 RED Zone의 IP정보와 같다면 UPLINK:ANY 으로 변경해야 한다.
                if self.main_data.delegate_zone.get(security_zone_destination) != None and zone_check == True:
                    if len(self.main_data.zone[self.main_data.delegate_zone.get(security_zone_destination)]["network"]["interface"][0]["ip"]) == 0:
                        case_msg = "RED VLAN(destination) 정보 없음(%s)" % (security_zone_destination)
                        log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                        #log.info("RED VLAN 정보 없음\n%s" % (self.print_list(dnat_row))) 
                        
                        self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
                    else:
                        for ip_info in self.main_data.zone[self.main_data.delegate_zone.get(security_zone_destination)]["network"]["interface"][0]["ip"]:
                            if destination_list == ip_info:
                                destination_list = "UPLINK:ANY"
                                
                else:
                    case_msg = "security_zone_destination 정보 없음(%s)" % (security_zone_destination)
                    log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                    #log.info("security zone destination 정보 없음\n%s" % (self.print_list(dnat_row))) 
                    
                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                # From Zone VD 와 To Zone VD 가 다른경우 UTM에서 지원되지 않으므로 Event처리(정현호:2015.05.28)
                if self.main_data.delegate_zone[security_zone_source] != self.main_data.delegate_zone[security_zone_destination] :
                    case_msg = "From / To Domain 정보 다름(From[%s], To[%s])" % (self.main_data.delegate_zone[security_zone_source], self.main_data.delegate_zone[security_zone_destination])
                    log.debug("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                    
                    # 미지원 기능 잠금 (2015.06.02 정현호)
                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "P", "S", case_msg, self.print_list(dnat_row))
                    #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
                temp_dnat_data = ""
                
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("=======================================================")
                                        
                #여러 라인이 선언될 수있는 경우에 대한 처리
                for line_info in dnat_row:
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "service":
                        if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                            source_port = self.scan_description_check("sport", line_info_split, "port")
                            destination_port = self.scan_description_check("dport", line_info_split, "port")
                            protocol = line_info_split[2]
                            
                            if self.main_data.temporary_dnat.get(dnat_profile_id) != None:
                                temp_dnat = []
                                
                                for dnat_profile_row in self.main_data.temporary_dnat.get(dnat_profile_id)["data"]:
                                    temp_dnat_data = dnat_default_data
                                    
                                    #프로파일 정보 가져오기
                                    profile_source_list = dnat_profile_row.get("access_from")
                                    profile_destination_list = dnat_profile_row.get("incoming_ip")
                                    profile_insert_port = dnat_profile_row.get("insert_port")
                                    profile_destination_port = dnat_profile_row.get("destination_port")
                                    profile_protocol = dnat_profile_row.get("protocol")
                                    
                                    if dnat_profile_row.get("insert_ip") == "RETURN":
                                        temp_dnat_data = temp_dnat_data.replace("DNAT", "RETURN")
                                            
                                    if profile_source_list == "any":
                                        profile_source_list = source_list
                                    
                                    if profile_source_list == "UPLINK:ANY":
                                        profile_source_list = ""
                                        temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "UPLINK:ANY")
                                            
                                    else:
                                        temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "")
                                
                                    if profile_destination_list == "any":
                                        profile_destination_list = destination_list
                                    
                                    if profile_destination_list == "UPLINK:ANY":
                                        profile_destination_list = ""
                                        temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "UPLINK:ANY")
                                            
                                    else:
                                        temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "")
                                            
                                    if profile_insert_port == "any" or profile_insert_port == "":
                                        profile_insert_port = source_port
                                        
                                    if profile_destination_port == "any" or profile_destination_port == "":
                                        profile_destination_port = destination_port
                                    
                                    if profile_protocol == "any":
                                        profile_protocol = protocol
                                    
                                    temp_dnat_data = temp_dnat_data.replace("Acction", enable_check)
                                    temp_dnat_data = temp_dnat_data.replace("AccessFrom(Network/IP/Range)", profile_source_list)
                                    temp_dnat_data = temp_dnat_data.replace("incomingIP(Network/IP/Range)", profile_destination_list)
                                    temp_dnat_data = temp_dnat_data.replace("InsertPort(Port/Range)", profile_insert_port)
                                    temp_dnat_data = temp_dnat_data.replace("incomingIP(Port/Range)", profile_destination_port)
                                    temp_dnat_data = temp_dnat_data.replace("Protocol", profile_protocol)
                                    temp_dnat_data = temp_dnat_data.replace("InsertIP(IP)", dnat_profile_row.get("insert_ip"))
                                    temp_dnat_data = temp_dnat_data.replace("Remark", dnat_id)
                                    temp_dnat_data = temp_dnat_data.replace("FilterPolicy", filter_policy)
                                    
                                    temp_dnat.append(temp_dnat_data)
                                    
                                if self.main_data.delegate_zone.get(security_zone_source) != None:
                                    for temp_dnat_row in temp_dnat:
                                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat"].append(temp_dnat_row.replace("any", ""))

                                else:
                                    case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (dnat_row)) 
                                    
                                    self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    continue
                                    
                        else:
                            case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                            log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                            #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (dnat_row)) 
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "P", "S", case_msg, self.print_list(dnat_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                            
                    elif line_info_split[0] == "service-group":
                        if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                            
                            append_original_data = False 
                            for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                                source_port = service_group_row.get("source_port")
                                destination_port = service_group_row.get("destination_port")
                                protocol = service_group_row.get("protocol")
                                
                                if self.main_data.temporary_dnat.get(dnat_profile_id) != None:
                                    temp_dnat = []
                                    
                                    for dnat_profile_row in self.main_data.temporary_dnat.get(dnat_profile_id)["data"]:
                                        temp_dnat_data = dnat_default_data
                                        
                                        #프로파일 정보 가져오기
                                        profile_source_list = dnat_profile_row.get("access_from")
                                        profile_destination_list = dnat_profile_row.get("incoming_ip")
                                        profile_insert_port = dnat_profile_row.get("insert_port")
                                        profile_destination_port = dnat_profile_row.get("destination_port")
                                        profile_protocol = dnat_profile_row.get("protocol")
                                        
                                        if dnat_profile_row.get("insert_ip") == "RETURN":
                                            temp_dnat_data = temp_dnat_data.replace("DNAT", "RETURN" )
                                            
                                        if profile_source_list == "any":
                                            profile_source_list = source_list
                                        
                                        if profile_source_list == "UPLINK:ANY":
                                            profile_source_list = ""
                                            temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "UPLINK:ANY")
                                                
                                        else:
                                            temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "")
                                
                                        if profile_destination_list == "any":
                                            profile_destination_list = destination_list
                                        
                                        if profile_destination_list == "UPLINK:ANY":
                                            profile_destination_list = ""
                                            temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "UPLINK:ANY")
                                                
                                        else:
                                            temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "")
                                                
                                        if profile_insert_port == "any" or profile_insert_port == "":
                                            profile_insert_port = source_port
                                            
                                        if profile_destination_port == "any" or profile_destination_port == "":
                                            profile_destination_port = destination_port
                                        
                                        if profile_protocol == "any":
                                            profile_protocol = protocol
                                        
                                        temp_dnat_data = temp_dnat_data.replace("Acction", enable_check)
                                        temp_dnat_data = temp_dnat_data.replace("AccessFrom(Network/IP/Range)", profile_source_list)
                                        temp_dnat_data = temp_dnat_data.replace("incomingIP(Network/IP/Range)", profile_destination_list)
                                        temp_dnat_data = temp_dnat_data.replace("InsertPort(Port/Range)", profile_insert_port)
                                        temp_dnat_data = temp_dnat_data.replace("incomingIP(Port/Range)", profile_destination_port)
                                        temp_dnat_data = temp_dnat_data.replace("Protocol", profile_protocol)
                                        temp_dnat_data = temp_dnat_data.replace("InsertIP(IP)", dnat_profile_row.get("insert_ip"))
                                        temp_dnat_data = temp_dnat_data.replace("Remark", dnat_id)
                                        temp_dnat_data = temp_dnat_data.replace("FilterPolicy", filter_policy)
                                        
                                        temp_dnat.append(temp_dnat_data)
                                    # loop dnat_profile_row
                                    
                                    if self.main_data.delegate_zone.get(security_zone_source) != None:
                                        for temp_dnat_row in temp_dnat:
                                            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat"].append(temp_dnat_row.replace("any", ""))
                                        
                                        # service-group original_data는 1번만 Append 하자
                                        if append_original_data == False :
                                            append_original_data = True
                                            temp_original_data["service_group"].append(line_info_split[1])
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.main_data.temporary_service_group[line_info_split[1]]["original_data"])

                                    else:
                                        case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                        log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                                        #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (dnat_row)) 
                                        
                                        self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                        continue
                
                if temp_dnat_data == "":
                    if self.main_data.temporary_dnat.get(dnat_profile_id) != None:
                        temp_dnat = []
                        
                        for dnat_profile_row in self.main_data.temporary_dnat.get(dnat_profile_id)["data"]:
                            temp_dnat_data = dnat_default_data
                            
                            #프로파일 정보 가져오기
                            profile_source_list = dnat_profile_row.get("access_from")
                            profile_destination_list = dnat_profile_row.get("incoming_ip")
                            profile_insert_port = dnat_profile_row.get("insert_port")
                            profile_destination_port = dnat_profile_row.get("destination_port")
                            profile_protocol = dnat_profile_row.get("protocol")
                            
                            if dnat_profile_row.get("insert_ip") == "RETURN":
                                temp_dnat_data = temp_dnat_data.replace("DNAT", "RETURN" )
                                
                            if profile_source_list == "any":
                                profile_source_list = source_list
                            
                            if profile_source_list == "UPLINK:ANY":
                                profile_source_list = ""
                                temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "UPLINK:ANY")
                                    
                            else:
                                temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(AccessFrom)", "")
                                
                            if profile_destination_list == "any":
                                profile_destination_list = destination_list
                            
                            if profile_destination_list == "UPLINK:ANY":
                                profile_destination_list = ""
                                temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "UPLINK:ANY")
                                    
                            else:
                                temp_dnat_data = temp_dnat_data.replace("UPLINK:ANY(incomingIP)", "")
                                            
                            if profile_insert_port == "any" or profile_insert_port == "":
                                profile_insert_port = source_port
                                
                            if profile_destination_port == "any" or profile_destination_port == "":
                                profile_destination_port = destination_port
                            
                            if profile_protocol == "any":
                                profile_protocol = protocol
                            
                            temp_dnat_data = temp_dnat_data.replace("Acction", enable_check)
                            temp_dnat_data = temp_dnat_data.replace("AccessFrom(Network/IP/Range)", profile_source_list)
                            temp_dnat_data = temp_dnat_data.replace("incomingIP(Network/IP/Range)", profile_destination_list)
                            temp_dnat_data = temp_dnat_data.replace("InsertPort(Port/Range)", profile_insert_port)
                            temp_dnat_data = temp_dnat_data.replace("incomingIP(Port/Range)", profile_destination_port)
                            temp_dnat_data = temp_dnat_data.replace("Protocol", profile_protocol)
                            temp_dnat_data = temp_dnat_data.replace("InsertIP(IP)", dnat_profile_row.get("insert_ip"))
                            temp_dnat_data = temp_dnat_data.replace("Remark", dnat_id)
                            temp_dnat_data = temp_dnat_data.replace("FilterPolicy", filter_policy)
                            
                            temp_dnat.append(temp_dnat_data)
                            
                        if self.main_data.delegate_zone.get(security_zone_source) != None:
                            for temp_dnat_row in temp_dnat:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat"].append(temp_dnat_row.replace("any", ""))

                        else:
                            case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(dnat_row)))
                            #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (dnat_row)) 
                            
                            self.main_data.set_event_msg_update("DN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(dnat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                
                # make orignal_data 
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.print_list(dnat_row))
                
                #source_group_set : ip group set
                for source_group_set_row in temp_original_data["source_group_set"] :
                    if self.main_data.temporary_ip_group.get(source_group_set_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                        ip_group_set_row = self.main_data.temporary_ip_group[source_group_set_row]["original_data"]
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(ip_group_set_row)
                        
                        # ip Group Find
                        for line_info in ip_group_set_row:
                            ######################################################
                            #######################파싱 예제########################
                            # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                            # - group test_ip_group_1
                            # - group test_ip_group_2
                            ######################################################
                            line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
        
                            #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                            if line_info_split[0] == "group":
                                source_group_row1 = line_info_split[1]
                                
                                if self.main_data.temporary_ip_group.get(source_group_row1) != None :
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.main_data.temporary_ip_group[source_group_row1]["original_data"])
                        # end loop ip group
                # ens loopip group set
                        
                #source_group : ip group
                for source_group_row2 in temp_original_data["source_group"] :
                    if self.main_data.temporary_ip_group.get(source_group_row2) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.main_data.temporary_ip_group[source_group_row2]["original_data"])
                # end loop ip group
                
                #service_group : service group
                for service_group_row in temp_original_data["service_group"] :
                    if self.main_data.temporary_service_group.get(service_group_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.main_data.temporary_service_group[service_group_row]["original_data"])
                # end loop service group
                
                #dnat_profile
                if self.main_data.temporary_dnat.get(dnat_profile_id) != None:
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append("")
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["dnat_original_data"].append(self.main_data.temporary_dnat.get(dnat_profile_id)["original_data"])
                               
            #E2E Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            e2e_msg_sub.set_parsing_type("2")   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER

            for zone_row in self.main_data.zone:
                dnat_cnt  = len(self.main_data.zone[zone_row]["dnat"])
                dnat_data = self.print_list(self.main_data.zone[zone_row]["dnat"])

                #룰 정보 없어도 만든다. (정현호 :2015.05.27)
                #if dnat_data == "":
                #    continue
                
                dnat_original_data = self.print_list(self.main_data.zone[zone_row]["dnat_original_data"])
                
                e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                e2e_msg_sub.set_message("DNAT 정보 파싱(%s)[%d]" % (zone_row, dnat_cnt))
                e2e_msg_sub.set_message_detail(dnat_data)
                e2e_msg_sub.set_parsing_cnt(dnat_cnt)         # 파싱 개수
                #e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":dnat_data, "original_data":dnat_original_data, "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["dnat"]["name"])}))
                e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":dnat_data, 
                                                           "original_data":dnat_original_data,
                                                           "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                    fail_msg = "DNAT 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
                #룰 정보 저장
                if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["dnat"]["name"]), dnat_data, "forced") == False:
                    fail_msg = "DNAT 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
            
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("dnat_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "DNAT 파싱 종료 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("DNAT 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg

    """
    @Created - 2015. 05. 26.
    @author - 김도영
    @description - firewall interzone 정보를 파싱한다.
    """
    def fw_interzone_parsing(self, fw_info):
        fail_msg = ""
         
        try:
#             temp_firewall_args = {"type"                        :"",
#                                   "firewall_row"                :firewall_row,
#                                   "default_data"                :"",
#                                   "security_zone_source"        :"",
#                                   "security_zone_destination"   :"",
#                                   "source_list"                 :"",
#                                   "destination_list"            :"",
#                                   "filter_policy"               :"",
#                                   "enable_check"                :""}
#             fw_interzone_default_data   =       "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
#                                                 "Acction",
#                                                 "Protocol",
#                                                 "Source(Network/IP)",
#                                                 "Destination(Network/IP)",
#                                                 "DestinationPort",
#                                                 "ACCEPT/DROP/REJECT",
#                                                 "", #SourceMAC
#                                                 "", #Remark
#                                                 "", #Log
#                                                 "", #Source(Zone/Interface)"
#                                                 "") #Destination(Zone/Interface)"            
             
            firewall_type               = fw_info.get("type")   # fw_interzone
            firewall_original_data      = "%s_original_data" % firewall_type
            firewall_row                = fw_info.get("firewall_row")
            firewall_default_data       = fw_info.get("default_data")
            security_zone_source        = fw_info.get("security_zone_source")
            security_zone_destination   = fw_info.get("security_zone_destination")
            enable_check                = fw_info.get("enable_check")
            source_list                 = fw_info.get("source_list")
            destination_list            = fw_info.get("destination_list")
            filter_policy               = fw_info.get("filter_policy")
             
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("=======================================================")
             
            #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
            temp_firewall_data = ""
             
            #여러 라인이 선언될 수있는 경우에 대한 처리
            for line_info in firewall_row:
                line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                  
                if line_info_split[0] == "service":
                    if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                        temp_firewall_data = firewall_default_data
                        destination_port = self.scan_description_check("dport", line_info_split, "port")
                        protocol = line_info_split[2]
      
                        temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                        temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                        temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                        temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                        temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                        temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                          
                        if self.main_data.delegate_zone.get(security_zone_source) != None:
                            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                              
                        else:
                            case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                              
                            self.main_data.set_event_msg_update("FZ", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                                  
                    else:
                        case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                        log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                        #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (firewall_row)) 
                        
                        # 미지원 기능 잠금 (2015.06.02 정현호)  
                        self.main_data.set_event_msg_update("FZ", self.find_utm_userid(security_zone_source, self.main_data), "P", "C", case_msg, self.print_list(firewall_row))
                        #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                          
                elif line_info_split[0] == "service-group":
                    if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                        for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                            temp_firewall_data = firewall_default_data
                            destination_port = service_group_row.get("destination_port")
                            protocol = service_group_row.get("protocol")
                              
                            temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                            temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                            temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                            temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                            temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                            temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                            if self.main_data.delegate_zone.get(security_zone_source) != None:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(service_group_row))
                                 
                            else:
                                case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                                  
                                self.main_data.set_event_msg_update("FZ", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                continue
         
            if temp_firewall_data == "":
                temp_firewall_data = firewall_default_data
                temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                temp_firewall_data = temp_firewall_data.replace("DestinationPort", "")
                temp_firewall_data = temp_firewall_data.replace("Protocol", "")
                temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                if self.main_data.delegate_zone.get(security_zone_source) != None:
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                      
                else:
                    case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                      
                    self.main_data.set_event_msg_update("FZ", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    fail_msg = case_msg
                    return fail_msg
          
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(firewall_row))
             
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("fw_interzone_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "방화벽[INTERZONE] 파싱 종료 (%s)" % (str(ex))
             
        finally:
            pass
         
        return fail_msg
    
    """
    @Created - 2015. 05. 26.
    @author - 김도영
    @description - firewall outgoing 정보를 파싱한다.
    """
    def fw_outgoing_parsing(self, fw_info):
        fail_msg = ""
         
        try:
#             temp_firewall_args = {"type"                        :"",
#                                   "firewall_row"                :firewall_row,
#                                   "default_data"                :"",
#                                   "security_zone_source"        :"",
#                                   "security_zone_destination"   :"",
#                                   "source_list"                 :"",
#                                   "destination_list"            :"",
#                                   "filter_policy"               :"",
#                                   "enable_check"                :""}
#             fw_outgoing_default_data    =       "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
#                                                 "Acction",
#                                                 "Protocol",
#                                                 "Source(Network/IP)",
#                                                 "Destination(Network/IP)",
#                                                 "DestinationPort",
#                                                 "ACCEPT/DROP/REJECT",
#                                                 "", #SourceMAC
#                                                 "", #Remark
#                                                 "", #Log
#                                                 "", #Source(Zone/Interface)"
#                                                 "", #Destination(Zone/Interface)"
#                                                 "") #의미없음
             
             
            firewall_type               = fw_info.get("type")   # fw_outgoing
            firewall_original_data      = "%s_original_data" % firewall_type
            firewall_row                = fw_info.get("firewall_row")
            firewall_default_data       = fw_info.get("default_data")
            security_zone_source        = fw_info.get("security_zone_source")
            security_zone_destination   = fw_info.get("security_zone_destination")
            enable_check                = fw_info.get("enable_check")
            source_list                 = fw_info.get("source_list")
            destination_list            = fw_info.get("destination_list")
            filter_policy               = fw_info.get("filter_policy")
             
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("=======================================================")
             
            #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
            temp_firewall_data = ""
             
            #여러 라인이 선언될 수있는 경우에 대한 처리
            for line_info in firewall_row:
                line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                  
                if line_info_split[0] == "service":
                    if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                        temp_firewall_data = firewall_default_data
                        destination_port = self.scan_description_check("dport", line_info_split, "port")
                        protocol = line_info_split[2]
      
                        temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                        temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                        temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                        temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                        temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                        temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                          
                        if self.main_data.delegate_zone.get(security_zone_source) != None:
                            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                              
                        else:
                            case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                              
                            self.main_data.set_event_msg_update("FO", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                                  
                    else:
                        case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                        log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                        #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (firewall_row)) 
                        
                        # 미지원 기능 잠금 (2015.06.02 정현호)
                        self.main_data.set_event_msg_update("FO", self.find_utm_userid(security_zone_source, self.main_data), "P", "C", case_msg, self.print_list(firewall_row))
                        #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                          
                elif line_info_split[0] == "service-group":
                    if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                        for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                            temp_firewall_data = firewall_default_data
                            destination_port = service_group_row.get("destination_port")
                            protocol = service_group_row.get("protocol")
                              
                            temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                            temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                            temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                            temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                            temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                            temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                            if self.main_data.delegate_zone.get(security_zone_source) != None:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(service_group_row))
                                 
                            else:
                                case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                                  
                                self.main_data.set_event_msg_update("FO", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                continue
         
            if temp_firewall_data == "":
                temp_firewall_data = firewall_default_data
                temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                temp_firewall_data = temp_firewall_data.replace("DestinationPort", "")
                temp_firewall_data = temp_firewall_data.replace("Protocol", "")
                temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                if self.main_data.delegate_zone.get(security_zone_source) != None:
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                      
                else:
                    case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                      
                    self.main_data.set_event_msg_update("FO", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    fail_msg = case_msg
                    return fail_msg
          
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(firewall_row))
             
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("fw_outgoing_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "방화벽[OUTGOING] 파싱 종료 (%s)" % (str(ex))
             
        finally:
            pass
         
        return fail_msg    

    """
    @Created - 2015. 05. 26.
    @author - 김도영
    @description - firewall incoming 정보를 파싱한다.
    """
    def fw_incoming_parsing(self, fw_info):
        fail_msg = ""
         
        try:
#             temp_firewall_args = {"type"                        :"",
#                                   "firewall_row"                :firewall_row,
#                                   "default_data"                :"",
#                                   "security_zone_source"        :"",
#                                   "security_zone_destination"   :"",
#                                   "source_list"                 :"",
#                                   "destination_list"            :"",
#                                   "filter_policy"               :"",
#                                   "enable_check"                :""}
#             fw_incoming_default_data       =   "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
#                                                 "Acction",
#                                                 "Protocol",
#                                                 "", #Source(Zone/Interface)
#                                                 "Source(Network/IP)",
#                                                 "", #Destination(Zone/Interface)"
#                                                 "Destination(Network/IP)",
#                                                 "DestinationPort",
#                                                 "ACCEPT/DROP/REJECT",
#                                                 "", #Remark
#                                                 "")  #Log
             
             
            firewall_type               = fw_info.get("type")
            firewall_original_data      = "%s_original_data" % firewall_type
            firewall_row                = fw_info.get("firewall_row")
            firewall_default_data       = fw_info.get("default_data")
            security_zone_source        = fw_info.get("security_zone_source")
            security_zone_destination   = fw_info.get("security_zone_destination")
            enable_check                = fw_info.get("enable_check")
            source_list                 = fw_info.get("source_list")
            destination_list            = fw_info.get("destination_list")
            filter_policy               = fw_info.get("filter_policy")
             
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("=======================================================")
             
            #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
            temp_firewall_data = ""
             
            #여러 라인이 선언될 수있는 경우에 대한 처리
            for line_info in firewall_row:
                line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                  
                if line_info_split[0] == "service":
                    if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                        temp_firewall_data = firewall_default_data
                        destination_port = self.scan_description_check("dport", line_info_split, "port")
                        protocol = line_info_split[2]
      
                        temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                        temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                        temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                        temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                        temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                        temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                          
                        if self.main_data.delegate_zone.get(security_zone_source) != None:
                            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                              
                        else:
                            case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                              
                            self.main_data.set_event_msg_update("FI", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                                  
                    else:
                        case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                        log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                        #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (firewall_row)) 
                        
                        # 미지원 기능 잠금 (2015.06.02 정현호)
                        self.main_data.set_event_msg_update("FI", self.find_utm_userid(security_zone_source, self.main_data), "P", "C", case_msg, self.print_list(firewall_row))
                        #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                          
                elif line_info_split[0] == "service-group":
                    if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                        for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                            temp_firewall_data = firewall_default_data
                            destination_port = service_group_row.get("destination_port")
                            protocol = service_group_row.get("protocol")
                              
                            temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                            temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                            temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                            temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                            temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                            temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                            if self.main_data.delegate_zone.get(security_zone_source) != None:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(service_group_row))
                                 
                            else:
                                case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                                  
                                self.main_data.set_event_msg_update("FI", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                continue
         
            if temp_firewall_data == "":
                temp_firewall_data = firewall_default_data
                temp_firewall_data = temp_firewall_data.replace("Acction", enable_check)
                temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                temp_firewall_data = temp_firewall_data.replace("DestinationPort", "")
                temp_firewall_data = temp_firewall_data.replace("Protocol", "")
                temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
      
                if self.main_data.delegate_zone.get(security_zone_source) != None:
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                      
                else:
                    case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                      
                    self.main_data.set_event_msg_update("FI", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    fail_msg = case_msg
                    return fail_msg
          
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append("")
            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_original_data].append(self.print_list(firewall_row))
             
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("fw_incoming_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "방화벽[INTERZONE] 파싱 종료 (%s)" % (str(ex))
             
        finally:
            pass
         
        return fail_msg 
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 방화벽 정보를 파싱한다.
    """
    def firewall_parsing2(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("방화벽 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            default_data = {"fw_interzone"      : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Acction",
                                                "Protocol",
                                                "Source(Network/IP)",
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT",
                                                "", #SourceMAC
                                                "", #Remark
                                                "", #Log
                                                "", #Source(Zone/Interface)"
                                                ""), #Destination(Zone/Interface)"
                            "fw_outgoing"       : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Acction",
                                                "Protocol",
                                                "Source(Network/IP)",
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT",
                                                "", #SourceMAC
                                                "", #Remark
                                                "", #Log
                                                "", #Source(Zone/Interface)"
                                                "", #Destination(Zone/Interface)"
                                                ""), #의미없음
                            "fw_incoming"      : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Acction",
                                                "Protocol",
                                                "", #Source(Zone/Interface)
                                                "Source(Network/IP)",
                                                "", #Destination(Zone/Interface)"
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT",
                                                "", #Remark
                                                "") #Log
                           }
            
            #firewall 정보 추출.
            for firewall_row in self.main_data.rule["security_policy"].get("firewall"):
                security_zone_source = ""
                security_zone_destination = ""
                destination_port = ""
                protocol = ""
                enable_check = "off"
                destination_list = ""
                source_list = ""
                filter_policy = ""
                zone_check = True
                firewall_type = ""
                
                temp_firewall_info = {"type"                        :"",
                                      "firewall_row"                :firewall_row,
                                      "default_data"                :"",
                                      "security_zone_source"        :"",
                                      "security_zone_destination"   :"",
                                      "source_list"                 :"",
                                      "destination_list"            :"",
                                      "filter_policy"               :"",
                                      "enable_check"                :""}
                
#                 #기본 DNAT정보 정의.
#                 firewall_default_data = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
#                                     "Acction",
#                                     "Protocol",
#                                     "Source(Network/IP)",
#                                     "Destination(Network/IP)",
#                                     "DestinationPort",
#                                     "ACCEPT/DROP/REJECT",
#                                     "", #SourceMAC
#                                     "", #Remark
#                                     "", #Log
#                                     "", #Source(Zone/Interface)"
#                                     "") #Destination(Zone/Interface)"
                
                for line_info in firewall_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip security policy from zone121_trust to zone121_trust 11 id 26
                    # - source group zone120_trust_local
                    # - source xxx.xxx.xxx.xxx
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - source xxx.xxx.xxx.xxx/x
                    # - destination group zone120_trust_local
                    # - destination xxx.xxx.xxx.xxx
                    # - destination range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - destination xxx.xxx.xxx.xxx/x
                    # - service proto tcp sport any dport eq 80
                    # - service proto tcp sport any dport mult 443
                    # - service proto tcp sport any dport range 443
                    # - service-group window_terminal
                    # - action pass log -> pass/drop/reject
                    # - enable
                    #
                    # - source / destination 이 any 인경우에는 source / destination 각각 1개 이상 선언될 수 없다.
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
            
                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        security_zone_source = line_info_split[4]
                        security_zone_destination = line_info_split[6]
                        
                        if self.main_data.delegate_zone.get(security_zone_source) == None:
                            case_msg = "security_zone_source(domain) 정보 없음(ip, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("security_zone_source(domain) 정보가 없습니다. - 방화벽(%s)\n%s" % (security_zone_source, firewall_row))
                            
                            self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_source)) == None:
                                case_msg = "security_zone_source(대표존) 정보 없음(ip, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("security_zone_source(대표존) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_source, firewall_row))
                                
                                self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_source) == None:
                                    case_msg = "security_zone_source(interface) 정보 없음(ip, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                    #log.info("security_zone_source(interface) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_source, firewall_row))
                                    
                                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                                
                        if self.main_data.delegate_zone.get(security_zone_destination) == None:
                            case_msg = "security_zone_destination(domain) 정보 없음(ip, %s)" % (security_zone_destination)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("security_zone_destination(domain) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                            
                            self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_destination)) == None:
                                case_msg = "security_zone_destination(대표존) 정보 없음(ip, %s)" % (security_zone_destination)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("security_zone_destination(대표존) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                                
                                self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_destination) == None:
                                    case_msg = "security_zone_destination(interface) 정보 없음(ip, %s)" % (security_zone_destination)
                                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                    #log.info("security_zone_destination(interface) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                                    
                                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                            
                    elif line_info_split[0] == "source":
                        if source_list == "":
                            source_list = "%s" % (self.scan_description_check("source", line_info_split, "ip"))
                        else:
                            source_list = "%s&%s" % (source_list, self.scan_description_check("source", line_info_split, "ip"))
                            
                        #source 이 any인 경우 더이상의 source 가 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if source_list == "any":
                            temp_source_list = ""
                                
                            for source_row in self.main_data.temporary_vlan[security_zone_source]["ip"]:
                                if temp_source_list == "":
                                    temp_source_list = "%s" % (source_row)
                                else:
                                    temp_source_list = "%s&%s" % (temp_source_list, source_row)
                                    
                            source_list = temp_source_list
                            
                    elif line_info_split[0] == "destination":
                        if destination_list == "":
                            destination_list = "%s" % (self.scan_description_check("destination", line_info_split, "ip"))

                        else:
                            destination_list = "%s&%s" % (destination_list, self.scan_description_check("destination", line_info_split, "ip"))
                        
                        #destination 이 any인 경우 더이상의 destination 이 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if destination_list == "any":
                            #destination_list 기본 값은 security policy 선언되어 있던 IP 정보를 사용한다.(추후 프로파일에서 정의된 정보가 있을 경우 프로파일 정보로 대체됨)
                            temp_destination_list = ""
                            
                            for destination_row in self.main_data.temporary_vlan[security_zone_destination]["ip"]:
                                if temp_destination_list == "":
                                    temp_destination_list = "%s" % (destination_row)
                                else:
                                    temp_destination_list = "%s&%s" % (temp_destination_list,destination_row)
                                        
                            destination_list = temp_destination_list

                    elif line_info_split[0].lower() == "action":
                        #Action에 따른 설정 값을 적용한다.
                        if line_info_split[1].lower() == "pass":
                            filter_policy = "ACCEPT"
                            
                        elif line_info_split[1].lower() == "drop":
                            filter_policy = "DROP"
                            
                        elif line_info_split[1].lower() == "reject":
                            filter_policy = "REJECT"
                            
                    elif line_info_split[0] == "enable":
                        enable_check = "on"
                
                if zone_check == False:
                    continue
                
                # incoming, outgoing, interzone 분기 관련
                eth_source_red_ip               = self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0].get("ip")
                security_zone_source_ip         = self.main_data.temporary_vlan.get(security_zone_source).get("ip")
                eth_destination_red_ip          = self.main_data.zone[self.main_data.delegate_zone.get(security_zone_destination)]["network"]["interface"][0].get("ip")
                security_zone_destination_ip    = self.main_data.temporary_vlan.get(security_zone_destination).get("ip")
                
                temp_firewall_info["security_zone_source"]          = security_zone_source
                temp_firewall_info["security_zone_destination"]     = security_zone_destination
                temp_firewall_info["source_list"]                   = source_list
                temp_firewall_info["destination_list"]              = destination_list
                temp_firewall_info["filter_policy"]                 = filter_policy
                temp_firewall_info["enable_check"]                  = enable_check
                
                check_continue = ""
                if eth_source_red_ip == security_zone_source_ip :
                    firewall_type                       = "fw_incoming" # incoming
                    temp_firewall_info["type"]          = firewall_type
                    temp_firewall_info["default_data"]  = default_data[temp_firewall_info.get("type")]
                    
                    check_continue = self.fw_incoming_parsing(temp_firewall_info)
                    
                elif eth_destination_red_ip == security_zone_destination_ip :
                    firewall_type                       = "fw_outgoing" # outgoing
                    temp_firewall_info["type"]          = firewall_type
                    temp_firewall_info["default_data"]  = default_data[temp_firewall_info.get("type")]
                    
                    check_continue = self.fw_outgoing_parsing(temp_firewall_info)
                    
                else :
                    firewall_type                       = "fw_interzone" # interzone
                    temp_firewall_info["type"]          = firewall_type
                    temp_firewall_info["default_data"]  = default_data[temp_firewall_info.get("type")]
                    
                    check_continue = self.fw_interzone_parsing(temp_firewall_info)
                    
                if check_continue != "" :
                    continue
                    
            # End firewall data Loop
                    
            # InterZone E2E Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            e2e_msg_sub.set_parsing_type("4")   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER, 6:FW[INCOMMIN], 7:FW[OUTGOING], 8:FW[INTERZONE]
            
            # InterZone
            for zone_row in self.main_data.zone:
                firewall_data = self.print_list(self.main_data.zone[zone_row]["firewall"])
                if firewall_data == "":
                    continue
                
                e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                e2e_msg_sub.set_message("방화벽 정보 파싱(%s)" % (zone_row))
                e2e_msg_sub.set_message_detail(firewall_data)
                #e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":firewall_data, "original_data":self.print_list(self.main_data.zone[zone_row]["firewall_original_data"]), "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["firewall"]["name"])}))
                e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":firewall_data, 
                                                           "original_data":self.print_list(self.main_data.zone[zone_row]["firewall_original_data"]), 
                                                           "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                    fail_msg = "방화벽 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
                #룰 정보 저장
                if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["firewall"]["name"]), firewall_data, "forced") == False:
                    fail_msg = "방화벽 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("firewall_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "방화벽 파싱 종료 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("방화벽 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg    
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 방화벽 정보를 파싱한다.
    """
    def firewall_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("방화벽 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
#           # 기본 FIREWALL 정보 정의.            
            default_data = {"fw_incoming"      : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Enabled",
                                                "Protocol",
                                                "UPLINK:ANY", #Source(Zone/Uplink)
                                                "Source(Network/IP)",
                                                "", #Destination(Zones)"
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT", #Action
                                                "Remark", #Remark
                                                ""), #Log
                            "fw_outgoing"       : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Enabled",
                                                "Protocol",
                                                "Source(Network/IP)",
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT", #Action
                                                "", #SourceMAC
                                                "Remark", #Remark
                                                "", #Log
                                                "", #Source(Zone/Interface)"
                                                "UPLINK:ANY", #Destination(Uplink)"
                                                ""), #의미없음
                            "fw_interzone"      : "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                                "Enabled",
                                                "Protocol",
                                                "Source(Network/IP)",
                                                "Destination(Network/IP)",
                                                "DestinationPort",
                                                "ACCEPT/DROP/REJECT", #Action
                                                "", #SourceMAC
                                                "Remark", #Remark
                                                "", #Log
                                                "", #Source(Zone/Interface)"
                                                "") #Destination(Zone/Interface)"
                           }
            
            
            
            #firewall 정보 추출.
            for firewall_row in self.main_data.rule["security_policy"].get("firewall"):
                security_zone_source = ""
                security_zone_destination = ""
                destination_port = ""
                protocol = ""
                enable_check = "off"
                destination_list = ""
                source_list = ""
                filter_policy = ""
                firewall_id = ""
                zone_check = True
                firewall_type = "firewall"  # fw_interzone, fw_outgoing, fw_incoming
                source_any_check = False
                destination_any_check = False
                temp_original_data = {"source_group_set"        : [],
                                      "source_group"            : [],
                                      "destination_group_set"   : [],
                                      "destination_group"       : [],
                                      "service_group"           : []}
                
                for line_info in firewall_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip security policy from zone121_trust to zone121_trust 11 id 26
                    # - source group zone120_trust_local
                    # - source xxx.xxx.xxx.xxx
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - source xxx.xxx.xxx.xxx/x
                    # - destination group zone120_trust_local
                    # - destination xxx.xxx.xxx.xxx
                    # - destination range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - destination xxx.xxx.xxx.xxx/x
                    # - service proto tcp sport any dport eq 80
                    # - service proto tcp sport any dport mult 443
                    # - service proto tcp sport any dport range 443
                    # - service-group window_terminal
                    # - action pass log -> pass/drop/reject
                    # - enable
                    #
                    # - source / destination 이 any 인경우에는 source / destination 각각 1개 이상 선언될 수 없다.
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
            
                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        security_zone_source = line_info_split[4]
                        security_zone_destination = line_info_split[6]
                        
                        # id를 룰 변환의 Remark값에 넣어주자(정현호:2015.05.28)
                        if line_info_split[8] == "id" :
                            firewall_id = line_info_split[9]
                        
                        if self.main_data.delegate_zone.get(security_zone_source) == None:
                            case_msg = "security_zone_source(domain) 정보 없음(ip, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("security_zone_source(domain) 정보가 없습니다. - 방화벽(%s)\n%s" % (security_zone_source, firewall_row))
                            
                            self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_source)) == None:
                                case_msg = "security_zone_source(대표존) 정보 없음(ip, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("security_zone_source(대표존) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_source, firewall_row))
                                
                                self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_source) == None:
                                    case_msg = "security_zone_source(interface) 정보 없음(ip, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                    #log.info("security_zone_source(interface) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_source, firewall_row))
                                    
                                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                                
                        if self.main_data.delegate_zone.get(security_zone_destination) == None:
                            case_msg = "security_zone_destination(domain) 정보 없음(ip, %s)" % (security_zone_destination)
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("security_zone_destination(domain) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                            
                            self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_destination)) == None:
                                case_msg = "security_zone_destination(대표존) 정보 없음(ip, %s)" % (security_zone_destination)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("security_zone_destination(대표존) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                                
                                self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_destination) == None:
                                    case_msg = "security_zone_destination(interface) 정보 없음(ip, %s)" % (security_zone_destination)
                                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                    #log.info("security_zone_destination(interface) 정보가 없습니다. - 방화벽(%s)\n%s" %  (security_zone_destination, firewall_row))
                                    
                                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                            
                    elif line_info_split[0] == "source":
                        # source 원본을 보여 주기 위함
                        if line_info_split[1] == "group" :
                            temp_original_data["source_group"].append(line_info_split[2])
                        elif line_info_split[1] == "group-set" :
                            temp_original_data["source_group_set"].append(line_info_split[2])
                            
                        if source_list == "":
                            source_list = "%s" % (self.scan_description_check("source", line_info_split, "ip"))
                        else:
                            source_list = "%s&%s" % (source_list, self.scan_description_check("source", line_info_split, "ip"))
                            
                        #source 이 any인 경우 더이상의 source 가 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if source_list == "any":
                            source_any_check = True
                            temp_source_list = ""
                                
                            for source_row in self.main_data.temporary_vlan[security_zone_source]["ip"]:
                                if temp_source_list == "":
                                    temp_source_list = "%s" % (source_row)
                                else:
                                    temp_source_list = "%s&%s" % (temp_source_list, source_row)
                                    
                            source_list = temp_source_list
                            
                    elif line_info_split[0] == "destination":
                        # destination 원본을 보여 주기 위함
                        if line_info_split[1] == "group" :
                            temp_original_data["destination_group"].append(line_info_split[2])
                        elif line_info_split[1] == "group-set" :
                            temp_original_data["destination_group_set"].append(line_info_split[2])
                            
                        if destination_list == "":
                            destination_list = "%s" % (self.scan_description_check("destination", line_info_split, "ip"))

                        else:
                            destination_list = "%s&%s" % (destination_list, self.scan_description_check("destination", line_info_split, "ip"))
                        
                        #destination 이 any인 경우 더이상의 destination 이 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if destination_list == "any":
                            destination_any_check = True
                            #destination_list 기본 값은 security policy 선언되어 있던 IP 정보를 사용한다.(추후 프로파일에서 정의된 정보가 있을 경우 프로파일 정보로 대체됨)
                            temp_destination_list = ""
                            
                            for destination_row in self.main_data.temporary_vlan[security_zone_destination]["ip"]:
                                if temp_destination_list == "":
                                    temp_destination_list = "%s" % (destination_row)
                                else:
                                    temp_destination_list = "%s&%s" % (temp_destination_list,destination_row)
                                        
                            destination_list = temp_destination_list

                    elif line_info_split[0].lower() == "action":
                        #Action에 따른 설정 값을 적용한다.
                        if line_info_split[1].lower() == "pass":
                            filter_policy = "ACCEPT"
                            
                        elif line_info_split[1].lower() == "drop":
                            filter_policy = "DROP"
                            
                        elif line_info_split[1].lower() == "reject":
                            filter_policy = "REJECT"
                            
                    elif line_info_split[0] == "enable":
                        enable_check = "on"
                
                if zone_check == False:
                    continue
                
                # From Zone VD 와 To Zone VD 가 다른경우 UTM에서 지원되지 않으므로 Event처리(정현호:2015.05.28)
                if self.main_data.delegate_zone[security_zone_source] != self.main_data.delegate_zone[security_zone_destination] :
                    case_msg = "From / To Domain 정보 다름(From[%s], To[%s])" % (self.main_data.delegate_zone[security_zone_source], self.main_data.delegate_zone[security_zone_destination])
                    log.debug("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                    
                    # 미지원 기능 잠금 (2015.06.02 정현호)
                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "P", "S", case_msg, self.print_list(firewall_row))
                    #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue

                # incoming, outgoing, interzone 분기 관련
                eth_source_red_ip               = self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0].get("ip")
                security_zone_source_ip         = self.main_data.temporary_vlan.get(security_zone_source).get("ip")
                eth_destination_red_ip          = self.main_data.zone[self.main_data.delegate_zone.get(security_zone_destination)]["network"]["interface"][0].get("ip")
                security_zone_destination_ip    = self.main_data.temporary_vlan.get(security_zone_destination).get("ip")
                
                if eth_source_red_ip == security_zone_source_ip :
                    firewall_type                       = "fw_incoming" # incoming
                    if source_any_check == True :
                        source_list = ""
                    
                elif eth_destination_red_ip == security_zone_destination_ip :
                    firewall_type                       = "fw_outgoing" # outgoing
                    if destination_any_check == True :
                        destination_list = ""
                    
                else :
                    firewall_type                       = "fw_interzone" # interzone

                # Real Firewall data Parsing
                firewall_default_data = default_data.get(firewall_type)
                
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("=======================================================")
                 
                #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
                temp_firewall_data = ""
                 
                #여러 라인이 선언될 수있는 경우에 대한 처리
                for line_info in firewall_row:
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "service":
                        if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                            temp_firewall_data = firewall_default_data
                            destination_port = self.scan_description_check("dport", line_info_split, "port")
                            protocol = line_info_split[2]
 
                            temp_firewall_data = temp_firewall_data.replace("Enabled", enable_check)
                            
                            if firewall_type == "fw_incoming" and source_any_check == False:
                                temp_firewall_data = temp_firewall_data.replace("UPLINK:ANY", "")
                            elif firewall_type == "fw_outgoing" and destination_any_check == False:
                                temp_firewall_data = temp_firewall_data.replace("UPLINK:ANY", "")
                            else :
                                pass
                            
                            temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                            temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                            temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                            temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                            temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                            temp_firewall_data = temp_firewall_data.replace("Remark", firewall_id)
                             
                            if self.main_data.delegate_zone.get(security_zone_source) != None:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                                 
                            else:
                                case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                                 
                                self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                continue
                                     
                        else:
                            case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                            log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                            #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (firewall_row)) 
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "P", "C", case_msg, self.print_list(firewall_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                             
                    elif line_info_split[0] == "service-group":
                        if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                            
                            append_original_data = False
                            for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                                temp_firewall_data = firewall_default_data
                                destination_port = service_group_row.get("destination_port")
                                protocol = service_group_row.get("protocol")
                                 
                                temp_firewall_data = temp_firewall_data.replace("Enabled", enable_check)
                                
                                if firewall_type == "fw_incoming" and source_any_check == False:
                                    temp_firewall_data = temp_firewall_data.replace("UPLINK:ANY", "")
                                elif firewall_type == "fw_outgoing" and destination_any_check == False:
                                    temp_firewall_data = temp_firewall_data.replace("UPLINK:ANY", "")
                                else :
                                    pass
                                
                                temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                                temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                                temp_firewall_data = temp_firewall_data.replace("DestinationPort", destination_port)
                                temp_firewall_data = temp_firewall_data.replace("Protocol", protocol)
                                temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                                temp_firewall_data = temp_firewall_data.replace("Remark", firewall_id)
 
                                if self.main_data.delegate_zone.get(security_zone_source) != None:
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                                    #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                                    #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.print_list(service_group_row))
                                    
                                    # service-group original_data는 1번만 Append 하자
                                    if append_original_data == False :
                                        append_original_data = True
                                        temp_original_data["service_group"].append(line_info_split[1])    
                                        #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                                        #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_service_group[line_info_split[1]]["original_data"])
                                    
                                else:
                                    case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                                     
                                    self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    continue
            
                if temp_firewall_data == "":
                    temp_firewall_data = firewall_default_data
                    temp_firewall_data = temp_firewall_data.replace("Enabled", enable_check)
                    temp_firewall_data = temp_firewall_data.replace("Source(Network/IP)", source_list)
                    temp_firewall_data = temp_firewall_data.replace("Destination(Network/IP)", destination_list)
                    temp_firewall_data = temp_firewall_data.replace("DestinationPort", "")
                    temp_firewall_data = temp_firewall_data.replace("Protocol", "")
                    temp_firewall_data = temp_firewall_data.replace("ACCEPT/DROP/REJECT", filter_policy)
                    temp_firewall_data = temp_firewall_data.replace("Remark", firewall_id)
 
                    if self.main_data.delegate_zone.get(security_zone_source) != None:
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type].append(temp_firewall_data.replace("any", ""))
                         
                    else:
                        case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                        log.info("%s\n%s" % (case_msg, self.print_list(firewall_row)))
                        #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (firewall_row)) 
                         
                        self.main_data.set_event_msg_update("FW", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(firewall_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
             
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.print_list(firewall_row))
                
                #Make Original Data =======================================================================================================================
                #source_group_set : ip group set
                for source_group_set_row in temp_original_data["source_group_set"] :
                    if self.main_data.temporary_ip_group.get(source_group_set_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                        source_ip_group_set_row = self.main_data.temporary_ip_group[source_group_set_row]["original_data"]
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(source_ip_group_set_row)
                        
                        # ip Group Find
                        for line_info in source_ip_group_set_row:
                            ######################################################
                            #######################파싱 예제########################
                            # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                            # - group test_ip_group_1
                            # - group test_ip_group_2
                            ######################################################
                            line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
        
                            #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                            if line_info_split[0] == "group":
                                source_ip_group_row1 = line_info_split[1]
                                
                                if self.main_data.temporary_ip_group.get(source_ip_group_row1) != None :
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.main_data.temporary_ip_group[source_ip_group_row1]["original_data"])
                        # end loop ip group
                # end loopip source group set
                        
                #source_group : ip group
                for source_ip_group_row2 in temp_original_data["source_group"] :
                    if self.main_data.temporary_ip_group.get(source_ip_group_row2) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.main_data.temporary_ip_group[source_ip_group_row2]["original_data"])
                # end loop ip source group
                
                #destination_group_set : ip group set
                for destination_group_set_row in temp_original_data["destination_group_set"] :
                    if self.main_data.temporary_ip_group.get(destination_group_set_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                        destination_ip_group_set_row = self.main_data.temporary_ip_group[source_group_set_row]["original_data"]
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(destination_ip_group_set_row)
                        
                        # ip Group Find
                        for line_info in destination_ip_group_set_row:
                            ######################################################
                            #######################파싱 예제########################
                            # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                            # - group test_ip_group_1
                            # - group test_ip_group_2
                            ######################################################
                            line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
        
                            #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                            if line_info_split[0] == "group":
                                destination_ip_group_row1 = line_info_split[1]
                                
                                if self.main_data.temporary_ip_group.get(destination_ip_group_row1) != None :
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.main_data.temporary_ip_group[destination_ip_group_row1]["original_data"])
                        # end loop ip group
                # end loopip destination group set
                        
                #destination_group : ip group
                for destination_ip_group_row2 in temp_original_data["destination_group"] :
                    if self.main_data.temporary_ip_group.get(destination_ip_group_row2) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.main_data.temporary_ip_group[destination_ip_group_row2]["original_data"])
                # end loop ip destination group
                
                #service_group : service group
                for service_group_row in temp_original_data["service_group"] :
                    if self.main_data.temporary_service_group.get(service_group_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)][firewall_type+"_original_data"].append(self.main_data.temporary_service_group[service_group_row]["original_data"])
                # end loop service group
                # END Original Data =======================================================================================================================
            # End firewall data Loop
            
            
            #E2E Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            
            for zone_row in self.main_data.zone:
                firewall_num = ""
                firewall_name = ""
                fail_msg = ""
                
                for firewall_row in default_data :
                    fail_msg = ""
                    
                    if firewall_row.lower() == "fw_incoming" :
                        firewall_type   = "fw_incoming"
                        firewall_num    = "6"
                        firewall_name   = "FW[INCOMING]"
                    elif firewall_row.lower() == "fw_outgoing" :
                        firewall_type   = "fw_outgoing"
                        firewall_num    = "7"
                        firewall_name   = "FW[OUTGOING]"
                    else :
                        firewall_type   = "fw_interzone"
                        firewall_num    = "8"
                        firewall_name   = "FW[INTERZONE]"
                
                    firewall_cnt  = len(self.main_data.zone[zone_row][firewall_type])
                    firewall_data = self.print_list(self.main_data.zone[zone_row][firewall_type])
                    
                    #룰 정보 없어도 만든다. (정현호 :2015.05.27)
                    #if firewall_data == "":
                    #    continue
                    
                    e2e_msg_sub.set_parsing_type(firewall_num)   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER, 6:FW[incoming], 7:FW[OUTGOING], 8:FW[INTERZONE]
                    e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                    e2e_msg_sub.set_message("방화벽(%s) 정보 파싱(%s)[%s]" % (firewall_name, zone_row, firewall_cnt))
                    e2e_msg_sub.set_message_detail(firewall_data)
                    e2e_msg_sub.set_parsing_cnt(firewall_cnt)         # 파싱 개수
                    #e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":firewall_data, "original_data":self.print_list(self.main_data.zone[zone_row]["firewall_original_data"]), "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["firewall"]["name"])}))
                    e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":firewall_data, 
                                                               "original_data":self.print_list(self.main_data.zone[zone_row][firewall_type+"_original_data"]), 
                                                               "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                    
                    #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                    if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                        fail_msg = "방화벽(%s) 저장 폴더 생성 실패.(IP:%s, office:%s)" % (firewall_name, self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    
                    #룰 정보 저장
                    if fail_msg == "" and self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info[firewall_type]["name"]), firewall_data, "forced") == False:
                        fail_msg = "방화벽(%s) 정보 저장 실패.(IP:%s, office:%s)" % (firewall_name, self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                        continue
                    
                    e2e_msg_sub.set_response_status(fail_msg)
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                    
                    if fail_msg != "" :
                        continue
                    
                # loop firewall
            # loop Zone
                    
            fail_msg = "" # 여기가지 왔으면 성공임.
                    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("firewall_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "방화벽 파싱 종료 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("방화벽 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - SNAT 정보를 파싱한다.
    """
    def snat_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("SNAT 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #기본 DNAT정보 정의.
            snat_default_data = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                "Acction",
                                "Protocol",
                                "Source(Network/IP)",
                                "Destination(Network/IP)",
                                "DestinationPort",
                                "UPLINK:ANY(Destination)", #Destination(Zone/VPN/Uplink)
                                "SNAT",
                                "Remark",   #Remark
                                "",  #Log
                                "SourceAddress")
            
            #snat 정보 추출.
            for snat_row in self.main_data.rule["security_policy"].get("snat"):
                snat_profile_id = ""
                security_zone_source = ""
                security_zone_destination = ""
                destination_port = ""
                protocol = ""
                enable_check = "off"
                destination_list = ""
                source_list = ""
                snat_id = ""
                zone_check = True
                
                temp_original_data = {"source_group_set"        : [],
                                      "source_group"            : [],
                                      "service_group"           : [],
                                      "destination_group_set"   : [],
                                      "destination_group"       : [],
                                      "dnat_profile"            : [],
                                      "snat_profile"            : []}
                
                for line_info in snat_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip security policy from zone120_trust to zone120_untrust 10 id 17
                    # - source group zone120_trust_local
                    # - source xxx.xxx.xxx.xxx
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - destination xxx.xxx.xxx.xxx
                    # - destination range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - destination xxx.xxx.xxx.xxx/x
                    # - snat-profile 101
                    # - action pass log
                    # - enable
                    #
                    # - source / destination 이 any 인경우에는 source / destination 각각 1개 이상 선언될 수 없다.
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
            
                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        security_zone_source = line_info_split[4]
                        security_zone_destination = line_info_split[6]
                        
                        # id를 룰 변환의 Remark값에 넣어주자(정현호:2015.05.28)
                        if line_info_split[8] == "id" :
                            snat_id = line_info_split[9]
                        
                        if self.main_data.delegate_zone.get(security_zone_source) == None:
                            case_msg = "security_zone_source(domain) 정보 없음(ip, %s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                            #log.info("security_zone_source(domain) 정보가 없습니다. - SNAT(%s)\n%s" % (security_zone_source, snat_row))
                            
                            self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_source)) == None:
                                case_msg = "security_zone_source(대표존) 정보 없음(ip, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                #log.info("security_zone_source(대표존) 정보가 없습니다. - SNAT(%s)\n%s" %  (security_zone_source, snat_row))
                                
                                self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_source) == None:
                                    case_msg = "security_zone_source(interface) 정보 없음(ip, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                    #log.info("security_zone_source(interface) 정보가 없습니다. - SNAT(%s)\n%s" %  (security_zone_source, snat_row))
                                    
                                    self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                                
                        if self.main_data.delegate_zone.get(security_zone_destination) == None:
                            case_msg = "security_zone_destination(domain) 정보 없음(ip, %s)" % (security_zone_destination)
                            log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                            #log.info("security_zone_destination(domain) 정보가 없습니다. - SNAT(%s)\n%s" %  (security_zone_destination, snat_row))
                            
                            self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                            zone_check = False
                            break
                        
                        else:
                            if self.main_data.zone.get(self.main_data.delegate_zone.get(security_zone_destination)) == None:
                                case_msg = "security_zone_destination(대표존) 정보 없음(ip, %s)" % (security_zone_destination)
                                log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                #log.info("security_zone_destination(대표존) 정보가 없습니다. - SNAT(%s)\n%s" %  (security_zone_destination, snat_row))
                                
                                self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                
                                zone_check = False
                                break
                            
                            else:
                                if self.main_data.temporary_vlan.get(security_zone_destination) == None:
                                    case_msg = "security_zone_destination(interface) 정보 없음(ip, %s)" % (security_zone_destination)
                                    log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                    #log.info("security_zone_destination(interface) 정보가 없습니다. - SNAT(%s)\n%s" %  (security_zone_destination, snat_row))
                                    
                                    self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_destination, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    
                                    zone_check = False
                                    break
                            
                    elif line_info_split[0] == "source":
                        # source 원본을 보여 주기 위함
                        if line_info_split[1] == "group" :
                            temp_original_data["source_group"].append(line_info_split[2])
                        elif line_info_split[1] == "group-set" :
                            temp_original_data["source_group_set"].append(line_info_split[2])
                        
                        if source_list == "":
                            source_list = "%s" % (self.scan_description_check("source", line_info_split, "ip"))
                        else:
                            source_list = "%s&%s" % (source_list, self.scan_description_check("source", line_info_split, "ip"))
                            
                        #source 이 any인 경우 더이상의 source 가 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if source_list == "any":
                            if self.main_data.temporary_vlan.get(security_zone_source) != None:
                                temp_source_list = ""
                                
                                for source_row in self.main_data.temporary_vlan[security_zone_source]["ip"]:
                                    if temp_source_list == "":
                                        temp_source_list = "%s" % (source_row)
                                    else:
                                        temp_source_list = "%s&%s" % (temp_source_list, source_row)
                                        
                                source_list = temp_source_list
                            else:
                                case_msg = "security_zone_source(interface) 정보 없음(source, %s)" % (security_zone_source)
                                log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (snat_row))
                                
                                self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                continue
                            
                    elif line_info_split[0] == "destination":
                        
                        # destination 원본을 보여 주기 위함
                        if line_info_split[1] == "group" :
                            temp_original_data["destination_group"].append(line_info_split[2])
                        elif line_info_split[1] == "group-set" :
                            temp_original_data["destination_group_set"].append(line_info_split[2])
                        
                        if line_info_split[1] == "dnat-profile":
                            #snat 정보중 destination 에 dnat-profile 정보가 있는 경우는 destination이 any로 판단한다.(정현호 선임님 확인사항으로 dnat정보는 snat 정보에 영향을 주지 않는다로 판단)
                            #destination_list 기본 값은 security policy 선언되어 있던 IP 정보를 사용한다.(추후 프로파일에서 정의된 정보가 있을 경우 프로파일 정보로 대체됨)
                            #temp_original_data["dnat_profile"].append(line_info_split[2])
                            destination_list = "any"
                           
                        else:
                            if destination_list == "":
                                destination_list = "%s" % (self.scan_description_check("destination", line_info_split, "ip"))
                            
                            else:
                                destination_list = "%s&%s" % (destination_list, self.scan_description_check("destination", line_info_split, "ip"))
                            
                        #destination 이 any인 경우 더이상의 destination 이 없기 때문에 any에 대한 처리를 한다.(zone에 할당된 IP로 세팅)
                        if destination_list == "any":
                            #destination_list 기본 값은 security policy 선언되어 있던 IP 정보를 사용한다.(추후 프로파일에서 정의된 정보가 있을 경우 프로파일 정보로 대체됨)
                            temp_destination_list = ""
                            
                            for destination_row in self.main_data.temporary_vlan[security_zone_destination]["ip"]:
                                if temp_destination_list == "":
                                    temp_destination_list = "%s" % (destination_row)
                                else:
                                    temp_destination_list = "%s&%s" % (temp_destination_list,destination_row)
                                        
                            destination_list = temp_destination_list
                                
                    elif line_info_split[0] == "snat-profile":
                        snat_profile_id = line_info_split[1]
                        temp_original_data["snat_profile"].append(snat_profile_id)
                        
                    elif line_info_split[0] == "enable":
                        enable_check = "on"
                
                #destination_list 가 RED Zone의 IP정보와 같다면 UPLINK:ANY 으로 변경해야 한다.
                if self.main_data.delegate_zone.get(security_zone_source) != None and zone_check == True:
                    if len(self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0]["ip"]) == 0:
                        case_msg = "RED VLAN(source) 정보 없음(%s)" % (security_zone_source)
                        log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                        #log.info("RED VLAN 정보 없음\n%s" % (snat_row)) 
                        
                        self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        
                        continue
                    else:
                        for ip_info in self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["network"]["interface"][0]["ip"]:
                            if destination_list == ip_info:
                                destination_list = "UPLINK:ANY"
                                
                else:
                    case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                    log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                    #log.info("security zone source 정보 없음\n%s" % (snat_row))  
                    
                    self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    
                    continue

                # From Zone VD 와 To Zone VD 가 다른경우 UTM에서 지원되지 않으므로 Event처리(정현호:2015.05.28)
                if self.main_data.delegate_zone[security_zone_source] != self.main_data.delegate_zone[security_zone_destination] :
                    case_msg = "From / To Domain 정보 다름(From[%s], To[%s])" % (self.main_data.delegate_zone[security_zone_source], self.main_data.delegate_zone[security_zone_destination])
                    log.debug("%s\n%s" % (case_msg, self.print_list(snat_row)))
                    
                    # 미지원 기능 잠금 (2015.06.02 정현호)
                    self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "P", "S", case_msg, self.print_list(snat_row))
                    #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #이 값이 변하지 않는 경우 service 혹은 service-group 정보가 없는 경우이다 이런 경우 protocol/port 정보 등이 없이 생성 한다.
                temp_snat_data = ""
                
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("=======================================================")
                
                #여러 라인이 선언될 수있는 경우에 대한 처리
                for line_info in snat_row:
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "service":
                        if line_info_split[2] == "tcp" or line_info_split[2] == "udp" or line_info_split[2] == "any":
                            destination_port = self.scan_description_check("dport", line_info_split, "port")
                            protocol = line_info_split[2]
                            
                            if self.main_data.temporary_snat.get(snat_profile_id) != None:
                                temp_snat = []
                                
                                for snat_profile_row in self.main_data.temporary_snat.get(snat_profile_id)["data"]:
                                    temp_snat_data = snat_default_data
                                    
                                    #프로파일 정보 가져오기
                                    profile_source_list = snat_profile_row.get("source")
                                    profile_destination_list = snat_profile_row.get("destination")
                                    profile_insert = snat_profile_row.get("dynamic")
                                    
                                    if profile_insert == "RETURN":
                                        temp_snat_data = temp_snat_data.replace("SNAT", "RETURN")
                                        
                                    if profile_source_list == "any":
                                        profile_source_list = source_list
                                    
                                    if profile_destination_list == "any":
                                        profile_destination_list = destination_list
                                    
                                    if profile_destination_list == "UPLINK:ANY":
                                        profile_destination_list = ""
                                        temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "UPLINK:ANY")
                                            
                                    else:
                                        temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "")
                                
                                    temp_snat_data = temp_snat_data.replace("Acction", enable_check)
                                    temp_snat_data = temp_snat_data.replace("Source(Network/IP)", profile_source_list)
                                    temp_snat_data = temp_snat_data.replace("Destination(Network/IP)", profile_destination_list)
                                    temp_snat_data = temp_snat_data.replace("DestinationPort", destination_port)
                                    temp_snat_data = temp_snat_data.replace("Protocol", protocol)
                                    temp_snat_data = temp_snat_data.replace("Remark", snat_id)
                                    temp_snat_data = temp_snat_data.replace("SourceAddress", profile_insert)
                                    
                                    temp_snat.append(temp_snat_data)
                                    
                                if self.main_data.delegate_zone.get(security_zone_source) != None:
                                    for temp_snat_row in temp_snat:
                                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat"].append(temp_snat_row.replace("any", ""))
                                    
                                else:
                                    case_msg = "security_zone_source 정보 없음(service, %s)" % (security_zone_source)
                                    log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                    #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (snat_row)) 
                                    
                                    self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                    continue
                                    
                        else:
                            case_msg = "정의되지 않은 protocol 정보(service, %s)" % (line_info_split[2])
                            log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                            #log.info("정의되지 않은 protocol 정보 입니다.\n%s" % (snat_row)) 
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "P", "S", case_msg, self.print_list(snat_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                            
                    elif line_info_split[0] == "service-group":
                        if self.main_data.temporary_service_group.get(line_info_split[1]) != None:
                            
                            append_original_data = False
                            for service_group_row in self.main_data.temporary_service_group[line_info_split[1]]["data"]:
                                destination_port = service_group_row.get("destination_port")
                                protocol = service_group_row.get("protocol")
                                
                                if self.main_data.temporary_snat.get(snat_profile_id) != None:
                                    for snat_profile_row in self.main_data.temporary_snat.get(snat_profile_id)["data"]:
                                        temp_snat_data = snat_default_data
                                        
                                        #프로파일 정보 가져오기
                                        profile_source_list = snat_profile_row.get("source")
                                        profile_destination_list = snat_profile_row.get("destination")
                                        profile_insert = snat_profile_row.get("dynamic")
                                        
                                        if profile_insert == "RETURN":
                                            temp_snat_data = temp_snat_data.replace("SNAT", "RETURN")
                                        
                                        if profile_source_list == "any":
                                            profile_source_list = source_list
                                        
                                        if profile_destination_list == "any":
                                            profile_destination_list = destination_list
                                        
                                        if profile_destination_list == "UPLINK:ANY":
                                            profile_destination_list = ""
                                            temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "UPLINK:ANY")
                                                
                                        else:
                                            temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "")
                                        
                                        temp_snat_data = temp_snat_data.replace("Acction", enable_check)
                                        temp_snat_data = temp_snat_data.replace("Source(Network/IP)", profile_source_list)
                                        temp_snat_data = temp_snat_data.replace("Destination(Network/IP)", profile_destination_list)
                                        temp_snat_data = temp_snat_data.replace("DestinationPort", destination_port)
                                        temp_snat_data = temp_snat_data.replace("Protocol", protocol)
                                        temp_snat_data = temp_snat_data.replace("Remark", snat_id)
                                        temp_snat_data = temp_snat_data.replace("SourceAddress", profile_insert)
                                        
                                        temp_snat.append(temp_snat_data)
                                        
                                    if self.main_data.delegate_zone.get(security_zone_source) != None:
                                        for temp_snat_row in temp_snat:
                                            self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat"].append(temp_snat_row.replace("any", ""))
                                            
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_service_group[line_info_split[1]]["original_data"])
                                            
                                        # service-group original_data는 1번만 Append 하자
                                        if append_original_data == False :
                                            append_original_data = True
                                            temp_original_data["service_group"].append(line_info_split[1])    
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                                            #self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_service_group[line_info_split[1]]["original_data"])
                                        
                                    else:
                                        case_msg = "security_zone_source 정보 없음(service-group, %s)" % (security_zone_source)
                                        log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                                        #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (snat_row))
                                        
                                        self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                        continue
                
                if temp_snat_data == "":
                    if self.main_data.temporary_snat.get(snat_profile_id) != None:
                        temp_snat = []
                        
                        for snat_profile_row in self.main_data.temporary_snat.get(snat_profile_id)["data"]:
                            temp_snat_data = snat_default_data
                            
                            #프로파일 정보 가져오기
                            profile_source_list = snat_profile_row.get("source")
                            profile_destination_list = snat_profile_row.get("destination")
                            profile_insert = snat_profile_row.get("dynamic")
                            
                            if profile_insert == "RETURN":
                                temp_snat_data = temp_snat_data.replace("SNAT", "RETURN")
                                        
                            if profile_source_list == "any":
                                profile_source_list = source_list
                            
                            if profile_destination_list == "any":
                                profile_destination_list = destination_list
                            
                            if profile_destination_list == "UPLINK:ANY":
                                profile_destination_list = ""
                                temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "UPLINK:ANY")
                                    
                            else:
                                temp_snat_data = temp_snat_data.replace("UPLINK:ANY(Destination)", "")
                                        
                            temp_snat_data = temp_snat_data.replace("Acction", enable_check)
                            temp_snat_data = temp_snat_data.replace("Source(Network/IP)", profile_source_list)
                            temp_snat_data = temp_snat_data.replace("Destination(Network/IP)", profile_destination_list)
                            temp_snat_data = temp_snat_data.replace("DestinationPort", "")
                            temp_snat_data = temp_snat_data.replace("Protocol", "")
                            temp_snat_data = temp_snat_data.replace("Remark", snat_id)
                            temp_snat_data = temp_snat_data.replace("SourceAddress", profile_insert)
                            
                            temp_snat.append(temp_snat_data)
                            
                        if self.main_data.delegate_zone.get(security_zone_source) != None:
                            for temp_snat_row in temp_snat:
                                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat"].append(temp_snat_row.replace("any", ""))
                                
                        else:
                            case_msg = "security_zone_source 정보 없음(%s)" % (security_zone_source)
                            log.info("%s\n%s" % (case_msg, self.print_list(snat_row)))
                            #log.info("정의되지 않은 zone 정보 입니다.\n%s" % (snat_row)) 
                            
                            self.main_data.set_event_msg_update("SN", self.find_utm_userid(security_zone_source, self.main_data), "C", "S", case_msg, self.print_list(snat_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            continue
                
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.print_list(snat_row))
                
                #Make Original Data 
                #source_group_set : ip group set
                for source_group_set_row in temp_original_data["source_group_set"] :
                    if self.main_data.temporary_ip_group.get(source_group_set_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                        source_ip_group_set_row = self.main_data.temporary_ip_group[source_group_set_row]["original_data"]
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(source_ip_group_set_row)
                        
                        # ip Group Find
                        for line_info in source_ip_group_set_row:
                            ######################################################
                            #######################파싱 예제########################
                            # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                            # - group test_ip_group_1
                            # - group test_ip_group_2
                            ######################################################
                            line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
        
                            #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                            if line_info_split[0] == "group":
                                source_group_row1 = line_info_split[1]
                                
                                if self.main_data.temporary_ip_group.get(source_group_row1) != None :
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_ip_group[source_group_row1]["original_data"])
                        # end loop ip group
                # end loopip source group set
                        
                #source_group : ip group
                for source_group_row2 in temp_original_data["source_group"] :
                    if self.main_data.temporary_ip_group.get(source_group_row2) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_ip_group[source_group_row2]["original_data"])
                # end loop ip source group
                
                #destination_group_set : ip group set
                for destination_group_set_row in temp_original_data["destination_group_set"] :
                    if self.main_data.temporary_ip_group.get(destination_group_set_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                        destination_ip_group_set_row = self.main_data.temporary_ip_group[source_group_set_row]["original_data"]
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(destination_ip_group_set_row)
                        
                        # ip Group Find
                        for line_info in destination_ip_group_set_row:
                            ######################################################
                            #######################파싱 예제########################
                            # - security zone jhhtest_untrust ip group-set test_ip_groupset_A
                            # - group test_ip_group_1
                            # - group test_ip_group_2
                            ######################################################
                            line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.
        
                            #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                            if line_info_split[0] == "group":
                                destination_group_row1 = line_info_split[1]
                                
                                if self.main_data.temporary_ip_group.get(destination_group_row1) != None :
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_ip_group[destination_group_row1]["original_data"])
                        # end loop ip group
                # end loopip destination group set
                        
                #destination_group : ip group
                for destination_group_row2 in temp_original_data["destination_group"] :
                    if self.main_data.temporary_ip_group.get(source_group_row2) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_ip_group[destination_group_row2]["original_data"])
                # end loop ip destination group
                
                #service_group : service group
                for service_group_row in temp_original_data["service_group"] :
                    if self.main_data.temporary_service_group.get(service_group_row) != None :
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                        self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_service_group[service_group_row]["original_data"])
                # end loop service group
                
                # snat-profile
                if self.main_data.temporary_snat.get(snat_profile_id) != None:
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append("")
                    self.main_data.zone[self.main_data.delegate_zone.get(security_zone_source)]["snat_original_data"].append(self.main_data.temporary_snat.get(snat_profile_id)["original_data"])
                # end snat-profile
                
            # 5월 23일 삭제(정현호 : EDIAN Default SNAT 삭제로 해결)
            # e2e를 위한 no NAT 데이터 추가.
#             for zone_row in self.main_data.zone:
#                 for interface_row in self.main_data.zone[zone_row]["network"]["interface"]:
#                     if interface_row.get("vlan_id") != "eth1" and interface_row.get("check") == True:
#                         for ip_row in interface_row.get("ip"):
#                             
#                             vlan_ip_split = ip_row.split(".")
#                             #공인아이피 체크를 위해서는 C클래스까지 체크가 가능하여 마지막 값은 0으로 만든다.
#                             ip_info = "%s.%s.%s.%s" % (str(vlan_ip_split[0]), str(vlan_ip_split[1]), str(vlan_ip_split[2]), "0")
#                             
#                             #PUBLIC, PRIVATE
#                             if IP(ip_info).iptype() == "PUBLIC":
#                                 snat_public_info = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
#                                 "on",
#                                 "",
#                                 ip_row,
#                                 "",
#                                 "",
#                                 "UPLINK:ANY",
#                                 "RETURN",
#                                 "",
#                                 "",
#                                 "RETURN")
#                                 
#                                 #SNAT 정보에 추가한다.
#                                 self.main_data.zone[zone_row]["snat"].append(snat_public_info)
                
            #E2E Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            e2e_msg_sub.set_parsing_type("3")   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER

            for zone_row in self.main_data.zone:
                snat_cnt  = len(self.main_data.zone[zone_row]["snat"])
                snat_data = self.print_list(self.main_data.zone[zone_row]["snat"])
                
                #룰 정보 없어도 만든다. (정현호 :2015.05.27)
                #if snat_data == "":
                #    continue
                
                snat_original_data = self.print_list(self.main_data.zone[zone_row]["snat_original_data"])
                
                e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                e2e_msg_sub.set_message("SNAT 정보 파싱(%s)[%s]" % (zone_row, snat_cnt))
                e2e_msg_sub.set_message_detail(snat_data)
                e2e_msg_sub.set_parsing_cnt(snat_cnt)         # 파싱 개수
                #e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":snat_data, "original_data":snat_original_data, "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["snat"]["name"])}))
                e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":snat_data, 
                                                           "original_data":snat_original_data, 
                                                           "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                    fail_msg = "SNAT 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
                #SNAT 정보 저장
                if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["snat"]["name"]), snat_data, "forced") == False:
                    fail_msg = "SNAT 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("snat_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "SNAT 파싱 종료 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("SNAT 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 룰 정보 확인(메모리 관리)
    """
    def division_detail_split(self, division_detail):
        data = []
        
        for line_info in division_detail.split('\n'):
            line_info = line_info.strip()
            
            if line_info != "":
                data.append(line_info)
        
        return data
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 룰 정보 확인(메모리 관리)
    """
    def scan_rule_file(self):
        fail_msg = ""
        
        try:
            # "!" 를 기준으로 텍스트를 잘라낸다.
            for division_detail in self.main_data.rule_file_info.split('!'):
                #각 라인별로 분리.
                for line_info in division_detail.split('\n'):
                    #앞/뒤 공백제거
                    line_info = line_info.strip()
                    
                    #ip dhcp multi-server
                    if line_info.startswith("ip dhcp multi-server") and line_info.startswith("ip dhcp multi-server pool") == False:
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["dhcp"].append(return_data)
                        break
                    #ip dhcp multi-server pool
                    elif line_info.startswith("ip dhcp multi-server pool"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["dhcp_pool"].append(return_data)
                        break
                    
                    #interface eth
                    elif line_info.startswith("interface eth") and line_info.find(".") != -1:
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["interface"].append(return_data)
                        break
                    
                    #interface bridge
                    elif line_info.startswith("interface bridge"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["bridge"].append(return_data)
                        break
                    
                    #router virtual
                    elif line_info.startswith("router virtual"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["router_virtual"].append(return_data)
                        break
                    
                    #security zone ... group
                    elif line_info.startswith("security zone") and line_info.find("group") != -1 and  line_info.find("group-set") == -1:
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["ip_group"].append(return_data)
                        break
                    
                    #security zone ... group-set
                    elif line_info.startswith("security zone") and line_info.find("group-set") != -1:
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["ip_group_set"].append(return_data)
                        break
                    
                    #service group
                    elif line_info.startswith("service group"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["service_group"].append(return_data)
                        break
                    
                    #ip snat profile
                    elif line_info.startswith("ip snat profile"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["snat_profile"].append(return_data)
                        break
                    
                    #ip dnat profile
                    elif line_info.startswith("ip dnat profile"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["dnat_profile"].append(return_data)
                        break
                    
                    elif line_info.startswith("ip security policy from"):
                        #DNAT에 대한 security policy 정보 저장
                        if division_detail.find("dnat-profile") != -1:
                            return_data = self.division_detail_split(division_detail)
                            self.main_data.rule["security_policy"]["dnat"].append(return_data)
                        
                        #SNAT에 대한 security policy 정보 저장
                        if division_detail.find("snat-profile") != -1:
                            return_data = self.division_detail_split(division_detail)
                            self.main_data.rule["security_policy"]["snat"].append(return_data)
                        
                        #방화벽에 대한 security policy 정보 저장
#                         if division_detail.find("snat-profile") == -1 and division_detail.find("dnat-profile") == -1:
                        if division_detail.find("dnat-profile") == -1:
                            return_data = self.division_detail_split(division_detail)
                            self.main_data.rule["security_policy"]["firewall"].append(return_data)
                            
                        break
                    
                    #virtual domain
                    elif line_info.startswith("virtual domain"):
                        return_data = self.division_detail_split(division_detail)
                        self.main_data.rule["virtual_domain"].append(return_data)
                        break
                    
                    #hostname
                    elif line_info.startswith("hostname"):
                        return_data = self.division_detail_split(division_detail)
                        
                        for line_info in return_data:
                            #######################파싱 예제########################
                            # - hostname KT_RnD
                            ######################################################
                            line_info_split = line_info.split(" ")  #정상작인 라인을 스페이스를 구분으로 분리한다.
                        
                            if line_info_split[0] == "hostname":
                                self.main_data.ax_gate_info["ax_gate_name"] = line_info_split[1]
                                self.main_data.event_msg["host_name"] = self.main_data.ax_gate_info["ax_gate_name"]
                                break
                            
                        break
            
            # hostName을 DB에 저장한다. 우선 update
            updateState = self.connect.update(self.sql.update_hwutm_info(self.main_data.ax_gate_info["office_code"],
                                                                         self.main_data.ax_gate_info["ax_gate_ip"],
                                                                         self.main_data.ax_gate_info["ax_gate_name"]))
            if updateState == 0 : # insert 한다.
                updateState = self.connect.insert(self.sql.insert_hwutm_info(self.main_data.ax_gate_info["office_code"],
                                                                         self.main_data.ax_gate_info["ax_gate_ip"],
                                                                         self.main_data.ax_gate_info["ax_gate_name"]))
                    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_rule_file", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "룰 정보 확인 실패 (%s)" % (str(ex))
        
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 임시로 사용할 bridge 정보를 파싱 한다
    """
    def temporary_bridge_parsing(self):
        fail_msg = ""
        
        try:
            #메모리에 저장한 router virtual 정보를 파싱한다.
            for virtual_router_info_row in self.main_data.rule["bridge"]:
                security_zone = ""
                bridge_port = []
                proxy_arp = []
                ip_address = []
                bridge_id = ""
                shutdown = "off"
                
                #구분한 데이터 정보 확인
                for line_info in virtual_router_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - interface bridge80    <- 브릿지명 : interface bridge 여기까지는 default 정보이고 interface bridge 뒤부터 브릿지 명이 된다. 
                    # - description zone80_bridge_클렉스    <- 설명
                    # - mtu 1024    <- MTU (68-9216)
                    # - arp-ageing-timeout 1000    <- ARP Aging Timeout (1-3000초)
                    # - ip proxy-arp    <- IP Proxy ARP 설정 (range는 자동으로 설정되며 ip proxy-arp 설정이 안되어 있는경우 해당 라인 없음)
                    # - ip proxy-arp alias range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - ip proxy-arp alias range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx
                    # - bridge ageing 300    <- Aging 설정 (0-21474836)
                    # - bridge priority 32768    <- 우선순위 설정 (0-65535)
                    # - bridge forward-delay 3    <- Forward-delay 설정 (0-21474836)
                    # - bridge hello-time 2    <- Hello Time 설정 (0-21474836)
                    # - bridge max-age 20    <- Max Age 설정 (0-21474836)
                    # - bridge stp enable    <- STP 활성화 옵션 선택 하면 해당 라인 없음 
                    # - bridge multi-port    <- Multi-Port 활성화 옵션 선택 하면 해당 라인 없음 
                    # - bridge port eth3.80 priority 32    <- 통합 인터페이스
                    # - bridge port eth3.81 priority 32    <- 통합 인터페이스
                    # - ip address xxx.xxx.xxx.xxx/x    <- 기본 IP: 없음, STATIC
                    # - ip address xxx.xxx.xxx.xxx/x secondary    <- 보조 IP
                    # - bandwidth 100    <- 서비스 품질 대역폭 (1-10000 Mbps)
                    # - no multicast    <- 멀티캐스트 옵션 선택 하면 해당 라인 없음
                    # - no icmp-redirects    <- icmp-redirects 옵션 선택 하면 해당 라인 없음
                    # - no ip forwarding    <- ip forwarding 옵션 선택 하면 해당 라인 없음
                    # - promisc-mode    <- promisc-mode 옵션 선택 안하면 해당 라인 없음
                    # - link-check 60 30 arp 5.5.5.5    <- 링크 체크 검사주기(1-60초), 실패허용개수(1-60회), 검사방법(arp, icmp), 검사대상 IP (옵션)
                    # - security-zone zone80_trust    <- Security Zone (설정안함, zoneXXX)
                    # - no shutdown    <- 활성화 여부
                    ######################################################
                    line_info_split = line_info.split(" ")  #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "interface":
                        bridge_id = line_info_split[1]
                          
                    elif line_info_split[0] == "security-zone":
                        security_zone = line_info_split[1]
                    
                    elif line_info_split[0] == "ip" and line_info_split[1] == "address":
                        ip_address.append(line_info_split[2])
                            
                    elif line_info_split[0] == "ip" and line_info_split[1] == "proxy-arp":
                        #proxy_arp는 ip정보는 사용하지 않고 proxy_arp 적용 여부만 사용한다.
                        #proxy-arp 여러 라인이 존재할 수 있어 값이 없는 경우에만 append 한다.
                        if len(proxy_arp) == 0:
                            proxy_arp.append("ok")
                    
                    elif line_info_split[0] == "bridge" and line_info_split[1] == "port":
                        bridge_port.append(line_info_split[2])
                    
                    #shutdown 처리 : no shutdown (정현호:2015.05.28)
                    elif line_info_split[0] == "shutdown":
                        shutdown = "on"
                    
                    #shutdown 처리 : no shutdown (정현호:2015.05.28)
                    elif line_info_split[0] == "no" and line_info_split[1] == "shutdown":
                        shutdown = "off"
                
                #필수 정보값(IP) 확인
                if len(ip_address) == 0:
                    case_msg = "IP 정보 없음(%s)" % (ip_address)
                    log.info("%s\n%s" % (case_msg, self.print_list(virtual_router_info_row)))
                    #log.info("IP 정보가 존재하지 않습니다.\n(temporary_bridge_parsing)\n%s" % (self.print_list(virtual_router_info_row)))
                    
                    self.main_data.set_event_msg_update("BR", 
                                                        self.find_utm_userid(security_zone, self.main_data),
                                                        "C", "S", case_msg, self.print_list(virtual_router_info_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #필수 정보값(Port) 확인
                if len(bridge_port) == 0:
                    case_msg = "bridge_port 정보 없음"
                    log.info("%s\n%s" % (case_msg, self.print_list(virtual_router_info_row)))
                    #log.info("bridge_port 정보가 존재하지 않습니다.\n(temporary_bridge_parsing)\n%s" % (self.print_list(virtual_router_info_row)))
                    
                    self.main_data.set_event_msg_update("BR", 
                                                        self.find_utm_userid(security_zone, self.main_data),
                                                        "C", "S", case_msg, self.print_list(virtual_router_info_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                        
#                     #브릿지 정보에 bridge_port 정보가 포함되지 않는 경우가 있어 분석을 위한 데이터 추가
#                     self.main_data.temporary_bridge[security_zone] = {"ip":ip_address,
#                                                                       "proxy":proxy_arp,
#                                                                       "bridge_id":bridge_id,
#                                                                       "original_data":self.print_list(virtual_router_info_row)}
                    continue
                
                #임시 브릿지 정보 저장
                for bridge_port_row in bridge_port:
                    self.main_data.temporary_bridge[bridge_port_row] = {"ip":ip_address,
                                                                        "proxy":proxy_arp,
                                                                        "security-zone":security_zone,
                                                                        "bridge_id":bridge_id,
                                                                        "shutdown":shutdown}
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("temporary_bridge_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "bridge 파싱 실패 (%s)" % (str(ex))
        
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - virtual router 정보를 파싱 한다.
    """
    def virtual_router_parsing(self):
        fail_msg = ""
        
        try:
            router_format = "on,,ip_1,ip_2,static_routing,,,,,,,"   #router 기본 파싱 데이터 포멧

            #메모리에 저장한 router virtual 정보를 파싱한다.
            for virtual_router_info_row in self.main_data.rule["router_virtual"]:
                temp_virtual_router_ip = {"default_router":[], "secondary_router":[], "original_data":""} #임시 저장 변수 선언
                                    
                #구분한 데이터 정보 확인
                for line_info in virtual_router_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - router virtual 250
                    # - virtual ip route xxx.xxx.xxx.xxx/x xxx.xxx.xxx.xxx
                    # - virtual ip route xxx.xxx.xxx.xxx/x xxx.xxx.xxx.xxx
                    ######################################################
                    line_info_split = line_info.split(" ")  #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "router":
                        router_id = line_info_split[2]
                    
                    elif line_info_split[0] == "virtual":
                        if line_info_split[3] == "0.0.0.0/0":
                            #temp_virtual_router_ip["default_router"].append(router_format.replace("ip_1", line_info_split[3]).replace("ip_2", line_info_split[4]))
                            temp_virtual_router_ip["default_router"].append(line_info_split[4])
                        else:
                            temp_virtual_router_ip["secondary_router"].append(router_format.replace("ip_1", line_info_split[3]).replace("ip_2", line_info_split[4]))
                
                # default router 없으면 파싱하지 않는다. (정현호 : 2015.05.12)
                # deligate_zone 하싱시 처리하지 않토록함. 
                        
                #임시? 로직으로 김복순 박사님의 요청으로 추가되었음
                temp_virtual_router_ip["secondary_router"].append(config.SyncConfig.testing_secondary_router)   # "on,,10.0.0.0/24,192.168.10.1,test_routing,,,,,,,"
                
                #원본데이터 저장
                temp_virtual_router_ip["original_data"] = self.print_list(virtual_router_info_row)

                #파싱 데이터 저장
                self.main_data.temporary_router_virtual[router_id] = temp_virtual_router_ip
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("virtual_router_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "virtual router 파싱 실패 (%s)" % (str(ex))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 대표존ID 를 생성한다.
    """
    def scan_delegate_zone_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("고객/Router 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
            #E2E Sub Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_indent(2)
            
            #메모리에 저장한 router virtual 정보를 파싱한다.
            for virtual_domain_info_row in self.main_data.rule["virtual_domain"]:
                temp_delegate_zone_id = ""   #임시 대표존ID 저장변수
                temp_label = ""              #임시 VD 저장변수
                temp_utm_zoneid = ""    # 임시 존ID ex) zone120_trust ==> zone12
                temp_security_zone = [] 
                temp_virtual_zone = {}
                e2e_virtual_domain = {}
                
                for line_info in virtual_domain_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - virtual domain vd3 id 3
                    # -  label 틸론
                    # -  security-zone zone120_trust
                    # -  security-zone zone120_untrust
                    # -  security-zone zone121_trust
                    # -  security-zone zone123_trust
                    # -  router-virtual 3
                    ######################################################
                    line_info_split = line_info.split(" ")  #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "virtual":
                        temp_delegate_zone_id = line_info_split[2]
                        
                    elif line_info_split[0] == "label":
                        temp_label = line_info_split[1]
                        
                    elif line_info_split[0] == "security-zone":
                        #파싱 데이터 저장
                        temp_virtual_zone[line_info_split[1]] = temp_delegate_zone_id#"%s_%s" % (temp_delegate_zone_id, str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_"))
                        temp_security_zone.append( line_info_split[1] )
                        
                        # UTM 매핑정보 만들기 (맨 처음 존만 만든다)
                        if temp_utm_zoneid == "":
                            zone_split = line_info_split[1].split("_")
                            if len(zone_split[0]) != 0 :
                                temp_utm_zoneid = "%s_%s" % (zone_split[0][0:len(zone_split[0])-1], str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") )
                            
                            else :
                                case_msg = "대표존 정보 부정확(%s)" % (line_info_split[1])
                                log.info("%s\n%s" % (case_msg, self.print_list(virtual_domain_info_row)) )
                                #log.info("대표존 정보가 없습니다.\n%s" % (self.print_list(virtual_domain_info_row)) )
                                
                                self.main_data.set_event_msg_update("ZO", 
                                                                    "%s_%s" % (temp_delegate_zone_id, str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") ),
                                                                    #self.find_utm_userid(line_info_split[1], self.main_data),
                                                                    "C", "S", case_msg, self.print_list(virtual_domain_info_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                break;
                            
                    elif line_info_split[0] == "router-virtual":
                        #router-virtual 정보가 없는 경우 대표존ID로 데이터를 생성하지 않는다.
                        if self.main_data.temporary_router_virtual.get(line_info_split[1]) != None:
                            
                            if len(self.main_data.temporary_router_virtual[line_info_split[1]]["default_router"]) == 0 :
                                case_msg = "default gateway 정보 없음(%s %s)" % (line_info_split[0], line_info_split[1])
                                log.info("%s\n%s" % (case_msg, self.print_list(virtual_domain_info_row)) )
                                #log.info("default router 정보가 존재하지 않습니다.\n%s" % (self.print_list(virtual_domain_info_row)) )
                                
                                self.main_data.set_event_msg_update("ZO", 
                                                                    "%s_%s" % (temp_delegate_zone_id, str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") ),
                                                                    "C", "S", case_msg, self.print_list(virtual_domain_info_row))
                                self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                                break

                            self.main_data.zone[temp_delegate_zone_id] = {}
                            self.main_data.zone[temp_delegate_zone_id]["label"] = temp_label
                            self.main_data.zone[temp_delegate_zone_id]["network"] = {}
                            self.main_data.zone[temp_delegate_zone_id]["network"]["interface"] = [
                                                                                                  {"check":False, "vlan_id":"eth1", "vlan":"", "ip":[], "proxy":[], "shutdown":""}, 
                                                                                                  {"check":False, "vlan_id":"eth2", "vlan":"", "ip":[], "proxy":[], "shutdown":""},
                                                                                                  {"check":False, "vlan_id":"eth3", "vlan":"", "ip":[], "proxy":[], "shutdown":""},
                                                                                                  {"check":False, "vlan_id":"eth4", "vlan":"", "ip":[], "proxy":[], "shutdown":""},
                                                                                                  {"check":False, "vlan_id":"eth5", "vlan":"", "ip":[], "proxy":[], "shutdown":""},
                                                                                                  ]
                            
                            self.main_data.zone[temp_delegate_zone_id]["network"]["bridge"] = [[], [], []]          # bridge 0, 1, 2
                            self.main_data.zone[temp_delegate_zone_id]["network"]["proxy-arp"] = [[], [], []]       # bridge poxy-arp 0, 1, 2
                            self.main_data.zone[temp_delegate_zone_id]["network"]["shutdown"] = ["", "", ""]        # bridge shutdown 0, 1, 2
                            self.main_data.zone[temp_delegate_zone_id]["network"]["default_router"] = self.main_data.temporary_router_virtual[line_info_split[1]]["default_router"][0]
                            
                            self.main_data.zone[temp_delegate_zone_id]["utm_mappid"] = temp_utm_zoneid
                            self.main_data.zone[temp_delegate_zone_id]["utm_userid"] = "%s_%s" % (temp_delegate_zone_id, str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") ) 
                            
                            self.main_data.zone[temp_delegate_zone_id]["router"] = self.main_data.temporary_router_virtual.get(line_info_split[1])["secondary_router"]
                            self.main_data.zone[temp_delegate_zone_id]["dhcp"] =[]
                            self.main_data.zone[temp_delegate_zone_id]["snat"] = []
                            self.main_data.zone[temp_delegate_zone_id]["dnat"] = []
                            self.main_data.zone[temp_delegate_zone_id]["firewall"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_outgoing"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_incoming"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_interzone"] = []
                            
                            self.main_data.zone[temp_delegate_zone_id]["router_original_data"] = self.main_data.temporary_router_virtual.get(line_info_split[1])["original_data"]
                            self.main_data.zone[temp_delegate_zone_id]["dhcp_original_data"] =[]
                            self.main_data.zone[temp_delegate_zone_id]["snat_original_data"] = []
                            self.main_data.zone[temp_delegate_zone_id]["dnat_original_data"] = []
                            self.main_data.zone[temp_delegate_zone_id]["firewall_original_data"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_outgoing_original_data"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_incoming_original_data"] = []
                            self.main_data.zone[temp_delegate_zone_id]["fw_interzone_original_data"] = []
                            
                            #정상적으로 로직이 진행된 경우에만 대표존ID를 생성한다.
                            for temp_virtual_zone_row in temp_virtual_zone:
                                self.main_data.delegate_zone[temp_virtual_zone_row] = temp_virtual_zone[temp_virtual_zone_row]
                            
                            #e2e를 위한 vd 저장 
                            e2e_virtual_domain[temp_delegate_zone_id] = {"security-zone" : temp_security_zone,
                                                                         "original_data" : self.print_list(virtual_domain_info_row),
                                                                         "router-virtual" : line_info_split[1] }
                            
                            #고객정보 Sub e2e 결과
                            e2e_msg_sub.set_utm_userid(self.main_data.zone[temp_delegate_zone_id]["utm_userid"])
                            e2e_msg_sub.set_message("고객 정보 파싱(%s)" % (temp_delegate_zone_id))
                            e2e_msg_sub.set_message_detail(str(e2e_virtual_domain).replace("],", "],\n%s\n" % (config.SyncConfig.sub_split)) )
                            #e2e_msg_sub.set_message_detail(e2e_virtual_domain)
                            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                            
                            # vd 정보를 mss_order 네 update 한다. [5.23](없는 경우 pass)
                            self.connect.update(self.sql.updateMssOrder_axgate_info(self.main_data.zone[temp_delegate_zone_id]["utm_mappid"],
                                                                                    self.main_data.zone[temp_delegate_zone_id]["label"],
                                                                                    temp_delegate_zone_id))
                                                        
                        else:
                            case_msg = "router virtual 정보 없음(%s %s)" % (line_info_split[0], line_info_split[1])
                            log.info("%s\n%s" % (case_msg, self.print_list(virtual_domain_info_row)) )
                            #log.info("router virtual 정보가 존재하지 않습니다.\n%s" % (self.print_list(virtual_domain_info_row)) )
                            
                            self.main_data.set_event_msg_update("ZO",
                                                                "%s_%s" % (temp_delegate_zone_id, str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") ),
                                                                "C", "S", case_msg, self.print_list(virtual_domain_info_row))
                            self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                
            # Router 정보 Parsing
            e2e_msg_sub.set_parsing_type("5")   # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER
            
            for zone_row in self.main_data.zone:
                router_cnt  = len(self.main_data.zone[zone_row]["router"])
                router_data = self.print_list(self.main_data.zone[zone_row]["router"])
                
                #룰 정보 없어도 만든다. (정현호 :2015.05.27)
                #if router_data == "":
                #    continue
                
                router_original_data = self.main_data.zone[zone_row]["router_original_data"]
                
                e2e_msg_sub.set_utm_userid(self.main_data.zone[zone_row].get("utm_userid"))
                e2e_msg_sub.set_message("Router 정보 파싱(%s)[%s]" % (zone_row, router_cnt))
                e2e_msg_sub.set_message_detail(router_data)
                e2e_msg_sub.set_parsing_cnt(router_cnt)         # 파싱 개수
                #e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":router_data, "original_data":router_original_data, "rule_data":"%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["router"]["name"])}))
                e2e_msg_sub.set_rule_file_name(json.dumps({"parsing_data":router_data, 
                                                           "original_data":router_original_data, 
                                                           "original_rule":"%s/%s/%s" % (self.main_data.file_info["directory_path"], config.SyncConfig.directory_info["ruleDirectory"], self.main_data.file_info["rule"]["name"])}))
                self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                
                #로그 폴더가 생성 되어있는지 확인후 없으면 폴더를 생성한다.            
                if self.create_directory("%s/%s" % (self.main_data.file_info["directory_path"], zone_row), config.SyncConfig.directory_path["os_type"]) == False:
                    fail_msg = "Router 저장 폴더 생성 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
                #룰 정보 저장
                if self.fileWrite("%s/%s/%s" % (self.main_data.file_info["directory_path"], zone_row, self.main_data.file_info["router"]["name"]), router_data, "forced") == False:
                    fail_msg = "Router 정보 저장 실패.(IP:%s, office:%s)" % (self.main_data.ax_gate_info["ax_gate_ip"], self.main_data.ax_gate_info["office_code"])
                    continue
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("scan_delegate_zone_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "고객/Router 정보 파싱 실패 (%s)" % (str(ex))
        
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("고객/Router 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - DNAT 프로파일중  exclude 일때 정보를 파싱한다
    """
    def exclude_parsing(self, line_info_split):
        access_from_list = self.scan_description_check("source", line_info_split, True)
        incoming_ip_list = self.scan_description_check("destination", line_info_split, True)
        dport_list = self.scan_description_check("dport", line_info_split, True)

        return  access_from_list, incoming_ip_list, dport_list
   
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - 찾고자 하는 값이 존재하는지의 유무를 판단
    """
    def value_check(self, value, line_info_split):
        for line_info_split_value in line_info_split:
            if line_info_split_value.lower() == "label":
                break
            
            elif line_info_split_value.lower() == value.lower():
                return True
        
        return False
        
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - DNAT 프로파일 정보를 파싱한다
    """
    def dnat_profile_parsing(self):
        fail_msg = ""
        e2e_dnat_profile = {}
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("DNAT 프로파일 파싱")
            e2e_msg.set_indent(2)
            
            #sdat proFile정보 추출.
            for dnat_info_row in self.main_data.rule["dnat_profile"]:
                dnat_profile_num = ""   #프로파일 명에 대한 저장 변수
                temp_dnat_profile_list = [] #dnat profile저장을 위한 임시 변수
                
                for line_info in dnat_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip dnat profile 999
                    # - label jhh_dnat_test
                    # - health-check load-balancing 60 30 arp source 1.1.1.1
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx destination xxx.xxx.xxx.xxx/x tcp dport 80 exclude
                    # - source xxx.xxx.xxx.xxx destination any xns-idp port-forwarding xxx.xxx.xxx.xxx port 2233 prio 1
                    # - source xxx.xxx.xxx.xxx destination xxx.xxx.xxx.xxx tcp static xxx.xxx.xxx.xxx prio 2
                    # - source any destination range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx udp dport range 1000 1100 load-balancing xxx.xxx.xxx.xxx weight 100 range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx prio 4
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        #프로파일 이름 저장
                        dnat_profile_num = line_info_split[3]

                    elif line_info_split[0] == "source":
                        #옵션별 정보 처리
                        access_from_list = self.scan_description_check("source", line_info_split, "ip")
                        incoming_ip_list = self.scan_description_check("destination", line_info_split, "ip")
                        destination_port_list = self.scan_description_check("dport", line_info_split, "port")
                        insert_port_list = self.scan_description_check("port", line_info_split, "port")
                        protocol = ""
                        
                        #insert ip 는 옵션에 따라 처리 방식이 다르기 때문에 따로 처리 한다.
                        if self.value_check("exclude", line_info_split) == True:
                            insert_ip = "RETURN"
                             
                        elif self.value_check("port-forwarding", line_info_split) == True:
                            insert_ip = self.scan_description_check("port-forwarding", line_info_split, "ip")
                            protocol = self.scan_description_check("destination", line_info_split, "protocol")
                            
                        elif self.value_check("static", line_info_split) == True:
                            insert_ip = self.scan_description_check("static", line_info_split, "ip")
                            
                        elif self.value_check("load-balancing", line_info_split) == True:
                            insert_ip = self.scan_description_check("load-balancing", line_info_split, "ip")
                            protocol = self.scan_description_check("destination", line_info_split, "protocol")
                            
                        else:
                            case_msg = "처리 방식 지원되지 않음(%s)" % (line_info_split)
                            log.info("%s\n%s" % (case_msg, self.print_list(dnat_info_row)))
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("DP", "", "P", "C", case_msg, self.print_list(dnat_info_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            #log.info("처리 방식이 지정되지 않은 정보 입니다. \n%s" %(dnat_info_row))
                            continue
                        
                        #snat profile 하나에는 여러개의 snat profile존재할 수 있기 때문에 모든 라인 처리후 최종 적으로 데이터를 저장한다.
                        temp_dnat_profile_list.append({"access_from":access_from_list, "incoming_ip":incoming_ip_list, "insert_ip":insert_ip, "destination_port":destination_port_list, "insert_port":insert_port_list, "protocol":protocol})
                
                #dnat profile 하나에 대한 최종 정보 저장 
                self.main_data.temporary_dnat[dnat_profile_num] = {}
                self.main_data.temporary_dnat[dnat_profile_num]["data"] = temp_dnat_profile_list
                self.main_data.temporary_dnat[dnat_profile_num]["original_data"] = self.print_list(dnat_info_row)
                
                e2e_dnat_profile[dnat_profile_num] = {}
                e2e_dnat_profile[dnat_profile_num]["data"] = temp_dnat_profile_list
                e2e_dnat_profile[dnat_profile_num]["original_data"] = [self.print_list(dnat_info_row)]
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("dnat_profile_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "dnat_profile 생성 실패 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            detail_msg = str(e2e_dnat_profile).replace("}],", "}],\n%s\n" % (config.SyncConfig.sub_split)).replace("]},", "]},\n%s\n\n" % (config.SyncConfig.line_split))
            e2e_msg.set_message_detail(detail_msg if fail_msg == "" else fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_dnat if fail_msg == "" else fail_msg)
            #e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - SNAT 프로파일 정보를 파싱한다
    """
    def snat_profile_parsing(self):
        fail_msg = ""
        e2e_snat_profile = {}
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("SNAT 프로파일 파싱")
            e2e_msg.set_indent(2)

            #sdat proFile정보 추출.
            for snat_info_row in self.main_data.rule["snat_profile"]:
                snat_profile_num = ""   #프로파일 명에 대한 저장 변수
                temp_snat_profile_list = [] #snat profile저장을 위한 임시 변수
                
                for line_info in snat_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - ip snat profile 41
                    # - label dki_snat_static
                    # - source xxx.xxx.xxx.xxx destination xxx.xxx.xxx.xxx exclude    -> NAT 타입이 Exclude인 경우 자동으로 prio=0 (룰에는 표시안됨)
                    # - source range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx destination xxx.xxx.xxx.xxx/x dynamic range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx prio 1    -> prio X : 우선순위
                    # - source any destination xxx.xxx.xxx.xxx/x dynamic xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx prio 2
                    # - source xxx.xxx.xxx.xxx/x destination range xxx.xxx.xxx.xxx xxx.xxx.xxx.xxx dynamic auto prio 3
                    # - source xxx.xxx.xxx.xxx destination any static xxx.xxx.xxx.xxx prio 5
                    ######################################################
                    line_info_split = line_info.split(" ")   #정상작인 라인을 스페이스를 구분으로 분리한다.

                    #첫번째 값에 따라 파싱하는 경우에 수를 정의한다.
                    if line_info_split[0] == "ip":
                        #프로파일 이름 저장
                        snat_profile_num = line_info_split[3]
                        
                    elif line_info_split[0] == "source":
                        #옵션별 정보 처리
                        source_ip_list = self.scan_description_check("source", line_info_split, "ip")
                        destination_ip_list = self.scan_description_check("destination", line_info_split, "ip")
                        
                        #insert ip 는 옵션에 따라 처리 방식이 다르기 때문에 따로 처리 한다.
                        if self.value_check("exclude", line_info_split) == True:
                            dynamic_ip = "RETURN"

                        elif self.value_check("static", line_info_split) == True:
                            dynamic_ip = self.scan_description_check("static", line_info_split, "ip")
                            
                        elif self.value_check("dynamic", line_info_split) == True:
                            dynamic_ip = self.scan_description_check("dynamic", line_info_split, "dynamic")
                        
                        else:
                            case_msg = "처리 방식 지원되지 않음(%s)" % (line_info_split)
                            log.info("%s\n%s" % (case_msg, self.print_list(snat_info_row)))
                            #log.info("처리 방식이 지정되지 않은 정보 입니다. \n%s" %(snat_info_row))
                            
                            # 미지원 기능 잠금 (2015.06.02 정현호)
                            self.main_data.set_event_msg_update("SP", "", "P", "C", case_msg, self.print_list(snat_info_row))
                            #self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                            
                        #snat profile 하나에는 여러개의 snat profile존재할 수 있기 때문에 모든 라인 처리후 최종 적으로 데이터를 저장한다.
                        temp_snat_profile_list.append({"source":source_ip_list, "destination":destination_ip_list, "dynamic":dynamic_ip})
                
                #snat profile 하나에 대한 최종 정보 저장 
                self.main_data.temporary_snat[snat_profile_num] = {}
                self.main_data.temporary_snat[snat_profile_num]["data"] = temp_snat_profile_list
                self.main_data.temporary_snat[snat_profile_num]["original_data"] = self.print_list(snat_info_row)
                
                e2e_snat_profile[snat_profile_num] = {}
                e2e_snat_profile[snat_profile_num]["data"] = temp_snat_profile_list
                e2e_snat_profile[snat_profile_num]["original_data"] = [self.print_list(snat_info_row)]
                
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("snat_profile_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "snat_profile 생성 실패 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            detail_msg = str(e2e_snat_profile).replace("}],", "}],\n%s\n" % (config.SyncConfig.sub_split)).replace("]},", "]},\n%s\n\n" % (config.SyncConfig.line_split))
            e2e_msg.set_message_detail(detail_msg if fail_msg == "" else fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_snat if fail_msg == "" else fail_msg)
            #e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
            
        return fail_msg
    
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - VLAN정보를 파싱한다
    """
    def vlan_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("Interface 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))           
            
            #E2E Sub Trace 생성
            e2e_msg_sub = MsgData()
            e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg_sub.set_main_class("Sync Manager")
            e2e_msg_sub.set_sub_class("Rule Sync")
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg_sub.set_start_dttm()
            e2e_msg_sub.set_message("Interface 정보 파싱")
            e2e_msg_sub.set_indent(2)
               
            #메모리에 저장한 router virtual 정보를 파싱한다.
            for interface_info_row in self.main_data.rule["interface"]:
                security_zone = ""
                interface = ""
                ip_address = []
                proxy_arp = []
                temp_bridge_info = {}
                shutdown = ""
                
                #구분한 데이터 정보 확인
                for line_info in interface_info_row:
                    ######################################################
                    #######################파싱 예제########################
                    # - interface eth3.120
                    # - description 고객_틸론_서버
                    # - ip address xxx.xxx.xxx.xxx/x
                    # - ip address xxx.xxx.xxx.xxx/x secondary
                    # - promisc-mode
                    # - security-zone zone120_trust
                    # - no shutdown
                    ######################################################
                    line_info_split = line_info.split(" ")  #정상작인 라인을 스페이스를 구분으로 분리한다.
                    
                    if line_info_split[0] == "security-zone":
                        security_zone = line_info_split[1]
                        
                    elif line_info_split[0] == "interface":
                        interface = line_info_split[1]
                    
                    elif line_info_split[0] == "ip" and line_info_split[1] == "address":
                        ip_address.append(line_info_split[2])
                    
                    elif line_info_split[0] == "ip" and line_info_split[1] == "proxy-arp":
                        #proxy_arp는 ip정보는 사용하지 않고 proxy_arp 적용 여부만 사용한다.
                        #proxy-arp 여러 라인이 존재할 수 있어 값이 없는 경우에만 append 한다.
                        if len(proxy_arp) == 0:
                            proxy_arp.append("ok")
                            
                    #shutdown 인경우에 대한 처리 고민(정현호:2015.05.28)
                    elif line_info_split[0] == "shutdown":
                        shutdown = "on"
                         
                    elif line_info_split[0] == "no" and line_info_split[1] == "shutdown":
                        shutdown = "off"     
                    
                #security-zone 정보 확인
                if security_zone == "":
                    #security-zone 정보가 없을 경우 bridge정보에서 해당 interface로 찾아본다
                    if self.main_data.temporary_bridge.get(interface) != None:
                        if self.main_data.temporary_bridge[interface].get("security-zone") != None:
                            security_zone = self.main_data.temporary_bridge[interface]["security-zone"]
                            
                            #이전에 처리한 bridge정보 처리중 bridge port 정보가 없는 경우 security-zone 으로 interface정보를 임시로 처리했기 때문에 한번더 확인해본다.
                            #if security_zone == "":
                            #    security_zone = self.main_data.temporary_bridge[security_zone]["security-zone"]
                                
                    #bridge 정보에도 interface 정보가 없을 경우 사용할 수 없는 interface 로 판단 해당 정보는 파싱하지 않는다.
                    if security_zone == "":
                        case_msg = "security-zone 정보 확인 불가(%s)" % (interface)
                        log.info("%s\n%s" % (case_msg, self.print_list(interface_info_row)))
                        #log.info("security-zone 정보를 확인할 수 없습니다.\n%s" % (self.print_list(interface_info_row)))
                        
                        self.main_data.set_event_msg_update("IF", 
                                                            self.find_utm_userid(security_zone, self.main_data),
                                                            "C", "S", case_msg, self.print_list(interface_info_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
                
                #ip address 정보 확인
                if len(ip_address) == 0:
                    #ip address 정보가 없을 경우 bridge 정보에서 해당 interface 로 찾아본다
                    if self.main_data.temporary_bridge.get(interface) != None:
                        if self.main_data.temporary_bridge[interface].get("ip") != None:
                            ip_address = self.main_data.temporary_bridge[interface]["ip"]
                            
                            # shutdown 값이 없으면 bridge의 shutdown 값을 가져와서 넣는다.
                            if shutdown == "" :
                                shutdown = self.main_data.temporary_bridge[interface]["shutdown"]
                    
                    #bridge 정보에도 interface 정보가 없을 경우 사용할 수 없는 interface 로 판단 해당 정보는 파싱하지 않는다.
                    if len(ip_address) == 0:
                        case_msg = "ip address 정보 확인 불가(%s)" % (ip_address)
                        log.info("%s\n%s" % (case_msg, self.print_list(interface_info_row)))
                        #log.info("ip address 정보를 확인할 수 없습니다.\n%s" % (self.print_list(interface_info_row)))
                        
                        self.main_data.set_event_msg_update("IF", 
                                                            self.find_utm_userid(security_zone, self.main_data),
                                                            "C", "S", case_msg, self.print_list(interface_info_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
                
                # default shutdown 값을 Setting
                if shutdown == "" :
                    shutdown = "off"
                
                # 대표존 및 security-zone 확인
                if self.main_data.delegate_zone.get(security_zone) != None:
                    zone = self.main_data.delegate_zone.get(security_zone)
                    
                    interface_split = interface.split(".")  #interface : eth3.120
                    
                    if self.main_data.zone.get(zone) != None:
                        
                        # vlan 이 2XXX 번때 이면 eth1 정보이다.
                        if len(interface_split[1]) == 4 and interface_split[1].startswith("2") == True:
                            self.main_data.zone[zone]["network"]["interface"][0]["ip"] = ip_address
                            self.main_data.zone[zone]["network"]["interface"][0]["vlan"] = interface
                            self.main_data.zone[zone]["network"]["interface"][0]["check"] = True
                            self.main_data.zone[zone]["network"]["interface"][0]["proxy"] = proxy_arp
                            self.main_data.zone[zone]["network"]["interface"][0]["shutdown"] = shutdown
    
                        else:
                            eth_start_point = 1 # RED Zone eth0 제외 (2000번대 vlan)
#                             while eth_start_point < len(self.main_data.zone[zone]["network"]["interface"]):
#                                 if self.main_data.zone[zone]["network"]["interface"][eth_start_point]["check"] == False:
#                                     self.main_data.zone[zone]["network"]["interface"][eth_start_point]["ip"] = ip_address
#                                     self.main_data.zone[zone]["network"]["interface"][eth_start_point]["vlan"] = interface
#                                     self.main_data.zone[zone]["network"]["interface"][eth_start_point]["check"] = True
#                                     self.main_data.zone[zone]["network"]["interface"][eth_start_point]["proxy"] = proxy_arp
#      
#                                     if temp_bridge_info.get(str(ip_address)) == None:
#                                         temp_bridge_info[str(ip_address)] = {"vlan":"%s" % (self.main_data.zone[zone]["network"]["interface"][eth_start_point]["vlan_id"]), "proxy":proxy_arp}
#                                     else:
#                                         temp_bridge_info[str(ip_address)]["vlan"] = "%s\n%s" % (temp_bridge_info[str(ip_address)]["vlan_id"], self.main_data.zone[zone]["network"]["interface"][eth_start_point]["vlan_id"]) 
#                                          
#                                         if len(temp_bridge_info[str(ip_address)]["proxy"]) == 0:
#                                             temp_bridge_info[str(ip_address)]["proxy"] = proxy_arp
#                                      
#                                     break
#      
#                                 eth_start_point = eth_start_point + 1
                                
                            # 김복순 박사님의 요청으로 변경 (5월 23일)
                            eth_point = eth_start_point
                            while eth_start_point < len(self.main_data.zone[zone]["network"]["interface"]):
                                if self.main_data.zone[zone]["network"]["interface"][eth_start_point]["check"] == False:
                                    # vlan 라스트가  10x+1의 경우 eth2 정보
                                    if interface_split[1].endswith("1") == True :
                                        eth_point = 1
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["ip"] = ip_address
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["vlan"] = interface
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["check"] = True
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["proxy"] = proxy_arp
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["shutdown"] = shutdown
                                        
                                    # vlan 라스트가 10x의 경우 eth3 정보    
                                    elif interface_split[1].endswith("0") == True :
                                        eth_point = 2
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["ip"] = ip_address
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["vlan"] = interface
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["check"] = True
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["proxy"] = proxy_arp
                                        self.main_data.zone[zone]["network"]["interface"][eth_point]["shutdown"] = shutdown
                                    # 나버지 순서대로 ... eth4, eth5
                                    else :
                                        eth_point = eth_start_point
                                        self.main_data.zone[zone]["network"]["interface"][eth_start_point]["ip"] = ip_address
                                        self.main_data.zone[zone]["network"]["interface"][eth_start_point]["vlan"] = interface
                                        self.main_data.zone[zone]["network"]["interface"][eth_start_point]["check"] = True
                                        self.main_data.zone[zone]["network"]["interface"][eth_start_point]["proxy"] = proxy_arp
                                        self.main_data.zone[zone]["network"]["interface"][eth_start_point]["shutdown"] = shutdown
     
                                    if temp_bridge_info.get(str(ip_address)) == None:
                                        temp_bridge_info[str(ip_address)] = {"vlan":"%s" % (self.main_data.zone[zone]["network"]["interface"][eth_point]["vlan_id"]), "proxy":proxy_arp, "shutdown":shutdown}
                                    else:
                                        temp_bridge_info[str(ip_address)]["vlan"] = "%s\n%s" % (temp_bridge_info[str(ip_address)]["vlan_id"], self.main_data.zone[zone]["network"]["interface"][eth_point]["vlan_id"]) 
                                        
                                        if len(temp_bridge_info[str(ip_address)]["proxy"]) == 0:
                                            temp_bridge_info[str(ip_address)]["proxy"] = proxy_arp
                                        
                                        if len(temp_bridge_info[str(ip_address)]["shutdown"]) == 0:
                                            temp_bridge_info[str(ip_address)]["shutdown"] = shutdown
                                     
                                    break
     
                                eth_start_point = eth_start_point + 1
                        
                        # bridge 정보를 추가하여 준다. e2e trace 목적임.
                        bridge_id = None
                        bridge_proxy = None
                        if self.main_data.temporary_bridge.get(interface) != None :
                            bridge_id       = [self.main_data.temporary_bridge[interface].get("bridge_id")]
                            bridge_proxy    = self.main_data.temporary_bridge[interface].get("proxy")
                        
                        
                        # vlan 정보 저장
                        if self.main_data.temporary_vlan.get(security_zone) != None :
                            # 동일 브릿지에 interface 2개 이상의 경우 "," 구분자로 표기함.(2015.05.19)
                            self.main_data.temporary_vlan[security_zone]["vlan"] = "%s, %s" % (self.main_data.temporary_vlan[security_zone]["vlan"], interface)
                            
                        else :
                            # Bridge e2e Trace 상세정보에 브릿지 정보 추가 필요 (2015.05.19)
                            self.main_data.temporary_vlan[security_zone] = {"vlan": interface,
                                                                            "ip": ip_address,
                                                                            "proxy": proxy_arp if len(proxy_arp) > 0 else (bridge_proxy if bridge_proxy != None else []),
                                                                            "bridge": bridge_id if bridge_id != None else []}
                            
                    else:
                        case_msg = "대표존 정보 없음(%s)" % (zone)
                        log.info("%s\n%s" % (case_msg, self.print_list(interface_info_row)))
                        #log.info("대표존 정보를 확인할 수 없습니다.\n%s" % (self.print_list(interface_info_row)))
                        
                        self.main_data.set_event_msg_update("IF", 
                                                            self.find_utm_userid(security_zone, self.main_data),
                                                            "C", "S", case_msg, self.print_list(interface_info_row))
                        self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                        continue
                else:
                    case_msg = "security zone 정보 없음(%s)" % (security_zone)
                    log.info("%s\n%s" % (case_msg, self.print_list(interface_info_row)))
                    #log.info("security zone 정보를 확인할 수 없습니다.\n%s" % (self.print_list(interface_info_row)))
                    
                    self.main_data.set_event_msg_update("IF", 
                                                        self.find_utm_userid(security_zone, self.main_data),
                                                        "C", "S", case_msg, self.print_list(interface_info_row))
                    self.connect.insert(self.sql.insert_syncmgr_event(self.main_data.event_msg))
                    continue
                
                #interface 정보 bridge 정보를 생성 한다.
                for bridge_id_row in temp_bridge_info:
                    bridge_point = 0
                    while bridge_point < len(self.main_data.zone[zone]["network"]["bridge"]):
                        
                        if len(self.main_data.zone[zone]["network"]["bridge"][bridge_point]) == 0:
                            self.main_data.zone[zone]["network"]["bridge"][bridge_point].append(temp_bridge_info[bridge_id_row]["vlan"])
                            
                            if len(self.main_data.zone[zone]["network"]["proxy-arp"][bridge_point]) == 0:
                                self.main_data.zone[zone]["network"]["proxy-arp"][bridge_point] = temp_bridge_info[bridge_id_row]["proxy"]
                                
                            #IP로 bridge에 해당하는 사항을 찾는다. shutdown값을 넣기위함.
                            #interface가 br로 묶이지 않는 경우는 interface의 shutdown값을 넣어준다. 
                            if len(self.main_data.zone[zone]["network"]["shutdown"][bridge_point]) == 0:
                                # br에서 찾는다.
                                if self.main_data.temporary_bridge.get(bridge_id_row) != None:
                                    self.main_data.zone[zone]["network"]["shutdown"][bridge_point] = self.main_data.temporary_bridge[bridge_id_row]["shutdown"]
                                
                                # interface에서 찾아 넣는다.
                                else :
                                    self.main_data.zone[zone]["network"]["shutdown"][bridge_point] = temp_bridge_info[bridge_id_row]["shutdown"]
                            
                            break
                        
                        bridge_point = bridge_point + 1
                
                #브릿지 정보 보정(운용자의 요청으로 vlan 값의 마지막 값이 1로 끝나는 경우 br0에,  0으로 끝나는 경우 br1으로 설정해 달라는 요청이 들어왔음)
                bridge_point_check = 0
                while bridge_point_check < len(self.main_data.zone[zone]["network"]["bridge"]): #3번
                    for eth_row in self.main_data.zone[zone]["network"]["bridge"][bridge_point_check]:
                        vlan_id_point = 1
                        while vlan_id_point < len(self.main_data.zone[zone]["network"]["interface"]):
                            if self.main_data.zone[zone]["network"]["interface"][vlan_id_point]["vlan_id"] == eth_row:
                                if eth_row != "eth1" and str(self.main_data.zone[zone]["network"]["interface"][vlan_id_point]["vlan"]).endswith("0") == True:
                                    #bridge 정보를 보정한다.
                                    temp_bridge_point = self.main_data.zone[zone]["network"]["bridge"][1]
                                    self.main_data.zone[zone]["network"]["bridge"][1] = self.main_data.zone[zone]["network"]["bridge"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["bridge"][bridge_point_check] = temp_bridge_point
                                       
                                    #proxy_arp 정보를 보정한다.(bridge정보와 동일하게 움직인다.)
                                    temp_proxy_arp_point = self.main_data.zone[zone]["network"]["proxy-arp"][1]
                                    self.main_data.zone[zone]["network"]["proxy-arp"][1] = self.main_data.zone[zone]["network"]["proxy-arp"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["proxy-arp"][bridge_point_check] = temp_proxy_arp_point
                                    
                                    #shutdown 정보를 보정한다.(bridge정보와 동일하게 움직인다.)
                                    temp_shutdown_point = self.main_data.zone[zone]["network"]["shutdown"][1]
                                    self.main_data.zone[zone]["network"]["shutdown"][1] = self.main_data.zone[zone]["network"]["shutdown"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["shutdown"][bridge_point_check] = temp_shutdown_point
                                    break
                                   
                                if eth_row != "eth1" and str(self.main_data.zone[zone]["network"]["interface"][vlan_id_point]["vlan"]).endswith("1") == True:
                                    #bridge 정보를 보정한다.
                                    temp_bridge_point = self.main_data.zone[zone]["network"]["bridge"][0]
                                    self.main_data.zone[zone]["network"]["bridge"][0] = self.main_data.zone[zone]["network"]["bridge"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["bridge"][bridge_point_check] = temp_bridge_point
                                        
                                    #proxy_arp 정보를 보정한다.(bridge정보와 동일하게 움직인다.)
                                    temp_proxy_arp_point = self.main_data.zone[zone]["network"]["proxy-arp"][0]
                                    self.main_data.zone[zone]["network"]["proxy-arp"][0] = self.main_data.zone[zone]["network"]["bridge"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["proxy-arp"][bridge_point_check] = temp_proxy_arp_point
                                    
                                    #shutdown 정보를 보정한다.(bridge정보와 동일하게 움직인다.)
                                    temp_shutdown_point = self.main_data.zone[zone]["network"]["shutdown"][0]
                                    self.main_data.zone[zone]["network"]["shutdown"][0] = self.main_data.zone[zone]["network"]["shutdown"][bridge_point_check]
                                    self.main_data.zone[zone]["network"]["shutdown"][bridge_point_check] = temp_shutdown_point
                                    break
                                   
                            vlan_id_point = vlan_id_point + 1
                               
                    bridge_point_check = bridge_point_check + 1
                    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("vlan_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "Interface 정보 파싱 실패 (%s)" % (str(ex))
            
        finally:
            #E2E Sub Trace 생성    
            e2e_msg_sub.set_end_dttm()
            e2e_msg_sub.set_response_status(fail_msg)
            e2e_msg_sub.set_message_detail(str(self.main_data.temporary_vlan).replace("]},", "]},\n%s\n" % (config.SyncConfig.line_split)) if fail_msg == "" else fail_msg)
            #e2e_msg_sub.set_message_detail(self.main_data.temporary_vlan if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
            
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("Interface 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            #e2e_msg.set_message_detail(self.main_data.temporary_vlan if fail_msg == "" else fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
        
        return fail_msg
            
    """
    @Created - 2015. 04. 22.
    @author - 정현성
    @description - PUBLIC IP 정보를 파싱한다.
    """
    def public_ip_parsing(self):
        fail_msg = ""
        
        try:
            #E2E Trace 생성
            e2e_msg = MsgData()
            e2e_msg.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
            e2e_msg.set_orgseq (self.main_data.ax_gate_info["org_seq"])
            e2e_msg.set_main_class("Sync Manager")
            e2e_msg.set_sub_class("Rule Sync")
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
            e2e_msg.set_start_dttm()
            e2e_msg.set_message("Public IP 정보 파싱 시작")
            e2e_msg.set_indent(1)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))

            for zone_row in self.main_data.zone:
                e2e_public_ip_list = []
                
                for interface_row in self.main_data.zone[zone_row]["network"]["interface"]:
                    if interface_row.get("vlan_id") != "eth1" and interface_row.get("check") == True:
                        for ip_row in interface_row.get("ip"):
                            
                            vlan_ip_split = ip_row.split(".")
                            #공인아이피 체크를 위해서는 C클래스까지 체크가 가능하여 마지막 값은 0으로 만든다.
                            ip_info = "%s.%s.%s.%s" % (str(vlan_ip_split[0]), str(vlan_ip_split[1]), str(vlan_ip_split[2]), "0")
                            
                            #PUBLIC, PRIVATE
                            if IP(ip_info).iptype() == "PUBLIC":
                                snat_public_info = "%s,%s,%s,%s,%s,%s,%s,%s,%s,%s" % (
                                "on",
                                "",
                                ip_row,
                                "",
                                "",
                                "UPLINK:ANY",
                                "RETURN",
                                "",
                                "",
                                "RETURN")
                                
                                #SNAT 정보에 추가한다.
                                #self.main_data.zone[zone_row]["snat"].append(snat_public_info)
                                
                                e2e_public_ip_list.append(snat_public_info)
                                
                                #E2E Trace 생성
#                                 e2e_msg_sub = MsgData()
#                                 e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
#                                 e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
#                                 e2e_msg_sub.set_main_class("Sync Manager")
#                                 e2e_msg_sub.set_sub_class("Rule Sync")
#                                 e2e_msg_sub.set_response_status(fail_msg)
#                                 e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
#                                 e2e_msg_sub.set_start_dttm()
#                                 e2e_msg_sub.set_end_dttm()
#                                 e2e_msg_sub.set_indent(2)
#                                 e2e_msg_sub.set_message("Public IP 정보 SNAT 추가(%s)" % (zone_row))
#                                 e2e_msg_sub.set_message_detail(snat_public_info)
#                                 self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                                
                if len(e2e_public_ip_list) > 0:
                    #E2E Trace 생성
                    e2e_msg_sub = MsgData()
                    e2e_msg_sub.set_sysnc_seq(self.main_data.ax_gate_info["trace_seq"])
                    e2e_msg_sub.set_orgseq (self.main_data.ax_gate_info["org_seq"])
                    e2e_msg_sub.set_main_class("Sync Manager")
                    e2e_msg_sub.set_sub_class("Rule Sync")
                    e2e_msg_sub.set_response_status(fail_msg)
                    e2e_msg_sub.set_axgate_job(self.main_data.ax_gate_info["ax_gate_job"])
                    e2e_msg_sub.set_start_dttm()
                    e2e_msg_sub.set_end_dttm()
                    e2e_msg_sub.set_indent(2)
                    e2e_msg_sub.set_message("Public IP 정보 SNAT 추가(%s)" % (zone_row))
                    e2e_msg_sub.set_message_detail(self.print_list(e2e_public_ip_list))
                    self.connect.insert(self.sql.insert_e2e_trace(e2e_msg_sub))
                    
        except Exception as ex:
            log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("public_ip_parsing", str(type(ex)), str(ex.args), str(ex)))
            fail_msg = "Public IP 정보 파싱 (%s)" % (str(ex))
        finally:
            #E2E Trace 결과 설정
            e2e_msg.set_end_dttm()
            e2e_msg.set_response_status(fail_msg)
            e2e_msg.set_message("Public IP 정보 파싱 종료")
            e2e_msg.set_message_detail(fail_msg)
            self.connect.insert(self.sql.insert_e2e_trace(e2e_msg))
        
        return fail_msg
    
        """
    @Created - 2015. 05. 18.
    @author -  김도영
    @description - security_zone 으로 utm_userid를 만들어 준다. (zone Name + "_"+ IP(XXX_XXX_XXX_XXX) 
    """
    def find_utm_userid(self, security_zone_name, main_data):
        
        utm_userid = ""
        if security_zone_name != None and security_zone_name != "" and self.main_data.delegate_zone.get(security_zone_name) != None :
            utm_userid = "%s_%s" % (self.main_data.delegate_zone.get(security_zone_name), str(self.main_data.ax_gate_info["ax_gate_ip"]).replace(".", "_") )
        
        return utm_userid
    
    """
    @Created - 2015. 05. 18.
    @author -  김도영
    @description - shutdown 값을 status 값으로 변환한다. ( on -> off, off -> on)
    """
    def convert_shutdown2status(self, checkValue, shutdown ):
        status = ""
        if checkValue != None and len(checkValue) > 0 :
            status = "on"
            if shutdown == "on":
                status = "off"
                
        return status