#-*- coding: utf-8 -*-
###################################################################
# author : 정현성
# Date : 2015.4.20
# Description : 로그 Print
# 수정자    변경일시              내용
# 김도영    2015.05.28    등급 및 Level 조정
###################################################################
import datetime
import config.SyncConfig

class Logger(object):
    
    def __init__(self, logLevel=None):
        if logLevel == None :
            self.logLevel = config.SyncConfig.logLevel
        else :
            self.logLevel = logLevel
    
    def set_logLevel(self, logLevel):
        self.logLevel = logLevel
    
    # 디버그보다 더 상세한 정보
    def trace(self, log_data):
        if self.logLevel > 50 :
            self.print_log(log_data, "T")
     
    # 내부 실행 상황을 추적해 볼 수 있는 상세 정보    
    def debug(self, log_data):
        if self.logLevel >= 50 :
            self.print_log(log_data, "D")
     
    # 주요 실행정보
    def info(self, log_data):
        if self.logLevel >= 40 :
            self.print_log(log_data, "I")
     
    # 주요 실행정보 중 반드시 알려야 하는 상태
    def noti(self, log_data):
        if self.logLevel >= 30 :
            self.print_log(log_data, "N")
     
    # 잠재적인 위험을 안고 있는 상태
    def warn(self, log_data):
        if self.logLevel >= 20 :
            self.print_log(log_data, "W")
     
    # 오류가 발생 하였지만 애플리케이션은 계속 실행할 수 있는 상태 
    def error(self, log_data):
        if self.logLevel >= 10 :
            self.print_log(log_data, "E")
     
    # 애플리케이션을 중지해야 하는 심각한 오류
    def fatal(self, log_data):
        if self.logLevel >= 0 :
            self.print_log(log_data, "F")
        
    def print_log(self, log_data, log_type):
        print "%s[%s] %s" % (datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S %f"), log_type, log_data)
