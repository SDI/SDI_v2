#-*- coding: utf-8 -*-
'''
Created on 2015. 3. 6.

@author: 정현성
'''
import datetime

class MsgData():
    sync_seq = "null"
    main_class = "null"   
    sub_class = "null"
    axgate_job = "null"
    thread_job = "null"
    provision_seq = None
    message = None
    request_cmd = None
    request_parameter = None
    execute_time = None
    response = None
    response_status = None
    reg_dttm = None
    start_dttm = None
    end_dttm = None
    message_detail = None
    rule_file_name = None 
    indent = None 
    utm_userid = None 
    parsing_type = None # 1:DHCP, 2:DNAT, 3:SNAT, 4:FIREWALL, 5:ROUTER
    orgseq = None
    parsing_cnt = None
    
    def msg_clear(self):
        self.provision_seq = None
        self.message = None
        self.request_cmd = None
        self.request_parameter = None
        self.execute_time = None
        self.response = None
        self.response_status = None
        self.reg_dttm = None
        self.start_dttm = None
        self.end_dttm = None
        self.message_detail = None
        self.rule_file_name = None 
        self.indent = None 
        self.utm_userid = None 
        self.parsing_type = None
        self.orgseq = None
        self.parsing_cnt = None
    
    def set_sysnc_seq(self, value):
        self.sync_seq = value
    
    def set_main_class(self, value):
        self.main_class = "'%s'" % (value)
        
    def set_sub_class(self, value):
        self.sub_class = "'%s'" % (value)
    
    def set_axgate_job(self, value):
        self.axgate_job = "'%s'" % (value)
    
    def set_thread_job(self, value):
        self.thread_job = "'%s'" % (value)
    
    def set_provision_seq(self, value):
        self.provision_seq = value
    
    def set_message(self, value):
        self.message = value
    
    def set_request_cmd(self, value):
        self.request_cmd = value
    
    def set_request_parameter(self, value):
        self.request_parameter = value
    
    def set_response(self, value):
        self.response = value
    
    def set_response_status(self, value):
        self.response_status = value
    
    #시작 시간 설정
    def set_start_dttm(self):
        self.start_dttm = """to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss')""" % (str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%SS")))
    
    #종료 시간 설정
    def set_end_dttm(self):
        self.end_dttm = """to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss')""" % (str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%SS")))
        
        if self.start_dttm != None:
            self.execute_time = """%s - %s""" % (self.end_dttm, self.start_dttm)
    
    def set_message_detail(self, value):
        self.message_detail = value
    
    def set_rule_file_name(self, value):
        self.rule_file_name = value
    
    def set_indent(self, value):
        self.indent = value
    
    def set_utm_userid(self, value):
        self.utm_userid = value
    
    def set_parsing_type(self, value):
        self.parsing_type = value
    
    def set_orgseq(self, value):
        self.orgseq = value
    
    def set_parsing_cnt(self, value):
        self.parsing_cnt = value

if __name__ == '__main__':
    pass