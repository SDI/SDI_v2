#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@created: 2015. 2. 12.
@summary:
@author: 정현성
'''

import base64

from Crypto import Random
from Crypto.Cipher import AES


class AESCipher(object):
    """
    AES 암호화
    """

    def __init__( self, key ):
        self.key = key

    def encrypt( self, raw ):
        BS = 16
        pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
        raw = pad(raw)
        iv = Random.new().read( AES.block_size )
        cipher = AES.new( self.key, AES.MODE_CBC, iv )
        return base64.b64encode( iv + cipher.encrypt( raw ) )

    def decrypt( self, enc ):
        unpad = lambda s : s[:-ord(s[len(s)-1:])]
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv )
        return unpad(cipher.decrypt( enc[16:] ))


if __name__ == '__main__':
    data = "211.224.204.172"
    #print data
    aes = AESCipher('Qfkrkstkghkwhgdk')
    print aes.encrypt("utmVm!234")
    print aes.encrypt("218.144.90.62")
    print aes.encrypt("10.0.0.71:8080")
    print aes.encrypt("211.224.204.160")

    #encstr = aes.encrypt(data)
    #print encstr
#     encstr = aes.encrypt('211.224.204.158')
#     print encstr
#     encstr = aes.encrypt('5432')
#     print encstr
#     encstr = aes.encrypt('orchestrator')
#     print encstr
#     encstr = aes.encrypt('orch!234')
#     print encstr

    #decstr = aes.decrypt(encstr)
    #print decstr