#-*- coding: utf-8 -*-
'''
Created on 2015-02-08
@author: 김도영
@summary: MSS 연동 관련
    testScheduled - 테스트용 스케줄
'''
import datetime
import json

import config.SyncConfig
from pgSql.SyncManagerSql import SyncManagerSql
from scheduler.AXGateData import AXGateData
from scheduler.AXGateThread import AXGateThread
from scheduler.DBAccess import DBAccess
from scheduler.HttpClient import HttpClient
from util.Logger import Logger
from util.MsgData import MsgData
from util.aescipher import AESCipher
import sys

log = Logger()

def print_list(self, value):
    data = ""
    
    try:
        for value_row in value:
            
            if data == "":
                data = "%s" % (value_row)
            else:
                data = "%s\n%s" % (data, value_row)
                
    except Exception as ex:
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("print_list", str(type(ex)), str(ex.args), str(ex)))
        data = "생성 실패 (%s)" % (str(ex))

    return data
    
def test_mss_linkage(arg_caller, arg_orgseq):
    
    log.info(config.SyncConfig.line_split)
    log.info("MSS Linkage Start Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
    
    try:        
        #기준 시간 설정(파일이름 기준 시간 설정값)
        file_name_time = str(datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
        
        #DB커넥션 연결.
        connect = DBAccess()
        sql = SyncManagerSql()

        #SyncSeq 값을 가져온다
        trace_seq = get_sync_seq(connect, sql)              
        
        #SyncSeq 값을 가져오지 못하면 프로그램을 종료 한다
        if trace_seq == None:
            return
        
        #MSS연동
        result = mss_linkage(connect, sql, trace_seq, arg_orgseq)
        
        #MSS연동에 실패한 경우 종료 한다
        if result == None:
            return
        
    except Exception as ex :
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("ax_gate_rule_scheduled", str(type(ex)), str(ex.args), str(ex)))
        
    finally:
        pass
    
    log.info("SyncManager End Parameters[Caller:%s, orgseq:%s]" % (arg_caller, arg_orgseq))
    log.info(config.SyncConfig.line_split)

def mss_linkage(connect, sql, trace_max_count, arg_orgseq):
    
    fail_msg    = ""   #연동오류 혹은 문제 발생시 실패에 대한 메시지를 담기 위한 변수 선언
    
    try:
        log.info("MSS연동 시작")
        
        #연동 기준 시간 설정(나중에 연동 기준 시간으로 UPDATE되지 않은 AXGate, 고객사는 삭제시키기 위함)
        eventTime = str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))
        
        #MSS연동을 위한 RestClient 객체생성
        client = HttpClient()
        
        #국사 정보 Select(NFC에서 관리되고 있는 국사 코드를 MSS에 연동하여 해당 국사에 고객 정보를 요청한다)
        data = "%s officecode IS NOT NULL AND pop_yn = 'Y'" % ("" if arg_orgseq == "" else "orgseq = %s" % (arg_orgseq))
        db_officeList = connect.select(sql.select_office_list(arg_orgseq))
        
        # AX Gate의 국사 정보가 존재하지 않는다. EEROR
        detail_msg  = "MSS연동 AXGate 국사 조회 결과 : (%s)" % (len(db_officeList))
        
        if len(db_officeList) < 1 :
            fail_msg = "MSS연동 AXGate 국사 조회 결과 없음"
            detail_msg  = fail_msg
            return fail_msg
        
        log.info(detail_msg)    
         
        tot_deleteCnt = 0   #총건수 저장위함.
        office_axgate_info = {} # officecode[axgateip[zoneid[companyname]]]
        
        if fail_msg == "" : 
            #국사별 고객 정보 MSS연동
            for db_officeRow in db_officeList:
                fail_msg = ""    #초기화
                
                #파라미터 생성(국사 코드로 연동한다)
                db_officecode   = db_officeList[db_officeRow].get("officecode")
                db_orgname      = db_officeList[db_officeRow].get("orgname")
                db_orgseq       = db_officeList[db_officeRow].get("orgseq")
                data = "office_code=" + db_officecode
    
                #MSS연동 시작
                redata = client.send(data, config.SyncConfig.mss_linkage_info["url"], config.SyncConfig.mss_linkage_info["httpMethod"], "http://%s" % (config.SyncConfig.mss_linkage_info["host"]))
                
                #MSS연동 결과 분석
                if redata.get("result") != "fail":
                    
                    #MSS연동 성공 처리
                    for mss_officeRow in redata:
                        
                        for mss_companyList in redata[mss_officeRow]:
                            ax_gate_ip  = ""
                            zone_id     = ""
                            company_name= ""
                            fail_msg = ""
                            
                            for mss_companyRow in mss_companyList:
                                if mss_companyRow.get("ax_gate_ip") != None:
                                    ax_gate_ip = mss_companyRow.get("ax_gate_ip")#.encode('utf-8')
                                    
                                if mss_companyRow.get("zone_id") != None:
                                    zone_id = mss_companyRow.get("zone_id")#.encode('utf-8')
                                    
                                if mss_companyRow.get("company_name") != None:
                                    company_name = mss_companyRow.get("company_name")#.encode('utf-8')
                            # Loop End mss_companyRow
                            
                            if ax_gate_ip == "" or len(ax_gate_ip) != 15: # AXGate IP 이상함.
                                fail_msg = "장치 IP 이상"
                                continue
                                
                            if zone_id == "" : # zone_id 이상.
                                fail_msg = "zone_id 이상"
                                continue
                            
                            if office_axgate_info.get(mss_officeRow) == None :
                                office_axgate_info[mss_officeRow] = {}
                            
                            if office_axgate_info[mss_officeRow].get(ax_gate_ip) == None :
                                office_axgate_info[mss_officeRow][ax_gate_ip] = {}
                            
                            if office_axgate_info[mss_officeRow][ax_gate_ip].get(zone_id) == None :
                                office_axgate_info[mss_officeRow][ax_gate_ip][zone_id] = {"company_name":company_name}
                            else :
                                temp_data = office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"]
                                office_axgate_info[mss_officeRow][ax_gate_ip][zone_id]["company_name"] = temp_data +", "+ company_name
                            
                            # officecode, ax_gate_ip, zone_id, company_name("," 구분 멀티), utm_mappid 저장

                        # Loop End mss_companyList
                    # Loop End mss_officeRow
                else:
                    #MSS연동 실패 처리
                    fail_msg = "MSS 연동 실패"
                
        # End if fail_msg
        print "office_axgate_info " 
        print json.dumps(office_axgate_info)
        
    except Exception as ex:
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("mss_linkage", str(type(ex)), str(ex.args), str(ex)))
        fail_msg = str(ex)
    finally:
        pass
    
    return "" if fail_msg == "" else None

def get_sync_seq(connect, sql):
    
    try:
        #syncseq 값 Select
        dic_trace_count = connect.select(sql.select_syncmgr_trace_max())
        
        trace_seq = 0
        
        #Trace가 최초 시작일때 값이 없으므로 1로 시작할 수 있도록 예외 처리한다.
        if dic_trace_count[0].get("max_count") == None:
            trace_seq = 1
        else:
            trace_seq = dic_trace_count[0].get("max_count")

    except Exception as ex :
        log.error("\t%s - ex : \n\t%s\n\t%s\n\t%s" % ("get_sync_seq", str(type(ex)), str(ex.args), str(ex)))
        trace_seq = None
        
    return trace_seq
            
def config_decoding():
    aes = AESCipher(config.SyncConfig.security_key)
    config.SyncConfig.postgresql_db_info["db_host"] = aes.decrypt(config.SyncConfig.postgresql_db_info["db_host"])
    config.SyncConfig.postgresql_db_info["id"] = aes.decrypt(config.SyncConfig.postgresql_db_info["id"])
    config.SyncConfig.postgresql_db_info["pw"] = aes.decrypt(config.SyncConfig.postgresql_db_info["pw"])
    
    config.SyncConfig.mss_linkage_info["host"] = aes.decrypt(config.SyncConfig.mss_linkage_info["host"])
    
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["id"])
    config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"] = aes.decrypt(config.SyncConfig.ax_gate_info["ax_gate_server_info"]["password"])
    
    config.SyncConfig.orchestrator_server_info["info"]["ip"] = aes.decrypt(config.SyncConfig.orchestrator_server_info["info"]["ip"])
    
    config.SyncConfig.utm_password = aes.decrypt(config.SyncConfig.utm_password)
    
if __name__ == '__main__':
    
    arg_caller = "System";
    arg_orgseq = "";
    
    if len(sys.argv) == 2 :
        arg_caller = sys.argv[1]   # caller
    
    if len(sys.argv) == 3 :
        arg_orgseq = sys.argv[2]   # orgseq
        
    #암호화된 정보 복호화
    config_decoding()
    
    test_mss_linkage(arg_caller, arg_orgseq)
    

    