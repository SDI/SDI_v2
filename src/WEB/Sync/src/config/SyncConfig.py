# -*- coding: utf-8 -*-
###################################################################
# author : 정현성
# Date : 2015.2.10
# Description : 설정 데이터
###################################################################

from sdi_config import SDI_Web_Config
cfg = SDI_Web_Config("../config/web.conf")

# NGKIM: test print>> check by using --help option
#print "db= %s" % cfg.get("database_web", "db"),

#Run 장비 Real Or Local 구분
isReal = True  # 운영[True], DEV[False]
logLevel = 40  # 60[Trace], 50[Debug], 40[Info], 30[Noti], 20[Warn], 10[Error], 0[Fatal], 만일 -값 이면 로그 하나도 안찍음.

sub_split  = "-------------------------------------------------------"
line_split = "======================================================="

###Staging
#postgresql DB 정보들(암호화 상태)

db_host=cfg.get("database_sync", "db_host_fake")
src_path=cfg.get("sync_manager", "src_path_fake")
os_type=cfg.get("sync_manager", "os_type_fake")

if isReal == True:
	db_host 	= cfg.get("database_sync", "db_host")
	src_path=cfg.get("sync_manager", "src_path")
	os_type=cfg.get("sync_manager", "os_type")	
	
# DB 접속 정보.
postgresql_db_info = {
    "db_host": db_host,
    "id"     : cfg.get("database_sync", "id"),  #(orchestrator)
    "pw"     : cfg.get("database_sync", "pw"),  #(orch!234)
    "db"     : cfg.get("database_sync", "db"),
    "port"   : cfg.get("database_sync", "port"),
    "tag"    : cfg.get("database_sync", "tag")
}

# 기본 폴더
directory_path = {
    "path"      : src_path,
    "os_type"   : os_type
}

# DB 접속 정보.
mss_linkage_info = {
    "host"			: cfg.get("mss", "host"),
    "url"        	: cfg.get("mss", "url"),
    "httpMethod" 	: cfg.get("mss", "method")
}

#디렉터리 정보.
directory_info = {
    "logDirectory"      : "orchestrator",
    "rootDirectory"     : "rulecollector",
    "ruleDirectory"     : "runFull"
}

#파일 이름 정보.
file_name_info = {
    "runFull"       : "XGateShowRunFull.log",
    "dhcp"          : "dhcp_converted.log",
    "dNat"          : "dnat_converted.log",
    "sNat"          : "snat_converted.log",
    "firewall"      : "firewall_converted.log",
    "fw_outgoing"   : "outgoing_converted.log",
    "fw_incoming"   : "incoming_converted.log",
    "fw_interzone"  : "interznoe_converted.log",
    "router"        : "router_converted.log"
}

#AXGate 접속 정보
ax_gate_info = {
    "ax_gate_server_info"  : {
            "ssh_access_path"   : cfg.get("axgate", "ssh_access_path"),
            "id"                : cfg.get("axgate", "id"),
            "password"          : cfg.get("axgate", "password"),
            "port"              : cfg.get("axgate", "port")}    #(axroot, Admin12#$)
}

#생성되는 모든 UTM의 비밀번호이다.(주의!! 아래 비밀번호 수정시 생성되어 있는 모든 UTM의 비밀번호를 수정하거나 기존에 생성된 UTM을 삭제해야한다)
# 소스상에서 처리하는 부분 : 고객사id + 패스워드로 변경됨 (2015.05.22)
utm_password = cfg.get("vUTM", "password")
security_key = cfg.get("key", "key")

#orchestrator URL
orchestrator_server_info = {
    "info"  : {"ip": cfg.get("orchestrator", "ip") if isReal == True else cfg.get("orchestrator", "ip_fake"),
               "url":{"create_userid"       :"/soap2/service/checkuser", 
                      "get_provisions_seq"  :"/soap2/service/provisions/ready",
                      "create_provisions"   :"/soap2/service/provisions",
                      "delete_utm"          :"/soap2/service/provisions/",
                      "security_group"      :"/soap2/openstack/changesecuritygroup"}}
}

# Case message
case_type_info = {
    "module" : {"AA":"AXGate ACCESS",
                "ZO":"ZONE", "BR":"BRIDGE", "IF":"INTERFACE",
                "SG":"SERVICE GROUP", "IG":"IP GROUP",
                "SP":"SNAT PROFILE", "DP":"DNAT PROFILE",
                "DH":"DHCP", "DN":"DNAT", "SN":"SNAT", "RO":"ROUTER",
                "FW":"FIREWALL", "FI":"FW[INCOMING]", "FO":"FW[OUTGOING]", "FZ":"FW[INTERZONE]",
                "PO":"PROVISION"},
    "case" : {"C":"명명규칙 위반", "P":"미지원 기능", "U":"UTM 생성 오류", "S":"System 오류"},
    "process" : {"S":"Stop", "C":"Continue"}
    
}

#임시? 로직으로 김복순 박사님의 요청으로 추가되었음 (대전연구소에서 사용)
#계속 유지된다면.. 보안성 검토시 암호화로 바꿔야 하는 부분... --> 제거 필요 (소스부분까지)
testing_secondary_router = "on,,10.0.0.0/24,192.168.10.1,test_routing,,,,,,,"