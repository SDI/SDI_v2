#-*- coding: utf-8 -*-
'''
@project: 인스턴스 설정 및 테스트를 담당 하는 mgmt-vm v2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''
import os, sys, json, time, uuid
import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.options
import paramiko

from config.connect_config import utm_conn_info, AESCipherKey
from util.aescipher import AESCipher
from util.openstackclient import NovaClient
from sql.provisioningsql import Provisioningsql
from helper.psycopg_helper import PsycopgHelper
from helper.logHelper import myLogger

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='utmconfig_utmtest', logdir='./log', loglevel='debug', logConsole=True).get_instance()

class SyncmanagerHandler(tornado.web.RequestHandler, PsycopgHelper):
    def post(self):       
        reqdata = json.loads(self.request.body)   
        result = self.configfileupload(reqdata)
        self.write(result)

    def configfileupload(self, reqdata):       
        try:
            aes = AESCipher(AESCipherKey['key'])
                        
            test = Testutils()
            log.info("----------Syncmanagerhandler start")
            self.init_config_from_dict_config_file
            sql = Provisioningsql()
                                              
            orgseq = reqdata["orgseq"]
            vmid = reqdata["vmid"]
            userid = reqdata["userid"]
            password = reqdata["password"]
            localFile = reqdata["localfile"]
            remoteFile = reqdata["remotefile"]
            jobcontrol = reqdata["jobcontrol"]
            
            log.debug("orgseq-> %s"%orgseq)
            log.debug("vmid-> %s"%vmid)
            log.debug("userid-> %s"%userid)
#             log.debug("password-> %s"%password)
            log.debug("localFile-> %s"%localFile)
            log.debug("remoteFile-> %s"%remoteFile)
            log.debug("jobcontrol-> %s"%jobcontrol)
            
            
            if len(jobcontrol) == 0:
                raise Exception("jobcontrol is none")    
            
            log.info("jobcontrol --> %s" %jobcontrol)

            log.debug("[%s]-----configfileupload "%userid)
            
            query = sql.getopenstackinfra(orgseq)
            rows = self.execute_getone(query)
            
            
            self.authurl = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["token_url"])
            self.host = aes.decrypt(rows[0]["infraprops"]["openstack_connect"]["host"])  
            
            nova = NovaClient()
            nova.authenticate(self.host, userid, userid, password, self.authurl)
            server = nova.getServer(vmid)
            
            ipAddress = server["server"]["addresses"]["global_mgmt_net"][0]["addr"]                        
           
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())


            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            
            client.connect(hostname=ipAddress, username=user, password=password)
            sftp = client.open_sftp()
            log.debug("sftp.put now %s:%s->%s" %(ipAddress, localFile, remoteFile))
            sftp.put(localFile, remoteFile)

            cmd = "chown nobody:nobody %s"%remoteFile

            stdin, stdout, stderr = client.exec_command(cmd)
            log.info("sendcommand --> %s" %cmd)

#             result_networkupdate = test.setnetworkconfig(ipAddress, "jobcontrol restart setsnat")           
            result_networkupdate = test.setnetworkconfig(ipAddress, jobcontrol)
            print result_networkupdate
#             time.sleep(1)                                        
#             result_updatelinks = test.setnetworkconfig(ipAddress, " jobcontrol restart setdnat")
#             print result_updatelinks
#             time.sleep(1)                                        
#             result_updatelinks = test.setnetworkconfig(ipAddress, " jobcontrol request dhcp.updatewizard")
#             print result_updatelinks
                                        
            result = {"result":"success","description":result_networkupdate}
        except Exception, e:
            sftp.close()
            client.close()
#             os.remove(localFile)
            log.error("uploadFile exception  %s" %str(e))
            result = {"result":"fail","description":e.message}

        sftp.close()
        client.close()
        
        return result      
        
class VnfmanagerConfigfileHhandler(tornado.web.RequestHandler):
    def post(self):
        print self.request.body
        reqdata = json.loads(self.request.body)   
        result = self.setconfig(reqdata)
        self.write(result)
    
    def setconfig(self, reqdata):
        try:
            log.info("----------Vnfmanagerconfigfilehandler start")
            mgmtnetaddr = reqdata["configinfo"]["redinfo"]["mgmtnetaddr"]
            test = Testutils()

            result, cmd, out = test.islistenport("22", mgmtnetaddr)
            if result == False:
                log.error(u"22번 포트가 열려있지 않습니다.")
                return {"result":"fail", "message":u"22번 포트가 열려있지 않습니다.", "cmd": cmd, "detail": out}
            else :
                log.info(u"22번 포트가 정상적으로 열려있습니다.")
    
            result, cmd, out = test.islistenport("10443", mgmtnetaddr)
            if result == False:
                log.error(u"10443번 포트가 열려있지 않습니다.")
                return {"result":"fail", "message":u"10443번 포트가 열려있지 않습니다.", "cmd": cmd, "detail": out}
            else :
                log.info(u"10443번 포트가 정상적으로 열려있습니다.")            

  
            mainConfingFile, mainconfig = self.createmainconffile(reqdata)
            
            ethernetConfigFile, ethernetconfig = self.createethernetconffile(reqdata)
            br0ConfigFile, br0config = self.createbr0conffile(reqdata)
            br1ConfigFile, br1config = self.createbr1conffile(reqdata)
            br2ConfigFile, br2config = self.createbr2conffile(reqdata)
            routerConfigFile, routerconfig = self.createrouterconffile(reqdata)
            
            if len(reqdata["configinfo"]["hostname"]) > 0:
                hostnameConfigFile, hostname = self.createhostnameconffile(reqdata["configinfo"]["hostname"], reqdata["configinfo"]["domainname"])
                test.uploadfile(mgmtnetaddr, hostnameConfigFile, "/var/efw/host/settings")
                result_updatelinks = test.setnetworkconfig(mgmtnetaddr, " jobcontrol request hostname.updatewizard")
                print result_updatelinks           
                
            test.uploadfile(mgmtnetaddr, mainConfingFile, "/var/efw/uplinks/main/settings")
            test.uploadfile(mgmtnetaddr, ethernetConfigFile, "/var/efw/ethernet/settings")
            test.uploadfile(mgmtnetaddr, br0ConfigFile, "/var/efw/ethernet/br0")
            test.uploadfile(mgmtnetaddr, br1ConfigFile, "/var/efw/ethernet/br1")
            test.uploadfile(mgmtnetaddr, br2ConfigFile, "/var/efw/ethernet/br2")
            test.uploadfile(mgmtnetaddr, routerConfigFile, "/var/efw/routing/config")

            time.sleep(1)
            log.debug("upload end")
            file_type_red = "file /var/efw/uplinks/main/settings"
            file_type_ethernet = "file /var/efw/ethernet/settings"
            
            result_red_settings_file_type = test.checkconfigfiletype(mgmtnetaddr, file_type_red)
            result_ethernet_settings_file_type = test.checkconfigfiletype(mgmtnetaddr, file_type_ethernet)
            
            result_networkupdate = test.setnetworkconfig(mgmtnetaddr, "jobcontrol request network.updatewizard")
            print result_networkupdate
            time.sleep(1)                                        
            result_updatelinks = test.setnetworkconfig(mgmtnetaddr, " jobcontrol request uplinksdaemonjob.updatewizard")
            print result_updatelinks
            time.sleep(1)
#             test.setnetworkconfig(mgmtnetaddr, " ip route add 10.0.0.0/24 via 192.168.10.1")
        except Exception, e:
            log.debug("exception error %s"%e.message)
            return {"result":"fail"}
            
        return {"result":"success", "message":"/var/efw/uplinks/main/settings, /var/efw/ethernet/settings", "mainconfig":mainconfig, "ethernetconfig":ethernetconfig, "br0":br0config,"br1":br1config,"br2":br2config, "router":routerconfig, "result_red_settings_file_type":result_red_settings_file_type, "result_ethernet_settings_file_type":result_ethernet_settings_file_type, "result_networkupdate":result_networkupdate, "result_updatelinks":result_updatelinks}

    def createmainconffile(self, reqdata):
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        loader = tornado.template.Loader('/usr/local/CloudManager/template')

        red_address = reqdata["configinfo"]["redinfo"]["red_address"]
        red_defaultgateway = reqdata["configinfo"]["redinfo"]["red_defaultgateway"]
        red_broadcast = reqdata["configinfo"]["redinfo"]["red_broadcast"]
        red_cidr = reqdata["configinfo"]["redinfo"]["red_cidr"]
        red_netmask = reqdata["configinfo"]["redinfo"]["red_netmask"]
        red_ips = reqdata["configinfo"]["redinfo"]["red_ips"]
        red_netaddr = reqdata["configinfo"]["redinfo"]["red_netaddr"]
        
        dns1 = reqdata["configinfo"]["dns1"]
        dns2 = reqdata["configinfo"]["dns2"]
            
        generateStr = loader.load("main_settings.template").generate(red_default_gateway=red_defaultgateway,
                                                                     red_addr=red_address,
                                                                     red_broadcast=red_broadcast,
                                                                     red_cidr=red_cidr,
                                                                     red_ips=red_ips,
                                                                     red_netaddr=red_netaddr,
                                                                     red_netmask=red_netmask,
                                                                     dns1 = dns1,
                                                                     dns2 = dns2)
        redfile = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        redfile.write(generateStr)
        redfile.close()
        return "/usr/local/CloudManager/template/"+filename, generateStr

    def createethernetconffile(self, reqdata):
        log.info("----------createethernetconffile")
#         loader = tornado.template.Loader('C:/tmp')
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        loader = tornado.template.Loader('/usr/local/CloudManager/template')

        config_type = reqdata["configinfo"]["configtype"]
        green_address = reqdata["configinfo"]["greeninfo"]["green_address"]
        green_broadcast = reqdata["configinfo"]["greeninfo"]["green_broadcast"]
        green_netaddr = reqdata["configinfo"]["greeninfo"]["green_netaddr"]
        green_ips = reqdata["configinfo"]["greeninfo"]["green_ips"]
        green_netmask = reqdata["configinfo"]["greeninfo"]["green_netmask"]
        green_cidr = reqdata["configinfo"]["greeninfo"]["green_cidr"]

        orange_address = reqdata["configinfo"]["orangeinfo"]["orange_address"]
        orange_broadcast = reqdata["configinfo"]["orangeinfo"]["orange_broadcast"]
        orange_cidr = reqdata["configinfo"]["orangeinfo"]["orange_cidr"]
        orange_ips = reqdata["configinfo"]["orangeinfo"]["orange_ips"]
        orange_netaddr = reqdata["configinfo"]["orangeinfo"]["orange_netaddr"]
        orange_netmask = reqdata["configinfo"]["orangeinfo"]["orange_netmask"]
        
        blue_address = reqdata["configinfo"]["blueinfo"]["blue_address"]
        blue_broadcast = reqdata["configinfo"]["blueinfo"]["blue_broadcast"]
        blue_cidr = reqdata["configinfo"]["blueinfo"]["blue_cidr"]
        blue_ips = reqdata["configinfo"]["blueinfo"]["blue_ips"]
        blue_netaddr = reqdata["configinfo"]["blueinfo"]["blue_netaddr"]
        blue_netmask = reqdata["configinfo"]["blueinfo"]["blue_netmask"]
        
        generateStr = loader.load("ethernet_settings.template").generate(config_type=config_type,
                                                                         green_addr=green_address,
                                                                         green_broadcast=green_broadcast,
                                                                         green_ips=green_ips,
                                                                         green_netaddr=green_netaddr,
                                                                         green_netmask=green_netmask,
                                                                         green_cidr=green_cidr,
                                                                         orange_addr=orange_address,
                                                                         orange_broadcast=orange_broadcast,
                                                                         orange_cidr=orange_cidr,
                                                                         orange_ips=orange_ips,
                                                                         orange_netaddr=orange_netaddr,
                                                                         orange_netmask=orange_netmask,
                                                                         blue_cidr=blue_cidr,
                                                                         blue_addr=blue_address,
                                                                         blue_netmask=blue_netmask,
                                                                         blue_broadcast=blue_broadcast,
                                                                         blue_ips=blue_ips,
                                                                         blue_netaddr=blue_netaddr)
        orangefile = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        orangefile.write(generateStr)
        orangefile.close()
        return "/usr/local/CloudManager/template/"+filename, generateStr    

    def createbr0conffile(self, reqdata):
        filename = str(uuid.uuid1()).replace('-','')+".gen"       
        br0 = reqdata["configinfo"]["br0"]["value"]
        br0file = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        for row in br0:
            br0file.write(row+"\n")
            
        br0file.close()
        return "/usr/local/CloudManager/template/"+filename, br0

    def createbr1conffile(self, reqdata):
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        br1 = reqdata["configinfo"]["br1"]["value"]
        br1file = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        for row in br1:
            br1file.write(row+"\n")
        br1file.close()
        return "/usr/local/CloudManager/template/"+filename, br1

    def createbr2conffile(self, reqdata):
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        
        br2 = reqdata["configinfo"]["br2"]["value"]
        
        br2file = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        
        for row in br2:
            br2file.write(row+"\n")

        br2file.close()
        return "/usr/local/CloudManager/template/"+filename, br2        
    
    def createrouterconffile(self, reqdata):
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        
        routers = reqdata["configinfo"]["routers"]
        
        routerfile = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        
        for row in routers:
            routerfile.write(row +"\n")

        routerfile.close()
        return "/usr/local/CloudManager/template/"+filename, routers    

    def createhostnameconffile(self, hostname, domainname):
        filename = str(uuid.uuid1()).replace('-','')+".gen"
        
       
        hostnamefile = open("/usr/local/CloudManager/template/%s" % filename, 'w')
        
        if domainname == "":
            domainname = "localdomain"
        
        hostnamefile.write("DOMAINNAME="+domainname+"\n"+"HOSTNAME="+hostname)

        hostnamefile.close()
        
        return "/usr/local/CloudManager/template/"+filename, hostname    
        
class VnfmanagerProxyArpHhandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        print self.request.body   
        result = self.checkproxyarp(reqdata)
        self.write(result)

    def checkproxyarp(self, reqdata):
        log.info("----------VnfmanagerProxyArpHhandler")
        utm_mgmt = reqdata["utm_mgmt"]
        cmdlist = reqdata["cmd"]
        test = Testutils()
        for cmd in cmdlist:
            print "cmd -> %s"%cmd
            result = test.checkproxyarpcmd(utm_mgmt, cmd)
            log.debug("checkproxyarp result %s"%result)
            if result == False:
                result = {"result":"fail", "message":"checkproxyarp result fail", "cmd": cmd, "detail": ""}
                return result
        checkcmd = "ebtables -t nat --atomic-file /var/efw/inithooks/ebtables-nat-proxyarp --atomic-save"
        out = test.sendcommand(utm_mgmt, checkcmd)
        checkcmd = "ebtables -t nat -L CUSTOM_PROXY_ARP"
        out = test.sendcommand(utm_mgmt, checkcmd)
        result_checkcmd = ""
        for line in out:
            result_checkcmd = result_checkcmd + line + "<br>"
        print result_checkcmd
        result = {"result":"success", "message":"proxyarp config success", "cmd": cmdlist, "detail": result_checkcmd}
        return result

class VnfmanagerInterfaceStatusHhandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)
        print self.request.body   
        result = self.interfacestatus(reqdata)
        self.write(result)

    def interfacestatus(self, reqdata):
        log.info("----------VnfmanagerInterfaceStatusHhandler")
        utm_mgmt = reqdata["utm_mgmt"]        
        cmdlist = reqdata["status"]
        test = Testutils()
        for cmd in cmdlist:
            print "cmd -> %s"%cmd
            result = test.checkinterfacecmd(utm_mgmt, cmd)
            log.debug("checkinterfacecmd result %s"%result)
            if result == False:
                result = {"result":"fail", "message":"checkinterfacecmd result fail", "cmd": cmd, "detail": ""}
                return result
        result = {"result":"success", "message":"checkinterfacecmd result success", "cmd": cmdlist, "detail": ""}
        return result


class TestmanagerCheckPortHandler(tornado.web.RequestHandler):        
    def get(self, portnum):
#         portnum = self.get_arguments(portnum)
        utm_mgmt = self.get_argument("utm_mgmt")
        result = self.checkport(portnum, utm_mgmt)
        self.write(result)

    def checkport(self, portnum, utmMgmtAddr):
        log.info("----------Testmanagercheckporthandler")
        test = Testutils()
        try:
            if portnum == "22":
                chkportnum = ":::22"
            elif portnum == "10443":
                chkportnum = ":10443"
            result, cmd, out = test.islistenport(chkportnum, utmMgmtAddr)
            if result == False:
                return {"result":"fail", "message":u"포트가 열려있지 않습니다", "cmd": cmd, "detail":out}
            else:
                return {"result":"success", "message":u"포트가 열려있습니다", "cmd": cmd, "detail":out}
        except:
            return {"result":"fail", "message":u"조회하려는 ip가 올바르지 않습니다.", "cmd": cmd, "detail":out}

class TestmanagerCheckRouteHandler(tornado.web.RequestHandler):
    def get(self, searchip):
        utm_mgmt = self.get_argument("utm_mgmt")
        result = self.checkroute(searchip, utm_mgmt)
        self.write(result)

    def checkroute(self, searchip, utm_mgmt):
        log.info("----------TestmanagerCheckRouteHandler")
        test = Testutils()
#         netaddr = test.getnetaddr(searchip)
        result, cmd, out = test.checkroute(utm_mgmt, searchip)
        if result == False:
            return {"result":"fail", "message":searchip+u"를 찾지못했습니다.", "cmd": cmd, "detail": out}
        else:
            return {"result":"success", "message":searchip+u"를 찾았습니다.", "cmd": cmd, "detail": out}

class TestmanagerCheckSecondaryIPHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)   
        searchip = reqdata["searchip"]
        nic = reqdata["nic"]
        utm_mgmt = reqdata["utm_mgmt"]
        result = self.checksecondaryip(searchip, nic, utm_mgmt)
        self.write(result)

    def checksecondaryip(self, searchip, nic, utm_mgmt):
        log.info("----------TestmanagerCheckSecondaryIPHandler")
        test = Testutils()
        result, cmd, out = test.checksecondip(utm_mgmt, searchip, nic)
        if result == False:
            return {"result":"fail", "message":searchip+u"를 찾지못했습니다.", "cmd": cmd, "detail": out}
        else:
            return {"result":"success", "message":searchip+u"를 찾았습니다.", "cmd": cmd, "detail": out}
        
class TestmanagerCheckJobHandler(tornado.web.RequestHandler):
    def post(self):
        reqdata = json.loads(self.request.body)   
        result = self.checkjob(reqdata)
        self.write(result)

    def checkjob(self, reqdata):
        log.info("----------TestmanagerCheckJobHandler")
        test = Testutils()
        reqdata = json.loads(self.request.body)
        utmMgmtAddr = reqdata["mgmtaddr"]
        job = reqdata["job"]
        searchText = reqdata["searchtext"]
        try:
            result, cmd, out = test.checkjobs(utmMgmtAddr, job, searchText)
            if result == False:
                message = u"job %s이 없습니다."%job
                return {"result":"fail", "message":message, "cmd": cmd, "detail":out}
            else:
                message = u"job %s이 존재합니다."%job
                return {"result":"success", "message":message, "cmd": cmd, "detail":out}
        except:
            return {"result":"fail", "message":u"조회하려는 ip가 올바르지 않습니다.", "cmd": cmd, "detail":"addr[%s] job[%s]" %(utmMgmtAddr,job)}

class TestmanagerCheckNic(tornado.web.RequestHandler):
    def get(self, nic):
        utm_mgmt = self.get_argument("utm_mgmt")
        searchip = self.get_argument("searchip")
        result = self.checknic(nic, utm_mgmt, searchip)
        self.write(result)    

    def checknic(self, nic, utm_mgmt, searchip):
        log.info("----------TestmanagerCheckNic")
        test = Testutils()
        result, cmd, out = test.checknic(utm_mgmt, nic, searchip)
        if result == False:
            return {"result":"fail", "message":nic+u" 셋팅이 잘못되었습니다.", "cmd": cmd, "detail": out}
        else:
            return {"result":"success", "message":nic+u" 셋팅이 정상입니다.", "cmd": cmd, "detail": out}
                                                       
class Testutils():
    def islistenport(self, portnum, utmMgmtAddr):
        log.info("----------isListenPort")
        log.info("utmMgmtAddr %s" %utmMgmtAddr)
#         cmd = 'netstat -an | grep LISTEN'
        if "22" in portnum:
            cmd = 'netstat -an | grep :::22 |grep -v 222'
        elif "10443" in portnum:
            cmd = 'netstat -an | grep :10443'
        out = self.sendcommand(utmMgmtAddr, cmd)
        for line in out:
            if portnum in line:
                log.info("result True")
                return True, cmd, out
        log.info("result False")
               
        return False, cmd, out

    def checknic(self, utmMgmtAddr, net, searchText):
        log.info("----------checkNic")
        log.info("utmMgmtAddr %s net %s searchText %s" %(utmMgmtAddr, net, searchText))
        cmd = "ifconfig %s |grep %s"%(net, searchText)
        result = False
        out = self.sendcommand(utmMgmtAddr, cmd)
        for line in out:
            if searchText in line:
                result = True
                break
        log.info("result %s" %result)
        return result, cmd, out

    def checkroute(self, utmMgmtAddr, searchText):
        log.info("----------checkRoute")
        log.info("utmMgmtAddr %s searchText %s" %(utmMgmtAddr, searchText))
        cmd = "route -n |grep %s"%searchText
        result = False
        out = self.sendcommand(utmMgmtAddr, cmd)
        for line in out:
            if searchText in line:
                result = True
                break
        log.info("result %s" %result)
        return result, cmd, out

    def checksecondip(self, utmMgmtAddr, searchText, nic):
        log.info("----------checkRoute")
        log.info("utmMgmtAddr %s searchText %s nic %s" %(utmMgmtAddr, searchText, nic))
        cmd = "ip addr show %s |grep %s"%(nic, searchText)
        result = False
        out = self.sendcommand(utmMgmtAddr, cmd)
        for line in out:
            if searchText in line:
                result = True
                break
        log.info("result %s" %result)
        return result, cmd, out
    
    def checkproxyarpcmd(self, utmMgmtAddr, cmd):
        log.info("----------checkproxyarp")
        log.info("utmMgmtAddr %s cmd %s" %(utmMgmtAddr, cmd))
        result = True
        client = paramiko.SSHClient()
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            client.connect(hostname=utmMgmtAddr, username=user, password=password)
            stdin, stdout, stderr = client.exec_command(cmd)
            log.info("sendcommand --> %s" %cmd)
            for line in stdout:
                print "line %s"%line
                result = False
        except Exception, e:
            log.error("sendcommand error %s" %str(e))
            sys.exit()
        finally:
            client.close()
            return result

    def checkinterfacecmd(self, utmMgmtAddr, cmd):
        log.info("----------checkinterfacecmd")
        log.info("utmMgmtAddr %s cmd %s" %(utmMgmtAddr, cmd))
        result = True
        client = paramiko.SSHClient()
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            client.connect(hostname=utmMgmtAddr, username=user, password=password)
            stdin, stdout, stderr = client.exec_command(cmd)
            log.info("sendcommand --> %s" %cmd)
            for line in stdout:
                print "line %s"%line
                result = False
        except Exception, e:
            log.error("sendcommand error %s" %str(e))
            sys.exit()
        finally:
            client.close()
            return result
        
    def checkproxyarptestcmd(self, utmMgmtAddr,cmd):
        log.info("----------checkproxyarptestcmd")
        log.info("utmMgmtAddr %s cmd %" %utmMgmtAddr)
        result = False
        out = self.sendcommand(utmMgmtAddr, cmd)
        cmdresult = ""
        for line in out:
            cmdresult = cmdresult + line
        log.info("result %s" %result)
        return result, cmd, cmdresult
        
    def checkjobs(self, utmMgmtAddr, job, searchText):
        log.info("----------checkJobs")
        log.info("utmMgmtAddr %s" %utmMgmtAddr)
        cmd = "jobcontrol list|grep %s"%job
        result = False
        out = self.sendcommand(utmMgmtAddr, cmd)
        for line in out:
            if searchText in line:
                result = True
                break
        log.info("result %s" %result)
        return result, cmd, out
            
    def sendcommand(self, ipAddress, cmd):
        log.info("----------sendcommand")
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            client.connect(hostname=ipAddress, username=user, password=password, timeout= 5)
            cmd_list = [cmd]
            req = ';'.join(cmd_list)
            stdin, stdout, stderr = client.exec_command(req)
            log.info("sendcommand --> %s" %req)
            for line in stdout:
                result.append(line.replace('\n','').replace('       ',''))
                log.info(line.replace('\n',''))
        except Exception, e:
            log.error("sendcommand error %s" %str(e))
            sys.exit()
        finally:
            client.close()
            return result

    def sendcommandforproxyarp(self, ipAddress, cmd):
        log.info("----------sendcommand")
        client = paramiko.SSHClient()
        result = []
        try:
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            client.connect(hostname=ipAddress, username=user, password=password)
            stdin, stdout, stderr = client.exec_command(cmd)
            log.info("sendcommand --> %s" %cmd)
        except Exception, e:
            log.error("sendcommand error %s" %str(e))
            sys.exit()
        finally:
            client.close()
            return stdout
        
#     def getnetaddr(self, ipaddr):
#         srcArr = ipaddr.split('.')
#         return srcArr[0] + "." + srcArr[1] + "." + srcArr[2] + ".0"
    
    def uploadfile(self, ipAddress, localFile, remoteFile):
        log.info("----------uploadFile")
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            aes = AESCipher(AESCipherKey['key'])
            user = aes.decrypt(utm_conn_info["user"])
            password = aes.decrypt(utm_conn_info["password"])
            client.connect(hostname=ipAddress, username=user, password=password)
            sftp = client.open_sftp()
            log.info("sftp.put now %s:%s->%s" %(ipAddress, localFile, remoteFile))
            sftp.put(localFile, remoteFile)

            cmd = "chown nobody:nobody %s"%remoteFile
            stdin, stdout, stderr = client.exec_command(cmd)
            log.info("sendcommand --> %s" %cmd)
                        
        except Exception, e:
            sftp.close()
            client.close()
            os.remove(localFile)
            log.error("uploadFile exception  %s" %str(e))

        sftp.close()
        client.close()
        os.remove(localFile)                

    def checkconfigfiletype(self, utmMgmtAdd, cmd):
        log.info("----------checkconfigfiletype")
#         cmd = "jobcontrol request network.updatewizard"
        result = False  
        out = self.sendcommand(utmMgmtAdd, cmd)
        log.info("result --> %s"%out)
        for line in out:
            if "ASCII text" in line:
                result = True
                break
        if result == []:
            result = True
                    
        log.info("result %s" %result)
        return out

    def setnetworkconfig(self, utmMgmtAdd, cmd):
        log.info("----------setnetworkconfig")
#         cmd = "jobcontrol request network.updatewizard"
        result = False  
        out = self.sendcommand(utmMgmtAdd, cmd)
        log.info("result --> %s"%out)
        for line in out:
            if "Emit" in line:
                result = True
                break
        if result == []:
            result = True
                    
        log.info("result %s" %result)
        return out

class Test(tornado.web.RequestHandler):
    def get(self):
        self.write("hello world")
                               
application = tornado.web.Application([
        (r'/fileupload', SyncmanagerHandler),
        (r'/utmconfig', VnfmanagerConfigfileHhandler),
        (r'/proxyarp', VnfmanagerProxyArpHhandler),
        (r'/interfacestatus', VnfmanagerInterfaceStatusHhandler),        
        (r'/check/port/(?P<portnum>[^\/]+)/?', TestmanagerCheckPortHandler),
        (r'/check/route/(?P<searchip>[^\/]+)/?', TestmanagerCheckRouteHandler),
        (r'/check/secondary', TestmanagerCheckSecondaryIPHandler),
        (r'/check/net/(?P<nic>[^\/]+)/?', TestmanagerCheckNic),
        (r'/check/jobs', TestmanagerCheckJobHandler),
        (r'/test', Test),
])

def main():
    try:
        tornado.options.parse_command_line()
        log.info("Start the UTM network configuration service ")
#         ssl_options = {'certfile': os.path.join(settings.SSL_PATH, 'certificate.crt'),
#                    'keyfile': os.path.join(settings.SSL_PATH, 'privateKey.key'),
#                    }
#         http_server = tornado.httpserver.HTTPServer(application, ssl_options=ssl_options)
        http_server = tornado.httpserver.HTTPServer(application)
#         http_server.listen(8080)
        http_server.bind(80)
        http_server.start(0)        
#         http_server.listen(8080)
        tornado.ioloop.IOLoop.instance().start()

    except KeyboardInterrupt:
        log.exception("Stop the UTM network configuration service")
        
if __name__ == '__main__':
    main()            