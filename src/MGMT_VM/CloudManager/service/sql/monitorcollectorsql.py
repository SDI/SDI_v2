# -*- coding: utf-8 -*-
'''
Created on 2015. 1. 14.

@author: 진수
'''
from time import strptime
import time

class OpenstackHostStateInfoSql(object):
    '''
    DB 쿼리를 집합.
    '''


    def __init__(self):
        '''
        Constructor
        '''

    def insertOpenstackHostStateInfo(self, createTime, orgseq, host, hostip, service, component, processcount):
        result = """INSERT INTO tb_monitor_openstack_host_state_info (create_time, orgseq, host, hostip, service, component, pcount)
                    VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, '%s', '%s', '%s', '%s', %d)
        """ % (createTime, orgseq, host, hostip, service, component, processcount )
        return result

    def insertOpenstackSwitchStateInfo(self, createTime, orgseq, switchName, ipaddr, port, ifOperStatus, ifType, ifSpeed, ifName):
        result = """INSERT INTO tb_monitor_openstack_switch_state_info(
            create_time, orgseq, switchname, ipaddr, port, ifoperstatus,
            iftype, ifspeed, ifname)
        VALUES (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, '%s', '%s', %d, %d,
             %d, %f, '%s')
        """  %(createTime, orgseq, switchName, ipaddr, port, ifOperStatus, ifType, ifSpeed, ifName )

#         print result
        return result

    def insertOpenstackInstanceStateInfo(self, createtime, orgseq, vmid, status, vmname, hostname, resource, usertype, prdname, conntracklist,completeyn, createvm):
#         if len(resource) > 0:
#             conntracklist = conntracklist.replace('tcp',' <br> tcp').replace('udp',' <br> udp')
#         print "------"*50
#         print "resource[3] %s"%resource[3]
#         print "conntracklist %s"%conntracklist
        result = """INSERT INTO tb_monitor_openstack_instance_state_info(
            create_time, orgseq, vmid, status, vmname, host, cpu, mem, disk, usertype, productname, conntrackcnt, conntracklist, completeyn, createvm)
        VALUES (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), '%s', '%s', '%s', '%s', '%s', %d, %d, %d, '%s', '%s', '%s', '%s', '%s', '%s')
        """ %(createtime, orgseq, vmid, status, vmname, hostname, float(resource[0]), float(resource[1]), float(resource[2]), usertype, prdname, resource[3], conntracklist, completeyn, createvm )
#         print "query %s"%result
#         print "query len %s"%len(result)
        return result

    def selectsmsdata(self, orgseq, hostname, objecttype, objectname, status):
        result = """SELECT count(*) as count FROM tb_monitor_sms_data
        WHERE orgseq = %s and host_name = '%s' and object_type = '%s' and object_name = '%s' and status = '%s'
        and create_time > (now() - interval '60 minutes') and sms_send_yn = 'N' and sms_check_yn = 'N'
        """%(orgseq, hostname, objecttype, objectname, status)
        
        return result

    def insertsmsdata(self, createtime, orgseq, hostname, objecttype, objectname, status):
        result = """INSERT INTO tb_monitor_sms_data(create_time, orgseq, host_name, object_type, object_name, status, sms_send_yn, sms_check_yn)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s', 'N', 'N')
        """%(createtime, orgseq, hostname, objecttype, objectname, status)
    
        return result

    def selectOpenstackHostStateInfoMnnode(self, orgseq):
        result = """select coh.cnt, oh.host, oh.service, oh.component, oh.pcount
                    from tb_monitor_openstack_host_state_info as oh
                    inner join
                    (select ct.host, ct.service, ct.orgseq, count(*) as cnt
                    from tb_monitor_openstack_host_state_info as ct
                    where ct.create_time =
                    (select MAX(create_time) from tb_monitor_openstack_host_state_info)
                    and ct.orgseq = %s
                    group by ct.host, ct.service, ct.orgseq) as coh
                    on oh.host = coh.host and oh.service = coh.service
                    where oh.create_time =
                    (select MAX(create_time) from tb_monitor_openstack_host_state_info)
                    and oh.orgseq = %s
                    group by coh.cnt, oh.host, oh.service, oh.component, oh.pcount, oh.orgseq
                    order by oh.orgseq """ % (orgseq, orgseq)
        return result

    def selectOpenstackSwitchStateInfoMnnode(self, orgseq):
        result = """select switchname, port, ifoperstatus, iftype, ifspeed, ifname from tb_monitor_openstack_switch_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_switch_state_info)
                    and orgseq = %s
                    order by switchname, port""" % orgseq
        return result

    def selectOpenstackInstanceStateInfo(self, orgseq):
        result = """select vmid, status, vmname, host
                    from tb_monitor_openstack_instance_state_info as oi
                    where oi.create_time =
                    (select MAX(create_time) from tb_monitor_openstack_instance_state_info)
                    and orgseq = %s""" % orgseq
        return result

    def selectOrgTree(self):
        result = """select orgseq, orgname from tb_org order by orgseq"""
        return result

    def insertOpenstackStateChange(self, create_time, orgseq, infotype, data_type, confirm_yn, collect_data):
        result = """INSERT INTO tb_openstack_state_change(create_time, orgseq, infotype, data_type, reg_dttm, confirm_yn, collect_data)
                    VALUES (to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, %d, '%s', now(), '%s', '%s'::json);""" %(create_time, orgseq, infotype, data_type, confirm_yn, collect_data)
        return result

    def selectOpenstackInstanceStateInfoExt(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, vmid, status, vmname, host, conntrackcnt as conntrack, completeyn, createvm, 'T' as check
                    from tb_monitor_openstack_instance_state_info as oi
                    where oi.create_time =
                    (select MAX(create_time) from tb_monitor_openstack_instance_state_info)
                    and orgseq = %s """ % orgseq
        return result

    def selectOpenstackSwitchStateInfoMnnodeExt(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq,
                    switchname, ipaddr, port, ifoperstatus, iftype, ifspeed, ifname, 'T' as check
                    from tb_monitor_openstack_switch_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_switch_state_info)
                    and orgseq = %s
                    order by switchname, port""" % orgseq
        return result

    def selectOpenstackHostStateInfoMnnodeExt(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, host, hostip, service, component, pcount, 'T' as check
                    from tb_monitor_openstack_host_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_host_state_info)
                    and orgseq = %s
                    order by host, service, component""" % orgseq
        return result

    def selectNeUertypeInfo(self, vmid):
        result = """select count(*) as count from tb_ne where vm_id = '%s' and usgstatuscode = '1' and del_dttm is null and console_url is not null
        """%vmid
        return result

    def selectProductname(self, vmid):
        result = """select modelname from tb_necat nc
                    inner join tb_ne ne on ne.necatseq =  nc.necatseq
                    where ne.vm_id = '%s'
        """ %vmid
        return result

    def selectopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s       
        """%orgseq
        return result

class CustomerServerStateInfoSql(object):
    
    def __init__(self):
        '''
        Constructor
        '''
            
    def selectCustomerServerStateInfoExt(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, customername, servername, serverip, servertype, status, 'T' as check
                    from tb_monitor_customer_server_state_info as oi
                    where oi.create_time =
                    (select MAX(create_time) from tb_monitor_customer_server_state_info)
                    and orgseq = %s """ % orgseq
        return result
    
    def selectCustomerServerList(self, orgseq):
        result = """
        select cs.server_ip as serverip, cs.servername as servername, cs.server_type as servertype, ct.customername as customername 
        from tb_customer_server_info cs
        inner join tb_user us on us.customerseq = cs.customerseq
        inner join tb_customer ct on ct.customerseq = cs.customerseq
        where us.orgseq = %s
        """%orgseq
        return result

    def insertCustomerServerStateInfo(self, createTime, orgseq, customername, servername, serverip, servertype, status):
        result = """INSERT INTO tb_monitor_customer_server_state_info 
        (create_time, orgseq, customername, servername, serverip, servertype, status)
                    VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %d, '%s', '%s', '%s', '%s', '%s')
        """ % (createTime, orgseq, customername, servername, serverip, servertype, status)
        return result

class SdnApplianceStateInfoSql(object):
    def __init__(self):
        '''
        Constructor
        '''
        
    def selectSdnApplianceStateInfoExt(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, appliance_id as applianceid, appliance_type as appliancetype, appliance_name as appliance_name, ip_addr as ipaddr, port_num as portnum, switch_dpid as switchdpid, connect_type as connecttype, status, companyname, 'T' as check
        from tb_monitor_sdn_appliance_state_info as oi
        where oi.create_time =
        (select MAX(create_time) from tb_monitor_sdn_appliance_state_info)
        and orgseq = %s""" % orgseq
        return result    

    def selectSdnApplianceList(self, orgseq):
        result = """
        select sd.appliance_id as applianceid, sd.appliance_type as appliancetype, sd.appliance_name as appliancename, sd.ip_addr as ipaddr, sd.port_num as portnum, sd.switch_dpid as switchdpid, sd.connect_type as connecttype
        from tb_sdn_appliance sd
        inner join tb_org og on og.officecode = sd.officecode
        where sd.appliance_type = 'HW_UTM' and og.orgseq = %s
        """%orgseq
        return result

    def insertSdnApplianceStateInfo(self, createTime, orgseq, applianceid, appliancetype, appliancename, ipaddr, portnum, switchdpid, connecttype, status, companyname):
        result = """INSERT INTO tb_monitor_sdn_appliance_state_info 
        (create_time, orgseq, appliance_id, appliance_type, appliance_name, 
            ip_addr, port_num, switch_dpid, connect_type, status, companyname)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s', %s, '%s', '%s', '%s', '%s')
        """ % (createTime, orgseq, applianceid, appliancetype, appliancename, ipaddr, portnum, switchdpid, connecttype, status, companyname)
        return result

class CloudServerStateInfoSql(object):
    
    def selectOpenstackServerStateInfo(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, host, hostip, status, 'T' as check
                    from tb_monitor_openstack_server_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_server_state_info)
                    and orgseq = %s
                    order by host""" % orgseq
        return result    


    def getopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s
        """%orgseq
        return result   

    def insertOpenStackServerStateInfo(self, createTime, orgseq, host, hostip, status):
        result = """INSERT INTO tb_monitor_openstack_server_state_info 
        (create_time, orgseq, host, hostip, status)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s')
        """ % ( createTime, orgseq, host, hostip, status)
        return result

class SwitchStateInfoSql(object):
    
    def selectSwitchStateInfo(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, switch, switchip, status, 'T' as check
                    from tb_monitor_openstack_switch_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_switch_state_info)
                    and orgseq = %s
                    order by switch""" % orgseq
        return result    


    def getopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s
        """%orgseq
        return result   

    def insertOpenStackSwitchStateInfo(self, createTime, orgseq, switch, switchip, status):
        result = """INSERT INTO tb_monitor_openstack_switch_state_info 
        (create_time, orgseq, switch, switchip, status)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s')
        """ % ( createTime, orgseq, switch, switchip, status)
        return result         

class SdnSwitchStateInfoSql(object):
    
    def selectSdnSwitchStateInfo(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, sdnswitchdpid, sdnswitch, sdnswitchip, status, 'T' as check
                    from tb_monitor_openstack_sdnswitch_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_switch_state_info)
                    and orgseq = %s
                    order by sdnswitch""" % orgseq
        return result    


    def getsdnswitchinfo(self, orgseq):
        result = """select sdn.switch_dpid, sdn.switch_name,sdn.mgmt_ip from tb_sdn_switch sdn inner join tb_org og on og.officecode = sdn.officecode 
        where og.orgseq = %s
        """%orgseq
        return result   

    def insertOpenStackSdnSwitchStateInfo(self, createTime, orgseq, sdnswitchdpid, sdnswitchname, sdnswitchip, status):
        result = """INSERT INTO tb_monitor_openstack_sdnswitch_state_info 
        (create_time, orgseq, sdnswitchdpid, sdnswitch, sdnswitchip, status)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s')
        """ % ( createTime, orgseq, sdnswitchdpid, sdnswitchname, sdnswitchip, status)
        return result             

class SdnIPmappingStateInfoSql(object):
    def selectSdnIPMappingStateInfo(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active, 'T' as check
                    from tb_monitor_openstack_utm_switchover_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_utm_switchover_state_info)
                    and orgseq = %s
                    order by source_ip""" % orgseq
        return result    


    def getSdnIPMappinginfo(self, orgseq):
        result = """select sdn.source_ip, sdn.source_ip_active,sdn.dest_ip,sdn.dest_ip_active from tb_sdn_ip_mapping sdn inner join tb_org og on og.officecode = sdn.officecode 
        where og.orgseq = %s
        """%orgseq
        return result   

    def insertSdnIPMappingStateInfo(self, createTime, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active):
        result = """INSERT INTO tb_monitor_openstack_utm_switchover_state_info 
        (create_time, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s')
        """ % ( createTime, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active)
        return result     

class CollectorProcessStateInfoSql(object):
    
    def selectCollectorProcessStateInfo(self, orgseq):
        result = """select to_char(create_time, 'YYYY-MM-DD HH24:MI:SS') as create_time, orgseq, process_name, process_status, 'T' as check
                    from tb_monitor_openstack_process_monitoring_state_info
                    where create_time =
                    (select MAX(create_time) from tb_monitor_openstack_process_monitoring_state_info)
                    and orgseq = %s
                    order by process_name""" % orgseq
        return result    

    def getProcessNameinfo(self, orgseq):
        result = """select process_name from tb_collector_monitor_process_list where orgseq = %s
        """%orgseq
        return result   
    
    def insertCollectorProcessStateInfo(self, createTime, orgseq, process_name, process_status, cmd, out):
        result = """INSERT INTO tb_monitor_openstack_process_monitoring_state_info 
        (create_time, orgseq, process_name, process_status, cmd, cmd_result)
        VALUES(to_timestamp('%s', 'yyyy-mm-dd hh24:mi:ss'), %s, '%s', '%s', '%s', '%s')
        """ % ( createTime, orgseq, process_name, process_status, cmd, out)
        return result  

    def getopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s
        """%orgseq
        return result            
if __name__ == '__main__':
    pass