#-*- coding: utf-8 -*-
'''
@project: Service Orchestrator V2.0
@copyright:  © 2015 kt corp. All rights reserved.

@created: 2015. 2. 6.
@summary:
@author: umisue
'''
class Provisioningsql(object):
    '''
    프로비저닝 관련 DB 쿼리를 집합.

    '''

    def __init__(self):
        '''
        Constructor
        '''
    def settraceforprovision(self, trace):

        print "logstep %s %s"%(trace["logstep"], trace["stepmsg"])

#         print "cmdtype %s"%trace["cmdtype"]
#         print "managegroup %s"%trace["managegroup"]
#         print "managetype %s"%trace["managetype"]
#         print "provisionseq %s"%trace["provisionseq"]
#         print "userid %s"%trace["userid"]
#         print "stepmsg %s"%trace["stepmsg"]
#         print "indent %s"%trace["indent"]
#         print "logstep %s"%trace["logstep"]
#         print "msglevel %s"%trace["msglevel"]
#         print "stepmsgcmd %s"%trace["stepmsgcmd"]
#         print "stepmsgdetails %s"%trace["stepmsgdetails"]
#         print "resourceid %s"%trace["resourceid"]

        try:

            if trace["svctype"] == None:
                trace["svctype"] = 99

            result = """
                        insert into tb_provision_trace
                        (cmd_type, manage_group, manage_type, provisionseq, userid,
                        step_msg, indent, step, msg_level, step_msg_cmd,
                        step_msg_details, svc_type, resource_id, reg_dttm)
                        values('%s', '%s', %s, %s, '%s',
                        '%s', %s, '%s', %s, '%s',
                        '%s',  %s, '%s',now())
                    """%(trace["cmdtype"], trace["managegroup"], trace["managetype"], trace["provisionseq"], trace["userid"],
                         trace["stepmsg"], trace["indent"], trace["logstep"], trace["msglevel"],trace["stepmsgcmd"],
                         trace["stepmsgdetails"], trace["svctype"], trace["resourceid"])

        except Exception,e:
            print e.message

        return result

    def getuserinfofordeleteprovisioning(self, provisionseq):
        result = """
                    select b.userid as userid,
                    convert_from(decrypt(decode((b.password), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
                    a.res_type as restype, a.res_id as resid, a.res_name as resname
                    from tb_openstack_resource a
                    inner join tb_user b on a.userid = b.userid
                    where a.provisionseq = %s
        """ %provisionseq
        return result

    def setnsprovisioning(self, provisionseq):
        result = """
                    update tb_nsprovisioning set procstatecode = '0', del_dttm = now()
                    where nsprovisioningseq = %s
        """ %provisionseq
        return result

    def setne(self, provisionseq):
        result = """
        update tb_ne set usgstatuscode = '2', del_dttm = now() where provisionseq = %s
        """ %provisionseq

        return result

    def setvlan(self, userid):

        result = """
        update tb_vlan set allocate_yn = 'N', userid = null where userid = '%s'
        """ %userid

        return result


    def setnsprovisioningforcreateprovision(self, userid, orgseq):
        print "userid %s"%userid
        print "orgseq %s"%orgseq
        result1 = """
        insert into tb_nsprovisioning (userid, orgseq) values ('%s', %s)
        """%(userid, orgseq)

        result2 = """
        select max(nsprovisioningseq) as nsprovisioningseq from  tb_nsprovisioning where userid = '%s' and orgseq =  %s
        """%(userid, orgseq)

        return result1, result2

    def setneforcreateprovision(self, provisionseq, templateseq, cloidifyid, vmid, application, reqdata):
        result = """
        insert into tb_ne ( neclsseq, necatseq, templateseq, provisionseq, usgstatuscode, cloudifyid, vm_id, config_props)
        values (%s, %s, %s, %s, %s, ,'%s', '%s', '%s'::json)
        """%(application["neclsseq"], application["necatseq"], templateseq, provisionseq, 1, cloidifyid, vmid, reqdata)
        return result

    def getuserinfoforcreateprovision(self, userid):
        result = """
        select userid,
        convert_from(decrypt(decode((select password from tb_user where userid = '%s'), 'hex'), 'gigaoffice!234', '3des'), 'SQL_ASCII') as password,
        username, userauth, orgseq, customerseq, role_id from tb_user where userid = '%s'
        """%(userid, userid)

        return result

    def setvlanforcreateprovision(self, userid, greenvlan, orangevlan):
        result = """
        update tb_vlan set allocate_yn = 'Y', userid = '%s' where vlan in (%s, %s)
        """%(userid, greenvlan, orangevlan)
        return result

    def setfirstprovisionstepforprovision(self, provisionseq, stepstatus, stepmessage):
#         print "workstep %s %s"%(stepstatus, stepmessage)
        result = """
        insert into tb_provision_step(provisionseq, step, step_message, upd_dttm)
        values(%s, %s, '%s', now())
        """%(provisionseq, stepstatus, stepmessage)
        return result

    def setchangeprovisionstepforprovision(self, provisionseq, stepstatus, stepmessage):
#         print "workstep %s %s"%(stepstatus, stepmessage)
        result = """
        update tb_provision_step set step = %s, step_message = '%s', upd_dttm = now() where provisionseq = %s
        """%(stepstatus, stepmessage, provisionseq)
        return result

    def getopenstackinfra(self, orgseq):
        result = """select infraprops from tb_openstack_infra where orgseq = %s
        """%orgseq
        return result    