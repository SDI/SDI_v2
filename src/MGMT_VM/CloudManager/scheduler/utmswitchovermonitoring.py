#-*- coding: utf-8 -*-
'''
Created on 2015. 1. 12.

@author: umisue
@summary: 수집모듈.
        openstack(mn_node, cnode01, cnode02) 관련 상태정보 수집
        switch(aggr, tor) 관련 상태정보 수집
        openstack instance 상태정보 수집

Modified on 2015. 01. 28 모니터링 모듈 추가
                         tb_openstack_state_change 변경내용 저장
Modified on 2015. 03. 03 모니터링 모듈 추가
                         tb_customer_servier_state_info 수집 대상 추가                         
'''
import collections, datetime, json, os, sys 

from apscheduler.schedulers.blocking import BlockingScheduler
import psycopg2
import ssl
from websocket import create_connection
from config.connect_config import db_conn_info, websocket_conn_info, AESCipherKey
from helper.logHelper import myLogger
from sql.monitorcollectorsql import SdnIPmappingStateInfoSql
from util.aescipher import AESCipher
from sql.provisioningsql import Provisioningsql
from config.monitor_config import org

org_dir= os.getcwd()
os.chdir("../")
usr_add_dir = os.getcwd()
sys.path.append(usr_add_dir)
os.chdir(org_dir)
log = myLogger(tag='utm_switchover_monitoring', logdir='./log', loglevel='debug', logConsole=True).get_instance()

orgseq = org["orgseq"]

class UtmSwitchOver(object):
    global dic_utmover
    dic_utmover = []
    d_utmover = collections.OrderedDict()   
        
    def __init__(self):
        self.org_dir= os.getcwd()
        self.init_config_from_dict_config_file()
        self.sdnsql = SdnIPmappingStateInfoSql()

    def init_config_from_dict_config_file(self):
        aes = AESCipher(AESCipherKey['key'])
        self.WS_URL = aes.decrypt(websocket_conn_info['url']) 
        self.dbdatabase = aes.decrypt(db_conn_info['db'])
        self.dbuser = aes.decrypt(db_conn_info['id'])
        self.dbpassword = aes.decrypt(db_conn_info['pw'])
        self.dbhost = aes.decrypt(db_conn_info['db_host'])
        self.dbport = aes.decrypt(db_conn_info['port'])
        
    def initdicUtmOver(self):
        try:
            log.info("#"*100)
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            conn.autocommit = True
            cur = conn.cursor()
            myquery = self.sdnsql.selectSdnIPMappingStateInfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            for row in rows:
                d_utmover = dict(zip(columns, row))
                dic_utmover.append(d_utmover)
            log.info("initdicUtmSwitchOver %s" %len(dic_utmover))
        except psycopg2.DatabaseError, e:
#                 print 'Error %s' % e
            log.error('[%s]Database ExceptionError %s' %e)
        except Exception, e:
            log.error('[%s]Exception Error %s' %e)
                
    def run(self, invokeTime):
        try:
            self.invokeTime = invokeTime
            conn = psycopg2.connect(database=self.dbdatabase, user=self.dbuser, password=self.dbpassword, host=self.dbhost, port=self.dbport)
            cur = conn.cursor()          
            myquery = self.sdnsql.getSdnIPMappinginfo(orgseq)
            cur.execute(myquery)
            rows = cur.fetchall()
            columns = [desc[0] for desc in cur.description]
            utmlist = []
            for row in rows:
                utmlist.append(dict(zip(columns, row)))
            for  utm in utmlist:
                print json.dumps(utm)
                source_ip = utm["source_ip"]
                source_ip_active =  utm["source_ip_active"]
                dest_ip =  utm["dest_ip"]
                dest_ip_active =  utm["dest_ip_active"]

                myquery = self.sdnsql.insertSdnIPMappingStateInfo(self.invokeTime, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active)
                cur.execute(myquery)
                result_utmover, checkData = self.checkutmSwitchoverStatus(self.invokeTime, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active)
                log.debug("[%s] source_ip %s status %s dest_ip %s status %s"%(result_utmover, source_ip, source_ip_active, dest_ip, dest_ip_active))
                if (result_utmover <> "EXIST")  :
                    if result_utmover == "CHANGE":
                        data_type = "C"
                    elif result_utmover == "FIRST":
                        data_type = "I"
                    else:
                        data_type = "N"
#                     changequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, data_type, "N", checkData)
#                     cur.execute(changequery)
                    sendData = {"command":"notifyAll", "user_type":"1", "data_type":data_type, "alarm_type":"0", "alarm_detail":"", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":8, "collect_data":checkData}
                    sendData_json = json.dumps(sendData)
                    self.sendBroadcasting(sendData_json)                       
            log.info("initdicUtmSwitchover %s" %len(dic_utmover))           
            data_len = len(dic_utmover)
            for index in range(0, data_len) :
                if dic_utmover[data_len-(index + 1)]["check"] == "F":
                    send_delete_data = {"command":"notifyAll", "data_type":"D", "create_time":self.invokeTime, "orgseq": orgseq, "infotype":8, "collect_data":dic_utmover[data_len-(index + 1)]}
                    send_delete_data_json = json.dumps(send_delete_data)
                    dic_utmover.pop(data_len-(index + 1))
#                     deletedequery = self.monitorsql.insertOpenstackStateChange(self.invokeTime, orgseq, 4, "D", "N", json.dumps(dic_utmover[data_len-(index + 1)]))
#                     cur.execute(deletedequery)                        
                    self.sendBroadcasting(send_delete_data_json)
            self.checkResetCustomer()            
            conn.commit()
        except Exception, e:
            log.error("UtmSwitchover Monitor exception error %s"%e.message)
            if conn is not None:
                conn.rollback()
        finally:
            if conn is not None:
                conn.close()
    
    def checkResetCustomer(self):
        for row in dic_utmover:
            row["check"] = "F"
            
    def checkutmSwitchoverStatus(self, create_time, orgseq, source_ip, source_ip_active, dest_ip, dest_ip_active):
        count = 0
        for row in dic_utmover:
            if row["orgseq"] == orgseq and row["source_ip"] == source_ip and row["dest_ip"] == dest_ip:
                row["check"] = "T"
                if row["source_ip_active"] == source_ip_active and row["dest_ip_active"] == dest_ip_active:
                    return "EXIST", json.dumps(dic_utmover[count])
                else:
                    row["source_ip"] = source_ip
                    row["dest_ip"] = dest_ip
                    row["source_ip_active"] = source_ip_active
                    row["dest_ip_active"] = dest_ip_active
#                     print "[%d]change status"%count
                    return "CHANGE",json.dumps(dic_utmover[count])
            count = count + 1

        d_utmover = {"create_time": create_time,"orgseq":orgseq, "source_ip":source_ip, "source_ip_active":source_ip_active, "dest_ip":dest_ip, "dest_ip_active": dest_ip_active, "check":"T"}
        dic_utmover.append(d_utmover)
        return "FIRST",json.dumps(d_utmover)
    
    def sendBroadcasting(self, sendData):
        try:
            print "#"*100
            print self.WS_URL            
#             cert_path=os.path.join(os.path.dirname(__file__), "cert")
            settings = dict(
#                 ssl_options = {
#                     "certfile": os.path.join(cert_path, "server.crt"),
#                     "keyfile": os.path.join(cert_path, "server.key")
#                 }
                sslopt={"cert_reqs": ssl.CERT_NONE}
            )            
            ws = create_connection(self.WS_URL, **settings)
            ws.send(sendData)
            result = ws.recv()
            log.debug("#"*100)
            log.debug("UTM SwitchOver send(received) data:%s", result)
#             log.debug("#"*80)
            ws.close()
        except Exception, e:
            log.error("Socket Error %s" %e)
                                   
sched = BlockingScheduler()

aes = AESCipher(AESCipherKey['key'])

dbdatabase = aes.decrypt(db_conn_info['db'])
dbuser = aes.decrypt(db_conn_info['id'])
dbpassword = aes.decrypt(db_conn_info['pw'])
dbhost = aes.decrypt(db_conn_info['db_host'])
dbport = aes.decrypt(db_conn_info['port'])

sql = Provisioningsql()
conn = psycopg2.connect(database=dbdatabase, user=dbuser, password=dbpassword, host=dbhost, port=dbport)
conn.autocommit = True
cur = conn.cursor()
query = sql.getopenstackinfra(orgseq)
cur.execute(query)
rows = cur.fetchone()

if "utmswitchovermonitoring" in rows[0]["process_period"]:
    period = rows[0]["process_period"]["utmswitchovermonitoring"]
else:
    period = "10"
                  
@sched.scheduled_job('cron', second=period)
def Utm_switchover_job():
    starttime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("Utm_switchover_job start. %s period[%s]" %(starttime, period))
    log.debug("dicutmoverSize[%d]"%len(dic_utmover))
    log.debug("-"*50)
    UtmSwitchOver().run(starttime)
    endtime = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    log.debug("-"*50)
    log.debug("dicutmoverSize[%d]"%len(dic_utmover))
    log.debug("Utm_switchover_job end. %s" %endtime)
    log.debug("-"*50)
             
def main():
    log.info("Scheduler Start")
    UtmSwitchOver().initdicUtmOver()
    sched.start()

if __name__ == '__main__':
    main()
